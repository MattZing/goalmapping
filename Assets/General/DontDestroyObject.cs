﻿using UnityEngine;
using System.Collections;

public class DontDestroyObject : MonoBehaviour {

	// Don't destroy this object
	void Start () {
		GameObject.DontDestroyOnLoad(this);
	}
}
