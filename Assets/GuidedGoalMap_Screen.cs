using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using LitJson;

public class GuidedGoalMap_Screen : MenuScreen
{
	private const int numberOfGoals = 5;
	private const int numberOfWhys = 3;
	private const int numberOfHows = 3;
	private const int numberOfWhos = 3;
	private const int numberOfCharsInInputs = 85;
	private const int numberOfDownloadSteps = 9;
	private int minFontSize = 0;
	private int maxFontSize = 0;
	
	private bool saveInProgress = false;
//	private bool loadShareScreen = false;
	private bool dontResetGMIndex = false;
	
	//Signature texture
	private Texture2D sigtex;
	private GameObject sigBox;
	
	//User editable pictures
	private List<GameObject> page1goals;
	private List<GameObject> page2goals;
	private List<GameObject> page3goals;
	private List<GameObject> page4goals;
	private List<GameObject> page1Whys;
	private List<GameObject> page1Hows;
	private List<GameObject> page1Whos;
	private List<GameObject> page2Whys;
	private List<GameObject> page2Hows;
	private List<GameObject> page2Whos;
	private List<GameObject> videoObjects;
	private List<GameObject> hideWithVideo;
	private GameObject localCanvas;
	private GameObject editCanvas;
	private GameObject picMainGoalInner;
	private GameObject picMainGoalLabel;
	private GameObject picSubGoal1inner;
	private GameObject picSubGoal1Label;
	private GameObject picSubGoal2inner;
	private GameObject picSubGoal2Label;
	private GameObject picSubGoal3inner;
	private GameObject picSubGoal3Label;
	private GameObject picSubGoal4inner;
	private GameObject picSubGoal4Label;
	private GameObject drawWhy1inner;
	private GameObject drawWhy1label;
	private GameObject drawWhy2inner;
	private GameObject drawWhy2label;
	private GameObject drawWhy3inner;
	private GameObject drawWhy3label;
	private GameObject why1label;
	private GameObject why2label;
	private GameObject why3label;
	private GameObject drawHow1inner;
	private GameObject drawHow1label;
	private GameObject drawHow2inner;
	private GameObject drawHow2label;
	private GameObject drawHow3inner;
	private GameObject drawHow3label;
	private GameObject how1label;
	private GameObject how2label;
	private GameObject how3label;
	private GameObject drawWho1label;
	private GameObject drawWho2label;
	private GameObject drawWho3label;
	private GameObject who1inner;
	private GameObject who1label;
	private GameObject who2inner;
	private GameObject who2label;
	private GameObject who3inner;
	private GameObject who3label;
	private GameObject startDateLabelHow;
	private GameObject stopDateLabelHow;
	private GameObject drawStartDateLabelHow;
	private GameObject drawStopDateLabelHow;
	private GameObject drawStartDateLabelWho;
	private GameObject drawStopDateLabelWho;
	private GameObject startDateLabelWho;
	private GameObject stopDateLabelWho;
	private GameObject changeTextPicGoalMapButton;
	private GameObject videoBackground;
	private GameObject videoTitleBackground;
	private GameObject playButtonObject;
#if !UNITY_ANDROID && !UNITY_IOS
	private GameObject muteButtonObject;
	private GameObject volumeSliderObject;
#endif
	private GameObject audioPositionSliderObject;
	private GameObject currentTimeLabel;
	private GameObject totalTimeLabel;
	private GameObject skipButton;
	private GameObject page12SignHereText;
	private GameObject popup;
	
	private GameObject saveScreenshotFileName;
	private GameObject goalMapPrefab;
	private GameObject saveGMFileName;
	private GameObject showIntroVideoBackButton;
	private GameObject savingSpinner;
	private GameObject menuBG;
	private GameObject window;
	private GameObject saveLabel;
	private GameObject saveGoalMapButton;
	private GameObject closeButtonBackground;
	
	private GameObject titleBar;
	
	private Sprite[] zoomInSprite;
	private Sprite[] zoomOutSprite;
	private Sprite[] rotateSprite;
	private Sprite[] changeTextPicSprite;
	private Sprite[] doneSprite;
	private Sprite videoImg;
	private Sprite mapBack;
	private Sprite mapBackInner1;
	private Sprite mapBackInner2;
	private Sprite mapBackInner3;
	private Sprite mapBackInner4;
	private Sprite mapBackInner5;
	private Sprite goalMapExample;
	private Sprite upArrowIcon;
	private Sprite downArrowIcon;
	private Sprite leftArrowIcon;
	private Sprite rightArrowIcon;
	private Sprite spinnerSprite;
	private Sprite videoBackgroundSprite;
	private Sprite[] playSprite;
	private Sprite[] skipSprite;
#if !UNITY_ANDROID && !UNITY_IPHONE
	private Sprite muteSprite;
	private Sprite unmuteSprite;
#endif
	private Sprite arrowSprite;
	private Sprite shareIcon;
	private Sprite mainIcon;
	private Texture2D plusTex;
	private Texture2D minusTex;
	
#if !UNITY_ANDROID && !UNITY_IPHONE
	private Texture2D drawTex;

	private MovieTexture introClip;
	private MovieTexture dreamClip;
	private MovieTexture orderClip;
	private MovieTexture drawClip;
	private MovieTexture whyClip;
	private MovieTexture whenClip;
	private MovieTexture howClip;
	private MovieTexture whoClip;
	private MovieTexture signClip;
	private MovieTexture tempMovieTex;
#endif

#if UNITY_ANDROID || UNITY_IPHONE
//	private bool debugSkipVideos = false;
#endif

	private bool showVideo = true;
	private bool goalMapRotated = false;
	public bool wantsEditting = true;
	private bool savingPopupCreated = false;
	private bool saveComplete = false;
	private bool playing = true;
#if !UNITY_ANDROID && !UNITY_IPHONE
	private bool mute = false;
	private bool firstVideo = true;
#endif
	private bool allDownloaded = false;
	private System.DateTime start;
	private System.DateTime finish;
	private Dictionary<int, GameObject> internalScenes;
	private Dictionary<int, String> internalSceneTitles;
	private MCP.GoalMap goalMap;
	private float movieTimer = 0;
	private float spinnerRot = 0;
	private float startDist;
	private float startZoom;
	private float zoom = 0;
	private int downloadStep = 0;		// Current max is 9
	private Rect screenSize;
	private Rect stepTextPos;
	private Rect sigBackgroundBox;
	private Rect nextButtonPos;
	private Rect backButtonPos;
	public Shader multiplyShader, alphaBlendShader;		// Needs to be loaded before use in painter
	private string fileName = MCP.Text (2501);/*"My Goal Map";*/
	
//#if UNITY_WEBPLAYER
#if !UNITY_IPHONE && !UNITY_ANDROID
	private string introUrl = MCP.serverURL + "Videos/goal_mapping_intro.ogg".Replace (" ", "%20");
	private string dreamUrl = MCP.serverURL + "Videos/goal_mapping_dream.ogg".Replace (" ", "%20");
	private string orderUrl = MCP.serverURL + "Videos/goal_mapping_order.ogg".Replace (" ", "%20");
	private string drawUrl = MCP.serverURL + "Videos/goal_mapping_draw.ogg".Replace (" ", "%20");
	private string whenUrl = MCP.serverURL + "Videos/goal_mapping_when.ogg".Replace (" ", "%20");
	private string whyUrl = MCP.serverURL + "Videos/goal_mapping_why.ogg".Replace (" ", "%20");
	private string howUrl = MCP.serverURL + "Videos/goal_mapping_how.ogg".Replace (" ", "%20");
	private string whoUrl = MCP.serverURL + "Videos/goal_mapping_who.ogg".Replace (" ", "%20");
	private string signUrl = MCP.serverURL + "Videos/goal_mapping_sign.ogg".Replace (" ", "%20");
#else
	private string introUrl = MCP.serverURL + "Videos/goal_mapping_intro.mp4".Replace (" ", "%20");
	private string dreamUrl = MCP.serverURL + "Videos/goal_mapping_dream.mp4".Replace (" ", "%20");
	private string orderUrl = MCP.serverURL + "Videos/goal_mapping_order.mp4".Replace (" ", "%20");
	private string drawUrl = MCP.serverURL + "Videos/goal_mapping_draw.mp4".Replace (" ", "%20");
	private string whenUrl = MCP.serverURL + "Videos/goal_mapping_when.mp4".Replace (" ", "%20");
	private string whyUrl = MCP.serverURL + "Videos/goal_mapping_why.mp4".Replace (" ", "%20");
	private string howUrl = MCP.serverURL + "Videos/goal_mapping_how.mp4".Replace (" ", "%20");
	private string whoUrl = MCP.serverURL + "Videos/goal_mapping_who.mp4".Replace (" ", "%20");
	private string signUrl = MCP.serverURL + "Videos/goal_mapping_sign.mp4".Replace (" ", "%20");
	private string currentVidUrl;
#endif

	private WWW wwwData;
	private Vector2 dragStart = Vector2.zero;
	private Vector2 startPan = Vector2.zero;
	private Vector2 prevUV = Vector2.zero;
	
	private enum InternalState
	{
		page1video = 0,
		page1text = 1,
		page2text = 2,
		page3display = 3,
		page4intro = 4,
		page4display = 5,
		page5display = 6,
		page6draw = 7,
		page7when = 8,
		page8draw = 9,
		page9draw = 10,
		page10draw = 11,
		page11draw = 12,
		page12draw = 13,
		showGoalMap = 14,
		saveGoalMap = 15,
		goalMapComplete = 16,
		exit = 17,
	};

	private InternalState internalState;
	private int selectedImageToEdit = 0;

	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		// Initialise the base.
		Init ();

		// Initialize arrays/lists
		zoomInSprite = new Sprite[2];
		zoomOutSprite = new Sprite[2];
		rotateSprite = new Sprite[2];
		changeTextPicSprite = new Sprite[4];
		doneSprite = new Sprite[2];
		playSprite = new Sprite[2];
		skipSprite = new Sprite[2];
		internalScenes = new Dictionary<int, GameObject> ();
		internalSceneTitles = new Dictionary<int, String> ();
		videoObjects = new List<GameObject>();
		hideWithVideo = new List<GameObject>();
		
		// Load resources.
		videoImg = Resources.Load<Sprite> ("bm_logo2");
		mapBack = Resources.Load<Sprite> ("GUI/bm_darkbox_rounded32px");
		mapBackInner1 = Resources.Load<Sprite> ("GUI/bm_box_ltpurple");
		mapBackInner2 = Resources.Load<Sprite> ("GUI/bm_box_orange");
		mapBackInner3 = Resources.Load<Sprite> ("GUI/bm_box_dkpurple");
		mapBackInner4 = Resources.Load<Sprite> ("GUI/bm_box_purple");
		mapBackInner5 = Resources.Load<Sprite> ("GUI/bm_box_blue");
		goalMapExample = Resources.Load<Sprite> ("GUI/goal_maps_icon");
		upArrowIcon = Resources.Load<Sprite> ("GUI/bm_arrowup2");
		downArrowIcon = Resources.Load<Sprite> ("GUI/bm_arrowdown2");
		leftArrowIcon = Resources.Load<Sprite> ("GUI/bm_arrowleft2");
		rightArrowIcon = Resources.Load<Sprite> ("GUI/bm_arrowright2");
		zoomInSprite[0] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_plus");
		zoomInSprite[1] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_plus_active");
		zoomOutSprite[0] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_minus");
		zoomOutSprite[1] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_minus_active");
		rotateSprite[0] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_orientation");
		rotateSprite[1] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_orientation_active");
		changeTextPicSprite[0] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_text");
		changeTextPicSprite[1] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_text_active");
		changeTextPicSprite[2] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_picture");
		changeTextPicSprite[3] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_picture_active");
		doneSprite[0] = Resources.Load<Sprite> ("GUI/Painter/paintbox_done");
		doneSprite[1] = Resources.Load<Sprite> ("GUI/Painter/paintbox_done_active");
		spinnerSprite = Resources.Load<Sprite> ("GUI/bm_rotate");
		videoBackgroundSprite = Resources.Load<Sprite> ("GUI/bm_darkbox_rounded32px");
		playSprite[0] = Resources.Load<Sprite> ("GUI/bm_play_button");
		playSprite[1] = Resources.Load<Sprite> ("GUI/bm_play_button_active");
		skipSprite[0] = Resources.Load<Sprite> ("playericon_fastforward");
		skipSprite[1] = Resources.Load<Sprite> ("playericon_fastforward");
#if !UNITY_ANDROID && !UNITY_IPHONE
		muteSprite = Resources.Load<Sprite> ("GUI/bm-nosound");
		unmuteSprite = Resources.Load<Sprite> ("GUI/bm-sound");
#endif
		arrowSprite = Resources.Load<Sprite> ("right_arrow_long");
		plusTex = Resources.Load<Sprite>("GUI/GoalMap/goalmap_plus").texture;
		minusTex = Resources.Load<Sprite>("GUI/GoalMap/goalmap_minus").texture;
		shareIcon = Resources.Load<Sprite> ("GUI/bm_myfriends");
		mainIcon = Resources.Load<Sprite> ("GUI/bm_myprofile");
		
#if !UNITY_ANDROID && !UNITY_IPHONE
		drawTex = Resources.Load("GUI/brush") as Texture2D;
#endif
		
		// Set the variable defaults.
		internalState = InternalState.saveGoalMap;

		nextButtonPos = new Rect (Screen.width * 0.75f, (Screen.height * 0.875f), Screen.width * 0.2f, Screen.height * 0.08f);
		backButtonPos = new Rect (Screen.width * 0.025f, (Screen.height * 0.02f), Screen.width * 0.06f, Screen.width * 0.06f);
		stepTextPos = new Rect (Screen.width * 0.05f, (Screen.height * 0.14f), Screen.width * 0.875f, Screen.height * 0.15f);
		sigBackgroundBox = new Rect(Screen.width * 0.445f, Screen.height * 0.18f, Screen.width * 0.5f, Screen.height * 0.57f);
		
#if !UNITY_ANDROID && !UNITY_IPHONE
		GetComponent<GUITexture>().pixelInset = new Rect(Screen.width * 0.71f, Screen.height * 0.8f, Screen.width * -0.42f, Screen.height * -0.54f);
		
		// Start the intro video downloading.
		MCP.videoFilePath = introUrl;
		StartCoroutine(MCP.GetVideoFile ());
#else
		GetComponent<GUITexture>().enabled = false;
		currentVidUrl = introUrl;
#endif

		minFontSize = (int)_fontSize_Small;
		maxFontSize = (int)_fontSize_Large;
	}
	
	private void SetToDefaultGoalMap()
	{
		start = System.DateTime.Now;
		finish = System.DateTime.Now;
		
		// For each goal...
		for (int i = 0; i < numberOfGoals; i++)
		{
			MCP.Goal goalTmp = new MCP.Goal ();
			// Set default text.
			goalTmp.goal = MCP.Text (2711)/*"Goal"*/ + " " + (i + 1);
			
			goalMap.goals.Add (goalTmp);
		}
		
		goalMap.whys = new List<MCP.Why> ();
		// For each why...
		for (int i = 0; i < numberOfWhys; i++)
		{
			MCP.Why goalTmp = new MCP.Why ();
			// Set default text.
			goalTmp.why = MCP.Text (2710)/*"Why"*/ + " " + (i + 1);
			
			goalMap.whys.Add (goalTmp);
		}
		
		goalMap.hows = new List<MCP.How> ();
		// For each how...
		for (int i = 0; i < numberOfHows; i++)
		{
			MCP.How goalTmp = new MCP.How ();
			// Set default text.
			goalTmp.how = MCP.Text (2709)/*"How"*/ + " " + (i + 1);
			
			goalMap.hows.Add (goalTmp);
		}
		
		goalMap.whos = new List<MCP.Who> ();
		// For each who...
		for (int i = 0; i < numberOfWhos; i++)
		{
			MCP.Who goalTmp = new MCP.Who ();
			// Set default text.
			goalTmp.who = MCP.Text (2708)/*"Who"*/ + " " + (i + 1);
			
			goalMap.whos.Add (goalTmp);
		}
	}

	/// <summary>
	/// Do the legacy GUI.
	/// </summary>
	public override void DoGUI ()
	{
		if (screenSize.width == 0 || Screen.width != screenSize.width)
		{
			screenSize = new Rect (0, 0, Screen.width, Screen.height);
			
			// If the local canvas hasn't been created...
			if(localCanvas == null)
			{
				// Initalize goal map.
				goalMap = new MCP.GoalMap ();
				goalMap.goals = new List<MCP.Goal> ();
				
				if(MCP.gmIndex != -1)
				{
					if(MCP.goalMaps != null && MCP.goalMaps.Count > 0)
					{
						goalMap = MCP.goalMaps[MCP.gmIndex];
						start = goalMap.start;
						finish = goalMap.finish;
					}
					else
					{
						SetToDefaultGoalMap();
					}
				}
				else
				{
					SetToDefaultGoalMap();
				}
				
				
				// Create the scene GUI.
				CreateGUI ();
				
				localCanvas.transform.localScale = Vector2.zero;
			}
		}
		
		float middleX = (Screen.width * 0.6f);
		float topY = Screen.height * 0.2175f;
		float bottomY = Screen.height * 0.6f;
		float outerWidth = Screen.width * 0.25f;
		float outerHeight = Screen.height * 0.25f;
		float innerWidth = (outerWidth) * 0.9f;
		float innerHeight = (outerHeight) * 0.85f;
		float paddingY = ((outerHeight) - (innerHeight)) * 0.5f;

#if UNITY_ANDROID || UNITY_IPHONE
//		showVideo = false;
#endif

		if(!showVideo)
		{
			switch (internalState)
			{
			case InternalState.page7when:
				float canvasOffset = localCanvas.GetComponent<RectTransform>().localPosition.x;
				// Get the start and end dates from buttons.
				finish = GetDateTime (new Rect (middleX - (Screen.width * 0.08f) + (canvasOffset * Screen.width * 0.05f), topY + (paddingY * 4.0f) - (Screen.height * 0.13f), innerWidth * 1.75f, innerHeight * 1.5f), finish, true);
				GetDateTime (new Rect (middleX - (Screen.width * 0.08f) + (canvasOffset * Screen.width * 0.05f), bottomY + (paddingY * 1.0f) - (Screen.height * 0.066f), innerWidth * 1.75f, innerHeight * 1.5f), start, false);
				break;
				
			case InternalState.page8draw:
				// Set the date information in each object it is needed.
				startDateLabelHow.GetComponent<Text>().text = GetDateString(start);
				stopDateLabelHow.GetComponent<Text>().text = GetDateString(finish);
				drawStartDateLabelHow.GetComponent<Text>().text = GetDateString(start);
				drawStopDateLabelHow.GetComponent<Text>().text = GetDateString(finish);
				drawStartDateLabelWho.GetComponent<Text>().text = GetDateString(start);
				drawStopDateLabelWho.GetComponent<Text>().text = GetDateString(finish);
				startDateLabelWho.GetComponent<Text>().text = GetDateString(start);
				stopDateLabelWho.GetComponent<Text>().text = GetDateString(finish);
				break;
			}
		}
	}

	void CreateGUI ()
	{
		// Find the current canvas.
		localCanvas = MakeCanvas ();
		
		// Make canvas for asking if user wants to edit a picture.
		editCanvas = MakeCanvas (true);
		
		// Make the background window.
		GameObject backGround = MakeWindowBackground (localCanvas, new Rect (Screen.width * 0.01f, Screen.height * 0.01f, Screen.width * 0.98f, Screen.height * 0.98f), window_back); 
		backGround.name = "Background";
		
		// Make a background to cover the main content during a video.
		videoTitleBackground = MakeWindowBackground (localCanvas, new Rect (Screen.width * 0.02f, Screen.height * 0.02f, Screen.width * 0.96f, Screen.height * 0.5f), videoBackgroundSprite); 
		videoTitleBackground.name = "videoTitleBackground";
		videoObjects.Add (videoTitleBackground);

		// Make the title bar.
		titleBar = MakeTitleBar (localCanvas, MCP.Text (2801)/*"Stage 1 - 7 Steps of Goal Mapping"*/, "SmallTitle");
		titleBar.name = "TitleBar";
		
		// Make the side gizmo
		MakeSideGizmo (localCanvas, true, false);	
		
		// Draw back button.
		closeButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.75f, Screen.height * 0.04f, Screen.width * 0.15f, Screen.height * 0.06f), buttonBackground, true);
		closeButtonBackground.name = "CloseButtonBackground";
		hideWithVideo.Add (closeButtonBackground);
#if !UNITY_ANDROID && !UNITY_IPHONE
		closeButtonBackground.SetActive (false);
#endif
		//fileBrowserObjects.Add (closeButtonBackground);
		
		List<MyVoidFunc> funcSaveList = new List<MyVoidFunc>()
		{
			delegate 
			{
				saveInProgress = true;
				StartCoroutine(SaveGoalMap());
				Debug.Log( "Saved" );
			}
		};

		// Make the save goal map button.
		saveGoalMapButton = MakeButton (localCanvas, MCP.Text (2832)/*"Save"*/, new Rect(Screen.width * 0.75f, Screen.height * 0.04f, Screen.width * 0.15f, Screen.height * 0.06f), funcSaveList);
		saveGoalMapButton.GetComponent<Button>().image = closeButtonBackground.GetComponent<Image>();
		saveGoalMapButton.GetComponent<Text> ().alignment = TextAnchor.MiddleCenter;
		saveGoalMapButton.name = "SaveGoalMapButton";
		hideWithVideo.Add (saveGoalMapButton);
#if !UNITY_ANDROID && !UNITY_IPHONE
		saveGoalMapButton.SetActive (false);
#endif
		
//#if UNITY_ANDROID || UNITY_IPHONE
//		if(Debug.isDebugBuild)
//		{
//			GameObject debugSkipVideoCheckbox = MakeCheckbox (localCanvas, new Rect(Screen.width * 1.15f, Screen.height * 0.54f, Screen.width * 0.2f, Screen.height * 0.05f), "Skip Videos");
//			debugSkipVideoCheckbox.name = "DebugSkipVideoCheckbox";
//			debugSkipVideoCheckbox.GetComponent<Toggle>().onValueChanged.AddListener (
//				delegate
//				{
//					debugSkipVideos = debugSkipVideoCheckbox.GetComponent<Toggle>().isOn;
//				});
//		} 
//#endif
		
		///////////////////////////////////////////////////////////////////////////////////////////
		//***************************************************************************************//
		// ***** Save goal map ******************************************************************//
		//***************************************************************************************//
		///////////////////////////////////////////////////////////////////////////////////////////
		// Make the save goal map parent object.
		GameObject saveGoalMap = MakePanel (localCanvas, new Rect (Screen.width * 0.01f, Screen.height * 0.01f, Screen.width * 0.98f, Screen.height * 0.98f));//MakeWindowBackground (localCanvas, new Rect(MenuScreen.canvasSize.width * 0.01f, MenuScreen.canvasSize.height * 0.01f , MenuScreen.canvasSize.width * 0.98f, MenuScreen.canvasSize.height * 0.98f), window_back);
		saveGoalMap.name = "Save Goal Map";
		
		// Make a label to explain saving the goal map.
		Rect r = new Rect(stepTextPos.x, stepTextPos.y, stepTextPos.width * 0.975f, stepTextPos.height);
		GameObject saveGMText = MakeLabel (localCanvas, MCP.Text (2830)/*"Enter a name to save this Goal Map under. This will allow you to find your Goal Map easily to review your progress towards your goals."*/, r, "SmallText");
		saveGMText.name = "Save GM Text";
		saveGMText.transform.SetParent (saveGoalMap.transform, true);
		saveGMText.transform.localScale = Vector3.one;
		
		// Make a label indicating the name should be written here.
		GameObject saveGMNameLabel = MakeLabel (localCanvas, MCP.Text (2831)/*"Name: "*/, new Rect(Screen.width * 0.06f, (Screen.height * 0.42f), Screen.width * 0.2f, Screen.height * 0.1f), "InnerLabel");
		saveGMNameLabel.name = "SaveGMNameLabel";
		saveGMNameLabel.transform.SetParent (saveGoalMap.transform, true);
		saveGMNameLabel.transform.localScale = Vector3.one;
		
		// Make an input box to accept the file name.
		if( MCP.gmIndex == -1 )
		{
			saveGMFileName = MakeInputField (localCanvas, fileName, new Rect(Screen.width * 0.1f, Screen.height * 0.515f, Screen.width * 0.8f, Screen.height * 0.08f), false, buttonBackground);
		}
		else
		{
			saveGMFileName = MakeInputField (localCanvas, goalMap.name, new Rect(Screen.width * 0.1f, Screen.height * 0.515f, Screen.width * 0.8f, Screen.height * 0.08f), false, buttonBackground);			
		}
		saveGMFileName.name = "SaveGMFileNameInput";
		saveGMFileName.transform.parent.SetParent (saveGoalMap.transform);
		saveGMFileName.GetComponent<Text>().resizeTextForBestFit = true;
		saveGMFileName.GetComponent<InputField>().characterLimit = 36;
		saveGMFileName.GetComponent<InputField>().onValueChange.AddListener (
				delegate
				{
				saveGMFileName.GetComponent<InputField>().text = saveGMFileName.GetComponent<InputField>().text.Replace ("\t", "");
			});
		
		// Make the save goal map button.
		GameObject saveGoalMapNextButton = MakeActionButton (localCanvas, MCP.Text (2803)/*"Next"*/, new Rect(nextButtonPos.x, nextButtonPos.y, nextButtonPos.width, nextButtonPos.height), DoStateButtonID, (int)InternalState.page1video);
		saveGoalMapNextButton.name = "SaveGoalMapNextButton";
		saveGoalMapNextButton.transform.SetParent (saveGoalMap.transform, true);
		saveGoalMapNextButton.transform.localScale = Vector3.one;
		
		List<MyVoidFunc> restartVideo = new List<MyVoidFunc>()
		{
			delegate
			{
#if UNITY_ANDROID || UNITY_IPHONE
				Handheld.PlayFullScreenMovie (introUrl);
//#else
#endif
				foreach(GameObject g in videoObjects)
				{
					if(g != null)
					{
						g.SetActive (true);
					}
				}
				titleBar.GetComponent<Text>().text = MCP.Text (2801)/*"Stage 1 - 7 Steps of Goal Mapping"*/;
				showVideo = true;
//#endif
			}
		};
		// Make the previous page button.
		showIntroVideoBackButton = MakeButton (localCanvas, "", backButtonSprite, new Rect(backButtonPos.x, backButtonPos.y, backButtonPos.width, backButtonPos.height), restartVideo);
		showIntroVideoBackButton.name = "ShowIntroVideoBackButton";
		showIntroVideoBackButton.transform.SetParent ( saveGoalMap.transform, true );
		showIntroVideoBackButton.transform.localScale = Vector3.one;
		
		//saveGoalMap.SetActive (false);
		internalScenes.Add ((int)InternalState.saveGoalMap, saveGoalMap);
		internalSceneTitles.Add ((int)InternalState.saveGoalMap, MCP.Text (2833)/*"Save Your Goal Map"*/);
		// ***** End save goal map **************************************************************//
		
		///////////////////////////////////////////////////////////////////////////////////////////
		//***************************************************************************************//
		//******* Page 1 - Wishes Video *********************************************************//
		//***************************************************************************************//
		///////////////////////////////////////////////////////////////////////////////////////////
		// Make page 1 video parent.
		GameObject page1video = MakePanel (localCanvas, new Rect (Screen.width * 0.01f, Screen.height * 0.01f, Screen.width * 0.98f, Screen.height * 0.98f));//MakeWindowBackground (localCanvas, new Rect(MenuScreen.canvasSize.width * 0.01f, MenuScreen.canvasSize.height * 0.01f , MenuScreen.canvasSize.width * 0.98f, MenuScreen.canvasSize.height * 0.98f), window_back);
		page1video.name = "Page 1 - Video";
		
		// Make page 1 text background.
		GameObject page1TextBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.51f, Screen.height * 0.175f, Screen.width * 0.44f, Screen.height * 0.625f), window_back);
		page1TextBackground.name = "Page1TextBackground";
		page1TextBackground.transform.SetParent (page1video.transform, true);

		// Make the page text.
		GameObject page1video_text = MakeLabel (localCanvas, MCP.Text(2802)/*"Relax. Sit in a comfortable upright position with your back straight and feet flat on the floor. Take a deep breath, in through your nose, wait a moment, before breathing out slowly through your mouth while saying the word 'Relax' to yourself three times."*/, new Rect (Screen.width * 0.53f, Screen.height * 0.225f, Screen.width * 0.4f, Screen.height * 0.525f), "SmallText");
		page1video_text.name = "Page 1 Video Text";
		page1video_text.GetComponent<Text>().resizeTextForBestFit = true;
		page1video_text.GetComponent<Text>().resizeTextMaxSize = (int)_fontSize_Large;
		page1video_text.GetComponent<Text>().resizeTextMinSize = (int)_fontSize_Small;
		page1video_text.transform.SetParent (page1video.transform, true);
		
		// Make the start button.
		GameObject startButton = MakeActionButton (localCanvas, MCP.Text (2803)/*"Next"*/, nextButtonPos, DoStateButtonID, (int)InternalState.page1text);
		startButton.name = "StartButton";
		startButton.transform.SetParent (page1video.transform, true);

		// Create the video.
		GameObject page1Logo = MakeImage (localCanvas, new Rect (Screen.width * 0.14f, Screen.height * 0.2f, Screen.width * 0.25f, Screen.height * 0.6f), videoImg);
		page1Logo.name = "Logo";
		page1Logo.transform.SetParent (page1video.transform, true);
		
		// Make the previous page button.
		GameObject saveGoalMapBackButton = MakeActionButton (localCanvas, backButtonSprite, new Rect(backButtonPos.x, backButtonPos.y, backButtonPos.width, backButtonPos.height), DoStateButtonID, (int)InternalState.saveGoalMap);
		saveGoalMapBackButton.name = "SaveGoalMapBackButton";
		saveGoalMapBackButton.transform.SetParent (page1video.transform, true);
		saveGoalMapBackButton.transform.localScale = Vector3.one;
		
		page1video.SetActive( false );
		internalScenes.Add ((int)InternalState.page1video, page1video);
		internalSceneTitles.Add ((int)InternalState.page1video, MCP.Text (2804)/*"Step 1 - Dream"*/);
		//****** End page 1 *********************************************************************//
		
		///////////////////////////////////////////////////////////////////////////////////////////
		//***************************************************************************************//
		//****** Page 1 - Goals Text entry ******************************************************//
		//***************************************************************************************//
		///////////////////////////////////////////////////////////////////////////////////////////
		// Make the page 1 text entry parent.
		GameObject page1text = MakePanel (localCanvas, new Rect (Screen.width * 0.01f, Screen.height * 0.01f, Screen.width * 0.98f, Screen.height * 0.98f));//MakeWindowBackground (localCanvas, new Rect(MenuScreen.canvasSize.width * 0.01f, MenuScreen.canvasSize.height * 0.01f , MenuScreen.canvasSize.width * 0.98f, MenuScreen.canvasSize.height * 0.98f), window_back);
		page1text.name = "Page 1 - Text";
		// Make the page text.
		GameObject page1text_text = MakeLabel (localCanvas, MCP.Text (2805)/*"What do you want to achieve? If you already know your goals, write them here. If you are unclear about your goals, return to the previous page and watch the video again."*/, new Rect(stepTextPos.x, stepTextPos.y, stepTextPos.width, stepTextPos.height), "SmallText");
		page1text_text.name = "Page 1  Text";
		page1text_text.transform.SetParent (page1text.transform, true);
		// Make the scroll content parent.
		GameObject page1scroll = MakePanel (localCanvas, new Rect (Screen.width * 0.01f, Screen.height * 0.01f, Screen.width * 0.98f, Screen.height * 0.83f));//MakeWindowBackground (localCanvas, new Rect(MenuScreen.canvasSize.width * 0.01f, MenuScreen.canvasSize.height * 0.01f , MenuScreen.canvasSize.width * 0.98f, MenuScreen.canvasSize.height * 0.98f), window_back);
		page1scroll.name = "Page1Scroll";
		page1scroll.transform.SetParent (page1text.transform, true);

		page1goals = new List<GameObject> ();
		// For each goal...
		for (int i = 0; i < numberOfGoals; i++)
		{
			// Check the goals are properly initialized
			if(goalMap.goals [i].goal == null || goalMap.goals [i].goal == "")
			{
				goalMap.goals [i].goal = MCP.Text (2711)/*"Goal"*/ + " " + i;
			}
			
			// Make the goal number background.
			GameObject enumIcon = MakeImage (localCanvas, new Rect (Screen.width * 0.105f, (i * (Screen.height * 0.16f)) + (Screen.height * 0.34f), Screen.width * 0.1f, Screen.width * 0.1f), enumIconSprite);
			enumIcon.name = "Goal " + (i + 1);
			enumIcon.transform.SetParent (page1scroll.transform, true);
			
			// Make the goal number label.
			GameObject enumIconText = MakeLabel (localCanvas, (i + 1).ToString (), new Rect (Screen.width * 0.105f, (i * (Screen.height * 0.16f)) + (Screen.height * 0.34f), Screen.width * 0.1f, Screen.height * 0.15f), null);
			enumIconText.name = "Goal " + (i + 1);
			enumIconText.transform.SetParent (page1scroll.transform, true);
			enumIconText.GetComponent<Text>().fontSize = (int)_fontSize_Large;
			enumIconText.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
			
			// Make the goal input field.
			GameObject enumLabel = MakeInputField (localCanvas, goalMap.goals [i].goal, new Rect (Screen.width * 0.21f, i * (Screen.height * 0.16f) + (Screen.height * 0.335f), Screen.width * 0.675f, Screen.height * 0.15f), false);
			enumLabel.name = "Label " + (i + 1);
			enumLabel.transform.parent.SetParent (page1scroll.transform, true);
			enumLabel.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Wrap;
			enumLabel.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
			enumLabel.GetComponent<Text>().resizeTextForBestFit = true;
			enumLabel.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
			enumLabel.GetComponent<Text>().resizeTextMinSize = minFontSize;
			enumLabel.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;
			enumLabel.GetComponent<InputField>().onValueChange.AddListener (
				delegate
				{
					enumLabel.GetComponent<InputField>().text = enumLabel.GetComponent<InputField>().text.Replace ("\t", "");
					
					if(enumLabel.GetComponent<InputField>().text.Length > numberOfCharsInInputs)
					{
						enumLabel.GetComponent<InputField>().text = enumLabel.GetComponent<InputField>().text.Substring (0, numberOfCharsInInputs);
					}
			   });
			page1goals.Add (enumLabel);
		}
		
		// Make the goal scroll area.
		GameObject page1ScrollArea = MakeScrollRect (localCanvas, new Rect (Screen.width * 0.1f, Screen.height * 0.275f, Screen.width * 0.8f, Screen.height * 0.55f), page1scroll);
		page1ScrollArea.name = "ScrollArea";
		page1ScrollArea.transform.SetParent (page1text.transform, true);
		page1ScrollArea.GetComponent<ScrollRect> ().horizontal = false;
		page1ScrollArea.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2(page1ScrollArea.GetComponent<RectTransform> ().sizeDelta.x, page1scroll.GetComponent<RectTransform>().rect.height);
		page1ScrollArea.transform.GetChild(0).GetChild (0).gameObject.GetComponent<RectTransform>().transform.localPosition = new Vector2(0, Screen.height * -1.25f);

		// Make the next page button.
		GameObject page1NextButton = MakeActionButton (localCanvas, MCP.Text (2803)/*"Next"*/, nextButtonPos, DoStateButtonID, (int)InternalState.page2text);
		page1NextButton.transform.SetParent (page1text.transform, true);
		page1NextButton.name = "Page1NextButton";
		
		// Make the previous page button.
		GameObject page1BackButton = MakeActionButton (localCanvas, backButtonSprite, backButtonPos, DoStateButtonID, (int)InternalState.page1video);
		page1BackButton.name = "Page1BackButton";
		page1BackButton.transform.SetParent (page1text.transform, true);
		
		page1text.SetActive (false);
		internalScenes.Add ((int)InternalState.page1text, page1text);
		internalSceneTitles.Add ((int)InternalState.page1text, MCP.Text (2804)/*"Step 1 - Dream"*/);
		//****** End page 1 - text entry ********************************************************//
		
		///////////////////////////////////////////////////////////////////////////////////////////
		//***************************************************************************************//
		//****** Start Page 2 - Order goals *****************************************************//
		//***************************************************************************************//
		///////////////////////////////////////////////////////////////////////////////////////////
		
		// Make the order goal parent object.
		GameObject page2text = MakePanel (localCanvas, new Rect (Screen.width * 0.01f, Screen.height * 0.01f, Screen.width * 0.98f, Screen.height * 0.98f));//MakeWindowBackground (localCanvas, new Rect(MenuScreen.canvasSize.width * 0.01f, MenuScreen.canvasSize.height * 0.01f , MenuScreen.canvasSize.width * 0.98f, MenuScreen.canvasSize.height * 0.98f), window_back);
		page2text.name = "Page 2 - Order";
		GameObject page2text_text = MakeLabel (localCanvas, MCP.Text (2806)/*"In the last step, Dream, you were guided to think creatively with your right-brain to envisage your goals and desires. In this step, use your logical left-brain to place your goals in order of priority."*/, new Rect(stepTextPos.x, stepTextPos.y, stepTextPos.width - (Screen.width * 0.02f), stepTextPos.height), "SmallText");
		page2text_text.name = "Page 2  Text";
		page2text_text.GetComponent<Text>().resizeTextForBestFit = true;
		page2text_text.GetComponent<Text>().resizeTextMaxSize = (int)_fontSize_Large;
		page2text_text.GetComponent<Text>().resizeTextMinSize = (int)_fontSize_Small;
		page2text_text.transform.SetParent (page2text.transform, true);
		
		// Make scroll content panel.
		GameObject page2scroll = MakePanel (localCanvas, new Rect (Screen.width * 0.01f, Screen.height * 0.01f, Screen.width * 0.98f, Screen.height * 0.83f));//MakeWindowBackground (localCanvas, new Rect(MenuScreen.canvasSize.width * 0.01f, MenuScreen.canvasSize.height * 0.01f , MenuScreen.canvasSize.width * 0.98f, MenuScreen.canvasSize.height * 0.98f), window_back);
		page2scroll.name = "Page2Scroll";
		page2scroll.transform.SetParent (page2text.transform, true);

		page2goals = new List<GameObject> ();
		// For each goal...
		for (int i = 0; i < numberOfGoals; i++)
		{
			// Check the goals are properly initialized.
			if(goalMap.goals [i].goal == null || goalMap.goals [i].goal == "")
			{
				goalMap.goals [i].goal = MCP.Text (2711)/*"Goal"*/ + " " + i;
			}
			
			// Make the goal number background.
			GameObject enumIcon = MakeImage (localCanvas, new Rect (Screen.width * 0.105f, (i * (Screen.height * 0.16f)) + (Screen.height * 0.34f), Screen.width * 0.1f, Screen.width * 0.1f), enumIconSprite);
			enumIcon.name = "Goal " + (i + 1);
			enumIcon.transform.SetParent (page2scroll.transform);
			
			// Make the goal number label.
			GameObject enumIconText = MakeLabel (localCanvas, (i + 1).ToString (), new Rect (Screen.width * 0.105f, (i * (Screen.height * 0.16f)) + (Screen.height * 0.34f), Screen.width * 0.1f, Screen.height * 0.15f), null);
			enumIconText.name = "Goal " + (i + 1);
			enumIconText.transform.SetParent (page2scroll.transform, true);
			enumIconText.GetComponent<Text>().fontSize = (int)_fontSize_Large;
			enumIconText.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
			
			// Make the goal text background.
			GameObject enumText = MakeWindowBackground (localCanvas, new Rect (Screen.width * 0.2f, i * (Screen.height * 0.16f) + (Screen.height * 0.34f), Screen.width * 0.69f, Screen.height * 0.15f), window_back);
			enumText.name = "Goal " + (i + 1) + " text";
			enumText.transform.SetParent (page2scroll.transform, true);
			
			// Make the goal text.
			GameObject enumLabel = MakeLabel (localCanvas, goalMap.goals [i].goal, new Rect (Screen.width * 0.21f, i * (Screen.height * 0.16f) + (Screen.height * 0.34f), Screen.width * 0.575f, Screen.height * 0.15f));
			enumLabel.name = "GoalLabel" + (i + 1);
			enumLabel.transform.SetParent (page2scroll.transform, true);
			page2goals.Add (enumLabel);

			// If this is not the first goal...
			if (i != 0)
			{
				// Make the arrow to move the goal order.
				GameObject enumUpArrow = MakeActionButton (localCanvas, upArrowIcon, new Rect (Screen.width * 0.785f, (i * (Screen.height * 0.16f)) + (Screen.height * 0.37f), Screen.width * 0.05f, Screen.width * 0.05f), DoReorderGoalUpButton, i);
				enumUpArrow.name = "UpArrow " + (i + 1);
				enumUpArrow.transform.SetParent (page2scroll.transform, true);
			}
			// If this is not the last goal...
			if (i != numberOfGoals - 1)
			{
				// Make the arrow to move the goal order.
				GameObject enumDownArrow = MakeActionButton (localCanvas, downArrowIcon, new Rect (Screen.width * 0.835f, (i * (Screen.height * 0.16f)) + (Screen.height * 0.37f), Screen.width * 0.05f, Screen.width * 0.05f), DoReorderGoalDownButton, i);
				enumDownArrow.name = "DownpArrow " + (i + 1);
				enumDownArrow.transform.SetParent (page2scroll.transform, true);
			}
		}

		// Make the page scroll area.
		GameObject page2ScrollArea = MakeScrollRect (localCanvas, new Rect (Screen.width * 0.1f, Screen.height * 0.275f, Screen.width * 0.8f, Screen.height * 0.55f), page2scroll);
		page2ScrollArea.name = "ScrollRect";
		page2ScrollArea.transform.SetParent (page2text.transform, true);
		page2ScrollArea.GetComponent<ScrollRect> ().horizontal = false;
		page2ScrollArea.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2(page2ScrollArea.GetComponent<RectTransform> ().sizeDelta.x, page2scroll.GetComponent<RectTransform>().rect.height);
		page2ScrollArea.transform.GetChild(0).GetChild (0).gameObject.GetComponent<RectTransform>().transform.localPosition = new Vector2(0, Screen.height * -1.25f);

		// Make the next page button.
		GameObject page2NextButton = MakeActionButton (localCanvas, MCP.Text (2803)/*"Next"*/, nextButtonPos, DoStateButtonID, (int)InternalState.page3display);
		page2NextButton.name = "page2NextButton";
		page2NextButton.transform.SetParent (page2text.transform, true);
		
		// Make the previous page button.
		GameObject page2BackButton = MakeActionButton (localCanvas, backButtonSprite, backButtonPos, DoStateButtonID, (int)InternalState.page1text);
		page2BackButton.name = "Page2BackButton";
		page2BackButton.transform.SetParent (page2text.transform, true);

		page2text.SetActive (false);
		internalScenes.Add ((int)InternalState.page2text, page2text);
		internalSceneTitles.Add ((int)InternalState.page2text, MCP.Text (2807)/*"Step 2 - Order"*/);
		//****** End page 2 - Order goals *******************************************************//
		
		///////////////////////////////////////////////////////////////////////////////////////////
		//***************************************************************************************//
		//****** Start Page 2 - Display Goals ***************************************************//
		//***************************************************************************************//
		///////////////////////////////////////////////////////////////////////////////////////////
		// Make the page 3 display parent
		GameObject page3display = MakePanel (localCanvas, new Rect (Screen.width * 0.01f, Screen.height * 0.01f, Screen.width * 0.98f, Screen.height * 0.98f));//MakeWindowBackground (localCanvas, new Rect(MenuScreen.canvasSize.width * 0.01f, MenuScreen.canvasSize.height * 0.01f , MenuScreen.canvasSize.width * 0.98f, MenuScreen.canvasSize.height * 0.98f), window_back);
		page3display.name = "Page 3 - Order";
		// Make the page text.
		GameObject page3text_text = MakeLabel (localCanvas, MCP.Text (2808)/*"If you are happy with how your goals have been prioritized, click next to continue to the next step, otherwise click back to make changes."*/, new Rect(stepTextPos.x, stepTextPos.y, stepTextPos.width - (Screen.width * 0.02f), stepTextPos.height), "SmallText");
		page3text_text.name = "Page 3  Text";
		page3text_text.transform.SetParent (page3display.transform, true);
		
		float leftX = Screen.width * 0.05f;
		float rightX = Screen.width * 0.65f;
		float topY = Screen.height * 0.2175f;
		float bottomY = Screen.height * 0.55f;
		float outerWidth = Screen.width * 0.25f;
		float outerHeight = Screen.height * 0.25f;
		float innerWidth = (outerWidth) * 0.9f;
		float innerHeight = (outerHeight) * 0.85f;
		float paddingX = ((outerWidth) - (innerWidth)) * 0.5f;
		float paddingY = ((outerHeight) - (innerHeight)) * 0.5f;

		float mainX = Screen.width * 0.325f;
		float mainY = Screen.height * 0.25f;

		float mainWidth = Screen.width * 0.3f;
		float mainHeight = Screen.height * 0.45f;

		float mainInnerWidth = (mainWidth) * 0.9f;
		float mainInnerHeight = (mainHeight) * 0.85f;

		float mainPaddingX = ((mainWidth) - (mainInnerWidth)) * 0.5f;
		float mainPaddingY = ((mainHeight) - (mainInnerHeight)) * 0.5f;

		page3goals = new List<GameObject> ();
		
		// Main goal
		GameObject mainGoal = MakeImage (localCanvas, new Rect (mainX - (mainWidth * 0.1f), mainY, mainWidth * 1.2f, mainHeight * 1.29f), mapBack, true);
		mainGoal.transform.SetParent (page3display.transform, true);
		
		GameObject mainGoalInner = MakeImage (localCanvas, new Rect (mainX + mainPaddingX - (mainWidth * 0.1f), mainY + mainPaddingY, mainInnerWidth + (mainWidth * 0.2f), mainInnerHeight + (mainHeight * 0.29f)), mapBackInner2, true);
		mainGoalInner.transform.SetParent (page3display.transform, true);
		
		GameObject mainGoalLabel = MakeLabel (localCanvas, goalMap.goals [0].goal, new Rect (mainX + mainPaddingX - (mainWidth * 0.1f), mainY + mainPaddingY, mainInnerWidth + (mainWidth * 0.2f), mainInnerHeight + (mainHeight * 0.29f)), "MainGoal");
		mainGoalLabel.GetComponent<Text>().resizeTextForBestFit = true;
		mainGoalLabel.GetComponent<Text>().resizeTextMinSize = minFontSize;
		mainGoalLabel.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
		mainGoalLabel.transform.SetParent (page3display.transform, true);
		
		mainGoal.name = "MainGoal";
		mainGoalInner.name = "MainGoal";
		mainGoalLabel.name = "MainGoal";
		page3goals.Add (mainGoalLabel);

		// Sub goals
		GameObject subGoal1 = MakeImage (localCanvas, new Rect (leftX, topY * 1.225f, outerWidth * 0.9f, outerHeight), mapBack, true);
		subGoal1.transform.SetParent (page3display.transform, true);
		
		GameObject subGoal1inner = MakeImage (localCanvas, new Rect (leftX + paddingX, topY + paddingY + (topY * 0.225f), innerWidth - (outerWidth * 0.1f), innerHeight), mapBackInner1);
		subGoal1inner.transform.SetParent (page3display.transform, true);
		
		GameObject subGoal1Label = MakeLabel (localCanvas, goalMap.goals [1].goal, new Rect (leftX + paddingX, topY + paddingY + (topY * 0.225f), innerWidth - (outerWidth * 0.1f), innerHeight), "SubGoal");
		subGoal1Label.GetComponent<Text>().resizeTextForBestFit = true;
		subGoal1Label.GetComponent<Text>().resizeTextMinSize = minFontSize;
		subGoal1Label.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
		subGoal1Label.transform.SetParent (page3display.transform, true);
		
		subGoal1.name = "SubGoal1";
		subGoal1inner.name = "SubGoal1";
		subGoal1Label.name = "SubGoal1";
		page3goals.Add (subGoal1Label);

		GameObject subGoal2 = MakeImage (localCanvas, new Rect (rightX + (rightX * 0.05f), topY * 1.225f, outerWidth * 0.9f, outerHeight), mapBack, true);
		subGoal2.transform.SetParent (page3display.transform, true);
		
		GameObject subGoal2inner = MakeImage (localCanvas, new Rect (rightX + paddingX + (rightX * 0.05f), topY + paddingY + (topY * 0.225f), innerWidth - (outerWidth * 0.1f), innerHeight), mapBackInner1);
		subGoal2inner.transform.SetParent (page3display.transform, true);
		
		GameObject subGoal2Label = MakeLabel (localCanvas, goalMap.goals [2].goal, new Rect (rightX + paddingX + (rightX * 0.05f), topY + paddingY + (topY * 0.225f), innerWidth - (outerWidth * 0.1f), innerHeight), "SubGoal");
		subGoal2Label.GetComponent<Text>().resizeTextForBestFit = true;
		subGoal2Label.GetComponent<Text>().resizeTextMinSize = minFontSize;
		subGoal2Label.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
		subGoal2Label.transform.SetParent (page3display.transform, true);
		
		subGoal2.name = "SubGoal2";
		subGoal2inner.name = "SubGoal2";
		subGoal2Label.name = "SubGoal2";
		page3goals.Add (subGoal2Label);

		GameObject subGoal3 = MakeImage (localCanvas, new Rect (leftX, bottomY * 1.025f, outerWidth * 0.9f, outerHeight), mapBack, true);
		subGoal3.transform.SetParent (page3display.transform, true);
		
		GameObject subGoal3inner = MakeImage (localCanvas, new Rect (leftX + paddingX, bottomY + paddingY + (bottomY * 0.025f), innerWidth - (outerWidth * 0.1f), innerHeight), mapBackInner1);
		subGoal3inner.transform.SetParent (page3display.transform, true);
		
		GameObject subGoal3Label = MakeLabel (localCanvas, goalMap.goals [3].goal, new Rect (leftX + paddingX, bottomY + paddingY + (bottomY * 0.025f), innerWidth - (outerWidth * 0.1f), innerHeight), "SubGoal");
		subGoal3Label.GetComponent<Text>().resizeTextForBestFit = true;
		subGoal3Label.GetComponent<Text>().resizeTextMinSize = minFontSize;
		subGoal3Label.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
		subGoal3Label.transform.SetParent (page3display.transform, true);
		
		subGoal3.name = "SubGoal3";
		subGoal3inner.name = "SubGoal3";
		subGoal3Label.name = "SubGoal3";
		page3goals.Add (subGoal3Label);


		GameObject subGoal4 = MakeImage (localCanvas, new Rect (rightX + (rightX * 0.05f), bottomY * 1.025f, outerWidth * 0.9f, outerHeight), mapBack, true);
		subGoal4.transform.SetParent (page3display.transform, true);
		
		GameObject subGoal4inner = MakeImage (localCanvas, new Rect (rightX + paddingX + (rightX * 0.05f), bottomY + paddingY + (bottomY * 0.025f), innerWidth - (outerWidth * 0.1f), innerHeight), mapBackInner1);
		subGoal4inner.transform.SetParent (page3display.transform, true);
		
		GameObject subGoal4Label = MakeLabel (localCanvas, goalMap.goals [4].goal, new Rect (rightX + paddingX + (rightX * 0.05f), bottomY + paddingY + (bottomY * 0.025f), innerWidth - (outerWidth * 0.1f), innerHeight), "SubGoal");
		subGoal4Label.GetComponent<Text>().resizeTextForBestFit = true;
		subGoal4Label.GetComponent<Text>().resizeTextMinSize = minFontSize;
		subGoal4Label.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
		subGoal4Label.transform.SetParent (page3display.transform, true);
		
		subGoal4.name = "SubGoal4";
		subGoal4inner.name = "SubGoal4";
		subGoal4Label.name = "SubGoal4";
		page3goals.Add (subGoal4Label);
		
		// Connecting panels
		GameObject subgoal1Panel = MakeImage (localCanvas, new Rect(Screen.width * 0.265f, Screen.height * 0.31f, Screen.width * 0.045f, Screen.height * 0.05f), menuBackground);
		subgoal1Panel.name = "Subgoal1Panel";
		subgoal1Panel.transform.SetParent (page3display.transform, true);
		
		GameObject subgoal2Panel = MakeImage (localCanvas, new Rect(Screen.width * 0.64f, Screen.height * 0.31f, Screen.width * 0.055f, Screen.height * 0.05f), menuBackground);
		subgoal2Panel.name = "Subgoal2Panel";
		subgoal2Panel.transform.SetParent (page3display.transform, true);
		
		GameObject subgoal3Panel = MakeImage (localCanvas, new Rect(Screen.width * 0.265f, Screen.height * 0.675f, Screen.width * 0.045f, Screen.height * 0.05f), menuBackground);
		subgoal3Panel.name = "Subgoal3Panel";
		subgoal3Panel.transform.SetParent (page3display.transform, true);
		
		GameObject subgoal4Panel = MakeImage (localCanvas, new Rect(Screen.width * 0.64f, Screen.height * 0.675f, Screen.width * 0.055f, Screen.height * 0.05f), menuBackground);
		subgoal4Panel.name = "Subgoal4Panel";
		subgoal4Panel.transform.SetParent (page3display.transform, true);

		// Make the next page button.
		GameObject page3NextButton = MakeActionButton (localCanvas, MCP.Text (2803)/*"Next"*/, nextButtonPos, DoStateButtonID, (int)InternalState.page4intro);
		page3NextButton.name = "Page3NextButton";
		page3NextButton.transform.SetParent (page3display.transform, true);
		
		// Make the previous page button.
		GameObject page3BackButton = MakeActionButton (localCanvas, backButtonSprite, backButtonPos, DoStateButtonID, (int)InternalState.page2text);
		page3BackButton.name = "Page3BackButton";
		page3BackButton.transform.SetParent (page3display.transform, true);
		
		page3display.SetActive (false);
		internalScenes.Add ((int)InternalState.page3display, page3display);
		internalSceneTitles.Add ((int)InternalState.page3display, MCP.Text (2807)/*"Step 2 - Order"*/);
		//****** End page 2 - Display goals *****************************************************//
		
		///////////////////////////////////////////////////////////////////////////////////////////
		//***************************************************************************************//
		//******* Page 4 - Draw Intro ***********************************************************//
		//***************************************************************************************//
		///////////////////////////////////////////////////////////////////////////////////////////
		// Make the page 4 into parent object.
		GameObject page4intro = MakePanel (localCanvas, new Rect (Screen.width * 0.01f, Screen.height * 0.01f, Screen.width * 0.98f, Screen.height * 0.98f));//MakeWindowBackground (localCanvas, new Rect(MenuScreen.canvasSize.width * 0.01f, MenuScreen.canvasSize.height * 0.01f , MenuScreen.canvasSize.width * 0.98f, MenuScreen.canvasSize.height * 0.98f), window_back);
		page4intro.name = "Page 4 - Intro";
		
		// Make the text background
		GameObject page4TextBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.33f, Screen.height * 0.15f, Screen.width * 0.64f, Screen.height * 0.65f), window_back);
		page4TextBackground.name = "Page4TextBackground";
		page4TextBackground.transform.SetParent (page4intro.transform, true);
		
		// Make the page text.
		GameObject page4intro_text = MakeLabel (localCanvas, MCP.Text (2810)/*"The success of any type of goal setting is dependent on registering your goals with your subconcious autopilot. The major pathway to your subconcious is through your right brain and your right brain thinks in pictures. Drawing pictures that represent your goals activates your right brain and powerfully connects your goals to your subconscious. You can use simple stick men or basic symbols. Nobody else even needs to understand what your drawing means. This is a communication between your conscious and your subconcious using imagery to represent your goals."*/, new Rect (Screen.width * 0.35f, Screen.height * 0.17f, Screen.width * 0.6f, Screen.height * 0.625f), "SmallText");
		page4intro_text.name = "Page 4 Intro Text";
		page4intro_text.GetComponent<Text>().resizeTextForBestFit = true;
		page4intro_text.GetComponent<Text>().resizeTextMaxSize = (int)_fontSize_Large;
		page4intro_text.GetComponent<Text>().resizeTextMinSize = (int)_fontSize_Small;
		page4intro_text.transform.SetParent (page4intro.transform, true);
		
		// Make the goal map example image.
		GameObject exampleImg = MakeImage (localCanvas, new Rect (Screen.width * 0.03f, Screen.height * 0.15f, Screen.width * 0.285f, Screen.width * 0.35f), goalMapExample);
		exampleImg.name = "GM Example";
		exampleImg.transform.SetParent (page4intro.transform, true);

		// Make the next page button.
		GameObject page4NextButton = MakeActionButton (localCanvas, MCP.Text (2803)/*"Next"*/, nextButtonPos, DoStateButtonID, (int)InternalState.page4display);
		page4NextButton.name = "Page3NextButton";
		page4NextButton.transform.SetParent (page4intro.transform, true);
		
		// Make the previous page button.
		GameObject page4BackButton = MakeActionButton (localCanvas, backButtonSprite, backButtonPos, DoStateButtonID, (int)InternalState.page3display);
		page4BackButton.name = "Page3BackButton";
		page4BackButton.transform.SetParent (page4intro.transform, true);
		
		page4intro.SetActive (false);

		internalScenes.Add ((int)InternalState.page4intro, page4intro);
		internalSceneTitles.Add ((int)InternalState.page4intro, MCP.Text (2811)/*"Step 3 - Draw"*/);
		//****** End page 4 - Draw Intro ********************************************************//
		
		///////////////////////////////////////////////////////////////////////////////////////////
		//***************************************************************************************//
		//****** Start Page 4 - Display Goals ***************************************************//
		//***************************************************************************************//
		///////////////////////////////////////////////////////////////////////////////////////////
		
		page4goals = new List<GameObject> ();
		
		// Make the page 4 display parent object.
		GameObject page4display = MakePanel (localCanvas, new Rect (Screen.width * 0.01f, Screen.height * 0.01f, Screen.width * 0.98f, Screen.height * 0.98f));//MakeWindowBackground (localCanvas, new Rect(MenuScreen.canvasSize.width * 0.01f, MenuScreen.canvasSize.height * 0.01f , MenuScreen.canvasSize.width * 0.98f, MenuScreen.canvasSize.height * 0.98f), window_back);
		page4display.name = "Page 4 - Draw";
		
		// Make the page text.
		GameObject page4text_text = MakeLabel (localCanvas, MCP.Text (2812)	/*"Select a goal to change an image. Please use as much color as possible to help stimulate your right brain."*/, stepTextPos, "SmallText");
		page4text_text.name = "Page 4  Text";
		page4text_text.transform.SetParent (page4display.transform, true);

		// Make the sub goal objects.
		GameObject picSubGoal1 = MakeImage (localCanvas, new Rect (leftX, topY * 1.15f, outerWidth, outerHeight), mapBack, true);
		picSubGoal1.name = "SubGoalBackground";
		picSubGoal1.transform.SetParent (page4display.transform, true);
		
		picSubGoal1inner = MakeImage (localCanvas, new Rect (leftX + paddingX, topY + paddingY + (topY * 0.15f), innerWidth, innerHeight), mapBackInner1);
		picSubGoal1inner.name = "SubGoalInner";
		picSubGoal1inner.transform.SetParent (page4display.transform, true);
		
		picSubGoal1Label = MakeActionButton (localCanvas, MCP.Text (2809)/*"Sub Goal "*/ + "1", new Rect (leftX + paddingX, topY + paddingY + (topY * 0.15f), innerWidth, innerHeight), PictureEdit, 1, "SubGoal");
		picSubGoal1Label.transform.SetParent (page4display.transform, true);
		
		GameObject picSubGoal2 = MakeImage (localCanvas, new Rect (rightX, topY * 1.15f, outerWidth, outerHeight), mapBack, true);
		picSubGoal2.name = "SubGoalBackground";
		picSubGoal2.transform.SetParent (page4display.transform, true);
		
		picSubGoal2inner = MakeImage (localCanvas, new Rect (rightX + paddingX, topY + paddingY + (topY * 0.15f), innerWidth, innerHeight), mapBackInner1);
		picSubGoal2inner.name = "SubGoalInner";
		picSubGoal2inner.transform.SetParent (page4display.transform, true);
		
		picSubGoal2Label = MakeActionButton (localCanvas, MCP.Text (2809)/*"Sub Goal "*/ + "2", new Rect (rightX + paddingX, topY + paddingY + (topY * 0.15f), innerWidth, innerHeight), PictureEdit, 2, "SubGoal");
		picSubGoal2Label.transform.SetParent (page4display.transform, true);

		GameObject picSubGoal3 = MakeImage (localCanvas, new Rect (leftX, bottomY, outerWidth, outerHeight), mapBack, true);
		picSubGoal3.name = "SubGoalBackground";
		picSubGoal3.transform.SetParent (page4display.transform, true);
		
		picSubGoal3inner = MakeImage (localCanvas, new Rect (leftX + paddingX, bottomY + paddingY, innerWidth, innerHeight), mapBackInner1);
		picSubGoal3inner.name = "SubGoalInner";
		picSubGoal3inner.transform.SetParent (page4display.transform, true);
		
		picSubGoal3Label = MakeActionButton (localCanvas, MCP.Text (2809)/*"Sub Goal "*/ + "3", new Rect (leftX + paddingX, bottomY + paddingY, innerWidth, innerHeight), PictureEdit, 3, "SubGoal");
		picSubGoal3Label.transform.SetParent (page4display.transform, true);

		GameObject picSubGoal4 = MakeImage (localCanvas, new Rect (rightX, bottomY, outerWidth, outerHeight), mapBack, true);
		picSubGoal4.name = "SubGoalBackground";
		picSubGoal4.transform.SetParent (page4display.transform, true);
		
		picSubGoal4inner = MakeImage (localCanvas, new Rect (rightX + paddingX, bottomY + paddingY, innerWidth, innerHeight), mapBackInner1);
		picSubGoal4inner.name = "SubGoalInner";
		picSubGoal4inner.transform.SetParent (page4display.transform, true);
		
		picSubGoal4Label = MakeActionButton (localCanvas, MCP.Text (2809)/*"Sub Goal "*/ + "4", new Rect (rightX + paddingX, bottomY + paddingY, innerWidth, innerHeight), PictureEdit, 4, "SubGoal");
		picSubGoal4Label.transform.SetParent (page4display.transform, true);
		
		// Make the main goal objects.
		GameObject picMainGoal = MakeImage (localCanvas, new Rect (mainX, mainY, mainWidth, mainHeight), mapBack, true);
		picMainGoal.name = "MainGoalBackground";
		picMainGoal.transform.SetParent (page4display.transform, true);
		
		picMainGoalInner = MakeImage (localCanvas, new Rect (mainX + mainPaddingX, mainY + mainPaddingY, mainInnerWidth, mainInnerHeight), mapBackInner2, false);
		picMainGoalInner.name = "MainGoalInner";
		picMainGoalInner.transform.SetParent (page4display.transform, true);
		
		picMainGoalLabel = MakeActionButton (localCanvas, MCP.Text (2813)/*"Main Goal"*/, new Rect (mainX + mainPaddingX, mainY + mainPaddingY, mainInnerWidth, mainInnerHeight), PictureEdit, 0, "SubGoal");
		picMainGoalLabel.transform.SetParent (page4display.transform, true);
		
		// Keep track of the pic goal objects.
		page4goals.Add(picMainGoalLabel);
		page4goals.Add(picSubGoal1Label);
		page4goals.Add(picSubGoal2Label);
		page4goals.Add(picSubGoal3Label);
		page4goals.Add(picSubGoal4Label);
		
		// Make the next page button.
		GameObject page4displayNextButton = MakeActionButton (localCanvas, MCP.Text (2803)/*"Next"*/, nextButtonPos, DoStateButtonID, (int)InternalState.page5display);
		page4displayNextButton.name = "Page4NextButton";
		page4displayNextButton.transform.SetParent (page4display.transform, true);
		
		// Make the previous page button.
		GameObject page4displayBackButton = MakeActionButton (localCanvas, backButtonSprite, backButtonPos, DoStateButtonID, (int)InternalState.page4intro);
		page4displayBackButton.name = "Page4BackButton";
		page4displayBackButton.transform.SetParent (page4display.transform, true);
		page4display.SetActive (false);
		internalScenes.Add ((int)InternalState.page4display, page4display);
		internalSceneTitles.Add ((int)InternalState.page4display, MCP.Text (2811)/*"Step 3 - Draw"*/);
		//****** End page 4 - Display goals *****************************************************//
		
		///////////////////////////////////////////////////////////////////////////////////////////
		//***************************************************************************************//
		//****** Start Page 5 - Text Why ********************************************************//
		//***************************************************************************************//
		///////////////////////////////////////////////////////////////////////////////////////////
		
		// Make the page 5 display parent object.
		GameObject page5display = MakePanel (localCanvas, new Rect (Screen.width * 0.01f, Screen.height * 0.01f, Screen.width * 0.98f, Screen.height * 0.98f));//MakeWindowBackground (localCanvas, new Rect(MenuScreen.canvasSize.width * 0.01f, MenuScreen.canvasSize.height * 0.01f , MenuScreen.canvasSize.width * 0.98f, MenuScreen.canvasSize.height * 0.98f), window_back);
		page5display.name = "Page 5 - Why";
		
		// Make the page text.
		GameObject page5text_text = MakeLabel (localCanvas, MCP.Text (2814)/*"Step 4 is to identify the emotional reasons for your desire to achieve your goals. If you are creating a personal Goal Map search deeply beyond your logical 'needs' and identify your passionate, positive, desires. Write your reasons in these boxes."*/, new Rect(stepTextPos.x, stepTextPos.y, stepTextPos.width - (Screen.width * 0.02f), stepTextPos.height), "SmallText");
		page5text_text.name = "Page 5  Text";
		page5text_text.GetComponent<Text>().resizeTextForBestFit = true;
		page5text_text.GetComponent<Text>().resizeTextMaxSize = (int)_fontSize_Large;
		page5text_text.GetComponent<Text>().resizeTextMinSize = (int)_fontSize_Small;
		page5text_text.transform.SetParent (page5display.transform, true);

		float middleY = Screen.height * 0.35f;
		float middleX = (Screen.width * 0.475f) - (outerWidth * 0.5f);
		
		// Make the how objects.
		page1Whys = new List<GameObject>();
		
		// Make the why objects.
		GameObject why1 = MakeImage (localCanvas, new Rect (leftX, bottomY, outerWidth, outerHeight), mapBack, true);
		why1.name = "Why1Image";
		why1.transform.SetParent (page5display.transform, true);
		
		GameObject why1inner = MakeImage (localCanvas, new Rect (leftX + paddingX, bottomY + paddingY, innerWidth, innerHeight), mapBackInner3);
		why1inner.name = "Why1inner";
		why1inner.transform.SetParent (page5display.transform, true);
		
		why1label = MakeInputField (localCanvas, goalMap.whys [0].why, new Rect (leftX + paddingX, bottomY + paddingY, innerWidth, innerHeight), false, mapBackInner3);
		why1label.name = "Why1label";
		why1label.transform.parent.SetParent (page5display.transform, true);
		why1label.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Wrap;
		why1label.GetComponent<Text>().alignment = TextAnchor.UpperCenter;
		why1label.GetComponent<Text>().color = Color.white;
		why1label.GetComponent<Text>().resizeTextForBestFit = true;
		why1label.GetComponent<Text>().resizeTextMinSize = minFontSize;
		why1label.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
		why1label.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;
//		why1label.GetComponent<InputField>().characterLimit = numberOfCharsInInputs;
		why1label.GetComponent<InputField>().onValueChange.AddListener (
			delegate
			{
				why1label.GetComponent<InputField>().text = why1label.GetComponent<InputField>().text.Replace ("\t", "");
					
				if(why1label.GetComponent<InputField>().text.Length > numberOfCharsInInputs)
				{
					why1label.GetComponent<InputField>().text = why1label.GetComponent<InputField>().text.Substring (0, numberOfCharsInInputs);
				}
			});
		page1Whys.Add (why1label);
			
		GameObject why2 = MakeImage (localCanvas, new Rect (middleX, middleY, outerWidth, outerHeight), mapBack, true);
		why2.name = "Why2Image";
		why2.transform.SetParent (page5display.transform, true);
		
		GameObject why2inner = MakeImage (localCanvas, new Rect (middleX + paddingX, middleY + paddingY, innerWidth, innerHeight), mapBackInner3);
		why2inner.name = "Why2inner";
		why2inner.transform.SetParent (page5display.transform, true);
		
		why2label = MakeInputField (localCanvas, goalMap.whys [1].why, new Rect (middleX + paddingX, middleY + paddingY, innerWidth, innerHeight), false, mapBackInner3);
		why2label.name = "Why2label";
		why2label.transform.parent.SetParent (page5display.transform, true);
		why2label.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Wrap;
		why2label.GetComponent<Text>().alignment = TextAnchor.UpperCenter;
		why2label.GetComponent<Text>().color = Color.white;
		why2label.GetComponent<Text>().resizeTextForBestFit = true;
		why2label.GetComponent<Text>().resizeTextMinSize = minFontSize;
		why2label.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
		why2label.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;
//		why2label.GetComponent<InputField>().characterLimit = numberOfCharsInInputs;
		why2label.GetComponent<InputField>().onValueChange.AddListener (
			delegate
			{
				why2label.GetComponent<InputField>().text = why2label.GetComponent<InputField>().text.Replace ("\t", "");
					
				if(why2label.GetComponent<InputField>().text.Length > numberOfCharsInInputs)
				{
					why2label.GetComponent<InputField>().text = why2label.GetComponent<InputField>().text.Substring (0, numberOfCharsInInputs);
				}
			});
		page1Whys.Add (why2label);
		
		GameObject why3 = MakeImage (localCanvas, new Rect (rightX, bottomY, outerWidth, outerHeight), mapBack, true);
		why3.name = "Why3Image";
		why3.transform.SetParent (page5display.transform, true);
		
		GameObject why3inner = MakeImage (localCanvas, new Rect (rightX + paddingX, bottomY + paddingY, innerWidth, innerHeight), mapBackInner1);
		why3inner.name = "Why3inner";
		why3inner.transform.SetParent (page5display.transform, true);
		
		why3label = MakeInputField (localCanvas, goalMap.whys [2].why, new Rect (rightX + paddingX, bottomY + paddingY, innerWidth, innerHeight), false, mapBackInner3);
		why3label.name = "Why3label";
		why3label.transform.parent.SetParent (page5display.transform, true);
		why3label.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Wrap;
		why3label.GetComponent<Text>().alignment = TextAnchor.UpperCenter;
		why3label.GetComponent<Text>().color = Color.white;
		why3label.GetComponent<Text>().resizeTextForBestFit = true;
		why3label.GetComponent<Text>().resizeTextMinSize = minFontSize;
		why3label.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
		why3label.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;
//		why3label.GetComponent<InputField>().characterLimit = numberOfCharsInInputs;
		why3label.GetComponent<InputField>().onValueChange.AddListener (
			delegate
			{
				why3label.GetComponent<InputField>().text = why3label.GetComponent<InputField>().text.Replace ("\t", "");
					
				if(why3label.GetComponent<InputField>().text.Length > numberOfCharsInInputs)
				{
					why3label.GetComponent<InputField>().text = why3label.GetComponent<InputField>().text.Substring (0, numberOfCharsInInputs);
				}
			});
		page1Whys.Add (why3label);
		
		// Make the swap order buttons for how.
		for(int i = 0; i < numberOfWhys; i++)
		{
			// If this is not the first goal...
			if (i != 0)
			{
				// Make the arrow to move the goal order.
				GameObject enumUpArrow = MakeActionButton (localCanvas, leftArrowIcon, new Rect ((Screen.width * 0.1f) + (i * (Screen.width * 0.3f)), i % 2 == 0 ? (Screen.height * 0.47f) : (Screen.height * 0.62f), Screen.width * 0.05f, Screen.width * 0.05f), DoReorderWhyUpButton, i);
				enumUpArrow.name = "UpArrow " + (i + 1);
				enumUpArrow.transform.SetParent (page5display.transform, true);
			}
			// If this is not the last goal...
			if (i != numberOfWhys - 1)
			{
				// Make the arrow to move the goal order.
				GameObject enumDownArrow = MakeActionButton (localCanvas, rightArrowIcon, new Rect ((Screen.width * 0.2f) + (i * (Screen.width * 0.3f)), i % 2 == 0 ? (Screen.height * 0.47f) : (Screen.height * 0.62f), Screen.width * 0.05f, Screen.width * 0.05f), DoReorderWhyDownButton, i);
				enumDownArrow.name = "DownArrow " + (i + 1);
				enumDownArrow.transform.SetParent (page5display.transform, true);
			}
		}
		
		// Make the next page button.
		GameObject page5displayNextButton = MakeActionButton (localCanvas, MCP.Text (2803)/*"Next"*/, nextButtonPos, DoStateButtonID, (int)InternalState.page6draw);
		page5displayNextButton.name = "Page5NextButton";
		page5displayNextButton.transform.SetParent (page5display.transform, true);
		
		// Make the previous page button.
		GameObject page5displayBackButton = MakeActionButton (localCanvas, backButtonSprite, backButtonPos, DoStateButtonID, (int)InternalState.page4display);
		page5displayBackButton.name = "Page5BackButton";
		page5displayBackButton.transform.SetParent (page5display.transform, true);
		page5display.SetActive (false);
		internalScenes.Add ((int)InternalState.page5display, page5display);
		internalSceneTitles.Add ((int)InternalState.page5display, MCP.Text (2815)/*"Step 4 - Why"*/);
		//****** End page 5 - Text Why **********************************************************//
		
		///////////////////////////////////////////////////////////////////////////////////////////
		//***************************************************************************************//
		//****** Start Page 6 - draw Why ********************************************************//
		//***************************************************************************************//
		///////////////////////////////////////////////////////////////////////////////////////////
		// Make the page 6 parent object.
		GameObject page6draw = MakePanel (localCanvas, new Rect (Screen.width * 0.01f, Screen.height * 0.01f, Screen.width * 0.98f, Screen.height * 0.98f));//MakeWindowBackground (localCanvas, new Rect(MenuScreen.canvasSize.width * 0.01f, MenuScreen.canvasSize.height * 0.01f , MenuScreen.canvasSize.width * 0.98f, MenuScreen.canvasSize.height * 0.98f), window_back);
		page6draw.name = "Page 6 - Why";
		
		// Make the page text.
		GameObject page6text_text = MakeLabel (localCanvas, MCP.Text (2816)/*"As before, to register the 'Whys' with your subconscious, you should now draw a picture corresponding to each of the reasons you have identified. Click a 'Why' to create a picture."*/, stepTextPos, "SmallText");
		page6text_text.name = "Page 6  Text";
		page6text_text.transform.SetParent (page6draw.transform, true);
		
		// Make the who objects.
		page2Whys = new List<GameObject>();
		
		// Make the why objects.
		GameObject drawWhy1 = MakeImage (localCanvas, new Rect (leftX, bottomY, outerWidth, outerHeight), mapBack, true);
		drawWhy1.name = "DrawWhy1";
		drawWhy1.transform.SetParent (page6draw.transform, true);
		
		drawWhy1inner = MakeImage (localCanvas, new Rect (leftX + paddingX, bottomY + paddingY, innerWidth, innerHeight), mapBackInner3);
		drawWhy1inner.name  = "DrawWhy1inner";
		drawWhy1inner.transform.SetParent (page6draw.transform, true);
		
		drawWhy1label = MakeActionButton (localCanvas, MCP.Text (2710)/*"Why"*/ + " 1", new Rect (leftX + paddingX, bottomY + paddingY, innerWidth, innerHeight), PictureEdit, 0, "SubWhy");
		drawWhy1label.name = "DrawWhy1label";
		drawWhy1label.transform.SetParent (page6draw.transform, true);
		page2Whys.Add (drawWhy1label);

		GameObject drawWhy2 = MakeImage (localCanvas, new Rect (middleX, middleY, outerWidth, outerHeight), mapBack, true);
		drawWhy2.name = "DrawWhy2";
		drawWhy2.transform.SetParent (page6draw.transform, true);
		
		drawWhy2inner = MakeImage (localCanvas, new Rect (middleX + paddingX, middleY + paddingY, innerWidth, innerHeight), mapBackInner3);
		drawWhy2inner.name = "DrawWhy2inner";
		drawWhy2inner.transform.SetParent (page6draw.transform, true);
		
		drawWhy2label = MakeActionButton (localCanvas, MCP.Text (2710)/*"Why"*/ + " 2", new Rect (middleX + paddingX, middleY + paddingY, innerWidth, innerHeight), PictureEdit, 1, "SubWhy");
		drawWhy2label.name = "DrawWhy2label";
		drawWhy2label.transform.SetParent (page6draw.transform, true);
		page2Whys.Add (drawWhy2label);

		GameObject drawWhy3 = MakeImage (localCanvas, new Rect (rightX, bottomY, outerWidth, outerHeight), mapBack, true);
		drawWhy3.name = "DrawWhy3";
		drawWhy3.transform.SetParent (page6draw.transform, true);
		
		drawWhy3inner = MakeImage (localCanvas, new Rect (rightX + paddingX, bottomY + paddingY, innerWidth, innerHeight), mapBackInner3);
		drawWhy3inner.name = "DrawWhy3inner";
		drawWhy3inner.transform.SetParent (page6draw.transform, true);
		
		drawWhy3label = MakeActionButton (localCanvas, MCP.Text (2710)/*"Why"*/ + " 3", new Rect (rightX + paddingX, bottomY + paddingY, innerWidth, innerHeight), PictureEdit, 2, "SubWhy");
		drawWhy3label.name = "DrawWhy3label";
		drawWhy3label.transform.SetParent (page6draw.transform, true);
		page2Whys.Add (drawWhy3label);

		// Make the next page button.
		GameObject page6drawNextButton = MakeActionButton (localCanvas, MCP.Text (2803)/*"Next"*/, nextButtonPos, DoStateButtonID, (int)InternalState.page7when);
		page6drawNextButton.name = "Page5NextButton";
		page6drawNextButton.transform.SetParent (page6draw.transform, true);
		
		// Make the previous page button.
		GameObject page6drawBackButton = MakeActionButton (localCanvas, backButtonSprite, backButtonPos, DoStateButtonID, (int)InternalState.page5display);
		page6drawBackButton.name = "Page5BackButton";
		page6drawBackButton.transform.SetParent (page6draw.transform, true);
		page6draw.SetActive (false);
		internalScenes.Add ((int)InternalState.page6draw, page6draw);
		internalSceneTitles.Add ((int)InternalState.page6draw, MCP.Text (2815)/*"Step 4 - Why"*/);
		//****** End page 6 - Text Why **********************************************************//
		
		///////////////////////////////////////////////////////////////////////////////////////////
		//***************************************************************************************//
		//****** Start Page 7 - When ************************************************************//
		//***************************************************************************************//
		///////////////////////////////////////////////////////////////////////////////////////////
		// Make the page 7 draw parent.
		GameObject page7when = MakePanel (localCanvas, new Rect (Screen.width * 0.01f, Screen.height * 0.01f, Screen.width * 0.98f, Screen.height * 0.98f));//MakeWindowBackground (localCanvas, new Rect(MenuScreen.canvasSize.width * 0.01f, MenuScreen.canvasSize.height * 0.01f , MenuScreen.canvasSize.width * 0.98f, MenuScreen.canvasSize.height * 0.98f), window_back);
		page7when.name = "Page 7 - When";
		
		// Make the text background.
		GameObject page7TextBackground = MakeImage (localCanvas, new Rect(stepTextPos.x - (Screen.width * 0.02f), stepTextPos.y, stepTextPos.width - (Screen.width * 0.41f), Screen.height * 0.725f), window_back);
		page7TextBackground.name = "Page7TextBackground";
		page7TextBackground.transform.SetParent (page7when.transform, true);
		
		// Make the page text.
		GameObject page7when_text = MakeLabel (localCanvas, MCP.Text (2817)/*"A defining feature of a goal is that it has a definate achievement date. Stating an achievement date for your goal demonstrates belief in yourself and enables you to prioritize actions. All of your goals may have slightly different achievement dates, so focus on your Main Goal, as you have already identified it as your priority. Work with both sides of your brain, logic and intuition, to identify a date that feels right. Enter your achievement date on the right and your start date on the left."*/, new Rect(stepTextPos.x, stepTextPos.y + (Screen.height * 0.02f), stepTextPos.width - (Screen.width * 0.45f), Screen.height * 0.675f), "SmallText");
		page7when_text.name = "Page 7  When";
		page7when_text.GetComponent<Text>().resizeTextForBestFit = true;
		page7when_text.GetComponent<Text>().resizeTextMaxSize = (int)_fontSize_Large;
		page7when_text.GetComponent<Text>().resizeTextMinSize = (int)_fontSize_Small;
		page7when_text.transform.SetParent (page7when.transform, true);
		//page7when_text.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;

		// Make the timeline objects.
		GameObject drawTimelineInner = MakeImage (localCanvas, new Rect ((rightX + paddingX) + (Screen.width * 0.162f), topY + paddingY + (innerHeight * 0.35f - (Screen.height * 0.05f)), innerWidth * 0.325f, innerHeight * 2.0f), mapBackInner1);
		drawTimelineInner.name = "DrawTimelineInner";
		drawTimelineInner.transform.SetParent (page7when.transform, true);
		
		GameObject drawTimelineLabel = MakeLabel (localCanvas, MCP.Text (2712)/*"TIME LINE"*/, new Rect (rightX + paddingX, topY + paddingY + (innerHeight * 0.35f), innerHeight * 2.0f, innerWidth * 0.4f), "OrangeText");
		drawTimelineLabel.name = "DrawTimelineLabel";
		drawTimelineLabel.transform.SetParent (page7when.transform, true);
		drawTimelineLabel.GetComponent<Text>().color = bmDarkBlue;
		drawTimelineLabel.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
		
		Vector3 newRot = drawTimelineLabel.GetComponent<RectTransform>().localEulerAngles;
		newRot.z = 90f;
		drawTimelineLabel.GetComponent<RectTransform>().localEulerAngles = newRot;
		drawTimelineLabel.GetComponent<RectTransform>().localPosition = new Vector3(Screen.width * 1.34f, Screen.height * 0.475f, 0);
		
		GameObject drawTimelineArrow = MakeImage (localCanvas, new Rect (rightX + paddingX, topY + paddingY + (innerHeight * 0.35f), innerHeight * 1.0f, innerWidth * 0.1f), arrowSprite);
		drawTimelineArrow.name = "DrawTimelineArrow";
		drawTimelineArrow.transform.SetParent (page7when.transform, true);
		drawTimelineArrow.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
		
		newRot = drawTimelineArrow.GetComponent<RectTransform>().localEulerAngles;
		newRot.z = 90f;
		drawTimelineArrow.GetComponent<RectTransform>().localEulerAngles = newRot;
		drawTimelineArrow.GetComponent<RectTransform>().localPosition = new Vector3(Screen.width * 1.3725f, Screen.height * 0.475f, 0);
		
		GameObject drawStartDate = MakeImage (localCanvas, new Rect (middleX + (Screen.width * 0.15f), topY - paddingY + (Screen.height * 0.06f), outerWidth, outerHeight * 0.47f), mapBack, true);
		drawStartDate.name = "DrawStart";
		drawStartDate.transform.SetParent (page7when.transform, true);
		
		GameObject drawStartDateInner = MakeImage (localCanvas, new Rect (middleX + paddingX + (Screen.width * 0.15f), topY + (Screen.height * 0.06f), innerWidth, innerHeight * 0.4f), null);
		drawStartDateInner.name = "DrawStartInner";
		drawStartDateInner.transform.SetParent (page7when.transform, true);
		
		GameObject drawStart = MakeImage (localCanvas, new Rect (rightX + (Screen.width * 0.13f), topY - paddingY + (Screen.height * 0.06f), outerWidth * 0.65f, outerHeight * 0.47f), mapBack, true);
		drawStart.name = "DrawStart";
		drawStart.transform.SetParent (page7when.transform, true);
		
		GameObject drawStartInner = MakeImage (localCanvas, new Rect (rightX + paddingX + (Screen.width * 0.13f), topY + (Screen.height * 0.06f), innerWidth * 0.6f, innerHeight * 0.4f), null);
		drawStartInner.name = "DrawStartInner";
		drawStartInner.transform.SetParent (page7when.transform, true);
		
		GameObject drawStartLabel = MakeLabel (localCanvas, MCP.Text (2818)/*"When"*/, new Rect (rightX + paddingX + (Screen.width * 0.085f), topY - paddingY + (Screen.height * 0.045f), innerWidth, innerHeight * 0.75f), "OrangeText");
		drawStartLabel.name = "DrawStartLabel";
		drawStartLabel.transform.SetParent (page7when.transform, true);
		
		GameObject drawStopDate = MakeImage (localCanvas, new Rect (middleX + (Screen.width * 0.15f), bottomY - paddingY + (Screen.height * 0.12f), outerWidth, outerHeight * 0.47f), mapBack, true);
		drawStopDate.name = "DrawStop";
		drawStopDate.transform.SetParent (page7when.transform, true);
		
		GameObject drawStopDateInner = MakeImage (localCanvas, new Rect (middleX + paddingX + (Screen.width * 0.15f), bottomY + (Screen.height * 0.12f), innerWidth, innerHeight * 0.4f), null);
		drawStopDateInner.name = "DrawStopInner";
		drawStopDateInner.transform.SetParent (page7when.transform, true);
		
		GameObject drawStop = MakeImage (localCanvas, new Rect (rightX + (Screen.width * 0.13f), bottomY - paddingY + (Screen.height * 0.12f), outerWidth * 0.65f, outerHeight * 0.47f), mapBack, true);
		drawStop.name = "DrawStop";
		drawStop.transform.SetParent (page7when.transform, true);
		
		GameObject drawStopInner = MakeImage (localCanvas, new Rect (rightX + paddingX + (Screen.width * 0.13f), bottomY + (Screen.height * 0.12f), innerWidth * 0.6f, innerHeight * 0.4f), null);
		drawStopInner.name = "drawStopInner";
		drawStopInner.transform.SetParent (page7when.transform, true);
		
		GameObject drawStopLabel = MakeLabel (localCanvas, MCP.Text (2713)/*"Start"*/, new Rect (rightX + paddingX + (Screen.width * 0.085f), bottomY - (paddingY * 1.0f) + (Screen.height * 0.1f), innerWidth, innerHeight * 0.75f), "OrangeText");
		drawStopLabel.name = "DrawStopLabel";
		drawStopLabel.transform.SetParent (page7when.transform, true);

		// Make the next page button.
		GameObject page7whenNextButton = MakeActionButton (localCanvas, MCP.Text (2803)/*"Next"*/, nextButtonPos, DoStateButtonID, (int)InternalState.page8draw);
		page7whenNextButton.name = "Page5NextButton";
		page7whenNextButton.transform.SetParent (page7when.transform, true);
		
		// Make the previous page button.
		GameObject page7drawBackButton = MakeActionButton (localCanvas, backButtonSprite, backButtonPos, DoStateButtonID, (int)InternalState.page6draw);
		page7drawBackButton.name = "Page5BackButton";
		page7drawBackButton.transform.SetParent (page7when.transform, true);
		page7when.SetActive (false);
		internalScenes.Add ((int)InternalState.page7when, page7when);
		internalSceneTitles.Add ((int)InternalState.page7when, MCP.Text (2819)/*"Step 5 - When"*/);
		//****** End page 7 - When **************************************************************//

		///////////////////////////////////////////////////////////////////////////////////////////
		//***************************************************************************************//
		//****** Start Page 8 - How Text ********************************************************//
		//***************************************************************************************//
		///////////////////////////////////////////////////////////////////////////////////////////
		// Make page 8 text parent.
		GameObject page8draw = MakePanel (localCanvas, new Rect (Screen.width * 0.01f, Screen.height * 0.01f, Screen.width * 0.98f, Screen.height * 0.98f));//MakeWindowBackground (localCanvas, new Rect(MenuScreen.canvasSize.width * 0.01f, MenuScreen.canvasSize.height * 0.01f , MenuScreen.canvasSize.width * 0.98f, MenuScreen.canvasSize.height * 0.98f), window_back);
		page8draw.name = "Page 8 - How";
		
		// Make the text background.
		GameObject page8TextBackground = MakeImage (localCanvas, new Rect (Screen.width * 0.03f, Screen.height * 0.15f, Screen.width * 0.305f, Screen.height * 0.8f), window_back);
		page8TextBackground.name = "Page8TextBackground";
		page8TextBackground.transform.SetParent (page8draw.transform, true);
		
		// Make the page text
		GameObject page8text_text = MakeLabel (localCanvas, MCP.Text (2820)/*"Step 6 is to focus on how you will achieve your goals. What are the major actions you will need to take that will move you along a path towards the achievement of your Main Goal? Are there new skills that you will need to gain? Resources that you still need to secure?"*/, new Rect (Screen.width * 0.05f, Screen.height * 0.17f, Screen.width * 0.275f, Screen.height * 0.75f), "SmallText");
		page8text_text.name = "Page 9  Text";
		page8text_text.GetComponent<Text>().resizeTextForBestFit = true;
		page8text_text.GetComponent<Text>().resizeTextMaxSize = (int)_fontSize_Large;
		page8text_text.GetComponent<Text>().resizeTextMinSize = (int)_fontSize_Small;
		page8text_text.transform.SetParent (page8draw.transform, true);
		
		// Make the connectors.
		GameObject how1Connector = MakeImage (localCanvas, new Rect(Screen.width * 0.5f, Screen.height * 0.26f, Screen.width * 0.3f, Screen.height * 0.05f), menuBackground);
		how1Connector.name = "How1Connector";
		how1Connector.transform.SetParent (page8draw.transform, true);
		GameObject how2Connector = MakeImage (localCanvas, new Rect(Screen.width * 0.5f, Screen.height * 0.45f, Screen.width * 0.3f, Screen.height * 0.05f), menuBackground);
		how2Connector.name = "How1Connector";
		how2Connector.transform.SetParent (page8draw.transform, true);
		GameObject how3Connector = MakeImage (localCanvas, new Rect(Screen.width * 0.5f, Screen.height * 0.675f, Screen.width * 0.3f, Screen.height * 0.05f), menuBackground);
		how3Connector.name = "How1Connector";
		how3Connector.transform.SetParent (page8draw.transform, true);

		// Make the how objects.
		page1Hows = new List<GameObject>();
		
		GameObject how1 = MakeImage (localCanvas, new Rect (rightX, topY - (outerHeight * 0.5f), outerWidth, outerHeight), mapBack, true);
		how1.name = "How1Image";
		how1.transform.SetParent (page8draw.transform, true);
		hideWithVideo.Add (how1);
		
		GameObject how1inner = MakeImage (localCanvas, new Rect (rightX + paddingX, topY + paddingY - (outerHeight * 0.5f), innerWidth, innerHeight), mapBackInner4);
		how1inner.name = "How1inner";
		how1inner.transform.SetParent (page8draw.transform, true);
		hideWithVideo.Add (how1inner);
		
		how1label = MakeInputField (localCanvas, goalMap.hows [0].how, new Rect (rightX + (paddingX * 1.5f), topY + paddingY - (outerHeight * 0.5f), innerWidth - (paddingX * 1.0f), innerHeight), false, mapBackInner4);
		how1label.name = "How1label";
		how1label.transform.parent.SetParent (page8draw.transform, true);
		how1label.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Wrap;
		how1label.GetComponent<Text>().alignment = TextAnchor.UpperCenter;
		how1label.GetComponent<Text>().color = Color.white;
		how1label.GetComponent<Text>().resizeTextForBestFit = true;
		how1label.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
		how1label.GetComponent<Text>().resizeTextMinSize = minFontSize;
		how1label.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;
//		how1label.GetComponent<InputField>().characterLimit = numberOfCharsInInputs;
		how1label.GetComponent<InputField>().onValueChange.AddListener (
			delegate
			{
				how1label.GetComponent<InputField>().text = how1label.GetComponent<InputField>().text.Replace ("\t", "");
					
				if(how1label.GetComponent<InputField>().text.Length > numberOfCharsInInputs)
				{
					how1label.GetComponent<InputField>().text = how1label.GetComponent<InputField>().text.Substring (0, numberOfCharsInInputs);
				}
			});
		page1Hows.Add (how1label);
		hideWithVideo.Add (how1label.transform.parent.gameObject);

		GameObject how2 = MakeImage (localCanvas, new Rect (rightX, middleY, outerWidth, outerHeight), mapBack, true);
		how2.name = "How2Image";
		how2.transform.SetParent (page8draw.transform, true);
		
		GameObject how2inner = MakeImage (localCanvas, new Rect (rightX + paddingX, middleY + paddingY, innerWidth, innerHeight), mapBackInner4);
		how2inner.name = "How2inner";
		how2inner.transform.SetParent (page8draw.transform, true);
		
		how2label = MakeInputField (localCanvas, goalMap.hows [1].how, new Rect (rightX + (paddingX * 1.5f), middleY + paddingY, innerWidth - (paddingX * 1.0f), innerHeight), false, mapBackInner4);
		how2label.name = "How2label";
		how2label.transform.parent.SetParent (page8draw.transform, true);
		how2label.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Wrap;
		how2label.GetComponent<Text>().alignment = TextAnchor.UpperCenter;
		how2label.GetComponent<Text>().color = Color.white;
		how2label.GetComponent<Text>().resizeTextForBestFit = true;
		how2label.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
		how2label.GetComponent<Text>().resizeTextMinSize = minFontSize;
		how2label.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;
//		how2label.GetComponent<InputField>().characterLimit = numberOfCharsInInputs;
		how2label.GetComponent<InputField>().onValueChange.AddListener (
			delegate
			{
				how2label.GetComponent<InputField>().text = how2label.GetComponent<InputField>().text.Replace ("\t", "");
					
				if(how2label.GetComponent<InputField>().text.Length > numberOfCharsInInputs)
				{
					how2label.GetComponent<InputField>().text = how2label.GetComponent<InputField>().text.Substring (0, numberOfCharsInInputs);
				}
			});
		page1Hows.Add (how2label);

		GameObject how3 = MakeImage (localCanvas, new Rect (rightX, bottomY + (Screen.height * 0.055f), outerWidth, outerHeight), mapBack, true);
		how3.name = "How3Image";
		how3.transform.SetParent (page8draw.transform, true);
		
		GameObject how3inner = MakeImage (localCanvas, new Rect (rightX + paddingX, bottomY + paddingY + (Screen.height * 0.055f), innerWidth, innerHeight), mapBackInner4);
		how3inner.name = "How3inner";
		how3inner.transform.SetParent (page8draw.transform, true);
		
		how3label = MakeInputField (localCanvas, goalMap.hows [2].how, new Rect (rightX + (paddingX * 1.5f), bottomY + paddingY + (Screen.height * 0.055f), innerWidth - (paddingX * 1.0f), innerHeight), false, mapBackInner4);
		how3label.name = "How3label";
		how3label.transform.parent.SetParent (page8draw.transform, true);
		how3label.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Wrap;
		how3label.GetComponent<Text>().alignment = TextAnchor.UpperCenter;
		how3label.GetComponent<Text>().color = Color.white;
		how3label.GetComponent<Text>().resizeTextForBestFit = true;
		how3label.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
		how3label.GetComponent<Text>().resizeTextMinSize = minFontSize;
		how3label.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;
//		how3label.GetComponent<InputField>().characterLimit = numberOfCharsInInputs;
		how3label.GetComponent<InputField>().onValueChange.AddListener (
			delegate
			{
				how3label.GetComponent<InputField>().text = how3label.GetComponent<InputField>().text.Replace ("\t", "");
					
				if(how3label.GetComponent<InputField>().text.Length > numberOfCharsInInputs)
				{
					how3label.GetComponent<InputField>().text = how3label.GetComponent<InputField>().text.Substring (0, numberOfCharsInInputs);
				}
			});
		page1Hows.Add (how3label);

		// Make the timeline objects.
		GameObject howRotateTimeline = MakePanel (localCanvas, new Rect (middleX + (outerWidth * 0.535f) + (Screen.width * 0.5f), bottomY, outerWidth, outerHeight * 0.3f));
		howRotateTimeline.name = "HowRotateTimeline";
		howRotateTimeline.transform.SetParent (page8draw.transform, true);

		GameObject timelineHow = MakeImage (howRotateTimeline, new Rect ((Screen.height * 0.4f), paddingY * -3.85f + (Screen.height * 0.5f), outerHeight * 2.75f, outerWidth * 0.25f), mapBack, true);
		timelineHow.name = "TimelineHow";
		GameObject timelineInnerHow = MakeImage (howRotateTimeline, new Rect ((Screen.height * 0.4f), paddingY * -4.6f + (Screen.height * 0.5f), innerHeight * 3.0f, innerWidth * 0.35f), mapBackInner1);
		timelineInnerHow.name = "TimelineInnerHow2";
		GameObject timelineLabelHow = MakeLabel (howRotateTimeline, MCP.Text (2712)/*"TIME LINE"*/, new Rect ((Screen.height * 0.45f), paddingY * -5.0f + (Screen.height * 0.5f), innerHeight * 3.0f, innerWidth * 0.2f), "OrangeText");
		timelineLabelHow.name = "TimelineLabelHow";
		timelineLabelHow.GetComponent<Text>().color = bmDarkBlue;
		GameObject timelineHowArrow = MakeImage (howRotateTimeline, new Rect ((Screen.height * 0.55f), paddingY * -0.6f + (Screen.height * 0.5f), innerHeight * 2.0f, innerWidth * 0.1f), arrowSprite);
		timelineHowArrow.name = "timelineHowArrow";
		howRotateTimeline.transform.Rotate (new Vector3 (0f, 0f, 90f));

		GameObject startHow = MakeImage (localCanvas, new Rect (middleX, bottomY + (outerHeight * 0.3f) + (Screen.height * 0.13f), outerWidth, outerHeight * 0.55f), mapBack, true);
		startHow.name = "StartHow";
		startHow.transform.SetParent (page8draw.transform, true);
		
		GameObject startInnerHow = MakeImage (localCanvas, new Rect (middleX + paddingX, bottomY + paddingY + (innerHeight * 0.25f) + (Screen.height * 0.15f), innerWidth, innerHeight * 0.5f), null);
		startInnerHow.name = "StartInnerHow";
		startInnerHow.transform.SetParent (page8draw.transform, true);
		
		GameObject startLabelHow = MakeLabel (localCanvas, MCP.Text (2713)/*"Start"*/, new Rect (middleX + paddingX, bottomY + paddingY + (innerHeight * 0.15f) + (Screen.height * 0.15f), innerWidth, innerHeight * 0.5f), "OrangeText");
		startLabelHow.name = "StartLabelHow";
		startLabelHow.transform.SetParent (page8draw.transform, true);
		
		startDateLabelHow = MakeLabel (localCanvas, start.ToShortDateString (), new Rect (middleX + paddingX, bottomY + paddingY + (innerHeight * 0.35f) + (Screen.height * 0.15f), innerWidth, innerHeight * 0.5f), "OrangeText");
		startDateLabelHow.name = "StartDateLabelHow";
		startDateLabelHow.transform.SetParent (page8draw.transform, true);

		GameObject stopHow = MakeImage (localCanvas, new Rect (middleX, topY - (outerHeight * 0.15f) - (Screen.height * 0.06f), outerWidth, outerHeight * 0.575f), mapBack, true);
		stopHow.name = "StopHow";
		stopHow.transform.SetParent (page8draw.transform, true);
		
		GameObject stopInnerHow = MakeImage (localCanvas, new Rect (middleX + paddingX, topY - (innerHeight * 0.25f) + paddingY - (Screen.height * 0.045f), innerWidth, innerHeight * 0.52f), null);
		stopInnerHow.name = "StopInnerHow";
		stopInnerHow.transform.SetParent (page8draw.transform, true);
		
		GameObject stopLabelHow = MakeLabel (localCanvas, MCP.Text (2818)/*"When"*/, new Rect (middleX + paddingX, topY - (innerHeight * 0.35f) + paddingY - (Screen.height * 0.05f), innerWidth, innerHeight * 0.5f), "OrangeText");
		stopLabelHow.name = "StopLabelHow";
		stopLabelHow.transform.SetParent (page8draw.transform, true);
		
		stopDateLabelHow = MakeLabel (localCanvas, finish.ToShortDateString (), new Rect (middleX + paddingX, topY - (innerHeight * 0.125f) + paddingY - (Screen.height * 0.05f), innerWidth, innerHeight * 0.5f), "OrangeText");
		stopDateLabelHow.name = "StopDateLabelHow";
		stopDateLabelHow.transform.SetParent (page8draw.transform, true);
		
		// Make the swap order buttons for how.
		for(int i = 0; i < numberOfHows; i++)
		{
			// If this is not the first goal...
			if (i != 0)
			{
				// Make the arrow to move the goal order.
				GameObject enumUpArrow = MakeActionButton (localCanvas, upArrowIcon, new Rect (Screen.width * 0.9f, (i * (Screen.height * 0.275f)) + (Screen.height * 0.12f), Screen.width * 0.05f, Screen.width * 0.05f), DoReorderHowUpButton, i);
				enumUpArrow.name = "UpArrow " + (i + 1);
				enumUpArrow.transform.SetParent (page8draw.transform, true);
			}
			// If this is not the last goal...
			if (i != numberOfHows - 1)
			{
				// Make the arrow to move the goal order.
				GameObject enumDownArrow = MakeActionButton (localCanvas, downArrowIcon, new Rect (Screen.width * 0.9f, (i * (Screen.height * 0.275f)) + (Screen.height * 0.22f), Screen.width * 0.05f, Screen.width * 0.05f), DoReorderHowDownButton, i);
				enumDownArrow.name = "DownArrow " + (i + 1);
				enumDownArrow.transform.SetParent (page8draw.transform, true);
			}
		}

		// Make the next page button.
		GameObject page8drawNextButton = MakeActionButton (localCanvas, MCP.Text (2803)/*"Next"*/, nextButtonPos, DoStateButtonID, (int)InternalState.page9draw);
		page8drawNextButton.name = "Page8NextButton";
		page8drawNextButton.transform.SetParent (page8draw.transform, true);
		
		// Make the previous page button.
		GameObject page8drawBackButton = MakeActionButton (localCanvas, backButtonSprite, backButtonPos, DoStateButtonID, (int)InternalState.page7when);
		page8drawBackButton.name = "Page8BackButton";
		page8drawBackButton.transform.SetParent (page8draw.transform, true);
		page8draw.SetActive (false);
		internalScenes.Add ((int)InternalState.page8draw, page8draw);
		internalSceneTitles.Add ((int)InternalState.page8draw, MCP.Text (2821)/*"Step 6 - How"*/);
		//****** End page 9 - How ***************************************************************//
		
		///////////////////////////////////////////////////////////////////////////////////////////
		//***************************************************************************************//
		//****** Start Page 9 - How Draw*********************************************************//
		//***************************************************************************************//
		///////////////////////////////////////////////////////////////////////////////////////////
		// Make the page 9 draw parent object.
		GameObject page9draw = MakePanel (localCanvas, new Rect (Screen.width * 0.01f, Screen.height * 0.01f, Screen.width * 0.98f, Screen.height * 0.98f));//MakeWindowBackground (localCanvas, new Rect(MenuScreen.canvasSize.width * 0.01f, MenuScreen.canvasSize.height * 0.01f , MenuScreen.canvasSize.width * 0.98f, MenuScreen.canvasSize.height * 0.98f), window_back);
		page9draw.name = "Page 9 - How";
		
		// Make the text background.
		GameObject page9TextBackground = MakeImage (localCanvas, new Rect (Screen.width * 0.03f, Screen.height * 0.15f, Screen.width * 0.305f, Screen.height * 0.8f), window_back);
		page9TextBackground.name = "Page9TextBackground";
		page9TextBackground.transform.SetParent (page9draw.transform, true);
		
		// Make the page text.
		GameObject page9text_text = MakeLabel (localCanvas, MCP.Text (2822)/*"Once you are happy with the order of your 'Hows', create pictures by selecting the boxes to the right. Remember, creating the pictures creates paths to your unconscious autopilot, and they only need to make sense to you."*/, new Rect (Screen.width * 0.05f, Screen.height * 0.17f, Screen.width * 0.275f, Screen.height * 0.85f), "SmallText");
		page9text_text.name = "Page 9  Text";
		page9text_text.transform.SetParent (page9draw.transform, true);
		
		// Make the connectors.
		GameObject drawHow1Connector = MakeImage (localCanvas, new Rect(Screen.width * 0.5f, Screen.height * 0.26f, Screen.width * 0.3f, Screen.height * 0.05f), menuBackground);
		drawHow1Connector.name = "DrawHow1Connector";
		drawHow1Connector.transform.SetParent (page9draw.transform, true);
		GameObject drawHow2Connector = MakeImage (localCanvas, new Rect(Screen.width * 0.5f, Screen.height * 0.45f, Screen.width * 0.3f, Screen.height * 0.05f), menuBackground);
		drawHow2Connector.name = "DrawHow2Connector";
		drawHow2Connector.transform.SetParent (page9draw.transform, true);
		GameObject drawHow3Connector = MakeImage (localCanvas, new Rect(Screen.width * 0.5f, Screen.height * 0.675f, Screen.width * 0.3f, Screen.height * 0.05f), menuBackground);
		drawHow3Connector.name = "DrawHow3Connector";
		drawHow3Connector.transform.SetParent (page9draw.transform, true);
		
		// Make the who objects.
		page2Hows = new List<GameObject>();
		
		// Make the how objects.
		GameObject drawHow1 = MakeImage (localCanvas, new Rect (rightX, topY - (outerHeight * 0.5f), outerWidth, outerHeight), mapBack, true);
		drawHow1.name = "DrawHow1";
		drawHow1.transform.SetParent (page9draw.transform, true);
		
		drawHow1inner = MakeImage (localCanvas, new Rect (rightX + paddingX, topY + paddingY - (outerHeight * 0.5f), innerWidth, innerHeight), mapBackInner4);
		drawHow1inner.name = "DrawHow1inner";
		drawHow1inner.transform.SetParent (page9draw.transform, true);
		
		drawHow1label = MakeActionButton (localCanvas, MCP.Text(2709)/*"How"*/ + " 1", new Rect (rightX + paddingX, topY + paddingY - (outerHeight * 0.5f), innerWidth, innerHeight), PictureEdit, 0, "SubWhy");
		drawHow1label.name = "DrawHow1label";
		drawHow1label.transform.SetParent (page9draw.transform, true);
		page2Hows.Add (drawHow1label);

		GameObject drawHow2 = MakeImage (localCanvas, new Rect (rightX, middleY, outerWidth, outerHeight), mapBack, true);
		drawHow2.name = "DrawHow2";
		drawHow2.transform.SetParent (page9draw.transform, true);
		
		drawHow2inner = MakeImage (localCanvas, new Rect (rightX + paddingX, middleY + paddingY, innerWidth, innerHeight), mapBackInner4);
		drawHow2inner.name = "DrawHow2inner";
		drawHow2inner.transform.SetParent (page9draw.transform, true);
		
		drawHow2label = MakeActionButton (localCanvas, MCP.Text (2709)/*"How"*/ + " 2", new Rect (rightX + paddingX, middleY + paddingY, innerWidth, innerHeight), PictureEdit, 1, "SubWhy");
		drawHow2label.name = "DrawHow2label";
		drawHow2label.transform.SetParent (page9draw.transform, true);
		page2Hows.Add (drawHow2label);

		GameObject drawHow3 = MakeImage (localCanvas, new Rect (rightX, bottomY + (Screen.height * 0.055f), outerWidth, outerHeight), mapBack, true);
		drawHow3.name = "DrawHow3";
		drawHow3.transform.SetParent (page9draw.transform, true);
		
		drawHow3inner = MakeImage (localCanvas, new Rect (rightX + paddingX, bottomY + paddingY + (Screen.height * 0.055f), innerWidth, innerHeight), mapBackInner4);
		drawHow3inner.name = "DrawHow3inner";
		drawHow3inner.transform.SetParent (page9draw.transform, true);
		
		drawHow3label = MakeActionButton (localCanvas, MCP.Text (2709)/*"How"*/ + " 3", new Rect (rightX + paddingX, bottomY + paddingY + (Screen.height * 0.055f), innerWidth, innerHeight), PictureEdit, 2, "SubWhy");
		drawHow3label.name = "DrawHow3label";
		drawHow3label.transform.SetParent (page9draw.transform, true);
		page2Hows.Add (drawHow3label);

		// Make the timeline objects.
		GameObject rotateTimeline = MakePanel (localCanvas, new Rect (middleX + (outerWidth * 0.535f) + (Screen.width * 0.5f), bottomY, outerWidth, outerHeight * 0.3f));
		rotateTimeline.name = "RotateTimeline";
		rotateTimeline.transform.SetParent (page9draw.transform, true);

		GameObject drawTimelineHow = MakeImage (rotateTimeline, new Rect ((Screen.height * 0.4f), paddingY * -3.85f + (Screen.height * 0.5f), outerHeight * 2.75f, outerWidth * 0.25f), mapBack, true);
		drawTimelineHow.name = "DrawTimelineHow";
		GameObject drawTimelineInnerHow = MakeImage (rotateTimeline, new Rect ((Screen.height * 0.4f), paddingY * -4.6f + (Screen.height * 0.5f), innerHeight * 3.0f, innerWidth * 0.35f), mapBackInner1);
		drawTimelineInnerHow.name = "DrawTimelineInnerHow";
		GameObject drawTimelineLabelHow = MakeLabel (rotateTimeline, MCP.Text (2712)/*"TIME LINE"*/, new Rect ((Screen.height * 0.45f), paddingY * -5.0f + (Screen.height * 0.5f), innerHeight * 3.0f, innerWidth * 0.2f), "OrangeText");
		drawTimelineLabelHow.name = "DrawTimelineLabelHow";
		drawTimelineLabelHow.GetComponent<Text>().color = bmDarkBlue;
		GameObject timelineDrawHowArrow = MakeImage (rotateTimeline, new Rect ((Screen.height * 0.55f), paddingY * -0.6f + (Screen.height * 0.5f), innerHeight * 2.0f, innerWidth * 0.1f), arrowSprite);
		timelineDrawHowArrow.name = "TimelineDrawHowArrow";
		rotateTimeline.transform.Rotate (new Vector3 (0f, 0f, 90f));

		GameObject drawStartHow = MakeImage (localCanvas, new Rect (middleX, bottomY + (outerHeight * 0.3f) + (Screen.height * 0.13f), outerWidth, outerHeight * 0.55f), mapBack, true);
		drawStartHow.name = "DrawStartHow";
		drawStartHow.transform.SetParent (page9draw.transform, true);
		
		GameObject drawStartInnerHow = MakeImage (localCanvas, new Rect (middleX + paddingX, bottomY + paddingY + (innerHeight * 0.25f) + (Screen.height * 0.15f), innerWidth, innerHeight * 0.5f), null);
		drawStartInnerHow.name = "DrawStartInnerHow";
		drawStartInnerHow.transform.SetParent (page9draw.transform, true);
		
		GameObject drawStartLabelHow = MakeLabel (localCanvas, MCP.Text (2713)/*"Start"*/, new Rect (middleX + paddingX, bottomY + paddingY + (innerHeight * 0.15f) + (Screen.height * 0.15f), innerWidth, innerHeight * 0.5f), "OrangeText");
		drawStartLabelHow.name = "DrawStartLabelHow";
		drawStartLabelHow.transform.SetParent (page9draw.transform, true);
		
		drawStartDateLabelHow = MakeLabel (localCanvas, start.ToShortDateString (), new Rect (middleX + paddingX, bottomY + paddingY + (innerHeight * 0.35f) + (Screen.height * 0.15f), innerWidth, innerHeight * 0.5f), "OrangeText");
		drawStartDateLabelHow.name = "DrawStartDateLabelHow";
		drawStartDateLabelHow.transform.SetParent (page9draw.transform, true);

		GameObject drawStopHow = MakeImage (localCanvas, new Rect (middleX, topY - (outerHeight * 0.15f) - (Screen.height * 0.06f), outerWidth, outerHeight * 0.575f), mapBack, true);
		drawStopHow.name = "DrawStopHow";
		drawStopHow.transform.SetParent (page9draw.transform, true);
		
		GameObject drawStopInnerHow = MakeImage (localCanvas, new Rect (middleX + paddingX, topY - (innerHeight * 0.25f) + paddingY - (Screen.height * 0.045f), innerWidth, innerHeight * 0.52f), null);
		drawStopInnerHow.name = "DrawStopInnerHow";
		drawStopInnerHow.transform.SetParent (page9draw.transform, true);
		
		GameObject drawStopLabelHow = MakeLabel (localCanvas, MCP.Text (2818)/*"When"*/, new Rect (middleX + paddingX, topY - (innerHeight * 0.35f) + paddingY - (Screen.height * 0.05f), innerWidth, innerHeight * 0.5f), "OrangeText");
		drawStopLabelHow.name = "DrawStopLabelHow";
		drawStopLabelHow.transform.SetParent (page9draw.transform, true);
		
		drawStopDateLabelHow = MakeLabel (localCanvas, finish.ToShortDateString (), new Rect (middleX + paddingX, topY - (innerHeight * 0.125f) + paddingY - (Screen.height * 0.05f), innerWidth, innerHeight * 0.5f), "OrangeText");
		drawStopDateLabelHow.name = "DrawStopDateLabelHow";
		drawStopDateLabelHow.transform.SetParent (page9draw.transform, true);

		// Make the next page button.
		GameObject page9drawNextButton = MakeActionButton (localCanvas, MCP.Text (2803)/*"Next"*/, nextButtonPos, DoStateButtonID, (int)InternalState.page10draw);
		page9drawNextButton.name = "Page5NextButton";
		page9drawNextButton.transform.SetParent (page9draw.transform, true);
		
		// Make the previous page button.
		GameObject page9drawBackButton = MakeActionButton (localCanvas, backButtonSprite, backButtonPos, DoStateButtonID, (int)InternalState.page8draw);
		page9drawBackButton.name = "Page5BackButton";
		page9drawBackButton.transform.SetParent (page9draw.transform, true);
		page9draw.SetActive (false);
		internalScenes.Add ((int)InternalState.page9draw, page9draw);
		internalSceneTitles.Add ((int)InternalState.page9draw, MCP.Text (2821)/*"Step 6 - How"*/);
		//****** End page 9 - How ***************************************************************//
		
		///////////////////////////////////////////////////////////////////////////////////////////
		//***************************************************************************************//
		//****** Start Page 10 - Who ************************************************************//
		//***************************************************************************************//
		///////////////////////////////////////////////////////////////////////////////////////////
		// Make the page 10 draw parent object.
		GameObject page10draw = MakePanel (localCanvas, new Rect (Screen.width * 0.01f, Screen.height * 0.01f, Screen.width * 0.98f, Screen.height * 0.98f));//MakeWindowBackground (localCanvas, new Rect(MenuScreen.canvasSize.width * 0.01f, MenuScreen.canvasSize.height * 0.01f , MenuScreen.canvasSize.width * 0.98f, MenuScreen.canvasSize.height * 0.98f), window_back);
		page10draw.name = "Page 10 - Who";
		
		// Make the text background.
		GameObject page10TextBackground = MakeImage (localCanvas, new Rect (rightX - Screen.width * 0.04f, Screen.height * 0.13f, Screen.width * 0.365f, Screen.height * 0.72f), window_back);
		page10TextBackground.name = "Page10TextBackground";
		page10TextBackground.transform.SetParent (page10draw.transform, true);
		
		// Make the page text.
		GameObject page10text_text = MakeLabel (localCanvas, MCP.Text (2823)/*"Throughout history, the most successful men and women have been open and considerate of the advice, guidance or insight of other people. Any meaningful goal will normally require advice, assistance and support from somebody else. Place the names of the person, team or organisation that you feel is most important."*/, new Rect (rightX - Screen.width * 0.02f, Screen.height * 0.145f, Screen.width * 0.325f, Screen.height * 0.7f), "SmallText");
		page10text_text.name = "Page 10  Text";
		page10text_text.GetComponent<Text>().resizeTextForBestFit = true;
		page10text_text.GetComponent<Text>().resizeTextMaxSize = (int)_fontSize_Large;
		page10text_text.GetComponent<Text>().resizeTextMinSize = (int)_fontSize_Small;
		page10text_text.transform.SetParent (page10draw.transform, true);
		
		// Make the connectors.
		GameObject who1Connector = MakeImage (localCanvas, new Rect(Screen.width * 0.2f, Screen.height * 0.26f, Screen.width * 0.3f, Screen.height * 0.05f), menuBackground);
		who1Connector.name = "Who1Connector";
		who1Connector.transform.SetParent (page10draw.transform, true);
		GameObject who2Connector = MakeImage (localCanvas, new Rect(Screen.width * 0.2f, Screen.height * 0.45f, Screen.width * 0.3f, Screen.height * 0.05f), menuBackground);
		who2Connector.name = "Who2Connector";
		who2Connector.transform.SetParent (page10draw.transform, true);
		GameObject who3Connector = MakeImage (localCanvas, new Rect(Screen.width * 0.2f, Screen.height * 0.675f, Screen.width * 0.3f, Screen.height * 0.05f), menuBackground);
		who3Connector.name = "Who3Connector";
		who3Connector.transform.SetParent (page10draw.transform, true);
		
		// Make the who objects.
		page1Whos = new List<GameObject>();
 
		// Make the who objects.
		GameObject drawWho1 = MakeImage (localCanvas, new Rect (leftX + (Screen.width * 0.025f), topY - (outerHeight * 0.5f) + (Screen.height * 0.05f), outerWidth, outerHeight), mapBack, true);
		drawWho1.name = "DrawWho1";
		drawWho1.transform.SetParent (page10draw.transform, true);
		
		GameObject drawWho1inner = MakeImage (localCanvas, new Rect (leftX + paddingX + (Screen.width * 0.025f), topY + paddingY - (outerHeight * 0.5f) + (Screen.height * 0.05f), innerWidth, innerHeight), mapBackInner5);
		drawWho1inner.name = "DrawWho1inner";
		drawWho1inner.transform.SetParent (page10draw.transform, true);
		
		drawWho1label = MakeInputField (localCanvas, goalMap.whos [0].who, new Rect (leftX + (paddingX * 1.5f) + (Screen.width * 0.025f), topY + paddingY - (outerHeight * 0.5f) + (Screen.height * 0.05f), innerWidth - (paddingX * 1.0f), innerHeight), false, mapBackInner5);
		drawWho1label.name = "DrawWho1label";
		drawWho1label.transform.parent.SetParent (page10draw.transform, true);
		drawWho1label.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Wrap;
		drawWho1label.GetComponent<Text>().alignment = TextAnchor.UpperCenter;
		drawWho1label.GetComponent<Text>().color = Color.white;
		drawWho1label.GetComponent<Text>().resizeTextForBestFit = true;
		drawWho1label.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
		drawWho1label.GetComponent<Text>().resizeTextMinSize = minFontSize;
		drawWho1label.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;
//		drawWho1label.GetComponent<InputField>().characterLimit = numberOfCharsInInputs;
		drawWho1label.GetComponent<InputField>().onValueChange.AddListener (
			delegate
			{
				drawWho1label.GetComponent<InputField>().text = drawWho1label.GetComponent<InputField>().text.Replace ("\t", "");
					
				if(drawWho1label.GetComponent<InputField>().text.Length > numberOfCharsInInputs)
				{
					drawWho1label.GetComponent<InputField>().text = drawWho1label.GetComponent<InputField>().text.Substring (0, numberOfCharsInInputs);
				}
			});
			
		page1Whos.Add (drawWho1label);

		GameObject drawWho2 = MakeImage (localCanvas, new Rect (leftX + (Screen.width * 0.025f), middleY + (Screen.height * 0.05f), outerWidth, outerHeight), mapBack, true);
		drawWho2.name = "DrawWho2";
		drawWho2.transform.SetParent (page10draw.transform, true);
		
		GameObject drawWho2inner = MakeImage (localCanvas, new Rect (leftX + paddingX + (Screen.width * 0.025f), middleY + paddingY + (Screen.height * 0.05f), innerWidth, innerHeight), mapBackInner5);
		drawWho2inner.name = "DrawWho2inner";
		drawWho2inner.transform.SetParent (page10draw.transform, true);
		
		drawWho2label = MakeInputField (localCanvas, goalMap.whos [1].who, new Rect (leftX + (paddingX * 1.5f) + (Screen.width * 0.025f), middleY + paddingY + (Screen.height * 0.05f), innerWidth - (paddingX * 1.0f), innerHeight), false, mapBackInner5);
		drawWho2label.name = "DrawWho2label";
		drawWho2label.transform.parent.SetParent (page10draw.transform, true);
		drawWho2label.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Wrap;
		drawWho2label.GetComponent<Text>().alignment = TextAnchor.UpperCenter;
		drawWho2label.GetComponent<Text>().color = Color.white;
		drawWho2label.GetComponent<Text>().resizeTextForBestFit = true;
		drawWho2label.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
		drawWho2label.GetComponent<Text>().resizeTextMinSize = minFontSize;
		drawWho2label.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;
//		drawWho2label.GetComponent<InputField>().characterLimit = numberOfCharsInInputs;
		drawWho2label.GetComponent<InputField>().onValueChange.AddListener (
			delegate
			{
				drawWho2label.GetComponent<InputField>().text = drawWho2label.GetComponent<InputField>().text.Replace ("\t", "");
					
				if(drawWho2label.GetComponent<InputField>().text.Length > numberOfCharsInInputs)
				{
					drawWho2label.GetComponent<InputField>().text = drawWho2label.GetComponent<InputField>().text.Substring (0, numberOfCharsInInputs);
				}
			});
		
		page1Whos.Add (drawWho2label);

		GameObject drawWho3 = MakeImage (localCanvas, new Rect (leftX + (Screen.width * 0.025f), bottomY + (Screen.height * 0.105f), outerWidth, outerHeight), mapBack, true);
		drawWho3.name = "DrawWho3";
		drawWho3.transform.SetParent (page10draw.transform, true);
		
		GameObject drawWho3inner = MakeImage (localCanvas, new Rect (leftX + paddingX + (Screen.width * 0.025f), bottomY + paddingY + (Screen.height * 0.105f), innerWidth, innerHeight), mapBackInner5);
		drawWho3inner.name = "DrawWho3inner";
		drawWho3inner.transform.SetParent (page10draw.transform, true);
		
		drawWho3label = MakeInputField (localCanvas, goalMap.whos [2].who, new Rect (leftX + (paddingX * 1.5f) + (Screen.width * 0.025f), bottomY + paddingY + (Screen.height * 0.105f), innerWidth - (paddingX * 1.0f), innerHeight), false, mapBackInner5);
		drawWho3label.name = "DrawWho3label";
		drawWho3label.transform.parent.SetParent (page10draw.transform, true);
		drawWho3label.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Wrap;
		drawWho3label.GetComponent<Text>().alignment = TextAnchor.UpperCenter;
		drawWho3label.GetComponent<Text>().color = Color.white;
		drawWho3label.GetComponent<Text>().resizeTextForBestFit = true;
		drawWho3label.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
		drawWho3label.GetComponent<Text>().resizeTextMinSize = minFontSize;
		drawWho3label.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;
//		drawWho3label.GetComponent<InputField>().characterLimit = numberOfCharsInInputs;
		drawWho3label.GetComponent<InputField>().onValueChange.AddListener (
			delegate
			{
				drawWho3label.GetComponent<InputField>().text = drawWho3label.GetComponent<InputField>().text.Replace ("\t", "");
					
				if(drawWho3label.GetComponent<InputField>().text.Length > numberOfCharsInInputs)
				{
					drawWho3label.GetComponent<InputField>().text = drawWho3label.GetComponent<InputField>().text.Substring (0, numberOfCharsInInputs);
				}
			});
		
		page1Whos.Add (drawWho3label);
		
		// Make the timeline objects.
		GameObject rotateTimelineWho = MakePanel (localCanvas, new Rect (middleX + (outerWidth * 0.535f) + (Screen.width * 0.5f), bottomY, outerWidth, outerHeight * 0.3f));
		rotateTimelineWho.name = "RotateTimelineWho";
		rotateTimelineWho.transform.SetParent (page10draw.transform, true);

		GameObject drawTimelineWho = MakeImage (rotateTimelineWho, new Rect ((Screen.height * 0.4f), paddingY * -3.85f + (Screen.height * 0.5f), outerHeight * 2.75f, outerWidth * 0.25f), mapBack, true);
		drawTimelineWho.name = "DrawTimelineWho";
		GameObject drawTimelineInnerWho = MakeImage (rotateTimelineWho, new Rect ((Screen.height * 0.4f), paddingY * -4.6f + (Screen.height * 0.5f), innerHeight * 3.0f, innerWidth * 0.35f), mapBackInner1);
		drawTimelineInnerWho.name = "DrawTimelineInnerWho";
		GameObject drawTimelineLabelWho = MakeLabel (rotateTimelineWho, MCP.Text (2712)/*"TIME LINE"*/, new Rect ((Screen.height * 0.45f), paddingY * -5.0f + (Screen.height * 0.5f), innerHeight * 3.0f, innerWidth * 0.2f), "OrangeText");
		drawTimelineLabelWho.name = "DrawTimelineLabelWho";
		drawTimelineLabelWho.GetComponent<Text>().color = bmDarkBlue;
		GameObject timelineWhoArrow = MakeImage (rotateTimelineWho, new Rect ((Screen.height * 0.55f), paddingY * -0.6f + (Screen.height * 0.5f), innerHeight * 2.0f, innerWidth * 0.1f), arrowSprite);
		timelineWhoArrow.name = "TimelineWhoArrow";
		rotateTimelineWho.transform.Rotate (new Vector3 (0f, 0f, 90f));

		GameObject drawStartWho = MakeImage (localCanvas, new Rect (middleX, bottomY + (outerHeight * 0.3f) + (Screen.height * 0.13f), outerWidth, outerHeight * 0.55f), mapBack, true);
		drawStartWho.name = "DrawStartWho";
		drawStartWho.transform.SetParent (page10draw.transform, true);
		
		GameObject drawStartInnerWho = MakeImage (localCanvas, new Rect (middleX + paddingX, bottomY + paddingY + (innerHeight * 0.25f) + (Screen.height * 0.15f), innerWidth, innerHeight * 0.5f), null);
		drawStartInnerWho.name = "DrawStartInnerWho";
		drawStartInnerWho.transform.SetParent (page10draw.transform, true);
		
		GameObject drawStartLabelWho = MakeLabel (localCanvas, MCP.Text (2713)/*"Start"*/, new Rect (middleX + paddingX, bottomY + paddingY + (innerHeight * 0.15f) + (Screen.height * 0.15f), innerWidth, innerHeight * 0.5f), "OrangeText");
		drawStartLabelWho.name = "DrawStartLabelWho";
		drawStartLabelWho.transform.SetParent (page10draw.transform, true);
		
		drawStartDateLabelWho = MakeLabel (localCanvas, start.ToShortDateString (), new Rect (middleX + paddingX, bottomY + paddingY + (innerHeight * 0.35f) + (Screen.height * 0.15f), innerWidth, innerHeight * 0.5f), "OrangeText");
		drawStartDateLabelWho.name = "DrawStartDateLabelWho";
		drawStartDateLabelWho.transform.SetParent (page10draw.transform, true);

		GameObject drawStopWho = MakeImage (localCanvas, new Rect (middleX, topY - (outerHeight * 0.15f) - (Screen.height * 0.06f), outerWidth, outerHeight * 0.575f), mapBack, true);
		drawStopWho.name = "DrawStopWho";
		drawStopWho.transform.SetParent (page10draw.transform, true);
		
		GameObject drawStopInnerWho = MakeImage (localCanvas, new Rect (middleX + paddingX, topY - (innerHeight * 0.25f) + paddingY - (Screen.height * 0.045f), innerWidth, innerHeight * 0.52f), null);
		drawStopInnerWho.name = "drawStopInnerWho";
		drawStopInnerWho.transform.SetParent (page10draw.transform, true);
		
		GameObject drawStopLabelWho = MakeLabel (localCanvas, MCP.Text (2818)/*"When"*/, new Rect (middleX + paddingX, topY - (innerHeight * 0.35f) + paddingY - (Screen.height * 0.05f), innerWidth, innerHeight * 0.5f), "OrangeText");
		drawStopLabelWho.name = "DrawStopLabelWho";
		drawStopLabelWho.transform.SetParent (page10draw.transform, true);
		
		drawStopDateLabelWho = MakeLabel (localCanvas, finish.ToShortDateString (), new Rect (middleX + paddingX, topY - (innerHeight * 0.125f) + paddingY - (Screen.height * 0.05f), innerWidth, innerHeight * 0.5f), "OrangeText");
		drawStopDateLabelWho.name = "DrawStopDateLabelWho";
		drawStopDateLabelWho.transform.SetParent (page10draw.transform, true);
		
		// Make the swap order buttons for how.
		for(int i = 0; i < numberOfWhos; i++)
		{
			// If this is not the first goal...
			if (i != 0)
			{
				// Make the arrow to move the goal order.
				GameObject enumUpArrow = MakeActionButton (localCanvas, upArrowIcon, new Rect (Screen.width * 0.025f, (i * (Screen.height * 0.275f)) + (Screen.height * 0.17f), Screen.width * 0.05f, Screen.width * 0.05f), DoReorderWhoUpButton, i);
				enumUpArrow.name = "UpArrow " + (i + 1);
				enumUpArrow.transform.SetParent (page10draw.transform, true);
			}
			// If this is not the last goal...
			if (i != numberOfWhos - 1)
			{
				// Make the arrow to move the goal order.
				GameObject enumDownArrow = MakeActionButton (localCanvas, downArrowIcon, new Rect (Screen.width * 0.025f, (i * (Screen.height * 0.275f)) + (Screen.height * 0.27f), Screen.width * 0.05f, Screen.width * 0.05f), DoReorderWhoDownButton, i);
				enumDownArrow.name = "DownArrow " + (i + 1);
				enumDownArrow.transform.SetParent (page10draw.transform, true);
			}
		}

		// Make the next page button.
		GameObject page10drawNextButton = MakeActionButton (localCanvas, MCP.Text (2803)/*"Next"*/, nextButtonPos, DoStateButtonID, (int)InternalState.page11draw);
		page10drawNextButton.name = "Page10NextButton";
		page10drawNextButton.transform.SetParent (page10draw.transform, true);
		
		// Make the previous page button.
		GameObject page10drawBackButton = MakeActionButton (localCanvas, backButtonSprite, backButtonPos, DoStateButtonID, (int)InternalState.page9draw);
		page10drawBackButton.name = "Page10BackButton";
		page10drawBackButton.transform.SetParent (page10draw.transform, true);
		page10draw.SetActive (false);
		internalScenes.Add ((int)InternalState.page10draw, page10draw);
		internalSceneTitles.Add ((int)InternalState.page10draw, MCP.Text (2824)/*"Step 7 - Who"*/);
		//****** End page 10 - Who **************************************************************//
		
		///////////////////////////////////////////////////////////////////////////////////////////
		//***************************************************************************************//
		//****** Start Page 11 - Who Draw *******************************************************//
		//***************************************************************************************//
		///////////////////////////////////////////////////////////////////////////////////////////
		// Make page 11 draw parent object.
		GameObject page11draw = MakePanel (localCanvas, new Rect (Screen.width * 0.01f, Screen.height * 0.01f, Screen.width * 0.98f, Screen.height * 0.98f));//MakeWindowBackground (localCanvas, new Rect(MenuScreen.canvasSize.width * 0.01f, MenuScreen.canvasSize.height * 0.01f , MenuScreen.canvasSize.width * 0.98f, MenuScreen.canvasSize.height * 0.98f), window_back);
		page11draw.name = "Page 11 - Who Draw";
		
		// Make the text background.
		GameObject page11TextBackground = MakeImage (localCanvas, new Rect (rightX - Screen.width * 0.04f, Screen.height * 0.125f, Screen.width * 0.365f, Screen.height * 0.72f), window_back);
		page11TextBackground.name = "Page11TextBackground";
		page11TextBackground.transform.SetParent (page11draw.transform, true);
		
		// Make the page text.
		GameObject page11text_text = MakeLabel (localCanvas, MCP.Text (2825)/*"To complete this step, click on a 'Who' to create a picture. Try to use bright colors and make the picture symbolize the person, team or organization you are making a connection with."*/, new Rect (rightX, Screen.height * 0.16f, Screen.width * 0.275f, Screen.height * 0.85f), "SmallText");
		page11text_text.name = "Page 11  Text";
		page11text_text.transform.SetParent (page11draw.transform, true);
		
		// Make the connectors.
		GameObject drawWho1Connector = MakeImage (localCanvas, new Rect(Screen.width * 0.2f, Screen.height * 0.26f, Screen.width * 0.3f, Screen.height * 0.05f), menuBackground);
		drawWho1Connector.name = "DrawWho1Connector";
		drawWho1Connector.transform.SetParent (page11draw.transform, true);
		GameObject drawWho2Connector = MakeImage (localCanvas, new Rect(Screen.width * 0.2f, Screen.height * 0.45f, Screen.width * 0.3f, Screen.height * 0.05f), menuBackground);
		drawWho2Connector.name = "DrawWho2Connector";
		drawWho2Connector.transform.SetParent (page11draw.transform, true);
		GameObject drawWho3Connector = MakeImage (localCanvas, new Rect(Screen.width * 0.2f, Screen.height * 0.675f, Screen.width * 0.3f, Screen.height * 0.05f), menuBackground);
		drawWho3Connector.name = "DrawWho3Connector";
		drawWho3Connector.transform.SetParent (page11draw.transform, true);
		
		// Make the who objects.
		page2Whos = new List<GameObject>();
		
		// Make the who objects.
		GameObject who1 = MakeImage (localCanvas, new Rect (leftX + (Screen.width * 0.025f), topY - (outerHeight * 0.5f) + (Screen.height * 0.05f), outerWidth, outerHeight), mapBack, true);
		who1.name = "Who1Image";
		who1.transform.SetParent (page11draw.transform, true);
		
		who1inner = MakeImage (localCanvas, new Rect (leftX + paddingX + (Screen.width * 0.025f), topY + paddingY - (outerHeight * 0.5f) + (Screen.height * 0.05f), innerWidth, innerHeight), mapBackInner5);
		who1inner.name = "Who1ImageInner";
		who1inner.transform.SetParent (page11draw.transform, true);
		
		who1label = MakeActionButton (localCanvas, MCP.Text (2708)/*"Who"*/ + " 1", new Rect (leftX + paddingX + (Screen.width * 0.025f), topY + paddingY - (outerHeight * 0.5f) + (Screen.height * 0.05f), innerWidth, innerHeight), PictureEdit, 0, "SubWhy");
		who1label.name = "Who1NameLabel";
		who1label.transform.SetParent (page11draw.transform, true);
		page2Whos.Add (who1label);

		GameObject who2 = MakeImage (localCanvas, new Rect (leftX + (Screen.width * 0.025f), middleY + (Screen.height * 0.05f), outerWidth, outerHeight), mapBack, true);
		who2.name = "Who2Image";
		who2.transform.SetParent (page11draw.transform, true);
		
		who2inner = MakeImage (localCanvas, new Rect (leftX + paddingX + (Screen.width * 0.025f), middleY + paddingY + (Screen.height * 0.05f), innerWidth, innerHeight), mapBackInner5);
		who2inner.name = "Who2ImageInner";
		who2inner.transform.SetParent (page11draw.transform, true);
		
		who2label = MakeActionButton (localCanvas, MCP.Text (2708)/*"Who"*/ + " 2", new Rect (leftX + paddingX + (Screen.width * 0.025f), middleY + paddingY + (Screen.height * 0.05f), innerWidth, innerHeight), PictureEdit, 1, "SubWhy");
		who2label.name = "Who2ImageLabel";
		who2label.transform.SetParent (page11draw.transform, true);
		page2Whos.Add (who2label);

		GameObject who3 = MakeImage (localCanvas, new Rect (leftX + (Screen.width * 0.025f), bottomY + (Screen.height * 0.105f), outerWidth, outerHeight), mapBack, true);
		who3.name = "Who3Image";
		who3.transform.SetParent (page11draw.transform, true);
		
		who3inner = MakeImage (localCanvas, new Rect (leftX + paddingX + (Screen.width * 0.025f), bottomY + paddingY + (Screen.height * 0.105f), innerWidth, innerHeight), mapBackInner5);
		who3inner.name = "Who3ImageInner";
		who3inner.transform.SetParent (page11draw.transform, true);
		
		who3label = MakeActionButton (localCanvas, MCP.Text (2708)/*"Who"*/ + " 3", new Rect (leftX + paddingX + (Screen.width * 0.025f), bottomY + paddingY + (Screen.height * 0.105f), innerWidth, innerHeight), PictureEdit, 2, "SubWhy");
		who3label.name = "Who3ImageLabel";
		who3label.transform.SetParent (page11draw.transform, true);
		page2Whos.Add (who3label);

		// Make timeline objects.
		GameObject rotateTimelineWhoDraw = MakePanel (localCanvas, new Rect (middleX + (outerWidth * 0.535f) + (Screen.width * 0.5f), bottomY, outerWidth, outerHeight * 0.3f));
		rotateTimelineWhoDraw.name = "RotateTimelineWhoDraw";
		rotateTimelineWhoDraw.transform.SetParent (page11draw.transform, true);

		GameObject drawTimelineWhoDraw = MakeImage (rotateTimelineWhoDraw, new Rect ((Screen.height * 0.4f), paddingY * -3.85f + (Screen.height * 0.5f), outerHeight * 2.75f, outerWidth * 0.25f), mapBack, true);
		drawTimelineWhoDraw.name = "DrawTimelineWhoDraw";
		GameObject drawTimelineInnerWhoDraw = MakeImage (rotateTimelineWhoDraw, new Rect ((Screen.height * 0.4f), paddingY * -4.6f + (Screen.height * 0.5f), innerHeight * 3.0f, innerWidth * 0.35f), mapBackInner1);
		drawTimelineInnerWhoDraw.name = "DrawTimelineInnerWhoDraw";
		GameObject drawTimelineLabelWhoDraw = MakeLabel (rotateTimelineWhoDraw, MCP.Text (2712)/*"TIME LINE"*/, new Rect ((Screen.height * 0.45f), paddingY * -5.0f + (Screen.height * 0.5f), innerHeight * 3.0f, innerWidth * 0.2f), "OrangeText");
		drawTimelineLabelWhoDraw.name = "DrawTimelineLabelWhoDraw";
		drawTimelineLabelWhoDraw.GetComponent<Text>().color = bmDarkBlue;
		GameObject timelineDrawWhoArrow = MakeImage (rotateTimelineWhoDraw, new Rect ((Screen.height * 0.55f), paddingY * -0.6f + (Screen.height * 0.5f), innerHeight * 2.0f, innerWidth * 0.1f), arrowSprite);
		timelineDrawWhoArrow.name = "TimelineDrawHowArrow";
		rotateTimelineWhoDraw.transform.Rotate (new Vector3 (0f, 0f, 90f));

		GameObject startWho = MakeImage (localCanvas, new Rect (middleX, bottomY + (outerHeight * 0.3f) + (Screen.height * 0.13f), outerWidth, outerHeight * 0.55f), mapBack, true);
		startWho.name = "StartWho";
		startWho.transform.SetParent (page11draw.transform, true);
		
		GameObject startInnerWho = MakeImage (localCanvas, new Rect (middleX + paddingX, bottomY + paddingY + (innerHeight * 0.25f) + (Screen.height * 0.15f), innerWidth, innerHeight * 0.5f), null);
		startInnerWho.name = "StartInnerWho";
		startInnerWho.transform.SetParent (page11draw.transform, true);
		
		GameObject startLabelWho = MakeLabel (localCanvas, MCP.Text (2713)/*"Start"*/, new Rect (middleX + paddingX, bottomY + paddingY + (innerHeight * 0.15f) + (Screen.height * 0.15f), innerWidth, innerHeight * 0.5f), "OrangeText");
		startLabelWho.name = "StartLabelWho";
		startLabelWho.transform.SetParent (page11draw.transform, true);
		
		startDateLabelWho = MakeLabel (localCanvas, start.ToShortDateString (), new Rect (middleX + paddingX, bottomY + paddingY + (innerHeight * 0.35f) + (Screen.height * 0.15f), innerWidth, innerHeight * 0.5f), "OrangeText");
		startDateLabelWho.name = "StartDateLabelWho";
		startDateLabelWho.transform.SetParent (page11draw.transform, true);

		GameObject stopWho = MakeImage (localCanvas, new Rect (middleX, topY - (outerHeight * 0.15f) - (Screen.height * 0.06f), outerWidth, outerHeight * 0.575f), mapBack, true);
		stopWho.name = "StopWho";
		stopWho.transform.SetParent (page11draw.transform, true);
		
		GameObject stopInnerWho = MakeImage (localCanvas, new Rect (middleX + paddingX, topY - (innerHeight * 0.25f) + paddingY - (Screen.height * 0.045f), innerWidth, innerHeight * 0.52f), null);
		stopInnerWho.name = "StopInnerWho";
		stopInnerWho.transform.SetParent (page11draw.transform, true);
		
		GameObject stopLabelWho = MakeLabel (localCanvas, MCP.Text (2818)/*"When"*/, new Rect (middleX + paddingX, topY - (innerHeight * 0.35f) + paddingY - (Screen.height * 0.05f), innerWidth, innerHeight * 0.5f), "OrangeText");
		stopLabelWho.name = "StopLabelWho";
		stopLabelWho.transform.SetParent (page11draw.transform, true);
		
		stopDateLabelWho = MakeLabel (localCanvas, finish.ToShortDateString (), new Rect (middleX + paddingX, topY - (innerHeight * 0.125f) + paddingY - (Screen.height * 0.05f), innerWidth, innerHeight * 0.5f), "OrangeText");
		stopDateLabelWho.name = "StopDateLabelWho";
		stopDateLabelWho.transform.SetParent (page11draw.transform, true);

		// Make the next page button.
		GameObject page11drawNextButton = MakeActionButton (localCanvas, MCP.Text (2803)/*"Next"*/, nextButtonPos, DoStateButtonID, (int)InternalState.page12draw);
		page11drawNextButton.name = "Page11NextButton";
		page11drawNextButton.transform.SetParent (page11draw.transform, true);
		
		// Make the previous page button.
		GameObject page11drawBackButton = MakeActionButton (localCanvas, backButtonSprite, backButtonPos, DoStateButtonID, (int)InternalState.page10draw);
		page11drawBackButton.name = "Page11BackButton";
		page11drawBackButton.transform.SetParent (page11draw.transform, true);
		page11draw.SetActive (false);
		internalScenes.Add ((int)InternalState.page11draw, page11draw);
		internalSceneTitles.Add ((int)InternalState.page11draw, MCP.Text (2824)/*"Step 7 - Who"*/);
		//****** End page 11 - Who Draw *********************************************************//
		
		///////////////////////////////////////////////////////////////////////////////////////////
		//***************************************************************************************//
		//****** Start Page 12 - Outro **********************************************************//
		//***************************************************************************************//
		///////////////////////////////////////////////////////////////////////////////////////////
		// Make the page 12 draw parent object.
		GameObject page12draw = MakePanel (localCanvas, new Rect (Screen.width * 0.01f, Screen.height * 0.01f, Screen.width * 0.98f, Screen.height * 0.98f));//MakeWindowBackground (localCanvas, new Rect(MenuScreen.canvasSize.width * 0.01f, MenuScreen.canvasSize.height * 0.01f , MenuScreen.canvasSize.width * 0.98f, MenuScreen.canvasSize.height * 0.98f), window_back);
		page12draw.name = "Page 12 - Outro";
		
		// Make text background.
		GameObject page12textbackground = MakeImage (localCanvas, new Rect(Screen.width * 0.03f, Screen.height * 0.18f, Screen.width * 0.395f, Screen.height * 0.765f), iconButton);
		page12textbackground.name = "page12textbackground";
		page12textbackground.transform.SetParent (page12draw.transform, true);
		
		// Make the page text.
		GameObject page12text_text = MakeLabel (localCanvas, MCP.Text (2826)/*"Congratulations. In the process of creating your Goal map you have also created a powerful command for your subconscious about what you want, and a succinct plan for your conscious mind on how you are going to achieve it. Complete your Goal Map now by signing it, thereby giving your promise to follow through to the achievement of your goals."*/, new Rect(Screen.width * 0.05f, Screen.height * 0.2f, Screen.width * 0.35f, Screen.height * 0.7f), "SmallText");
		page12text_text.name = "Page 12  Text";
		page12text_text.GetComponent<Text>().resizeTextForBestFit = true;
		page12text_text.GetComponent<Text>().resizeTextMaxSize = (int)_fontSize_Large;
		page12text_text.GetComponent<Text>().resizeTextMinSize = (int)_fontSize_Small;
		page12text_text.transform.SetParent (page12draw.transform, true);
		
		GameObject sigbackground = MakeImage (localCanvas, sigBackgroundBox, iconButton);
		sigbackground.name = "sigbackground";
		sigbackground.transform.SetParent (page12draw.transform, true);
		// Make the signature box.
		sigBox = GameObject.CreatePrimitive (PrimitiveType.Quad);
		
		// Get the background camera.
		GameObject backgroundCameraObject = GameObject.Find ("BackgroundCamera");
		double height, width;

		if(backgroundCameraObject != null)
		{
			height = backgroundCameraObject.GetComponent<Camera> ().orthographicSize * 1f;
	
			width = height * Screen.width / Screen.height;
			sigBox.transform.position = new Vector3 (backgroundCameraObject.GetComponent<Camera> ().transform.position.x + ((float)width * 0.45f), 
			                                         backgroundCameraObject.GetComponent<Camera> ().transform.position.y + ((float)height * 0.08f), -0.05f);
			sigBox.transform.localScale = new Vector3 ((float)width * 1.08f, (float)height * 1.2f, 0.1f);
	
			sigtex = new Texture2D (512, 512);
			
			for(int x = 0; x < sigtex.width; x++)
			{
				for(int y = 0; y < sigtex.height; y++)
				{
					sigtex.SetPixel (x, y, Color.white);
				}
			}
			sigtex.Apply();
			
			Material material = new Material (Shader.Find ("Unlit/Texture"));
			material.color = Color.white;
			sigBox.GetComponent<Renderer> ().sharedMaterial = material;
			sigBox.GetComponent<Renderer> ().sharedMaterial.mainTexture = sigtex;
			
			sigBox.transform.SetParent (backgroundCameraObject.transform, true);
		}
		
		sigBox.SetActive (false);
		
		// Make sign here text.
		page12SignHereText = MakeLabel (localCanvas, "Sign Here", new Rect(Screen.width * 0.45f, Screen.height * 0.2f, Screen.width * 0.5f, Screen.height * 0.55f), TextAnchor.MiddleCenter);
		page12SignHereText.name = "Page12SignHereText";
		page12SignHereText.GetComponent<Text>().color = bmOrange;
		page12SignHereText.GetComponent<Text>().fontSize = (int)_fontSize_Large;
		
		Vector3 newPos = page12SignHereText.GetComponent<RectTransform>().localPosition;
		newPos.z = -10;
		page12SignHereText.GetComponent<RectTransform>().localPosition = newPos;
		page12SignHereText.transform.SetParent (page12draw.transform, true);
		
		// Make the reset signature button.
		GameObject page12resetTexture = MakeActionButton (localCanvas, MCP.Text (2827)/*"Reset"*/, new Rect (nextButtonPos.x - (Screen.width * 0.3f), nextButtonPos.y, nextButtonPos.width, nextButtonPos.height), ResetSigTex, 0);
		page12resetTexture.name = "Page12NextButton";
		page12resetTexture.transform.SetParent (page12draw.transform, true);
		
		// Make the next page button.
		GameObject page12drawNextButton = MakeActionButton (localCanvas, MCP.Text (2828)/*"View"*/, nextButtonPos, DoStateButtonID, (int)InternalState.showGoalMap);
		page12drawNextButton.name = "Page12NextButton";
		page12drawNextButton.transform.SetParent (page12draw.transform, true);
		
		// Make the previous page button.
		GameObject page12drawBackButton = MakeActionButton (localCanvas, backButtonSprite, backButtonPos, DoStateButtonID, (int)InternalState.page11draw);
		page12drawBackButton.name = "Page12BackButton";
		page12drawBackButton.transform.SetParent (page12draw.transform, true);
		page12draw.SetActive (false);
		internalScenes.Add ((int)InternalState.page12draw, page12draw);
		internalSceneTitles.Add ((int)InternalState.page12draw, MCP.Text (2829)/*"The Goal Mapping Ritual"*/);
		//****** End page 12 - Outro ************************************************************//
		
		// Make a background to cover the main content during a video.
		videoBackground = MakeWindowBackground (localCanvas, new Rect (Screen.width * 0.02f, Screen.height * 0.13f, Screen.width * 0.96f, Screen.height * 0.85f), videoBackgroundSprite); 
		videoBackground.name = "VideoBackground";
		videoObjects.Add (videoBackground);
		
		GameObject visualPanelObject = MakeWindowBackground (localCanvas, new Rect(Screen.width * 0.2f, Screen.height * 0.225f, Screen.width * 0.6f, Screen.height * 0.6f), menuBackground);
		visualPanelObject.name = "VisualPanelObject";
		videoObjects.Add (visualPanelObject);
		
		// Set the button navigation mode.
		UnityEngine.UI.Navigation nav = new UnityEngine.UI.Navigation();
		nav.mode = UnityEngine.UI.Navigation.Mode.None;
		
		// Make play/pause button
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			ControlPlay
		};
		playButtonObject = MakeButton (localCanvas, "", playSprite[0], new Rect(Screen.width * 0.439f, Screen.height * 0.4f, Screen.width * 0.1f, Screen.width * 0.1f), funcList);
		playButtonObject.name = "PlayButton";
		videoObjects.Add (playButtonObject);
		playButtonObject.GetComponent<Button>().navigation = nav;
		// Change the button transition to sprite swap.
		playButtonObject.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		SpriteState spriteState = new SpriteState();
		playButtonObject.GetComponent<Button>().targetGraphic = playButtonObject.transform.GetChild (0).gameObject.GetComponent<Image>();
		// Set the highlighted sprite.
		spriteState.highlightedSprite = playSprite[1];
		spriteState.pressedSprite = playSprite[1];
		playButtonObject.GetComponent<Button>().spriteState = spriteState;
		
#if !UNITY_ANDROID && !UNITY_IPHONE
		// Make volume slider
		volumeSliderObject = MakeSliderVertical (localCanvas, new Rect(Screen.width * 0.875f, Screen.height * 0.225f, Screen.width * 0.025f, Screen.height * 0.5f));
		volumeSliderObject.name = "VolumeSliderObject";
		videoObjects.Add (volumeSliderObject);
		volumeSliderObject.GetComponent<Slider>().maxValue = 20f;
		volumeSliderObject.GetComponent<Slider>().minValue = 0f;
		volumeSliderObject.GetComponent<Slider>().value = 10f;
		volumeSliderObject.GetComponent<Slider>().wholeNumbers = true;
		
		// Make volume/mute button
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				ControlMute();
				volumeSliderObject.GetComponent<Slider>().interactable = !volumeSliderObject.GetComponent<Slider>().interactable;
			}
		};
		muteButtonObject = MakeButton (localCanvas, "", unmuteSprite, new Rect(Screen.width * 0.8675f, Screen.height * 0.7625f, Screen.width * 0.04f, Screen.width * 0.04f), funcList);
		muteButtonObject.name = "MuteButtonObject";
		videoObjects.Add (muteButtonObject);
#endif
		
		// Set the audio position background.
		GameObject audioPosBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.2f, Screen.height * 0.7125f, Screen.width * 0.6f, Screen.height * 0.1125f), menuBackground);
		audioPosBackground.name = "AudioPosBackground";
		videoObjects.Add (audioPosBackground);
		
		// Make the audio position slider,
		audioPositionSliderObject = MakeSliderHorizontal (localCanvas, new Rect(Screen.width * 0.32f, Screen.height * 0.285f, Screen.width * 0.36f, Screen.height * 0.025f));
		audioPositionSliderObject.name = "AudioPositionSliderObject";
		videoObjects.Add (audioPositionSliderObject);
		// Set the slider attributes.
		audioPositionSliderObject.GetComponent<Slider>().maxValue = 100f;
		audioPositionSliderObject.GetComponent<Slider>().minValue = 0f;
		audioPositionSliderObject.GetComponent<Slider>().value = 0f;
		audioPositionSliderObject.GetComponent<Slider>().interactable = false;
		
		// Make the current time label.
		currentTimeLabel = MakeLabel (localCanvas, "00:00:00", new Rect(Screen.width * 0.205f, Screen.height * 0.7225f, Screen.width * 0.1f, Screen.height * 0.1f), "SmallText");
		currentTimeLabel.name = "CurrentTimeLabel";
		videoObjects.Add (currentTimeLabel);
		// Set the current time attributes.
		currentTimeLabel.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
		currentTimeLabel.GetComponent<Text>().resizeTextForBestFit = false;
		currentTimeLabel.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		
		// Make the total time label.
		totalTimeLabel = MakeLabel (localCanvas, "00:00:00", new Rect(Screen.width * 0.7f, Screen.height * 0.7225f, Screen.width * 0.1f, Screen.height * 0.1f), "SmallText");
		totalTimeLabel.name = "TotalTimeLabel";
		videoObjects.Add (totalTimeLabel);
		// Set the total time label.
		totalTimeLabel.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
		totalTimeLabel.GetComponent<Text>().resizeTextForBestFit = false;
		totalTimeLabel.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		
		// Make the play panel to cover the AV panel.
		funcList = new List<MyVoidFunc>
		{
			ControlPlay
		};
		GameObject playPanel = MakeButton (localCanvas, "", new Rect(Screen.width * 0.2f, Screen.height * 0.225f, Screen.width * 0.6f, Screen.height * 0.5f), funcList);
		playPanel.name = "PlayPanel";
		videoObjects.Add (playPanel);
		
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				if(Debug.isDebugBuild && Application.isEditor)
				{
					Destroy (popup);
					downloadStep = 9;
					allDownloaded = true;
				}
						
				SkipVideo();
			}
		};
		skipButton = MakeButton (localCanvas, MCP.Text (218)/*"Continue"*/, buttonBackground, new Rect(nextButtonPos.x, nextButtonPos.y, nextButtonPos.width, nextButtonPos.height), funcList);
		skipButton.name = "SkipButton";
		videoObjects.Add (skipButton);
		
		// Make downloading videos popup.
		popup = MakePopup (localCanvas, new Rect(Screen.width * 0.25f, Screen.height * 0.25f, Screen.width * 0.5f, Screen.height * 0.5f), MCP.Text (2836)/*"Downloading Videos"*/, "0%", MCP.Text (207)/*"Cancel"*/, CommonTasks.LoadLevel, "Main_Screen");
		popup.name = "Popup";

		///////////////////////////////////////////////////////////////////////////////////////////
		//***************************************************************************************//
		//****** Show goal map ******************************************************************//
		//***************************************************************************************//
		///////////////////////////////////////////////////////////////////////////////////////////
		// Make the view goal map parent object.
		GameObject viewGoalMap = MakePanel (localCanvas, new Rect (Screen.width * 0.01f, Screen.height * 0.01f, Screen.width * 0.98f, Screen.height * 0.98f));//MakeWindowBackground (localCanvas, new Rect(MenuScreen.canvasSize.width * 0.01f, MenuScreen.canvasSize.height * 0.01f , MenuScreen.canvasSize.width * 0.98f, MenuScreen.canvasSize.height * 0.98f), window_back);
		viewGoalMap.name = "Goal Map View";
		
		// Make the mask to hide the parts of the goal map outside of the center of the screen.
		GameObject viewGoalMapMask = MakePanel (viewGoalMap, new Rect (Screen.width * 1.1f, Screen.height * -0.3775f, Screen.width * 0.75f, Screen.height * 0.873f));
		viewGoalMapMask.name = "ViewGoalMapMask";
		viewGoalMapMask.AddComponent<Mask>();
		viewGoalMapMask.GetComponent<Mask>().showMaskGraphic = false;
		viewGoalMapMask.AddComponent<Image>();
		
		// Make the goal map object.
		goalMapPrefab = MakeGoalMap (viewGoalMapMask);
		goalMapPrefab.name = "GoalMapPrefab";
		goalMapPrefab.transform.SetParent(viewGoalMapMask.transform);
		goalMapPrefab.transform.localPosition = new Vector3(Screen.width * 0.4f, Screen.height * 0.65f, 0);
		goalMapPrefab.transform.localScale = Vector3.one;
		
		// Move the image map off the screen initially to stop overlaying.
		goalMapPrefab.transform.GetChild (1).position = new Vector3(0, Screen.height * 100f, 0);
		
		nav = new UnityEngine.UI.Navigation();
		nav.mode = UnityEngine.UI.Navigation.Mode.None;
		
		// Make the zoom in button.
		funcList = new List<MyVoidFunc>
		{
			ZoomIn
		};
		GameObject zoomInButton = MakeButton (localCanvas, "", zoomInSprite[0], new Rect(Screen.width * 0.015f, Screen.height * 0.25f, Screen.width * 0.07f, Screen.width * 0.07f), funcList);
		zoomInButton.name = "ZoomInButton";
		zoomInButton.transform.SetParent (viewGoalMap.transform, true);
		
		// Make hover sprite
		zoomInButton.GetComponent<Button>().navigation = nav;
		zoomInButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		zoomInButton.GetComponent<Button>().targetGraphic = zoomInButton.GetComponentInChildren<Image>();
		spriteState.highlightedSprite = zoomInSprite[1];
		spriteState.pressedSprite = zoomInSprite[1];
		zoomInButton.GetComponent<Button>().spriteState = spriteState;
		
		// Make the zoom out button.
		funcList = new List<MyVoidFunc>
		{
			ZoomOut
		};
		GameObject zoomOutButton = MakeButton (localCanvas, "", zoomOutSprite[0], new Rect(Screen.width * 0.015f, Screen.height * 0.4f, Screen.width * 0.07f, Screen.width * 0.07f), funcList);
		zoomOutButton.name = "ZoomOutButton";
		zoomOutButton.transform.SetParent (viewGoalMap.transform, true);
		// Make hover sprite
		zoomOutButton.GetComponent<Button>().navigation = nav;
		zoomOutButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		zoomOutButton.GetComponent<Button>().targetGraphic = zoomOutButton.GetComponentInChildren<Image>();
		spriteState.highlightedSprite = zoomOutSprite[1];
		spriteState.pressedSprite = zoomOutSprite[1];
		zoomOutButton.GetComponent<Button>().spriteState = spriteState;
		
		// Make the rotate goal map button.
		funcList = new List<MyVoidFunc>
		{
			RotateGoalMap
		};
		GameObject rotateGoalMapButton = MakeButton (localCanvas, "", rotateSprite[0], new Rect(Screen.width * 0.015f, Screen.height * 0.55f, Screen.width * 0.07f, Screen.width * 0.07f), funcList);
		rotateGoalMapButton.name = "RotateGoalMapButton";
		rotateGoalMapButton.transform.SetParent (viewGoalMap.transform, true);
		// Make hover sprite
		rotateGoalMapButton.GetComponent<Button>().navigation = nav;
		rotateGoalMapButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		rotateGoalMapButton.GetComponent<Button>().targetGraphic = rotateGoalMapButton.GetComponentInChildren<Image>();
		spriteState.highlightedSprite = rotateSprite[1];
		spriteState.pressedSprite = rotateSprite[1];
		rotateGoalMapButton.GetComponent<Button>().spriteState = spriteState;
		
		// Make the change from text to/from picture button.
		funcList = new List<MyVoidFunc>
		{
			ChangeTextPicture
		};
		changeTextPicGoalMapButton = MakeButton (localCanvas, "", changeTextPicSprite[0], new Rect(Screen.width * 0.015f, Screen.height * 0.7f, Screen.width * 0.07f, Screen.width * 0.07f), funcList);
		changeTextPicGoalMapButton.name = "ChangeTextPicGoalMapButton";
		changeTextPicGoalMapButton.transform.SetParent (viewGoalMap.transform, true);
		// Make hover sprite
		changeTextPicGoalMapButton.GetComponent<Button>().navigation = nav;
		changeTextPicGoalMapButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		changeTextPicGoalMapButton.GetComponent<Button>().targetGraphic = changeTextPicGoalMapButton.GetComponentInChildren<Image>();
		spriteState.highlightedSprite = changeTextPicSprite[1];
		spriteState.pressedSprite = changeTextPicSprite[1];
		changeTextPicGoalMapButton.GetComponent<Button>().spriteState = spriteState;
		
		// Make the next page button.
		GameObject viewGoalMapNextButton = MakeActionButton (localCanvas, doneSprite[0], new Rect(Screen.width * 0.88f, Screen.height * 0.765f, Screen.width * 0.07f, Screen.width * 0.07f), DoStateButtonID, (int)InternalState.goalMapComplete);
		viewGoalMapNextButton.name = "viewGoalMapNextButton";
		viewGoalMapNextButton.transform.SetParent (viewGoalMap.transform, true);
		// Make hover sprite
		viewGoalMapNextButton.GetComponent<Button>().navigation = nav;
		viewGoalMapNextButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		viewGoalMapNextButton.GetComponent<Button>().targetGraphic = viewGoalMapNextButton.GetComponentInChildren<Image>();
		spriteState.highlightedSprite = doneSprite[1];
		spriteState.pressedSprite = doneSprite[1];
		viewGoalMapNextButton.GetComponent<Button>().spriteState = spriteState;
		
		// Make the goal map done button.
		GameObject viewGoalMapDoneButton = MakeActionButton (localCanvas, MCP.Text (222)/*"DONE"*/, new Rect(Screen.width * 0.84f, Screen.height * 0.85f, Screen.width * 0.15f, Screen.height * 0.12f), DoStateButtonID, (int)InternalState.goalMapComplete, "InnerLabel");
		viewGoalMapDoneButton.name = "ViewGoalMapDoneButton";
		viewGoalMapDoneButton.transform.SetParent (viewGoalMap.transform, true);
		viewGoalMapDoneButton.GetComponent<Image>().enabled = false;
		
		// Make the previous page button.
		GameObject viewGoalMapBackButton = MakeActionButton (localCanvas, backButtonSprite, new Rect(backButtonPos.x, backButtonPos.y, backButtonPos.width, backButtonPos.height), DoStateButtonID, (int)InternalState.page12draw);
		viewGoalMapBackButton.name = "viewGoalMapBackButton";
		viewGoalMapBackButton.transform.SetParent (viewGoalMap.transform, true);
		viewGoalMapBackButton.transform.localScale = Vector3.one;
		
		viewGoalMap.SetActive (false);
		internalScenes.Add ((int)InternalState.showGoalMap, viewGoalMap);
		internalSceneTitles.Add ((int)InternalState.showGoalMap, MCP.Text (2501)/*"My Goal Map"*/);
		// ***** End show goal map **************************************************************//
		
		///////////////////////////////////////////////////////////////////////////////////////////
		//***************************************************************************************//
		// ***** Goal Map Complete **************************************************************//
		//***************************************************************************************//
		///////////////////////////////////////////////////////////////////////////////////////////
		// Make the save goal map parent object.
		GameObject goalMapComplete = MakePanel (localCanvas, new Rect (Screen.width * 0.01f, Screen.height * 0.01f, Screen.width * 0.98f, Screen.height * 0.98f));//MakeWindowBackground (localCanvas, new Rect(MenuScreen.canvasSize.width * 0.01f, MenuScreen.canvasSize.height * 0.01f , MenuScreen.canvasSize.width * 0.98f, MenuScreen.canvasSize.height * 0.98f), window_back);
		goalMapComplete.name = "Goal Map Complete";
		
		// Make a label to explain saving the goal map.
		GameObject goalMapLabel = MakeLabel (localCanvas, MCP.Text (2837)/*"You have now completed your Goal Map! Now share your Goal Map with friends to keep you motivated to follow through on your goals! Otherwise, click on the finish button to return to the main screen."*/, r, "SmallText");
		goalMapLabel.name = "Goal Map Label";
		goalMapLabel.transform.SetParent (goalMapComplete.transform, true);
		goalMapLabel.transform.localScale = Vector3.one;
		
		List<MyVoidFunc> funcShareList = new List<MyVoidFunc>()
		{
			delegate 
			{
				dontResetGMIndex = true;
				saveInProgress = true;
				StartCoroutine(SaveGoalMap());
				Debug.Log( "Saved" );
//				loadShareScreen = true;
				lastMenuScreen.Add ("Main_Screen");
				MCP.gmIndex = MCP.goalMaps.Count;
				Application.LoadLevel( "Share_Screen" );
			}
		};
		
		// Make the save goal map button.
		GameObject shareGoalMapButton = MakeIconButton (localCanvas, MCP.Text (1805)/*"Share"*/, iconButton, shareIcon, new Rect(Screen.width * 0.225f, Screen.height * 0.5f, Screen.width * 0.2f, Screen.height * 0.3f), funcShareList);
		shareGoalMapButton.name = "shareGoalMapButton";
		shareGoalMapButton.transform.SetParent (goalMapComplete.transform, true);
		shareGoalMapButton.transform.localScale = Vector3.one;
		
		// Make an input box to accept the file name.
		//saveGMFileName = MakeInputField (localCanvas, fileName, new Rect(Screen.width * 0.2f, Screen.height * 0.515f, Screen.width * 0.6f, Screen.height * 0.08f), false, buttonBackground);
		//saveGMFileName.name = "SaveGMFileNameInput";
		//saveGMFileName.transform.parent.SetParent (goalMapComplete.transform);
		////		saveGMFileName.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
		//saveGMFileName.GetComponent<InputField>().characterLimit = 64;
		//saveGMFileName.GetComponent<InputField>().onValueChange.AddListener (
		//	delegate
		//	{
		//	saveGMFileName.GetComponent<InputField>().text = saveGMFileName.GetComponent<InputField>().text.Replace ("\t", "");
		//});
		
		// Make the save goal map button.
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				sigBox.SetActive (false);
				gameObject.GetComponent<WebVideoPlayer> ().enabled = false;
				SkipVideo();
				
				// Save the goal map.
				//finish = System.DateTime.Now;
//				saveInProgress = true;
//				StartCoroutine(SaveGoalMap());
				
				ChangeState ((int)InternalState.exit);
				Application.LoadLevel( "Main_Screen" );
			}
		};
		GameObject goalMapDoneButton = MakeIconButton (localCanvas, MCP.Text (2838)/*"Finish"*/, iconButton, mainIcon, new Rect(Screen.width * 0.575f, Screen.height * 0.5f, Screen.width * 0.2f, Screen.height * 0.3f), funcList);
		goalMapDoneButton.name = "GoalMapButton";
		goalMapDoneButton.transform.SetParent (goalMapComplete.transform, true);
		goalMapDoneButton.transform.localScale = Vector3.one;
		
		// Make the previous page button.
		//GameObject saveGoalMapBackButton = MakeActionButton (saveGoalMap, backButtonSprite, new Rect(backButtonPos.x - (Screen.width * 0.0095f), backButtonPos.y - (Screen.height * 0.01f), backButtonPos.width, backButtonPos.height), DoStateButtonID, (int)InternalState.showGoalMap);
		//saveGoalMapBackButton.name = "SaveGoalMapBackButton";
		
		goalMapComplete.SetActive (false);
		internalScenes.Add ((int)InternalState.goalMapComplete, goalMapComplete);
		internalSceneTitles.Add ((int)InternalState.goalMapComplete, MCP.Text (2839)/*"Goal Map Complete"*/);
		// ***** End Goal Map Complete **********************************************************//
		
		// Edit picture background panel.
		GameObject editBackgroundPanel = MakeImage (editCanvas, new Rect(Screen.width * 0.2f, Screen.height * 0.2f, Screen.width * 0.6f, Screen.height * 0.6f), window_back, true);
		editBackgroundPanel.name = "EditBackgroundPanel";
		
		// Edit picture text.
		GameObject editTitleLabel = MakeLabel(editCanvas, MCP.Text (2834)/*"Would you like to edit this picture?"*/, new Rect(Screen.width * 0.35f, Screen.height * 0.35f, Screen.width * 0.3f, Screen.height * 0.15f), TextAnchor.UpperCenter);
		editTitleLabel.name = "EditTitleLabel";
		editTitleLabel.GetComponent<Text>().color = bmOrange;
		
		// Edit picture yes button.
		GameObject editYesButtonBackground = MakeImage (editCanvas, new Rect(Screen.width * 0.25f, Screen.height * 0.65f, Screen.width * 0.15f, Screen.height * 0.1f), buttonBackground, true);
		editYesButtonBackground.name = "EditYesButtonBackground";
		
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				MenuScreen.firstFrameTimer = 0;
				GameObject camObj = GameObject.FindGameObjectWithTag ("MainCamera");
				camObj.AddComponent<painter> ();
				GameObject.Find ("CanvasCamera").GetComponent<Camera>().enabled  = true;
				
				localCanvas.SetActive (true);
				editCanvas.SetActive (false);
				camObj.GetComponent<painter> ().edittingImage = true;
				camObj.GetComponent<painter> ().sceneCanvas = localCanvas;
				camObj.GetComponent<painter> ().editCanvas = editCanvas;
			}
		};
		GameObject editYesButton = MakeButton (editCanvas, MCP.Text (201)/*"Yes"*/, null, new Rect(Screen.width * 0.25f, Screen.height * 0.65f, Screen.width * 0.15f, Screen.height * 0.1f), funcList, "InnerLabel");
		editYesButton.name = "EditYesButton";
		editYesButton.transform.GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
		
		// Edit picture no button.
		GameObject editNoButtonBackground = MakeImage (editCanvas, new Rect(Screen.width * 0.6f, Screen.height * 0.65f, Screen.width * 0.15f, Screen.height * 0.1f), buttonBackground, true);
		editNoButtonBackground.name = "EditNoButtonBackground";
		
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				wantsEditting = false;
				FileBrowserClosed ();
			}
		};
		GameObject editNoButton = MakeButton (editCanvas, MCP.Text (202)/*"No"*/, null, new Rect(Screen.width * 0.6f, Screen.height * 0.65f, Screen.width * 0.15f, Screen.height * 0.1f), funcList, "InnerLabel");
		editNoButton.name = "EditNoButton";
		editNoButton.transform.GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
		
		editCanvas.SetActive (false);
		
#if UNITY_ANDROID || UNITY_IPHONE
//		foreach(GameObject g in videoObjects)
		{
//			g.SetActive (false);
		}
#endif
		
		// Make background shading
		menuBG = MakeImage (localCanvas, new Rect(Screen.width * ( -0.5f + 0.5f ), Screen.height * ( -0.5f + 0.5f ), Screen.width, Screen.height), menuBackground);
		newPos = menuBG.GetComponent<RectTransform>().localPosition;
		newPos.z = -10;
		menuBG.GetComponent<RectTransform>().localPosition = newPos;

		// Make saving panel
		window = MakeImage (localCanvas, new Rect(Screen.width * ( -0.25f + 0.5f ), Screen.height * ( -0.1f + 0.5f ), Screen.width * 0.5f, Screen.height * 0.45f), window_back, true);
		newPos = window.GetComponent<RectTransform>().localPosition;
		newPos.z = -10;
		window.GetComponent<RectTransform>().localPosition = newPos;

		// Make saving text
		saveLabel = MakeLabel (localCanvas, MCP.Text (2835)/*"Saving..."*/, new Rect(Screen.width * ( -0.15f + 0.5f ), Screen.height * ( -0.125f + 0.5f ), Screen.width * 0.3f, Screen.height * 0.2f), TextAnchor.MiddleCenter, "InnerLabel");
		newPos = saveLabel.GetComponent<RectTransform>().localPosition;
		newPos.z = -10;
		saveLabel.GetComponent<RectTransform>().localPosition = newPos;

		// Make saving spinner
		savingSpinner = MakeImage (localCanvas, new Rect(Screen.width * ( 0.5f ), Screen.height * ( 0.5f ), Screen.width * ( 0.08f ), Screen.width * ( 0.08f )), spinnerSprite, false);
		newPos = savingSpinner.GetComponent<RectTransform>().localPosition;
		newPos.z = -10;
		savingSpinner.GetComponent<RectTransform>().localPosition = newPos;
		savingSpinner.name = "SavingSpinner";
		savingSpinner.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
		
		menuBG.SetActive( false );
		window.SetActive( false );
		saveLabel.SetActive( false );
		savingSpinner.SetActive( false );
	}

	/// <summary>
	/// Resets the signature texture.
	/// </summary>
	/// <returns><c>true</c>, if signature texture was reset, <c>false</c> otherwise.</returns>
	/// <param name="i">The index.</param>
	bool ResetSigTex (int i)
	{
		sigtex = new Texture2D (512, 512);
		
		for(int x = 0; x < sigtex.width; x++)
		{
			for(int y = 0; y < sigtex.height; y++)
			{
				sigtex.SetPixel (x, y, Color.white);
			}
		}
		sigtex.Apply();
		
		sigBox.GetComponent<Renderer> ().sharedMaterial.mainTexture = sigtex;
		return true;
	}

	/// <summary>
	/// Draws a square onto the signature texture.
	/// </summary>
	/// <param name="x">The x coordinate.</param>
	/// <param name="y">The y coordinate.</param>
	/// <param name="col">Color.</param>
	private void BrushSquare(int x, int y, Color col)
	{
		sigtex.SetPixel (x, y, col);
		
		// Apply brush size.
		for(int xOff = 0; xOff < (int)1; xOff++)
		{
			for(int yOff = 0; yOff < (int)1; yOff++)
			{
				sigtex.SetPixel (x + xOff, y + yOff, col);
				sigtex.SetPixel (x + xOff, y - yOff, col);
				sigtex.SetPixel (x - xOff, y + yOff, col);
				sigtex.SetPixel (x - xOff, y - yOff, col);
			}
		}
	}
	
	/// <summary>
	/// Brushes from point to point.
	/// </summary>
	/// <param name="start">Start.</param>
	/// <param name="end">End.</param>
	/// <param name="col">Color.</param>
	private void BrushPointToPoint(Vector2 start, Vector2 end, Color col)
	{
		// If the start point is not valid, return.
		if(start == Vector2.zero)
		{
			return;
		}
		
		// If the end point is not valid, treat it as the start point.
		if(end == Vector2.zero)
		{
			end = start;
		}
		
		float lengthX, lengthY;
		lengthX = end.x - start.x;
		lengthY = end.y - start.y;
		
		// Draw for 1000 points between the start and end of the line.
		for(int i = 0; i < 100; i++)
		{
			Vector2 uv = start;
			
			uv.x += (lengthX / 100 * i);
			uv.y += (lengthY / 100 * i);
			
			uv.x *= sigtex.width;
			uv.y *= sigtex.height;
			BrushSquare ((int)-uv.x, (int)uv.y, col);
		}
		
		// Apply the pixel changes.
		sigtex.Apply ();
	}
	
	/// <summary>
	/// Moves the goal map.
	/// </summary>
	/// <param name="pointer">Pointer position.</param>
	/// <param name="pressed">If set to <c>true</c> pressed.</param>
	/// <param name="held">If set to <c>true</c> held.</param>
	/// <param name="unpressed">If set to <c>true</c> unpressed.</param>
	/// <param name="controlRect">Rect where the move can start.</param>
	void MoveGoalMap(Vector2 pointer, bool pressed, bool held, bool unpressed, Rect controlRect)
	{
		// Store the current position of the camera.
		Vector3 newPos = goalMapPrefab.transform.localPosition;
		
		// If this is the initial press.
		if(pressed)
		{
			// If the pointer is within the area the user can start moving...
			if(controlRect.Contains (pointer))
			{
				// Set the drag start to the initial camera position.
				dragStart = goalMapPrefab.transform.localPosition;
				// Get the starting position of the pointer.
				startPan = pointer;
			}
			// Otherwise, reset the start pan.
			else
			{
				startPan = Vector2.zero;
			}
		}
		
		// If the mouse pointer is held...
		if(held && startPan != Vector2.zero)
		{
			// Get the offset between the current mouse position and the new mouse position.
			newPos.x = (startPan.x - pointer.x);
			newPos.y = (startPan.y - pointer.y);
			// Set this offset to the camera position.
			newPos = new Vector3(dragStart.x, dragStart.y, 0) - (newPos / 1f);
			
			// Make sure the camera does not move off the canvas. Change depending on rotation.
			if(goalMapRotated)
			{
				if(newPos.x < screenSize.width * -0.2f)
				{
					newPos.x = screenSize.width * -0.2f;
				}
				
				if(newPos.x > screenSize.width * 0.8f)
				{
					newPos.x = screenSize.width * 0.8f;
				}
				
				if(newPos.y < screenSize.height * -0.0f)
				{
					newPos.y = screenSize.height * -0.0f;
				}
				
				if(newPos.y > screenSize.height * 1.0f)
				{
					newPos.y = screenSize.height * 1.0f;
				}
			}
			else
			{
				if(newPos.x < screenSize.width * 0.0f)
				{
					newPos.x = screenSize.width * 0.0f;
				}
				
				if(newPos.x > screenSize.width * 0.8f)
				{
					newPos.x = screenSize.width * 0.8f;
				}
				
				if(newPos.y < screenSize.height * 0.0f)
				{
					newPos.y = screenSize.height * 0.0f;
				}
				
				if(newPos.y > screenSize.height * 1.35f)
				{
					newPos.y = screenSize.height * 1.35f;
				}
			}
			
			// Set the new camera position.
			goalMapPrefab.transform.localPosition = newPos;
		}
		
		// If the pointer has been released...
		if(unpressed)
		{
			// Reset the initial positions.
			dragStart = Vector2.zero;
			startPan = Vector2.zero;
		}
	}
	
	/// <summary>
	/// Changes between the text goal map and the picture goal map.
	/// </summary>
	public void ChangeTextPicture()
	{
		// Enable/disable text.
		goalMapPrefab.transform.GetChild (0).gameObject.SetActive (!goalMapPrefab.transform.GetChild (0).gameObject.activeSelf);
		// Disable/enable pictures.
		goalMapPrefab.transform.GetChild (1).gameObject.SetActive (!goalMapPrefab.transform.GetChild (1).gameObject.activeSelf);
		
		// Move the image map back on the screen.
		goalMapPrefab.transform.GetChild (0).localPosition = Vector3.zero;
		goalMapPrefab.transform.GetChild (1).localPosition = Vector3.zero;
		Vector3 newPos = goalMapPrefab.transform.localPosition;
		
		// Reset the position.
		if(goalMapRotated)
		{
			newPos.x = Screen.width * 0.25f;
			newPos.y = Screen.height * 0.45f;
		}
		else
		{
			newPos.x = Screen.width * 0.4f;
			newPos.y = Screen.height * 0.65f;
		}
		
		// Set the new camera position.
		goalMapPrefab.transform.localPosition = newPos;
		
		// If the picture object is active...
		if(!goalMapPrefab.transform.GetChild (0).gameObject.activeSelf)
		{
			// Set to the change to text sprite.
			changeTextPicGoalMapButton.GetComponent<Image>().sprite = changeTextPicSprite[2];
			
			// Make hover sprite
			UnityEngine.UI.Navigation nav = new UnityEngine.UI.Navigation();
			nav.mode = UnityEngine.UI.Navigation.Mode.None;
			
			changeTextPicGoalMapButton.GetComponent<Button>().navigation = nav;
			changeTextPicGoalMapButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
			
			SpriteState spriteState = new SpriteState();
			changeTextPicGoalMapButton.GetComponent<Button>().targetGraphic = changeTextPicGoalMapButton.GetComponentInChildren<Image>();
			spriteState.highlightedSprite = changeTextPicSprite[3];
			spriteState.pressedSprite = changeTextPicSprite[3];
			changeTextPicGoalMapButton.GetComponent<Button>().spriteState = spriteState;
		}
		// Otherwise...
		else
		{
			// Set to the change to picture sprite.
			changeTextPicGoalMapButton.GetComponent<Image>().sprite = changeTextPicSprite[0];
			
			// Make hover sprite
			UnityEngine.UI.Navigation nav = new UnityEngine.UI.Navigation();
			nav.mode = UnityEngine.UI.Navigation.Mode.None;
			
			changeTextPicGoalMapButton.GetComponent<Button>().navigation = nav;
			changeTextPicGoalMapButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
			
			SpriteState spriteState = new SpriteState();
			changeTextPicGoalMapButton.GetComponent<Button>().targetGraphic = changeTextPicGoalMapButton.GetComponentInChildren<Image>();
			spriteState.highlightedSprite = changeTextPicSprite[1];
			spriteState.pressedSprite = changeTextPicSprite[1];
			changeTextPicGoalMapButton.GetComponent<Button>().spriteState = spriteState;
		}
	}
	
	/// <summary>
	/// Rotates the goal map.
	/// </summary>
	public void RotateGoalMap()
	{
		goalMapRotated = !goalMapRotated;
		
		if(goalMapRotated)
		{
			// Rotate to 90 degrees.
			goalMapPrefab.transform.eulerAngles = new Vector3(0, 0, 90f);
		}
		else
		{
			// Rotate to 0 degrees.
			goalMapPrefab.transform.eulerAngles = new Vector3(0, 0, 0f);
		}
		
		Vector3 newPos = goalMapPrefab.transform.localPosition;
		
		// Update position to make sure it fits now it's rotated.
		if(goalMapRotated)
		{
			newPos.x = Screen.width * 0.25f;
			newPos.y = Screen.height * 0.45f;
		}
		else
		{
			newPos.x = Screen.width * 0.4f;
			newPos.y = Screen.height * 0.65f;
		}
		
		// Set the new camera position.
		goalMapPrefab.transform.localPosition = newPos;
	}
	
	/// <summary>
	/// Zooms the goal map out.
	/// </summary>
	public void ZoomOut()
	{
		zoom -= 2.0f;
		
		// Round zoom to 2dp.
		zoom = Mathf.Round (zoom * 100f) / 100f;
		
		// Keep the zoom bound within limits.
		if(zoom < -6.0f)
		{
			zoom = -6.0f;
		}
		
		if(zoom > 100f)
		{
			zoom = 100f;
		}
		
		// Set the size to the new zoom.
		goalMapPrefab.transform.localScale = new Vector3(0.5f + (zoom * 0.01f), 0.5f + (zoom * 0.01f), 1);
	}
	
	/// <summary>
	/// Zooms the goal map in.
	/// </summary>
	public void ZoomIn()
	{
		zoom += 2.0f;
		
		// Round zoom to 2dp.
		zoom = Mathf.Round (zoom * 100f) / 100f;
		
		// Keep the zoom bound within limits.
		if(zoom < -6.0f)
		{
			zoom = -6.0f;
		}
		
		if(zoom > 100f)
		{
			zoom = 100f;
		}
		
		// Set the size to the new zoom.
		goalMapPrefab.transform.localScale = new Vector3(0.5f + (zoom * 0.01f), 0.5f + (zoom * 0.01f), 1);
	}
	
	/// <summary>
	/// Process zooming the goal map.
	/// </summary>
	/// <param name="pointer">Pointer position.</param>
	/// <param name="pressed">If set to <c>true</c> pressed.</param>
	/// <param name="held">If set to <c>true</c> held.</param>
	/// <param name="unpressed">If set to <c>true</c> unpressed.</param>
	/// <param name="clickArea">Rect the zooming can start in.</param>
	public void ZoomGoalMap(Vector2 pointer, bool pressed, bool held, bool unpressed, Rect clickArea)
	{
		// If this is the initial press.
		if(pressed)
		{
			// Set the drag start to the current pointer position.
			dragStart = pointer;
			// Set the initial zoom to the current zoom.
			startZoom = zoom;
		}
		
		// If the pointer is released...
		if(unpressed)
		{
			// Reset the start position.
			dragStart = Vector2.zero;
		}
		
		// If the mouse is held and the drag began within the canvas.
		if(held && dragStart != Vector2.zero)
		{
			// Set the zoom to the difference between the current position and the initial zoom.
			zoom = startZoom - ((dragStart.y - pointer.y)) * 0.1f;
		}
		
		// Round zoom to 2dp.
		zoom = Mathf.Round (zoom * 100f) / 100f;
		
		// Keep the zoom bound within limits.
		if(zoom < -6.0f)
		{
			zoom = -6.0f;
		}
		
		if(zoom > 100f)
		{
			zoom = 100f;
		}
		
		// Set the size to the new zoom.
		goalMapPrefab.transform.localScale = new Vector3(0.5f + (zoom * 0.01f), 0.5f + (zoom * 0.01f), 1);
	}
	
	/// <summary>
	/// Process zooming the goal map.
	/// </summary>
	/// <param name="touches">Touches.</param>
	/// <param name="touchArea">Rect the zooming can begin.</param>
	void ZoomGoalMap(Touch[] touches, Rect touchArea)
	{
		// If the second touch has just begun...
		if(touches[1].phase == TouchPhase.Began)
		{
			// Set the initial distance between the touches.
			startDist = Vector2.Distance (touches[0].position, touches[1].position);
			
			// Set the start zoom to the current zoom.
			startZoom = zoom;
		}
		// Set the zoom to the difference between the current position and the initial zoom.
		zoom = (startZoom - ((startDist - Vector2.Distance (touches[0].position, touches[1].position)) * 0.05f));
		
		// Round zoom to 2dp.
		zoom = Mathf.Round (zoom * 100f) / 100f;
		
		// Keep the zoom bound within limits.
		if(zoom < -6.0f)
		{
			zoom = -6.0f;
		}
		
		if(zoom > 100f)
		{
			zoom = 100f;
		}
		
		goalMapPrefab.transform.localScale = new Vector3(0.5f + (zoom * 0.01f), 0.5f + (zoom * 0.01f), 1);
	}
	
	/// <summary>
	/// Zooms the canvas in using the scroll wheel.
	/// </summary>
	/// <param name="zoomAmount">Zoom amount.</param>
	public void ScrollZoomCanvas(float zoomAmount)
	{
		zoom += zoomAmount * 10f;
		
		// Round zoom to 2dp.
		zoom = Mathf.Round (zoom * 100f) / 100f;
		
		// Keep the zoom within set bounds.
		if(zoom < -6.0f)
		{
			zoom = -6.0f;
		}
		
		if(zoom > 100f)
		{
			zoom = 100f;
		}
		
		goalMapPrefab.transform.localScale = new Vector3(0.5f + (zoom * 0.01f), 0.5f + (zoom * 0.01f), 1);
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update ()
	{
		//Runaway words temporary repair/Hack until unity gets it together
		//Replace double space with one space
		if( why1label != null )
		{
			if( why1label.GetComponent<InputField>().text.Contains( "  " ) )
			{
				why1label.GetComponent<InputField>().text = why1label.GetComponent<InputField>().text.Replace( "  ", " " );
			}
		}

		if( why2label != null )
		{
			if( why2label.GetComponent<InputField>().text.Contains( "  " ) )
			{
				why2label.GetComponent<InputField>().text = why2label.GetComponent<InputField>().text.Replace( "  ", " " );
			}
		}

		if( why3label != null )
		{
			if( why3label.GetComponent<InputField>().text.Contains( "  " ) )
			{
				why3label.GetComponent<InputField>().text = why3label.GetComponent<InputField>().text.Replace( "  ", " " );
			}
		}

		if( how1label != null )
		{
			if( how1label.GetComponent<InputField>().text.Contains( "  " ) )
			{
				how1label.GetComponent<InputField>().text = how1label.GetComponent<InputField>().text.Replace( "  ", " " );
			}
		}
		
		if( how2label != null )
		{
			if( how2label.GetComponent<InputField>().text.Contains( "  " ) )
			{
				how2label.GetComponent<InputField>().text = how2label.GetComponent<InputField>().text.Replace( "  ", " " );
			}
		}
		
		if( how3label != null )
		{
			if( how3label.GetComponent<InputField>().text.Contains( "  " ) )
			{
				how3label.GetComponent<InputField>().text = how3label.GetComponent<InputField>().text.Replace( "  ", " " );
			}
		}

		if( drawWho1label != null )
		{
			if( drawWho1label.GetComponent<InputField>().text.Contains( "  " ) )
			{
				drawWho1label.GetComponent<InputField>().text = drawWho1label.GetComponent<InputField>().text.Replace( "  ", " " );
			}
		}
		
		if( drawWho2label != null )
		{
			if( drawWho2label.GetComponent<InputField>().text.Contains( "  " ) )
			{
				drawWho2label.GetComponent<InputField>().text = drawWho2label.GetComponent<InputField>().text.Replace( "  ", " " );
			}
		}
		
		if( drawWho3label != null )
		{
			if( drawWho3label.GetComponent<InputField>().text.Contains( "  " ) )
			{
				drawWho3label.GetComponent<InputField>().text = drawWho3label.GetComponent<InputField>().text.Replace( "  ", " " );
			}
		}

#if !UNITY_ANDROID && !UNITY_IPHONE
		// Download the videos.
		if(!MCP.isTransmitting && downloadStep < 1)
		{
			introClip = MCP.loadedVideo;
			downloadStep = 1;
			// Start the next video downloading.
			MCP.videoFilePath = dreamUrl;
			StartCoroutine(MCP.GetVideoFile ());
		}
		if(!MCP.isTransmitting && downloadStep < 2)
		{
			dreamClip = MCP.loadedVideo;
			downloadStep = 2;
			// Start the next video downloading.
			MCP.videoFilePath = orderUrl;
			StartCoroutine(MCP.GetVideoFile ());
		}
		if(!MCP.isTransmitting && downloadStep < 3)
		{
			orderClip = MCP.loadedVideo;
			downloadStep = 3;
			// Start the next video downloading.
			MCP.videoFilePath = drawUrl;
			StartCoroutine(MCP.GetVideoFile ());
		}
		if(!MCP.isTransmitting && downloadStep < 4)
		{
			drawClip = MCP.loadedVideo;
			downloadStep = 4;
			// Start the next video downloading.
			MCP.videoFilePath = whenUrl;
			StartCoroutine(MCP.GetVideoFile ());
		}
		if(!MCP.isTransmitting && downloadStep < 5)
		{
			whenClip = MCP.loadedVideo;
			downloadStep = 5;
			// Start the next video downloading.
			MCP.videoFilePath = whyUrl;
			StartCoroutine(MCP.GetVideoFile ());
		}
		if(!MCP.isTransmitting && downloadStep < 6)
		{
			whyClip = MCP.loadedVideo;
			downloadStep = 6;
			// Start the next video downloading.
			MCP.videoFilePath = howUrl;
			StartCoroutine(MCP.GetVideoFile ());
		}
		if(!MCP.isTransmitting && downloadStep < 7)
		{
			howClip = MCP.loadedVideo;
			downloadStep = 7;
			// Start the next video downloading.
			MCP.videoFilePath = whoUrl;
			StartCoroutine(MCP.GetVideoFile ());
		}
		if(!MCP.isTransmitting && downloadStep < 8)
		{
			whoClip = MCP.loadedVideo;
			downloadStep = 8;
			// Start the next video downloading.
			MCP.videoFilePath = signUrl;
			StartCoroutine(MCP.GetVideoFile ());
		}
		if(!MCP.isTransmitting && downloadStep < 9)
		{
			signClip = MCP.loadedVideo;
			downloadStep = 9;
			allDownloaded = true;
			Debug.Log ("All Videos Downloaded");
		}
		
		// Play the intro video when it has downloaded.
		if(introClip != null && firstVideo && allDownloaded && playing)
		{
			if(introClip.isReadyToPlay && !introClip.isPlaying)
			{
				GetComponent<GUITexture>().texture = introClip;
				tempMovieTex = GetComponent<GUITexture>().texture as MovieTexture;
				GetComponent<AudioSource>().clip = introClip.audioClip;
				
				tempMovieTex.Play ();
				GetComponent<AudioSource>().Play ();
				firstVideo = false;
				showVideo = true;
			}
		}
#else
		allDownloaded = true;
#endif

		// If not all videos have downloaded...
		if(allDownloaded)
		{
			if(popup != null)
			{
				Destroy (popup);
			}
			
			if(skipButton != null)
			{
				skipButton.GetComponent<Button>().interactable = true;
			}
		}
		else
		{
			// Output a progress depending on the number of download flags hit.
			float dlPercent = (downloadStep / (float)numberOfDownloadSteps) * 100f;
			if(popup != null)
			{
				popup.transform.GetChild (1).gameObject.GetComponent<Text>().text = ((int)dlPercent).ToString () + "%";
			}
			
			GetComponent<GUITexture>().enabled = false;
			
			// Don't allow to skip until all the videos have been downloaded.
			if(skipButton != null)
			{
				skipButton.GetComponent<Button>().interactable = false;
				
				if(Debug.isDebugBuild && Application.isEditor)
				{
					skipButton.GetComponent<Button>().interactable = true;
//					Destroy(popup);
				}
			}
		}
		
		if(showVideo)
		{
			if(allDownloaded && playing)
			{
				movieTimer += Time.deltaTime;
			}

			if( showIntroVideoBackButton != null )
			{
				showIntroVideoBackButton.SetActive( false );
			}
			
#if !UNITY_ANDROID && !UNITY_IPHONE
			// Move the video if the side gizmo is out.
			if(localCanvas != null)
			{
				GetComponent<GUITexture>().pixelInset = new Rect((Screen.width * 0.71f) - (Screen.width * 0.05f * -localCanvas.GetComponent<RectTransform>().localPosition.x), GetComponent<GUITexture>().pixelInset.y, GetComponent<GUITexture>().pixelInset.width, GetComponent<GUITexture>().pixelInset.height);
			}
			
			if(GetComponent<GUITexture>() != null && GetComponent<GUITexture>().texture != null && (GetComponent<GUITexture>().texture as MovieTexture) != null && (GetComponent<GUITexture>().texture as MovieTexture).duration > 0)
			{
				if(movieTimer > (GetComponent<GUITexture>().texture as MovieTexture).duration)
				{
					showVideo = false;
					ControlPlay();
					
					foreach(GameObject g in videoObjects)
					{
						if(g != null)
						{
							g.SetActive (false);
						}
					}
					
					foreach(GameObject g in hideWithVideo)
					{
						if(g != null)
						{
							g.SetActive (true);
						}
					}
				}
				else
				{
					foreach(GameObject g in hideWithVideo)
					{
						if(g != null)
						{
							g.SetActive (false);
						}
					}
					
					foreach(GameObject g in videoObjects)
					{
						if(g != null)
						{
							g.SetActive (true);
						}
					}
				}
			}
			
			// If the clip is playing...
			if(videoBackground != null)
			{
				videoBackground.SetActive(true);
			}
			// Make sure the GUITexture doesn't disable itself.
			if(allDownloaded)
			{
				GetComponent<GUITexture>().enabled = true;
			}
#else
			foreach(GameObject g in hideWithVideo)
			{
				if(g != null)
				{
					g.SetActive (false);
				}
			}
			
			foreach(GameObject g in videoObjects)
			{
				if(g != null)
				{
					g.SetActive (true);
				}
			}
#endif
			
			// Disable the progress slider as there is no functionality for jumping to different parts of the video.
			if(audioPositionSliderObject != null && GetComponent<AudioSource>().clip != null)
			{
				audioPositionSliderObject.GetComponent<Slider>().interactable = false;
				audioPositionSliderObject.GetComponent<Slider>().value = 100f - ((GetComponent<AudioSource>().clip.length - movieTimer) / GetComponent<AudioSource>().clip.length * 100f);
			}
			
#if !UNITY_ANDROID && !UNITY_IPHONE
			// Set the current time label text.
			System.TimeSpan currentTime = new System.TimeSpan(0, 0, (int)movieTimer);
			if(currentTimeLabel != null)
			{
				currentTimeLabel.GetComponent<Text>().text = currentTime.ToString ();
			}
			
			MovieTexture videoClip = (GetComponent<GUITexture>().texture as MovieTexture);
			if(videoClip != null)
			{
				if(videoClip.isPlaying)
				{
					// Set the total time label text.
					System.TimeSpan totalTime = new System.TimeSpan(0, 0, (int)GetComponent<AudioSource>().clip.length);
					totalTimeLabel.GetComponent<Text>().text = totalTime.ToString ();
				}
				
				// Check for when the clip is finished.
				if(movieTimer >= videoClip.duration && videoClip.isPlaying)
				{
					// When it is finished, handle stopping properly.
					ControlStop ();
				}
			}
			
			if(volumeSliderObject != null)
			{
				float volume = volumeSliderObject.GetComponent<Slider>().value * 5f;
				GetComponent<AudioSource>().volume = volume / 100f;
			}
#endif
			
			if(internalState == InternalState.page12draw)
			{
				sigBox.SetActive (false);
			}
			
#if !UNITY_ANDROID && !UNITY_IPHONE
			// If the slider is not enabled, fade the scroll handle.
			if(volumeSliderObject != null)
			{
				if(mute)
				{
					float multiplier = volumeSliderObject.GetComponent<Slider>().colors.disabledColor.a;
					Color c = new Color(multiplier, multiplier, multiplier, 1);
					volumeSliderObject.transform.GetChild (2).gameObject.GetComponent<Image>().color = c;
				}
				else
				{
					volumeSliderObject.transform.GetChild (2).gameObject.GetComponent<Image>().color = volumeSliderObject.GetComponent<Slider>().colors.normalColor;
				}
			}
#endif
		}
		else
		{
			if( showIntroVideoBackButton != null )
			{
				showIntroVideoBackButton.SetActive( true );
			}

			// Disable the GUITexture.
			GetComponent<GUITexture>().enabled = false;
			
			if(videoBackground != null)
			{
				videoBackground.SetActive(false);
			}
			
			foreach(GameObject g in videoObjects)
			{
				if(g != null)
				{
					g.SetActive (false);
				}
			}
			
			foreach(GameObject g in hideWithVideo)
			{
				if(g != null)
				{
					g.SetActive (true);
				}
			}
			
			if(internalState == InternalState.page12draw)
			{
				sigBox.SetActive (true);
			}
		}
		
		// If on the signature signing page...
		if (internalState == InternalState.page12draw)
		{
			// If the signature texture is not null...
			if (sigtex != null)
			{
				// Get the pointer position.
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				RaycastHit hit;
				if (Input.GetButton ("Fire1"))
				{
					if (Physics.Raycast (ray, out hit, Mathf.Infinity))
					{
						// Destroy the sign here object when the sign box is clicked.
						if(page12SignHereText != null)
						{
							Destroy (page12SignHereText);
						}
						
						// Find the u,v coordinate of the Texture
						Vector2 uv;
						uv.x = -(hit.point.x - hit.collider.bounds.min.x) / hit.collider.bounds.size.x;
						uv.y = (hit.point.y - hit.collider.bounds.min.y) / hit.collider.bounds.size.y;
						
						// Paint it blue
						BrushPointToPoint (uv, prevUV, Color.blue);
						
						// Apply the pixel changes.
						sigtex.Apply ();

						prevUV = uv;
					}
				}
				else
				{
					prevUV = Vector2.zero;
				}
				
#if !UNITY_ANDROID && !UNITY_IPHONE
				Rect sigBoxBorder = sigBackgroundBox;
				sigBoxBorder.y -= sigBoxBorder.height * 0.1f;
				sigBoxBorder.height = sigBoxBorder.height * 1.2f;
				sigBoxBorder.x -= sigBoxBorder.width * 0.1f;
				sigBoxBorder.width = sigBoxBorder.width * 1.2f;
				
				if (Physics.Raycast (ray, out hit, Mathf.Infinity))
				{
					Cursor.SetCursor (drawTex, Vector2.zero, CursorMode.Auto);
				}
				else if(sigBoxBorder.Contains (Input.mousePosition))
				{
					Cursor.SetCursor (null, Vector2.zero, CursorMode.Auto);
				}
#endif
			}
		}
		
		// If on the show goal map page...
		if(internalState == InternalState.showGoalMap)
		{
			Rect controlRect = new Rect(Screen.width * 0.1f, Screen.height * 0.025f, Screen.width * 0.775f, Screen.height * 0.85f);
			
#if UNITY_ANDROID || UNITY_IPHONE
			// If there are 2 or more touches...
			if(Input.touches.Length > 1)
			{
				// Process zooming.
				ZoomGoalMap(Input.touches, controlRect);
			}
			// Otherwise, if there is a touch, process moving.
			else if(Input.touches.Length > 0)
			{
				MoveGoalMap(Input.touches[0].position,		// Touch pos
					Input.touches[0].phase == TouchPhase.Began ? true : false,		// Whether it is the initial press.
				            true,		// Whether it is the initial press.
				            Input.touches[0].phase == TouchPhase.Ended ? true : false,		// Whether it is the initial press.
				            controlRect);
            }
#else
			// Process moving.
			MoveGoalMap (Input.mousePosition, Input.GetMouseButtonDown (0), Input.GetMouseButton (0), Input.GetMouseButtonUp (0), controlRect);
			// Process zooming.
			ZoomGoalMap(Input.mousePosition, Input.GetMouseButtonDown (1), Input.GetMouseButton (1), Input.GetMouseButtonUp (1), controlRect);
			ScrollZoomCanvas(Input.GetAxis ("Mouse ScrollWheel"));
#endif
		}
		
		// If the goal map is saving...
		if( saveInProgress )
		{
			// Update the saving spinner rotation
			spinnerRot -= Time.deltaTime * 75f;
			savingSpinner.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, spinnerRot);
			
			// If the save is complete...
			if(saveComplete && !MCP.isTransmitting)
			{
				//TODO: Change this to go to the share/finish page.
//				if( loadShareScreen )
				{
//					Application.LoadLevel( "Share_Screen" );
				}
				
				if(internalState == InternalState.exit)
				{
					// Destroy the signature box object.
					Destroy (sigBox);
					// Load the main screen.
					LoadMenu ("Main_Screen");
				}
				//savingSpinner.SetActive( false );
				saveInProgress = false;
				saveComplete = false;
			}
			// Otherwise, if not currently saving then save.
			else if(!MCP.isTransmitting)
			{
				saveInProgress = true;
				StartCoroutine (SaveGoalMap ());
			}

			// Update the saving spinner rotation
			menuBG.SetActive( true );
			window.SetActive( true );
			saveLabel.SetActive( true );
			savingSpinner.SetActive( true );
		}
		else
		{
			// If the save is complete...
			if(saveComplete && !MCP.isTransmitting)
			{
//				if( loadShareScreen )
				{
//					Application.LoadLevel( "Share_Screen" );
				}
				
				if(internalState == InternalState.exit)
				{
					// Destroy the signature box object.
					Destroy (sigBox);
					// Load the main screen.
					LoadMenu ("Main_Screen");
				}
				
				saveInProgress = false;
				saveComplete = false;
			}
			
			if( menuBG != null )
			{
				menuBG.SetActive( false );
			}
			if( window != null )
			{
				window.SetActive( false );
			}
			if( saveLabel != null )
			{
				saveLabel.SetActive( false );
			}
			if( savingSpinner != null )
			{
				savingSpinner.SetActive( false );
			}
		}
		
		// Attach the sigbox to the canvas for side gizmo animation.
		if(sigBox != null && localCanvas != null && sigBox.transform.parent != null && sigBox.transform.parent.gameObject != localCanvas)
		{
			sigBox.transform.SetParent (localCanvas.transform/*, true*/);
			sigBox.transform.localPosition = new Vector3 (Screen.width * 0.195f, 
	                                         		Screen.height * 0.03f, -0.05f);
			
			// Make sure this doesn't move the sig position
//			GameObject backgroundCameraObject = GameObject.Find ("BackgroundCamera");
//			double height, width;
//			
//			if(backgroundCameraObject != null)
//			{
//				height = backgroundCameraObject.GetComponent<Camera> ().orthographicSize * 1f;
//				
//				width = height * Screen.width / Screen.height;
//				sigBox.transform.position = new Vector3 (/*backgroundCameraObject.GetComponent<Camera> ().transform.position.x + */((float)width * 0.45f), 
//				                                         /*backgroundCameraObject.GetComponent<Camera> ().transform.position.y + */((float)height * 0.08f), -0.05f);
//				sigBox.transform.localScale = new Vector3 ((float)width * 1.08f, (float)height * 1.2f, 0.1f);	
//			}
		}
	}
	
#if UNITY_ANDROID || UNITY_IPHONE
	private IEnumerator PlayHandheldMovie(string movie)
	{
		Handheld.PlayFullScreenMovie (movie);
		
//		yield return new WaitForSeconds(2.0f);
		yield return new WaitForEndOfFrame();
		
		SkipVideo();
	}
#endif

	/// <summary>
	/// Action button delegates.
	/// </summary>
	/// <returns><c>true</c>, if state was changed, <c>false</c> otherwise.</returns>
	/// <param name="i">The index.</param>
	bool DoStateButtonID (int i)
	{
		sigBox.SetActive (false);
		gameObject.GetComponent<WebVideoPlayer> ().enabled = false;
		ForceSkipVideo();

		switch (i)
		{
		case (int)InternalState.page1video:
//			if((int)internalState < i)
			{
#if !UNITY_ANDROID && !UNITY_IPHONE
				if(introClip != null && !introClip.isPlaying)
				{
					if(dreamClip != null && dreamClip.isPlaying)
					{
						dreamClip.Stop ();
					}
					
					GetComponent<GUITexture>().texture = introClip;
					GetComponent<AudioSource>().clip = introClip.audioClip;
					
					playing = false;
				}
#endif
			}
			
			break;
		
		case (int)InternalState.page1text:
			if((int)internalState < i)
			{
				showVideo = true;
				
#if !UNITY_ANDROID && !UNITY_IPHONE
				// Play this pages video.
				if(dreamClip != null && !dreamClip.isPlaying)
				{
					movieTimer = 0;
					
					if(introClip != null && introClip.isPlaying)
					{
						introClip.Stop ();
					}
					if(orderClip != null && orderClip.isPlaying)
					{
						orderClip.Stop ();
					}
					
					GetComponent<GUITexture>().texture = dreamClip;
					GetComponent<AudioSource>().clip = dreamClip.audioClip;
					
					playing = false;
					ControlPlay();
				}
				else
				{
					showVideo = false;
				}
#else
				currentVidUrl = dreamUrl;
//				if(!debugSkipVideos)
//				{
//					StartCoroutine (PlayHandheldMovie(dreamUrl));
//				}
#endif
			}
			
			break;
		
		case (int)InternalState.page2text:
			// For each goal...
			for (int x = 0; x < numberOfGoals; x++)
			{
				// Set the goal text to the input text.
				goalMap.goals [x].goal = page1goals [x].GetComponent<InputField> ().text;
				// Set the next page goal text to the stored goal text.
				page2goals [x].GetComponent<Text> ().text = goalMap.goals [x].goal;
				page3goals [x].GetComponent<Text> ().text = goalMap.goals [x].goal;
			}
			
			picMainGoalLabel.transform.GetChild (0).gameObject.GetComponent<Text>().text = goalMap.goals [0].goal;
			picMainGoalLabel.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextForBestFit = true;
			picMainGoalLabel.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMinSize = minFontSize;
			picMainGoalLabel.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMaxSize = maxFontSize;

			picSubGoal1Label.transform.GetChild (0).gameObject.GetComponent<Text>().text = goalMap.goals [1].goal;
			picSubGoal1Label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextForBestFit = true;
			picSubGoal1Label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMinSize = minFontSize;
			picSubGoal1Label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMaxSize = maxFontSize;

			picSubGoal2Label.transform.GetChild (0).gameObject.GetComponent<Text>().text = goalMap.goals [2].goal;
			picSubGoal2Label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextForBestFit = true;
			picSubGoal2Label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMinSize = minFontSize;
			picSubGoal2Label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMaxSize = maxFontSize;

			picSubGoal3Label.transform.GetChild (0).gameObject.GetComponent<Text>().text = goalMap.goals [3].goal;
			picSubGoal3Label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextForBestFit = true;
			picSubGoal3Label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMinSize = minFontSize;
			picSubGoal3Label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMaxSize = maxFontSize;

			picSubGoal4Label.transform.GetChild (0).gameObject.GetComponent<Text>().text = goalMap.goals [4].goal;
			picSubGoal4Label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextForBestFit = true;
			picSubGoal4Label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMinSize = minFontSize;
			picSubGoal4Label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
			
			if((int)internalState < i)
			{
				showVideo = true;
				
#if !UNITY_ANDROID && !UNITY_IPHONE
				// Play this pages video.
				if(orderClip != null && !orderClip.isPlaying)
				{
					movieTimer = 0;
					
					if(dreamClip != null && dreamClip.isPlaying)
					{
						dreamClip.Stop ();
					}
					if(drawClip != null && drawClip.isPlaying)
					{
						drawClip.Stop ();
					}
					
					GetComponent<GUITexture>().texture = orderClip;
					GetComponent<AudioSource>().clip = orderClip.audioClip;
					
					playing = false;
					ControlPlay();
				}
				else
				{
					showVideo = false;
				}
#else
				currentVidUrl = orderUrl;
//				if(!debugSkipVideos)
//				{
//					StartCoroutine (PlayHandheldMovie(orderUrl));
//				}
#endif
			}
			
			break;
			
		case (int)InternalState.page4intro:
			if((int)internalState < i)
			{
				showVideo = true;
				
#if !UNITY_ANDROID && !UNITY_IPHONE
				// Play this pages video.
				if(drawClip != null && !drawClip.isPlaying)
				{
					movieTimer = 0;
					
					if(orderClip != null && orderClip.isPlaying)
					{
						orderClip.Stop ();
					}
					if(whyClip != null && whyClip.isPlaying)
					{
						whyClip.Stop ();
					}
					
					GetComponent<GUITexture>().texture = drawClip;
					GetComponent<AudioSource>().clip = drawClip.audioClip;
					
					playing = false;
					ControlPlay();
				}
				else
				{
					showVideo = false;
				}
#else
				currentVidUrl = drawUrl;
//				if(!debugSkipVideos)
//				{
//					StartCoroutine (PlayHandheldMovie(drawUrl));
//				}
#endif
			}
			
			break;
			
		case (int)InternalState.page4display:	
			
			if( goalMap.goals [0].image != null )
			{
				picMainGoalLabel.GetComponent<Image> ().enabled = true;
				picMainGoalLabel.GetComponent<Image> ().sprite = Sprite.Create (goalMap.goals [0].image, new Rect (0, 0, goalMap.goals [0].image.width, goalMap.goals [0].image.height), new Vector2 (0.5f, 0.5f));
				
				if(goalMap.goals [0].image != mapBackInner2.texture)
				{
					picMainGoalLabel.transform.Find ("ActionButtonText").GetComponent<Text> ().enabled = false;
				}
			}
			
			if( goalMap.goals [1].image != null )
			{
				picSubGoal1Label.GetComponent<Image> ().enabled = true;
				picSubGoal1Label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.goals [1].image, new Rect (0, 0, goalMap.goals [1].image.width, goalMap.goals [1].image.height), new Vector2 (0.5f, 0.5f));
				
				if(goalMap.goals [1].image != mapBackInner1.texture)
				{
					picSubGoal1Label.transform.Find ("ActionButtonText").GetComponent<Text> ().enabled = false;
				}
			}
			
			if( goalMap.goals [2].image != null )
			{
				picSubGoal2Label.GetComponent<Image> ().enabled = true;
				picSubGoal2Label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.goals [2].image, new Rect (0, 0, goalMap.goals [2].image.width, goalMap.goals [2].image.height), new Vector2 (0.5f, 0.5f));
				
				if(goalMap.goals [2].image != mapBackInner1.texture)
				{
					picSubGoal2Label.transform.Find ("ActionButtonText").GetComponent<Text> ().enabled = false;
				}
			}
			
			if( goalMap.goals [3].image != null )
			{
				picSubGoal3Label.GetComponent<Image> ().enabled = true;
				picSubGoal3Label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.goals [3].image, new Rect (0, 0, goalMap.goals [3].image.width, goalMap.goals [3].image.height), new Vector2 (0.5f, 0.5f));
				
				if(goalMap.goals [3].image != mapBackInner1.texture)
				{
					picSubGoal3Label.transform.Find ("ActionButtonText").GetComponent<Text> ().enabled = false;
				}
			}
			
			if( goalMap.goals [4].image != null )
			{
				picSubGoal4Label.GetComponent<Image> ().enabled = true;
				picSubGoal4Label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.goals [4].image, new Rect (0, 0, goalMap.goals [4].image.width, goalMap.goals [4].image.height), new Vector2 (0.5f, 0.5f));
				
				if(goalMap.goals [4].image != mapBackInner1.texture)
				{
					picSubGoal4Label.transform.Find ("ActionButtonText").GetComponent<Text> ().enabled = false;
				}
			}
			
			break;
			
		case (int)InternalState.page5display:
			if((int)internalState < i)
			{
				showVideo = true;
				
#if !UNITY_ANDROID && !UNITY_IPHONE
				// Play this pages video.
				if(whyClip != null && !whyClip.isPlaying)
				{
					movieTimer = 0;
					
					if(drawClip != null && drawClip.isPlaying)
					{
						drawClip.Stop ();
					}
					if(whenClip != null && whenClip.isPlaying)
					{
						whenClip.Stop ();
					}
					
					GetComponent<GUITexture>().texture = whyClip;
					GetComponent<AudioSource>().clip = whyClip.audioClip;
					
					playing = false;
					ControlPlay();
				}
				else
				{
					showVideo = false;
				}
#else
				currentVidUrl = whyUrl;
//				if(!debugSkipVideos)
//				{
//					StartCoroutine (PlayHandheldMovie(whyUrl));
//				}
#endif
			}
			
			break;
			
		case (int)InternalState.page6draw:
			
			if( goalMap.whys [0].image != null )
			{
				drawWhy1label.GetComponent<Image>().enabled = true;
				drawWhy1label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.whys [0].image, new Rect (0, 0, goalMap.whys[0].image.width, goalMap.whys[0].image.height), new Vector2 (0.5f, 0.5f));
				
				if(goalMap.whys [0].image != mapBackInner3.texture)
				{
					drawWhy1label.transform.Find("ActionButtonText").GetComponent<Text> ().enabled = false;
				}
			}
			
			if( goalMap.whys [1].image != null )
			{
				drawWhy2label.GetComponent<Image>().enabled = true;
				drawWhy2label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.whys [1].image, new Rect (0, 0, goalMap.whys[1].image.width, goalMap.whys[1].image.height), new Vector2 (0.5f, 0.5f));
				
				if(goalMap.whys [1].image != mapBackInner3.texture)
				{
					drawWhy2label.transform.Find("ActionButtonText").GetComponent<Text> ().enabled = false;
				}
			}
			
			if( goalMap.whys [2].image != null )
			{
				drawWhy3label.GetComponent<Image>().enabled = true;
				drawWhy3label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.whys [2].image, new Rect (0, 0, goalMap.whys[2].image.width, goalMap.whys[2].image.height), new Vector2 (0.5f, 0.5f));
				
				if(goalMap.whys [2].image != mapBackInner3.texture)
				{
					drawWhy3label.transform.Find("ActionButtonText").GetComponent<Text> ().enabled = false;
				}
			}
			
			break;
			
		case (int)InternalState.page7when:
			if((int)internalState < i)
			{
				showVideo = true;
				
#if !UNITY_ANDROID && !UNITY_IPHONE
				// Play this pages video.
				if(whenClip != null && !whenClip.isPlaying)
				{
					movieTimer = 0;
					
					if(whyClip != null && whyClip.isPlaying)
					{
						whyClip.Stop ();
					}
					if(howClip != null && howClip.isPlaying)
					{
						howClip.Stop ();
					}
					
					GetComponent<GUITexture>().texture = whenClip;
					GetComponent<AudioSource>().clip = whenClip.audioClip;
					
					playing = false;
					ControlPlay();
				}
				else
				{
					showVideo = false;
				}
#else
				currentVidUrl = whenUrl;
//				if(!debugSkipVideos)
//				{
//					StartCoroutine (PlayHandheldMovie(whenUrl));
//				}
#endif
			}
			
			break;
		
		case (int)InternalState.page8draw:
			if((int)internalState < i)
			{
				showVideo = true;
				
#if !UNITY_ANDROID && !UNITY_IPHONE
				// Play this pages video.
				if(howClip != null && !howClip.isPlaying)
				{
					movieTimer = 0;
					
					if(whenClip != null && whenClip.isPlaying)
					{
						whenClip.Stop ();
					}
					if(whoClip != null && whoClip.isPlaying)
					{
						whoClip.Stop ();
					}
					
					GetComponent<GUITexture>().texture = howClip;
					GetComponent<AudioSource>().clip = howClip.audioClip;
					
					playing = false;
					ControlPlay();
				}
				else
				{
					showVideo = false;
				}
#else
				currentVidUrl = howUrl;
//				if(!debugSkipVideos)
//				{
//					StartCoroutine (PlayHandheldMovie(howUrl));
//				}
#endif
			}
			
			break;
			
		case (int)InternalState.page9draw:		
			
			if( goalMap.hows [0].image != null )
			{
				drawHow1label.GetComponent<Image>().enabled = true;
				drawHow1label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.hows [0].image, new Rect (0, 0, goalMap.hows[0].image.width, goalMap.hows[0].image.height), new Vector2 (0.5f, 0.5f));
				
				if(goalMap.hows [0].image != mapBackInner4.texture)
				{
					drawHow1label.transform.Find("ActionButtonText").GetComponent<Text> ().enabled = false;
				}
			}
			
			if( goalMap.hows [1].image != null )
			{
				drawHow2label.GetComponent<Image>().enabled = true;
				drawHow2label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.hows [1].image, new Rect (0, 0, goalMap.hows[1].image.width, goalMap.hows[1].image.height), new Vector2 (0.5f, 0.5f));
				
				if(goalMap.hows [1].image != mapBackInner4.texture)
				{
					drawHow2label.transform.Find("ActionButtonText").GetComponent<Text> ().enabled = false;
				}
			}
			
			if( goalMap.hows [2].image != null )
			{
				drawHow3label.GetComponent<Image>().enabled = true;
				drawHow3label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.hows [2].image, new Rect (0, 0, goalMap.hows[2].image.width, goalMap.hows[2].image.height), new Vector2 (0.5f, 0.5f));
				
				if(goalMap.hows [2].image != mapBackInner4.texture)
				{
					drawHow3label.transform.Find("ActionButtonText").GetComponent<Text> ().enabled = false;
				}
			}
			break;
			
		case (int)InternalState.page10draw:
			if((int)internalState < i)
			{
				showVideo = true;
				
#if !UNITY_ANDROID && !UNITY_IPHONE
				// Play this pages video.
				if(whoClip != null && !whoClip.isPlaying)
				{
					movieTimer = 0;
					
					if(howClip != null && howClip.isPlaying)
					{
						howClip.Stop ();
					}
					if(signClip != null && signClip.isPlaying)
					{
						signClip.Stop ();
					}
					
					GetComponent<GUITexture>().texture = whoClip;
					GetComponent<AudioSource>().clip = whoClip.audioClip;
					
					playing = false;
					ControlPlay();
				}
				else
				{
					showVideo = false;
				}
#else
				currentVidUrl = whoUrl;
//				if(!debugSkipVideos)
//				{
//					StartCoroutine (PlayHandheldMovie(whoUrl));
//				}
#endif
			}
			
			break;
			
		case (int)InternalState.page11draw:
			
			if( goalMap.whos [0].image != null )
			{
				who1label.GetComponent<Image>().enabled = true;
				who1label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.whos [0].image, new Rect (0, 0, goalMap.whos[0].image.width, goalMap.whos[0].image.height), new Vector2 (0.5f, 0.5f));
				
				if(goalMap.whos [0].image != mapBackInner5.texture)
				{
					who1label.transform.Find("ActionButtonText").GetComponent<Text> ().enabled = false;
				}
			}
			
			if( goalMap.whos [1].image != null )
			{
				who2label.GetComponent<Image>().enabled = true;
				who2label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.whos [1].image, new Rect (0, 0, goalMap.whos[1].image.width, goalMap.whos[1].image.height), new Vector2 (0.5f, 0.5f));
				
				if(goalMap.whos [1].image != mapBackInner5.texture)
				{
					who2label.transform.Find("ActionButtonText").GetComponent<Text> ().enabled = false;
				}
			}
			
			if( goalMap.whos [2].image != null )
			{
				who3label.GetComponent<Image>().enabled = true;
				who3label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.whos [2].image, new Rect (0, 0, goalMap.whos[2].image.width, goalMap.whos[2].image.height), new Vector2 (0.5f, 0.5f));
				
				if(goalMap.whos [2].image != mapBackInner5.texture)
				{
					who3label.transform.Find("ActionButtonText").GetComponent<Text> ().enabled = false;
				}
			}
			break;
			
		case (int)InternalState.page12draw:
			if((int)internalState < i)
			{
				showVideo = true;
				
#if !UNITY_ANDROID && !UNITY_IPHONE
				// Play this pages video.
				if(signClip != null && !signClip.isPlaying)
				{
					movieTimer = 0;
					
					if(whoClip != null && whoClip.isPlaying)
					{
						whoClip.Stop ();
					}
					
					GetComponent<GUITexture>().texture = signClip;
					GetComponent<AudioSource>().clip = signClip.audioClip;
					
					playing = false;
					ControlPlay();
				}
				else
				{
					showVideo = false;
				}
#else
				currentVidUrl = signUrl;
//				if(!debugSkipVideos)
//				{
//					StartCoroutine (PlayHandheldMovie(signUrl));
//				}
#endif
			}
			
			// Reset the goal map switch left/right button.
			changeTextPicGoalMapButton.GetComponent<Image>().sprite = changeTextPicSprite[0];
			
			// Make hover sprite
			UnityEngine.UI.Navigation nav = new UnityEngine.UI.Navigation();
			nav.mode = UnityEngine.UI.Navigation.Mode.None;
			
			changeTextPicGoalMapButton.GetComponent<Button>().navigation = nav;
			changeTextPicGoalMapButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
			
			SpriteState spriteState = new SpriteState();
			changeTextPicGoalMapButton.GetComponent<Button>().targetGraphic = changeTextPicGoalMapButton.GetComponentInChildren<Image>();
			spriteState.highlightedSprite = changeTextPicSprite[1];
			spriteState.pressedSprite = changeTextPicSprite[1];
			changeTextPicGoalMapButton.GetComponent<Button>().spriteState = spriteState;
			
//			StartCoroutine (SaveGoalMap ());
			
			break;
			
		case (int)InternalState.showGoalMap:
			ZoomGoalMap (Vector2.zero, false, false, false, new Rect(0, 0, 0, 0));
			// Show the save button if user comes off the last screen.
			saveGoalMapButton.SetActive (true);
			closeButtonBackground.SetActive (true);
			break;

		case (int)InternalState.goalMapComplete:
			// Save the goal map
			saveInProgress = true;
			StartCoroutine(SaveGoalMap());
			// Hide the save button on the last screen.
			saveGoalMapButton.SetActive (false);
			closeButtonBackground.SetActive (false);
			break;
		}
		
		// Change the state.
		ChangeState (i);

		return true;
	}
	
	/// <summary>
	/// Saves the goal map.
	/// </summary>
	/// <returns>The goal map.</returns>
	private IEnumerator SaveGoalMap()
	{
		//MCP.isTransmitting = true;
		if(!savingPopupCreated)
		{
			//// Make background shading
			//MakeImage (localCanvas, new Rect(Screen.width * -0.5f, Screen.height * -0.5f, Screen.width, Screen.height), menuBackground);
			//
			//// Make saving panel
			//MakeImage (localCanvas, new Rect(Screen.width * -0.25f, Screen.height * -0.1f, Screen.width * 0.5f, Screen.height * 0.45f), window_back, true);
			//
			//// Make saving text
			//MakeLabel (localCanvas, MCP.Text (2835)/*"Saving..."*/, new Rect(Screen.width * -0.15f, Screen.height * -0.125f, Screen.width * 0.3f, Screen.height * 0.2f), TextAnchor.MiddleCenter, "InnerLabel");
			//
			//// Make saving spinner
			//savingSpinner = MakeImage (localCanvas, new Rect(0, 0, Screen.width * 0.08f, Screen.width * 0.08f), spinnerSprite, false);
			//savingSpinner.name = "SavingSpinner";
			//savingSpinner.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
			
			spinnerRot = 0;
			savingPopupCreated = true;
		}
		
		yield return null;
		
		// Store the pre-processed goal map
		Texture2D[] tempGoalImages = new Texture2D[goalMap.goals.Count];
		for(int i = 0; i < goalMap.goals.Count; i++)
		{
			tempGoalImages[i] = goalMap.goals[i].image;
		}
		
		Texture2D[] tempWhyImages = new Texture2D[goalMap.whys.Count];
		for(int i = 0; i < goalMap.whys.Count; i++)
		{
			tempWhyImages[i] = goalMap.whys[i].image;
		}
		
		Texture2D[] tempHowImages = new Texture2D[goalMap.hows.Count];
		for(int i = 0; i < goalMap.hows.Count; i++)
		{
			tempHowImages[i] = goalMap.hows[i].image;
		}
		
		Texture2D[] tempWhoImages = new Texture2D[goalMap.whos.Count];
		for(int i = 0; i < goalMap.whos.Count; i++)
		{
			tempWhoImages[i] = goalMap.whos[i].image;
		}
		
		// Turn the goal map into a json.
		goalMap.name = saveGMFileName.GetComponent<Text>().text;
		
		// Clear textures and huge byte arrays to stop them causing problems on the server.
		if(goalMap.goals != null)
		{
			for(int i = 0; i < goalMap.goals.Count; i++)
			{
				goalMap.goals[i].image = null;
				goalMap.goals[i].imageBytes = null;
			}
		}
		
		if(goalMap.whys != null)
		{
			for(int i = 0; i < goalMap.whys.Count; i++)
			{
				goalMap.whys[i].image = null;
				goalMap.whys[i].imageBytes = null;
			}
		}
		
		if(goalMap.whos != null)
		{
			for(int i = 0; i < goalMap.whos.Count; i++)
			{
				goalMap.whos[i].image = null;
				goalMap.whos[i].imageBytes = null;
			}
		}
		
		if(goalMap.hows != null)
		{
			for(int i = 0; i < goalMap.hows.Count; i++)
			{
				goalMap.hows[i].image = null;
				goalMap.hows[i].imageBytes = null;
			}
		}
		
		// Post the goal map.
		if( MCP.gmIndex == -1 || dontResetGMIndex )
		{
			if(MCP.userInfo != null)
			{
				goalMap.reference = MCP.userInfo.username + DateTime.UtcNow.Ticks.ToString ();
			}
			else
			{
				goalMap.reference = "Test" + DateTime.UtcNow.Ticks.ToString ();
			}
			
			// This allows the overwriting of the newly created GM
			MCP.gmIndex = 0;

            if (MCP.userInfo != null)
            {
                StartCoroutine(MCP.PostGoalMap(goalMap));
            }

			// Add to the user's total goal map number.
			if(MCP.userInfo != null && MCP.userInfo.memberProfile != null)
			{
				int gmNum = int.Parse (MCP.userInfo.memberProfile.numberOfGoalMaps);
				gmNum++;
				MCP.userInfo.memberProfile.numberOfGoalMaps = gmNum.ToString ();
				
				// Update the member profile.
				StartCoroutine (MCP.UpdateMemberProfile ());
			}
		}
		else
		{
            if (MCP.userInfo != null)
            {
                StartCoroutine(MCP.UpdateGoalMap(goalMap));
            }
		}
		
		// Re-add the images to the goal map
		for(int i = 0; i < goalMap.goals.Count; i++)
		{
			goalMap.goals[i].image = tempGoalImages[i];
		}
		
		for(int i = 0; i < goalMap.whys.Count; i++)
		{
			goalMap.whys[i].image = tempWhyImages[i];
		}
		
		for(int i = 0; i < goalMap.hows.Count; i++)
		{
			goalMap.hows[i].image = tempHowImages[i];
		}
		
		for(int i = 0; i < goalMap.whos.Count; i++)
		{
			goalMap.whos[i].image = tempWhoImages[i];
		}
		
		// Flag as save complete.
		saveComplete = true;
        //MCP.isTransmitting = false;
    }
	
	/// <summary>
	/// Reorders the goal up one.
	/// </summary>
	/// <returns><c>true</c>, if reorder was done, <c>false</c> otherwise.</returns>
	/// <param name="i">The index.</param>
	bool DoReorderGoalUpButton (int i)
	{
		// Swap the goal text.
		String tmpString = goalMap.goals [i - 1].goal;
		goalMap.goals [i - 1].goal = goalMap.goals [i].goal;
		goalMap.goals [i].goal = tmpString;		

		// For each goal...
		for (int x = 0; x < numberOfGoals; x++)
		{
			// Change the goal objects text.
			page1goals [x].GetComponent<InputField> ().text = goalMap.goals [x].goal;
			page2goals [x].GetComponent<Text> ().text = goalMap.goals[x].goal;
			page3goals [x].GetComponent<Text> ().text = goalMap.goals[x].goal;
			page4goals [x].transform.GetChild (0).gameObject.GetComponent<Text> ().text = goalMap.goals[x].goal;
		}
		
		// Swap the goal text.
		Texture2D tmpTex = goalMap.goals [i - 1].image;
		goalMap.goals [i - 1].image = goalMap.goals [i].image;
		goalMap.goals [i].image = tmpTex;		
		
		if(goalMap.goals[1].image == mapBackInner2.texture)
		{
			goalMap.goals[1].image = mapBackInner1.texture;
		}
		
		// For each goal...
		for (int x = 0; x < numberOfGoals; x++)
		{
			if(goalMap.goals[x].image != null)
			{
				// Change the next page goal object text.
				page4goals [x].GetComponent<Image> ().sprite = Sprite.Create (goalMap.goals[x].image, new Rect(0, 0, goalMap.goals[x].image.width, goalMap.goals[x].image.height), new Vector2(0.5f, 0.5f));
				
				if(x == 0)
				{
					if(goalMap.goals[x].image == mapBackInner2.texture)
					{
						page4goals [x].transform.GetChild (0).gameObject.GetComponent<Text>().enabled = true;
					}
				}
				else
				{
					if(goalMap.goals[x].image == mapBackInner1.texture)
					{
						page4goals [x].transform.GetChild (0).gameObject.GetComponent<Text>().enabled = true;
					}
				}
			}
		}

		return true;
	}
	
	/// <summary>
	/// Reorder the goal down one.
	/// </summary>
	/// <returns><c>true</c>, if reorder was done, <c>false</c> otherwise.</returns>
	/// <param name="i">The index.</param>
	bool DoReorderGoalDownButton (int i)
	{
		// Swap the goaltext
		String tmpString = goalMap.goals [i + 1].goal;
		goalMap.goals [i + 1].goal = goalMap.goals [i].goal;
		goalMap.goals [i].goal = tmpString;		

		// For each goal...
		for (int x = 0; x < numberOfGoals; x++)
		{
			// Change the next page goal object text.
			page1goals [x].GetComponent<InputField> ().text = goalMap.goals [x].goal;
			page2goals [x].GetComponent<Text> ().text = goalMap.goals[x].goal;
			page3goals [x].GetComponent<Text> ().text = goalMap.goals[x].goal;
			page4goals [x].transform.GetChild (0).gameObject.GetComponent<Text> ().text = goalMap.goals[x].goal;
		}
		
		// Assign the hows in the goal map.
		for(int j = 0; j < numberOfGoals; j++)
		{
			goalMap.goals[j].image = page4goals [j].GetComponent<Image> ().sprite.texture;
			
			if(goalMap.goals[j].image == buttonBackground.texture)
			{
				if(j == 0)
				{
					goalMap.goals[j].image = mapBackInner2.texture;
				}
				else
				{
					goalMap.goals[j].image = mapBackInner1.texture;
				}
			}
		}
		
		// Swap the goal text.
		Texture2D tmpTex = goalMap.goals [i + 1].image;
		goalMap.goals [i + 1].image = goalMap.goals [i].image;
		goalMap.goals [i].image = tmpTex;		
		
		if(goalMap.goals[0].image == mapBackInner1.texture)
		{
			goalMap.goals[0].image = mapBackInner2.texture;
		}
		
		// For each goal...
		for (int x = 0; x < numberOfGoals; x++)
		{
			if(goalMap.goals[x].image != null)
			{
				// Change the next page goal object text.
				page4goals [x].GetComponent<Image> ().sprite = Sprite.Create (goalMap.goals[x].image, new Rect(0, 0, goalMap.goals[x].image.width, goalMap.goals[x].image.height), new Vector2(0.5f, 0.5f));
				
				if(x == 0)
				{
					if(goalMap.goals[x].image == mapBackInner2.texture)
					{
						page4goals [x].transform.GetChild (0).gameObject.GetComponent<Text>().enabled = true;
					}
				}
				else
				{
					if(goalMap.goals[x].image == mapBackInner1.texture)
					{
						page4goals [x].transform.GetChild (0).gameObject.GetComponent<Text>().enabled = true;
					}
				}
			}
		}

		return true;
	}
	
	/// <summary>
	/// Reorders the how up one.
	/// </summary>
	/// <returns><c>true</c>, if reorder was done, <c>false</c> otherwise.</returns>
	/// <param name="i">The index.</param>
	bool DoReorderHowUpButton (int i)
	{
		// Assign the hows in the goal map.
		for(int j = 0; j < numberOfHows; j++)
		{
			goalMap.hows[j].how = page1Hows [j].GetComponent<Text> ().text;
		}
		
		// Swap the goal text.
		String tmpString = goalMap.hows [i - 1].how;
		goalMap.hows [i - 1].how = goalMap.hows [i].how;
		goalMap.hows [i].how = tmpString;		
		
		// For each goal...
		for (int x = 0; x < numberOfHows; x++)
		{
			// Change the next page goal object text.
			page1Hows [x].GetComponent<Text> ().text = goalMap.hows[x].how;
			page1Hows [x].GetComponent<InputField> ().text = goalMap.hows[x].how;
		}
		
		// Assign the hows in the goal map.
		for(int j = 0; j < numberOfHows; j++)
		{
			goalMap.hows[j].image = page2Hows [j].GetComponent<Image> ().sprite.texture;
			
			if(goalMap.hows[j].image == buttonBackground.texture)
			{
				goalMap.hows[j].image = mapBackInner4.texture;
			}
		}
		
		// Swap the goal text.
		Texture2D tmpTex = goalMap.hows [i - 1].image;
		goalMap.hows [i - 1].image = goalMap.hows [i].image;
		goalMap.hows [i].image = tmpTex;		
		
		// For each goal...
		for (int x = 0; x < numberOfHows; x++)
		{
			// Change the next page goal object text.
			page2Hows [x].GetComponent<Image> ().sprite = Sprite.Create (goalMap.hows[x].image, new Rect(0, 0, goalMap.hows[x].image.width, goalMap.hows[x].image.height), new Vector2(0.5f, 0.5f));
			
			if(goalMap.hows[x].image == mapBackInner4.texture)
			{
				page2Hows [x].transform.GetChild (0).gameObject.GetComponent<Text>().enabled = true;
			}
		}
		
		return true;
	}
	
	/// <summary>
	/// Reorder the how down one.
	/// </summary>
	/// <returns><c>true</c>, if reorder was done, <c>false</c> otherwise.</returns>
	/// <param name="i">The index.</param>
	bool DoReorderHowDownButton(int i)
	{
		// Assign the hows in the goal map.
		for(int j = 0; j < numberOfHows; j++)
		{
			goalMap.hows[j].how = page1Hows [j].GetComponent<Text> ().text;
		}
		
		// Swap the how text
		String tmpString = goalMap.hows [i + 1].how;
		goalMap.hows [i + 1].how = goalMap.hows [i].how;
		goalMap.hows [i].how = tmpString;		
		
		// For each goal...
		for (int x = 0; x < numberOfHows; x++)
		{
			// Change the next page how object text.
			page1Hows [x].GetComponent<Text> ().text = goalMap.hows[x].how;
			page1Hows [x].GetComponent<InputField> ().text = goalMap.hows[x].how;
		}
		
		// Assign the hows in the goal map.
		for(int j = 0; j < numberOfHows; j++)
		{
			goalMap.hows[j].image = page2Hows [j].GetComponent<Image> ().sprite.texture;
			
			if(goalMap.hows[j].image == buttonBackground.texture)
			{
				goalMap.hows[j].image = mapBackInner4.texture;
			}
		}
		
		// Swap the goal text.
		Texture2D tmpTex = goalMap.hows [i + 1].image;
		goalMap.hows [i + 1].image = goalMap.hows [i].image;
		goalMap.hows [i].image = tmpTex;		
		
		// For each goal...
		for (int x = 0; x < numberOfHows; x++)
		{
			// Change the next page goal object text.
			page2Hows [x].GetComponent<Image> ().sprite = Sprite.Create (goalMap.hows[x].image, new Rect(0, 0, goalMap.hows[x].image.width, goalMap.hows[x].image.height), new Vector2(0.5f, 0.5f));
			
			if(goalMap.hows[x].image == mapBackInner4.texture)
			{
				page2Hows [x].transform.GetChild (0).gameObject.GetComponent<Text>().enabled = true;
			}
		}
		
		return true;
	}
	
	/// <summary>
	/// Reorder the how down one.
	/// </summary>
	/// <returns><c>true</c>, if reorder was done, <c>false</c> otherwise.</returns>
	/// <param name="i">The index.</param>
	bool DoReorderWhyDownButton(int i)
	{
		// Assign the hows in the goal map.
		for(int j = 0; j < numberOfHows; j++)
		{
			goalMap.whys[j].why = page1Whys [j].GetComponent<Text> ().text;
		}
		
		// Swap the how text
		String tmpString = goalMap.whys [i + 1].why;
		goalMap.whys [i + 1].why = goalMap.whys [i].why;
		goalMap.whys [i].why = tmpString;		
		
		// For each goal...
		for (int x = 0; x < numberOfWhys; x++)
		{
			// Change the next page how object text.
			page1Whys [x].GetComponent<Text> ().text = goalMap.whys[x].why;
			page1Whys [x].GetComponent<InputField> ().text = goalMap.whys[x].why;
		}
		
		// Assign the hows in the goal map.
		for(int j = 0; j < numberOfWhys; j++)
		{
			goalMap.whys[j].image = page2Whys [j].GetComponent<Image> ().sprite.texture;
			
			if(goalMap.whys[j].image == buttonBackground.texture)
			{
				goalMap.whys[j].image = mapBackInner3.texture;
			}
		}
		
		// Swap the goal text.
		Texture2D tmpTex = goalMap.whys [i + 1].image;
		goalMap.whys [i + 1].image = goalMap.whys [i].image;
		goalMap.whys [i].image = tmpTex;		
		
		// For each goal...
		for (int x = 0; x < numberOfWhys; x++)
		{
			// Change the next page goal object text.
			page2Whys [x].GetComponent<Image> ().sprite = Sprite.Create (goalMap.whys[x].image, new Rect(0, 0, goalMap.whys[x].image.width, goalMap.whys[x].image.height), new Vector2(0.5f, 0.5f));
			
			if(goalMap.whys[x].image == mapBackInner3.texture)
			{
				page2Whys [x].transform.GetChild (0).gameObject.GetComponent<Text>().enabled = true;
			}
		}
		
		return true;
	}
	
	/// <summary>
	/// Reorders the how up one.
	/// </summary>
	/// <returns><c>true</c>, if reorder was done, <c>false</c> otherwise.</returns>
	/// <param name="i">The index.</param>
	bool DoReorderWhyUpButton (int i)
	{
		// Assign the hows in the goal map.
		for(int j = 0; j < numberOfWhys; j++)
		{
			goalMap.whys[j].why = page1Whys [j].GetComponent<Text> ().text;
		}
		
		// Swap the goal text.
		String tmpString = goalMap.whys [i - 1].why;
		goalMap.whys [i - 1].why = goalMap.whys [i].why;
		goalMap.whys [i].why = tmpString;		
		
		// For each goal...
		for (int x = 0; x < numberOfWhys; x++)
		{
			// Change the next page goal object text.
			page1Whys [x].GetComponent<Text> ().text = goalMap.whys[x].why;
			page1Whys [x].GetComponent<InputField> ().text = goalMap.whys[x].why;
		}
		
		// Assign the hows in the goal map.
		for(int j = 0; j < numberOfWhys; j++)
		{
			goalMap.whys[j].image = page2Whys [j].GetComponent<Image> ().sprite.texture;
			
			if(goalMap.whys[j].image == buttonBackground.texture)
			{
				goalMap.whys[j].image = mapBackInner3.texture;
				page2Whys [j].transform.GetChild (0).gameObject.GetComponent<Text>().enabled = true;
			}
		}
		
		// Swap the goal text.
		Texture2D tmpTex = goalMap.whys [i - 1].image;
		goalMap.whys [i - 1].image = goalMap.whys [i].image;
		goalMap.whys [i].image = tmpTex;		
		
		// For each goal...
		for (int x = 0; x < numberOfWhys; x++)
		{
			// Change the next page goal object text.
			page2Whys [x].GetComponent<Image> ().sprite = Sprite.Create (goalMap.whys[x].image, new Rect(0, 0, goalMap.whys[x].image.width, goalMap.whys[x].image.height), new Vector2(0.5f, 0.5f));
			
			if(goalMap.whys[x].image == mapBackInner3.texture)
			{
				page2Whys [x].transform.GetChild (0).gameObject.GetComponent<Text>().enabled = true;
			}
		}
		
		return true;
	}
	
	/// <summary>
	/// Reorders the how up one.
	/// </summary>
	/// <returns><c>true</c>, if reorder was done, <c>false</c> otherwise.</returns>
	/// <param name="i">The index.</param>
	bool DoReorderWhoUpButton (int i)
	{
		// Assign the hows in the goal map.
		for(int j = 0; j < numberOfWhos; j++)
		{
			goalMap.whos[j].who = page1Whos [j].GetComponent<Text> ().text;
		}
		
		// Swap the goal text.
		String tmpString = goalMap.whos [i - 1].who;
		goalMap.whos [i - 1].who = goalMap.whos [i].who;
		goalMap.whos [i].who = tmpString;		
		
		// For each goal...
		for (int x = 0; x < numberOfWhos; x++)
		{
			// Change the next page goal object text.
			page1Whos [x].GetComponent<Text> ().text = goalMap.whos[x].who;
			page1Whos [x].GetComponent<InputField> ().text = goalMap.whos[x].who;
		}
		
		// Assign the hows in the goal map.
		for(int j = 0; j < numberOfWhos; j++)
		{
			goalMap.whos[j].image = page2Whos [j].GetComponent<Image> ().sprite.texture;
			
			if(goalMap.whos[j].image == buttonBackground.texture)
			{
				goalMap.whos[j].image = mapBackInner5.texture;
			}
		}
		
		// Swap the goal text.
		Texture2D tmpTex = goalMap.whos [i - 1].image;
		goalMap.whos [i - 1].image = goalMap.whos [i].image;
		goalMap.whos [i].image = tmpTex;		
		
		// For each goal...
		for (int x = 0; x < numberOfWhos; x++)
		{
			// Change the next page goal object text.
			page2Whos [x].GetComponent<Image> ().sprite = Sprite.Create (goalMap.whos[x].image, new Rect(0, 0, goalMap.whos[x].image.width, goalMap.whos[x].image.height), new Vector2(0.5f, 0.5f));
			
			if(goalMap.whos[x].image == mapBackInner5.texture)
			{
				page2Whos [x].transform.GetChild (0).gameObject.GetComponent<Text>().enabled = true;
			}
		}
		
		return true;
	}
	
	/// <summary>
	/// Reorder the how down one.
	/// </summary>
	/// <returns><c>true</c>, if reorder was done, <c>false</c> otherwise.</returns>
	/// <param name="i">The index.</param>
	bool DoReorderWhoDownButton(int i)
	{
		// Assign the hows in the goal map.
		for(int j = 0; j < numberOfWhos; j++)
		{
			goalMap.whos[j].who = page1Whos [j].GetComponent<Text> ().text;
		}
		
		// Swap the how text
		String tmpString = goalMap.whos [i + 1].who;
		goalMap.whos [i + 1].who = goalMap.whos [i].who;
		goalMap.whos [i].who = tmpString;		
		
		// For each goal...
		for (int x = 0; x < numberOfWhos; x++)
		{
			// Change the next page how object text.
			page1Whos [x].GetComponent<Text> ().text = goalMap.whos[x].who;
			page1Whos [x].GetComponent<InputField> ().text = goalMap.whos[x].who;
		}
		
		// Assign the hows in the goal map.
		for(int j = 0; j < numberOfWhos; j++)
		{
			goalMap.whos[j].image = page2Whos [j].GetComponent<Image> ().sprite.texture;
			
			if(goalMap.whos[j].image == buttonBackground.texture)
			{
				goalMap.whos[j].image = mapBackInner5.texture;
			}
		}
		
		// Swap the goal text.
		Texture2D tmpTex = goalMap.whos [i + 1].image;
		goalMap.whos [i + 1].image = goalMap.whos [i].image;
		goalMap.whos [i].image = tmpTex;		
		
		// For each goal...
		for (int x = 0; x < numberOfWhos; x++)
		{
			// Change the next page goal object text.
			page2Whos [x].GetComponent<Image> ().sprite = Sprite.Create (goalMap.whos[x].image, new Rect(0, 0, goalMap.whos[x].image.width, goalMap.whos[x].image.height), new Vector2(0.5f, 0.5f));
			
			if(goalMap.whos[x].image == mapBackInner5.texture)
			{
				page2Whos [x].transform.GetChild (0).gameObject.GetComponent<Text>().enabled = true;
			}
		}
		
		return true;
	}

	/// <summary>
	/// Edit the picture.
	/// </summary>
	/// <returns><c>true</c>, if picture is being edited, <c>false</c> otherwise.</returns>
	/// <param name="i">The index.</param>
	bool PictureEdit (int i)
	{
		// Store the image to edit.
		selectedImageToEdit = i;
		
		// Reset the first frame timer.
		MenuScreen.firstFrameTimer = 0;
		// Add the select image function script to the camera.
		if(GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<SelectImageFunction> () == null)
		{
			GameObject.FindGameObjectWithTag ("MainCamera").AddComponent<SelectImageFunction> ().editCanvas = editCanvas;
		}
		
		switch (internalState)
		{
		case InternalState.page4display:
			// Set the image of the goal to the saved texture.
			if(goalMap.goals != null && goalMap.goals[selectedImageToEdit] != null && goalMap.goals [selectedImageToEdit].image != null && goalMap.goals [selectedImageToEdit].image.width > 8)
			{
				MCP.loadedTexture = goalMap.goals [selectedImageToEdit].image;
			}
			break;
			
		case InternalState.page6draw:
			// Set the image of the why to the saved texture.
			if(goalMap.whys != null && goalMap.whys[selectedImageToEdit] != null && goalMap.whys [selectedImageToEdit].image != null && goalMap.whys [selectedImageToEdit].image.width > 8)
			{
				MCP.loadedTexture = goalMap.whys [selectedImageToEdit].image;
			}
			break;
			
		case InternalState.page9draw:
			// Set the image of the how to the saved texture.
			if(goalMap.hows != null && goalMap.hows[selectedImageToEdit] != null && goalMap.hows [selectedImageToEdit].image != null && goalMap.hows [selectedImageToEdit].image.width > 8)
			{
				MCP.loadedTexture = goalMap.hows [selectedImageToEdit].image;
			}
			break;
			
		case InternalState.page11draw:
			// Set the image of the who to the saved texture.
			if(goalMap.whos != null && goalMap.whos[selectedImageToEdit] != null && goalMap.whos [selectedImageToEdit].image != null && goalMap.whos [selectedImageToEdit].image.width > 8)
			{
				MCP.loadedTexture = goalMap.whos [selectedImageToEdit].image;
			}
			break;
		}

		return true;
	}
	
	/// <summary>
	/// Process after the painter is closed.
	/// </summary>
	public void PainterClosed()
	{
		// Signal to not check if user wants to edit
		wantsEditting = false;
		// Handle the file browser closing.
		FileBrowserClosed ();
	}
	
	/// <summary>
	/// Handle the file browser closing.
	/// </summary>
	public void FileBrowserClosed ()
	{
		// Reset the first frame timer.
		MenuScreen.firstFrameTimer = 0;
		// Get a reference to the canvas camera.
		GameObject canvasCamObj = GameObject.Find ("CanvasCamera");
		
		// If the canvas camera is still active, return.
		if(canvasCamObj != null && canvasCamObj.GetComponent<Camera>() != null && canvasCamObj.GetComponent<Camera>().enabled)
		{
			return;
		}
		
		// If the user doesn't want to edit...
		if(!wantsEditting)
		{
			// If the current texture isn't null...
			if (MCP.loadedTexture != null)
			{
				// Get a unique reference name for the image.
				string imageRef = "";
				
				if(MCP.userInfo != null)
				{
					imageRef = MCP.userInfo.username + DateTime.UtcNow.Ticks.ToString ();
				}
				
				switch (internalState)
				{
				case InternalState.page4display:
					// Set the image of the goal to the saved texture.
					switch (selectedImageToEdit)
					{
					case 0:
						goalMap.goals [0].image = new Texture2D( MCP.loadedTexture.width, MCP.loadedTexture.height );
						goalMap.goals [0].image.SetPixels (MCP.loadedTexture.GetPixels ());
						goalMap.goals [0].image.Apply ();
						goalMap.goals [0].imageRef = imageRef;
						
						picMainGoalLabel.GetComponent<Image> ().enabled = true;
						picMainGoalLabel.GetComponent<Image> ().sprite = Sprite.Create (goalMap.goals [0].image, new Rect (0, 0, MCP.loadedTexture.width, MCP.loadedTexture.height), new Vector2 (0.5f, 0.5f));
						picMainGoalLabel.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextForBestFit = true;
						picMainGoalLabel.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextMaxSize = maxFontSize;
						picMainGoalLabel.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextMinSize = minFontSize;
						picMainGoalLabel.transform.Find ("ActionButtonText").GetComponent<Text> ().enabled = false;
						
						picMainGoalInner.SetActive (false);
						break;
						
					case 1:
						goalMap.goals [1].image = new Texture2D( MCP.loadedTexture.width, MCP.loadedTexture.height );
						goalMap.goals [1].image.SetPixels (MCP.loadedTexture.GetPixels ());
						goalMap.goals [1].image.Apply ();
						goalMap.goals [1].imageRef = imageRef;
						
						picSubGoal1Label.GetComponent<Image> ().enabled = true;
						picSubGoal1Label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.goals [1].image, new Rect (0, 0, MCP.loadedTexture.width, MCP.loadedTexture.height), new Vector2 (0.5f, 0.5f));
						picSubGoal1Label.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextForBestFit = true;
						picSubGoal1Label.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextMaxSize = maxFontSize;
						picSubGoal1Label.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextMinSize = minFontSize;
						picSubGoal1Label.transform.Find ("ActionButtonText").GetComponent<Text> ().enabled = false;
						
						picSubGoal1inner.SetActive (false);
						break;
							
					case 2:
						goalMap.goals [2].image = new Texture2D( MCP.loadedTexture.width, MCP.loadedTexture.height );
						goalMap.goals [2].image.SetPixels (MCP.loadedTexture.GetPixels ());
						goalMap.goals [2].image.Apply ();
						goalMap.goals [2].imageRef = imageRef;
						
						picSubGoal2Label.GetComponent<Image> ().enabled = true;
						picSubGoal2Label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.goals [2].image, new Rect (0, 0, MCP.loadedTexture.width, MCP.loadedTexture.height), new Vector2 (0.5f, 0.5f));
						picSubGoal2Label.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextForBestFit = true;
						picSubGoal2Label.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextMaxSize = maxFontSize;
						picSubGoal2Label.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextMinSize = minFontSize;
						picSubGoal2Label.transform.Find ("ActionButtonText").GetComponent<Text> ().enabled = false;
						
						picSubGoal2inner.SetActive (false);
						break;
							
					case 3:
						goalMap.goals [3].image = new Texture2D( MCP.loadedTexture.width, MCP.loadedTexture.height );
						goalMap.goals [3].image.SetPixels (MCP.loadedTexture.GetPixels ());
						goalMap.goals [3].image.Apply ();
						goalMap.goals [3].imageRef = imageRef;
						
						picSubGoal3Label.GetComponent<Image> ().enabled = true;
						picSubGoal3Label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.goals [3].image, new Rect (0, 0, MCP.loadedTexture.width, MCP.loadedTexture.height), new Vector2 (0.5f, 0.5f));
						picSubGoal3Label.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextForBestFit = true;
						picSubGoal3Label.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextMaxSize = maxFontSize;
						picSubGoal3Label.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextMinSize = minFontSize;
						picSubGoal3Label.transform.Find ("ActionButtonText").GetComponent<Text> ().enabled = false;	
						
						picSubGoal3inner.SetActive (false);
						break;
							
					case 4:
						goalMap.goals [4].image = new Texture2D( MCP.loadedTexture.width, MCP.loadedTexture.height );
						goalMap.goals [4].image.SetPixels (MCP.loadedTexture.GetPixels ());
						goalMap.goals [4].image.Apply ();
						goalMap.goals [4].imageRef = imageRef;
						
						picSubGoal4Label.GetComponent<Image> ().enabled = true;
						picSubGoal4Label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.goals [4].image, new Rect (0, 0, MCP.loadedTexture.width, MCP.loadedTexture.height), new Vector2 (0.5f, 0.5f));
						picSubGoal4Label.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextForBestFit = true;
						picSubGoal4Label.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextMaxSize = maxFontSize;
						picSubGoal4Label.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextMinSize = minFontSize;
						picSubGoal4Label.transform.Find ("ActionButtonText").GetComponent<Text> ().enabled = false;
						
						picSubGoal4inner.SetActive (false);
						break;
					}
					break;
					
				case InternalState.page6draw:
					// Set the image of the why to the saved texture.
					switch (selectedImageToEdit)
					{
					case 0:	
						goalMap.whys [0].image = new Texture2D( MCP.loadedTexture.width, MCP.loadedTexture.height );
						goalMap.whys [0].image.SetPixels (MCP.loadedTexture.GetPixels ());
						goalMap.whys [0].image.Apply ();
						goalMap.whys [0].imageRef = imageRef;
						
						drawWhy1label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.whys [0].image, new Rect (0, 0, MCP.loadedTexture.width, MCP.loadedTexture.height), new Vector2 (0.5f, 0.5f));
						drawWhy1label.GetComponent<Image> ().enabled = true;	
						drawWhy1label.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextForBestFit = true;
						drawWhy1label.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextMaxSize = maxFontSize;
						drawWhy1label.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextMinSize = minFontSize;
						drawWhy1label.transform.Find ("ActionButtonText").GetComponent<Text> ().enabled = false;	
						
						drawWhy1inner.SetActive (false);
						break;
							
					case 1:
						goalMap.whys [1].image = new Texture2D( MCP.loadedTexture.width, MCP.loadedTexture.height );
						goalMap.whys [1].image.SetPixels (MCP.loadedTexture.GetPixels ());
						goalMap.whys [1].image.Apply ();
						goalMap.whys [1].imageRef = imageRef;
						
						drawWhy2label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.whys [1].image, new Rect (0, 0, MCP.loadedTexture.width, MCP.loadedTexture.height), new Vector2 (0.5f, 0.5f));
						drawWhy2label.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextForBestFit = true;
						drawWhy2label.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextMaxSize = maxFontSize;
						drawWhy2label.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextMinSize = minFontSize;
						drawWhy2label.transform.Find ("ActionButtonText").GetComponent<Text> ().enabled = false;	
						drawWhy2label.GetComponent<Image> ().enabled = true;
						
						drawWhy2inner.SetActive (false);
						break;
						
					case 2:
						goalMap.whys [2].image = new Texture2D( MCP.loadedTexture.width, MCP.loadedTexture.height );
						goalMap.whys [2].image.SetPixels (MCP.loadedTexture.GetPixels ());
						goalMap.whys [2].image.Apply ();
						goalMap.whys [2].imageRef = imageRef;
						
						drawWhy3label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.whys [2].image, new Rect (0, 0, MCP.loadedTexture.width, MCP.loadedTexture.height), new Vector2 (0.5f, 0.5f));
						drawWhy3label.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextForBestFit = true;
						drawWhy3label.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextMaxSize = maxFontSize;
						drawWhy3label.transform.Find ("ActionButtonText").GetComponent<Text> ().resizeTextMinSize = minFontSize;
						drawWhy3label.transform.Find ("ActionButtonText").GetComponent<Text> ().enabled = false;	
						drawWhy3label.GetComponent<Image> ().enabled = true;
						
						drawWhy3inner.SetActive (false);
						break;
					}
					break;
	
				case InternalState.page9draw:
					// Set the image of the how to the saved texture.
					switch (selectedImageToEdit)
					{
					case 0:
						goalMap.hows [0].image = new Texture2D( MCP.loadedTexture.width, MCP.loadedTexture.height );
						goalMap.hows [0].image.SetPixels (MCP.loadedTexture.GetPixels ());
						goalMap.hows [0].image.Apply ();
						goalMap.hows [0].imageRef = imageRef;
						
						drawHow1label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.hows [0].image, new Rect (0, 0, MCP.loadedTexture.width, MCP.loadedTexture.height), new Vector2 (0.5f, 0.5f));
						drawHow1label.transform.Find ("ActionButtonText").GetComponent<Text> ().enabled = false;	
						drawHow1label.GetComponent<Image> ().enabled = true;
						
						drawHow1inner.SetActive (false);
						break;
					case 1:
						goalMap.hows [1].image = new Texture2D( MCP.loadedTexture.width, MCP.loadedTexture.height );
						goalMap.hows [1].image.SetPixels (MCP.loadedTexture.GetPixels ());
						goalMap.hows [1].image.Apply ();
						goalMap.hows [1].imageRef = imageRef;
						
						drawHow2label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.hows [1].image, new Rect (0, 0, MCP.loadedTexture.width, MCP.loadedTexture.height), new Vector2 (0.5f, 0.5f));
						drawHow2label.transform.Find ("ActionButtonText").GetComponent<Text> ().enabled = false;
						drawHow2label.GetComponent<Image> ().enabled = true;
						
						drawHow2inner.SetActive (false);
						break;
					case 2:
						goalMap.hows [2].image = new Texture2D( MCP.loadedTexture.width, MCP.loadedTexture.height );
						goalMap.hows [2].image.SetPixels (MCP.loadedTexture.GetPixels ());
						goalMap.hows [2].image.Apply ();
						goalMap.hows [2].imageRef = imageRef;
						
						drawHow3label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.hows [2].image, new Rect (0, 0, MCP.loadedTexture.width, MCP.loadedTexture.height), new Vector2 (0.5f, 0.5f));
						drawHow3label.transform.Find ("ActionButtonText").GetComponent<Text> ().enabled = false;
						drawHow3label.GetComponent<Image> ().enabled = true;
						
						drawHow3inner.SetActive (false);
						break;
					}
					break;
	
				case InternalState.page11draw:
					// Set the image of the who to the saved texture.
					switch (selectedImageToEdit)
					{
					case 0:
						goalMap.whos [0].image = new Texture2D( MCP.loadedTexture.width, MCP.loadedTexture.height );
						goalMap.whos [0].image.SetPixels (MCP.loadedTexture.GetPixels ());
						goalMap.whos [0].image.Apply ();
						goalMap.whos [0].imageRef = imageRef;
						
						who1label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.whos [0].image, new Rect (0, 0, MCP.loadedTexture.width, MCP.loadedTexture.height), new Vector2 (0.5f, 0.5f));
						who1label.transform.Find ("ActionButtonText").GetComponent<Text> ().enabled = false;
						who1label.GetComponent<Image> ().enabled = true;
						
						who1inner.SetActive (false);
						break;
					case 1:
						goalMap.whos [1].image = new Texture2D( MCP.loadedTexture.width, MCP.loadedTexture.height );
						goalMap.whos [1].image.SetPixels (MCP.loadedTexture.GetPixels ());
						goalMap.whos [1].image.Apply ();
						goalMap.whos [1].imageRef = imageRef;
						
						who2label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.whos [1].image, new Rect (0, 0, MCP.loadedTexture.width, MCP.loadedTexture.height), new Vector2 (0.5f, 0.5f));
						who2label.transform.Find ("ActionButtonText").GetComponent<Text> ().enabled = false;
						who2label.GetComponent<Image> ().enabled = true;
						
						who2inner.SetActive (false);
						break;
					case 2:
						goalMap.whos [2].image = new Texture2D( MCP.loadedTexture.width, MCP.loadedTexture.height );
						goalMap.whos [2].image.SetPixels (MCP.loadedTexture.GetPixels ());
						goalMap.whos [2].image.Apply ();
						goalMap.whos [2].imageRef = imageRef;
						
						who3label.GetComponent<Image> ().sprite = Sprite.Create (goalMap.whos [2].image, new Rect (0, 0, MCP.loadedTexture.width, MCP.loadedTexture.height), new Vector2 (0.5f, 0.5f));
						who3label.transform.Find ("ActionButtonText").GetComponent<Text> ().enabled = false;
						who3label.GetComponent<Image> ().enabled = true;
						
						who3inner.SetActive (false);
						break;
					}
					break;
				}
				
				if(MCP.userInfo != null)
				{
					// Post the image to the server.
					StartCoroutine(MCP.PostImage(MCP.userInfo.username, imageRef, MCP.loadedTexture.EncodeToPNG ()));
				}
				
				MCP.loadedTexture = null;
			}
			else 
			{
				Debug.Log ("MCP Null texture");
			}
			
			editCanvas.SetActive (false);
			localCanvas.SetActive (true);
			wantsEditting = true;
		}
		else
		{
			if(MCP.loadedTexture != null)
			{
				localCanvas.SetActive (false);
				editCanvas.SetActive (true);
			}
		}
	}
	
	/// <summary>
	/// End action button delegates.
	/// </summary>
	/// <returns><c>true</c>, if state was changed, <c>false</c> otherwise.</returns>
	/// <param name="stateIn">State in.</param>
	bool ChangeState (int stateIn)
	{
		int prevState = (int)internalState;
		InternalState enumState = (InternalState)stateIn;
		GameObject internalScene;
		internalScenes.TryGetValue ((int)internalState, out internalScene);
		internalScene.SetActive (false);
		// Exit stuff here 
		internalState = enumState;
		ControlStop();
		
		// Begin stuff
		String title;
		internalSceneTitles.TryGetValue ((int)enumState, out title);
		titleBar.GetComponent<Text> ().text = title; 
		internalScenes.TryGetValue ((int)enumState, out internalScene);
		
		if(internalScene != null)
		{
			internalScene.SetActive (true);
		}
		
		if(stateIn == (int)InternalState.page4display)
		{
			if(stateIn < prevState)
			{
				goalMap.whys[0].why = why1label.GetComponent<InputField>().text;
				goalMap.whys[1].why = why2label.GetComponent<InputField>().text;
				goalMap.whys[2].why = why3label.GetComponent<InputField>().text;
			}
		}
		
		if(stateIn == (int)InternalState.page5display)
		{
			why1label.GetComponent<InputField>().text = goalMap.whys[0].why;
			why2label.GetComponent<InputField>().text = goalMap.whys[1].why;
			why3label.GetComponent<InputField>().text = goalMap.whys[2].why;
		}
		
		// Set the why text from input.
		if(stateIn == (int)InternalState.page6draw)
		{  
			goalMap.whys[0].why = why1label.GetComponent<InputField>().text;
			goalMap.whys[1].why = why2label.GetComponent<InputField>().text;
			goalMap.whys[2].why = why3label.GetComponent<InputField>().text;
			
			// Set the next page text to the user input.
			drawWhy1label.transform.GetChild (0).gameObject.GetComponent<Text>().text = goalMap.whys[0].why;
			drawWhy1label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextForBestFit = true;
			drawWhy1label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
			drawWhy1label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMinSize = minFontSize;

			drawWhy2label.transform.GetChild (0).gameObject.GetComponent<Text>().text = goalMap.whys[1].why;
			drawWhy2label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextForBestFit = true;
			drawWhy2label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
			drawWhy2label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMinSize = minFontSize;

			drawWhy3label.transform.GetChild (0).gameObject.GetComponent<Text>().text = goalMap.whys[2].why;
			drawWhy3label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextForBestFit = true;
			drawWhy3label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
			drawWhy3label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMinSize = minFontSize;
		}
		
		// Set the when text from input.
		if(stateIn == (int)InternalState.page8draw)
		{  
			goalMap.start = start;
			goalMap.finish = finish;
		}
		
		// Set the how text from input.
		if(stateIn == (int)InternalState.page9draw)
		{  
			goalMap.hows[0].how = how1label.GetComponent<InputField>().text;
			goalMap.hows[1].how = how2label.GetComponent<InputField>().text;
			goalMap.hows[2].how = how3label.GetComponent<InputField>().text;
			
			// Set the next page text to the user input.
			drawHow1label.transform.GetChild (0).gameObject.GetComponent<Text>().text = goalMap.hows[0].how;
			drawHow1label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextForBestFit = true;
			drawHow1label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
			drawHow1label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMinSize = minFontSize;

			drawHow2label.transform.GetChild (0).gameObject.GetComponent<Text>().text = goalMap.hows[1].how;
			drawHow2label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextForBestFit = true;
			drawHow2label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
			drawHow2label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMinSize = minFontSize;

			drawHow3label.transform.GetChild (0).gameObject.GetComponent<Text>().text = goalMap.hows[2].how;
			drawHow3label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextForBestFit = true;
			drawHow3label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
			drawHow3label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMinSize = minFontSize;
		}
		
		// Set the who text from input.
		if(stateIn == (int)InternalState.page11draw)
		{  
			goalMap.whos[0].who = drawWho1label.GetComponent<InputField>().text;
			goalMap.whos[1].who = drawWho2label.GetComponent<InputField>().text;
			goalMap.whos[2].who = drawWho3label.GetComponent<InputField>().text;
			
			// Set the next page text to the user input.
			who1label.transform.GetChild (0).gameObject.GetComponent<Text>().text = goalMap.whos[0].who;
			who1label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextForBestFit = true;
			who1label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
			who1label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMinSize = minFontSize;

			who2label.transform.GetChild (0).gameObject.GetComponent<Text>().text = goalMap.whos[1].who;
			who2label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextForBestFit = true;
			who2label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
			who2label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMinSize = minFontSize;

			who3label.transform.GetChild (0).gameObject.GetComponent<Text>().text = goalMap.whos[2].who;
			who3label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextForBestFit = true;
			who3label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMaxSize = maxFontSize;
			who3label.transform.GetChild (0).gameObject.GetComponent<Text>().resizeTextMinSize = minFontSize;
		}
		
		// Populate the goal map object with the goal map.
		if(stateIn == (int)InternalState.showGoalMap)
		{
			// Enable text.
			goalMapPrefab.transform.GetChild (0).gameObject.SetActive (true);
			// Enable pictures.
			goalMapPrefab.transform.GetChild (1).gameObject.SetActive (true);
			
			if(goalMapPrefab.GetComponent<PopGM_Prefab>().picPrefab != null)
			{
				goalMapPrefab.GetComponent<PopGM_Prefab>().picPrefab.subGoal4 = null;
			}
			
			goalMapPrefab.GetComponent<PopGM_Prefab>().PopulateGoalMap(goalMap);
		}
		
		return true;
	}
	
	void ForceSkipVideo()
	{
		showVideo = false;
		ControlStop();
		
		foreach(GameObject g in videoObjects)
		{
			if(g != null)
			{
				g.SetActive (false);
			}
		}
	}
	
	void SkipVideo()
	{
		movieTimer = Mathf.Infinity;
	
#if UNITY_ANDROID || UNITY_IPHONE
		ForceSkipVideo ();
#else
		if(GetComponent<GUITexture>() != null && GetComponent<GUITexture>().texture != null && (GetComponent<GUITexture>().texture) as MovieTexture != null)
		{
			if(((GetComponent<GUITexture>().texture) as MovieTexture).duration < 0)
			{
				ForceSkipVideo ();
			}
		}
		else
		{
			ForceSkipVideo ();
		}
#endif
	}
	
	/// <summary>
	/// Control stopping the media.
	/// </summary>
	void ControlStop()
	{
		// Stop media and return to the beginning.
		GetComponent<AudioSource>().Stop();
#if !UNITY_ANDROID && !UNITY_IPHONE
		// If there is a video clip playing.
		MovieTexture videoClip = (GetComponent<GUITexture>().texture as MovieTexture);
		if(videoClip != null)
		{
			// Stop the video.
			videoClip.Stop ();
		}
#endif
		
		playing = false;
		// Change the play button sprite to play.
		playButtonObject.GetComponent<Image>().sprite = playSprite[0];
#if !UNITY_ANDROID && !UNITY_IPHONE
		// Add transparency to object to see the play button through it.
		Color c = GetComponent<GUITexture>().color;
		c.a = 64f / 255f;
		GetComponent<GUITexture>().color = c;
#endif
		movieTimer = 0;
	}
	
	/// <summary>
	/// Controls playing the media.
	/// </summary>
	void ControlPlay()
	{
#if !UNITY_ANDROID && !UNITY_IPHONE
		// If the file path store is not empty...
		// Check if the media is currently playing.
		playing = !playing;
		
		// If it is playing, pause it.
		if(playing)
		{
            try
            {
                // Play
                //GetComponent<AudioSource>().Play();
            }
            catch { }
		}
		// Otherwise, make it play.
		else
		{
            try
            {
                // Pause
                //GetComponent<AudioSource>().Pause();
            }
            catch { }
		}
		
		// Toggle transparency.
		Color c = GetComponent<GUITexture>().color;
		if(playing)
		{
			c.a = 1f;
		}
		else
		{
			c.a = 64f / 255f;
		}
		GetComponent<GUITexture>().color = c;
		
		// If the video clip is not null...
		MovieTexture videoClip = (GetComponent<GUITexture>().texture as MovieTexture);
		if(videoClip != null)
		{
			// If it is playing...
			if(playing)
			{
				// Unpause it.
				if(videoClip.isReadyToPlay && !videoClip.isPlaying)
				{
					videoClip.Play();
				}
			}
			// Otherwise...
			else
			{
				// Pause it
				if(videoClip.isPlaying)
				{
					videoClip.Pause();
				}
			}
		}
		
#else
		Debug.Log ("CurrentVid: " + currentVidUrl);
		StartCoroutine (PlayHandheldMovie(currentVidUrl));
#endif
	}
	
	/// <summary>
	/// Controls muting the audio.
	/// </summary>
	void ControlMute()
	{
#if !UNITY_ANDROID && !UNITY_IPHONE
		mute = !mute;
		GetComponent<AudioSource>().mute = mute;
		
		// If mute is now on.
		if(mute)
		{	
			// Mute sprite
			muteButtonObject.GetComponent<Image>().sprite = muteSprite;
		}
		// Otherwise...
		else
		{
			// Volume sprite
			muteButtonObject.GetComponent<Image>().sprite = unmuteSprite;
		}
#endif
	}

	/// <summary>
	/// Get the number of days the in month.
	/// </summary>
	/// <returns>The number of days in the month.</returns>
	/// <param name="month">Month.</param>
	private int DaysInMonth (int month)
	{
		// February
		if (month == 2)
		{
			return 28;
		}
		// April, June, September, November
		else if (month == 4 || month == 6 || month == 9 || month == 11)
		{
			return 30;
		}
	
		return 31;
	}
	
	/// <summary>
	/// Gets the day postfix.
	/// </summary>
	/// <returns>The day postfix.</returns>
	/// <param name="day">Day.</param>
	private string GetDayPostfix (int day)
	{
		if (day == 1)
		{
			return "st";
		}
		else if (day == 2)
		{
			return "nd";
		}
		else if (day == 3)
		{
			return "rd";
		}
		return "th";
	}
	
	private static string[] months =
		{
			"Jan",
			"Feb",
			"Mar",
			"Apr",
			"May",
			"Jun",
			"Jul",
			"Aug",
			"Sep",
			"Oct",
			"Nov",
			"Dec"
		};
	
	/// <summary>
	/// Gets the date and time.
	/// </summary>
	/// <returns>The date and time.</returns>
	/// <param name="area">Area.</param>
	/// <param name="date">Date.</param>
	private System.DateTime GetDateTime (Rect area, System.DateTime date, bool useButtons) // Draw a widget to get a datetime date
	{
		GUI.BeginGroup (area);
		
		Vector4 prevPadding = new Vector4();
		prevPadding.x = skin.GetStyle ("EmptyButton").padding.left;
		prevPadding.y = skin.GetStyle ("EmptyButton").padding.right;
		prevPadding.z = skin.GetStyle ("EmptyButton").padding.top;
		prevPadding.w = skin.GetStyle ("EmptyButton").padding.bottom;
		skin.GetStyle ("EmptyButton").padding.left = 8;
		skin.GetStyle ("EmptyButton").padding.right = 8;
		skin.GetStyle ("EmptyButton").padding.top = 8;
		skin.GetStyle ("EmptyButton").padding.bottom = 8;
	
		float ySpacing = area.height * 0.333f, xSpacing = area.width * 0.1666f;
		int day = date.Day, month = date.Month, year = date.Year, hour = date.Hour, minute = date.Minute, second = date.Second;
	
		TextAnchor ta = GUI.skin.label.alignment;
		GUI.skin.label.alignment = TextAnchor.MiddleCenter;
		
		if(useButtons)
		{
			// Draw day field
			if (DoRepeatButton (new Rect (0f, ySpacing * 2f, xSpacing, ySpacing), minusTex, "EmptyButton"))
			{
				// If in the current year and month, make sure the date does not go before today.
				if(year == DateTime.Now.Year && month == DateTime.Now.Month)
				{
					// Day minus
					if (--day < DateTime.Now.Day)
					{
						day = DaysInMonth (month);
					}
				}
				else
				{
					// Day minus
					if (--day < 1)
					{
						day = DaysInMonth (month);
					}
				}
			}
		}
	
		GUI.Label (new Rect (0f, ySpacing, xSpacing, ySpacing), day.ToString () + GetDayPostfix (day), "OrangeText"); // day label
	
		if(useButtons)
		{
			if (DoRepeatButton (new Rect (0f, 0f, xSpacing, ySpacing), plusTex, "EmptyButton"))
			{
				// If in the current year and month, make sure the date does not go before today.
				if(year == DateTime.Now.Year && month == DateTime.Now.Month)
				{
					// Day plus
					if (++day > DaysInMonth (month))
					{
						day = DateTime.Now.Day;
					}
				}
				else
				{
					// Day plus
					if (++day > DaysInMonth (month))
					{
						day = 1;
					}
				}
			}
			
			// Draw month field
			if (DoRepeatButton (new Rect (xSpacing, ySpacing * 2f, xSpacing, ySpacing), minusTex, "EmptyButton"))
			{
				// If in the current year, make sure the date does not go before today.
				if(year == DateTime.Now.Year)
				{
					// Month minus
					if (--month < DateTime.Now.Month)
					{
						month = 12;
					}
					
					// Check day
					if(month == DateTime.Now.Month)
					{
						if(day < DateTime.Now.Day)
						{
							day = DateTime.Now.Day;
						}
					}
				}
				else
				{
					// Month minus
					if (--month < 1)
					{
						month = 12;
					}
				}
					
				if (day > DaysInMonth (month))
				{
					day = DaysInMonth (month);
				}
			}
		}
	
		if (month > 0 && month < 13)
		{
			GUI.Label (new Rect (xSpacing, ySpacing, xSpacing, ySpacing), months [month - 1], "OrangeText"); // month label
		}
	
		if(useButtons)
		{
			if (DoRepeatButton (new Rect (xSpacing, 0f, xSpacing, ySpacing), plusTex, "EmptyButton"))
			{
				// If in the current year, make sure the date does not go before today.
				if(year == DateTime.Now.Year)
				{
					// Month plus
					if (++month > 12)
					{
						month = DateTime.Now.Month;
					}
					
					// Check day
					if(month == DateTime.Now.Month)
					{
						if(day < DateTime.Now.Day)
						{
							day = DateTime.Now.Day;
						}
					}
				}
				else
				{
					// Month plus
					if (++month > 12)
					{
						month = 1;
					}
				}
			
				if (day > DaysInMonth (month))
				{
					day = DaysInMonth (month);
				}
			}
		
			// Draw year field
			if (DoRepeatButton (new Rect (xSpacing * 2f, ySpacing * 2f, xSpacing, ySpacing), minusTex, "EmptyButton"))
			{
				// Year minus
				if (--year < DateTime.Now.Year)
				{
					year = DateTime.Now.Year + 100;
				}
				
				// Check month
				if(year == DateTime.Now.Year)
				{
					if (month < DateTime.Now.Month)
					{
						month = DateTime.Now.Month;
					}
					
					// Check day
					if(month == DateTime.Now.Month)
					{
						if(day < DateTime.Now.Day)
						{
							day = DateTime.Now.Day;
						}
					}
					
					if (day > DaysInMonth (month))
					{
						day = DaysInMonth (month);
					}
				}
			}
		}
	
		GUI.Label (new Rect (xSpacing * 2f, ySpacing, xSpacing, ySpacing), year.ToString ("D2"), "OrangeText"); // year label
	
		if(useButtons)
		{
			if (DoRepeatButton (new Rect (xSpacing * 2f, 0f, xSpacing, ySpacing), plusTex, "EmptyButton"))
			{
				// Year plus
				if (++year > DateTime.Now.Year + 100)
				{
					year = DateTime.Now.Year;
				}
				
				// Check month
				if(year == DateTime.Now.Year)
				{
					if (month < DateTime.Now.Month)
					{
						month = DateTime.Now.Month;
					}
					// Check day
					if(month == DateTime.Now.Month)
					{
						if(day < DateTime.Now.Day)
						{
							day = DateTime.Now.Day;
						}
					}
					
					if (day > DaysInMonth (month))
					{
						day = DaysInMonth (month);
					}
				}
			}
		}
		
		GUI.EndGroup ();
		
		skin.GetStyle ("EmptyButton").padding.left = (int)prevPadding.x;
		skin.GetStyle ("EmptyButton").padding.right = (int)prevPadding.y;
		skin.GetStyle ("EmptyButton").padding.top = (int)prevPadding.z;
		skin.GetStyle ("EmptyButton").padding.bottom = (int)prevPadding.w;
	
		GUI.skin.label.alignment = ta;
		return new System.DateTime (year, month, day, hour, minute, second);
	}
	
	void OnDestroy()
	{
		Destroy(sigBox);
		
		if( !dontResetGMIndex )
		{
			MCP.gmIndex = -1;
		}
		
		// Stop and null all movietextures.
#if !UNITY_ANDROID && !UNITY_IPHONE
		if(introClip != null)
		{
			introClip.Stop ();
			introClip = null;
		}
		if(dreamClip != null)
		{
			dreamClip.Stop ();
			dreamClip = null;
		}
		if(orderClip != null)
		{
			orderClip.Stop ();
			orderClip = null;
		}
		if(drawClip != null)
		{
			drawClip.Stop ();
			drawClip = null;
		}
		if(whyClip != null)
		{
			whyClip.Stop ();
			whyClip = null;
		}
		if(whenClip != null)
		{
			whenClip.Stop ();
			whenClip = null;
		}
		if(howClip != null)
		{
			howClip.Stop ();
			howClip = null;
		}
		if(whoClip != null)
		{
			whoClip.Stop ();
			whoClip = null;
		}
		if(signClip != null)
		{
			signClip.Stop ();
			signClip = null;
		}
#endif
	}
}
