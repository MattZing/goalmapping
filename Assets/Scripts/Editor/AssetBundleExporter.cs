﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;

public class AssetBundleExporter : EditorWindow
{
	public string[] buildTargetNames;
	public string outputFolderName = "MyAssetBundle";
	private int buildTargetNumber = 0;
	
	[MenuItem("Asset Bundles/Exporter")]
	/// <summary>
	/// Adds the menu item to access the exporter.
	/// </summary>
	public static void CreateWindow()
	{
		EditorWindow.GetWindow (typeof(AssetBundleExporter)).title = "Exporter";
	}
	
	BuildTarget GetBuildTarget(string buildTargetName)
	{
		switch(buildTargetName)
		{
		case "WebPlayer":
			return BuildTarget.WebPlayer;
			
		case "Android":
			return BuildTarget.Android;
			
		case "IOS":
			return BuildTarget.iOS;
			
		case "PC Standalone":
			return BuildTarget.StandaloneWindows;
		}
		
		return BuildTarget.WebPlayer;
	}
	
	void OnGUI()
	{
		if(buildTargetNames == null)
		{
			buildTargetNames = new string[]{"Android", "WebPlayer", "IOS", "PC Standalone"};
		}
		
		GUILayout.Label ("Build Platform:");
		buildTargetNumber = GUILayout.Toolbar (buildTargetNumber, buildTargetNames);
		
		GUILayout.Space (8);
		
		GUILayout.Label ("Output Folder name:");
		outputFolderName = GUILayout.TextField (outputFolderName);
		
		GUILayout.Space (8);
		
		if(GUILayout.Button ("Export"))
		{
			string bundleNames = "";
			
			foreach(string s in AssetDatabase.GetAllAssetBundleNames())
			{
				bundleNames += s + ", ";
			}
			
			Debug.Log ("Bundle Names: " + bundleNames);
			ExportBundle();
		}
	}
	
	void ExportBundle()
	{
		if(!Directory.Exists (outputFolderName))
		{
			Directory.CreateDirectory (outputFolderName);
		}
		
		#pragma warning disable 0618
		BuildPipeline.BuildAssetBundles (outputFolderName, BuildAssetBundleOptions.CollectDependencies, GetBuildTarget (buildTargetNames[buildTargetNumber]));
		#pragma warning restore 0618
//		BuildPipeline.BuildAssetBundles (outputFolderName);
	}
}
