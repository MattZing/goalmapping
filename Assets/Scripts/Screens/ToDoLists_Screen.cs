﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ToDoLists_Screen : MenuScreen
{
	private List<GameObject> mainScreenObjects;
	private List<GameObject> taskParentObjects;
	private GameObject[] numberOfTasksObjects;
	private GameObject[] viewTasksObjects;
	private GameObject localCanvas;
	private GameObject goalMapTitleObject;
	private GameObject showAllTasksButton;
	private GameObject switchBrainSideButton;
	private GameObject goalMapTextHowParentObject;
	private GameObject goalMapPicHowParentObject;
	private GameObject taskListScrollArea;
	private GameObject loadingPopup;
	private GameObject loadingSpinner;
	
	private Sprite[] allTasksSprite;
	private Sprite[] openGoalMapSprite;
	private Sprite[] switchBrainSideSprite;
	private Sprite[] viewSprite;
	private Sprite[] saveSprite;
	private Sprite[] addSprite;
	private Sprite deleteSprite;
	private Sprite spinnerSprite;
	
	private List<MCP.Task> taskList;
	private bool showAllTasks = true;
	private bool showingLeftBrain = false;
	private bool goalMapOpened = false;
	private float spinnerRot = 0;
	private int currentHowNumber = 0;
	private int downloadStep = 0;
	private int numberOfTasksUsed = 0;
	private MCP.GoalMap currentGoalMap;

	private const int numberOfHowButtons = 3;
			
	enum InternalState
	{
		viewTasks,
		addTask
	}
	
	// Use this for initialization
	void Start ()
	{
		// Initialize base.
		Init();
		
		// Initialize lists/arrays
		mainScreenObjects = new List<GameObject>();
		taskParentObjects = new List<GameObject>();
        taskList = new List<MCP.Task>();
        allTasksSprite = new Sprite[2];
		openGoalMapSprite = new Sprite[2];
		switchBrainSideSprite = new Sprite[4];
		viewSprite = new Sprite[2];
		saveSprite = new Sprite[2];
		addSprite = new Sprite[2];
		
		// Load resources
		allTasksSprite[0] = Resources.Load<Sprite>("GUI/richtext_alignleft");
		allTasksSprite[1] = Resources.Load<Sprite>("GUI/richtext_alignleft_active");
		openGoalMapSprite[0] = Resources.Load<Sprite>("GUI/GoalMap/goalmap_openmap");
		openGoalMapSprite[1] = Resources.Load<Sprite>("GUI/GoalMap/goalmap_openmap_active");
		switchBrainSideSprite[0] = Resources.Load<Sprite>("GUI/GoalMap/bm_left_brain");
		switchBrainSideSprite[1] = Resources.Load<Sprite>("GUI/GoalMap/bm_left_brain_active");
		switchBrainSideSprite[2] = Resources.Load<Sprite>("GUI/GoalMap/bm_right_brain");
		switchBrainSideSprite[3] = Resources.Load<Sprite>("GUI/GoalMap/bm_right_brain_active");
		viewSprite[0] = Resources.Load<Sprite>("GUI/GoalMap/goalmap_copy");
		viewSprite[1] = Resources.Load<Sprite>("GUI/GoalMap/goalmap_copy_active");
		saveSprite[0] = Resources.Load<Sprite>("GUI/Painter/paintbox_done");
		saveSprite[1] = Resources.Load<Sprite>("GUI/Painter/paintbox_done_active");
		addSprite[0] = Resources.Load<Sprite>("GUI/GoalMap/goalmap_plus");
		addSprite[1] = Resources.Load<Sprite>("GUI/GoalMap/goalmap_plus_active");
		deleteSprite = Resources.Load<Sprite>("GUI/menuicon_logout");
		spinnerSprite = Resources.Load<Sprite>("GUI/bm_rotate");
		
		// Get current task lists
		if(MCP.userInfo != null)
		{
			StartCoroutine (MCP.GetTaskList (MCP.userInfo.username));
		}
		
		// Make the GUI
		CreateGUI();
		
		localCanvas.transform.localScale = Vector2.zero;
	}
	
	void CreateGUI()
	{
		// Make the canvas
		localCanvas = MakeCanvas();
		
		UnityEngine.UI.Navigation nav = new UnityEngine.UI.Navigation();
		nav.mode = UnityEngine.UI.Navigation.Mode.None;
		SpriteState spriteState = new SpriteState();
		
		// Back the left background window
		GameObject leftBackgroundWindow = MakeImage (localCanvas, new Rect(Screen.width * 0.02f, Screen.height * 0.15f, Screen.width * 0.47f, Screen.height * 0.83f), window_back);
		leftBackgroundWindow.name = "LeftBackgroundWindow";
		mainScreenObjects.Add (leftBackgroundWindow);
		
		// Back the left background window
		GameObject rightBackgroundWindow = MakeImage (localCanvas, new Rect(Screen.width * 0.51f, Screen.height * 0.15f, Screen.width * 0.47f, Screen.height * 0.83f), window_back);
		rightBackgroundWindow.name = "RightBackgroundWindow";
		mainScreenObjects.Add (rightBackgroundWindow);
		
		// Make the title bar.
		GameObject titleBar = MakeTitleBar (localCanvas, MCP.Text (3506)/*"To Do List*"*/);
		titleBar.name = "TitleBar";
		
		// Make the side gizmo.
		MakeSideGizmo (localCanvas, true, true);
		
		// Make goal map title
		goalMapTitleObject = MakeLabel (localCanvas, MCP.Text (3507)/*"No Goal Map Selected*"*/, new Rect(Screen.width * 0.04f, Screen.height * 0.18f, Screen.width * 0.25f, Screen.height * 0.1f), TextAnchor.UpperLeft);
		goalMapTitleObject.name = "GoalMapTitleObject";
		
		// Make the goal map how objects
		goalMapTextHowParentObject = MakeTextGoalMapHowElements ();
		goalMapTextHowParentObject.transform.SetParent (localCanvas.transform);
		goalMapTextHowParentObject.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
		Vector3 newPos = new Vector3(Screen.width * -0.54f, Screen.height * 0.63f, 0);
		goalMapTextHowParentObject.transform.localPosition = newPos;
		
		goalMapTextHowParentObject.SetActive (false);
		
		goalMapPicHowParentObject = MakePicGoalMapHowElements ();
		goalMapPicHowParentObject.transform.SetParent (localCanvas.transform);
		goalMapPicHowParentObject.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
		newPos = new Vector3(Screen.width * -0.5f, Screen.height * 0.63f, 0);
		goalMapPicHowParentObject.transform.localPosition = newPos;
		
		// Make the show all tasks button.
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			ToggleShowAll
		};
		showAllTasksButton = MakeButton (localCanvas, "", allTasksSprite[1], new Rect(Screen.width * 0.04f, Screen.height * 0.28f, Screen.width * 0.08f, Screen.width * 0.08f), funcList);
		showAllTasksButton.name = "ShowAllTasksButton";
		// Make hover sprite
		showAllTasksButton.GetComponent<Button>().navigation = nav;
		showAllTasksButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		showAllTasksButton.GetComponent<Button>().targetGraphic = showAllTasksButton.GetComponentInChildren<Image>();
		spriteState.highlightedSprite = allTasksSprite[1];
		spriteState.pressedSprite = allTasksSprite[1];
		showAllTasksButton.GetComponent<Button>().spriteState = spriteState;
		
		GameObject showAllLabel = MakeButton (localCanvas, MCP.Text (3501)/*"Show\nAll*"*/, new Rect(Screen.width * 0.04f, (Screen.height * 0.28f) + (Screen.width * 0.0825f), Screen.width * 0.08f, Screen.height * 0.08f), funcList);
		showAllLabel.name = "ShowAllLabel";
		showAllLabel.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
		showAllLabel.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
		showAllLabel.GetComponent<Text>().color = Color.white;
		// Make hover sprite
		showAllLabel.GetComponent<Button>().navigation = nav;
		showAllLabel.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		showAllLabel.GetComponent<Button>().image = showAllTasksButton.GetComponentInChildren<Image>();
		showAllLabel.GetComponent<Button>().spriteState = spriteState;
		
		// Make the switch between left/right brain button.
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				showingLeftBrain = !showingLeftBrain;
				
				if(showingLeftBrain)
				{
					// Disable the right brain goal map
					goalMapPicHowParentObject.SetActive (false);
					// Enable the left brain goal map
					goalMapTextHowParentObject.SetActive (true);
					// Swap the button sprite to use the left brain sprites.
					switchBrainSideButton.GetComponent<Image>().sprite = switchBrainSideSprite[0];
					spriteState = new SpriteState();
					switchBrainSideButton.GetComponent<Button>().targetGraphic = switchBrainSideButton.GetComponentInChildren<Image>();
					spriteState.highlightedSprite = switchBrainSideSprite[1];
					spriteState.pressedSprite = switchBrainSideSprite[1];
					switchBrainSideButton.GetComponent<Button>().spriteState = spriteState;
				}
				else
				{
					// Disable the left brain goal map
					goalMapTextHowParentObject.SetActive (false);
					// Enable the right brain goal map
					goalMapPicHowParentObject.SetActive (true);
					// Swap the button sprite to use the right brain sprites.
					switchBrainSideButton.GetComponent<Image>().sprite = switchBrainSideSprite[2];
					spriteState = new SpriteState();
					switchBrainSideButton.GetComponent<Button>().targetGraphic = switchBrainSideButton.GetComponentInChildren<Image>();
					spriteState.highlightedSprite = switchBrainSideSprite[3];
					spriteState.pressedSprite = switchBrainSideSprite[3];
					switchBrainSideButton.GetComponent<Button>().spriteState = spriteState;
				}
			}
		};
		switchBrainSideButton = MakeButton (localCanvas, "", switchBrainSideSprite[2], new Rect(Screen.width * 0.04f, Screen.height * 0.53f, Screen.width * 0.08f, Screen.width * 0.08f), funcList);
		switchBrainSideButton.name = "SwitchBrainSideButton";
		// Make hover sprite
		switchBrainSideButton.GetComponent<Button>().navigation = nav;
		switchBrainSideButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		switchBrainSideButton.GetComponent<Button>().targetGraphic = switchBrainSideButton.GetComponentInChildren<Image>();
		spriteState.highlightedSprite = switchBrainSideSprite[3];
		spriteState.pressedSprite = switchBrainSideSprite[3];
		switchBrainSideButton.GetComponent<Button>().spriteState = spriteState;
		
		GameObject switchGMLabel = MakeButton (localCanvas, MCP.Text (3502)/*"Switch*"*/, new Rect(Screen.width * 0.04f, (Screen.height * 0.53f) + (Screen.width * 0.0825f), Screen.width * 0.08f, Screen.height * 0.08f), funcList);
		switchGMLabel.name = "SwitchGMLabel";
		switchGMLabel.GetComponent<Text>().alignment = TextAnchor.UpperCenter;
		switchGMLabel.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Overflow;
		switchGMLabel.GetComponent<Text>().color = Color.white;
		// Make hover sprite
		switchGMLabel.GetComponent<Button>().navigation = nav;
		switchGMLabel.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		switchGMLabel.GetComponent<Button>().image = switchBrainSideButton.GetComponentInChildren<Image>();
		switchGMLabel.GetComponent<Button>().spriteState = spriteState;
		
		// Make the open goal map button
		funcList = new List<MyVoidFunc>
		{
			OpenGoalMapBrowser
		};
		GameObject openGoalMapButton = MakeButton (localCanvas, "", openGoalMapSprite[0], new Rect(Screen.width * 0.04f, Screen.height * 0.78f, Screen.width * 0.08f, Screen.width * 0.08f), funcList);
		openGoalMapButton.name = "OpenGoalMapButton";
		// Make hover sprite
		openGoalMapButton.GetComponent<Button>().navigation = nav;
		openGoalMapButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		openGoalMapButton.GetComponent<Button>().targetGraphic = openGoalMapButton.GetComponentInChildren<Image>();
		spriteState.highlightedSprite = openGoalMapSprite[1];
		spriteState.pressedSprite = openGoalMapSprite[1];
		openGoalMapButton.GetComponent<Button>().spriteState = spriteState;
		
		GameObject openGMLabel = MakeButton (localCanvas, MCP.Text (3503)/*"Open*"*/, new Rect(Screen.width * 0.04f, (Screen.height * 0.78f) + (Screen.width * 0.0825f), Screen.width * 0.08f, Screen.height * 0.08f), funcList);
		openGMLabel.name = "OpenGMLabel";
		openGMLabel.GetComponent<Text>().alignment = TextAnchor.UpperCenter;
		openGMLabel.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Overflow;
		openGMLabel.GetComponent<Text>().color = Color.white;
		// Make hover sprite
		openGMLabel.GetComponent<Button>().navigation = nav;
		openGMLabel.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		openGMLabel.GetComponent<Button>().image = openGoalMapButton.GetComponentInChildren<Image>();
		openGMLabel.GetComponent<Button>().spriteState = spriteState;
		
		int numberOfHows = 3;
		numberOfTasksObjects = new GameObject[numberOfHows];
		viewTasksObjects = new GameObject[numberOfHows];
		for(int i = 0; i < numberOfHows; i++)
		{
			// Make the how number of tasks.
			numberOfTasksObjects[i] = MakeLabel (localCanvas, "0", new Rect(Screen.width * 0.295f, (Screen.height * 0.35f) + (Screen.height * 0.21f * i), Screen.width * 0.1f, Screen.height * 0.1f));
			numberOfTasksObjects[i].name = "NumberOfTasks";
			
			// Make the how view task list
			viewTasksObjects[i] = MakeActionButton (localCanvas, viewSprite[0], new Rect(Screen.width * 0.365f, (Screen.height * 0.32f) + (Screen.height * 0.21f * i), Screen.height * 0.1f, Screen.height * 0.1f), SetHowNumber, i);
			viewTasksObjects[i].name = "ViewTasks";
			// Make hover sprite
			viewTasksObjects[i].GetComponent<Button>().navigation = nav;
			viewTasksObjects[i].GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
			
			spriteState = new SpriteState();
			viewTasksObjects[i].GetComponent<Button>().targetGraphic = viewTasksObjects[i].GetComponentInChildren<Image>();
			spriteState.highlightedSprite = viewSprite[1];
			spriteState.pressedSprite = viewSprite[1];
			viewTasksObjects[i].GetComponent<Button>().spriteState = spriteState;
		}
		
		// Make task list title
		GameObject taskListTitle = MakeLabel (localCanvas, MCP.Text (3508)/*"Task List*"*/, new Rect(Screen.width * 0.525f, Screen.height * 0.18f, Screen.width * 0.3f, Screen.height * 0.1f), TextAnchor.UpperLeft);
		taskListTitle.name = "TaskListTitle";
		
		// Make task list scroll area
		taskListScrollArea = MakeScrollRect (localCanvas, new Rect(Screen.width * 0.525f, Screen.height * 0.25f, Screen.width * 0.42f, Screen.height * 0.5f), null);
		taskListScrollArea.name = "TaskListScrollArea";
		
		taskListScrollArea.GetComponent<ScrollRect>().horizontal = false;
		taskListScrollArea.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform>().pivot = Vector2.zero;
		
		// Make the save tasks button
		funcList = new List<MyVoidFunc>
		{
			// Save task list.
			SaveTaskList
		};
		GameObject saveTasks = MakeButton (localCanvas, "", saveSprite[0], new Rect(Screen.width * 0.725f, Screen.height * 0.78f, Screen.width * 0.08f, Screen.width * 0.08f), funcList);
		saveTasks.name = "SaveTasks";
		// Make hover sprite
		saveTasks.GetComponent<Button>().navigation = nav;
		saveTasks.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		saveTasks.GetComponent<Button>().targetGraphic = saveTasks.GetComponentInChildren<Image>();
		spriteState.highlightedSprite = saveSprite[1];
		spriteState.pressedSprite = saveSprite[1];
		saveTasks.GetComponent<Button>().spriteState = spriteState;
		
		GameObject saveTaskLabel = MakeButton (localCanvas, MCP.Text (3504)/*"Save*"*/, new Rect(Screen.width * 0.725f, (Screen.height * 0.78f) + (Screen.width * 0.0825f), Screen.width * 0.08f, Screen.height * 0.08f), funcList);
		saveTaskLabel.name = "SaveTaskLabel";
		saveTaskLabel.GetComponent<Text>().alignment = TextAnchor.UpperCenter;
		saveTaskLabel.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Overflow;
		saveTaskLabel.GetComponent<Text>().color = Color.white;
		// Make hover sprite
		saveTaskLabel.GetComponent<Button>().navigation = nav;
		saveTaskLabel.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		saveTaskLabel.GetComponent<Button>().image = saveTasks.GetComponentInChildren<Image>();
		saveTaskLabel.GetComponent<Button>().spriteState = spriteState;
		
		// Make the how add task
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				GameObject scrollContent = taskListScrollArea.transform.GetChild (0).GetChild (0).gameObject;
				
				if(showAllTasks)
				{
					numberOfTasksUsed = taskParentObjects.Count;
				}
				
				GameObject newTask = MakeTaskObject (scrollContent, new Rect(Screen.width * 0.025f, (Screen.height * -0.47f) + (Screen.height * 0.1f * numberOfTasksUsed), Screen.width * 0.35f, Screen.height * 0.08f), "", false);
				taskParentObjects.Add (newTask);
				// Make a new task on the list.
				taskList.Add (new MCP.Task());
				taskList[taskList.Count - 1].howNumber = currentHowNumber.ToString ();
				if(currentGoalMap != null && !showAllTasks)
				{
					taskList[taskList.Count - 1].gmReference = currentGoalMap.reference;
				}
				else
				{
					taskList[taskList.Count - 1].gmReference = null;
				}
				
				numberOfTasksUsed++;
				
				// Make sure the scroll area contains all tasks.
				if((Screen.height * 0.1f * numberOfTasksUsed) > Screen.height * 0.5f)
				{
					scrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(scrollContent.GetComponent<RectTransform>().sizeDelta.x, Screen.height * 0.125f * numberOfTasksUsed);
				}
				else
				{
					scrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(scrollContent.GetComponent<RectTransform>().sizeDelta.x, Screen.height * 0.525f);
				}
			}
		};
		GameObject addTasks = MakeButton (localCanvas, "", addSprite[0], new Rect(Screen.width * 0.85f, Screen.height * 0.78f, Screen.width * 0.08f, Screen.width * 0.08f), funcList);
		addTasks.name = "AddTasks";
		// Make hover sprite
		addTasks.GetComponent<Button>().navigation = nav;
		addTasks.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		addTasks.GetComponent<Button>().targetGraphic = addTasks.GetComponentInChildren<Image>();
		spriteState.highlightedSprite = addSprite[1];
		spriteState.pressedSprite = addSprite[1];
		addTasks.GetComponent<Button>().spriteState = spriteState;

		GameObject addTaskLabel = MakeButton (localCanvas, MCP.Text (3505)/*"Add*"*/, new Rect(Screen.width * 0.85f, (Screen.height * 0.78f) + (Screen.width * 0.0825f), Screen.width * 0.08f, Screen.height * 0.08f), funcList);
		addTaskLabel.name = "AddTaskLabel";
		addTaskLabel.GetComponent<Text>().alignment = TextAnchor.UpperCenter;
		addTaskLabel.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Overflow;
		addTaskLabel.GetComponent<Text>().color = Color.white;
		// Make hover sprite
		addTaskLabel.GetComponent<Button>().navigation = nav;
		addTaskLabel.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		addTaskLabel.GetComponent<Button>().image = addTasks.GetComponentInChildren<Image>();
		addTaskLabel.GetComponent<Button>().spriteState = spriteState;
	}
	
	private void ToggleShowAll()
	{
		if(currentGoalMap != null)
		{
			showAllTasks = !showAllTasks;
			
			UnityEngine.UI.Navigation nav = new UnityEngine.UI.Navigation();
			nav.mode = UnityEngine.UI.Navigation.Mode.None;
			SpriteState spriteState = new SpriteState();
			
			// If showing all tasks, make the normal state of the button active.
			if(showAllTasks)
			{
				showAllTasksButton.GetComponent<Image>().sprite = allTasksSprite[1];
				// Make hover sprite
				showAllTasksButton.GetComponent<Button>().navigation = nav;
				showAllTasksButton.GetComponent<Button>().transition = Selectable.Transition.ColorTint;
				
				// Deactivate the current how number button sprites.
				SetHowNumberButtonSprites(-1, numberOfHowButtons);
			}
			// Otherwise, make it normal with an active hover.
			else
			{
				showAllTasksButton.GetComponent<Image>().sprite = allTasksSprite[0];
				// Make hover sprite
				showAllTasksButton.GetComponent<Button>().navigation = nav;
				showAllTasksButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
				
				showAllTasksButton.GetComponent<Button>().targetGraphic = showAllTasksButton.GetComponentInChildren<Image>();
				spriteState.highlightedSprite = allTasksSprite[1];
				spriteState.pressedSprite = allTasksSprite[1];
				showAllTasksButton.GetComponent<Button>().spriteState = spriteState;
				
				// Activate the current how number button sprite.
				SetHowNumberButtonSprites(currentHowNumber, numberOfHowButtons);
			}
			
			// Destroy all task objects to refresh list.
			for(int i = taskParentObjects.Count - 1; i >= 0; i--)
			{
				Destroy (taskParentObjects[i]);
				taskParentObjects.RemoveAt (i);
			}
		}
	}
	
	private void SetHowNumberButtonSprites(int selectedHowNumber, int _numberOfHowButtons)
	{
		for(int i = 0; i < _numberOfHowButtons; i++)
		{
			if(selectedHowNumber == i)
			{
				if(!showAllTasks)
				{
					viewTasksObjects[i].GetComponent<Image>().sprite = viewSprite[1];
				}
			}
			else
			{
				viewTasksObjects[i].GetComponent<Image>().sprite = viewSprite[0];
			}
		}
	}
	
	private bool SetHowNumber(int i)
	{
		currentHowNumber = i;
		SetHowNumberButtonSprites(i, numberOfHowButtons);
		
		// Destroy all task objects to refresh list.
		for(int j = taskParentObjects.Count - 1; j >= 0; j--)
		{
			Destroy (taskParentObjects[j]);
			taskParentObjects.RemoveAt (j);
		}	
		
		return true;
	}
	
	private void OpenGoalMapBrowser()
	{
		// Reset the first frame timer.
		MenuScreen.firstFrameTimer = 0;
		// Add the simulated file browser script and set variables.
		Camera.main.gameObject.AddComponent<SimulateFileBrowser>().sceneCanvas = localCanvas;
	}
	
	private void SetGoalMapHowObjects()
	{
		goalMapTextHowParentObject.SetActive (true);
		goalMapPicHowParentObject.SetActive (true);
		
		for(int i = 0; i < currentGoalMap.hows.Count; i++)
		{
			goalMapTextHowParentObject.transform.GetChild ((i * 3) + 2).gameObject.GetComponent<Text>().text = currentGoalMap.hows[i].how;
			goalMapPicHowParentObject.transform.GetChild (i).gameObject.GetComponent<Image>().sprite = Sprite.Create (currentGoalMap.hows[i].image, new Rect(0, 0, currentGoalMap.hows[i].image.width, currentGoalMap.hows[i].image.height), new Vector2(0.5f, 0.5f));
		}
		
		if(showingLeftBrain)
		{
			goalMapPicHowParentObject.SetActive (false);
		}
		else
		{
			goalMapTextHowParentObject.SetActive (false);
		}
		
		// Set title object.
		if(currentGoalMap.name == null || currentGoalMap.name == "")
		{
			currentGoalMap.name = MCP.Text(2501)/*My Goal Map*/;
		}
		
		goalMapTitleObject.GetComponent<Text>().text = currentGoalMap.name;
	}
	
	/// <summary>
	/// Loadings the goal map.
	/// </summary>
	/// <returns>The goal map.</returns>
	private IEnumerator LoadingGoalMap()
	{
		yield return null;
		
		if(MCP.gmIndex != -1)
		{
			// Make the goal map json a goal map object.
			currentGoalMap = MCP.goalMaps[MCP.gmIndex];
			
			// Load in the images from their byte data.
			for(int i = 0; i < currentGoalMap.hows.Count; i++)
			{
				if(currentGoalMap.hows[i].image == null)
				{
					if(currentGoalMap.hows[i].imageBytes != null)
					{
						currentGoalMap.hows[i].image = new Texture2D(512, 512);
						currentGoalMap.hows[i].image.LoadImage (currentGoalMap.hows[i].imageBytes);
					}
					else
					{
						currentGoalMap.hows[i].image = lightPurpleBackground.texture;
					}
				}
			}
			
			// Populate the goal map ibject with the goal map.
//			SetGoalMapHowObjects();
			
			// Reset the map type label.
			switchBrainSideButton.GetComponent<Image>().sprite = switchBrainSideSprite[2];
			
			// Make hover sprite
			UnityEngine.UI.Navigation nav = new UnityEngine.UI.Navigation();
			nav.mode = UnityEngine.UI.Navigation.Mode.None;
			
			switchBrainSideButton.GetComponent<Button>().navigation = nav;
			switchBrainSideButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
			
			SpriteState spriteState = new SpriteState();
			switchBrainSideButton.GetComponent<Button>().targetGraphic = switchBrainSideButton.GetComponentInChildren<Image>();
			spriteState.highlightedSprite = switchBrainSideSprite[3];
			spriteState.pressedSprite = switchBrainSideSprite[3];
			switchBrainSideButton.GetComponent<Button>().spriteState = spriteState;
		}
		else
		{
			// Enable pictures.
			goalMapTextHowParentObject.SetActive (false);
			goalMapPicHowParentObject.SetActive (true);
		}
	}
	
	public void FileBrowserClosed()
	{
		//TODO: Handle if no map was selected.
		if(MCP.gmIndex != -1)
		{
			goalMapOpened = true;
			currentGoalMap = null;
			
			List<MyVoidFunc> funcList = new List<MyVoidFunc>
			{
				delegate
				{
					StopCoroutine (LoadingGoalMap ());
					Destroy(loadingPopup);
				}
			};
			// Create loading popup
			loadingPopup = MakePopup (localCanvas, defaultPopUpWindow, MCP.Text (214)/*"Loading..."*/, "", MCP.Text (207)/*"Cancel"*/, funcList);
			loadingSpinner = MakeImage (loadingPopup, new Rect(Screen.width * 0.25f, Screen.height * -0.4f, Screen.width * 0.1f, Screen.width * 0.1f), spinnerSprite);
			loadingSpinner.name = "LoadingSpinner";
			loadingSpinner.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
			
			StartCoroutine (LoadingGoalMap ());
			
			// Destroy all task objects to refresh list.
			for(int i = taskParentObjects.Count - 1; i >= 0; i--)
			{
				Destroy (taskParentObjects[i]);
				taskParentObjects.RemoveAt (i);
			}
		}
	}
	
	private GameObject MakeTaskObject(GameObject parent, Rect rect, string taskString, bool completed = false)
	{
		rect.x = rect.x + (Screen.width * 0.5f);
		rect.y = rect.y + (Screen.height * 0.5f);
		// Make a parent for the task option.
		GameObject taskParent = MakePanel (parent, rect);
		taskParent.name = "TaskParent";
		
		// Make checkbox for whether task is complete.
		GameObject taskCheckbox = MakeCheckbox(taskParent, new Rect(rect.width * 0.49f + (Screen.width * 0.5f), -rect.height * 0.5f + (Screen.height * 0.5f), rect.width * 0.18f, rect.height), "");
		taskCheckbox.name = "TaskCheckbox";
		if(completed)
		{
			taskCheckbox.GetComponent<Toggle>().isOn = true;
		}
		
		// Make the input field for the task.
		GameObject taskInputField = MakeInputField (taskParent, taskString, new Rect(rect.width * 0.19f + (Screen.width * 0.5f), -rect.height + (Screen.height * 0.5f), rect.width * 0.75f, rect.height), false);
		taskInputField.name = "TaskInputField";
		taskInputField.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		taskInputField.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;
		taskInputField.GetComponent<InputField>().characterLimit = 36;
		
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			delegate
			{
				// Remove this from the task list.
				for(int i = 0; i < taskParentObjects.Count; i++)
				{
					if(taskParentObjects[i] == taskParent)
					{
						taskList.RemoveAt (i);
					}
				}
				
				// Destroy all task objects to refresh list.
				for(int i = taskParentObjects.Count - 1; i >= 0; i--)
				{
					Destroy (taskParentObjects[i]);
					taskParentObjects.RemoveAt (i);
				}
				
				// Return the scroll content to its inital position.
				taskListScrollArea.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform>().localPosition = Vector3.zero;
				taskListScrollArea.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(taskListScrollArea.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform>().sizeDelta.x, Screen.height * 0.5f);
			}
		};
		GameObject taskDeleteButton = MakeButton (taskParent, "", deleteSprite, new Rect(rect.width * 0.975f + (Screen.width * 0.5f), -rect.height * 0.9f + (Screen.height * 0.5f), rect.width * 0.1f, rect.width * 0.1f), funcList);
		taskDeleteButton.name = "TaskDeleteButton";
		taskDeleteButton.GetComponent<Image>().color = Color.red;
		
		return taskParent;
	}
	
	void UpdateTaskInfo()
	{
		for(int i = 0; i < taskParentObjects.Count; i++)
		{
			if(taskParentObjects[i].GetComponent<RectTransform>() != null)
			{
				bool complete = taskParentObjects[i].transform.Find ("TaskCheckbox").gameObject.GetComponent<Toggle>().isOn;
				taskList[i].taskComplete = complete ? "1" : "0";
				
				string task = taskParentObjects[i].transform.Find ("Image").Find ("TaskInputField").gameObject.GetComponent<InputField>().text;
				if(task == null)
				{
					task = "";
				}
				taskList[i].taskString = task;
			}
		}
	}
	
	private void SetHowNumberLabels()
	{
		int[] numberOfTasks = new int[numberOfHowButtons];
		
		for(int i = 0; i < taskList.Count; i++)
		{
			if(currentGoalMap != null && taskList[i].gmReference == currentGoalMap.reference)
			{
				numberOfTasks[int.Parse (taskList[i].howNumber)]++;
			}
		}
		
		for(int i = 0; i < numberOfTasksObjects.Length; i++)
		{
			numberOfTasksObjects[i].GetComponent<Text>().text = numberOfTasks[i].ToString ();
		}
	}
	
	private void UpdateSpinner()
	{
		spinnerRot += Time.deltaTime * -55f;
		loadingSpinner.GetComponent<RectTransform>().localEulerAngles = new Vector3(0, 0, spinnerRot);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(taskList == null && !MCP.isTransmitting)
		{
			taskList = MCP.taskList;
		}
		
		if(loadingPopup != null)
		{
			UpdateSpinner();
		}
		
		// Get the images for the new goal map
		if(goalMapOpened && currentGoalMap != null)
		{
			if(!MCP.isTransmitting)
			{
				if(downloadStep != 0)
				{
					if(MCP.pulledImages != null && MCP.pulledImages.Count > 0 && MCP.pulledImages[0] != null)
					{
						currentGoalMap.hows[downloadStep - 1].image = MCP.pulledImages[0];
					}
					else
					{
						currentGoalMap.hows[downloadStep - 1].image = lightPurpleBackground.texture;
					}
				}
				
				if(downloadStep >= currentGoalMap.hows.Count)
				{
					SetGoalMapHowObjects();
					SetHowNumber(currentHowNumber);
					
					goalMapOpened = false;
					downloadStep = 0;
					
					if(loadingPopup != null)
					{
						// Turn off showign all tasks.
						if(showAllTasks)
						{
							ToggleShowAll();
						}
						
						Destroy(loadingPopup);
					}
				}
				else
				{
					MCP.pulledImages = new List<Texture2D>();
					
					if(currentGoalMap.hows[downloadStep].imageRef != null)
					{
						StartCoroutine (MCP.GetImage (MCP.userInfo.username, currentGoalMap.hows[downloadStep].imageRef));
					}
					
					downloadStep++;
				}
			}
		}
		
		if(taskList != null && taskList.Count > 0)
		{
			SetHowNumberLabels();
		}
		
		int numberOfHows = 3;
		// If show all tasks is enabled, make task objects for all tasks.
		if(showAllTasks)
		{
			for(int i = 0; i < numberOfHows; i++)
			{
				viewTasksObjects[i].GetComponent<Button>().interactable = false;
			}
			
			numberOfTasksUsed = taskParentObjects.Count;
			
			if(taskList != null && taskList.Count > 0)
			{
				if(taskParentObjects.Count == 0)
				{
					taskListScrollArea.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform>().localPosition = Vector3.zero;
					taskListScrollArea.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(taskListScrollArea.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform>().sizeDelta.x, 0);
					GameObject scrollContent = taskListScrollArea.transform.GetChild (0).GetChild (0).gameObject;
					
					for(int i = 0; i < taskList.Count; i++)
					{
						// Make tasks
						bool complete = taskList[i].taskComplete == "1" ? true : false;
						if(taskList[i].taskString == null)
						{
							taskList[i].taskString = "";
						}
						
						GameObject newTask = MakeTaskObject (scrollContent, new Rect(Screen.width * 0.025f, (Screen.height * 0.03f) + (Screen.height * 0.1f * i), Screen.width * 0.35f, Screen.height * 0.08f), taskList[i].taskString, complete);
						taskParentObjects.Add (newTask);
					}
					numberOfTasksUsed = taskParentObjects.Count;
					
					if((Screen.height * 0.1f * numberOfTasksUsed) > Screen.height * 0.5f)
					{
						scrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(scrollContent.GetComponent<RectTransform>().sizeDelta.x, Screen.height * 0.125f * numberOfTasksUsed);
						taskListScrollArea.GetComponent<ScrollRect>().velocity = new Vector2(0, -1000);
					}
					else
					{
						scrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(scrollContent.GetComponent<RectTransform>().sizeDelta.x, Screen.height * 0.525f);
						taskListScrollArea.GetComponent<ScrollRect>().velocity = new Vector2(0, -1000);
					}
				}
			}
		}
		// Otherwise, show the selected how tasks.
		else
		{
			for(int i = 0; i < numberOfHows; i++)
			{
				viewTasksObjects[i].GetComponent<Button>().interactable = true;
			}
			
			// If number of tasks is greater than 0, make objects for each task.
			if(taskList != null && taskList.Count > 0)
			{
				if(taskParentObjects.Count == 0)
				{
					taskListScrollArea.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform>().localPosition = Vector3.zero;
					taskListScrollArea.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(taskListScrollArea.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform>().sizeDelta.x, 0);
					GameObject scrollContent = taskListScrollArea.transform.GetChild (0).GetChild (0).gameObject;
					numberOfTasksUsed = 0;
					
					for(int i = 0; i < taskList.Count; i++)
					{
						if(currentGoalMap != null && taskList[i].gmReference == currentGoalMap.reference && taskList[i].howNumber == currentHowNumber.ToString())
						{
							// Make tasks
							bool complete = taskList[i].taskComplete == "1" ? true : false;
							GameObject newTask = MakeTaskObject (scrollContent, new Rect(Screen.width * 0.025f, (Screen.height * 0.03f) + (Screen.height * 0.1f * numberOfTasksUsed), Screen.width * 0.35f, Screen.height * 0.08f), taskList[i].taskString, complete);
							taskParentObjects.Add (newTask);
							// Track number of tasks used for positioning.
							numberOfTasksUsed++;
						}
						else
						{
							GameObject g = new GameObject();
							g.name = "TempTaskObj";
							taskParentObjects.Add (g);
						}
					}
					
					if((Screen.height * 0.1f * numberOfTasksUsed) > Screen.height * 0.5f)
					{
						scrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(scrollContent.GetComponent<RectTransform>().sizeDelta.x, Screen.height * 0.125f * numberOfTasksUsed);
						taskListScrollArea.GetComponent<ScrollRect>().velocity = new Vector2(0, -1000);
					}
					else
					{
						scrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(scrollContent.GetComponent<RectTransform>().sizeDelta.x, Screen.height * 0.525f);
						taskListScrollArea.GetComponent<ScrollRect>().velocity = new Vector2(0, -1000);
					}
				}
			}
			//TODO: Otherwise, display 'no tasks'.
		}
		
		UpdateTaskInfo();
	}
	
	private void SaveTaskList()
	{
		MCP.taskList = taskList;

        if (MCP.userInfo != null)
        {
            StartCoroutine(MCP.UpdateTaskList());
        }
	}
	
	public override void DoGUI() {}
}
