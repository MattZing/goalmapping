using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Forum_Screen : MenuScreen
{
	public List<MyMessages> friend;
	private Rect screenSize;
	private Rect topPanelRect;
	private Rect leftPanelRect;
	private Rect rightPanelRect;
	//private Texture closeIconImage;
	//private Sprite glassBackground;
	//private float teamButtonsWidth;
	//private float bottomPanelSpacer;
	private string postTitle = "";
	private string postContent = "";
	private string commentContent = "";
	private float elementHeight;
	//private Texture loginButtonImage;
	Vector2 scrollPosition1, scrollPosition2;
	//private MCP.ZingMessage selectedMessage;
	private bool hasTripped = false;
	private WordPressPost selectedPost;
	private WordPressPost.Comment selectedComment;
	private bool postScrollAreaCreated = false;
	private bool postUpdated = false;
	private bool newCommentCreated = false;

	private bool hasInitialized = false;

	//GameObject readPostScrollArea;
	//GameObject postScrollArea;
	GameObject sendButton;
	//GameObject messageFromText;
	//GameObject messageSubject;
	//GameObject messageContent;
	GameObject commentPost;
	GameObject commentSendButton;
	
	GameObject forumIndexGUI;
	GameObject readPostGUI;
	GameObject newPostGUI;
	GameObject newCommentGUI;
	GameObject titleInputField;
	GameObject messageInputField;

	private enum InternalState
	{
		displayContacts,
		displayPosts,
		contactContact,
		newComment,
		newPost,
		readPost 
	};

	private InternalState internalState;

	[System.Serializable]
	public class MyMessages
	{
		public string name;
		public string date;
		public string course;
		public string contact;
		public string email;
		public string phoneNumber;
	}
	
	WordPressPost MakePost(int currentNumber)
	{
		WordPressPost post = new WordPressPost();
		post.author = new WordPressPost.Author();
		post.author.name = "Matt Blackburn";
		post.author.id = 378;
		post.date = DateTime.Now;
		post.title = currentNumber.ToString () + " The post of posts";
		post.content = "A post full of strings full of characters.";
		
		post.comments = new WordPressPost.Comment[2];
		post.comments[0] = new WordPressPost.Comment();
		post.comments[0].author = new WordPressPost.Author();
		post.comments[0].author.name = "MDB";
		post.comments[0].content = "A comment commenting on a comet.";
		post.comments[0].date = DateTime.UtcNow;
		post.comments[0].id = 21;
		
		post.comments[1] = new WordPressPost.Comment();
		post.comments[1].author = new WordPressPost.Author();
		post.comments[1].author.name = "MDB";
		post.comments[1].content = "A comment commenting on a comet.";
		post.comments[1].date = DateTime.UtcNow;
		post.comments[1].id = 21;
		
		return post;
	}
	
	GameObject MakeComment(GameObject parent, float yOffset, WordPressPost.Comment comment)
	{
		//TODO: Expand to fit the height of content.
		GameObject commentBackgroundObj = MakeImage (parent, new Rect (0f, yOffset, rightPanelRect.width, rightPanelRect.height * 0.1f), greyBackground);
		commentBackgroundObj.name = "CommentBackground";
		GameObject commentObj = MakeLabel(parent, comment.author.name + ": " + comment.content, new Rect (rightPanelRect.width * 0.02f, yOffset, rightPanelRect.width * 0.99f, rightPanelRect.height * 0.1f), TextAnchor.MiddleLeft, "InnerLabel");
		commentObj.GetComponent<Text>().color = Color.white;
		
		commentObj.transform.SetParent (commentBackgroundObj.transform);
		
		return commentObj;
	}

	// Use this for initialization
	void Start ()
	{
		base.Init ();

		//rdr = new rssreader("http://192.168.1.210/wordpress/?feed=rss2");

		//closeIconImage = Resources.Load ("GUI/menuicon_logout") as Texture;

		//loginButtonImage = Resources.Load ("right_arrow") as Texture;
		//videoURLS = new List<VideoURL>();
		
		greyBackground = Resources.Load<Sprite> ("menu_bg");
		
		internalState = InternalState.displayPosts;
		StartCoroutine (MCP.GetForumPosts ("json=1&count=-1", "readAllPosts"));
		
		elementHeight = screenSize.height * 0.085f;
		//teamButtonsWidth = (leftPanelRect.width * 0.9f);
		//bottomPanelSpacer = rightPanelRect.height * 0.09f;
		
		// Temp forum posts.
		const int postNumber = 40;
		MCP.forumPosts = new WordPressPost[postNumber];
		for(int i = 0; i < postNumber; i++)
		{
			MCP.forumPosts[i] = MakePost(i);
		}

	}
	
	void SetSelectedPost(int postNumber)
	{
		Debug.Log ("SelectedPostSet: " + postNumber);
		selectedPost = MCP.forumPosts [postNumber];
	}
	public override void DoGUI ()
	{
		if (screenSize.width == 0 || Screen.width != screenSize.width)
		{
			screenSize = new Rect (0, 0, Screen.width, Screen.height);
			
			elementHeight = screenSize.height * 0.085f;
			//teamButtonsWidth = (leftPanelRect.width * 0.9f);
			//bottomPanelSpacer = rightPanelRect.height * 0.09f;
			if (!hasInitialized)
			{
				Debug.Log("Init here!");
				CreateGUI ();
				hasInitialized = true;
			}
		}

	}
	void CreateGUI()
	{
		topPanelRect = new Rect (screenSize.width * 0.11f, screenSize.height * 0.1375f, screenSize.width * 0.78f, screenSize.height * 0.8f);
		leftPanelRect = new Rect (screenSize.width * 0.075f, screenSize.height * 0.325f, screenSize.width * 0.16f, screenSize.height * 0.65f);
		rightPanelRect = new Rect (screenSize.width * 0.285f, screenSize.height * 0.175f, screenSize.width * 0.65f, screenSize.height * 0.75f);
		// Either make or get a reference to the canvas.
		GameObject localCanvas = MakeCanvas ();
		// Make the title bar.
		GameObject windowBack = MakeWindowBackground (localCanvas, new Rect(Screen.width * 0.01f, Screen.height * 0.01f , Screen.width * 0.98f, Screen.height * 0.98f), window_back);
		windowBack.name = "WindowBackground";
		
		GameObject titleBar = MakeTitleBar (localCanvas, MCP.Text (901)/*"Forum"*/);
		titleBar.name = "TitleBar";
		// Make the side gizmo.
		GameObject sideGizmo = MakeSideGizmo (localCanvas, true);
		sideGizmo.name = "SideGizmo";
		

		/* Make the main forum screen. */
		forumIndexGUI = new GameObject();
		forumIndexGUI.name = "ForumIndexGUI";
		forumIndexGUI.AddComponent<RectTransform>();
		forumIndexGUI.transform.SetParent (localCanvas.transform);
		
		GameObject inboxButtonBackground = MakeImage (localCanvas, new Rect(leftPanelRect.x, leftPanelRect.y, leftPanelRect.width * 0.8f, leftPanelRect.height * 0.08f), buttonBackground);
		inboxButtonBackground.name = "InboxButtonBackground";
		
		// Make inbox button.
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			delegate {
				internalState = InternalState.displayPosts;
			}
		};
		GameObject inboxButton = MakeButton (localCanvas, MCP.Text (902)/*"Inbox"*/, new Rect(leftPanelRect.x, leftPanelRect.y, leftPanelRect.width * 0.8f, leftPanelRect.height * 0.08f), funcList);
		inboxButton.name = "InboxButton";
		inboxButton.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
		inboxButton.GetComponent<Button>().image = inboxButtonBackground.GetComponent<Image>();
		//inboxButton.transform.parent = forumIndexGUI.transform;
		
		GameObject addPostButtonBackground = MakeImage (localCanvas, new Rect(leftPanelRect.x, leftPanelRect.y - leftPanelRect.height * 0.2f, leftPanelRect.width * 0.8f, leftPanelRect.height * 0.08f), buttonBackground);
		addPostButtonBackground.name = "AddPostButtonBackground";
		
		// Make add post button
		funcList = new List<MyVoidFunc>
		{
			delegate {
				internalState = InternalState.newPost;
			}
		};
		GameObject addPostButton = MakeButton (localCanvas, MCP.Text (903)/*"Add Post"*/, new Rect(leftPanelRect.x, leftPanelRect.y - leftPanelRect.height * 0.2f, leftPanelRect.width * 0.8f, leftPanelRect.height * 0.08f), funcList);
		addPostButton.name = "AddPostButton";
		//addPostButton.transform.parent = forumIndexGUI.transform;
		addPostButton.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
		addPostButton.GetComponent<Button>().image = addPostButtonBackground.GetComponent<Image>();
		
		// Make message area background.
		GameObject messageAreaBackground = MakeImage (localCanvas, new Rect(Screen.width * -0.25f, Screen.height * -0.325f, rightPanelRect.width, rightPanelRect.height), greyBackground);
		messageAreaBackground.name = "MessageAreaBackground";
		messageAreaBackground.transform.SetParent (forumIndexGUI.transform);
		// Make icon header background.
		GameObject iconHeaderBackground = MakeImage (localCanvas, new Rect((rightPanelRect.width * -0.385f), (rightPanelRect.height * -0.51f), rightPanelRect.width * 0.05f, rightPanelRect.height * 0.07f), greyBackground);
		iconHeaderBackground.name = "IconHeaderBackground";
		iconHeaderBackground.transform.SetParent (forumIndexGUI.transform);
		// Make date header background.
		GameObject dateHeaderBackground = MakeImage (localCanvas, new Rect((rightPanelRect.width * -0.32f), (rightPanelRect.height * -0.51f), rightPanelRect.width * 0.18f, rightPanelRect.height * 0.07f), greyBackground);
		dateHeaderBackground.name = "DateHeaderBackground";
		dateHeaderBackground.transform.SetParent (forumIndexGUI.transform);
		// Make date header.
		GameObject dateHeader = MakeLabel(localCanvas, MCP.Text (209)/*"Date"*/, new Rect((rightPanelRect.width * -0.32f), (rightPanelRect.height * -0.5f), rightPanelRect.width * 0.15f, rightPanelRect.height * 0.07f), TextAnchor.MiddleCenter, "InnerLabel");
		dateHeader.name = "DateHeader";
		dateHeader.transform.SetParent (forumIndexGUI.transform);
		// Make from header background.
		GameObject fromHeaderBackground = MakeImage (localCanvas, new Rect((rightPanelRect.width * -0.125f), (rightPanelRect.height * -0.51f), rightPanelRect.width * 0.2f, rightPanelRect.height * 0.07f), greyBackground);
		fromHeaderBackground.name = "FromHeaderBackground";
		fromHeaderBackground.transform.SetParent (forumIndexGUI.transform);
		// Make from header.
		GameObject fromHeader = MakeLabel(localCanvas, MCP.Text (210)/*"From"*/, new Rect((rightPanelRect.width * -0.14f), (rightPanelRect.height * -0.5f), rightPanelRect.width * 0.15f, rightPanelRect.height * 0.07f), TextAnchor.MiddleCenter, "InnerLabel");
		fromHeader.name = "FromHeader";
		fromHeader.transform.SetParent (forumIndexGUI.transform);
		// Make subject header backgrond.
		GameObject subjectHeaderBackground = MakeImage (localCanvas, new Rect((rightPanelRect.width * 0.085f), (rightPanelRect.height * -0.51f), rightPanelRect.width * 0.53f, rightPanelRect.height * 0.07f), greyBackground);
		subjectHeaderBackground.name = "SubjectHeaderBackground";
		subjectHeaderBackground.transform.SetParent (forumIndexGUI.transform);
		// Make subject header.
		GameObject subjectHeader = MakeLabel(localCanvas, MCP.Text (211)/*"Subject"*/, new Rect((rightPanelRect.width * 0.075f), (rightPanelRect.height * -0.5f), rightPanelRect.width * 0.17f, rightPanelRect.height * 0.07f), TextAnchor.MiddleCenter, "InnerLabel");
		subjectHeader.name = "SubjectHeader";
		subjectHeader.transform.SetParent (forumIndexGUI.transform);
		// Make message scroll area.
	//	postScrollArea = MakeScrollArea (localCanvas, new Rect (rightPanelRect.width * 0.01f, rightPanelRect.height * 0.10f, rightPanelRect.width * 0.905f, rightPanelRect.height * 0.875f), (GameObject)null);
	//	postScrollArea.name = "PostScrollArea";
	//	postScrollArea.transform.SetParent(forumIndexGUI.transform);
	//	postScrollArea.GetComponent<ObjectContent>().contentObject[0].GetComponent<RectTransform>().offsetMax = Vector2.zero;
		
		/* Make the add post screen. */
		newPostGUI = new GameObject();
		newPostGUI.name = "NewPostGUI";
		newPostGUI.AddComponent<RectTransform>();
		newPostGUI.transform.SetParent (localCanvas.transform);
		newPostGUI.transform.localPosition = new Vector3(Screen.width * -0.0f, Screen.height * -0.0f, 0);
		newPostGUI.transform.localScale = Vector2.one;
		newPostGUI.SetActive(false);
		
		GameObject titleLabelText = MakeLabel (newPostGUI, MCP.Text (212)/*"Title"*/, new Rect (topPanelRect.width * -0.4f, topPanelRect.height * -0.35f, topPanelRect.width * 0.2f, elementHeight), TextAnchor.MiddleRight, "InnerLabel");
		titleLabelText.name = "TitleText";
		
		titleInputField = MakeInputField (newPostGUI, postTitle, new Rect (topPanelRect.width * -0.15f, topPanelRect.height * -0.35f, topPanelRect.width * 0.7f, elementHeight), false, menuBackground);
		titleInputField.GetComponent<Text>().color = new Color(198f / 255f, 147f / 255f, 200f / 255f);
		
		GameObject messageLabelText = MakeLabel (newPostGUI, MCP.Text (904)/*"Message"*/, new Rect (topPanelRect.width * -0.4f, topPanelRect.height * -0.15f, topPanelRect.width * 0.2f, elementHeight), TextAnchor.MiddleRight, "InnerLabel");
		messageLabelText.name = "MessageText";
		
		messageInputField = MakeInputField (newPostGUI, postContent, new Rect (topPanelRect.width * -0.15f, topPanelRect.height * -0.15f, topPanelRect.width * 0.7f, elementHeight * 5f), false, menuBackground);
		messageInputField.GetComponent<Text>().color = new Color(198f / 255f, 147f / 255f, 200f / 255f);
		messageInputField.GetComponent<InputField>().lineType = InputField.LineType.MultiLineNewline;
		
		GameObject sendButtonBackground = MakeImage (newPostGUI, new Rect (topPanelRect.width * 0.25f, topPanelRect.height * 0.45f, topPanelRect.width * 0.3f, elementHeight), buttonBackground);
		sendButtonBackground.name = "SendButtonBackground";
		
		funcList = new List<MyVoidFunc>
		{
			SendForumRequestCreate, 
			PostPrivateMessage,
			GetForumPosts,
			delegate {
				internalState = InternalState.displayPosts;
			}
		};
		sendButton = MakeButton (newPostGUI, MCP.Text (213)/*"Send"*/, new Rect (topPanelRect.width * 0.25f, topPanelRect.height * 0.45f, topPanelRect.width * 0.3f, elementHeight), funcList, "InnerLabel");
		sendButton.name = "SendButton";
		sendButton.GetComponent<Text>().color = bmOrange;
		sendButton.GetComponent<Button>().image = sendButtonBackground.GetComponent<Image>();
		
		GameObject cancelButtonBackground = MakeImage (newPostGUI, new Rect (-topPanelRect.width * 0.15f, topPanelRect.height * 0.45f, topPanelRect.width * 0.2f, elementHeight), buttonBackground);
		cancelButtonBackground.name = "cancelButtonBackground";
		
		funcList = new List<MyVoidFunc>
		{
			delegate {
				internalState = InternalState.displayPosts;
				postTitle = "";
				postContent = "";
			}
		};
		GameObject cancelButton = MakeButton(newPostGUI, MCP.Text (207)/*"Cancel"*/, new Rect (-topPanelRect.width * 0.15f, topPanelRect.height * 0.45f, topPanelRect.width * 0.2f, elementHeight), funcList, "InnerLabel");
		cancelButton.name = "CancelButton";
		cancelButton.GetComponent<Text>().color = bmOrange;
		cancelButton.GetComponent<Button>().image = cancelButtonBackground.GetComponent<Image>();
		
		//GUI.DrawTexture (new Rect (topPanelRect.width - (topPanelRect.width * 0.175f), elementHeight * 8, elementHeight, elementHeight), closeIconImage, ScaleMode.ScaleToFit);
		//GUI.DrawTexture (new Rect (topPanelRect.width * 0.05f, elementHeight * 8, elementHeight * 0.8f, elementHeight * 0.8f), loginButtonImage, ScaleMode.ScaleToFit);
		
		
		/* Make the read post screen. */
		readPostGUI = new GameObject();
		readPostGUI.name = "ReadPostGUI";
		readPostGUI.AddComponent<RectTransform>();
		readPostGUI.transform.SetParent (localCanvas.transform);
		readPostGUI.transform.localPosition = Vector2.zero;
		readPostGUI.SetActive(false);
		
	//	readPostScrollArea = MakeScrollArea(readPostGUI, new Rect (rightPanelRect.width * 0.19f, rightPanelRect.height * -0.05f, rightPanelRect.width * 1.0f, rightPanelRect.height * 0.9f), (GameObject)null);
	//	readPostScrollArea.name = "ReadPostScrollArea";
	//	readPostScrollArea.transform.SetParent (readPostGUI.transform);
	//	readPostScrollArea.GetComponent<ObjectContent>().contentObject[0].GetComponent<RectTransform>().offsetMax = Vector2.zero;
	//	GameObject readPostScrollAreaChild = readPostScrollArea.GetComponent<ObjectContent>().contentObject[0];
		
	//	messageFromText = MakeLabel(readPostScrollAreaChild, new Rect (0f, rightPanelRect.height * 0.43f, rightPanelRect.width, rightPanelRect.height), "Message from: ", TextAnchor.MiddleLeft, "InnerLabel");
	//	messageFromText.name = "MessageFromText";
	//	messageFromText.transform.SetParent (readPostScrollAreaChild.transform);
	//	messageFromText.GetComponent<Text>().fontSize = 16;
	//	messageSubject = MakeLabel(readPostScrollAreaChild, new Rect (0f, rightPanelRect.height * 0.35f, rightPanelRect.width, elementHeight), "Subject: ", TextAnchor.MiddleLeft, "InnerLabel");
	//	messageSubject.name = "MessageSubject";
	//	messageSubject.transform.SetParent (readPostScrollAreaChild.transform);
	//	messageSubject.GetComponent<Text>().fontSize = 16;
		
	//	GameObject messageContentLabel = MakeLabel(readPostScrollAreaChild, new Rect (0f, rightPanelRect.height * 0.27f, rightPanelRect.width, elementHeight), "Message:", TextAnchor.MiddleLeft, "InnerLabel");
	//	messageContentLabel.name = "MessageContentLabel";
	//	messageContentLabel.transform.SetParent (readPostScrollAreaChild.transform);
	//	messageContentLabel.GetComponent<Text>().fontSize = 16;
		
	//	GameObject messageContentBackground = MakeImage (readPostScrollAreaChild, new Rect (0f, rightPanelRect.height * 0.22f, rightPanelRect.width, elementHeight * 4.5f), menuBackground); 
	//	messageContentBackground.name = "MessageContentBackground";
	//	messageContentBackground.transform.SetParent (readPostScrollAreaChild.transform);
	//	messageContentBackground.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 1);
		
	//	messageContent = MakeLabel (readPostScrollAreaChild, new Rect (rightPanelRect.width * 0.02f, rightPanelRect.height * 0.22f, rightPanelRect.width * 0.99f, elementHeight * 4.5f), "", TextAnchor.UpperLeft, "InnerLabel");
	//	messageContent.name = "MessageContent";
	//	messageContent.transform.SetParent (readPostScrollAreaChild.transform);
	//	messageContent.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 1);
	//	messageContent.GetComponent<Text>().color = Color.white;
		
		// Handle message functionality
		funcList = new List<MyVoidFunc>
		{
			delegate{
				WordPressPost.Comment comment = new WordPressPost.Comment ();
				comment.id = 0;
				comment.content = selectedPost.content;
				selectedComment = comment;
				internalState = InternalState.newComment;
			}
		};
		
	//	GameObject acceptButton = MakeButton (readPostScrollAreaChild, "Comment", new Rect (rightPanelRect.width * 0.25f, rightPanelRect.height * -0.39f, rightPanelRect.width * 0.2f, elementHeight), funcList, "InnerLabel");
	//	acceptButton.name = "AcceptButton";
	//	acceptButton.transform.SetParent (readPostScrollAreaChild.transform);
		
		funcList = new List<MyVoidFunc>
		{
			SendForumRequestDelete,
			delegate{
				internalState = InternalState.displayPosts;
			}
		};
	//	GameObject declineButton = MakeButton (readPostScrollAreaChild, "Delete", new Rect (rightPanelRect.width * 0.05f, rightPanelRect.height * -0.39f, rightPanelRect.width * 0.2f, elementHeight), funcList, "InnerLabel");
	//	declineButton.name = "DeclineButton";
	//	declineButton.transform.SetParent (readPostScrollAreaChild.transform);
		
		funcList = new List<MyVoidFunc>
		{
			delegate{
				internalState = InternalState.displayPosts;
			}
		};
	//	GameObject readCancelButton = MakeButton (readPostScrollAreaChild, "Back", new Rect (rightPanelRect.width * -0.25f, rightPanelRect.height * -0.39f, rightPanelRect.width * 0.135f, elementHeight), funcList, "InnerLabel");
	//	readCancelButton.name = "ReadCancelButton";
	//	readCancelButton.transform.SetParent (readPostScrollAreaChild.transform);
		
		
		/* Create new comment gui objects. */
		newCommentGUI = new GameObject();
		newCommentGUI.name = "newCommentGUI";
		newCommentGUI.AddComponent<RectTransform>();
		newCommentGUI.transform.SetParent (localCanvas.transform);
		newCommentGUI.transform.localPosition = Vector2.zero;
		newCommentGUI.SetActive(false);
		
		// Make post to reply to.
		//TODO: Make this update in update func.
		GameObject commentPostBackground = MakeImage (newCommentGUI, new Rect(rightPanelRect.x, rightPanelRect.height * 0.35f, rightPanelRect.width, rightPanelRect.height * 0.1f), greyBackground);
		commentPostBackground.name = "CommentPostBackground";
		
		commentPost = MakeLabel (newCommentGUI, "", new Rect(rightPanelRect.x, rightPanelRect.height * 0.35f, rightPanelRect.width * 0.99f, rightPanelRect.height * 0.1f), TextAnchor.MiddleLeft, "InnerLabel");
		commentPost.name = "CommentPost";
		commentPost.GetComponent<Text>().color = Color.white;
		
		// Make textbox for comment.
		//TODO:
		
		// Make send button.
		funcList = new List<MyVoidFunc>
		{
			// Process Login
			
			//StartCoroutine (MCP.SendForumRequest ("json=respond/submit_comment&parent=" + selectedComment.id + "&content=" + ReplaceSpaces (commentContent) + "&name=" + ReplaceSpaces (MCP.userInfo.username) + "&post_id=" + selectedPost.id, "controller=respond&method=submit_comment", "addComment"));
			////http://127.0.0.1/wordpress/?nonce=1fd82bb28b&json=submit_comment&post_id=28&name=riso&content=ttttghgpgg&parent=11
			
			//StartCoroutine (MCP.PostPrivateMessage (selectedMessage.username, MCP.userInfo.username, postTitle, postContent));
			//StartCoroutine (MCP.GetForumPosts ("json=1&count=-1", "readAllPosts"));
			delegate{
				
				internalState = InternalState.displayPosts;
			}
		};
		commentSendButton = MakeButton (newCommentGUI, MCP.Text (213)/*"Send"*/, new Rect (topPanelRect.width * 0.3f, rightPanelRect.height * -0.4f, topPanelRect.width * 0.3f, elementHeight), funcList, "InnerLabel");
		commentSendButton.name = "CommentSendButton";
		
		// Make cancel button.
		funcList = new List<MyVoidFunc>
		{
			delegate{
				commentContent = "";
				internalState = InternalState.readPost;
			}
		};
		GameObject commentCancelButton = MakeButton (newCommentGUI, MCP.Text (207)/*"Cancel"*/, new Rect (topPanelRect.width * -0.1f, rightPanelRect.height * -0.4f, topPanelRect.width * 0.135f, elementHeight), funcList, "InnerLabel");
		commentCancelButton.name = "CommentCancelButton";
	}
	
	void MakeMessageButton(float yOffset, int postNumber)
	{
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			delegate {
				internalState = InternalState.readPost;
			}
		};
		
		GameObject postButton = MakeImageButton (forumIndexGUI, (Sprite)null, new Rect (rightPanelRect.x + (rightPanelRect.width * 0.83f), yOffset, rightPanelRect.width * 0.97f, rightPanelRect.width * 0.05f), funcList);
		postButton.name = "PostButton";
		//postButton.transform.SetParent (postScrollArea.GetComponent<ObjectContent>().contentObject[0].transform);
		ColorBlock cb = postButton.GetComponent<Button>().colors;
		cb.colorMultiplier = 0;
		postButton.GetComponent<Button>().colors = cb;
		
		postButton.GetComponent<Button>().onClick.AddListener(
			delegate {
				Debug.Log ("SelectedPostSet: " + postNumber);
				selectedPost = MCP.forumPosts [postNumber];
			} );
		
	}
	
	void GetForumPosts()
	{
		StartCoroutine (MCP.GetForumPosts ("json=1&count=-1", "readAllPosts"));
	}
	
	void PostPrivateMessage()
	{
		//StartCoroutine (MCP.PostPrivateMessage (selectedMessage.username, MCP.userInfo.username, postTitle, postContent));
	}
	
	void SendForumRequestCreate()
	{
		StartCoroutine (MCP.SendForumRequest ("json=posts/create_post&status=publish&title=" + postTitle + "&content=" + postContent + "&author=" + MCP.userInfo.username, "controller=posts&method=create_post", "addNewPost"));
	}
	
	void SendForumRequestDelete()
	{
		StartCoroutine (MCP.SendForumRequest ("json=posts/delete_post&id=" + selectedPost.id, "controller=posts&method=delete_post", "deletePost"));
	}
	
	void GetFriendInfo()
	{
		StartCoroutine (MCP.GetFriendInfo (selectedPost.author.name));
	}
	
	void UpdatePostContentStrings()
	{
		postTitle = titleInputField.GetComponent<Text>().text;
		postContent = messageInputField.GetComponent<Text>().text;
	}
	
	// Update is called once per frame
	void Update ()
	{
		base.UpdateScreen ();
		//If the 'next' boolean is true
		if(hasInitialized)
		switch(internalState)
		{
		case InternalState.displayPosts:
			forumIndexGUI.SetActive (true);
			newPostGUI.SetActive(false);
			readPostGUI.SetActive (false);
			newCommentGUI.SetActive (false);
			
			//if (MCP.userInfo != null)
			{	
				if (MCP.friendsListNeedsUpdating && !hasTripped)
				{
					hasTripped = true;
//					StartCoroutine (MCP.AcceptFriend ());
				}
				
				float offset = rightPanelRect.height * 0.58f;
				
				if(!postScrollAreaCreated)
				{
					if (MCP.forumPosts != null)
					{
						//List<GameObject> postButton = new List<GameObject>();
						
						for (int i = 0; i < MCP.forumPosts.Length; i++)
						{
							WordPressPost post = MCP.forumPosts [i];
							GameObject postDateBackground = MakeImage (forumIndexGUI, new Rect (rightPanelRect.x + (rightPanelRect.width * 0.5125f), offset, rightPanelRect.width * 0.17f, rightPanelRect.width * 0.05f), greyBackground);
							postDateBackground.name = "PostDateBackground";
							//postDateBackground.transform.SetParent (postScrollArea.GetComponent<ObjectContent>().contentObject[0].transform);
							GameObject postDate = MakeLabel (forumIndexGUI, post.date.ToShortDateString(), new Rect (rightPanelRect.x + (rightPanelRect.width * 0.5125f), offset, rightPanelRect.width * 0.17f, rightPanelRect.width * 0.05f), TextAnchor.MiddleCenter, "SmallText");
							postDate.name = "PostDate";
							postDate.GetComponent<Text>().color = Color.white;
							postDate.GetComponent<Text>().fontSize = 16;
							//postDate.transform.SetParent (postScrollArea.GetComponent<ObjectContent>().contentObject[0].transform);
						
							GameObject postAuthorNameBackground = MakeImage (forumIndexGUI, new Rect (rightPanelRect.x + (rightPanelRect.width * 0.6925f), offset, rightPanelRect.width * 0.17f, rightPanelRect.width * 0.05f), greyBackground);
							postAuthorNameBackground.name = "PostAuthorNameBackground";
							//postAuthorNameBackground.transform.SetParent (postScrollArea.GetComponent<ObjectContent>().contentObject[0].transform);
							GameObject postAuthorName = MakeLabel (forumIndexGUI, post.author.name, new Rect (rightPanelRect.x + (rightPanelRect.width * 0.6925f), offset, rightPanelRect.width * 0.17f, rightPanelRect.width * 0.05f), TextAnchor.MiddleCenter, "SmallText");
							postAuthorName.name = "PostAuthorName";
							postAuthorName.GetComponent<Text>().color = Color.white;
							postAuthorName.GetComponent<Text>().fontSize = 16;
							//postAuthorName.transform.SetParent (postScrollArea.GetComponent<ObjectContent>().contentObject[0].transform);
							
							GameObject postTitleBackground = MakeImage (forumIndexGUI, new Rect (rightPanelRect.x + (rightPanelRect.width * 1.054f), offset, rightPanelRect.width * 0.5305f, rightPanelRect.width * 0.05f), greyBackground);
							postTitleBackground.name = "PostTitleBackground";
							//postTitleBackground.transform.SetParent (postScrollArea.GetComponent<ObjectContent>().contentObject[0].transform);
							GameObject postTitleObject = MakeLabel(forumIndexGUI, post.title, new Rect (rightPanelRect.x + (rightPanelRect.width * 1.054f), offset, rightPanelRect.width * 0.5305f, rightPanelRect.width * 0.05f), TextAnchor.MiddleLeft, "SmallText");
							postTitleObject.name = "PostTitle";
							postTitleObject.GetComponent<Text>().color = Color.white;
							postTitleObject.GetComponent<Text>().fontSize = 16;
							//postTitleObject.transform.SetParent (postScrollArea.GetComponent<ObjectContent>().contentObject[0].transform);
							
							MakeMessageButton (offset, i);
							
							offset -= rightPanelRect.width * 0.06f;
						}
						
						//postScrollArea.GetComponent<ObjectContent>().ResizeContent (false, true);
						postScrollAreaCreated = true;
					}
				}
			}
			
			break;
		case InternalState.newPost:
			forumIndexGUI.SetActive (false);
			newPostGUI.SetActive(true);
			readPostGUI.SetActive (false);
			newCommentGUI.SetActive (false);
			UpdatePostContentStrings();
			
			//TODO:
			//postTitle = GUI.TextField (new Rect ((topPanelRect.width * 0.05f) + (GUI.skin.GetStyle ("Label").CalcSize (new GUIContent ("TiTle")).x) + (topPanelRect.width * 0.015f), 0, (topPanelRect.width * 0.9f) - (GUI.skin.GetStyle ("Label").CalcSize (new GUIContent ("Title:")).x) - (topPanelRect.width * 0.015f), elementHeight), postTitle);
			
			//TODO:
			//postContent = GUI.TextField (new Rect (topPanelRect.width * 0.05f, elementHeight * 1.5f, topPanelRect.width * 0.9f, elementHeight * 6f), postContent);
			
			if (postTitle != null && postTitle != "" && postContent != null && postContent != "")
			{
				sendButton.GetComponent<Button>().interactable = true;
			}
			else
			{
				sendButton.GetComponent<Button>().interactable = false;
			}
			break;
			
		case InternalState.readPost:
			forumIndexGUI.SetActive (false);
			newPostGUI.SetActive(false);
			readPostGUI.SetActive (true);
			newCommentGUI.SetActive (false);
			
			if(!postUpdated)
			{
				//messageFromText.GetComponent<Text>().text = "Message from: " + selectedPost.author.name;
				//messageSubject.GetComponent<Text>().text = "Subject: " + selectedPost.title;
				//messageContent.GetComponent<Text>().text = selectedPost.content;
				
				List<WordPressPost.Comment> commentList = new List<WordPressPost.Comment> ();
				commentList.AddRange (selectedPost.comments);
				selectedPost.comments = SortComments (commentList, 0).ToArray ();
				
				//float commentOffset = rightPanelRect.height * -0.55f;
				
				//foreach (WordPressPost.Comment comment in selectedPost.comments)
				{
					//GameObject commentObj = MakeComment(readPostScrollArea.GetComponent<ObjectContent>().contentObject[0], commentOffset, comment);
					//commentObj.name = "CommentObj";
					
					//commentOffset -= topPanelRect.height * 0.1f;
				}
				
				//readPostScrollArea.GetComponent<ObjectContent>().ResizeContent (false, true);
				postUpdated = true;
			}
			
			break;
			
		case InternalState.newComment:
			forumIndexGUI.SetActive (false);
			newPostGUI.SetActive(false);
			readPostGUI.SetActive (false);
			newCommentGUI.SetActive (true);
			
			if(!newCommentCreated)
			{
				commentPost.GetComponent<Text>().text = selectedComment.content;
				
				if (commentContent != null && commentContent != "")
				{
					// Button is active.
					commentSendButton.GetComponent<Button>().interactable = true;
				}
				else
				{
					// Button is inactive
					commentSendButton.GetComponent<Button>().interactable = false;
				}
			}
			
			break;
		}
	}

/*
	public void DoGUI ()
	{

		switch (internalState) {
			
			case InternalState.readPost:
					
					scrollPosition2 = GUI.BeginScrollView (topPanelRect, scrollPosition2, new Rect (0, 0, topPanelRect.width * 0.95f, topPanelRect.height * 2), false, false);
		
					GUI.Label (new Rect (topPanelRect.width * 0.05f, 0, topPanelRect.width * 0.45f, elementHeight), "Message from: " + selectedPost.author.name);
					
					GUI.Label (new Rect (topPanelRect.width * 0.05f, elementHeight * 1f, topPanelRect.width * 0.45f, elementHeight), "Subject:");
		
					GUI.Label (new Rect ((topPanelRect.width * 0.05f) + (GUI.skin.GetStyle ("Label").CalcSize (new GUIContent ("Subject:")).x) + (topPanelRect.width * 0.015f), elementHeight * 1f, (topPanelRect.width * 0.9f) 
							- (GUI.skin.GetStyle ("Label").CalcSize (new GUIContent ("Subject:")).x) - (topPanelRect.width * 0.015f), elementHeight), selectedPost.title, "PopOutGizmo_Header");
		
					GUI.Label (new Rect (topPanelRect.width * 0.05f, elementHeight * 2f, topPanelRect.width * 0.9f, elementHeight), "Message:");
					GUI.Label (new Rect (topPanelRect.width * 0.05f, elementHeight * 3f, topPanelRect.width * 0.9f, elementHeight * 4.5f), selectedPost.content, "PopOutGizmo_Header");
		
					List<WordPressPost.Comment> commentList = new List<WordPressPost.Comment> ();
					commentList.AddRange (selectedPost.comments);
					selectedPost.comments = SortComments (commentList, 0).ToArray ();
					
					float commentOffset = elementHeight * 7.5f;
					float xOffest = 40;
		
					foreach (WordPressPost.Comment comment in selectedPost.comments) {
							WordPressPost.Comment pointer = comment;
			
							xOffest = 0;
							while (pointer.parent!=0) {
									pointer = commentList.Find (x => x.id == pointer.parent);
									xOffest += 40;
							}
							if (GUI.Button (new Rect (topPanelRect.width * 0.05f + xOffest - 40, commentOffset + topPanelRect.height * 0.05f, 40, topPanelRect.height * 0.1f), "R", GUI.skin.GetStyle ("RedButton"))) {
									selectedComment = comment;
									internalState = InternalState.newComment;
							}
							GUI.Label (new Rect (topPanelRect.width * 0.05f + xOffest, commentOffset, topPanelRect.width * 0.9f, topPanelRect.height * 0.2f), comment.name + ": " + comment.content);
			
							commentOffset += topPanelRect.height * 0.1f;
					}
		        
		
		
		// Handle message functionality
		
					if (int.Parse (MCP.zingMessages.messages [selectedPostIndex].type) == 3 && int.Parse (MCP.zingMessages.messages [selectedPostIndex].hasTripped) == 0) {
							if (DoButton (new Rect ((topPanelRect.width * 0.05f) + (elementHeight * 0.8f), elementHeight * 8, topPanelRect.width * 0.3f, elementHeight), "Accept", "RedButtonLeft") ||
									DoButton (new Rect ((topPanelRect.width * 0.05f), elementHeight * 8, elementHeight * 0.85f, elementHeight), "", "RedButtonLeft")) {
									hasTripped = false;
									StartCoroutine (MCP.GetFriendInfo (MCP.zingMessages.messages [selectedPostIndex].from));
									StartCoroutine (MCP.MarkMessageAsTripped (MCP.zingMessages.messages [selectedPostIndex].id));
									MCP.zingMessages.messages [selectedPostIndex].hasTripped = "1";
									internalState = InternalState.displayPosts;
				
							}
							GUI.DrawTexture (new Rect (topPanelRect.width * 0.05f, elementHeight * 8, elementHeight * 0.8f, elementHeight * 0.8f), loginButtonImage, ScaleMode.ScaleToFit);
							if (DoButton (new Rect (topPanelRect.width * 0.35f, elementHeight * 8, topPanelRect.width * 0.3f, elementHeight), "Decline", "BlueButton")) {
				
									// Process request declined
				
							}
					} else if (int.Parse (MCP.zingMessages.messages [selectedPostIndex].hasTripped) == 1) {
			
							GUI.DrawTexture (new Rect (topPanelRect.width * 0.05f, elementHeight * 8, elementHeight * 0.8f, elementHeight * 0.8f), loginButtonImage, ScaleMode.ScaleToFit);
							if (DoButton (new Rect (topPanelRect.width * 0.35f, elementHeight * 8, topPanelRect.width * 0.3f, elementHeight), "Delete", "BlueButton")) {
				
									internalState = InternalState.displayPosts;
				
							}
					} else {
							if (DoButton (new Rect ((topPanelRect.width * 0.05f) + (elementHeight * 0.8f), elementHeight * 8, topPanelRect.width * 0.3f, elementHeight), "Reply", "RedButtonLeft") ||
									DoButton (new Rect ((topPanelRect.width * 0.05f), elementHeight * 8, elementHeight * 0.85f, elementHeight), "", "RedButtonLeft")) {
				
									internalState = InternalState.newPost;
				
							}
							GUI.DrawTexture (new Rect (topPanelRect.width * 0.05f, elementHeight * 8, elementHeight * 0.8f, elementHeight * 0.8f), loginButtonImage, ScaleMode.ScaleToFit);
							if (DoButton (new Rect (topPanelRect.width * 0.35f, elementHeight * 8, topPanelRect.width * 0.3f, elementHeight), "Delete", "BlueButton")) {
				
									internalState = InternalState.displayPosts;
				
							}
					}
					
					if (DoButton (new Rect (topPanelRect.width - (topPanelRect.width * 0.175f), elementHeight * 8, topPanelRect.width * 0.135f, elementHeight), "Cancel", "BlueButton")) {
							internalState = InternalState.displayPosts;
					}
					if (DoButton (new Rect (topPanelRect.width - (topPanelRect.width * 0.320f), elementHeight * 8, topPanelRect.width * 0.135f, elementHeight), "Reply", "RedButton")) {
							WordPressPost.Comment comment = new WordPressPost.Comment ();
							comment.id = 0;
							comment.content = selectedPost.content;
							selectedComment = comment;
							internalState = InternalState.newComment;
					}
					if (DoButton (new Rect (topPanelRect.width - (topPanelRect.width * 0.470f), elementHeight * 8, topPanelRect.width * 0.135f, elementHeight), "Delete", "RedButton")) {
			
							StartCoroutine (MCP.SendForumRequest ("json=posts/delete_post&id=" + selectedPost.id, "controller=posts&method=delete_post", "deletePost"));
			
					}
					GUI.DrawTexture (new Rect (topPanelRect.width - (topPanelRect.width * 0.175f), elementHeight * 8, elementHeight, elementHeight), closeIconImage, ScaleMode.ScaleToFit);
		
					GUI.EndScrollView ();
					//GUI.EndGroup ();
					break;
		
			}
	
	
	}
*/

/*
	void 	DoSideMenu ()
	{
			GUI.BeginGroup (leftPanelRect);
			// Top Row
	
	
			if (internalState == InternalState.contactContact) {
		if (DoButton (new Rect ((leftPanelRect.width * 0.5f) - (teamButtonsWidth * 0.5f), 0, teamButtonsWidth, bottomPanelSpacer), "Friends List", "SelectedButton")) {
			
		}
	} else {
		if (DoButton (new Rect ((leftPanelRect.width * 0.5f) - (teamButtonsWidth * 0.5f), 0, teamButtonsWidth, bottomPanelSpacer), "Friends List", "RedButton")) {
			
			
			LoadMenu ("MyFriends_Screen");
		}
	}
	
			if (internalState == InternalState.displayPosts) {
					if (DoButton (new Rect ((leftPanelRect.width * 0.5f) - (teamButtonsWidth * 0.5f), bottomPanelSpacer * 1.5f, teamButtonsWidth, bottomPanelSpacer), "Inbox", "SelectedButton")) {
			
					}
			} else {
					if (DoButton (new Rect ((leftPanelRect.width * 0.5f) - (teamButtonsWidth * 0.5f), bottomPanelSpacer * 1.5f, teamButtonsWidth, bottomPanelSpacer), "Inbox", "RedButton")) {
			
			
							internalState = InternalState.displayPosts;
							//StartCoroutine (MCP.GetForumPosts ("json=1&count=-1", "readAllPosts"));
							//scrollHeight = 0;
			
					}
			}
	
			if (internalState == InternalState.readPost) {
		if (DoButton (new Rect ((leftPanelRect.width * 0.5f) - (teamButtonsWidth * 0.5f), bottomPanelSpacer * 3f, teamButtonsWidth, bottomPanelSpacer), "Sent", "SelectedButton")) {
			
		}
	} else {
		if (DoButton (new Rect ((leftPanelRect.width * 0.5f) - (teamButtonsWidth * 0.5f), bottomPanelSpacer * 3f, teamButtonsWidth, bottomPanelSpacer), "Sent", "RedButton")) {
			
			
			//scrollHeight = 0;
			
			internalState = InternalState.readPost;
		}
	}
	
	if (internalState == InternalState.readPost) {
		if (DoButton (new Rect ((leftPanelRect.width * 0.5f) - (teamButtonsWidth * 0.5f), bottomPanelSpacer * 4.5f, teamButtonsWidth, bottomPanelSpacer), "Deleted", "SelectedButton")) {
			
		}
	} else {
		if (DoButton (new Rect ((leftPanelRect.width * 0.5f) - (teamButtonsWidth * 0.5f), bottomPanelSpacer * 4.5f, teamButtonsWidth, bottomPanelSpacer), "Deleted", "RedButton")) {
			//LoadMenu("Glossary_Screen");
		}
	}
	
			if (internalState == InternalState.newPost) {
					if (DoButton (new Rect ((leftPanelRect.width * 0.5f) - (teamButtonsWidth * 0.5f), bottomPanelSpacer * 6f, teamButtonsWidth, bottomPanelSpacer), "Add Post", "SelectedButton")) {
			
					}
			} else {
					if (DoButton (new Rect ((leftPanelRect.width * 0.5f) - (teamButtonsWidth * 0.5f), bottomPanelSpacer * 6f, teamButtonsWidth, bottomPanelSpacer), "Add Post", "RedButton")) {
							//LoadMenu("Glossary_Screen");
							internalState = InternalState.newPost;
					}
			}
	
	
			GUI.EndGroup ();
	}
*/
	
	public void ServerResponseReceived (string actionName, string status)
	{
			if (status == "ok") {
					Debug.Log (actionName + ": ok");
					switch (actionName) {
					case "addNewPost":
							StartCoroutine (MCP.GetForumPosts ("json=1&count=-1", "readAllPostsAndResetForm"));
							break;
					case "addComment":				
							StartCoroutine (MCP.GetForumPosts ("json=1&count=-1", "updateSelectedPost"));
							break;
					case "updateSelectedPost":
							bool found = false;
							foreach (WordPressPost post in MCP.forumPosts) {
									if (post.id == selectedPost.id) {
											selectedPost = post;
											found = true;
											break;
									}
							}
							if (!found) 
									internalState = InternalState.displayPosts;
							else
									internalState = InternalState.readPost;
							commentContent = "";
							break;
					case "deletePost":
							StartCoroutine (MCP.GetForumPosts ("json=1&count=-1", "readAllPosts"));
							break;
					case "readAllPosts":
							internalState = InternalState.displayPosts;
							break;
					case "readAllPostsAndResetForm":
							internalState = InternalState.displayPosts;
							postTitle = "";
							postContent = "";
							break;
					}
			} else
					Debug.Log (actionName + ": " + status);
	}
	
	private List<WordPressPost.Comment> SortComments (List<WordPressPost.Comment> list, int id)
	{
			List<WordPressPost.Comment> returnList = new List<WordPressPost.Comment> ();
			List<WordPressPost.Comment> childList = list.FindAll (x => x.parent == id);
			if (childList.Count == 0)
					return childList;
			foreach (WordPressPost.Comment c in childList) {
					List<WordPressPost.Comment> grandChildList = SortComments (list, c.id);
					if (grandChildList.Count > 0) {
							c.latestChildDate = grandChildList [0].latestChildDate;
					} else {
							c.latestChildDate = c.date;
					}
			}
	
			childList.Sort (delegate(WordPressPost.Comment c1, WordPressPost.Comment c2) {
					return(c2.latestChildDate.CompareTo (c1.latestChildDate));
			});
			
			foreach (WordPressPost.Comment c in childList) {
					returnList.Add (c);
					List<WordPressPost.Comment> grandChildList = SortComments (list, c.id);
					if (grandChildList.Count > 0) {
							returnList.AddRange (grandChildList);
					}
			}
	
			return returnList;
	}
	
	private string ReplaceSpaces (string s)
	{
			return s.Replace (" ", "%20");
	}
}