using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class FAQScreen : MenuScreen
{
	private GameObject localCanvas;
	private string helpString;	//TODO: Set this from file.
	
	// Use this for initialization
	void Start ()
	{
		// Initialise the base.
		Init ();
		
		helpString = MCP.Text (208);/*"Coming soon...";*/
		
		// Create the scene GUI.
		CreateGUI ();
		
		localCanvas.transform.localScale = Vector2.zero;
	}
	
	void CreateGUI()
	{
		// Find the current canvas.
		localCanvas = MakeCanvas ();
		
		MakeTitleBar (localCanvas, MCP.Text (801)/*"FAQS"*/);
		
		MakeSideGizmo (localCanvas, true);
		
		// Make backing panel
		MakeImage (localCanvas, new Rect(Screen.width * 0.1f, Screen.height * 0.15f, Screen.width * 0.8f, Screen.height * 0.75f), window_back, true);
		
		// Make background for button area
		MakeImage (localCanvas, new Rect(Screen.width * 0.65f, Screen.height * 0.15f, Screen.width * 0.25f, Screen.height * 0.75f), menuBackground);
		
		GameObject helpButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.675f, Screen.height * 0.2f, Screen.width * 0.2f, Screen.height * 0.08f), buttonBackground, true);
		helpButtonBackground.name = "HelpButtonBackground";
		// Make help button
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
		
		};
		GameObject helpButton = MakeButton (localCanvas, MCP.Text (802)/*"Help"*/, new Rect(Screen.width * 0.675f, Screen.height * 0.2f, Screen.width * 0.2f, Screen.height * 0.08f), funcList, "InnerLabel");
		helpButton.name = "HelpButton";
		helpButton.GetComponent<Button>().image = helpButtonBackground.GetComponent<Image>();
		helpButton.GetComponent<Text>().color = bmOrange;
		
		GameObject glossaryButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.675f, Screen.height * 0.325f, Screen.width * 0.2f, Screen.height * 0.08f), buttonBackground, true);
		glossaryButtonBackground.name = "GlossaryButtonBackground";
		// Make glossary button
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				LoadMenu ("Glossary_Screen");
			}
		};
		GameObject glossaryButton = MakeButton (localCanvas, MCP.Text (803)/*"Glossary"*/, new Rect(Screen.width * 0.675f, Screen.height * 0.325f, Screen.width * 0.2f, Screen.height * 0.08f), funcList, "InnerLabel");
		glossaryButton.name = "GlossaryButton";
		glossaryButton.GetComponent<Button>().image = glossaryButtonBackground.GetComponent<Image>();
		glossaryButton.GetComponent<Text>().color = bmOrange;
		
		// Make help text
		GUIStyle guiStyle = new GUIStyle();
		guiStyle.alignment = TextAnchor.UpperLeft;
		guiStyle.font = defaultFont;
		guiStyle.fontSize = (int)_fontSize;
		guiStyle.fontStyle = FontStyle.Normal;
		guiStyle.wordWrap = true;
		
		float helpTextHeight = guiStyle.CalcHeight (new GUIContent(helpString), Screen.width * 0.49f);
		if(helpTextHeight < Screen.height * 0.65f)
		{
			helpTextHeight = Screen.height * 0.65f;
		}
		
		GameObject helpText = MakeLabel (localCanvas, helpString, new Rect(Screen.width * 0.13f, Screen.height * 0.18f, Screen.width * 0.49f, helpTextHeight));
		helpText.name = "HelpText";
		
		// Make a scroll rect for the help text
		GameObject helpScrollArea = MakeScrollRect (localCanvas, new Rect(Screen.width * 0.12f, Screen.height * 0.135f, Screen.width * 0.5f, Screen.height * 0.7f), null);
		helpScrollArea.name = "HelpScrollArea";
		helpScrollArea.GetComponent<ScrollRect>().horizontal = false;
		
		helpText.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
		helpText.GetComponent<Text>().resizeTextForBestFit = false;
		helpText.GetComponent<Text>().fontSize = (int)_fontSize;
		helpText.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
		helpText.transform.SetParent (helpScrollArea.transform.GetChild (0), true);
		helpText.transform.localScale = Vector3.one;
		helpScrollArea.GetComponent<ScrollRect>().content = helpText.GetComponent<RectTransform>();
	}
	
	public override void DoGUI (){}
}
