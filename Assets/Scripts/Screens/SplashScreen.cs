﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SplashScreen : MenuScreen
{
	private const int screenWidth = 1024, screenHeight = 576;
	
	private GameObject localCanvas;
	private GameObject logo;
	private GameObject zingLogoObject;
	
	private Sprite splashLogo;
	private Sprite zingLogo;
	
	private float fadeSpeed = 0.025f;
	private float idleTime = 1f;
	private float currentIdleTime = 0;
	private Rect screenSize;
	
	enum InternalState
	{
		FadeIn,
		Idle,
		FadeOut,
		ChangeScene
	};
	
	InternalState internalState = InternalState.FadeIn;
	
	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
#if UNITY_STANDALONE
		Screen.SetResolution( screenWidth, screenHeight, false );
#endif
		Application.runInBackground = true;
		
		if(Debug.isDebugBuild)
		{
//			Caching.CleanCache ();
		}
		
		// Initialise the base.
		Init ();
		
		// Load the splash logo.
		splashLogo = Resources.Load<Sprite>("bm_logo_words");
		zingLogo = Resources.Load<Sprite>("poweredbyZing");
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Find the current canvas.
		localCanvas = MakeCanvas (true);
		
		// Create the logo.
		logo = MakeImage (localCanvas, new Rect(screenSize.width * 0.375f, screenSize.height * 0.1f, screenSize.width * 0.25f, screenSize.height * 0.55f), splashLogo);
		logo.name = "Logo";
		// Set the initial logo alpha to 0.
		Color c = logo.GetComponent<Image>().color;
		c.a = 0;
		logo.GetComponent<Image>().color = c;
		
		// Create the logo.
		zingLogoObject = MakeImage (localCanvas, new Rect(screenSize.width * 0.85f, screenSize.height * 0.01f, screenSize.width * 0.1f, screenSize.width * 0.1f), zingLogo);
		zingLogoObject.name = "ZingLogo";
		zingLogoObject.GetComponent<Image>().color = c;
	}
	
	/// <summary>
	/// Fade the logo.
	/// </summary>
	/// <returns>
	/// Whether the fade is complete.
	/// </returns>
	bool Fade()
	{
		// Get the current logo.
		float currentFade = logo.GetComponent<Image>().color.a;
		
		switch(internalState)
		{
			case InternalState.FadeIn:
				// Add to the current fade value.
				currentFade += fadeSpeed;
				// If the logo is fully faded in, then limit the alpha to maximum, and return true.
				if(currentFade > 1f)
				{
					Color c = logo.GetComponent<Image>().color;
					c.a = 1f;
					logo.GetComponent<Image>().color = c;
					zingLogoObject.GetComponent<Image>().color = c;
					return true;
				}
				// Otherwise, set the alpha to the current fade value.
				else
				{
					Color c = logo.GetComponent<Image>().color;
					c.a = currentFade;
					logo.GetComponent<Image>().color = c;
					zingLogoObject.GetComponent<Image>().color = c;
				}
				
				break;
			
			case InternalState.FadeOut:
				// Subtract from the current fade value.
				currentFade -= fadeSpeed;
				// If the logo is fully faded out, then limit the alpha to 0, and return true.
				if(currentFade < 0)
				{
					Color c = logo.GetComponent<Image>().color;
					c.a = 0;
					logo.GetComponent<Image>().color = c;
					zingLogoObject.GetComponent<Image>().color = c;
					return true;
				}
				else
				// Otherwise, set the alpha to the current fade value.
				{
					Color c = logo.GetComponent<Image>().color;
					c.a = currentFade;
					logo.GetComponent<Image>().color = c;
					zingLogoObject.GetComponent<Image>().color = c;
				}
				break;
		}
		
		// Return false if the logo is not at maximum or minumum fade, depending on state.
		return false;
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update ()
	{
		if(screenSize.width == 0 || screenSize.height == 0 || screenSize.width != Screen.width || screenSize.height != Screen.height)
		{
			screenSize = new Rect(0, 0, Screen.width, Screen.height);
			
			if(localCanvas != null)
			{
				Destroy (localCanvas);
				localCanvas = null;
			}
				
			CreateGUI ();
			
			localCanvas.transform.localScale = Vector2.zero;
		}
		else
		{
			if(logo != null)
			{
				switch(internalState)
				{
				case InternalState.FadeIn:
					// Fade the logo in. If it is fully faded, move to the next state.
					if(Fade ())
					{
						internalState = InternalState.Idle;
					}
					break;
				case InternalState.Idle:
					// Add to the idle timer.
					currentIdleTime  += Time.deltaTime;
					// If the idle time is up, move to the next state.
					if(currentIdleTime >= idleTime)
					{
						internalState = InternalState.FadeOut;
					}
					break;
				case InternalState.FadeOut:
					// Fade the logo out. If it is fully faded out, move to the next state.
					if(Fade ())
					{
						internalState = InternalState.ChangeScene;
					}
					break;
				case InternalState.ChangeScene:
					// Move to the intro screen.
					LoadMenu ("Intro_Screen");
					break;
				}
			}
		}
	}
	
	public override void DoGUI (){}
}
