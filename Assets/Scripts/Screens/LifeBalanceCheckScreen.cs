﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LifeBalanceCheckScreen : MenuScreen
{
	private List<GameObject> introScreenObjects;
	private List<GameObject> setScoresScreenObjects;
	private List<GameObject> displayScoresScreenObjects;
	private List<GameObject> createMapScreenObjects;
	private List<GameObject> areaValueObjects;
	private GameObject localCanvas;
	private GameObject happinessValueObject;
	private GameObject selfWorthValueObject;
	private GameObject wellBeingValueObject;
	private GameObject financesValueObject;
	private GameObject relationshipsValueObject;
	private GameObject purposeValueObject;
	
	private Sprite leftArrowSprite;
	private Sprite rightArrowSprite;	
	private Sprite wheelBackgroundSprite;
	private Sprite happinessSprite;
	private Sprite selfWorthSprite;
	private Sprite wellBeingSprite;
	private Sprite financesSprite;
	private Sprite relationshipsSprite;
	private Sprite purposeSprite;
	
	private LifeBalance currentLifeBalance;
	private List<LifeBalance> balanceHistory;
	private InternalState internalState = InternalState.Intro;
	private Rect fullPanelRect;
	private Rect popupPanelRect;
	private Vector3 graphCenterPoint;
	
	enum InternalState
	{
		Intro,
		SetScores,
		DisplayScores,
		CreateMap
	}
	
	class LifeBalance
	{
		public string dateTimeString;
		public int happiness;
		public int selfWorth;
		public int wellBeing;
		public int finances;
		public int relationships;
		public int purpose;
		private int defaultValue = 5;
		
		public LifeBalance()
		{
			happiness = defaultValue;
			selfWorth = defaultValue;
			wellBeing = defaultValue;
			finances = defaultValue;
			relationships = defaultValue;
			purpose = defaultValue;
		}
	}
	
	// Use this for initialization
	void Start ()
	{
		// Initialize base.
		Init ();
		
		// Initalize lists/arrays.
		introScreenObjects = new List<GameObject>();
		setScoresScreenObjects = new List<GameObject>();
		displayScoresScreenObjects = new List<GameObject>();
		createMapScreenObjects = new List<GameObject>();
		areaValueObjects = new List<GameObject>();
		
		// Load resources
		leftArrowSprite = Resources.Load<Sprite>("left_arrow");
		rightArrowSprite = Resources.Load<Sprite>("right_arrow");
		wheelBackgroundSprite = Resources.Load<Sprite>("GUI/WheelBlue");
		happinessSprite = Resources.Load<Sprite>("head_goal");
		selfWorthSprite = Resources.Load<Sprite>("head_goal");
		wellBeingSprite = Resources.Load<Sprite>("head_goal");
		financesSprite = Resources.Load<Sprite>("head_goal");
		relationshipsSprite = Resources.Load<Sprite>("head_goal");
		purposeSprite = Resources.Load<Sprite>("head_goal");
		
		// Set the default variables
		fullPanelRect = new Rect(Screen.width * 0.02f, Screen.height * 0.15f, Screen.width * 0.96f, Screen.height * 0.83f);
		popupPanelRect = new Rect(Screen.width * 0.1f, Screen.height * 0.15f, Screen.width * 0.8f, Screen.height * 0.8f);
		
		currentLifeBalance = new LifeBalance();
		
		// Create the 3D GUI
		CreateGUI ();
		
		ChangeState (InternalState.Intro);
	}
	
	void CreateGUI()
	{
		// Make the canvas.
		localCanvas = MakeCanvas ();
		
		// Make the title bar.
		MakeTitleBar (localCanvas, MCP.Text(3801)/*"The Goal Mapping Life Wheel*"*/);
		
		// Make the side gizmo.
		MakeSideGizmo (localCanvas, true);
		
		// Make back panel.
		GameObject introPanelBackground = MakeImage (localCanvas, fullPanelRect, window_back);
		introPanelBackground.name = "IntroPanelBackground";
		introScreenObjects.Add (introPanelBackground);
		setScoresScreenObjects.Add (introPanelBackground);
		displayScoresScreenObjects.Add (introPanelBackground);
		createMapScreenObjects.Add (introPanelBackground);
		
		// Make subtitle.
		GameObject subtitleObject = MakeLabel (localCanvas, MCP.Text(3802)/*"Check your life-balance - it's easy!*"*/, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.02f), fullPanelRect.y + (fullPanelRect.height * 0.07f), fullPanelRect.width * 0.96f, fullPanelRect.height * 0.1f), "SmallText");
		subtitleObject.name = "SubtitleObject";
		introScreenObjects.Add (subtitleObject);
		
		// Make intro text.
		string introText = MCP.Text(3803);/*"Success and balance go hand in hand. The more balance we have between the important areas of our life, the more success we experience. To check your current life-balance give yourself a score in each of the areas below.\n\nWhen you answer, be intuitive and honest and try not to judge yourself against others. Simply compare your current balance with being your best. Score yourself one if you believe you are very low in any area, and up to ten if you believe you are high.";*/
		GameObject introTextObject = MakeLabel (localCanvas, introText, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.02f), fullPanelRect.y + (fullPanelRect.height * 0.22f), fullPanelRect.width * 0.6f, fullPanelRect.height * 0.75f), "SmallText");
		introTextObject.name = "IntroTextObject";
		introScreenObjects.Add (introTextObject);
		
		// Make intro graph background.
		GameObject introGraphBackground = MakeImage (localCanvas, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.65f), fullPanelRect.y + (fullPanelRect.height * 0.24f), fullPanelRect.width * 0.3f, fullPanelRect.width * 0.3f), wheelBackgroundSprite);
		introGraphBackground.name = "IntroGraphBackground";
		introScreenObjects.Add (introGraphBackground);
		
		// Make next button
		List<MyVoidFunc> funcList = new List<MyVoidFunc>()
		{
			NextButton
		};
		GameObject nextButton = MakeButton (localCanvas, "Next*", buttonBackground, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.76f), fullPanelRect.y + (fullPanelRect.height * 0.9f), fullPanelRect.width * 0.2f, fullPanelRect.height * 0.08f), funcList);
		nextButton.name = "NextButton";
		introScreenObjects.Add (nextButton);
		setScoresScreenObjects.Add (nextButton);
		
		// Make life area intro text.
		GameObject lifeAreaIntroText = MakeLabel (localCanvas, MCP.Text(3804)/*"Select a number between 1-10 to represent how you feel in the following areas:*"*/, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.02f), fullPanelRect.y + (fullPanelRect.height * 0.05f), fullPanelRect.width * 0.96f, fullPanelRect.height * 0.25f), "SmallText");
		lifeAreaIntroText.name = "LifeAreaIntroText";
		setScoresScreenObjects.Add (lifeAreaIntroText);
		
		// Make the life area objects.
		CreateAreaObjects ();
		
		// Make display text
		string displayTextString = MCP.Text(3811);/*"The orange circle on your life-wheel displays your current state of balance. The aim is to have an even circle, although most people have a dip or two. A 'flat' area on your life-wheel indicates where you need to focus to bring your life back into balance.\n\nClick the 'spoke' of the life wheel that has your lowest score to create a Goal Map for improving that area of your life.\n\nGive yourself a monthly balance check! Our lives and our balance are always changing. Check your life-balance using the wheel once every month and create short-term Goal Maps to boost your low scoring areas.";*/
		GameObject displayText = MakeLabel (localCanvas, displayTextString, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.02f), fullPanelRect.y + (fullPanelRect.height * 0.02f), fullPanelRect.width * 0.575f, fullPanelRect.height * 0.96f), "SmallText");
		displayText.name = "DisplayText";
		displayScoresScreenObjects.Add (displayText);
		
		// Make graph background.
		GameObject displayGraphBackground = MakeImage (localCanvas, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.6f), fullPanelRect.y + (fullPanelRect.height * 0.125f), fullPanelRect.width * 0.35f, fullPanelRect.width * 0.35f), wheelBackgroundSprite);
		displayGraphBackground.name = "DisplayGraphBackground";
		displayScoresScreenObjects.Add (displayGraphBackground);
		graphCenterPoint = new Vector3(fullPanelRect.x + (fullPanelRect.width * 0.45f) + (fullPanelRect.width * 0.2f),
		                               fullPanelRect.y + (fullPanelRect.height * 0.15f) + (fullPanelRect.width * 0.2f),
	                               	   0);
		
		// Make create map icon.
		
		// Make create map subtitle.
		
		// Make create map text.
		
		// Make create map button.
		
	}
	
	void CreateAreaObjects()
	{
		TextAnchor prevAlign = skin.GetStyle ("SmallText").alignment;
		skin.GetStyle ("SmallText").alignment = TextAnchor.MiddleLeft;
		
		List<MyVoidFunc> funcList;
		float iconWidth = fullPanelRect.width * 0.05f;
		
		float offsetY = fullPanelRect.y + (fullPanelRect.height * 0.25f) - (fullPanelRect.width * 0.035f);
		// Make happiness icon
		GameObject happinessIcon = MakeImage (localCanvas, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.05f), offsetY, iconWidth, iconWidth), happinessSprite);
		happinessIcon.name = "HappinessIcon";
		setScoresScreenObjects.Add (happinessIcon);
		
		// Make happiness text
		GameObject happinessText = MakeLabel (localCanvas, MCP.Text(3805)/*"What is your current level of <b>Happiness</b>*"*/, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.14f), offsetY, fullPanelRect.width * 0.575f, iconWidth), "SmallText");
		happinessText.name = "HappinessText";
		happinessText.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
		setScoresScreenObjects.Add (happinessText);
		
		// Make happiness down arrow
		funcList = new List<MyVoidFunc>()
		{
			delegate
			{
				currentLifeBalance.happiness = currentLifeBalance.happiness - 1 < 1 ? 1 : currentLifeBalance.happiness - 1;
			}
		};
		GameObject happinessDownButton = MakeButton (localCanvas, "", leftArrowSprite, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.75f), offsetY + (iconWidth * 0.25f), iconWidth * 0.5f, iconWidth * 0.5f), funcList);
		happinessDownButton.name = "HappinessDownButton";
		setScoresScreenObjects.Add (happinessDownButton);
		
		// Make happiness value
		happinessValueObject = MakeLabel(localCanvas, currentLifeBalance.happiness.ToString (), new Rect(fullPanelRect.x + (fullPanelRect.width * 0.835f), offsetY, iconWidth, iconWidth), "InnerLabel");
		happinessValueObject.name = "HappinessValueObject";
		setScoresScreenObjects.Add (happinessValueObject);
		areaValueObjects.Add (happinessValueObject);
		
		// Make happiness up arrow
		funcList = new List<MyVoidFunc>()
		{
			delegate
			{
				currentLifeBalance.happiness = currentLifeBalance.happiness + 1 > 10 ? 10 : currentLifeBalance.happiness + 1;
			}
		};
		GameObject happinessUpButton = MakeButton (localCanvas, "", rightArrowSprite, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.94f), offsetY + (iconWidth * 0.25f), iconWidth * 0.5f, iconWidth * 0.5f), funcList);
		happinessUpButton.name = "HappinessUpButton";
		setScoresScreenObjects.Add (happinessUpButton);
		
		offsetY += iconWidth + (fullPanelRect.height * 0.02f);
		
		// Make self worth icon
		GameObject selfWorthIcon = MakeImage (localCanvas, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.05f), offsetY, iconWidth, iconWidth), selfWorthSprite);
		selfWorthIcon.name = "SelfWorthIcon";
		setScoresScreenObjects.Add (selfWorthIcon);
		
		// Make self worth text
		GameObject selfWorthText = MakeLabel (localCanvas, MCP.Text(3806)/*"What is your feeling of <b>Self Worth?</b>*"*/, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.14f), offsetY, fullPanelRect.width * 0.575f, iconWidth), "SmallText");
		selfWorthText.name = "SelfWorthText";
		selfWorthText.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
		setScoresScreenObjects.Add (selfWorthText);
		
		// Make self worth down arrow
		funcList = new List<MyVoidFunc>()
		{
			delegate
			{
				currentLifeBalance.selfWorth = currentLifeBalance.selfWorth - 1 < 1 ? 1 : currentLifeBalance.selfWorth - 1;
			}
		};
		GameObject selfWorthDownButton = MakeButton (localCanvas, "", leftArrowSprite, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.75f), offsetY + (iconWidth * 0.25f), iconWidth * 0.5f, iconWidth * 0.5f), funcList);
		selfWorthDownButton.name = "SelfWorthDownButton";
		setScoresScreenObjects.Add (selfWorthDownButton);
		
		// Make self worth value
		selfWorthValueObject = MakeLabel(localCanvas, currentLifeBalance.selfWorth.ToString (), new Rect(fullPanelRect.x + (fullPanelRect.width * 0.835f), offsetY, iconWidth, iconWidth), "InnerLabel");
		selfWorthValueObject.name = "selfWorthValueObject";
		setScoresScreenObjects.Add (selfWorthValueObject);
		areaValueObjects.Add (selfWorthValueObject);
		
		// Make self worth up arrow
		funcList = new List<MyVoidFunc>()
		{
			delegate
			{
				currentLifeBalance.selfWorth = currentLifeBalance.selfWorth + 1 > 10 ? 10 : currentLifeBalance.selfWorth + 1;
			}
		};
		GameObject selfWorthUpButton = MakeButton (localCanvas, "", rightArrowSprite, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.94f), offsetY + (iconWidth * 0.25f), iconWidth * 0.5f, iconWidth * 0.5f), funcList);
		selfWorthUpButton.name = "SelfWorthUpButton";
		setScoresScreenObjects.Add (selfWorthUpButton);
		
		offsetY += iconWidth + (fullPanelRect.height * 0.02f);
		
		// Make well being icon
		GameObject wellBeingIcon = MakeImage (localCanvas, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.05f), offsetY, iconWidth, iconWidth), wellBeingSprite);
		wellBeingIcon.name = "WellBeingIcon";
		setScoresScreenObjects.Add (wellBeingIcon);
		
		// Make well being text
		GameObject wellBeingText = MakeLabel (localCanvas, MCP.Text(3807)/*"How do you rate your current state of <b>Well Being?</b>*"*/, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.14f), offsetY, fullPanelRect.width * 0.575f, iconWidth), "SmallText");
		wellBeingText.name = "WellBeingText";
		wellBeingText.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
		setScoresScreenObjects.Add (wellBeingText);
		
		// Make well being down arrow
		funcList = new List<MyVoidFunc>()
		{
			delegate
			{
				currentLifeBalance.wellBeing = currentLifeBalance.wellBeing - 1 < 1 ? 1 : currentLifeBalance.wellBeing - 1;
			}
		};
		GameObject wellBeingDownButton = MakeButton (localCanvas, "", leftArrowSprite, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.75f), offsetY + (iconWidth * 0.25f), iconWidth * 0.5f, iconWidth * 0.5f), funcList);
		wellBeingDownButton.name = "WellBeingDownButton";
		setScoresScreenObjects.Add (wellBeingDownButton);
		
		// Make well being value
		wellBeingValueObject = MakeLabel(localCanvas, currentLifeBalance.wellBeing.ToString (), new Rect(fullPanelRect.x + (fullPanelRect.width * 0.835f), offsetY, iconWidth, iconWidth), "InnerLabel");
		wellBeingValueObject.name = "WellBeingValueObject";
		setScoresScreenObjects.Add (wellBeingValueObject);
		areaValueObjects.Add (wellBeingValueObject);
		
		// Make well being up arrow
		funcList = new List<MyVoidFunc>()
		{
			delegate
			{
				currentLifeBalance.wellBeing = currentLifeBalance.wellBeing + 1 > 10 ? 10 : currentLifeBalance.wellBeing + 1;
			}
		};
		GameObject wellBeingUpButton = MakeButton (localCanvas, "", rightArrowSprite, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.94f), offsetY + (iconWidth * 0.25f), iconWidth * 0.5f, iconWidth * 0.5f), funcList);
		wellBeingUpButton.name = "WellBeingUpButton";
		setScoresScreenObjects.Add (wellBeingUpButton);
		
		offsetY += iconWidth + (fullPanelRect.height * 0.02f);
		
		// Make finances icon
		GameObject financeIcon = MakeImage (localCanvas, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.05f), offsetY, iconWidth, iconWidth), financesSprite);
		financeIcon.name = "FinanceIcon";
		setScoresScreenObjects.Add (financeIcon);
		
		// Make finances text
		GameObject financeText = MakeLabel (localCanvas, MCP.Text(3808)/*"How would you rate your <b>Finances?</b>*"*/, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.14f), offsetY, fullPanelRect.width * 0.575f, iconWidth), "SmallText");
		financeText.name = "FinanceText";
		financeText.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
		setScoresScreenObjects.Add (financeText);
		
		// Make finances down arrow
		funcList = new List<MyVoidFunc>()
		{
			delegate
			{
				currentLifeBalance.finances = currentLifeBalance.finances - 1 < 1 ? 1 : currentLifeBalance.finances - 1;
			}
		};
		GameObject financesDownButton = MakeButton (localCanvas, "", leftArrowSprite, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.75f), offsetY + (iconWidth * 0.25f), iconWidth * 0.5f, iconWidth * 0.5f), funcList);
		financesDownButton.name = "FinancesDownButton";
		setScoresScreenObjects.Add (financesDownButton);
		
		// Make finances value
		financesValueObject = MakeLabel(localCanvas, currentLifeBalance.finances.ToString (), new Rect(fullPanelRect.x + (fullPanelRect.width * 0.835f), offsetY, iconWidth, iconWidth), "InnerLabel");
		financesValueObject.name = "financesValueObject";
		setScoresScreenObjects.Add (financesValueObject);
		areaValueObjects.Add (financesValueObject);
		
		// Make finances up arrow
		funcList = new List<MyVoidFunc>()
		{
			delegate
			{
				currentLifeBalance.finances = currentLifeBalance.finances + 1 > 10 ? 10 : currentLifeBalance.finances + 1;
			}
		};
		GameObject financesUpButton = MakeButton (localCanvas, "", rightArrowSprite, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.94f), offsetY + (iconWidth * 0.25f), iconWidth * 0.5f, iconWidth * 0.5f), funcList);
		financesUpButton.name = "financesUpButton";
		setScoresScreenObjects.Add (financesUpButton);
		
		offsetY += iconWidth + (fullPanelRect.height * 0.02f);
		
		// Make relationships icon
		GameObject relationshipIcon = MakeImage (localCanvas, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.05f), offsetY, iconWidth, iconWidth), relationshipsSprite);
		relationshipIcon.name = "RelationshipIcon";
		setScoresScreenObjects.Add (relationshipIcon);
		
		// Make relationships text
		GameObject relationshipText = MakeLabel (localCanvas, MCP.Text(3809)/*"What is your current level of harmony in your <b>Relationships?</b>*"*/, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.14f), offsetY, fullPanelRect.width * 0.575f, iconWidth), "SmallText");
		relationshipText.name = "RelationshipText";
		relationshipText.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
		setScoresScreenObjects.Add (relationshipText);
		
		// Make relationships down arrow
		funcList = new List<MyVoidFunc>()
		{
			delegate
			{
				currentLifeBalance.relationships = currentLifeBalance.relationships - 1 < 1 ? 1 : currentLifeBalance.relationships - 1;
			}
		};
		GameObject relationshipDownButton = MakeButton (localCanvas, "", leftArrowSprite, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.75f), offsetY + (iconWidth * 0.25f), iconWidth * 0.5f, iconWidth * 0.5f), funcList);
		relationshipDownButton.name = "RelationshipDownButton";
		setScoresScreenObjects.Add (relationshipDownButton);
		
		// Make relationships value
		relationshipsValueObject = MakeLabel(localCanvas, currentLifeBalance.relationships.ToString (), new Rect(fullPanelRect.x + (fullPanelRect.width * 0.835f), offsetY, iconWidth, iconWidth), "InnerLabel");
		relationshipsValueObject.name = "RelationshipsValueObject";
		setScoresScreenObjects.Add (relationshipsValueObject);
		areaValueObjects.Add (relationshipsValueObject);
		
		// Make relationships up arrow
		funcList = new List<MyVoidFunc>()
		{
			delegate
			{
				currentLifeBalance.relationships = currentLifeBalance.relationships + 1 > 10 ? 10 : currentLifeBalance.relationships + 1;
			}
		};
		GameObject relationshipUpButton = MakeButton (localCanvas, "", rightArrowSprite, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.94f), offsetY + (iconWidth * 0.25f), iconWidth * 0.5f, iconWidth * 0.5f), funcList);
		relationshipUpButton.name = "RelationshipUpButton";
		setScoresScreenObjects.Add (relationshipUpButton);
		
		offsetY += iconWidth + (fullPanelRect.height * 0.02f);
		
		// Make purpose icon
		GameObject purposeIcon = MakeImage (localCanvas, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.05f), offsetY, iconWidth, iconWidth), purposeSprite);
		purposeIcon.name = "PurposeIcon";
		setScoresScreenObjects.Add (purposeIcon);
		
		// Make purpose text
		GameObject purposeText = MakeLabel (localCanvas, MCP.Text(3810)/*"What is your current sense of <b>Purpose?</b>*"*/, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.14f), offsetY, fullPanelRect.width * 0.575f, iconWidth), "SmallText");
		purposeText.name = "PurposeText";
		purposeText.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
		setScoresScreenObjects.Add (purposeText);
		
		// Make purpose down arrow
		funcList = new List<MyVoidFunc>()
		{
			delegate
			{
				currentLifeBalance.purpose = currentLifeBalance.purpose - 1 < 1 ? 1 : currentLifeBalance.purpose - 1;
			}
		};
		GameObject purposeDownButton = MakeButton (localCanvas, "", leftArrowSprite, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.75f), offsetY + (iconWidth * 0.25f), iconWidth * 0.5f, iconWidth * 0.5f), funcList);
		purposeDownButton.name = "PurposeDownButton";
		setScoresScreenObjects.Add (purposeDownButton);
		
		// Make purpose value
		purposeValueObject = MakeLabel(localCanvas, currentLifeBalance.purpose.ToString (), new Rect(fullPanelRect.x + (fullPanelRect.width * 0.835f), offsetY, iconWidth, iconWidth), "InnerLabel");
		purposeValueObject.name = "PurposeValueObject";
		setScoresScreenObjects.Add (purposeValueObject);
		areaValueObjects.Add (purposeValueObject);
		
		// Make purpose up arrow
		funcList = new List<MyVoidFunc>()
		{
			delegate
			{
				currentLifeBalance.purpose = currentLifeBalance.purpose + 1 > 10 ? 10 : currentLifeBalance.purpose + 1;
			}
		};
		GameObject purposeUpButton = MakeButton (localCanvas, "", rightArrowSprite, new Rect(fullPanelRect.x + (fullPanelRect.width * 0.94f), offsetY + (iconWidth * 0.25f), iconWidth * 0.5f, iconWidth * 0.5f), funcList);
		purposeUpButton.name = "PurposeUpButton";
		setScoresScreenObjects.Add (purposeUpButton);
		
		skin.GetStyle ("SmallText").alignment = prevAlign;
	}
	
	void ChangeState(InternalState newState)
	{
		internalState = newState;
		
		for (int i = 0; i < introScreenObjects.Count; i++)
		{
			introScreenObjects[i].SetActive (false);
		}
		
		for (int i = 0; i < setScoresScreenObjects.Count; i++)
		{
			setScoresScreenObjects[i].SetActive (false);
		}
		
		for (int i = 0; i < displayScoresScreenObjects.Count; i++)
		{
			displayScoresScreenObjects[i].SetActive (false);
		}
		
		for (int i = 0; i < createMapScreenObjects.Count; i++)
		{
			createMapScreenObjects[i].SetActive (false);
		}
		
		switch(newState)
		{
		case InternalState.Intro:
			for (int i = 0; i < introScreenObjects.Count; i++)
			{
				introScreenObjects[i].SetActive (true);
			}
			break;
			
		case InternalState.SetScores:
			for (int i = 0; i < setScoresScreenObjects.Count; i++)
			{
				setScoresScreenObjects[i].SetActive (true);
			}
			break;
			
		case InternalState.DisplayScores:
			for (int i = 0; i < displayScoresScreenObjects.Count; i++)
			{
				displayScoresScreenObjects[i].SetActive (true);
			}
			break;
			
		case InternalState.CreateMap:
			for (int i = 0; i < createMapScreenObjects.Count; i++)
			{
				createMapScreenObjects[i].SetActive (true);
			}
			break;
		}
	}
	
	void NextButton()
	{
		switch(internalState)
		{
		case InternalState.Intro:
			ChangeState(InternalState.SetScores);
			break;
		
		case InternalState.SetScores:
			ChangeState(InternalState.DisplayScores);
			break;
		
		case InternalState.DisplayScores:
			// Shouldn't be visible/active to press.
			ChangeState(InternalState.DisplayScores);
			break;
		
		case InternalState.CreateMap:
			// Goto goal mapping.
			break;
		}
	}
	
	enum MapArea
	{
		Happiness,
		SelfWorth,
		WellBeing,
		Finances,
		Relationships,
		Purpose
	}
	
	void SetCreateMapScreenArea(MapArea area)
	{
	}
	
	void UpdateAreaDisplayedScores()
	{
		areaValueObjects[0].GetComponent<Text>().text = currentLifeBalance.happiness.ToString();
		areaValueObjects[1].GetComponent<Text>().text = currentLifeBalance.selfWorth.ToString();
		areaValueObjects[2].GetComponent<Text>().text = currentLifeBalance.wellBeing.ToString();
		areaValueObjects[3].GetComponent<Text>().text = currentLifeBalance.finances.ToString();
		areaValueObjects[4].GetComponent<Text>().text = currentLifeBalance.relationships.ToString();
		areaValueObjects[5].GetComponent<Text>().text = currentLifeBalance.purpose.ToString();
	}
	
	void DrawRadarGraph()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(internalState == InternalState.SetScores)
		{
			UpdateAreaDisplayedScores();
		}
		
		if(internalState == InternalState.DisplayScores)
		{
			// TODO: DrawRadarGraph
		}
	}
	
	public override void Back ()
	{
		switch(internalState)
		{
		case InternalState.Intro:
			base.Back();
			break;
			
		case InternalState.SetScores:
			ChangeState(InternalState.Intro);
			break;
			
		case InternalState.DisplayScores:
			ChangeState(InternalState.SetScores);
			break;
			
		case InternalState.CreateMap:
			ChangeState(InternalState.DisplayScores);
			break;
		}
	}
	
	public override void DoGUI()
	{
		
	}
}
