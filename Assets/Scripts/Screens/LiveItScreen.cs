using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LiveItScreen : MenuScreen
{
	private GameObject localCanvas;
	private GameObject videoObj;
	private Sprite video;
	
//	private bool waitForVid = false;
//	private bool vidFinished = false;

	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		// Initialise the base.
		Init ();
		
		// Load the splash logo.
		video = Resources.Load<Sprite>("zingup_logo_splash");
		
		// Create the scene GUI.
		CreateGUI ();
		
		localCanvas.transform.localScale = Vector2.zero;
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Find the current canvas.
		localCanvas = MakeCanvas();
		
		// Make the title bar.
		GameObject titleBar = MakeTitleBar (localCanvas, MCP.Text (1201)/*"Live It Screen"*/);
		titleBar.name = "TitleBar";
		
		// Make the side gizmo.
		MakeSideGizmo (localCanvas, false);
		
		// Create the logo.
		videoObj = MakeImage (localCanvas, new Rect(0, 0, Screen.width * 0.5f, Screen.height * 0.5f), video);
		videoObj.name = "Logo";
	}
	
	public override void DoGUI() {}
}
