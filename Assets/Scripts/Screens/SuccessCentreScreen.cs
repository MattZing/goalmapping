using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SuccessCentreScreen : MenuScreen
{
	private GameObject localCanvas;
	private GameObject popup;
	private GameObject downloadSpinner;
	private GameObject popupProgressText;
	private GameObject loginPopup;
	private GameObject upgradePopup;
	private GameObject videoManagerObject;
	
	private Sprite bronzeSprite;
	private Sprite silverSprite;
	private Sprite goldSprite;
	private Sprite spinnerSprite;
	
	private bool downloadingBundle = false;
	private Category currentCat = Category.None;
	private float spinnerRot;
	private float downloadPercentage = 0;
	private Rect screenSize;
	public List<Media> bronzeMedia;
	public List<Media> silverMedia;
	public List<Media> goldMedia;
	
	public struct Media
	{
		public string displayName;
		public List<string> url;
		public Type type;
		
		public enum Type
		{
			Audio,
			Video
		}
		
		public Media (string _displayName, string _url, Type _type)
		{
			displayName = _displayName;
			url = new List<string>();
			url.Add (_url);
			type = _type;
		}
		
		public Media (string _displayName, List<string> _url, Type _type)
		{
			displayName = _displayName;
			url = _url;
			type = _type;
		}
	}
	
	enum Category
	{
		None,
		Bronze,
		Silver,
		Gold
	}
	
	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		// Initialise the base.
		Init ();
		
		// Load resources.
		bronzeSprite = Resources.Load<Sprite>("GUI/SuccessCentre/success_centre_bronze");
		silverSprite = Resources.Load<Sprite>("GUI/SuccessCentre/success_centre_silver");
		goldSprite = Resources.Load<Sprite>("GUI/SuccessCentre/success_centre_gold");
		spinnerSprite = Resources.Load<Sprite> ("GUI/bm_rotate");
		
		bronzeMedia = new List<Media>();
		silverMedia = new List<Media>();
		goldMedia = new List<Media>();
		
		string format = ".ogg";
#if UNITY_ANDROID || UNITY_IPHONE
		format = ".mp3";
#endif
		
		// Set up the media
		bronzeMedia.Add (new Media(MCP.Text(1916) + MCP.Text (1914)/*"The 7 Fundamental Laws of Creation Audio"*/, MCP.serverURL + ("Videos/SuccessCentre/1 The 7 Fundamental Laws of Creation" + format).Replace (" ", "%20"), Media.Type.Audio));
		bronzeMedia.Add (new Media(MCP.Text(1917) + MCP.Text (1914)/*"The 7 Empowering Questions Audio"*/, MCP.serverURL + ("Videos/SuccessCentre/2 The 7 Empowering Questions" + format).Replace (" ", "%20"), Media.Type.Audio));
//		bronzeMedia.Add (new Media(MCP.Text(1918) + MCP.Text (1914)/*"The 7 Steps of Goal Mapping Audio"*/, MCP.serverURL + ("Videos/SuccessCentre/3 The 7 Steps of Goal Mapping" + format).Replace (" ", "%20"), Media.Type.Audio));
		bronzeMedia.Add (new Media(MCP.Text(1919) + MCP.Text (1914)/*"The Goal Mapping Ritual Audio"*/, MCP.serverURL + ("Videos/SuccessCentre/4 The Goal Mapping Ritual" + format).Replace (" ", "%20"), Media.Type.Audio));
		
#if UNITY_ANDROID || UNITY_IPHONE
		format = ".mp4";
#endif

//#if !UNITY_IPHONE
		bronzeMedia.Add (new Media(MCP.Text(1917) + MCP.Text (1915)/*"The 7 Empowering Questions Video"*/, MCP.serverURL + ("Videos/SuccessCentre/Goal Mapping Process - 7 Empowering Questions" + format).Replace (" ", "%20"), Media.Type.Video));
		bronzeMedia.Add (new Media(MCP.Text(1916) + MCP.Text (1915)/*"The 7 Fundamental Laws of Creation Video"*/, MCP.serverURL + ("Videos/SuccessCentre/Goal Mapping Process - 7 Fundamental Laws of Creation" + format).Replace (" ", "%20"), Media.Type.Video));
//		bronzeMedia.Add (new Media(MCP.Text(1918) + MCP.Text (1915)/*"The 7 Steps of Goal Mapping Video"*/, MCP.serverURL + ("Videos/SuccessCentre/Goal Mapping Process - 7 Steps of Goal Mapping" + format).Replace (" ", "%20"), Media.Type.Video));
		bronzeMedia.Add (new Media(MCP.Text(1920) + MCP.Text (1915)/*"Following Your Map Video"*/, MCP.serverURL + ("Videos/SuccessCentre/Goal Mapping Process - Following your Map" + format).Replace (" ", "%20"), Media.Type.Video));
//#endif
		
#if UNITY_ANDROID || UNITY_IPHONE
		format = ".mp3";
#endif
		goldMedia.Add (new Media(MCP.Text(1921) + MCP.Text (1914)/*"The Winds of Change Audio"*/, MCP.serverURL + ("Videos/SuccessCentre/1 The Winds of Change" + format).Replace (" ", "%20"), Media.Type.Audio));
		goldMedia.Add (new Media(MCP.Text(1922) + MCP.Text (1914)/*"My Personal Learning Journey Audio"*/, MCP.serverURL + ("Videos/SuccessCentre/2 My Personal Learning Journey" + format).Replace (" ", "%20"), Media.Type.Audio));
		goldMedia.Add (new Media(MCP.Text(1923) + MCP.Text (1914)/*"The Power of Positive Thinking Audio"*/, MCP.serverURL + ("Videos/SuccessCentre/3 The Power of Positive Thinking" + format).Replace (" ", "%20"), Media.Type.Audio));
		goldMedia.Add (new Media(MCP.Text(1924) + MCP.Text (1914)/*"The DAC Factor Audio"*/, MCP.serverURL + ("Videos/SuccessCentre/4 The DAC Factor" + format).Replace (" ", "%20"), Media.Type.Audio));
		goldMedia.Add (new Media(MCP.Text(1925) + MCP.Text (1914)/*"The Winning Edge Audio"*/, MCP.serverURL + ("Videos/SuccessCentre/5 The Winning Edge " + format).Replace (" ", "%20"), Media.Type.Audio));
		goldMedia.Add (new Media(MCP.Text(1926) + MCP.Text (1914)/*"The 7 Principles of Lift Audio"*/, MCP.serverURL + ("Videos/SuccessCentre/6 The 7 Principles of Lift " + format).Replace (" ", "%20"), Media.Type.Audio));
		goldMedia.Add (new Media(MCP.Text(1927) + MCP.Text (1914)/*"Raise your Awareness Audio"*/, MCP.serverURL + ("Videos/SuccessCentre/Principle 1 Raise your Awareness" + format).Replace (" ", "%20"), Media.Type.Audio));
		goldMedia.Add (new Media(MCP.Text(1928) + MCP.Text (1914)/*"Develop Possibilty Consciousness Audio"*/, MCP.serverURL + ("Videos/SuccessCentre/Principle 2 Develop Possibilty Consciousness" + format).Replace (" ", "%20"), Media.Type.Audio));
		goldMedia.Add (new Media(MCP.Text(1929) + MCP.Text (1914)/*"Find Balance Audio"*/, MCP.serverURL + ("Videos/SuccessCentre/Principle 3 Find Balance" + format).Replace (" ", "%20"), Media.Type.Audio));
		goldMedia.Add (new Media(MCP.Text(1930) + MCP.Text (1914)/*"Be On Purpose Audio"*/, MCP.serverURL + ("Videos/SuccessCentre/Principle 4 Be on Purpose" + format).Replace (" ", "%20"), Media.Type.Audio));
		goldMedia.Add (new Media(MCP.Text(1931) + MCP.Text (1914)/*"Become Fully Response-Able Audio"*/, MCP.serverURL + ("Videos/SuccessCentre/Principle 5 Become Fully Response-Able" + format).Replace (" ", "%20"), Media.Type.Audio));
		goldMedia.Add (new Media(MCP.Text(1932) + MCP.Text (1914)/*"Maintain A Positive Focus Audio"*/, MCP.serverURL + ("Videos/SuccessCentre/Principle 6 Maintain a Positive Focus" + format).Replace (" ", "%20"), Media.Type.Audio));
		goldMedia.Add (new Media(MCP.Text(1933) + MCP.Text (1914)/*"Involve to Evolve Audio"*/, MCP.serverURL + ("Videos/SuccessCentre/Principle 7 Involve to Evolve" + format).Replace (" ", "%20"), Media.Type.Audio));
		
#if UNITY_ANDROID || UNITY_IPHONE
		format = ".mp4";
#endif
		
		goldMedia.Add(new Media(MCP.Text(1921) + MCP.Text (1915)/*"The Winds of Change Video"*/, MCP.serverURL + ("Videos/SuccessCentre/Inspiration 1 - The Winds of Change" + format).Replace (" ", "%20"), Media.Type.Video));
		goldMedia.Add(new Media(MCP.Text(1922) + MCP.Text (1915)/*"My Personal Learning Journey Video"*/, MCP.serverURL + ("Videos/SuccessCentre/Inspiration 2 - My Personal Learning Journey" + format).Replace (" ", "%20"), Media.Type.Video));
		goldMedia.Add(new Media(MCP.Text(1923) + MCP.Text (1915)/*"The Power of Positive Thinking Video"*/, MCP.serverURL + ("Videos/SuccessCentre/Inspiration 3 - The Power of Positive Thinking" + format).Replace (" ", "%20"), Media.Type.Video));
		goldMedia.Add(new Media(MCP.Text(1934) + MCP.Text (1915)/*"DAC Factor and 80-20 Law Journey Video"*/, MCP.serverURL + ("Videos/SuccessCentre/Inspiration 4 - DAC Factor and 80-20 Law Journey" + format).Replace (" ", "%20"), Media.Type.Video));
		goldMedia.Add(new Media(MCP.Text(1935) + MCP.Text (1915)/*"Ascending the Lift Ladder Video"*/, MCP.serverURL + ("Videos/SuccessCentre/Inspiration 5 - Ascendy the Lift Ladder" + format).Replace (" ", "%20"), Media.Type.Video));
		goldMedia.Add(new Media(MCP.Text(1927) + MCP.Text (1915)/*"Raise your Awareness Video"*/, MCP.serverURL + ("Videos/SuccessCentre/Wisdom Principle 1 - Raise your Awareness" + format).Replace (" ", "%20"), Media.Type.Video));
		goldMedia.Add(new Media(MCP.Text(1928) + MCP.Text (1915)/*"Develop Possibility Consciousness Video"*/, MCP.serverURL + ("Videos/SuccessCentre/Wisdom Principle 2 - Develop Possibility Consciousness" + format).Replace (" ", "%20"), Media.Type.Video));
		goldMedia.Add(new Media(MCP.Text(1929) + MCP.Text (1915)/*"Find Balance Video"*/, MCP.serverURL + ("Videos/SuccessCentre/Wisdom Principle 3 - Find Balance" + format).Replace (" ", "%20"), Media.Type.Video));
		goldMedia.Add(new Media(MCP.Text(1930) + MCP.Text (1915)/*"Be On Purpose Video"*/, MCP.serverURL + ("Videos/SuccessCentre/Wisdom Principle 4 - Be On Purpose" + format).Replace (" ", "%20"), Media.Type.Video));
		goldMedia.Add(new Media(MCP.Text(1931) + MCP.Text (1915)/*"Become Fully Response-Able Video"*/, MCP.serverURL + ("Videos/SuccessCentre/Wisdom Principle 5 - Become Fully Response-Able" + format).Replace (" ", "%20"), Media.Type.Video));
		goldMedia.Add(new Media(MCP.Text(1932) + MCP.Text (1915)/*"Maintain a Positive Focus Video"*/, MCP.serverURL + ("Videos/SuccessCentre/Wisdom Principle 6 - Maintain a Positive Focus" + format).Replace (" ", "%20"), Media.Type.Video));
		goldMedia.Add(new Media(MCP.Text(1933) + MCP.Text (1915)/*"Involve to Evolve Video"*/, MCP.serverURL + ("Videos/SuccessCentre/Wisdom Principle 7 - Involve to Evolve" + format).Replace (" ", "%20"), Media.Type.Video));
		
		videoManagerObject = transform.GetChild(0).gameObject;
		videoManagerObject.SetActive (false);
		
		// Create the GUI.
		CreateGUI ();
		
		localCanvas.transform.localScale = Vector2.zero;
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Find the current canvas.
		localCanvas = MakeCanvas ();
		
		// Make the background window.
		GameObject windowBack = MakeWindowBackground (localCanvas, new Rect(Screen.width * 0.01f, Screen.height * 0.01f , Screen.width * 0.98f, Screen.height * 0.98f), window_back);
		windowBack.name = "WindowBackground";

		// Make the title bar.
		GameObject titleBar = MakeTitleBar (localCanvas, MCP.Text (1901)/*"Success Centre"*/);
		titleBar.name = "TitleBar";

		// If the user is logged in...
		if(MCP.userInfo != null)
		{
			// Make the side gizmo with the gizmo menu.
			MakeSideGizmo(localCanvas, true);
		}
		// Otherwise...
		else
		{
			// Make the side gizmo without the gizmo menu.
			MakeSideGizmo(localCanvas, false);
		}
		
		// Make the bronze button.
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			BronzeButton
		};
		GameObject bronzeButton = MakeIconButton (localCanvas, MCP.Text (1902)/*"Bronze"*/, iconButton, bronzeSprite, new Rect(Screen.width * 0.05f, Screen.height * 0.25f, Screen.width * 0.25f, Screen.height * 0.4f), funcList, "InnerLabel");
		bronzeButton.name = "BronzeButton";
		bronzeButton.transform.GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
		
		GameObject bronzeText = MakeLabel(localCanvas, MCP.Text (1905)/*Learn about our success stories*/, new Rect(Screen.width * 0.1f, Screen.height * 0.7f, Screen.width * 0.15f, Screen.height * 0.25f));
		bronzeText.name = "BronzeText";
		
		// Make the silver button.
		funcList = new List<MyVoidFunc>
		{
			SilverButton
		};
		GameObject silverButton = MakeIconButton (localCanvas, MCP.Text (1903)/*"Silver"*/, iconButton, silverSprite, new Rect(Screen.width * 0.375f, Screen.height * 0.25f, Screen.width * 0.25f, Screen.height * 0.4f), funcList, "InnerLabel");
		silverButton.name = "SilverButton";
		silverButton.transform.GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
		
		GameObject silverText = MakeLabel(localCanvas, MCP.Text (1906)/*The process for finding you purpose*/, new Rect(Screen.width * 0.425f, Screen.height * 0.7f, Screen.width * 0.15f, Screen.height * 0.25f));
		silverText.name = "SilverText";

		// Make the gold button.
		funcList = new List<MyVoidFunc>
		{
			GoldButton
		};
		GameObject goldButton = MakeIconButton (localCanvas, MCP.Text (1904)/*"Gold"*/, iconButton, goldSprite, new Rect(Screen.width * 0.7f, Screen.height * 0.25f, Screen.width * 0.25f, Screen.height * 0.4f), funcList, "InnerLabel");
		goldButton.name = "GoldButton";
		goldButton.transform.GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
		
		GameObject goldText = MakeLabel(localCanvas, MCP.Text (1907)/*Video & audio for Inspiration & Wisdom*/, new Rect(Screen.width * 0.75f, Screen.height * 0.7f, Screen.width * 0.15f, Screen.height * 0.25f));
		goldText.name = "GoldText";
		
	}
	
	/// <summary>
	/// Process the bronze button.
	/// </summary>
	void BronzeButton()
	{
		currentCat = Category.Bronze;
		
		// If the bundle is not currently downloaded, download it.
		if(MCP.bronzeAssetBundle == null)
		{
			GetAssetBundle (Category.Bronze);
		}
		// Otherwise, use the downloaded version.
		else
		{
			CreateDownloadingPopup();
		}
	}
	
	/// <summary>
	/// Process the silver button.
	/// </summary>
	void SilverButton()
	{
		// If the user is not logged in, stop them from getting to the silver resources.
		if(MCP.userInfo == null)
		{
			if(loginPopup == null)
			{
				GameObject loginBackground = MakeImage (localCanvas, new Rect(Screen.width * -0.0f, Screen.height * -0.0f, Screen.width, Screen.height), menuBackground);
				loginBackground.name = "LoginBackground";
				
				loginPopup = MakePopup (localCanvas, defaultPopUpWindow, MCP.Text (1910)/*"Not Logged In"*/, MCP.Text (1911)/*"Please log in to access these resources."*/, MCP.Text (205)/*"Dismiss"*/, CommonTasks.DestroyParent);
				loginPopup.name = "LoginPopup";
				
				if(Debug.isDebugBuild)
				{
					// Get the dismiss button and move it to the left of the popup.
					GameObject dismissButton = loginPopup.transform.GetChild (2).gameObject;
					Vector3 newPos = dismissButton.GetComponent<RectTransform>().localPosition;
					newPos.x = Screen.width * 0.05f;
					dismissButton.GetComponent<RectTransform>().localPosition = newPos;
					dismissButton.GetComponent<RectTransform>().sizeDelta = new Vector2(dismissButton.GetComponent<RectTransform>().sizeDelta.x * 1.25f, dismissButton.GetComponent<RectTransform>().sizeDelta.y);
					
					// Make login button.
					MakeButton(loginPopup, MCP.Text (1936)/*"Log In"*/, buttonBackground, new Rect (Screen.width * 0.8f, Screen.height * 0.4f, Screen.width * 0.15f, Screen.height * 0.05f), CommonTasks.LoadLevel, "Login_Screen", "OrangeText");
				}
				
				// Make a debug button to allow testing of the guided goal map.
				List<MyVoidFunc> funcList = new List<MyVoidFunc>
				{
					delegate
					{
						currentCat = Category.Silver;
						
						if(MCP.silverAssetBundle == null)
						{
							GetAssetBundle (Category.Silver);
						}
						// Otherwise, use the downloaded version.
						else
						{
							CreateDownloadingPopup();
						}
						
						Destroy (loginPopup);
					}
				};
				
				if(Debug.isDebugBuild || MCP.siteURL.Contains ("192.168.1.146"))
				{
					MakeButton(loginPopup, "Debug", buttonBackground, new Rect (Screen.width * 0.875f, Screen.height * 0.55f, Screen.width * 0.1f, Screen.height * 0.05f), funcList, "OrangeText");
				}
				
				loginBackground.transform.SetParent (loginPopup.transform);
				loginBackground.transform.localScale = Vector3.one;
				loginBackground.transform.SetAsFirstSibling();
			}
			
			return;
		}
		
		// If the user does not have access to silver resources...
		if(MCP.userInfo.memberProfile.goalMapLevel == "0" || MCP.userInfo.memberProfile.goalMapLevel == "")
		{
			if(upgradePopup == null)
			{
				GameObject upgradeBackground = MakeImage (localCanvas, new Rect(Screen.width * -0.0f, Screen.height * -0.0f, Screen.width, Screen.height), menuBackground);
				upgradeBackground.name = "UpgradeBackground";
				
				upgradePopup = MakePopup (localCanvas, defaultPopUpWindow, MCP.Text (1912)/*"Please Upgrade"*/, MCP.Text (1913)/*"Your account is not a high enough level to access these resouces. Click continue to upgrade."*/, MCP.Text (205)/*"Dismiss"*/, CommonTasks.DestroyParent);
				upgradePopup.name = "UpgradePopup";
				// Get the dismiss button and move it to the left of the popup.
				GameObject dismissButton = upgradePopup.transform.GetChild (2).gameObject;
				Vector3 newPos = dismissButton.GetComponent<RectTransform>().localPosition;
				newPos.x = Screen.width * 0.05f;
				dismissButton.GetComponent<RectTransform>().localPosition = newPos;
				dismissButton.GetComponent<RectTransform>().sizeDelta = new Vector2(dismissButton.GetComponent<RectTransform>().sizeDelta.x * 1.25f, dismissButton.GetComponent<RectTransform>().sizeDelta.y);
				
				// Make continue button.
				MakeButton(upgradePopup, MCP.Text (218)/*"Continue"*/, buttonBackground, new Rect (Screen.width * 0.8f, Screen.height * 0.4f, Screen.width * 0.15f, Screen.height * 0.05f), CommonTasks.LoadLevel, "Upgrade_Screen", "OrangeText");
				
				// Make a debug button to allow testing of the guided goal map.
				List<MyVoidFunc> funcList = new List<MyVoidFunc>
				{
					delegate
					{
						currentCat = Category.Silver;
						
						if(MCP.silverAssetBundle == null)
						{
							GetAssetBundle (Category.Silver);
						}
						// Otherwise, use the downloaded version.
						else
						{
							CreateDownloadingPopup();
						}
						
						Destroy (upgradePopup);
					}
				};
				
				if(Debug.isDebugBuild || MCP.siteURL.Contains ("192.168.1.146"))
				{
					MakeButton(upgradePopup, "Debug", buttonBackground, new Rect (Screen.width * 0.875f, Screen.height * 0.55f, Screen.width * 0.1f, Screen.height * 0.05f), funcList, "OrangeText");
				}
				
				upgradeBackground.transform.SetParent (upgradePopup.transform);
				upgradeBackground.transform.localScale = Vector3.one;
				upgradeBackground.transform.SetAsFirstSibling();
			}
			
			return;
		}
		
		currentCat = Category.Silver;
		
		if(MCP.silverAssetBundle == null)
		{
			GetAssetBundle (Category.Silver);
		}
		// Otherwise, use the downloaded version.
		else
		{
			CreateDownloadingPopup();
		}
	}
	
	/// <summary>
	/// Process the gold button.
	/// </summary>
	void GoldButton()
	{
		// If the user is not logged in, stop them from getting to the gold resources.
		if(MCP.userInfo == null)
		{
			if(loginPopup == null)
			{
				GameObject loginBackground = MakeImage (localCanvas, new Rect(Screen.width * -0.0f, Screen.height * -0.0f, Screen.width, Screen.height), menuBackground);
				loginBackground.name = "LoginBackground";
				
				loginPopup = MakePopup (localCanvas, defaultPopUpWindow, MCP.Text (1910)/*"Not Logged In"*/, MCP.Text (1911)/*"Please log in to access these resources."*/, MCP.Text (205)/*"Dismiss"*/, CommonTasks.DestroyParent);
				loginPopup.name = "LoginPopup";
				
				if(Debug.isDebugBuild)
				{
					// Get the dismiss button and move it to the left of the popup.
					GameObject dismissButton = loginPopup.transform.GetChild (2).gameObject;
					Vector3 newPos = dismissButton.GetComponent<RectTransform>().localPosition;
					newPos.x = Screen.width * 0.05f;
					dismissButton.GetComponent<RectTransform>().localPosition = newPos;
					dismissButton.GetComponent<RectTransform>().sizeDelta = new Vector2(dismissButton.GetComponent<RectTransform>().sizeDelta.x * 1.25f, dismissButton.GetComponent<RectTransform>().sizeDelta.y);
					
					// Make login button.
					MakeButton(loginPopup, MCP.Text (1936)/*"Log In"*/, buttonBackground, new Rect (Screen.width * 0.8f, Screen.height * 0.4f, Screen.width * 0.15f, Screen.height * 0.05f), CommonTasks.LoadLevel, "Login_Screen", "OrangeText");
				}
				
				// Make a debug button to allow testing of the guided goal map.
				List<MyVoidFunc> funcList = new List<MyVoidFunc>
				{
					delegate
					{
						currentCat = Category.Gold;
						
						if(MCP.goldAssetBundle == null)
						{
							GetAssetBundle (Category.Gold);
						}
						// Otherwise, use the downloaded version.
						else
						{
							CreateDownloadingPopup();
						}
						
						Destroy (loginPopup);
					}
				};
				if(Debug.isDebugBuild || MCP.siteURL.Contains ("192.168.1.146"))
				{
					MakeButton(loginPopup, "Debug", buttonBackground, new Rect (Screen.width * 0.875f, Screen.height * 0.55f, Screen.width * 0.1f, Screen.height * 0.05f), funcList, "OrangeText");
				}
				
				loginBackground.transform.SetParent (loginPopup.transform);
				loginBackground.transform.localScale = Vector3.one;
				loginBackground.transform.SetAsFirstSibling();
			}
			
			return;
		}
		
		// If the user does not have access to gold resources...
		if(MCP.userInfo.memberProfile.goalMapLevel == "0" || MCP.userInfo.memberProfile.goalMapLevel == "1" || MCP.userInfo.memberProfile.goalMapLevel == "")
		{
			if(upgradePopup == null)
			{
				GameObject upgradeBackground = MakeImage (localCanvas, new Rect(Screen.width * -0.0f, Screen.height * -0.0f, Screen.width, Screen.height), menuBackground);
				upgradeBackground.name = "UpgradeBackground";
				
				upgradePopup = MakePopup (localCanvas, defaultPopUpWindow, MCP.Text (1912)/*"Please Upgrade"*/, MCP.Text (1913)/*"Your account is not a high enough level to access these resouces. Click continue to upgrade."*/, MCP.Text (205)/*"Dismiss"*/, CommonTasks.DestroyParent);
				upgradePopup.name = "UpgradePopup";
				
				// Get the dismiss button and move it to the left of the popup.
				GameObject dismissButton = upgradePopup.transform.GetChild (2).gameObject;
				Vector3 newPos = dismissButton.GetComponent<RectTransform>().localPosition;
				newPos.x = Screen.width * 0.05f;
				dismissButton.GetComponent<RectTransform>().localPosition = newPos;
				dismissButton.GetComponent<RectTransform>().sizeDelta = new Vector2(dismissButton.GetComponent<RectTransform>().sizeDelta.x * 1.25f, dismissButton.GetComponent<RectTransform>().sizeDelta.y);
				
				// Make continue button.
				MakeButton(upgradePopup, MCP.Text (218)/*"Continue"*/, buttonBackground, new Rect (Screen.width * 0.8f, Screen.height * 0.4f, Screen.width * 0.15f, Screen.height * 0.05f), CommonTasks.LoadLevel, "Upgrade_Screen", "OrangeText");
				
				// Make a debug button to allow testing of the guided goal map.
				List<MyVoidFunc> funcList = new List<MyVoidFunc>
				{
					delegate
					{
						currentCat = Category.Gold;
						
						if(MCP.goldAssetBundle == null)
						{
							GetAssetBundle (Category.Gold);
						}
						// Otherwise, use the downloaded version.
						else
						{
							CreateDownloadingPopup();
						}
						
						Destroy (upgradePopup);
					}
				};
				if(Debug.isDebugBuild || MCP.siteURL.Contains ("192.168.1.146"))
				{
					MakeButton(upgradePopup, "Debug", buttonBackground, new Rect (Screen.width * 0.875f, Screen.height * 0.55f, Screen.width * 0.1f, Screen.height * 0.05f), funcList, "OrangeText");
				}
				
				upgradeBackground.transform.SetParent (upgradePopup.transform);
				upgradeBackground.transform.localScale = Vector3.one;
				upgradeBackground.transform.SetAsFirstSibling();
			}
			
			return;
		}
		
		currentCat = Category.Gold;
		
		if(MCP.goldAssetBundle == null)
		{
			GetAssetBundle (Category.Gold);
		}
		// Otherwise, use the downloaded version.
		else
		{
			CreateDownloadingPopup();
		}
	}
	
	void GetAssetBundle(Category cat)
	{
		switch(cat)
		{
		case Category.Bronze:
			StartCoroutine (MCP.GetAssetBundle ("bronzeresources01.bronze", false));
			break;
		case Category.Silver:
			StartCoroutine (MCP.GetAssetBundle ("silverresources01.silver", false));
			break;
		case Category.Gold:
			StartCoroutine (MCP.GetAssetBundle ("goldresources01.gold", false));
			break;
		}
		
		CreateDownloadingPopup();
		
		downloadingBundle = true;
		MCP.isTransmitting = false;
	}
	
	private void CreateDownloadingPopup()
	{
		// Create the downloading popup background.
		popup = MakeWindowBackground(localCanvas, new Rect(Screen.width * -0.5f, Screen.height * -0.5f, Screen.width, Screen.height), menuBackground);
		popup.name = "Popup";
		
		GameObject popupBackground = MakeWindowBackground(localCanvas, new Rect(Screen.width * -0.25f, Screen.height * -0.25f, Screen.width * 0.5f, Screen.height * 0.6f), window_back);
		popupBackground.name = "PopupBackground";
		popupBackground.transform.SetParent (popup.transform, true);
		popupBackground.transform.localScale = Vector3.one;
		
		// Create popup text.
		GameObject popupText = MakeLabel (localCanvas, MCP.Text (1908)/*"Downloading..."*/, new Rect(Screen.width * -0.2f, Screen.height * -0.15f, Screen.width * 0.4f, Screen.height * 0.2f));
		popupText.name = "PopupText";
		popupText.transform.SetParent (popup.transform, true);
		popupText.transform.localScale = Vector3.one;
		
		// Create popup text.
		popupProgressText = MakeLabel (localCanvas, "0%", new Rect(Screen.width * -0.2f, Screen.height * 0.95f, Screen.width * 0.4f, Screen.height * 0.2f));
		popupProgressText.name = "PopupProgressText";
		popupProgressText.transform.SetParent (popup.transform, true);
		popupProgressText.transform.localScale = Vector3.one;
		popupProgressText.SetActive (false);
		
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			delegate
			{
				downloadingBundle = false;
				StopCoroutine ("GetAssetBundle");
				MCP.StopCurrentAssetBundleDownload();
				Destroy (popup);
			}
		};
		GameObject popupCloseButton = MakeButton (localCanvas, MCP.Text (207)/*"Cancel"*/, buttonBackground, new Rect(Screen.width * -0.1f, Screen.height * 0.15f, Screen.width * 0.2f, Screen.height * 0.08f), funcList);
		popupCloseButton.name = "PopupCloseButton";
		popupCloseButton.transform.SetParent (popup.transform, true);
		popupCloseButton.transform.localScale = Vector3.one;
		
		// Create popup spinner.
		downloadSpinner = MakeImage (localCanvas, new Rect(Screen.width * 0.0f, Screen.height * -0.15f, Screen.width * 0.08f, Screen.width * 0.08f), spinnerSprite, false);
		downloadSpinner.name = "downloadSpinner";
		downloadSpinner.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
		downloadSpinner.transform.SetParent (popup.transform, true);
		downloadSpinner.transform.localScale = Vector3.one;
		
		downloadingBundle = true;
	}
	
	/// <summary>
	/// Opens the AV player.
	/// </summary>
	/// <param name="cat">Category.</param>
	void OpenAVPlayer(Category cat)
	{
		string assetPack = "";
		switch(cat)
		{
		case Category.Bronze:
			assetPack = "Bronze";
			
			break;
			
		case Category.Silver:
			assetPack = "Silver";
			
			break;
			
		case Category.Gold:
			assetPack = "Gold";
			
			break;
		}
		
		// Reset first frame timer.
		MenuScreen.firstFrameTimer = 0;
		// Add the AV player script to the camera.
		gameObject.AddComponent<AVPlayer>();
		gameObject.GetComponent<AVPlayer>().sceneCanvas = localCanvas;
		gameObject.GetComponent<AVPlayer>().currentAssetPack = assetPack;
		gameObject.GetComponent<AVPlayer>().videoManagerObject = videoManagerObject;
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update()
	{
		if(screenSize.width == 0 || screenSize.width != Screen.width || screenSize.height != Screen.height)
		{
			screenSize = new Rect(0, 0, Screen.width, Screen.height);
		}
		
		// If downloading has started...
		if(downloadingBundle)
		{
			// If currently downloading...
			if(MCP.isTransmitting)
			{
				// Spin the downloading spinner.
				spinnerRot -= Time.deltaTime * 75f;
				downloadSpinner.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, spinnerRot);
				downloadPercentage = MCP.assetBundleDownload.progress;
				popupProgressText.GetComponent<Text>().text = ((int)downloadPercentage).ToString () + "%";
			}
			// Otherwise, if it has finished...
			else
			{
				downloadingBundle = false;
				
//				if(!alreadyDownloaded && (MCP.downloadedAssetBundle == null || MCP.downloadedAssetBundle.GetAllAssetNames ().Length == 0))
				{
					// Download failed.
//					popup.transform.GetChild (1).gameObject.GetComponent<Text>().text = MCP.Text (1909)/*"Download Failed"*/;
				}
//				else
				{
					// For the current category...
					switch(currentCat)
					{
					case Category.Bronze:
						if(MCP.bronzeAssetBundle == null)
						{
							// Store the asset bundle in MCP.
							MCP.bronzeAssetBundle = MCP.downloadedAssetBundle;
						}
						// Reset the downloaded asset bundle.
						MCP.downloadedAssetBundle = null;
						// Add the AV player and set the variables
						OpenAVPlayer(Category.Bronze);
						break;
						
					case Category.Silver:
						if(MCP.silverAssetBundle == null)
						{
							// Store the asset bundle in MCP.
							MCP.silverAssetBundle = MCP.downloadedAssetBundle;
						}
						// Reset the downloaded asset bundle.
						MCP.downloadedAssetBundle = null;
						// Add the AV player and set the variables
						OpenAVPlayer(Category.Silver);
						break;
						
					case Category.Gold:
						if(MCP.goldAssetBundle == null)
						{
							// Store the asset bundle in MCP.
							MCP.goldAssetBundle = MCP.downloadedAssetBundle;
						}
						// Reset the downloaded asset bundle.
						MCP.downloadedAssetBundle = null;
						// Add the AV player and set the variables
						OpenAVPlayer(Category.Gold);
						break;
					}
					
					Destroy (popup);
				}
			}
		}
	}
	
	public override void DoGUI (){}
}
