using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LoginScreen : MenuScreen
{
	private GameObject localCanvas;
	private GameObject password;
	private GameObject userName;
	private GameObject createAccountButton;
	private GameObject showPasswordIcon;
	private GameObject showPasswordButton;
	private GameObject loggingInBackground;
	private GameObject loggingInSpinner;
	private GameObject popupCanvas;
	private GameObject popupTextObject;
	private GameObject popupButton;
	
	private Sprite rotationSprite;
	private Sprite zingLogo;
	
	private bool loginClicked = false;
	private bool sendingData = false;
	private bool loginProblem = false;
	private bool rememberMe;
	private bool storedPassword = false;
	private bool showPassword = false;
	private bool prevTab = false;
	private float elementHeight;
	private float zingLogoScale;
	private float rotAngle = 0;
	private float timeout = 0;
	private float timeoutLimit = 10f;
	private string passwordToEdit = "PasswordPassword";
	private string userNameToEdit = "Username";
	private string passwordStr = "";
	private Rect loginGroupRect;
	private Rect screenSize;
	private Vector2 pivotPoint;

	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		// Initialise the base.
		Init ();

		// Set default variables.
		MCP.loginCompleted = false;
		MCP.loginSuccess = false;

		// If the user has checked Remember Me, fill in their info.
		if (PlayerPrefs.HasKey ("RememberMe"))
		{
			if (PlayerPrefs.GetInt ("RememberMe") == 1)
			{
				rememberMe = true;
				if (PlayerPrefs.HasKey ("UserName"))
				{
					userNameToEdit = PlayerPrefs.GetString ("UserName");
					if (PlayerPrefs.HasKey ("Password"))
					{
						passwordToEdit = PlayerPrefs.GetString ("Password");
						storedPassword = true;
					}
				}
			}
		}
		passwordStr = passwordToEdit;
		
		// Load resources
		rotationSprite = Resources.Load<Sprite>("GUI/bm_rotate");
		zingLogo = Resources.Load<Sprite>("poweredbyZing");
		
		// Create the scene GUI.
		CreateGUI ();
		
		localCanvas.transform.localScale = Vector2.zero;
	}

	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update ()
	{
		// Make tab switch between input fields.
		if(password != null && userName != null)
		{
			if(password.GetComponent<InputField>().isFocused)
			{
				if(Input.GetKey (KeyCode.Tab) && !prevTab)
				{
					userName.GetComponent<InputField>().Select ();
				}
			}
			else if(userName.GetComponent<InputField>().isFocused)
			{
				if(Input.GetKey (KeyCode.Tab) && !prevTab)
				{
					password.GetComponent<InputField>().Select ();
				}
				
				if(userName.GetComponent<InputField>().text == "Username")
				{
					userName.GetComponent<InputField>().text = "";
				}
			}
		}
		
		prevTab = Input.GetKey(KeyCode.Tab);
		
		// If enter is pressed when input is not selected, try to log in.
		if(Input.GetKey (KeyCode.Return))
		{
			if(!loginProblem)
			{
				loginProblem = false;
				
				// Process Login
				if (rememberMe)
				{
					PlayerPrefs.SetInt ("RememberMe", 1);
					PlayerPrefs.SetString ("UserName", userName.GetComponent<Text> ().text);
					if (!storedPassword)
					{
						PlayerPrefs.SetString ("Password", MCP.Md5Sum (password.GetComponent<InputField> ().text + MCP.secretKey));
					}
				}
				else
				{
					PlayerPrefs.SetInt ("RememberMe", 0);
					PlayerPrefs.SetString ("UserName", "");
					PlayerPrefs.SetString ("Password", "");
				}
				
				// If the user info has been saved...
				if (storedPassword)
				{
					// Use the stored values to login.
					StartCoroutine (MCP.CheckLogin (userNameToEdit, passwordStr));
				}
				// Otherwise...
				else
				{
					// Use the input fields to log in.
					StartCoroutine (MCP.CheckLogin (userName.GetComponent<Text> ().text, MCP.Md5Sum (password.GetComponent<InputField> ().text + MCP.secretKey)));
					Debug.Log ("Username = " + userName.GetComponent<Text> ().text + " Password = " + password.GetComponent<InputField> ().text + MCP.Md5Sum (passwordToEdit + MCP.secretKey));
				}
				loginClicked = true;
				
				// Show the logging in popup.
				loggingInBackground.SetActive (true);
			}
		}
		
		// Process hiding the encrypted password from users.
		if(password.GetComponent<InputField>().text == passwordStr)
		{
			if(password.GetComponent<InputField>().text.Length == password.GetComponent<Text>().text.Length
			   || PlayerPrefs.HasKey ("UserName") && PlayerPrefs.GetString ("UserName") != userName.GetComponent<InputField>().text
			   )
			{
				// Empty the password field.
				password.GetComponent<InputField>().text = "";
				// Activate the show password buttons.
				showPasswordIcon.GetComponent<Button>().interactable = true;
				showPasswordButton.GetComponent<Button>().interactable = true;
				// Change the password store to stop the field emptying if the user types out what was previously stored.
				passwordStr = "\b";
			}
			else
			{
				// Deactivate the show password buttons.
				showPasswordIcon.GetComponent<Button>().interactable = false;
				showPasswordButton.GetComponent<Button>().interactable = false;
			}
			
#if UNITY_ANDROID || UNITY_IPHONE
			// If the touch screen is up...
			if(TouchScreenKeyboard.visible)
			{
				// Empty the password field.
				password.GetComponent<InputField>().text = "";
				// Activate the show password buttons.
				showPasswordIcon.GetComponent<Button>().interactable = true;
				showPasswordButton.GetComponent<Button>().interactable = true;
				// Change the password store to stop the field emptying if the user types out what was previously stored.
				passwordStr = "\b";
			}
#endif
		}
		else
		{
			// Activate the show password buttons.
			showPasswordIcon.GetComponent<Button>().interactable = true;
			showPasswordButton.GetComponent<Button>().interactable = true;
		}
		
		// Process showing the user's password.
		if(showPassword)
		{
			if(password.GetComponent<InputField>().contentType != InputField.ContentType.Alphanumeric)
			{
				password.GetComponent<InputField>().contentType = InputField.ContentType.Alphanumeric;
				password.GetComponent<Text>().text = password.GetComponent<InputField>().text;
			}
		}
		else
		{
			if(password.GetComponent<InputField>().contentType != InputField.ContentType.Password)
			{
				password.GetComponent<InputField>().contentType = InputField.ContentType.Password;
				string s = "";
				for(int i = 0; i < password.GetComponent<InputField>().text.Length; i++)
				{
					// Limit the length of the hidden text.
					if(i > 12)
					{
						break;
					}
					
					s += "*";
				}
				password.GetComponent<Text>().text = s;
			}
		}
		
		if ((MCP.loginSuccess && !sendingData))
		{
			// If the user has set start full screen, set fullscreen to their preference.
			if ( PlayerPrefs.HasKey( "StartFullScreen" ))
			{
				if ( PlayerPrefs.GetInt("StartFullScreen") == 1 )
				{
					Screen.fullScreen = true;
				}	
				else
				{
					Screen.fullScreen = false;
				}
			}
			
			//Temp functionality
			Application.LoadLevel ("Main_Screen");
		}
		
		if (loginClicked && !MCP.loginCompleted)
		{
			// Check the amount of time the user has tried to log in. If this is too long, flag a problem.
			if (timeout > timeoutLimit)
			{
				loginProblem = true;
			}
			// Otherwise...
			else
			{
				// Spin the loggin spinner.
				rotAngle -= 55 * Time.deltaTime;
				loggingInSpinner.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, rotAngle);
				// Update the timeout timer.
				timeout += Time.deltaTime;
			}
		}
		
		if (MCP.loginCompleted && MCP.loginSuccess)
		{
			loginProblem = false;
			
			// Spin the loggin spinner.
			rotAngle -= 55 * Time.deltaTime;
			loggingInSpinner.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, rotAngle);
			
			if (int.Parse (MCP.userInfo.isVerified) != 1)
			{
				
			}
			else
			{
				loginProblem = false;
				MCP.SetPlayerName (userNameToEdit);
				
				if (MCP.userInfo != null)
				{
					if (MCP.userInfo.memberProfile == null && !sendingData)
					{
						// Get the user's member profile.
						StartCoroutine (MCP.GetMemberProfile ());
					}
					
					if (MCP.userInfo.friend_list == null && !sendingData)
					{
						// Get the user's friends.
						StartCoroutine (MCP.GetFriends (MCP.userInfo.username));
						
						sendingData = true;
					}
					else if (MCP.userInfo.friend_list != null)
					{
						sendingData = false;
					}
				}
			}
		}
		
		if (loginClicked && MCP.loginCompleted && !MCP.loginSuccess)
		{
			loginProblem = true;
		}
		
		//Needs thread
		LoginProblem ();
//
//		// If the mouse is over a button, change the cursor to a hand.
//		if (mouseOverCount > 0)
//		{
//#if UNITY_WEBPLAYER
//			Cursor.SetCursor(mouseHandSelect, Vector2.zero, CursorMode.Auto);
//#endif
//			mouseOverCount = 0;
//		}
//		// Otherwise, change it back to a pointer.
//		else
//		{
//#if UNITY_WEBPLAYER
//			Cursor.SetCursor(mouseHandNormal, Vector2.zero, CursorMode.Auto);
//#endif
//		}
	}

	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI ()
	{
		// Find the current canvas.
		localCanvas = MakeCanvas ();

		// Make the background window.
		GameObject windowBack = MakeWindowBackground (localCanvas, new Rect (Screen.width * 0.02f, Screen.height * 0.16f, Screen.width * 0.96f, Screen.height * 0.83f), window_back);
		windowBack.name = "WindowBackground";

		// MAke the title bar.
		MakeTitleBar (localCanvas, MCP.Text (101)/*Login Screen*/);

		// Make the side gizmo.
		MakeSideGizmo (localCanvas, false);
		
		GameObject zingLogoObject = MakeImage (localCanvas, new Rect(Screen.width * 0.85f, Screen.height * 0.01f, Screen.width * 0.1f, Screen.width * 0.1f), zingLogo);
		zingLogoObject.name = "ZingLogoObject";

		// Make the create account button.
		createAccountButton = MakeButton (localCanvas, MCP.Text (102)/*"Create Account"*/, buttonBackground, new Rect (Screen.width * 0.625f, Screen.height * 0.525f, Screen.width * 0.3f, Screen.height * 0.085f), CommonTasks.LoadLevel, "CreateAccount_Screen");
		createAccountButton.name = "CreateAccountButton";
		
		// Make the remember checkbox/button.
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			delegate
			{
				rememberMe = !rememberMe;
			}
		};
		
		Sprite checkSprite = tickBoxIcon;
		if(rememberMe)
		{
			checkSprite = tickBoxCheckIcon;
		}
		
		GameObject rememberIcon = MakeButton (localCanvas, "", checkSprite, new Rect (Screen.width * 0.1f, Screen.height * 0.54f, Screen.height * 0.075f, Screen.height * 0.075f), funcList, "EmptyButton");
		rememberIcon.name = "RememberMeIcon";
		GameObject rememberButton = MakeButton (localCanvas, MCP.Text (103) /*"Remember Me"*/, null, new Rect (Screen.width * 0.17f, Screen.height * 0.555f, Screen.width * 0.235f, Screen.height * 0.08f), funcList, "EmptyButton");
		rememberButton.name = "RememberMeButton";
		rememberButton.transform.GetChild (0).gameObject.GetComponent<Text>().font = defaultFont;
		rememberButton.transform.GetChild (0).gameObject.GetComponent<Text>().fontSize = (int)_fontSize;
		rememberButton.transform.GetChild (0).gameObject.GetComponent<Text>().fontStyle = FontStyle.Normal;
		// Add listener to change sprite.
		rememberIcon.GetComponent<Button>().onClick.AddListener (
			delegate
			{
			if(!loginProblem)
			{
				if(rememberMe)
				{
					rememberIcon.GetComponent<Image>().sprite = tickBoxCheckIcon;
				}
				else
				{
					rememberIcon.GetComponent<Image>().sprite = tickBoxIcon;
				}
			}
		});
		
		rememberButton.GetComponent<Button>().onClick.AddListener (
			delegate
			{
			if(!loginProblem)
			{
				if(rememberMe)
				{
					rememberIcon.GetComponent<Image>().sprite = tickBoxCheckIcon;
				}
				else
				{
					rememberIcon.GetComponent<Image>().sprite = tickBoxIcon;
				}
			}
		});
		
		// Make the show password checkbox/button.
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				showPassword = !showPassword;
			}
		};
		
		checkSprite = tickBoxIcon;
		
		showPasswordIcon = MakeButton (localCanvas, "", checkSprite, new Rect (Screen.width * 0.1f, Screen.height * 0.675f, Screen.height * 0.075f, Screen.height * 0.075f), funcList, "EmptyButton");
		showPasswordIcon.name = "ShowPasswordIcon";
		showPasswordButton = MakeButton (localCanvas, MCP.Text (113) /*"Show Password"*/, null, new Rect (Screen.width * 0.17f, Screen.height * 0.69f, Screen.width * 0.235f, Screen.height * 0.08f), funcList, "EmptyButton");
		showPasswordButton.name = "ShowPasswordButton";
		showPasswordButton.transform.GetChild (0).gameObject.GetComponent<Text>().font = defaultFont;
		showPasswordButton.transform.GetChild (0).gameObject.GetComponent<Text>().fontSize = (int)_fontSize;
		showPasswordButton.transform.GetChild (0).gameObject.GetComponent<Text>().fontStyle = FontStyle.Normal;
		// Add listener to change sprite.
		showPasswordIcon.GetComponent<Button>().onClick.AddListener (
			delegate
			{
				if(!loginProblem)
				{
					if(showPassword)
					{
						showPasswordIcon.GetComponent<Image>().sprite = tickBoxCheckIcon;
					}
					else
					{
						showPasswordIcon.GetComponent<Image>().sprite = tickBoxIcon;
					}
				}
			});
		
		showPasswordButton.GetComponent<Button>().onClick.AddListener (
			delegate
			{
				if(!loginProblem)
				{
					if(showPassword)
					{
						showPasswordIcon.GetComponent<Image>().sprite = tickBoxCheckIcon;
					}
					else
					{
						showPasswordIcon.GetComponent<Image>().sprite = tickBoxIcon;
					}
				}
			});
		
		// Make the forgot password button.
		GameObject forgotButton = MakeButton (localCanvas, MCP.Text (104) /*"Forgotten Password"*/, null, new Rect (Screen.width * 0.08f, Screen.height * 0.775f, Screen.width * 0.4f, Screen.height * 0.085f), CommonTasks.OpenUrl, MCP.siteURL+"GoalWeb/resetpassword.php", "EmptyButton");
		forgotButton.name = "ForgottonButton";
		forgotButton.transform.GetChild (0).gameObject.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
		
		// Make the username label.
		GameObject usernameLabel = MakeLabel (localCanvas, MCP.Text (105)/*"Username"*/, new Rect (Screen.width * 0.1f, Screen.height * 0.21f, Screen.height * 0.3f, Screen.height * 0.085f));
		usernameLabel.name = "UsernameLabel";
		usernameLabel.GetComponent<Text>().fontSize = (int)_fontSize_Large;
		usernameLabel.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;

		// Make the username input field.
		userName = MakeInputField (localCanvas, userNameToEdit, new Rect (Screen.width * 0.325f, Screen.height * 0.21f, Screen.width * 0.6f, Screen.height * 0.08f), false);
		userName.name = "UserName";
//		userName.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;

		// Make the password label.
		GameObject passwordLabel = MakeLabel (localCanvas, MCP.Text (106)/*"Password"*/, new Rect (Screen.width * 0.1f, Screen.height * 0.3575f, Screen.height * 0.3f, Screen.height * 0.1f));
		passwordLabel.name = "PasswordLabel";
		passwordLabel.GetComponent<Text>().fontSize = (int)_fontSize_Large;
		passwordLabel.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;

		// Make the password input field.
		password = MakeInputField (localCanvas, passwordToEdit, new Rect (Screen.width * 0.325f, Screen.height * 0.365f, Screen.width * 0.6f, Screen.height * 0.08f), true);
		password.name = "Password";
//		password.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
		password.GetComponent<InputField>().onEndEdit.AddListener(
			delegate
			{
				if(!loginProblem && Input.GetKey (KeyCode.Return))
				{
					loginProblem = false;
					
					// Process Login
					if (rememberMe)
					{
						PlayerPrefs.SetInt ("RememberMe", 1);
						PlayerPrefs.SetString ("UserName", userName.GetComponent<Text> ().text);
						if (!storedPassword)
						{
							PlayerPrefs.SetString ("Password", MCP.Md5Sum (password.GetComponent<InputField> ().text + MCP.secretKey));
						}
					}
					else
					{
						PlayerPrefs.SetInt ("RememberMe", 0);
						PlayerPrefs.SetString ("UserName", "");
						PlayerPrefs.SetString ("Password", "");
					}
					
					// If the user info has been saved...
					if (storedPassword)
					{
						// Use the stored values to login.
						StartCoroutine (MCP.CheckLogin (userNameToEdit, passwordStr));
					}
					// Otherwise...
					else
					{
						// Use the input fields to log in.
						StartCoroutine (MCP.CheckLogin (userName.GetComponent<Text> ().text, MCP.Md5Sum (password.GetComponent<InputField> ().text + MCP.secretKey)));
						Debug.Log ("Username = " + userName.GetComponent<Text> ().text + " Password = " + password.GetComponent<InputField> ().text + MCP.Md5Sum (passwordToEdit + MCP.secretKey));
					}
					loginClicked = true;
					
					// Show the logging in popup.
					loggingInBackground.SetActive (true);
				}
			});
		
		// If the user has a stored password...
		if(storedPassword)
		{
			// Set the password input field text to the stored password.
			password.GetComponent<InputField>().text = passwordStr;
		}
		// Make the password text a load of asterixis.
		password.GetComponent<Text>().text = "***********";
		// Set the username input field text to the stored username.
		userName.GetComponent<InputField>().text = userNameToEdit;
		
		// Change the stored password flag on changing the input field.
		password.GetComponent<InputField>().onValueChange.AddListener (
			delegate
			{
				storedPassword = false;
		});
		
		if(Debug.isDebugBuild)
		{
			GameObject debugBuildText = MakeLabel (localCanvas, "Debug Enabled", new Rect(Screen.width * 0.3f, Screen.height * 0.75f, Screen.width * 0.4f, Screen.height * 0.1f), TextAnchor.MiddleCenter);
			debugBuildText.name = "DebugBuildText";
			debugBuildText.GetComponent<Text>().color = bmOrange;
		}
		
		if(MCP.siteURL.Contains ("192.168"))
		{
			GameObject devBuildText = MakeLabel (localCanvas, "Development Server", new Rect(Screen.width * 0.3f, Screen.height * 0.85f, Screen.width * 0.4f, Screen.height * 0.1f), TextAnchor.MiddleCenter);
			devBuildText.name = "DevBuildText";
			devBuildText.GetComponent<Text>().color = bmOrange;
		}
		
		// Make the login button.
		GameObject loginButton = MakeLoginButton (localCanvas, MCP.Text (107)/*"Login"*/, buttonBackground, new Rect (Screen.width * 0.125f, Screen.height * 0.35f, Screen.width * 0.3f, Screen.height * 0.17f));
		loginButton.name = "LoginButton";
		
		// Make the logging in background.
		loggingInBackground = MakeImage (localCanvas, new Rect(0, 0, Screen.width, Screen.height), menuBackground);
		loggingInBackground.name = "LoggingInBackground";
		
		// Make the logging in panel.
		GameObject loggingInPanel = MakeImage (localCanvas, new Rect(Screen.width * 0.25f, Screen.height * 0.3f, Screen.width * 0.5f, Screen.height * 0.45f), popup_back, true);
		loggingInPanel.name = "LoggingInPanel";
		loggingInPanel.transform.SetParent (loggingInBackground.transform);
		
		// Make the text for the logging in popup.
		GameObject loggingInTextObject = MakeLabel (localCanvas, MCP.Text (108)/*"Logging In..."*/, new Rect(Screen.width * 0.35f, Screen.height * 0.35f, Screen.width * 0.3f, Screen.height * 0.2f), TextAnchor.MiddleCenter, "InnerLabel");
		loggingInTextObject.name = "LoggingInTextObject";
		loggingInTextObject.transform.SetParent (loggingInBackground.transform);
		
		// Make the login spinner.
		loggingInSpinner = MakeImage (localCanvas, new Rect(Screen.width * 0.5f, Screen.height * 0.45f, Screen.width * 0.08f, Screen.width * 0.08f), rotationSprite, false);
		loggingInSpinner.name = "LoggingInSpinner";
		// Move the pivot to the center of the object.
		loggingInSpinner.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
		loggingInSpinner.transform.SetParent (loggingInBackground.transform);
		loggingInBackground.SetActive (false);
		
		// Login problems
		popupCanvas = MakeCanvas (true);
		popupCanvas.GetComponent<Canvas>().planeDistance = 9f;
		
		// Make problem screen faders.
		GameObject popupScreenBackground = MakeImage (popupCanvas, new Rect(0, 0, Screen.width, Screen.height), menuBackground, true);
		popupScreenBackground.name = "PopupDarkerScreenBackground";
		GameObject popupDarkerScreenBackground = MakeImage (popupCanvas, new Rect(0, 0, Screen.width, Screen.height), menuBackground, true);
		popupDarkerScreenBackground.name = "PopupDarkerScreenBackground";
		
		// Make problem popup background.
		GameObject popupBackground = MakeImage (popupCanvas, new Rect(Screen.width * 0.25f, Screen.height * 0.3f, Screen.width * 0.5f, Screen.height * 0.45f), popup_back, true);
		popupBackground.name = "PopupBackground";
		
		// Make the text for the problem popup.
		popupTextObject = MakeLabel (popupCanvas, "", new Rect(Screen.width * 0.35f, Screen.height * 0.35f, Screen.width * 0.3f, Screen.height * 0.2f), "InnerLabel");
		popupTextObject.name = "PopupTextObject";
		
		// Make the problem popup close button.
		GameObject popupButtonBackground = MakeImage (popupCanvas, new Rect(Screen.width * 0.45f, Screen.height * 0.575f, Screen.width * 0.1f, Screen.height * 0.08f), buttonBackground, true);
		popupButtonBackground.name = "PopupButtonBackground";
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				MCP.loginSuccess = false;
				MCP.loginCompleted = false;
				loginClicked = false;
				timeout = 0;
				loginProblem = false;
				if(MCP.userInfo != null)
				{
					MCP.userInfo.loginSuccess = "";
				}
				createAccountButton.GetComponent<Button>().interactable = true;
				
				popupCanvas.SetActive (false);
			}
		};
		popupButton = MakeButton (popupCanvas, MCP.Text (203)/*"OK"*/, new Rect(Screen.width * 0.45f, Screen.height * 0.575f, Screen.width * 0.1f, Screen.height * 0.08f), funcList, "InnerLabel");
		popupButton.name = "PopupButton";
		popupButton.GetComponent<Text>().color = bmOrange;
		popupButton.GetComponent<Button>().image = popupButtonBackground.GetComponent<Image>();
		
		popupCanvas.SetActive (false);
	}

	/// <summary>
	/// Makes the login button.
	/// </summary>
	/// <returns>The login button.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="buttonText">Button text.</param>
	/// <param name="content">Content.</param>
	/// <param name="rect">Rect.</param>
	public GameObject MakeLoginButton (GameObject parent, string buttonText, Sprite content, Rect rect)
	{
		// Instantiate button.
		GameObject instButton = new GameObject ();
		instButton.name = buttonText;
		
		// Set button name.
		instButton.AddComponent<Image> ();
		instButton.AddComponent<Button> ();
		instButton.AddComponent<HoverPointer>();
		
		// Create button text.
		GameObject text = new GameObject ();
		text.AddComponent<Text> ();
		text.transform.SetParent( instButton.transform );
		// Set button text attributes.
		text.GetComponent<Text> ().resizeTextForBestFit = false;
		text.GetComponent<Text> ().fontSize = (int)_fontSize_xLarge;
		text.GetComponent<Text> ().fontStyle = FontStyle.Bold;
		text.GetComponent<Text> ().text = buttonText;
		text.GetComponent<Text> ().font = defaultFont;
		text.GetComponent<Text>().color = bmOrange;
		text.GetComponent<Text> ().alignment = TextAnchor.MiddleCenter;
		instButton.GetComponent<RectTransform> ().anchorMin = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().anchorMax = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().pivot = Vector2.zero;

		instButton.transform.SetParent(parent.transform);
		
		// Set button position.
		instButton.transform.localPosition = new Vector2 (rect.x, -rect.y);
		instButton.transform.localScale = Vector3.one;

		// Set the button size
		Vector2 sizeModifier = new Vector2 (rect.width, rect.height);
		instButton.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		sizeModifier = new Vector2 (rect.width * 0.9f, rect.height * 0.9f);
		text.GetComponent<RectTransform> ().sizeDelta = sizeModifier;

		// Set sprite.
		if (content != null)
		{
			instButton.GetComponent<Image> ().type = Image.Type.Sliced;
			instButton.GetComponent<Image> ().sprite = content;
		}
		else
		{
			Debug.Log ("NullTexture on: " + buttonText);
		}
		
		// Reset button listeners.
		instButton.GetComponent<Button> ().onClick.RemoveAllListeners ();
		// Add onClick listeners to button.
		instButton.GetComponent<Button> ().onClick.AddListener (() =>
			{
				if(!loginProblem)
				{
					loginProblem = false;
		
					// Process Login
					if (rememberMe)
					{
						PlayerPrefs.SetInt ("RememberMe", 1);
						PlayerPrefs.SetString ("UserName", userName.GetComponent<Text> ().text.TrimEnd (new char[]{' '}));
						if (!storedPassword)
						{
							PlayerPrefs.SetString ("Password", MCP.Md5Sum (password.GetComponent<InputField> ().text + MCP.secretKey));
						}
					}
					else
					{
						PlayerPrefs.SetInt ("RememberMe", 0);
						PlayerPrefs.SetString ("UserName", "");
						PlayerPrefs.SetString ("Password", "");
					}
		
					// If the user info has been saved...
					if (storedPassword)
					{
						// Use the stored values to login.
						StartCoroutine (MCP.CheckLogin (userNameToEdit, passwordStr));
					}
					// Otherwise...
					else
					{
						// Use the input fields to log in.
						StartCoroutine (MCP.CheckLogin (userName.GetComponent<Text> ().text.TrimEnd (new char[]{' '}), MCP.Md5Sum (password.GetComponent<InputField> ().text + MCP.secretKey)));
						Debug.Log ("Username = " + userName.GetComponent<Text> ().text + " Password = " + password.GetComponent<InputField> ().text + MCP.Md5Sum (passwordToEdit + MCP.secretKey));
					}
					loginClicked = true;
					
					// Show the logging in popup.
					loggingInBackground.SetActive (true);
				}
			});

		return instButton;
	}

	/// <summary>
	/// Processes login problems.
	/// </summary>
	void LoginProblem ()
	{
		if (loginProblem)
		{
			// Show the problem popup.
			popupCanvas.SetActive (true);
			loggingInBackground.SetActive (false);
			createAccountButton.GetComponent<Button>().interactable = false;
			
			// Output a message depending on the info returned by the server.
			if (MCP.userInfo != null)
			{
				if (MCP.userInfo.loginSuccess.CompareTo ("unverified") == 0)
				{
					popupTextObject.GetComponent<Text>().text = MCP.Text (109);/*"Sorry your account has not been verified. \n   Please check your email\n for an activation link.";*/
				}
				else if (MCP.userInfo.loginSuccess.CompareTo ("disabled") == 0)
				{
					popupTextObject.GetComponent<Text>().text = MCP.Text (110);/*"Sorry your account has been disabled, please contact hello@zingup.com.";*/
				}
				else
				{
					popupTextObject.GetComponent<Text>().text = MCP.Text (112);/*"Incorrect Username/Password.";*/
				}
			}
			else
			{
				popupTextObject.GetComponent<Text>().text = MCP.Text(112);/*"Incorrect Username/Password.";*/
			}
		}
	}
	
	/// <summary>
	/// Process back button.
	/// </summary>
	public override void Back ()
	{
		// Reset user info.
		MCP.userInfo = null;
		// Go back to the landing screen.
		BackToMenu ("Landing_Screen");
	}
	
	public override void DoGUI() {}
}
