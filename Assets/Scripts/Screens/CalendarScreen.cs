using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Globalization;
using System.Collections.Generic;

public class CalendarScreen : MenuScreen
{
	public List<GameObject> reminderDots;
	public GameObject[] dayLabels;         //Holds 35 labels
	public GameObject[] monthLabels;		// Holds Months of the year.
	public string[] months;  
	public int[] daysInMonth;				//Holds the months
	public GameObject headerLabel;         //The label used to show the Month
	public GameObject[] dayInputBox;
	public GameObject[] hourSetButton;
	public GameObject[] hourUpdateButton;
	private GameObject localCanvas;
	private GameObject popupObject;
	private GameObject popupPanel;
	private GameObject dayScrollArea;
	
	private Sprite dotSprite;
	
	public string[] hourLabels;
	
	private bool dotsUpdated = false;
	private bool firstFrame = true;
	
	private float scrollOffset = 0;
	private int currentMonth = 0;
	private int monthCount = 0;
	
#if !UNITY_IOS && !UNITY_ANDROID
	private int currentDay = -1;
#endif
	
	public DateTime iMonth;
	public DateTime reminderDate;
	private DateTime curDisplay;
	
	private bool updateDay = false;
	private bool updateComplete = false;
	
	private MCP.Events tempEvents;
	
	private Dictionary<int, GameObject> internalScenes;
	private InternalState internalState;
	
	private List<ReminderNotif> reminderList;
	
	public List<ELANNotification> reminderNotif;

#if UNITY_IOS
	public List<UnityEngine.iOS.LocalNotification> reminderNotifList;
#endif

	public class ReminderNotif
	{
		public DateTime date;
		public String message;
		public ELANNotification notif;
	}
	
	private enum InternalState
	{
		month = 1,
		day = 2,
		popup = 3
	}
	
	// 
	/// <summary>
	/// Used for initialization
	/// </summary>
	void Start ()
	{
		// Initialise the base.
		Init ();
		
		// Initialize arrays/lists
		internalScenes = new Dictionary<int, GameObject> ();
		reminderNotif = new List<ELANNotification> ();
		reminderList = new List<ReminderNotif> ();
#if UNITY_IOS
		reminderNotifList = new List<UnityEngine.iOS.LocalNotification>();
#endif
		
		// Get the user's events, or initialize them if they have none.
		if(MCP.userInfo != null)
		{
			StartCoroutine (MCP.GetEvents ());
		}
		else
		{
			tempEvents = new MCP.Events();
			tempEvents.eventReminders = new List<MCP.EventReminders>();
		}
		
		dayInputBox = new GameObject[24];
		hourSetButton = new GameObject[24];
		hourUpdateButton = new GameObject[24];
		
		// Load resources
		dotSprite = Resources.Load<Sprite>("GUI/colorcircle_picker");
		
		// Set initial variables.
		internalState = InternalState.month;

		// Get data to create months.
		CreateMonths();    
		// Set the days to the correct labels
		CreateCalendar();
		// Create the scene GUI.
		CreateGUI ();
		
		localCanvas.transform.localScale = Vector2.zero;
	}
	
	/// <summary>
	/// Creates the reminder dots.
	/// </summary>
	void CreateReminderDots()
	{
		if(MCP.userInfo != null && MCP.userInfo.eventReminders != null && MCP.userInfo.eventReminders.eventReminders != null)
		{
			// Get a reference to the user's reminders.
			List<MCP.EventReminders> reminders = MCP.userInfo.eventReminders.eventReminders;
			
			// For each reminder...
			for(int i = 0; i < reminders.Count; i++)
			{
				// Get the month of this reminder.
				int month = int.Parse ( reminders[i].date.Split (new char[]{'/'})[0] ) - 1;
				
				// If this reminder is in this month...
				if(month == currentMonth)
				{
					// Find which day it is on.
					int day = int.Parse ( reminders[i].date.Split (new char[]{'/'})[1] ) - 1;
					// Get the position of the day label.
					Rect rect = dayLabels[day].transform.GetChild (0).gameObject.GetComponent<RectTransform>().rect;
					rect.x = Camera.main.WorldToScreenPoint (dayLabels[day].transform.GetChild (0).position).x;
					rect.y = Screen.height - Camera.main.WorldToScreenPoint (dayLabels[day].transform.GetChild (0).position).y;
					
					// Create a dot in the to left corner of this point.
					GameObject reminderDot = MakeImage (localCanvas, new Rect(rect.x + (rect.width * 0.7f) - (Screen.width * 0.5f), rect.y - (rect.height * 0.9f) - (Screen.height * 0.5f), rect.width * 0.2f, rect.height * 0.2f), dotSprite);
					reminderDot.name = "ReminderDot M: " + month + ", D: " + day;
					reminderDots.Add (reminderDot);
				}
			}
			
			// Flag the completion of the update.
			dotsUpdated = true;
		}
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update()
	{
		// If reminders in a day have changed...
		if (updateDay && !updateComplete) 
		{
			// Update the reminders.
			UpdateDayReminders();
		}
		
		// If in the main calendar state and the reminder dots have not been created...
		if(!dotsUpdated && !firstFrame && internalState == InternalState.month)
		{
			// Create the reminder dots.
			CreateReminderDots ();
		}
		
		// If the confirmation popup is currently on the screen...
		if(internalState == InternalState.popup)
		{
			// If the popup has been destroyed...
			if(popupObject == null)
			{
				// Flag a change in the day's reminders.
				updateDay = true;
				updateComplete = false;
				dotsUpdated = false;
				// Reset the state to the calendar.
				ChangeState ((int)InternalState.day);
				
				// Destroy the current reminder dots.
				for(int j = reminderDots.Count - 1; j >= 0; j--)
				{
					Destroy(reminderDots[j]);
					reminderDots.RemoveAt (j);
				}
			}
		}
		
#if !UNITY_ANDROID && !UNITY_IPHONE
		// Loop through the month buttons and change the text color if the mouse is hovering over them.
		for(int i = 0; i < monthLabels.Length; i++)
		{
			Rect rect = monthLabels[i].transform.GetChild (0).gameObject.GetComponent<RectTransform>().rect;
			rect.x = Camera.main.WorldToScreenPoint (monthLabels[i].transform.GetChild (0).position).x;
			rect.y = Camera.main.WorldToScreenPoint (monthLabels[i].transform.GetChild (0).position).y;
			
			if(rect.Contains (Input.mousePosition))
			{
				monthLabels[i].transform.GetChild (0).GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
			}
			else if(i != currentMonth)
			{
				monthLabels[i].transform.GetChild (0).GetChild (0).gameObject.GetComponent<Text>().color = Color.white;
			}
		}
		
		// Loop through the day buttons and change the text color if the mouse is hovering over them or they are selected.
		for(int i = 0; i < dayLabels.Length; i++)
		{
			Rect rect = dayLabels[i].transform.GetChild (0).gameObject.GetComponent<RectTransform>().rect;
			rect.x = Camera.main.WorldToScreenPoint (dayLabels[i].transform.GetChild (0).position).x;
			rect.y = Camera.main.WorldToScreenPoint (dayLabels[i].transform.GetChild (0).position).y;
			
			if(rect.Contains (Input.mousePosition))
			{
				dayLabels[i].transform.GetChild (0).GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
			}

			else if(i == currentDay)
			{
				dayLabels[i].transform.GetChild (0).GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
			}
			else
			{
				dayLabels[i].transform.GetChild (0).GetChild (0).gameObject.GetComponent<Text>().color = Color.white;
			}
		}
#endif

		firstFrame = false;
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Find the current canvas.
		localCanvas = MakeCanvas ();
		
		// Make a panel for the background.
		GameObject windowBack = MakeWindowBackground (localCanvas, new Rect(Screen.width * 0.01f, Screen.height * 0.15f , Screen.width * 0.98f, Screen.height * 0.83f), window_back);
		windowBack.name = "WindowBackground";
		
		// Make the title bar.
		MakeTitleBar (localCanvas, MCP.Text (501)/*"Calendar"*/);
		
		// Make the side gizmo.
		MakeSideGizmo (localCanvas, true, true);
		
		// Make the parent object for the calendar objects.
		GameObject calendar = MakePanel(localCanvas, new Rect(Screen.width * 0.125f, Screen.height * 0.1f , Screen.width * 0.7f, Screen.height * 0.8f));
		calendar.name = "YearPanel";
		
		// Initialize the month labels.
		monthLabels = new GameObject[12];
		string monthFormat = "MMM";
		DateTime monthDate;
		int monthOffsetX = 0;
		int monthOffsetY = 0;
		scrollOffset = 0;
		
		// Set up button hover
		SpriteState spriteState = new SpriteState();
		spriteState.highlightedSprite = buttonBackground;
		
		// For each month...
		for (int i = 0; i < 12; i++) 
		{
			monthDate = new DateTime ();
			monthDate = monthDate.AddMonths(i);
			
			// Move every other month down and reset the x offset.
			if(i % 2 == 0)
			{
				monthOffsetY++;
				monthOffsetX = 0;
			}
			
			monthOffsetX++;
			
			// Make the parent object for the month button.
			monthLabels[i] = MakeLabel(calendar, "", new Rect((monthOffsetX * Screen.width * 0.125f) - (Screen.width * 0.2f), (monthOffsetY * Screen.height * 0.1f) + (Screen.height * -0.7f), Screen.width * 0.15f, Screen.height * 0.001f));
			
			// Make the button to change to this month.
			GameObject monthButton = MakeActionButton(monthLabels[i], monthDate.Date.ToString(monthFormat).ToUpper (), new Rect(0, 0, Screen.width * 0.1f, Screen.height * 0.08f),GoToMonth,i,"ActionButton");
			
			// Change the button transition state to sprite swap.
#if !UNITY_ANDROID && !UNITY_IPHONE
			monthButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
			monthButton.GetComponent<Button>().spriteState = spriteState;
#endif
			
			// If this is the current month, change the background to the highlighted background, and the text to orange.
			if(i == currentMonth)
			{
				monthButton.GetComponent<Image>().sprite = buttonBackground;
				monthButton.transform.GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
			}
			// Otherwise, change the button back to normal.
			else
			{
				monthButton.transform.GetChild (0).gameObject.GetComponent<Text>().color = Color.white;
			}
			
			// Set the button attributes.
			monthButton.transform.GetChild (0).gameObject.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
			monthButton.transform.GetChild (0).gameObject.GetComponent<Text>().fontSize = (int)_fontSize;
			monthLabels[i].name = monthDate.Date.ToString(monthFormat);
		}
		
		dayLabels = new GameObject[31];
		calendar.name = "Calendar";
		
		// Add the calendar to the internal scenes.
		internalScenes.Add ((int)InternalState.month, calendar);
		
		int weekDay = 0;
		float heightSpacer = Screen.height * 0.2f;
		
		// For each day...
		for(int d = 0; d < dayLabels.Length; d++)
		{
			// Make the day parent object.
			dayLabels[d] = MakeLabel(localCanvas, "", new Rect((weekDay * Screen.width * 0.08f) + (Screen.width * 0.2f), heightSpacer, Screen.width * 0.06f, Screen.height * 0.1f));
			
			// Make the button to go to the day's reminder list.
			GameObject dayButton = MakeActionButton(dayLabels[d], (d + 1).ToString (), new Rect(0, 0, Screen.width * 0.06f, Screen.height * 0.1f), DoStateButtonID, d, "ActionButton");
			// Set the button's transition state to use sprite swap.
#if !UNITY_ANDROID && !UNITY_IPHONE
			dayButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
			dayButton.GetComponent<Button>().spriteState = spriteState;
#endif
			
			// Set day button attributes.
			dayButton.transform.GetChild (0).gameObject.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
			dayButton.transform.GetChild (0).gameObject.GetComponent<Text>().color = Color.white;
			dayButton.transform.GetChild (0).gameObject.GetComponent<Text>().fontSize = (int)_fontSize;
			dayLabels[d].name = "Day " + d;
			dayLabels[d].transform.SetParent(calendar.transform,false);
			
			// Move the days down the screen every 7 days.
			if(weekDay > 5)
			{
				weekDay = 0;
				heightSpacer += Screen.height * 0.12f;
			}
			else
			{
				weekDay++;
			}
		}
		
		// Create the parent object for the reminders list screen.
		GameObject dayPanel = MakePanel(localCanvas, new Rect(Screen.width * 0.125f, Screen.height * 0.1f , Screen.width * 0.7f, Screen.height * 0.8f));
		dayPanel.name = "DayPanel";
		internalScenes.Add((int)InternalState.day, dayPanel);
		dayPanel.SetActive (false);
		
		// Make a scroll parent for the reminder objects.
		GameObject dayScroll = MakePanel (localCanvas, new Rect (Screen.width * 0.125f, Screen.height * -0.1f, Screen.width * 0.7f, Screen.height * 2.5f));
		dayScroll.name = "Day Scroll";
		
		// Initialize labels for each hour of the day.
		hourLabels = new string[24];
		
		// Set each default reminder text.
		for (int i = 0; i < 24; i++) 
		{
			hourLabels[i] = MCP.Text (504);/*"Enter a reminder.";*/
		}
		
		// For each hour in the day...
		for(int i = 0; i < 24; i++) 
		{
			// Make the parent object for the hour reminder objects.
			GameObject curHour = MakeLabel(dayScroll, (i).ToString("00")+":00", new Rect (Screen.width * 0.025f, (i*(Screen.height * 0.1f)) + (Screen.height * -2.1875f), Screen.width * 0.1f, Screen.height * 0.08f), null);
			curHour.name = (i).ToString() + " Hour";
			
			float width;
#if UNITY_ANDROID || UNITY_IPHONE
			width = Screen.width * 0.35f;
#else
			width = Screen.width * 0.575f;
#endif
			
			// Make the input field to type in the reminder text.
			dayInputBox[i] = MakeInputField(dayScroll, hourLabels[i], new Rect ( Screen.width * 0.125f, (i*(Screen.height * 0.1f)) + (Screen.height * -2.2f), width, Screen.height * 0.08f), false);
			dayInputBox[i].name = (i).ToString() + " Label";
			// Set input field attributes.
			dayInputBox[i].GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;
			dayInputBox[i].GetComponent<InputField>().characterLimit = 36;
			dayInputBox[i].GetComponent<Text>().fontSize = (int)_fontSize_Small;
#if !UNITY_ANDROID && !UNITY_IPHONE
			dayInputBox[i].GetComponent<InputField>().interactable = false;
#endif
			
#if UNITY_ANDROID || UNITY_IPHONE
			// Make a button to set/unset the reminder.
			hourSetButton[i] = MakeActionButton(dayScroll, MCP.Text (505)/*"Set"*/, new Rect( Screen.width * 0.5f, (i*(Screen.height * 0.1f)) + (Screen.height * 0.3f) /*+ ((Screen.width * 0.035f) * 0.5f)*/, Screen.width * 0.1f, Screen.height * 0.07f), SetReminder, (i));
			hourSetButton[i].name = (i).ToString() + " Button";
			
			// Make a button to update the reminder.
			hourUpdateButton[i] = MakeActionButton(dayScroll, MCP.Text (506)/*"Update"*/, new Rect( Screen.width * 0.6125f, (i*(Screen.height * 0.1f)) + (Screen.height * 0.3f)/*+ ((Screen.width * 0.035f) * 0.5f)*/, Screen.width * 0.125f, Screen.height * 0.07f), UpdateReminder, (i));
			hourUpdateButton[i].name = (i).ToString() + " Button";
			// Default this button to start disabled.
			hourUpdateButton[i].GetComponent<Button>().interactable = false;
#endif
			
			// Offset for the next hour.
			scrollOffset += Screen.height * 0.1f;
		}
		
		// Make a scroll rect for the reminder objects in the day.
		dayScrollArea = MakeScrollRect (dayPanel, new Rect (0, Screen.height * -0.75f , Screen.width * 0.75f, Screen.height * 0.75f), dayScroll);
		dayScrollArea.name = "Day Scroll Area";
		// Set scroll rect attributes.
		dayScrollArea.GetComponent<ScrollRect> ().horizontal = false;
		// Get a reference to the scroll content.
		GameObject scrollContent = dayScrollArea.transform.GetChild (0).GetChild (0).gameObject;
		// Resize the scroll area to contain all of the hour obects.
		scrollContent.GetComponent<RectTransform> ().sizeDelta = new Vector2(dayScrollArea.GetComponent<RectTransform> ().sizeDelta.x, scrollOffset);
		
		// Make a parent for the popup objects.
		popupPanel = MakePanel (localCanvas, new Rect (Screen.width * 0.125f, Screen.height * 0.1f, Screen.width * 0.7f, Screen.height * 0.8f));
		popupPanel.name = "PopupPanel";
		internalScenes.Add ((int)InternalState.popup, popupPanel);
		popupPanel.SetActive (false);
	}
	
	/// <summary>
	/// Initializes the months.
	/// </summary>
	void CreateMonths()
	{
		// Initialize months.
		months = new string[12];
		daysInMonth = new int[12];
		iMonth = System.DateTime.Now;
		
		// Set the values for the names and number of days in each month.
		for(int i = 0; i < 12; ++i)
		{
			months[i] = iMonth.ToString("MMMM");
			daysInMonth[i] = System.DateTime.DaysInMonth(System.DateTime.Now.Year, i+1);
		}
	}
	
	/// <summary>
	/// Sets the days to their correct labels.
	/// </summary>
	void CreateCalendar()
	{
		// Gets the current month.
		curDisplay = iMonth;
		
		// Get the number of days in the month.
		while(curDisplay.Month == iMonth.Month)
		{
			curDisplay = curDisplay.AddDays(1);
		}
	}
	
	/// <summary>
	/// Action button delegate.
	/// </summary>
	/// <returns><c>true</c>, if state is changed.</returns>
	/// <param name="i">The index.</param>
	bool DoStateButtonID(int i)
	{
		// Go to the reminders list for the selected day.
		internalState = InternalState.day;
		reminderDate = new DateTime ();
		reminderDate = reminderDate.AddYears (DateTime.Now.Year - 1);
		reminderDate = reminderDate.AddMonths (monthCount);
		reminderDate = reminderDate.AddDays (i);
#if !UNITY_IPHONE && !UNITY_ANDROID
		currentDay = i;
#endif
		
		// Flag that the buttons have been pressed.
		updateDay = true;
		ChangeState ((int)internalState);
		dotsUpdated = false;
		
		// Reset the reminder dots.
		for(int j = reminderDots.Count - 1; j >= 0; j--)
		{
			Destroy(reminderDots[j]);
			reminderDots.RemoveAt (j);
		}
		
		// Get a reference to the scroll content.
		GameObject scrollContent = dayScrollArea.transform.GetChild (0).GetChild (0).gameObject;
		// Reset scroll area position
		Vector3 newPos = scrollContent.GetComponent<RectTransform>().localPosition;
		newPos.y = -scrollOffset * 2;
		scrollContent.GetComponent<RectTransform>().localPosition = newPos;
		dayScrollArea.GetComponent<ScrollRect> ().velocity = new Vector2(0, 1000);
		
		return true;
	}
	
	/// <summary>
	/// Processes clicking on a new month.
	/// </summary>
	/// <returns><c>true</c>, if to month selected, <c>false</c> otherwise.</returns>
	/// <param name="i">The index.</param>
	bool GoToMonth(int i)
	{
		monthCount = i;
#if !UNITY_IPHONE && !UNITY_ANDROID
		currentDay = -1;
#endif

		// For each month...
		for(int j = 0; j < monthLabels.Length; j++)
		{
			// If this is the currently selected month...
			if(i == j)
			{
				// Change to selected sprite.
				monthLabels[j].transform.GetChild (0).gameObject.GetComponent<Image>().sprite = buttonBackground;
				// Change the text color.
				monthLabels[j].transform.GetChild (0).GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
			}
			// Otherwise...
			else
			{
				// Change to normal sprite.
				monthLabels[j].transform.GetChild (0).gameObject.GetComponent<Image>().sprite = Sprite.Create (skin.GetStyle ("ActionButton").normal.background, new Rect(0,0,skin.GetStyle ("ActionButton").normal.background.width,skin.GetStyle ("ActionButton").normal.background.height),new Vector2(0.5f,0.5f));
				// Change the text color.
				monthLabels[j].transform.GetChild (0).GetChild (0).gameObject.GetComponent<Text>().color = Color.white;
			}
		}
		
		// Update the number of days in the month.
		UpdateMonthDays(System.DateTime.DaysInMonth(DateTime.Now.Year, (i+1)));
		
		// Reset the month state.
		ChangeState ((int)internalState);
		
		currentMonth = i;
		
		// Reset the reminder dots.
		dotsUpdated = false;
		for(int j = reminderDots.Count - 1; j >= 0; j--)
		{
			Destroy(reminderDots[j]);
			reminderDots.RemoveAt (j);
		}
		
		return true;
	}
	
	/// <summary>
	/// Updates the reminder.
	/// </summary>
	/// <returns><c>true</c>, if reminder was updated, <c>false</c> otherwise.</returns>
	/// <param name="i">The index.</param>
	bool UpdateReminder(int i)
	{
		// Reset reminder Date Time.
		reminderDate = reminderDate.Date;
		// Add the hours for the button clicked.
		reminderDate = reminderDate.AddHours (i);
		
		// Set up the reminder info.
		ReminderNotif tempReminder = new ReminderNotif();
		tempReminder.notif = GetComponent<ELANNotification> ();
		tempReminder.date = reminderDate;
		tempReminder.message = dayInputBox[i].GetComponent<InputField>().text;
		// Add it to the list.
		reminderList.Add (tempReminder);
		
		// Create an event reminder to store the reminders.
		MCP.EventReminders eventNotif = new MCP.EventReminders ();
		eventNotif.date = reminderDate.ToString();
		eventNotif.message = dayInputBox[i].GetComponent<InputField>().text;
		
		// Create a reference to the event reminders list.
		List<MCP.EventReminders> eventRemindersList;
		
		// Set the event reminders.
		if(MCP.userInfo != null)
		{
			eventRemindersList = MCP.userInfo.eventReminders.eventReminders;
		}
		else
		{
			eventRemindersList = tempEvents.eventReminders;
		}
		
		// If there is already a reminder for this time, update it.
		bool updating = false;
		for(int j = 0; j < eventRemindersList.Count; j++)
		{
			if(eventRemindersList[j].date == eventNotif.date)
			{
				eventRemindersList[j].message = dayInputBox[i].GetComponent<InputField>().text;
				updating = true;
			}
		}
		
		// If this is not an update, then add the event to the list. This should never happen.
		if(!updating)
		{
			Debug.Log ("Event not found to update. Adding new event to the list.");
			eventRemindersList.Add (eventNotif);
		}
			
		if(MCP.userInfo != null)
		{
			// Update the events on the server.
			StartCoroutine(MCP.UpdateEvents ());
		}
		
		// Change to the popup state.
		ChangeState ((int)InternalState.popup);
		
		// Set the popup message.
		string popupTitle = MCP.Text (508);/*"Reminder Updated";*/
		//TODO: Change date to local format.
		string popupMessage = eventNotif.message + "\n\n" + eventNotif.date;;
		
		// Create the popup.
		popupObject = MakePopup(localCanvas, defaultPopUpWindow, popupTitle, popupMessage, MCP.Text (205)/*"Dismiss"*/, CommonTasks.DestroyParent);
		popupObject.name = "popupBackground";
		
		return true;
	}
	
	/// <summary>
	/// Action button set reminder delegate
	/// </summary>
	/// <returns><c>true</c>, if reminder was set, <c>false</c> otherwise.</returns>
	/// <param name="i">The index.</param>
	bool SetReminder(int i)
	{
		// Reset reminder Date Time.
		reminderDate = reminderDate.Date;
		// Add the hours for the button clicked.
		reminderDate = reminderDate.AddHours (i);
		
		// Set up the reminder info.
		ReminderNotif tempReminder = new ReminderNotif();
		tempReminder.notif = GetComponent<ELANNotification> ();
		tempReminder.date = reminderDate;
		tempReminder.message = dayInputBox[i].GetComponent<InputField>().text;
		reminderList.Add (tempReminder);
		
		// Create an event reminder to store the reminder.
		MCP.EventReminders eventNotif = new MCP.EventReminders ();
		eventNotif.date = reminderDate.ToString();
		eventNotif.message = dayInputBox[i].GetComponent<InputField>().text;
		
		// Create a local reference to the event reminders list.
		List<MCP.EventReminders> eventRemindersList;
		
		if(MCP.userInfo != null)
		{
			eventRemindersList = MCP.userInfo.eventReminders.eventReminders;
		}
		else
		{
			eventRemindersList = tempEvents.eventReminders;
		}
		
		bool remove = false;
		
		// If there is already a reminder for this time, remove it.
		for(int j = 0; j < eventRemindersList.Count; j++)
		{
			if(eventRemindersList[j].date == eventNotif.date)
			{
				eventRemindersList.RemoveAt(j);
				remove = true;
			}
		}
		
		// Otherwise, add a new event.
		if(!remove)
		{
			eventRemindersList.Add (eventNotif);
			
#if UNITY_ANDROID
			// Send notification to device
			tempReminder.notif.message =  tempReminder.message;
			tempReminder.notif.title = "Goal Mapping";
			TimeSpan waitTime = tempReminder.date.Subtract (System.DateTime.Now);
			tempReminder.notif.delayTypeTime = EnumTimeType.Seconds;
			tempReminder.notif.delay = (int)waitTime.TotalSeconds;
			tempReminder.notif.send ();
#elif UNITY_IPHONE
			UnityEngine.iOS.LocalNotification notif = new UnityEngine.iOS.LocalNotification();
			notif.alertBody = tempReminder.message;
			notif.fireDate = tempReminder.date;
			UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(notif);
#endif
		}
		
		if(MCP.userInfo != null)
		{
			// Update the events on the server.
			StartCoroutine(MCP.UpdateEvents ());
		}
		
		// Change popup text depending on whether the reminder was set or removed.
		string popupTitle = "";
		string popupMessage = MCP.Text (510);/*"Reminder Removed";*/
		if(!remove)
		{
			popupTitle = MCP.Text (509);/*"Reminder Set";*/
			//TODO: Change date to local format.
			popupMessage = eventNotif.message + "\n\n" + eventNotif.date;
		}
		
		// Change to the popup state.
		ChangeState ((int)InternalState.popup);
		
		// Create the popup object.
		popupObject = MakePopup(localCanvas, defaultPopUpWindow, popupTitle, popupMessage, MCP.Text (205)/*"Dismiss"*/, CommonTasks.DestroyParent);
		popupObject.name = "popupBackground";
		popupPanel.SetActive (false);
		
		return true;
	}
	
	/// <summary>
	/// Updates the day reminders information from the stored event reminders.
	/// </summary>
	void UpdateDayReminders()
	{
		if (!MCP.isTransmitting) 
		{
			DateTime tempDate;
			
			// Reset the day reminders.
			for (int i = 0; i < 24; i++) 
			{
				dayInputBox[i].GetComponent<InputField>().text = MCP.Text (504);/* "Enter a reminder.";*/
				if(hourSetButton != null && hourSetButton[i] != null)
				{
					hourSetButton[i].transform.GetChild (0).GetComponent<Text>().text = MCP.Text(505);/*"Set";*/
					hourUpdateButton[i].GetComponent<Button>().interactable = false;
				}
			}
			
			// Update the days with any set reminders.
			for (int i = 0; i < 24; i++)
			{
				reminderDate = reminderDate.Date;
				reminderDate = reminderDate.AddHours (i);
				
				// Make a local reference to the event reminders list.
				List<MCP.EventReminders> dayRemindersList;
				
				if(MCP.userInfo != null)
				{
					dayRemindersList = MCP.userInfo.eventReminders.eventReminders;
				}
				else
				{
					dayRemindersList = tempEvents.eventReminders;
				}
				
				// For each reminder in the list...
				for (int j = 0; j < dayRemindersList.Count; j++)
				{
					// Get the date/time of the reminder.
					tempDate = Convert.ToDateTime (dayRemindersList [j].date);
					
					// If the date/time is on this day, put the reminder info in this slot.
					if (tempDate == reminderDate)
					{
						// Fill in the notification message.
						dayInputBox[i].GetComponent<InputField>().text = dayRemindersList [j].message;
						
						// Change set button to unset and activate the update button.
						if(hourSetButton != null && hourSetButton[i] != null)
						{
							hourSetButton[i].transform.GetChild (0).GetComponent<Text>().text = MCP.Text (507);/*"Unset";*/
							hourUpdateButton[i].GetComponent<Button>().interactable = true;
						}
					}
				}
			}
			
			// Reset the updateDay flag.
			updateDay = false;
		}
	}
	
	/// <summary>
	/// Updates the select day objects.
	/// </summary>
	/// <param name="days">The number of days in the month..</param>
	void UpdateMonthDays(int days)
	{
		// Convert from human-readable day count to computer day count.
		days = days - 1;
		
		// For each potential day...
		for(int i = 0; i < 31; i++)
		{
			if(i > days)
			{
				dayLabels[i].SetActive (false);
			}
			else
			{
				dayLabels[i].SetActive (true);
			}
		}
	}
	
	/// <summary>
	/// Changes the state.
	/// </summary>
	/// <returns><c>true</c>, if state was changed, <c>false</c> otherwise.</returns>
	/// <param name="newState">The new state.</param>
	bool ChangeState(int newState)
	{
		// For each state...
		for (int i = 1; i < internalScenes.Count; i++) 
		{
			// If this is not the new state...
			if(	i != newState )
			{
				// Deactivate the scene's parent object.
				internalScenes[i].SetActive(false);
			}
		}
		
		// Activate this scene's parent object.
		internalScenes [newState].SetActive (true);
		
		// Set the stored internal state.
		internalState = (InternalState)newState;
		
		return true;
	}
	
	public override void Back ()
	{
		// If there is a state to go to, update to that state.
		if ((int)internalState - 1 >= 1) 
		{
			ChangeState((int)internalState - 1);
			
			// Reset the reminder dots.
			dotsUpdated = false;
			for(int j = reminderDots.Count - 1; j >= 0; j--)
			{
				Destroy(reminderDots[j]);
				reminderDots.RemoveAt (j);
			}
		}
		// Otherwise, go back to main screen.
		else 
		{
			LoadMenu("Main_Screen");
		}
	}
	
	public override void DoGUI() {}
}