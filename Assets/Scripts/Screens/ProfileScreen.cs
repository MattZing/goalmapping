using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

//TODO: Try on an old GUI texture for iOS.

public class ProfileScreen : MenuScreen
{
	private List<GameObject> displayObjects;
	private List<GameObject> editObjects;
	private List<GameObject> iconObjects;
	private List<GameObject> photoObjects;
	private List<GameObject> takePhotoObjects;
	private GameObject localCanvas;
	private GameObject usernameText;
	private GameObject nameText;
	private GameObject avatarBackground;
	private GameObject avatarForeground;
	private GameObject editButton;
	public GameObject avatarPrefab;
	private GameObject takePhotoPanel;
	private GameObject photoPanel;
	private GameObject countryInput;
	private GameObject countryInputLabel;
	private GameObject aboutMeInput;
	private GameObject aboutMeInputLabel;
	private GameObject myGoalsInput;
	private GameObject myGoalsInputLabel;
	private GameObject takePictureButton;
	private GameObject iconPhotoSwitchButton;
	private GameObject noCamFoundText;

	public Texture avatarImage;
	public Texture avatarBackgroundTex;
	public Sprite leftArrowSprite;
	public Sprite rightArrowSprite;
	private Sprite webcamSprite;
	private Sprite currentPhotoSprite;
	private Sprite[] flipVertSprite;
	private Sprite[] flipHoriSprite;
	
	private List<MCP.MyAvatar> avatarList;
	private bool editing = false;
	private bool takingPhoto = false;
	private bool iconShowing = true;
	private bool camSet = false;
	private bool requested = false;
	private bool playSet = false;
	private bool exiting = false;
	private bool sizePopupPopped = false;
	private InternalState internalState;
	private int avatarIndex;
	private int colorIndex;
	private float startTakePhotoPanelX;
	private SpriteTex tempSpriteTex;
	private string playername;
	private string username;
	private string frontCamName;
	private string backCamName;
	
	private WebCamTexture webcamTexture;
	private WebCamDevice[] devices;
	
	private const int photoSize = 256;
	private MCP.UserInfo localUserInfo;
	
	enum InternalState
	{
		display,
		edit
	}
	
	public class SpriteTex
	{
		public Sprite avatarImage;
		public Sprite avatarBackground;
	}
	
	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		// Initialise the base.
		Init ();
		
		localUserInfo = new MCP.UserInfo();
		
		if( MCP.friendMemberProfile != null )
		{
			localUserInfo.memberProfile = MCP.friendMemberProfile;
			localUserInfo.username = MCP.friendUsername;
			localUserInfo.firstName = MCP.friendMemberProfile.firstName;
			localUserInfo.lastName = MCP.friendMemberProfile.lastName;
		}
		else
		{
			localUserInfo = MCP.userInfo;
		}
		
		// Get the avatar object.
		avatarPrefab = GameObject.Find ("AvatarImage");
		
		// Initialize lists.
		avatarList = new List<MCP.MyAvatar>();
		displayObjects = new List<GameObject>();
		editObjects = new List<GameObject>();
		iconObjects = new List<GameObject>();
		photoObjects = new List<GameObject>();
		takePhotoObjects = new List<GameObject>();
		flipHoriSprite = new Sprite[2];
		flipVertSprite = new Sprite[2];
		
		// Load resources.
		leftArrowSprite = Resources.Load<Sprite> ("left_arrow");
		rightArrowSprite = Resources.Load<Sprite> ("right_arrow");
		flipVertSprite[0] = Resources.Load<Sprite> ("GUI/flip_vert");
		flipVertSprite[1] = Resources.Load<Sprite> ("GUI/flip_vert_active");
		flipHoriSprite[0] = Resources.Load<Sprite> ("GUI/flip_horiz");
		flipHoriSprite[1] = Resources.Load<Sprite> ("GUI/flip_horiz_active");
		
		// Set default avatars.
		MCP.MyAvatar defaultMAvatar = new MCP.MyAvatar();
		defaultMAvatar.indexColor = 0;
		defaultMAvatar.indexX = 0;
		defaultMAvatar.indexY = 0;
		defaultMAvatar.name = "Male Avatar";
		defaultMAvatar.sheetIndex = 0;
		
		avatarList.Add(defaultMAvatar);
		
		MCP.MyAvatar defaultFAvatar = new MCP.MyAvatar();
		defaultFAvatar.indexColor = 0;
		defaultFAvatar.indexX = 1;
		defaultFAvatar.indexY = 0;
		defaultFAvatar.name = "Female Avatar";
		defaultFAvatar.sheetIndex = 0;
		
		avatarList.Add(defaultFAvatar);
		
		// Get the user's selected avatar.
		if(localUserInfo != null && localUserInfo.memberProfile != null && localUserInfo.memberProfile.profileImage != null)
		{
			for(int i = 0; i < avatarList.Count; i++)
			{
				if(avatarList[i].indexX == localUserInfo.memberProfile.profileImage.indexX && avatarList[i].indexY == localUserInfo.memberProfile.profileImage.indexY)
				{
					avatarIndex = i;
				}
			}
			
			colorIndex = localUserInfo.memberProfile.profileImage.indexColor;
		}
		else
		{
			avatarIndex = 0;
			colorIndex = 0;
		}
		
        if(localUserInfo != null)
		StartCoroutine (MCP.GetProfilePhoto ( localUserInfo.username ));
		
		// If user is using a photo, default to photo
		if(localUserInfo == null || localUserInfo.memberProfile == null)
		{
			Debug.Log ("No member profile found");
			LoadMenu ("Main_Screen");
			return;
		}
		
//#if !UNITY_IOS
		if(localUserInfo.memberProfile.usingPhoto == "1")
		{
			iconShowing = false;
		}
//#endif
		
		// Create the scene GUI.
		CreateGUI ();
		
		localCanvas.transform.localScale = Vector2.zero;
		
		if( MCP.friendMemberProfile != null )
		{
			if( MCP.friendMemberProfile.numberOfGoalMaps != "Not Found" )
			{
				// Update the avatar object.
				tempSpriteTex = UpdateAvatar (avatarIndex, avatarList, colorIndex);
				avatarBackground.GetComponent<Image>().sprite = tempSpriteTex.avatarBackground;
				avatarForeground.GetComponent<Image>().sprite = tempSpriteTex.avatarImage;
			}
		}
		else
		{
			if( MCP.userInfo.memberProfile != null )
			{			
				// Update the avatar object.
				tempSpriteTex = UpdateAvatar (avatarIndex, avatarList, colorIndex);
				avatarBackground.GetComponent<Image>().sprite = tempSpriteTex.avatarBackground;
				avatarForeground.GetComponent<Image>().sprite = tempSpriteTex.avatarImage;
			}
		}
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Find the current canvas.
		localCanvas = MakeCanvas ();
		
		// Make the background window.
		GameObject windowBack = MakeWindowBackground (localCanvas, new Rect(Screen.width * 0.01f, Screen.height * 0.01f , Screen.width * 0.98f, Screen.height * 0.98f), window_back);
		windowBack.name = "WindowBackground";
		
		// Make the title bar.
		GameObject titleBar = MakeTitleBar (localCanvas, localUserInfo.username + MCP.Text (1501)/*"'s Profile"*/);
		titleBar.name = "TitleBar";
		
		// Make the side gizmo.
		MakeSideGizmo (localCanvas, true);
		
		if( MCP.friendMemberProfile != null )
		{
			if( MCP.friendMemberProfile.numberOfGoalMaps == "Not Found" )
			{
				MakePopup( localCanvas, new Rect( Screen.width * 0.1f, Screen.height * 0.2f, Screen.width * 0.8f, Screen.height * 0.6f ), "No Profile Found", "Please contact us for more information.", "Main Menu", CommonTasks.LoadLevel, "Main_Screen" );
				return;
			}
		}
		else
		{
			if( MCP.userInfo.memberProfile == null )
			{
				MakePopup( localCanvas, new Rect( Screen.width * 0.1f, Screen.height * 0.2f, Screen.width * 0.8f, Screen.height * 0.6f ), "No Profile Found", "Please contact us for more information.", "Main Menu", CommonTasks.LoadLevel, "Main_Screen" );
				return;
			}
		}
		// Make the avatar background.
		GameObject leftPanelBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.1f, Screen.height * 0.175f, Screen.width * 0.2f, Screen.height * 0.75f), window_back, true);
		leftPanelBackground.name = "LeftPanelBackground";
		
		// Make the avatar.
		avatarBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.1f, Screen.height * 0.175f, Screen.width * 0.2f, Screen.width * 0.2f), window_back);
		avatarBackground.name = "AvatarBackground";
		iconObjects.Add (avatarBackground);
		
		// Make the avatar.
		avatarForeground = MakeImage (localCanvas, new Rect(Screen.width * 0.145f, (Screen.height * 0.175f) + (Screen.width * 0.05f), Screen.width * 0.11f, Screen.width * 0.11f), window_back);
		avatarForeground.name = "AvatarForeground";
		iconObjects.Add (avatarForeground);
		
		// Make the choose avatar label.
		GameObject chooseAvatarLabel = MakeLabel (localCanvas, MCP.Text (1502)/*"Choose Avatar:"*/, new Rect(Screen.width * 0.125f, Screen.height * 0.515f, Screen.width * 0.15f, Screen.height * 0.08f));
		chooseAvatarLabel.name = "ChooseAvatarLabel";
		chooseAvatarLabel.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		chooseAvatarLabel.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Overflow;
		editObjects.Add (chooseAvatarLabel);
		iconObjects.Add (chooseAvatarLabel);
		
		// Make the change avatar left button.
		List<MyVoidFunc> funcList = new List<MyVoidFunc>()
		{
			delegate
			{
				// Reduce the avatar index.
				avatarIndex--;
				
				// If the index is lower than 0, set this to the highest number of avatars.
				if(avatarIndex < 0)
				{
					avatarIndex = avatarList.Count - 1;
				}
				
				// Update the avatar object.
				tempSpriteTex = UpdateAvatar (avatarIndex, avatarList, colorIndex);
				avatarBackground.GetComponent<Image>().sprite = tempSpriteTex.avatarBackground;
				avatarForeground.GetComponent<Image>().sprite = tempSpriteTex.avatarImage;
			}
		};
		GameObject avatarLeftButton = MakeButton (localCanvas, "", leftArrowSprite, new Rect(Screen.width * 0.125f, Screen.height * 0.575f, Screen.width * 0.04f, Screen.width * 0.04f), funcList);
		avatarLeftButton.name = "AvatarLeftButton";
		editObjects.Add (avatarLeftButton);
		iconObjects.Add (avatarLeftButton);
		
		// Make the change avatar right button.
		funcList = new List<MyVoidFunc>()
		{
			delegate
			{
				// Increase the avatar index.
				avatarIndex++;
				
				// If the index is higher than the number of avatars, set this to 0.
				if(avatarIndex > avatarList.Count - 1)
				{
					avatarIndex = 0;
				}
				
				// Update the avatar object.
				tempSpriteTex = UpdateAvatar (avatarIndex, avatarList, colorIndex);
				avatarBackground.GetComponent<Image>().sprite = tempSpriteTex.avatarBackground;
				avatarForeground.GetComponent<Image>().sprite = tempSpriteTex.avatarImage;
			}
		};
		GameObject avatarRightButton = MakeButton (localCanvas, "", rightArrowSprite, new Rect(Screen.width * 0.2375f, Screen.height * 0.575f, Screen.width * 0.04f, Screen.width * 0.04f), funcList);
		avatarRightButton.name = "AvatarRightButton";
		editObjects.Add (avatarRightButton);
		iconObjects.Add (avatarRightButton);
		
		// Make the choose color label.
		GameObject chooseColorLabel = MakeLabel (localCanvas, MCP.Text (1503)/*"Choose Color:"*/, new Rect(Screen.width * 0.125f, Screen.height * 0.675f, Screen.width * 0.15f, Screen.height * 0.08f));
		chooseColorLabel.name = "ChooseColorLabel";
		chooseColorLabel.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		chooseColorLabel.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Overflow;
		editObjects.Add (chooseColorLabel);
		iconObjects.Add (chooseColorLabel);
		
		// Make the choose color left button.
		funcList = new List<MyVoidFunc>()
		{
			delegate
			{
				// Decrease the color index.
				colorIndex--;
				
				// If the color index is below 0, set it to the highest value.
				if(colorIndex < 0)
				{
					colorIndex = 5;
				}
				
				// Update the avatar object.
				tempSpriteTex = UpdateAvatar (avatarIndex, avatarList, colorIndex);
				avatarBackground.GetComponent<Image>().sprite = tempSpriteTex.avatarBackground;
				avatarForeground.GetComponent<Image>().sprite = tempSpriteTex.avatarImage;
			}
		};
		GameObject colorLeftButton = MakeButton (localCanvas, "", leftArrowSprite, new Rect(Screen.width * 0.125f, Screen.height * 0.735f, Screen.width * 0.04f, Screen.width * 0.04f), funcList);
		colorLeftButton.name = "ColorLeftButton";
		editObjects.Add (colorLeftButton);
		iconObjects.Add (colorLeftButton);
		
		funcList = new List<MyVoidFunc>()
		{
			delegate
			{
				// Increase the color index.
				colorIndex++;
				
				// If the color index goes above the number of colors available, set it to 0.
				if(colorIndex > 5)
				{
					colorIndex = 0;
				}
				
				// Update the avatar object.
				tempSpriteTex = UpdateAvatar (avatarIndex, avatarList, colorIndex);
				avatarBackground.GetComponent<Image>().sprite = tempSpriteTex.avatarBackground;
				avatarForeground.GetComponent<Image>().sprite = tempSpriteTex.avatarImage;
			}
		};
		GameObject colorRightButton = MakeButton (localCanvas, "", rightArrowSprite, new Rect(Screen.width * 0.2375f, Screen.height * 0.735f, Screen.width * 0.04f, Screen.width * 0.04f), funcList);
		colorRightButton.name = "ColorRightButton";
		editObjects.Add (colorRightButton);
		iconObjects.Add (colorRightButton);
		
		photoPanel = MakeImage (localCanvas, new Rect(Screen.width * 0.11f, Screen.height * 0.19f, Screen.width * 0.18f, Screen.height * 0.225f), currentPhotoSprite);
		photoPanel.name = "PhotoPanel";
		displayObjects.Add (photoPanel);
		editObjects.Add (photoPanel);
		photoObjects.Add (photoPanel);
		
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				Texture2D t = photoPanel.GetComponent<Image>().sprite.texture;
				t = FlipTexture(t);
				
				if(MCP.loadedTexture != null)
				{
					MCP.loadedTexture = FlipTexture (MCP.loadedTexture);
				}
				
				photoPanel.GetComponent<Image>().sprite = Sprite.Create (t, new Rect(0, 0, t.width, t.height), new Vector2(0.5f, 0.5f));
			}
		};
		GameObject flipVertButton = MakeButton (localCanvas, "", flipVertSprite[0], new Rect(Screen.width * 0.12f, Screen.height * 0.5f, Screen.height * 0.08f, Screen.height * 0.08f), funcList);
		flipVertButton.name = "FlipVertButton";
		
		// Set the button navigation mode.
		Navigation nav = new Navigation();
		nav.mode = Navigation.Mode.None;
		flipVertButton.GetComponent<Button>().navigation = nav;
		flipVertButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		SpriteState spriteState = new SpriteState();
		flipVertButton.GetComponent<Button>().targetGraphic = flipVertButton.GetComponentInChildren<Image>();
		spriteState.highlightedSprite = flipVertSprite[1];
		spriteState.pressedSprite = flipVertSprite[1];
		flipVertButton.GetComponent<Button>().spriteState = spriteState;
		
		photoObjects.Add (flipVertButton);
		editObjects.Add(flipVertButton);
		
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				Texture2D t = photoPanel.GetComponent<Image>().sprite.texture;
				t = FlipTextureHorizontal(t);
				
				if(MCP.loadedTexture != null)
				{
					MCP.loadedTexture = FlipTextureHorizontal (MCP.loadedTexture);
				}
				
				photoPanel.GetComponent<Image>().sprite = Sprite.Create (t, new Rect(0, 0, t.width, t.height), new Vector2(0.5f, 0.5f));
			}
		};
		GameObject flipHoriButton = MakeButton (localCanvas, "", flipHoriSprite[0], new Rect(Screen.width * 0.22f, Screen.height * 0.5f, Screen.height * 0.08f, Screen.height * 0.08f), funcList);
		flipHoriButton.name = "FlipHoriButton";
		
		// Set the button navigation mode.
		flipHoriButton.GetComponent<Button>().navigation = nav;
		flipHoriButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		flipHoriButton.GetComponent<Button>().targetGraphic = flipHoriButton.GetComponentInChildren<Image>();
		spriteState.highlightedSprite = flipHoriSprite[1];
		spriteState.pressedSprite = flipHoriSprite[1];
		flipHoriButton.GetComponent<Button>().spriteState = spriteState;
		
		photoObjects.Add (flipHoriButton);
		editObjects.Add(flipHoriButton);
		
		// Make take picture button.
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				takingPhoto = true;
				
				foreach(GameObject g in photoObjects)
				{
					g.SetActive (false);
				}
				foreach(GameObject g in takePhotoObjects)
				{
					g.SetActive (true);
				}
			}
		};
		GameObject newPictureButton = MakeButton (localCanvas, MCP.Text (1508)/*"New Photo"*/, buttonBackground, new Rect(Screen.width * 0.11f, Screen.height * 0.735f, Screen.width * 0.18f, Screen.height * 0.06f), funcList);
		newPictureButton.name = "NewPictureButton";
		newPictureButton.transform.GetChild (0).gameObject.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
		photoObjects.Add (newPictureButton);
		editObjects.Add(newPictureButton);
		
		startTakePhotoPanelX = Screen.width * 0.11f;
		takePhotoPanel = MakeImage (localCanvas, new Rect(startTakePhotoPanelX, Screen.height * 0.19f, Screen.width * 0.18f, Screen.height * 0.225f), webcamSprite);
		takePhotoPanel.name = "TakePhotoPanel";
		takePhotoObjects.Add (takePhotoPanel);
		
		// No camera found text.
		noCamFoundText = MakeLabel(localCanvas, MCP.Text(3001)/*"No camera found"*/, new Rect(Screen.width * 0.11f, Screen.height * 0.59f, Screen.width * 0.18f, Screen.height * 0.1f));
		noCamFoundText.name = "NoCamFoundText";
		takePhotoObjects.Add(noCamFoundText);
		editObjects.Add(noCamFoundText);
		
		// Make take picture button.
		funcList = new List<MyVoidFunc>
		{
			SaveProfile
		};
		takePictureButton = MakeButton (localCanvas, MCP.Text (207)/*"Cancel"*/, buttonBackground, new Rect(Screen.width * 0.11f, Screen.height * 0.735f, Screen.width * 0.18f, Screen.height * 0.06f), funcList);
		takePictureButton.name = "TakePictureButton";
		takePictureButton.transform.GetChild (0).gameObject.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
		takePhotoObjects.Add (takePictureButton);
		editObjects.Add(takePictureButton);
		
//#if !UNITY_IOS
		// Make switch to photo button.
		funcList = new List<MyVoidFunc>();
		iconPhotoSwitchButton = MakeButton (localCanvas, iconShowing ? MCP.Text (1510)/*"Switch to Photo"*/ : MCP.Text (1509)/*"Switch to Icon"*/, buttonBackground, new Rect(Screen.width * 0.11f, Screen.height * 0.835f, Screen.width * 0.18f, Screen.height * 0.06f), funcList);
		iconPhotoSwitchButton.name = "IconPhotoSwitchButton";
		iconPhotoSwitchButton.transform.GetChild (0).gameObject.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		iconPhotoSwitchButton.transform.GetChild (0).gameObject.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
		editObjects.Add(iconPhotoSwitchButton);
		photoObjects.Add(iconPhotoSwitchButton);
		iconObjects.Add(iconPhotoSwitchButton);
		
		iconPhotoSwitchButton.GetComponent<Button>().onClick.AddListener (
			delegate
			{
			if(iconShowing)
			{
				iconPhotoSwitchButton.transform.GetChild (0).gameObject.GetComponent<Text>().text = "Switch to Icon";
				
				// Flag the user using photo.
				localUserInfo.memberProfile.usingPhoto = "1";
				
				foreach(GameObject g in iconObjects)
				{
					g.SetActive (false);
				}
				foreach(GameObject g in photoObjects)
				{
					g.SetActive (true);
				}
			}
			else
			{
				iconPhotoSwitchButton.transform.GetChild (0).gameObject.GetComponent<Text>().text = "Switch to Photo";
				
				// Flag the user using icons.
				localUserInfo.memberProfile.usingPhoto = "0";
				
				foreach(GameObject g in photoObjects)
				{
					g.SetActive (false);
				}
				foreach(GameObject g in iconObjects)
				{
					g.SetActive (true);
				}
			}
			
			iconShowing = !iconShowing;
		});
//#endif
		
		// Make the profile info background.
		GameObject profileInfoBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.325f, Screen.height * 0.175f, Screen.width * 0.55f, Screen.height * 0.75f), window_back, true);
		profileInfoBackground.name = "ProfileInfoBackground";
		
		float offsetY = Screen.height * 0.2125f;
		
		// Make the name text.
		playername = "No MCP";
		username = "No MCP";
		
		if(localUserInfo != null)
		{
			playername = localUserInfo.firstName + " " + localUserInfo.lastName;
			username = localUserInfo.username;
		}
		
		// Make the username text.
		usernameText = MakeLabel (localCanvas, MCP.Text (1504)/*"Username: "*/ + username, new Rect(Screen.width * 0.35f, offsetY, Screen.width * 0.55f, Screen.height * 0.06f), TextAnchor.MiddleLeft);
		usernameText.name = "UsernameText";
		usernameText.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
		
		offsetY += Screen.height * 0.075f;
		
		// Make the name text.
		nameText = MakeLabel (localCanvas, MCP.Text (1505)/*"Name: "*/ + playername, new Rect(Screen.width * 0.35f, offsetY, Screen.width * 0.55f, Screen.height * 0.06f), TextAnchor.MiddleLeft);
		nameText.name = "NameText";
		nameText.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
		
		offsetY += Screen.height * 0.075f;
		
		GameObject countryLabel = MakeLabel (localCanvas, "Country: ", new Rect(Screen.width * 0.35f, offsetY, Screen.width * 0.325f, Screen.height * 0.06f), TextAnchor.UpperLeft);
		countryLabel.name = "CountryLabel";
		
		countryInputLabel = MakeLabel (localCanvas, localUserInfo.memberProfile.country, new Rect(Screen.width * 0.5f, offsetY, Screen.width * 0.325f, Screen.height * 0.08f), TextAnchor.UpperLeft);
		countryInputLabel.name = "CountryInputLabel";
		countryInputLabel.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		countryInputLabel.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Wrap;
		countryInputLabel.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
		displayObjects.Add (countryInputLabel);
		
		countryInput = MakeInputField (localCanvas, localUserInfo.memberProfile.country, new Rect(Screen.width * 0.5f, offsetY, Screen.width * 0.325f, Screen.height * 0.08f), false);
		countryInput.name = "CountryInput";
		editObjects.Add (countryInput.transform.parent.gameObject);
		
		offsetY += Screen.height * 0.1f;
		
		GameObject aboutMeLabel = MakeLabel (localCanvas, MCP.Text (1506)/* "About Me: "*/, new Rect(Screen.width * 0.35f, offsetY, Screen.width * 0.55f, Screen.height * 0.125f));
		aboutMeLabel.name = "AboutMeLabel";
		// Set about me label attrbiutes.
		aboutMeLabel.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
		
		aboutMeInputLabel = MakeLabel (localCanvas, localUserInfo.memberProfile.aboutMe, new Rect(Screen.width * 0.5f, offsetY, Screen.width * 0.325f, Screen.height * 0.125f));
		aboutMeInputLabel.name = "AboutMeInputLabel";
		aboutMeInputLabel.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
		aboutMeInputLabel.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		displayObjects.Add (aboutMeInputLabel);
		
		aboutMeInput = MakeInputField (localCanvas, localUserInfo.memberProfile.aboutMe, new Rect(Screen.width * 0.5f, offsetY, Screen.width * 0.325f, Screen.height * 0.125f), false);
		aboutMeInput.name = "AboutMeInput";
		aboutMeInput.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
		aboutMeInput.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		aboutMeInput.GetComponent<InputField>().lineType = InputField.LineType.MultiLineNewline;
		aboutMeInput.GetComponent<InputField>().characterLimit = 100;
		editObjects.Add (aboutMeInput.transform.parent.gameObject);
		
		offsetY += Screen.height * 0.15f;
		
		GameObject myGoalsLabel = MakeLabel (localCanvas, MCP.Text (1507)/*"My Goals: "*/, new Rect(Screen.width * 0.35f, offsetY, Screen.width * 0.55f, Screen.height * 0.125f));
		myGoalsLabel.name = "MyGoalsLabel";
		myGoalsLabel.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
		
		myGoalsInputLabel = MakeLabel (localCanvas, localUserInfo.memberProfile.myGoals, new Rect(Screen.width * 0.5f, offsetY, Screen.width * 0.325f, Screen.height * 0.125f));
		myGoalsInputLabel.name = "MyGoalsInputLabel";
		myGoalsInputLabel.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
		myGoalsInputLabel.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		displayObjects.Add (myGoalsInputLabel);
		
		myGoalsInput = MakeInputField (localCanvas, localUserInfo.memberProfile.myGoals, new Rect(Screen.width * 0.5f, offsetY, Screen.width * 0.325f, Screen.height * 0.125f), false);
		myGoalsInput.name = "MyGoalsInput";
		myGoalsInput.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
		myGoalsInput.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		myGoalsInput.GetComponent<InputField>().characterLimit = 100;
		editObjects.Add (myGoalsInput.transform.parent.gameObject);
		
		offsetY += Screen.height * 0.15f;
		
		GameObject editButtonBackground = null;
		
		if( localUserInfo.username == MCP.userInfo.username )
		{
			// Make the edit/save button.
			editButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.6375f, Screen.height * 0.7875f, Screen.width * 0.2f, Screen.height * 0.1f), buttonBackground, true);
			editButtonBackground.name = "EditButtonBackground";
		}
		
		funcList = new List<MyVoidFunc>()
		{
			delegate
			{
				if(internalState == InternalState.display)
				{
					editing = true;
					internalState = InternalState.edit;
					editButton.GetComponent<Text>().text = MCP.Text (215)/*"Save"*/;
					
					for(int i = 0; i < displayObjects.Count; i++)
					{
						displayObjects[i].SetActive (false);
					}
					
					for(int i = 0; i < editObjects.Count; i++)
					{
						editObjects[i].SetActive (true);
					}
					for(int i = 0; i < takePhotoObjects.Count; i++)
					{
						takePhotoObjects[i].SetActive (false);
					}
					
					if(iconShowing)
					{
						for(int i = 0; i < photoObjects.Count; i++)
						{
							photoObjects[i].SetActive (false);
						}
						for(int i = 0; i < iconObjects.Count; i++)
						{
							iconObjects[i].SetActive (true);
						}
					}
					else
					{
						for(int i = 0; i < iconObjects.Count; i++)
						{
							iconObjects[i].SetActive (false);
						}
						for(int i = 0; i < photoObjects.Count; i++)
						{
							photoObjects[i].SetActive (true);
						}
					}
				}
				else
				{
					if( localUserInfo.username == MCP.userInfo.username )
					{
						editing = false;
						internalState = InternalState.display;
						editButton.GetComponent<Text>().text = MCP.Text (216)/*"Edit"*/;
						SaveProfile ();
					}
					
					for(int i = 0; i < editObjects.Count; i++)
					{
						editObjects[i].SetActive (false);
					}
					
					for(int i = 0; i < displayObjects.Count; i++)
					{
						displayObjects[i].SetActive (true);
					}
					
					if(iconShowing)
					{
						photoPanel.SetActive (false);
						avatarForeground.SetActive (true);
						avatarBackground.SetActive (true);
					}
					else
					{
						photoPanel.SetActive (true);
						avatarForeground.SetActive (false);
						avatarBackground.SetActive (false);
					}
				}
			}	
		};
		
		if( localUserInfo.username == MCP.userInfo.username )
		{
			editButton = MakeButton (localCanvas, MCP.Text (216)/*"Edit"*/, new Rect(Screen.width * 0.6375f, Screen.height * 0.7875f, Screen.width * 0.2f, Screen.height * 0.1f), funcList);
			editButton.name = "EditButton";
			editButton.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
			editButton.GetComponent<Button>().image = editButtonBackground.GetComponent<Image>();
		}
		
		// Deactivate the edit objects.
		for(int i = 0; i < editObjects.Count; i++)
		{
			editObjects[i].SetActive (false);
		}
		
		foreach(GameObject g in takePhotoObjects)
		{
			g.SetActive (false);
		}
		
		for(int i = 0; i < displayObjects.Count; i++)
		{
			displayObjects[i].SetActive (true);
		}
		
		if(iconShowing)
		{
			photoPanel.SetActive (false);
		}
		else
		{
			avatarForeground.SetActive (false);
			avatarBackground.SetActive (false);
		}
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update()
	{
		if( MCP.friendMemberProfile != null )
		{
			if( MCP.friendMemberProfile.numberOfGoalMaps != "Not Found" )
			{
				if(!MCP.isTransmitting && MCP.loadedTexture != null && MCP.loadedTexture.width > 8)
				{
					currentPhotoSprite = Sprite.Create(MCP.loadedTexture, new Rect(0, 0, MCP.loadedTexture.width, MCP.loadedTexture.height), new Vector2(0.5f, 0.5f));
					photoPanel.GetComponent<Image>().sprite = currentPhotoSprite;
					// Reset the loaded texture.
					MCP.loadedTexture = null;
				}
				else
				{
					currentPhotoSprite = iconButton;
				}
				
				// Update the name text.
				if(localUserInfo != null)
				{
					playername = localUserInfo.firstName + " " + localUserInfo.lastName;
					nameText.GetComponent<Text>().text = MCP.Text (1505)/*"Name: "*/ + playername;
				}
				
				// Update the username text.
				if(localUserInfo != null)
				{
					username = localUserInfo.username;
					usernameText.GetComponent<Text>().text = MCP.Text (1504)/*"Username: "*/ + username;
				}
			}
		}
		else
		{
			if(MCP.userInfo != null && MCP.userInfo.memberProfile != null && !MCP.isTransmitting)
			{
				//Duplicate of above code in null check
				if(MCP.loadedTexture != null && MCP.loadedTexture.width > 8)
				{
					currentPhotoSprite = Sprite.Create(MCP.loadedTexture, new Rect(0, 0, MCP.loadedTexture.width, MCP.loadedTexture.height), new Vector2(0.5f, 0.5f));
					photoPanel.GetComponent<Image>().sprite = currentPhotoSprite;
					// Reset the loaded texture.
					MCP.loadedTexture = null;
					
					if(!iconShowing)
					{
						avatarBackground.SetActive (false);
						avatarForeground.SetActive (false);
					}
				}
				else if(currentPhotoSprite == null || currentPhotoSprite.texture == null || currentPhotoSprite.texture.width < 16)
				{
					// Flag the user using icons.
					localUserInfo.memberProfile.usingPhoto = "0";
					
					if(editing)
					{
						iconPhotoSwitchButton.transform.GetChild (0).gameObject.GetComponent<Text>().text = "Switch to Photo";
					}
					else
					{
						iconShowing = true;
						
						for(int i = 0; i < displayObjects.Count; i++)
						{
							displayObjects[i].SetActive (true);
						}
						
						if(iconShowing)
						{
							photoPanel.SetActive (false);
							takePhotoPanel.SetActive (false);
							
							avatarBackground.SetActive (true);
							avatarForeground.SetActive (true);
						}
					}
				}
				
				// Update the name text.
				if(localUserInfo != null)
				{
					playername = localUserInfo.firstName + " " + localUserInfo.lastName;
					nameText.GetComponent<Text>().text = MCP.Text (1505)/*"Name: "*/ + playername;
					
					username = localUserInfo.username;
					usernameText.GetComponent<Text>().text = MCP.Text (1504)/*"Username: "*/ + username;
				}
			}
		}
		
		if(takingPhoto && editing)
		{
			if(!camSet)
			{
				// Get the connected devices.
				devices = WebCamTexture.devices;
				
				// If the user has not authorized the webcam, ask them to.
				if(!Application.HasUserAuthorization (UserAuthorization.WebCam))
				{
					if(!requested)
					{
						Application.RequestUserAuthorization(UserAuthorization.WebCam);
						requested = true;
					}
				}
				// Otherwise, try and get webcams.
				else
				{
					for( int i = 0 ; i < devices.Length ; i++ )
					{
						if (devices[i].isFrontFacing)
						{
							frontCamName = devices[i].name;
						}
						else
						{
							backCamName = devices[i].name;
						}
					}
				
					webcamTexture = new WebCamTexture(backCamName, photoSize, photoSize, 8);
					// If there is a valid front camera...
					if(frontCamName != null && frontCamName != "")
					{
						// Flag the camera as set.
						camSet = true;
						// Set the webcamTexture to the front cam.
						webcamTexture = new WebCamTexture(frontCamName, photoSize, photoSize, 8);
						// Mirror the take photo object.
						Vector3 newScale = takePhotoPanel.GetComponent<RectTransform>().localScale;
						newScale.x = -1;
						takePhotoPanel.GetComponent<RectTransform>().localScale = newScale;
						Vector3 newPos = takePhotoPanel.GetComponent<RectTransform>().localPosition;
						newPos.x = (startTakePhotoPanelX - (Screen.width * 0.5f)) + takePhotoPanel.GetComponent<RectTransform>().rect.width;
						takePhotoPanel.GetComponent<RectTransform>().localPosition = newPos;
					}
					// Otherwise, if there is a valid back camera...
					else if(backCamName != null && backCamName != "")
					{
						// Flag the camera as set.
						camSet = true;
						// Set the webcamTexture to the back cam.
						webcamTexture = new WebCamTexture(backCamName, photoSize, photoSize, 8);
						// Unmirror the take photo object.
						Vector3 newScale = takePhotoPanel.GetComponent<RectTransform>().localScale;
						newScale.x = 1;
						takePhotoPanel.GetComponent<RectTransform>().localScale = newScale;
					}
					
					// Make sure the camera is currently off.
					if(webcamTexture != null)
					{
						webcamTexture.Stop();
					}
				}
			}
			else
			{
				// If there is a device and the camera is set.
				if(devices.Length > 0)
				{
					noCamFoundText.SetActive (false);
					takePictureButton.transform.GetChild (0).gameObject.GetComponent<Text>().text = MCP.Text (1511)/*"Take Photo"*/;
	
					// If the texture is not playing...
					if(!webcamTexture.isPlaying && !playSet)
					{
						// Start it playing.
						webcamTexture.Play ();
						playSet = true;
					}
					else if(webcamTexture.width > 32)
					{
						// Create a sprite from the webcam texture.
						Texture2D spriteTex = new Texture2D(webcamTexture.width, webcamTexture.height);
						
						if(webcamTexture != null && webcamTexture.isPlaying)
						{
							spriteTex.SetPixels (webcamTexture.GetPixels ());
							spriteTex.Apply ();
						}
						
						webcamSprite = Sprite.Create (spriteTex, new Rect(0, 0, spriteTex.width, spriteTex.height), new Vector2(0.5f, 0.5f));
						
						takePhotoPanel.GetComponent<Image>().sprite = webcamSprite;
						
						if(Debug.isDebugBuild && !sizePopupPopped)
						{
							MakePopup (localCanvas, defaultPopUpWindow, "Camera Size", "Width: " + webcamTexture.width + "\nHeight: " + webcamTexture.height, "Dismiss", CommonTasks.DestroyParent);
							sizePopupPopped = true;
						}
					}
				}
			}
		}
		
		if(noCamFoundText != null)
		{
			if(takingPhoto && !camSet)
			{
				noCamFoundText.SetActive (true);
			}
			else
			{
				noCamFoundText.SetActive (false);
			}
		}
		
		if(exiting)
		{
			if(webcamTexture != null)
			{
				if(webcamTexture.isPlaying)
				{
					webcamTexture.Stop();
				}
				webcamTexture = null;
			}
			
			devices = null;
			
			MCP.friendMemberProfile = null;
			MCP.friendUsername = null;
			
			base.Back ();
		}
	}
	
	/// <summary>
	/// Updates the avatar.
	/// </summary>
	/// <returns>The avatar.</returns>
	/// <param name="index">Index.</param>
	/// <param name="avatarList">Avatar list.</param>
	/// <param name="color">Color.</param>
	public SpriteTex UpdateAvatar( int index, List<MCP.MyAvatar> avatarList, int color )
	{
		SpriteTex newAvatarTex = new SpriteTex ();
		// Get the avatar texture from the texture sheet.
		newAvatarTex = GetTextureFromSheet (avatarList[index].indexX, avatarList[index].indexY, color, avatarList[index].sheetIndex);
		
		return newAvatarTex;
	}
	
	/// <summary>
	/// Gets the texture from sheet.
	/// </summary>
	/// <returns>The texture from sheet.</returns>
	/// <param name="x">The x coordinate.</param>
	/// <param name="y">The y coordinate.</param>
	/// <param name="colorIndex">Color index.</param>
	/// <param name="textureSheet">Texture sheet.</param>
	private SpriteTex GetTextureFromSheet (int x, int y, int colorIndex, int textureSheet)
	{
		SpriteTex newAvatarTex = new SpriteTex ();
		// Pick the sprite from the sprite sheet.
		avatarPrefab.GetComponentInChildren<StaticSpriteSheet> ().PickSprite (x, y, colorIndex, textureSheet);
		// Update the avatar object.
		newAvatarTex.avatarBackground = avatarPrefab.GetComponentInChildren<StaticSpriteSheet> ().selectedBackground;
		newAvatarTex.avatarImage = avatarPrefab.GetComponentInChildren<StaticSpriteSheet> ().selectedAvatar;
		
		return newAvatarTex;
	}
	
	/// <summary>
	/// Saves the profile.
	/// </summary>
	private void SaveProfile()
	{
		takingPhoto = false;
		
		if(localUserInfo != null)
		{
			// Set the profile text.
			MCP.userInfo.memberProfile.country = countryInput.GetComponent<InputField>().text;
			countryInputLabel.GetComponent<Text>().text = MCP.userInfo.memberProfile.country;
			MCP.userInfo.memberProfile.aboutMe = aboutMeInput.GetComponent<InputField>().text;
			aboutMeInputLabel.GetComponent<Text>().text = MCP.userInfo.memberProfile.aboutMe;
			MCP.userInfo.memberProfile.myGoals = myGoalsInput.GetComponent<InputField>().text;
			myGoalsInputLabel.GetComponent<Text>().text = MCP.userInfo.memberProfile.myGoals;
			
			// If the avatar info is null, initialize it.
			if(MCP.userInfo.myAvatar == null)
			{
				MCP.userInfo.myAvatar = new MCP.MyAvatar();
			}
			if(MCP.userInfo.memberProfile == null)
			{
				MCP.userInfo.memberProfile = new MCP.MemberProfile();
			}
			if(MCP.userInfo.memberProfile.profileImage == null)
			{
				MCP.userInfo.memberProfile.profileImage = new MCP.MyAvatar();
			}
			
			// Set the avatar info in the member profile.
			MCP.userInfo.memberProfile.profileImage.indexColor = colorIndex;
			MCP.userInfo.memberProfile.profileImage.indexX = avatarList[avatarIndex].indexX;
			MCP.userInfo.memberProfile.profileImage.indexY = avatarList[avatarIndex].indexY;
			MCP.userInfo.myAvatar.indexX = avatarList[avatarIndex].indexX;
			MCP.userInfo.myAvatar.indexY = avatarList[avatarIndex].indexY;
			MCP.userInfo.memberProfile.profileImage.name = avatarList[avatarIndex].name;
			MCP.userInfo.memberProfile.profileImage.sheetIndex = avatarList[avatarIndex].sheetIndex;
			
			if(webcamTexture != null && webcamTexture.isPlaying)
			{
				// Reset the taking photo flag.
				takingPhoto = false;
				
				if(iconShowing)
				{
					if(MCP.userInfo.memberProfile != null)
					{
						MCP.userInfo.memberProfile.usingPhoto = "0";
					}
				}
				else
				{
					// Take and save the photo.
					TakePhoto();
					
					// Flag the user's use of photo.
					if(MCP.userInfo.memberProfile != null)
					{
						MCP.userInfo.memberProfile.usingPhoto = "1";
					}
				}				
			}
			else
			{
				// Flag that the user is not using a photo.
				if(MCP.userInfo.memberProfile != null)
				{
					if(iconShowing)
					{
						MCP.userInfo.memberProfile.usingPhoto = "0";
					}
					else
					{
						MCP.userInfo.memberProfile.usingPhoto = "1";
						
						StartCoroutine (MCP.PostProfilePhoto(photoPanel.GetComponent<Image>().sprite.texture.EncodeToPNG()));
					}
				}
				
				for(int i = 0; i < editObjects.Count; i++)
				{
					editObjects[i].SetActive (true);
				}
				
				for(int i = 0; i < takePhotoObjects.Count; i++)
				{
					takePhotoObjects[i].SetActive (false);
				}
				
				for(int i = 0; i < iconObjects.Count; i++)
				{
					iconObjects[i].SetActive (false);
				}
				
				for(int i = 0; i < photoObjects.Count; i++)
				{
					photoObjects[i].SetActive (true);
				}
			}
			
			// Update the member profile.
			if(MCP.userInfo != null && MCP.userInfo.memberProfile != null)
			{
				StartCoroutine (MCP.UpdateMemberProfile ());
			}
		}
		else
		{
			for(int i = 0; i < editObjects.Count; i++)
			{
				editObjects[i].SetActive (true);
			}
			
			for(int i = 0; i < takePhotoObjects.Count; i++)
			{
				takePhotoObjects[i].SetActive (false);
			}
			
			for(int i = 0; i < iconObjects.Count; i++)
			{
				iconObjects[i].SetActive (false);
			}
			
			for(int i = 0; i < photoObjects.Count; i++)
			{
				photoObjects[i].SetActive (true);
			}
		}
	}
	
	Texture2D FlipTextureHorizontal( Texture2D original )
	{
		Texture2D flipped = new Texture2D( original.width, original.height );
		
		int xN = original.width;
		int yN = original.height;
		
		for ( int i = 0; i < xN; i++ )
		{
			for ( int j = 0; j < yN; j++ )
			{
				flipped.SetPixel(i, yN - j, original.GetPixel(xN - i, yN - j));
			}
		}
		
		flipped.Apply();
		
		return flipped;
	}
	
	Texture2D FlipTexture(Texture2D original)
	{
		Texture2D flipped = new Texture2D(original.width, original.height);
		
		int xN = original.width;
		int yN = original.height;
		
		
		for(int i = 0; i < xN; i++)
		{
			for(int j = 0; j < yN; j++)
			{
				flipped.SetPixel(xN - i, j, original.GetPixel(xN - i, yN - j));
			}
		}
		
		flipped.Apply();
		
		return flipped;
	}
	
	/// <summary>
	/// Saves the picture.
	/// </summary>
	public void TakePhoto()
	{
		// If there is a valid webcam texture...
		if(webcamTexture != null && webcamTexture.isPlaying)
		{
			// Store the texture.
			Texture2D tex = new Texture2D(webcamTexture.width, webcamTexture.height);
			
			// If mirrored, flip pixel x positions
			if(takePhotoPanel.GetComponent<RectTransform>().localScale.x < 0)
			{
				for(int x = 0; x < webcamTexture.width; x++)
				{
					for(int y = 0; y < webcamTexture.height; y++)
					{
						tex.SetPixel (x, y, webcamTexture.GetPixel (tex.width - x - 1, y));
					}
				}
			}
			else
			{
//				tex.SetPixels (webcamTexture.GetPixels());
				for(int x = 0; x < webcamTexture.width; x++)
				{
					for(int y = 0; y < webcamTexture.height; y++)
					{
						tex.SetPixel (x, y, webcamTexture.GetPixel (x, y));
					}
				}
			}

			tex.Apply();
			StartCoroutine (MCP.PostProfilePhoto(tex.EncodeToPNG()));
			MCP.loadedTexture = tex;
			
			currentPhotoSprite = Sprite.Create(MCP.loadedTexture, new Rect(0, 0, webcamTexture.width, webcamTexture.height), new Vector2(0.5f, 0.5f));
			photoPanel.GetComponent<Image>().sprite = currentPhotoSprite;
		}
		
		for(int i = 0; i < editObjects.Count; i++)
		{
			editObjects[i].SetActive (true);
		}
		
		for(int i = 0; i < takePhotoObjects.Count; i++)
		{
			takePhotoObjects[i].SetActive (false);
		}
		
		for(int i = 0; i < iconObjects.Count; i++)
		{
			iconObjects[i].SetActive (false);
		}
		
		for(int i = 0; i < photoObjects.Count; i++)
		{
			photoObjects[i].SetActive (true);
		}
		
		iconShowing = false;
		camSet = false;
		takingPhoto = false;
		playSet = false;
		
		if(webcamTexture != null)
		{
			webcamTexture.Stop();
			webcamTexture = null;
		}
	}
	
	public override void Back ()
	{
		exiting = true;
	}
	
	public override void DoGUI (){}
}
