using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ShareScreen : MenuScreen
{
	private List<GameObject> mainWindow;
	private List<GameObject> friendsWindow;
	private List<GameObject> messageWindow;
	private GameObject localCanvas;
	private GameObject contactContactsBackground;
	private GameObject friendsScrollArea;
	private GameObject sendGoalMapButtonObject;
	private GameObject messageToObj;
	private GameObject messageSubjectObj;
	private GameObject messageTextObj;
	private GameObject sendButton;
	private GameObject popup;
	private GameObject friendLabel;
	private GameObject indexLabel;
	
	private Sprite[] selectFriendSprite;
	private Sprite[] selectGoalMapSprite;
	
	private bool toggleFriendWindow = false;
	private bool friendsUpdated = false;
	private bool messageSent = false;
	private float usernameOffsetX = 0.1f;
	private float usernameWidth = 0.25f;
	private float realnameOffsetX = 0.35f;
	private float realnameWidth = 0.25f;
	private float contactOffsetX = 0.675f;
	private float contactWidth = 0.2f;
	public int prevGMIndex;
	public int gmIndex;
	private Rect topPanelRect;
	private Rect screenSize;
	private string selectedFriend;
	private string prevSelectedFriend;
	private string messageSubject = "";
	public string gmName;

	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		// Initialise the base.
		Init ();
		mainWindow = new List<GameObject>();
		friendsWindow = new List<GameObject>();
		messageWindow = new List<GameObject>();
		selectFriendSprite = new Sprite[2];
		selectGoalMapSprite = new Sprite[2];
		
		selectFriendSprite[0] = Resources.Load<Sprite> ("GUI/select_friend");
		selectFriendSprite[1] = Resources.Load<Sprite> ("GUI/select_friend_active");
		selectGoalMapSprite[0] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_openmap");
		selectGoalMapSprite[1] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_openmap_active");

		selectedFriend = MCP.Text (3301)/*"No Friend Selected"*/;
		prevSelectedFriend = selectedFriend;

		gmIndex = -1;
		prevGMIndex = gmIndex;
		gmName = MCP.Text (3302)/*"No Goal Map selected"*/;
		
		gmIndex = MCP.gmIndex;

		if(MCP.userInfo != null)
		{
			// Get the user's friends.
			StartCoroutine (MCP.GetFriends (MCP.userInfo.username));
			
			// Get the user's goal maps.
			StartCoroutine (MCP.GetGoalMap(MCP.userInfo.username));
		}

		// Create the scene GUI.
		//CreateGUI ();
	}

	void Update()
	{
		if( Input.GetKeyDown( KeyCode.A ) )
		{
			toggleFriendWindow = !toggleFriendWindow;
			ToggleGameObjectEnabled( friendsWindow, toggleFriendWindow );
		}

		if( !MCP.isTransmitting && !friendsUpdated )
		{
			CreateGUI ();
			
			localCanvas.transform.localScale = Vector2.zero;
			
			toggleFriendWindow = false;
			ToggleGameObjectEnabled( friendsWindow, toggleFriendWindow );
			ToggleGameObjectEnabled( mainWindow, !toggleFriendWindow );
			friendsUpdated = true;
		}

		if( selectedFriend != prevSelectedFriend )
		{
			friendLabel.GetComponent<Text>().text = selectedFriend;
			friendLabel.name = "FriendLabel";
			toggleFriendWindow = !toggleFriendWindow;
			ToggleGameObjectEnabled( friendsWindow, toggleFriendWindow );
			ToggleGameObjectEnabled( mainWindow, !toggleFriendWindow );
			prevSelectedFriend = selectedFriend;
		}

		if( prevGMIndex != gmIndex )
		{
			// Sets the name of the goal map to the selected goal map.
			if(gmIndex == -1)
			{
				gmName = "No Goal Map selected";
			}
			else
			{
				if(MCP.goalMaps != null && MCP.goalMaps.Count - 1 >= gmIndex && MCP.goalMaps[gmIndex] != null)
				{
					gmName = MCP.goalMaps[gmIndex].name;
				}
			}
			
			if(indexLabel != null)
			{
				indexLabel.GetComponent<Text>().text = gmName;
				indexLabel.name = "IndexLabel";
				if(MCP.goalMaps[gmIndex] != null)
				{
					prevGMIndex = gmIndex;
				}
			}
		}
		
		// Make the send button interactable if there is a user and a goal map selected.
		if(sendGoalMapButtonObject != null && sendGoalMapButtonObject.GetComponent<Button>() != null)
		{
			if(gmIndex != -1 && selectedFriend != MCP.Text (3301))
			{
				sendGoalMapButtonObject.GetComponent<Button>().interactable = true;
			}
			else
			{
				sendGoalMapButtonObject.GetComponent<Button>().interactable = false;
			}
		}
		
		// Return to main window after sending a message and dismissing the popup.
		if(messageSent && popup == null)
		{
			contactContactsBackground.SetActive( false );
			ToggleGameObjectEnabled (mainWindow, true);
			
			// Reset variables
			messageSent = false;
			messageToObj.GetComponent<InputField>().text = "";
			messageSubjectObj.GetComponent<InputField>().text = "";
			messageTextObj.GetComponent<InputField>().text = "";
		}
	}

	/// <summary>
	/// Do the legacy GUI.
	/// </summary>
	public override void DoGUI ()
	{
		if ((screenSize.width == 0 || Screen.width != screenSize.width) && MCP.userInfo != null && MCP.userInfo.friend_list != null && MCP.userInfo.friend_list.friends != null)
		{
			screenSize = new Rect (0, 0, Screen.width, Screen.height);

			
			// If the GUI has not aready been created, create the scene GUI.
			if(localCanvas == null)
			{
				//CreateGUI ();
			}
		}
	}
	
	void CreateGUI()
	{
		// Find the current canvas.
		localCanvas = MakeCanvas ();
		
		// Make the title bar.
		MakeTitleBar (localCanvas, MCP.Text (1701)/*"Share Your Map"*/);
		
		// Make the side gizmo
		MakeSideGizmo (localCanvas, false);
		
		GameObject windowBackground = MakeWindowBackground (localCanvas, new Rect(Screen.width * 0.02f, Screen.height * 0.15f, Screen.width * 0.96f, Screen.height * 0.825f), window_back);
		windowBackground.name = "WindowBackground";
		mainWindow.Add (windowBackground);

		UnityEngine.UI.Navigation nav = new UnityEngine.UI.Navigation();

		// *******  Main Menu *******

		// Make the open background file button.
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			delegate
			{
				toggleFriendWindow = !toggleFriendWindow;
				ToggleGameObjectEnabled( friendsWindow, toggleFriendWindow );
				ToggleGameObjectEnabled( mainWindow, !toggleFriendWindow );
				Debug.Log( "Button Pressed" );
			}
		};

		GameObject loadBackgroundButton = MakeButton (localCanvas, "", selectFriendSprite[0], new Rect(Screen.width * 0.1f, Screen.height * 0.3f, Screen.width * 0.1f, Screen.width * 0.1f), funcList);
		loadBackgroundButton.name = "LoadBackgroundButton";
		// Make hover sprite
		loadBackgroundButton.GetComponent<Button>().navigation = nav;
		loadBackgroundButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		SpriteState spriteState = new SpriteState();
		loadBackgroundButton.GetComponent<Button>().targetGraphic = loadBackgroundButton.GetComponentInChildren<Image>();
		spriteState.highlightedSprite = selectFriendSprite[1];
		spriteState.pressedSprite = selectFriendSprite[1];
		loadBackgroundButton.GetComponent<Button>().spriteState = spriteState;
		
		mainWindow.Add (loadBackgroundButton);
		
		GameObject loadFriendTextButton = MakeButton (localCanvas, "", null, new Rect(Screen.width * 0.2f, Screen.height * 0.3f, Screen.width * 0.55f, Screen.width * 0.1f), funcList);
		loadFriendTextButton.name = "LoadFriendTextButton";
		loadFriendTextButton.GetComponent<Button>().navigation = nav;
		loadFriendTextButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		loadFriendTextButton.GetComponent<Button>().targetGraphic = loadBackgroundButton.GetComponentInChildren<Image>();
		spriteState.highlightedSprite = selectFriendSprite[1];
		spriteState.pressedSprite = selectFriendSprite[1];
		loadFriendTextButton.GetComponent<Button>().spriteState = spriteState;
		
		mainWindow.Add (loadFriendTextButton);

		// Make the open background file button.
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				Camera.main.gameObject.AddComponent<SimulateFileBrowser>().sceneCanvas = localCanvas;
				Debug.Log( "Button Pressed" );
			}
		};
		
		loadBackgroundButton = MakeButton (localCanvas, "", selectGoalMapSprite[0], new Rect(Screen.width * 0.1f, Screen.height * 0.6f, Screen.width * 0.1f, Screen.width * 0.1f), funcList);
		loadBackgroundButton.name = "LoadBackgroundButton";
		// Make hover sprite
		loadBackgroundButton.GetComponent<Button>().navigation = nav;
		loadBackgroundButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		loadBackgroundButton.GetComponent<Button>().targetGraphic = loadBackgroundButton.GetComponentInChildren<Image>();
		spriteState.highlightedSprite = selectGoalMapSprite[1];
		spriteState.pressedSprite = selectGoalMapSprite[1];
		loadBackgroundButton.GetComponent<Button>().spriteState = spriteState;
		
		mainWindow.Add (loadBackgroundButton);
		
		GameObject loadGoalMapTextButton = MakeButton (localCanvas, "", null, new Rect(Screen.width * 0.2f, Screen.height * 0.6f, Screen.width * 0.55f, Screen.width * 0.1f), funcList);
		loadGoalMapTextButton.name = "LoadGoalMapTextButton";
		loadGoalMapTextButton.GetComponent<Button>().navigation = nav;
		loadGoalMapTextButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		loadGoalMapTextButton.GetComponent<Button>().targetGraphic = loadBackgroundButton.GetComponentInChildren<Image>();
		spriteState.highlightedSprite = selectGoalMapSprite[1];
		spriteState.pressedSprite = selectGoalMapSprite[1];
		loadGoalMapTextButton.GetComponent<Button>().spriteState = spriteState;
		
		mainWindow.Add (loadGoalMapTextButton);
		
		friendLabel = MakeLabel (localCanvas, selectedFriend, new Rect(Screen.width * 0.25f, Screen.height * 0.3f, Screen.width * 0.5f, Screen.width * 0.1f));
		friendLabel.name = "FriendLabel";
		friendLabel.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
		friendLabel.GetComponent<Text>().fontSize = (int)_fontSize_Large;
		friendLabel.transform.SetParent (loadFriendTextButton.transform);
		mainWindow.Add (friendLabel);

		indexLabel = MakeLabel (localCanvas, gmName, new Rect(Screen.width * 0.25f, Screen.height * 0.6f, Screen.width * 0.5f, Screen.width * 0.1f), null);
		indexLabel.name = "IndexLabel";
		indexLabel.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
		indexLabel.GetComponent<Text>().fontSize = (int)_fontSize_Large;
		indexLabel.transform.SetParent (loadGoalMapTextButton.transform);
		mainWindow.Add (indexLabel);
		
		// Draw back button.		
//		GameObject closeButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.35f, Screen.height * 0.7f, Screen.width * 0.3f, Screen.height * 0.08f), buttonBackground, true);
//		closeButtonBackground.name = "CloseButtonBackground";
		//		hideForScreenshot.Add (closeButtonBackground);
		
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				LoadMenu ("ViewGoalMap_Screen");
			}
		};
		GameObject emailGoalMapButtonObject = MakeButton (localCanvas, MCP.Text (3304)/* "Email Goal Map"*/, buttonBackground, new Rect(Screen.width * 0.04f, Screen.height * 0.85f, Screen.width * 0.3f, Screen.height * 0.08f), funcList, "SmallText");
		emailGoalMapButtonObject.name = "EmailGoalMapButtonObject";
		//		closeButtonObject.GetComponent<Button>().image = closeButtonBackground.GetComponent<Image>();
		emailGoalMapButtonObject.transform.GetChild (0).gameObject.GetComponent<Text> ().alignment = TextAnchor.MiddleCenter;
		emailGoalMapButtonObject.transform.GetChild (0).gameObject.GetComponent<Text> ().color = bmOrange;
		//mainWindow.Add (emailGoalMapButtonObject);
		emailGoalMapButtonObject.SetActive (false);
		
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				messageToObj.GetComponent<InputField>().text = selectedFriend;		 
				messageSubjectObj.GetComponent<InputField>().text = "" + gmName + "";
				ToggleGameObjectEnabled( messageWindow, true );
				ToggleGameObjectEnabled( mainWindow, false );
				Debug.Log( "Button Pressed" );
			}
		};
		sendGoalMapButtonObject = MakeButton (localCanvas, MCP.Text (3303)/* "Send Goal Map"*/, buttonBackground, new Rect(Screen.width * 0.65f, Screen.height * 0.85f, Screen.width * 0.3f, Screen.height * 0.08f), funcList, "SmallText");
		sendGoalMapButtonObject.name = "SendGoalMapButtonObject";
		//		closeButtonObject.GetComponent<Button>().image = closeButtonBackground.GetComponent<Image>();
		sendGoalMapButtonObject.transform.GetChild (0).gameObject.GetComponent<Text> ().alignment = TextAnchor.MiddleCenter;
		sendGoalMapButtonObject.transform.GetChild (0).gameObject.GetComponent<Text> ().color = bmOrange;
		mainWindow.Add (sendGoalMapButtonObject);

		///////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////


		///////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// *******  Display contacts - state view *******
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////
		topPanelRect = new Rect (Screen.width * 0.15f, Screen.height * 0.14f, Screen.width , Screen.height );
		GameObject displayContactsBackground = MakeWindowBackground (localCanvas, new Rect(topPanelRect.width * 0.02f, topPanelRect.height * 0.15f, (Screen.width * 0.98f) - (topPanelRect.width * 0.02f), (Screen.height * 0.98f) - (topPanelRect.height * 0.17f)), window_back);
		displayContactsBackground.name = "Display Contacts";
		
		// Make username label.
		GameObject nameLabel = MakeLabel(localCanvas, MCP.Text (2002)/*"Username"*/, new Rect (topPanelRect.width * (usernameOffsetX - 0.04f), topPanelRect.y + (topPanelRect.height * 0.05f), topPanelRect.width * usernameWidth, topPanelRect.height * 0.05f));
		nameLabel.name = "Name"; //the name
		nameLabel.GetComponent<Text>().color = bmPink;
		nameLabel.GetComponent<Text>().fontStyle = FontStyle.Bold;
		nameLabel.transform.SetParent (displayContactsBackground.transform, true);
		
		// Make real name label. 
		GameObject realLabel = MakeLabel(localCanvas, MCP.Text (2003)/*"Realname"*/, new Rect (topPanelRect.width * (realnameOffsetX), topPanelRect.y + (topPanelRect.height * 0.05f), topPanelRect.width * realnameWidth, topPanelRect.height * 0.05f));
		realLabel.name = "RealName"; //the real name
		realLabel.GetComponent<Text>().color = bmPink;
		realLabel.GetComponent<Text>().fontStyle = FontStyle.Bold;
		realLabel.transform.SetParent (displayContactsBackground.transform, true);
		
		// Make contact button label.
		GameObject shareLabel = MakeLabel(localCanvas, MCP.Text (2012)/*"Share"*/, new Rect (topPanelRect.width * (contactOffsetX + 0.015f), topPanelRect.y + (topPanelRect.height * 0.05f), topPanelRect.width * contactWidth, topPanelRect.height * 0.05f));
		shareLabel.name = "Share"; //the contact button
		shareLabel.GetComponent<Text>().color = bmPink;
		shareLabel.GetComponent<Text>().fontStyle = FontStyle.Bold;
		shareLabel.transform.SetParent (displayContactsBackground.transform, true);
		
		// Make scroll area for the friends list.
		friendsScrollArea = MakeScrollRect (localCanvas, new Rect(topPanelRect.width * 0.02f, topPanelRect.height * 0.225f, (Screen.width * 0.98f) - (topPanelRect.width * 0.075f), (Screen.height * 0.98f) - (topPanelRect.height * 0.3f)), null);
		friendsScrollArea.name = "FriendsScrollArea";
		friendsScrollArea.GetComponent<ScrollRect>().horizontal = false;
		friendsScrollArea.transform.SetParent (displayContactsBackground.transform, true);
		friendsScrollArea.transform.GetChild (0).gameObject.GetComponent<Mask>().showMaskGraphic = false;
		// If the user has any friends...
		
		//friendsWindow.Add ( displayContactsBackground );
		
		if(MCP.userInfo.friend_list.friends.Count > 0)
		{
			float offset = topPanelRect.height * 0.175f;
			int i = 0;
			
			foreach (MCP.Friend vid in MCP.userInfo.friend_list.friends)
			{
				// Make an avatar image for this friend.
				//GameObject avatarImage = MakeImage (displayContactsBackground, new Rect (topPanelRect.width * 0.035f, offset - (topPanelRect.height * 0.81f), topPanelRect.width * 0.0325f, topPanelRect.width * 0.0325f), teamAvatarImage);
				//avatarImage.name = "avImage";
				//avatarImage.transform.SetParent (friendsScrollArea.transform.GetChild (0).GetChild (0), true);
				//avatarImage.transform.localScale = Vector3.one;
				
				// Make a label for the username.
				GameObject name = MakeLabel(localCanvas, vid.username , new Rect (topPanelRect.width * usernameOffsetX, offset + (topPanelRect.height * 0.15f), topPanelRect.width * usernameWidth, topPanelRect.width * 0.0325f));
				name.name = "Name"; //the name
				name.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
				name.transform.SetParent (friendsScrollArea.transform.GetChild (0).GetChild (0), true);
				name.transform.localScale = Vector3.one;
				name.GetComponent<Text>().resizeTextForBestFit = false;
				name.GetComponent<Text>().fontSize = (int)_fontSize;
				
				// Make a label for the real name.
				GameObject real = MakeLabel(displayContactsBackground,vid.firstName + " " + vid.lastName, new Rect (topPanelRect.width * realnameOffsetX, offset - (Screen.height * 0.8f), topPanelRect.width * realnameWidth, topPanelRect.width * 0.0325f));
				real.name = "Name"; //the name
				real.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
				real.transform.SetParent (friendsScrollArea.transform.GetChild (0).GetChild (0), true);
				real.transform.localScale = Vector3.one;
				real.GetComponent<Text>().resizeTextForBestFit = false;
				real.GetComponent<Text>().fontSize = (int)_fontSize;
			
				//List<MyVoidFunc> newFuncList = new List<MyVoidFunc>
				//{
				//	delegate
				//	{
				//		selectedFriend = vid.username;
				//		toggleFriendWindow = !toggleFriendWindow;
				//		ToggleGameObjectEnabled( friendsWindow, toggleFriendWindow );
				//		Debug.Log( "User Selected: " + selectedFriend );
				//	}
				//};
				
				// Make a button to contact this friend.
				GameObject closeButtonBackground = MakeImage (displayContactsBackground, new Rect (topPanelRect.width * contactOffsetX, offset - (Screen.height * 0.8f), topPanelRect.width * contactWidth * 0.95f, topPanelRect.width * 0.0325f), buttonBackground, true);
				closeButtonBackground.name = "CloseButtonBackground";
				closeButtonBackground.transform.SetParent (friendsScrollArea.transform.GetChild (0).GetChild (0), true);
				closeButtonBackground.transform.localScale = Vector3.one;
				//friendsWindow.Add( closeButtonBackground );
			
				GameObject contactButton = MakeActionButton (displayContactsBackground, MCP.Text (2012)/*"Share"*/,  new Rect (topPanelRect.width * contactOffsetX, offset, topPanelRect.width * contactWidth * 0.95f, topPanelRect.width * 0.0325f), DoContactButtonID, i);
				contactButton.name = "ContactButton";
				contactButton.transform.GetChild (0).gameObject.GetComponent<Text>().fontStyle = FontStyle.Bold;
				contactButton.transform.SetParent (friendsScrollArea.transform.GetChild (0).GetChild (0), true);
				contactButton.transform.localScale = Vector3.one;
				//friendsWindow.Add( contactButton );
				
				offset += topPanelRect.width * 0.05f;			
				
				i++;
			}
			
			if(offset > topPanelRect.height)
			{
				// Make the friends scroll area big enough for the content.
				friendsScrollArea.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(friendsScrollArea.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform>().sizeDelta.x, offset + (Screen.height * 0.0f));
				// Reset the scroll content position to start at the top.
				Vector3 newPos = friendsScrollArea.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform>().localPosition;
				newPos.y = -offset - (Screen.height * 1.5f);
				friendsScrollArea.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform>().localPosition = newPos;
			}
		}
		else
		{
			//TODO: Make a newgui object
			GUI.Label (topPanelRect, MCP.Text (2006)/*"No friends."*/);
		}
		
		friendsWindow.Add ( displayContactsBackground );

		///////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// ******* Begin Contact friend *******
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////
	
		contactContactsBackground = MakeWindowBackground (localCanvas, new Rect(topPanelRect.width * 0.15f, topPanelRect.height * 0.15f, (MenuScreen.canvasSize.width * 0.98f) - (topPanelRect.width * 0.3f), (MenuScreen.canvasSize.height * 0.98f) - (topPanelRect.height * 0.25f)), window_back);
		contactContactsBackground.name = "Contact Contact";
		
		Rect areaRect = contactContactsBackground.GetComponent<RectTransform>().rect;
		// Make the button to go back to the friends list.
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				messageToObj.GetComponent<InputField>().text = "";
				messageSubjectObj.GetComponent<InputField>().text = "";
				messageTextObj.GetComponent<InputField>().text = "";
				
				contactContactsBackground.SetActive( false );
				ToggleGameObjectEnabled (mainWindow, true);
			}
		};
		GameObject backButton = MakeButton (contactContactsBackground, MCP.Text (219)/*"Back"*/, buttonBackground,  new Rect (areaRect.width * 0.05f, topPanelRect.height * -0.08f, topPanelRect.width * 0.15f, topPanelRect.width * 0.0325f), funcList );
		backButton.name = "BackButton";
		
		// Make the label for Send to
		GameObject sendTo = MakeLabel(contactContactsBackground, MCP.Text (2006)/*"Send to:"*/ , new Rect (areaRect.width * 0.01f, (topPanelRect.height * 0.0675f) - (Screen.height * 0.725f), areaRect.width * 0.2f, topPanelRect.height * 0.06f));
		sendTo.name = "Send To"; //the name
		sendTo.GetComponent<Text>().resizeTextForBestFit = false;
		sendTo.GetComponent<Text>().fontSize = (int)_fontSize;
		
		// Make an input field for the recipient.
		messageToObj = MakeInputField (contactContactsBackground, messageSubject, new Rect (areaRect.width * 0.275f, (topPanelRect.height * 0.0f) - (Screen.height * 0.675f), areaRect.width * 0.66f, Screen.height * 0.08f), false);
		messageToObj.name = "messageTo";
//		messageToObj.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
		
		// Make the subject label
		GameObject subjectObj = MakeLabel(contactContactsBackground, MCP.Text (211)/*"Subject"*/ + ":" , new Rect (areaRect.width * 0.01f, (topPanelRect.height * 0.175f) - (Screen.height * 0.7125f), areaRect.width * 0.2f, topPanelRect.width * 0.0325f));
		subjectObj.name = "Subject"; //the name
		subjectObj.GetComponent<Text>().resizeTextForBestFit = false;
		subjectObj.GetComponent<Text>().fontSize = (int)_fontSize;
		
		// Make the input field for the subject.
		messageSubjectObj = MakeInputField (contactContactsBackground, messageSubject, new Rect (areaRect.width * 0.275f, (topPanelRect.height * 0.09f) - (Screen.height * 0.65f), areaRect.width * 0.66f, Screen.height * 0.08f), false);
		messageSubjectObj.name = "messageSubject";
//		messageSubjectObj.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
		
		// Make the label for the message.
		GameObject messageObj = MakeLabel(contactContactsBackground, MCP.Text (2007)/*"Message:"*/ , new Rect (areaRect.width * 0.01f, (topPanelRect.height * 0.3f) - (Screen.height * 0.7f), areaRect.width * 0.2f, topPanelRect.width * 0.0325f));
		messageObj.name = "Message"; //the name
		messageObj.GetComponent<Text>().resizeTextForBestFit = false;
		messageObj.GetComponent<Text>().fontSize = (int)_fontSize;
		
		// Make the input field for the message.
		messageTextObj = MakeInputField (contactContactsBackground, messageSubject, new Rect (areaRect.width * 0.275f, (topPanelRect.height * 0.19f) - (Screen.height * 0.6f), areaRect.width * 0.66f, topPanelRect.height * 0.3f), false);
		messageTextObj.name = "Message Text";
		
		// Make the send button.
		GameObject sendButtonBackground = MakeImage (contactContactsBackground, new Rect(areaRect.width * 0.7175f, (areaRect.height * 0.72f) - (Screen.height * 0.6f), topPanelRect.width * 0.15f, topPanelRect.width * 0.0325f), buttonBackground, true);
		
		funcList = new List<MyVoidFunc>
		{
			SendPrivateMessage
		};
		GameObject sendButton = MakeButton (contactContactsBackground, MCP.Text (213)/*"Send"*/, null, new Rect(areaRect.width * 0.7175f, (areaRect.height * 0.72f) - (Screen.height * 0.6f), topPanelRect.width * 0.15f, topPanelRect.width * 0.0325f), funcList);
		sendButton.name = "SendButton";
		sendButton.transform.GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
		sendButton.transform.GetChild (0).gameObject.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
		sendButton.GetComponent<Button>().image = sendButtonBackground.GetComponent<Image>();
		
		messageWindow.Add ( contactContactsBackground );
		contactContactsBackground.SetActive(false);
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////
	}

	void ToggleGameObjectEnabled( List<GameObject> tempObj, bool enable )
	{
		for( int i = 0; i < tempObj.Count; i++ )
		{
			tempObj[i].SetActive (enable);
		}
	}

	bool DoContactButtonID(int i)
	{		
		// Set the sent to text to the friend's name.
		selectedFriend = MCP.userInfo.friend_list.friends[i].username;
		
		return true;
	}

	/// <summary>
	/// Sends the private message.
	/// </summary>
	public void SendPrivateMessage()
	{
		messageSubjectObj.GetComponent<InputField>().text = messageSubjectObj.GetComponent<InputField>().text + "<" + gmIndex.ToString() + ">";
		
		// Post the message.
		StartCoroutine( MCP.PostPrivateMessage(messageToObj.GetComponent<Text>().text, MCP.userInfo.username, messageSubjectObj.GetComponent<Text>().text, messageTextObj.GetComponent<Text>().text, (20).ToString()) );
		
		// Flag message as sent
		messageSent = true;
		
		// Make the message sent popup.
		popup = MakePopup (localCanvas, defaultPopUpWindow, "", MCP.Text (2008)/*"Message Sent"*/, MCP.Text (205)/*"Dismiss"*/, CommonTasks.DestroyParent);
		popup.name = "Popup";
	}
	
	void FileBrowserClosed()
	{
		gmIndex = MCP.gmIndex;
	}

	public override void Back ()
	{
		if( toggleFriendWindow == true )
		{
			toggleFriendWindow = !toggleFriendWindow;
			ToggleGameObjectEnabled( friendsWindow, toggleFriendWindow );
			ToggleGameObjectEnabled( mainWindow, !toggleFriendWindow );
		}
		else
		{
			base.Back();
		}
	}
}
