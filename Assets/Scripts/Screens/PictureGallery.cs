﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PictureGallery : MenuScreen
{
	public Sprite spinnerSprite;
	
	public bool isClipArt = false;
	
	private List<GameObject> pictureObjects;
	public GameObject sceneCanvas;
	private GameObject localCanvas;
	private GameObject pictureScrollArea;
	private GameObject loadingBackground;
	private GameObject loadingSpinner;
	private GameObject loadingProgressLabel;
	private GameObject noneFoundObject;
	
	private List<Texture2D> clipArtTextures;
	private Sprite deleteSprite;
	
	private bool imagesPulled = false;
	private bool imageObjectsMade = false;
//	private bool clipArtLoaded = false;
	private float spinnerRot = 0;
	private float progressPercent = 0;
	private int index = 0;

	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		// Initialize the base class.
		Init ();
		
		// Load resources.
		spinnerSprite = Resources.Load<Sprite> ("GUI/bm_rotate");
		deleteSprite = Resources.Load<Sprite>("GUI/menuicon_logout");
		
		// Reset the pulled images list.
		MCP.pulledImages = new System.Collections.Generic.List<Texture2D>();
		clipArtTextures = new List<Texture2D>();
		pictureObjects = new List<GameObject>();
		
		// Get the number of clip art images the user has access to.
		if(!isClipArt)
		{
			StartCoroutine (MCP.GetNumberOfImages(MCP.userInfo.username));
		}
		else
		{
//			if(MCP.clipArtBundle == null)
//			{
//				StartCoroutine (MCP.GetAssetBundle ("clipart.clipart", false));
//			}
			
			//TODO: Either get the asset bundle working, or move this to the server. Maybe try zip files?
			clipArtTextures.Add (Resources.Load ("ClipArt/a-blank-frame-with-birds") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/books") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/cherries") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/coffee") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/credit-cards") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/four-leaf-clover") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/globe") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/gold-star") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/graduation-cap") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/green-footprints") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/green-hand-prints") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/green-recycle-symbol") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/laptop-computer") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/light-bulb") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/music-notes") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/open-book") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/orange-slice") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/pencil") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/pink-splash") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/red-and-an-orange-balloon") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/red-hearts") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/red-lips") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/red-santa-hat") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/red-tulips") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/snowflake") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/strawberries") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/sun") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/tools") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/tree-with-leaves") as Texture2D);
			clipArtTextures.Add (Resources.Load ("ClipArt/yin-yang-symbol") as Texture2D);
		}
		
		// Create the gui.
		CreateGUI();
		
		localCanvas.transform.localScale = Vector2.zero;
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Get the current scene's canvas.
		sceneCanvas.SetActive (false);
		
		// Make a canvas for this script.
		localCanvas = MakeCanvas (true);
		localCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
		MenuScreen.firstFrameTimer = -5;
		
		// Make a scroll rect for the images.
		pictureScrollArea = MakeScrollRect (localCanvas, new Rect(Screen.width * 0.1f, Screen.height * 0.1f, Screen.width * 0.8f, Screen.height * 0.6f), null, false, true);
		pictureScrollArea.name = "PictureScrollArea";
		pictureScrollArea.GetComponent<ScrollRect>().vertical = false;
		
		List<MyVoidFunc> funcList;
//#if !UNITY_WEBPLAYER
		if(!isClipArt)
		{
			// Make the switch to local storage button.
//			GameObject switchToDeviceButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.48f, Screen.height * 0.87f, Screen.width * 0.25f, Screen.height * 0.08f), buttonBackground, true);
//			switchToDeviceButtonBackground.name = "SwitchToDeviceButtonBackground";
//			
//			funcList = new List<MyVoidFunc>
//			{
//				delegate
//				{
//					// Reenable the main scene canvas.
//					sceneCanvas.SetActive (true);
//					
//					// Destroy all file browser game objects.
//					localCanvas.SetActive (false);
//					Destroy (localCanvas);
//					
//					// Reset the first frame timer.
//					MenuScreen.firstFrameTimer = 0;
//					// Add the file browser to the camera.
//					GameObject.FindGameObjectWithTag ("MainCamera").AddComponent<FileBrowser> ().sceneCanvas = sceneCanvas;
//					// Set the file browser variables.
//					GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<FileBrowser> ().currentFilePath = Application.persistentDataPath;
//					GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<FileBrowser> ().upPathLimit = Application.persistentDataPath;
//					GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<FileBrowser> ().allowedExtensions = new string[]{"png", "jpg"};
//					
//					// Remove this script from the object to close.
//					Destroy (this);
//				}
//			};
//			GameObject switchToDeviceButton = MakeButton (localCanvas, MCP.Text(1401)/*"Switch To Device"*/, null, new Rect(Screen.width * 0.48f, Screen.height * 0.87f, Screen.width * 0.25f, Screen.height * 0.08f), funcList, "SmallText");
//			switchToDeviceButton.name = "SwitchToDeviceButton";
//			// Set the button attributes.
//			switchToDeviceButton.GetComponent<Button>().image = switchToDeviceButtonBackground.GetComponent<Image>();
//			switchToDeviceButton.transform.GetChild (0).gameObject.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
//			switchToDeviceButton.transform.GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
		}
//#endif
		
		// Make the upload label
		GameObject uploadLabel = MakeLabel (localCanvas, "Upload*", new Rect(Screen.width * 0.1f, Screen.height * 0.87f, Screen.width * 0.2f, Screen.height * 0.08f), TextAnchor.MiddleLeft);
		uploadLabel.name = "UploadLabel";
		uploadLabel.SetActive (false);
		
		// Make the upload file name box
		GameObject uploadInputField = MakeInputField (localCanvas, "Drag Image Here*", new Rect(Screen.width * 0.2f, Screen.height * 0.87f, Screen.width * 0.25f, Screen.height * 0.08f), false);
		uploadInputField.name = "UploadInputField";
		uploadInputField.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		uploadInputField.transform.parent.gameObject.SetActive (false);
		
		// Make the close button.
		GameObject closeButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.75f, Screen.height * 0.87f, Screen.width * 0.15f, Screen.height * 0.08f), buttonBackground, true);
		closeButtonBackground.name = "CloseButtonBackground";
		
		// Draw back button.
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				MCP.loadedTexture = null;
				Close();
			}
		};
		GameObject closeButtonObject = MakeButton (localCanvas, MCP.Text (206)/*"Close"*/, new Rect(Screen.width * 0.75f, Screen.height * 0.87f, Screen.width * 0.15f, Screen.height * 0.08f), funcList, "SmallText");
		closeButtonObject.name = "CloseButtonObject";
		// Set the close button attributes.
		closeButtonObject.GetComponent<Button>().image = closeButtonBackground.GetComponent<Image>();
		closeButtonObject.GetComponent<Text> ().alignment = TextAnchor.MiddleCenter;
		closeButtonObject.GetComponent<Text> ().color = bmOrange;
		
		// Make the none found text.
		noneFoundObject = MakeLabel (localCanvas, MCP.Text (1402)/*"No images found"*/, new Rect(Screen.width * 0.25f, Screen.height * 0.4f, Screen.width * 0.5f, Screen.height * 0.1f), TextAnchor.UpperCenter, "InnerLabel");
		noneFoundObject.name = "NoneFoundObject";
		noneFoundObject.transform.SetParent (pictureScrollArea.transform.GetChild (0).GetChild (0), true);
		noneFoundObject.transform.localScale = Vector3.one;
		noneFoundObject.SetActive (false);
		
		// Make the loading background.
		loadingBackground = MakeImage (localCanvas, new Rect(0, 0, Screen.width, Screen.height), menuBackground);
		loadingBackground.name = "LoadingBackground";
		
		// Make the loading background panel.
		GameObject loadingBackgroundPanel = MakeImage (localCanvas, new Rect(Screen.width * 0.25f, Screen.height * 0.2f, Screen.width * 0.5f, Screen.height * 0.45f), popup_back, true);
		loadingBackgroundPanel.name = "LoadingBackgroundPanel";
		loadingBackgroundPanel.transform.SetParent (loadingBackground.transform);
		
		// Make the loading text.
		GameObject loadingLabel = MakeLabel (localCanvas, MCP.Text (214)/*"Loading..."*/, new Rect(Screen.width * 0.35f, Screen.height * 0.2125f, Screen.width * 0.3f, Screen.height * 0.2f), TextAnchor.MiddleCenter, "InnerLabel");
		loadingLabel.name = "LoadingLabel";
		loadingLabel.transform.SetParent (loadingBackground.transform);
		
		// Make the loading text.
		loadingProgressLabel = MakeLabel (localCanvas, "0%", new Rect(Screen.width * 0.35f, Screen.height * 0.3f, Screen.width * 0.3f, Screen.height * 0.2f), TextAnchor.MiddleCenter, "InnerLabel");
		loadingProgressLabel.name = "loadingProgressLabel";
		loadingProgressLabel.transform.SetParent (loadingBackground.transform);
		
		// Make the loadign spinner.
		loadingSpinner = MakeImage (localCanvas, new Rect(Screen.width * 0.5f, Screen.height * 0.375f, Screen.width * 0.08f, Screen.width * 0.08f), spinnerSprite, false);
		loadingSpinner.name = "LoadingSpinner";
		loadingSpinner.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
		loadingSpinner.transform.SetParent (loadingBackground.transform);
		
		loadingBackground.SetActive (false);
		sceneCanvas.SetActive (false);
	}
	
	void RefreshImages()
	{
		for(int i = pictureObjects.Count - 1; i >= 0; i--)
		{
			Destroy(pictureObjects[i]);
			pictureObjects.RemoveAt (i);
		}
		
		MCP.numberOfImages = 0;
		MCP.pulledImages = new List<Texture2D>();
		imageObjectsMade = false;
		imagesPulled = false;
		imageObjectsMade = false;
		index = 0;
		
		StartCoroutine (MCP.GetNumberOfImages(MCP.userInfo.username));
	}
	
	/// <summary>
	/// Updates the loading popup.
	/// </summary>
	void UpdateLoadingPopup()
	{
		spinnerRot -= Time.deltaTime * 75f;
		loadingSpinner.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, spinnerRot);
		
		if(MCP.pulledImages.Count > 0)
		{
			progressPercent = ((float)index / (float)MCP.numberOfImages) * 100f;
			loadingProgressLabel.GetComponent<Text>().text = ((int)progressPercent).ToString () + "%";
		}
		else
		{
			loadingProgressLabel.GetComponent<Text>().text = "0%";
		}
	}
	
	void DeletePicture(int imageIndex)
	{
		StartCoroutine (MCP.DeletePicture(imageIndex));
		
		RefreshImages ();
	}
	
	bool MakeDeletePopup(int imageIndex)
	{
		// Make a popup with an 'Are you sure' message
		GameObject deletePopup = MakePopup (localCanvas, defaultPopUpWindow, "Delete Picture", "Are you sure you want to delete this picture?\n\n Warning: This will remove this picture from any Goal Maps using it.", MCP.Text (207)/*Cancel*/, CommonTasks.DestroyParent);
		deletePopup.name = "DeletePopup";
		
		// Get the cancel button from the popup and move it to allow room for the delete button
		GameObject cancelButton = deletePopup.transform.GetChild (2).gameObject;
		Vector3 newPos = cancelButton.GetComponent<RectTransform>().localPosition;
		newPos.x = Screen.width * 0.1f;
		cancelButton.GetComponent<RectTransform>().localPosition = newPos;
		
		// Make the delete button
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			delegate
			{
				// Delete the selected picture
				DeletePicture(imageIndex);
				// Destroy the popup object.
				Destroy (deletePopup);
			}
		};
		GameObject deleteButton = MakeButton (localCanvas, "Delete", buttonBackground, new Rect(Screen.width * 0.05f, Screen.height * 0.15f, Screen.width * 0.1f, Screen.height * 0.05f), funcList);
		deleteButton.name = "DeleteButton";
		deleteButton.transform.SetParent (cancelButton.transform.parent);
		deleteButton.transform.localScale = Vector3.one;
		
		return true;
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update ()
	{
		if(isClipArt)
		{
			// Store the pulled clip art asset bundle
//			if(MCP.clipArtBundle == null && MCP.downloadedAssetBundle != null)
//			{
//				MCP.clipArtBundle = MCP.downloadedAssetBundle;
//				MCP.downloadedAssetBundle = null;
//				Debug.Log("ClipArtPulled: " + MCP.clipArtBundle.GetAllAssetNames().Length);
//			}
			
//			if(MCP.clipArtBundle != null && MCP.clipArtBundle.GetAllAssetNames ().Length > 0)
			{
//				if(!clipArtLoaded)
//				{
//					foreach(string s in MCP.clipArtBundle.GetAllAssetNames())
//					{
//						Texture2D currentTex = new Texture2D(4, 4);
//						currentTex.SetPixels ((MCP.clipArtBundle.LoadAsset(s) as Texture2D).GetPixels());
//						
//						clipArtTextures.Add (currentTex);
//					}
//					
//					clipArtLoaded = true;
//				}
//				else
				{
					if(!imageObjectsMade)
					{
						// Get the scroll content object.
						GameObject scrollParent = pictureScrollArea.transform.GetChild (0).GetChild (0).gameObject;
						
						// Make objects for each image.
						for(int i = 0; i < clipArtTextures.Count; i++)
						{
							List<MyVoidFunc> funcList = new List<MyVoidFunc>{};
							
							// Make buttons for each image to allow them to be selected.
							float yPos = Screen.height * -0.6f;
							float height = Screen.width * 0.25f;
							if(clipArtTextures[i].width / clipArtTextures[i].height > 1)
							{
								height = (Screen.width * 0.25f) / (clipArtTextures[i].width / clipArtTextures[i].height);
								yPos = (Screen.height * -0.6f) + ((clipArtTextures[i].width - clipArtTextures[i].height) * 0.25f);
							}
							GameObject imageObject = MakeButton (scrollParent, "", Sprite.Create(clipArtTextures[i], new Rect(0, 0, clipArtTextures[i].width, clipArtTextures[i].height), new Vector2(0.5f, 0.5f)), new Rect((Screen.width * 0.05f) + (Screen.width * 0.3f * i), yPos, Screen.width * 0.25f, height), funcList);
							imageObject.name = "ImageObject_" + i.ToString ();
							
							imageObject.GetComponent<Button>().onClick.AddListener (
								delegate
								{
									// Select image by splitting the name. Passing in 'i' will just set every button to the same number.
									SelectImage (int.Parse (imageObject.name.Split (new char[]{'_'})[1]));
								});
						}
						
						// Get the width of all of the pictures.
						float width = (Screen.width * 0.35f) + (Screen.width * 0.3f * (clipArtTextures.Count - 1));
						// Set the size of the scroll content object to this width.
						scrollParent.GetComponent<RectTransform>().sizeDelta = new Vector2(width, scrollParent.GetComponent<RectTransform>().sizeDelta.y);
						// Flag the images as created.
						imageObjectsMade = true;
					}
				}
			}
		}
		else
		{
			// If any images have been loaded...
			if(MCP.numberOfImages > 0)
			{
				// Go through each image, waiting for the previous one to be pulled before moving on the the next.
				if(!MCP.isTransmitting)
				{
					if(index < MCP.numberOfImages)
					{
						if(MCP.userInfo != null)
						{
							// If the user is looking for clip art...
							if(isClipArt)
							{
								// Get the clip art image.
								StartCoroutine (MCP.GetClipArt (MCP.userInfo.memberProfile.clipArtCategoriesOwnedJson, index.ToString ()));
							}
							else
							{
								// Get the user's stored image.
								StartCoroutine (MCP.GetImage (MCP.userInfo.username, "", index.ToString ()));
							}
						}
						
						// Move to the next image.
						index++;
					}
					else
					{
						// Flag the images as pulled to allow the user to interact.
						imagesPulled = true;
					}
				}
			}
			else
			{
				if(!MCP.isTransmitting)
				{
					// Flag the images as pulled to allow the user to interact.
					imagesPulled = true;
					
					// Display no images found.
					noneFoundObject.SetActive (true);
				}
			}
			
			// If the images have been pulled but not created...
			if(imagesPulled && !imageObjectsMade)
			{
				// Get the scroll content object.
				GameObject scrollParent = pictureScrollArea.transform.GetChild (0).GetChild (0).gameObject;
				
				MCP.pulledImages.Remove (null);
				int skippedDrawing = 0;
				
				// Make objects for each image.
				for(int i = 0; i < MCP.pulledImages.Count; i++)
				{
					if(MCP.pulledImages[i] != null && MCP.pulledImages[i].width > 8)
					{
						List<MyVoidFunc> funcList = new List<MyVoidFunc>{};
						
						// Make buttons for each image to allow them to be selected.
						GameObject imageObject = MakeButton (scrollParent, "", Sprite.Create(MCP.pulledImages[i], new Rect(0, 0, MCP.pulledImages[i].width, MCP.pulledImages[i].height), new Vector2(0.5f, 0.5f)), new Rect((Screen.width * 0.05f) + (Screen.width * 0.3f * (i - skippedDrawing)), Screen.height * -1.15f, Screen.width * 0.25f, Screen.height * 0.375f), funcList);
						imageObject.name = "ImageObject_" + i.ToString ();
						
						imageObject.GetComponent<Button>().onClick.AddListener (
						delegate
						{
							// Select image by splitting the name. Passing in 'i' will just set every button to the same number.
							SelectImage (int.Parse (imageObject.name.Split (new char[]{'_'})[1]));
						});
						
						pictureObjects.Add (imageObject);
						
						// Make a delete button for this picture.
						GameObject deleteButton = MakeActionButton (scrollParent, deleteSprite, new Rect((Screen.width * 0.125f) + (Screen.width * 0.3f * (i - skippedDrawing)), Screen.height * -0.175f, Screen.width * 0.1f, Screen.width * 0.1f), MakeDeletePopup, i);
						deleteButton.name = "DeleteButton";
						deleteButton.GetComponent<Image>().color = Color.red;
						pictureObjects.Add (deleteButton);
					}
					else
					{
						Debug.Log ("Null or error Found");
						skippedDrawing++;
					}
				}
				
				// Get the width of all of the pictures.
				float width = (Screen.width * 0.35f) + (Screen.width * 0.3f * (MCP.pulledImages.Count - skippedDrawing - 1));
				// Set the size of the scroll content object to this width.
				scrollParent.GetComponent<RectTransform>().sizeDelta = new Vector2(width, scrollParent.GetComponent<RectTransform>().sizeDelta.y);
				
				// Flag the images as created.
				imageObjectsMade = true;
			}
			
			// If the images are not created, show the loading popup.
			if(!imagesPulled || !imageObjectsMade)
			{
				loadingBackground.SetActive (true);
				UpdateLoadingPopup();
			}
			// Otherwise, hide the loading popup.
			else
			{
				loadingBackground.SetActive (false);
			}
		}
	}
	
	/// <summary>
	/// Selects the image.
	/// </summary>
	/// <param name="index">Index.</param>
	private void SelectImage(int index)
	{
		if(isClipArt)
		{
			MCP.loadedTexture = clipArtTextures[index];
		}
		else
		{
			MCP.loadedTexture = MCP.pulledImages[index];
		}
		Close ();
	}
	
	/// <summary>
	/// Close this instance and return to the main scene.
	/// </summary>
	void Close()
	{
		// Reenable the main scene canvas.
		sceneCanvas.SetActive (true);
		
		// Reenable other scripts.
		SendMessage ("FileBrowserClosed", SendMessageOptions.DontRequireReceiver);
		
		// Destroy all file browser game objects.
		Destroy (localCanvas);
		
		// Remove this script from the object to close.
		Destroy (this);
	}
	
	void OnDestroy()
	{
		for(int i = 0; i < clipArtTextures.Count; i++)
		{
			clipArtTextures[i] = null;
		}
	}
	
	public override void DoGUI() {}
}
