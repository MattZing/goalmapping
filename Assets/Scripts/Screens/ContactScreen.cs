using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ContactScreen : MenuScreen
{
	private List<GameObject> mainScreenGUI;
	private List<GameObject> messagingGUI;
	private List<GameObject> contactDirectSpecificGUI;
	private GameObject sectionDescriptionLabel;
	private GameObject messageTextObject;
	private GameObject subjectTextObject;
	private GameObject popup;
	private GameObject localCanvas;
	
	private Sprite contactSprite;
	private Sprite suggestionSprite;
	private Sprite reportBugSprite;
	
	private string subject = "";
	private string message = "";
	
	enum InternalState
	{
		Main,
		Contact,
		Suggestion,
		Bug,
		Popup
	}
	InternalState internalState = InternalState.Main;
	
	/// <summary>
	/// Used for initialization
	/// </summary>
	void Start ()
	{
		// Initialise the base.
		Init ();
		
		// Load resources
		contactSprite = Resources.Load<Sprite>("contact_direct");
		suggestionSprite = Resources.Load<Sprite>("suggest_box");
		reportBugSprite = Resources.Load<Sprite>("report_bug");
		
		// Initialize variable defaults
		mainScreenGUI = new List<GameObject>();
		messagingGUI = new List<GameObject>();
		contactDirectSpecificGUI = new List<GameObject>();
		
		// Create the scene GUI.
		CreateGUI ();
		
		localCanvas.transform.localScale = Vector2.zero;
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Find the current canvas.
		localCanvas = MakeCanvas ();
		
		// Make the title bar.
		MakeTitleBar (localCanvas, MCP.Text (601)/*"Contact Screen"*/);
		
		// Make the side gizmo.
		MakeSideGizmo (localCanvas, true);
		
		// Make the contact direct button.
		GameObject contactButtonBackground = MakeImage(localCanvas, new Rect(Screen.width * 0.1f, Screen.height * 0.325f, Screen.width * 0.25f, Screen.height * 0.4f), window_back, true);
		contactButtonBackground.name = "ContactButtonBackground";
		mainScreenGUI.Add (contactButtonBackground);
		
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			delegate
			{
				internalState = InternalState.Contact;
				sectionDescriptionLabel.GetComponent<Text>().text = MCP.Text (602);/*"Please enter your message";*/
				
				for(int i = 0; i < mainScreenGUI.Count; i++)
				{
					mainScreenGUI[i].SetActive (false);
				}
				
				for(int i = 0; i < messagingGUI.Count; i++)
				{
					messagingGUI[i].SetActive (true);
				}
			}
		};
		GameObject contactButton = MakeButton (localCanvas, "", contactSprite, new Rect(Screen.width * 0.1f, Screen.height * 0.35f, Screen.width * 0.25f, Screen.height * 0.35f), funcList);
		contactButton.name = "ContactButton";
		mainScreenGUI.Add (contactButton);
		
		// Make the suggestion box button.
		GameObject suggestionButtonBackground = MakeImage(localCanvas, new Rect(Screen.width * 0.375f, Screen.height * 0.325f, Screen.width * 0.25f, Screen.height * 0.4f), window_back, true);
		suggestionButtonBackground.name = "SuggestionButtonBackground";
		mainScreenGUI.Add (suggestionButtonBackground);
		
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				internalState = InternalState.Suggestion;
				sectionDescriptionLabel.GetComponent<Text>().text = MCP.Text (603);/* "\nPlease enter your suggestion";*/
				
				for(int i = 0; i < mainScreenGUI.Count; i++)
				{
					mainScreenGUI[i].SetActive (false);
				}
				
				for(int i = 0; i < messagingGUI.Count; i++)
				{
					messagingGUI[i].SetActive (true);
				}
				
				for(int i = 0; i < contactDirectSpecificGUI.Count; i++)
				{
					contactDirectSpecificGUI[i].SetActive (false);
				}
			}
		};
		GameObject suggestionButton = MakeButton (localCanvas, "", suggestionSprite, new Rect(Screen.width * 0.375f, Screen.height * 0.35f, Screen.width * 0.25f, Screen.height * 0.35f), funcList);
		suggestionButton.name = "SuggestionButton";
		mainScreenGUI.Add (suggestionButton);
		
		// Make the report a bug button.
		GameObject reportBugButtonBackground = MakeImage(localCanvas, new Rect(Screen.width * 0.65f, Screen.height * 0.325f, Screen.width * 0.25f, Screen.height * 0.4f), window_back, true);
		reportBugButtonBackground.name = "ReportBugButtonBackground";
		mainScreenGUI.Add (reportBugButtonBackground);
		
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				internalState = InternalState.Bug;
				sectionDescriptionLabel.GetComponent<Text>().text = MCP.Text (604);/*"We are sorry to hear that you are having a problem\n\nPlease enter as much information as you can";*/
				
				for(int i = 0; i < mainScreenGUI.Count; i++)
				{
					mainScreenGUI[i].SetActive (false);
				}
				
				for(int i = 0; i < messagingGUI.Count; i++)
				{
					messagingGUI[i].SetActive (true);
				}
				
				for(int i = 0; i < contactDirectSpecificGUI.Count; i++)
				{
					contactDirectSpecificGUI[i].SetActive (false);
				}
			}
		};
		GameObject reportBugButton = MakeButton (localCanvas, "", reportBugSprite, new Rect(Screen.width * 0.65f, Screen.height * 0.35f, Screen.width * 0.25f, Screen.height * 0.35f), funcList);
		reportBugButton.name = "ReportBugButton";
		mainScreenGUI.Add (reportBugButton);
		
		// Message background panel
		GameObject backgroundPanel = MakeImage (localCanvas, new Rect(Screen.width * 0.1f, Screen.height * 0.15f, Screen.width * 0.8f, Screen.height * 0.8f), window_back, true);
		backgroundPanel.name = "BackgroundPanel";
		messagingGUI.Add (backgroundPanel);
		
		sectionDescriptionLabel = MakeLabel (localCanvas, "", new Rect(Screen.width * 0.15f, Screen.height * 0.175f, Screen.width * 0.7f, Screen.height * 0.2f), TextAnchor.UpperCenter, "InnerLabel");
		sectionDescriptionLabel.name = "SectionDescriptionLabel";
		messagingGUI.Add(sectionDescriptionLabel);
		
		GameObject subjectLabel = MakeLabel (localCanvas, MCP.Text (605)/*"Subject: "*/, new Rect(Screen.width * 0.15f, Screen.height * 0.235f, Screen.width * 0.2f, Screen.height * 0.1f), TextAnchor.MiddleLeft, "InnerLabel");
		subjectLabel.name = "SubjectLabel";
		messagingGUI.Add (subjectLabel);
		contactDirectSpecificGUI.Add (subjectLabel);
		
		subjectTextObject = MakeInputField (localCanvas, subject, new Rect(Screen.width * 0.25f, Screen.height * 0.235f, Screen.width * 0.6f, Screen.height * 0.1f), false, menuBackground);
		subjectTextObject.name = "SubjectTextObject";
		subjectTextObject.GetComponent<Text>().color = bmOrange;
		messagingGUI.Add (subjectTextObject.transform.parent.gameObject);
		contactDirectSpecificGUI.Add (subjectTextObject.transform.parent.gameObject);
		
		GameObject messageLabel = MakeLabel (localCanvas, MCP.Text (606)/*"Message: "*/, new Rect(Screen.width * 0.15f, Screen.height * 0.35f, Screen.width * 0.6f, Screen.height * 0.05f), TextAnchor.UpperLeft, "InnerLabel");
		messageLabel.name = "MessageLabel";
		messagingGUI.Add (messageLabel);
		
		messageTextObject = MakeInputField (localCanvas, message, new Rect(Screen.width * 0.15f, Screen.height * 0.4f, Screen.width * 0.7f, Screen.height * 0.4f), false, menuBackground);
		messageTextObject.name = "MessageTextObject";
		messageTextObject.GetComponent<Text>().color = bmOrange;
		messageTextObject.GetComponent<InputField>().lineType = InputField.LineType.MultiLineNewline;
		messagingGUI.Add (messageTextObject.transform.parent.gameObject);
		
		GameObject cancelMessageButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.2f, Screen.height * 0.84f, Screen.width * 0.2f, Screen.height * 0.08f), buttonBackground, true);
		cancelMessageButtonBackground.name = "CancelMessageButtonBackground";
		messagingGUI.Add (cancelMessageButtonBackground);
		
		funcList = new List<MyVoidFunc>
		{
			Back
		};
		GameObject cancelMessageButton = MakeButton (localCanvas, MCP.Text (207)/*"Cancel"*/, new Rect(Screen.width * 0.2f, Screen.height * 0.84f, Screen.width * 0.2f, Screen.height * 0.08f), funcList);
		cancelMessageButton.name = "CancelMessageButton";
		cancelMessageButton.GetComponent<Button>().image = cancelMessageButtonBackground.GetComponent<Image>();
		cancelMessageButton.GetComponent<Text>().color = bmOrange;
		cancelMessageButton.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
		messagingGUI.Add (cancelMessageButton);
		
		GameObject sendMessageButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.6f, Screen.height * 0.84f, Screen.width * 0.2f, Screen.height * 0.08f), buttonBackground, true);
		sendMessageButtonBackground.name = "sendMessageButtonBackground";
		messagingGUI.Add (sendMessageButtonBackground);
		
		funcList = new List<MyVoidFunc>
		{
			SendUserMessage
		};
		GameObject sendMessageButton = MakeButton (localCanvas, MCP.Text (607)/*"Send"*/, new Rect(Screen.width * 0.6f, Screen.height * 0.84f, Screen.width * 0.2f, Screen.height * 0.08f), funcList);
		sendMessageButton.name = "SendMessageButton";
		sendMessageButton.GetComponent<Button>().image = sendMessageButtonBackground.GetComponent<Image>();
		sendMessageButton.GetComponent<Text>().color = bmOrange;
		sendMessageButton.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
		messagingGUI.Add (sendMessageButton);
		
		for(int i = 0; i < messagingGUI.Count; i++)
		{
			messagingGUI[i].SetActive (false);
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(internalState == InternalState.Popup)
		{
			if(popup == null)
			{
				internalState = InternalState.Main;
				
				for(int i = 0; i < mainScreenGUI.Count; i++)
				{
					mainScreenGUI[i].SetActive (true);
				}
			}
		}
	}
	
	public override void Back ()
	{
		if(internalState == InternalState.Main)
		{
			base.Back ();
		}
		else
		{
			internalState = InternalState.Popup;
			popup = MakePopup (localCanvas, defaultPopUpWindow, "", MCP.Text (608)/*"Message Sent"*/, MCP.Text (205)/*"Dismiss"*/, CommonTasks.DestroyParent);
			
			for(int i = 0; i < messagingGUI.Count; i++)
			{
				messagingGUI[i].SetActive (false);
			}
			
			// Empty text boxes
			subjectTextObject.GetComponent<InputField>().text = "";
			messageTextObject.GetComponent<InputField>().text = "";
		}
	}
	
	void SendUserMessage()
	{
		// Send user message
		StartCoroutine (MCP.SubmitContactMessage (subjectTextObject.GetComponent<Text>().text, messageTextObject.GetComponent<Text>().text));
		
		Back ();
	}
	
	public override void DoGUI (){}
}
