﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CreateAccount_Screen : MenuScreen
{
	private List<GameObject> tabableInputObjects;
	private GameObject localCanvas;
	private GameObject spinnerBackground;
	private GameObject spinner;
	private GameObject emailInputObject;
	private GameObject fNameInputObject;
	private GameObject lNameInputObject;
	private GameObject usernameInputObject;
	private GameObject passwordInputObject;
	private GameObject confirmPasswordInputObject;
	private GameObject promoCodeInputObject;
	private GameObject referrerInputObject;
	private GameObject tAndcCheckbox;
	private GameObject createAccountButton;
	
	private Sprite spinnerSprite;
	
	private bool registering = false;
	private float spinnerRot = 0f;
//	private System.DateTime dob;
	private string firstName = "";
	private string lastName = "";
	private string email = "";
	private string username = "";
	private string password = "";
	private string confirmPassword = "";
	private string promoCode = "";
	private string referrer = "";

	/// <summary>
	/// Used for initialization
	/// </summary>
	void Start()
	{
		// Initialize the base class.
		Init ();
		
		tabableInputObjects = new List<GameObject>();
		
		// Load resources.
		spinnerSprite = Resources.Load<Sprite>("GUI/bm_rotate");
		
		if(Debug.isDebugBuild)
		{
//			firstName = "Test";
//			lastName = "Test";
//			email = "matt@zingup.com";
//			username = "TestMatt01";
//			password = "password";
//			confirmPassword = "password";
		}
			
		// Create the gui.
		CreateGUI();
		
		localCanvas.transform.localScale = Vector2.zero;
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Get a reference to the scene canvas.
		localCanvas = MakeCanvas ();
		
		// Make Title
		MakeTitleBar(localCanvas, MCP.Text (701)/*"Create Account"*/, "ScreenTitle");
		
		// Make side gizmo back button
		MakeSideGizmo (localCanvas, false);
		
		// Make background panel
		MakeWindowBackground (localCanvas, new Rect(Screen.width * 0.01f, Screen.height * 0.15f, Screen.width * 0.98f, Screen.height * 0.8f), window_back);
		
		// Make scroll area
		GameObject scrollArea = MakeScrollRect (localCanvas, new Rect(Screen.width * 0.025f, Screen.height * 0.2f, Screen.width * 0.9f, Screen.height * 0.6f), null);
		scrollArea.GetComponent<ScrollRect>().horizontal = false;
		GameObject scrollContent = scrollArea.transform.GetChild (0).GetChild (0).gameObject;
		
		float yOffset = Screen.height * -0.6f;
		
		// Make email label
		MakeLabel (scrollContent, MCP.Text (702)/*"Email"*/, new Rect(Screen.width * 0.575f, yOffset, Screen.width * 0.2f, Screen.height * 0.1f), TextAnchor.MiddleLeft, "InnerLabel").name = "EmailLabel";
		
		// Make email input field.
		emailInputObject = MakeInputField (scrollContent, email, new Rect(Screen.width * 0.8f, yOffset, Screen.width * 0.5f, Screen.height * 0.08f), false, inputBackground);
		emailInputObject.name = "EmailInputField";
		emailInputObject.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
		emailInputObject.GetComponent<InputField>().contentType = InputField.ContentType.EmailAddress;
		tabableInputObjects.Add (emailInputObject);
		
		yOffset += Screen.height * 0.15f;
		
		// Make email label
		MakeLabel (scrollContent, MCP.Text (713)/*"First name"*/, new Rect(Screen.width * 0.575f, yOffset, Screen.width * 0.2f, Screen.height * 0.1f), TextAnchor.MiddleLeft, "InnerLabel").name = "EmailLabel";
		
		// Make email input field.
		fNameInputObject = MakeInputField (scrollContent, firstName, new Rect(Screen.width * 0.8f, yOffset, Screen.width * 0.5f, Screen.height * 0.08f), false, inputBackground);
		fNameInputObject.name = "fNameInputObject";
		fNameInputObject.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
		tabableInputObjects.Add (fNameInputObject);
		
		yOffset += Screen.height * 0.15f;
		
		// Make email label
		MakeLabel (scrollContent, MCP.Text (714)/*"last name"*/, new Rect(Screen.width * 0.575f, yOffset, Screen.width * 0.2f, Screen.height * 0.1f), TextAnchor.MiddleLeft, "InnerLabel").name = "EmailLabel";
		
		// Make email input field.
		lNameInputObject = MakeInputField (scrollContent, lastName, new Rect(Screen.width * 0.8f, yOffset, Screen.width * 0.5f, Screen.height * 0.08f), false, inputBackground);
		lNameInputObject.name = "lNameInputObject";
		lNameInputObject.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
		tabableInputObjects.Add (lNameInputObject);
		
		yOffset += Screen.height * 0.15f;
		
		// Make username label
		MakeLabel (scrollContent, MCP.Text (703)/*"Create Username"*/, new Rect(Screen.width * 0.575f, yOffset, Screen.width * 0.2f, Screen.height * 0.1f), TextAnchor.MiddleLeft, "InnerLabel").name = "CreateUsernameLabel";
		
		// Make Username input box
		usernameInputObject = MakeInputField (scrollContent, username, new Rect(Screen.width * 0.8f, yOffset, Screen.width * 0.5f, Screen.height * 0.08f), false, inputBackground);
		usernameInputObject.name = "UsernameInputField";
		usernameInputObject.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
		tabableInputObjects.Add (usernameInputObject);
		
		yOffset += Screen.height * 0.15f;
		
		// Make password label
		MakeLabel (scrollContent, MCP.Text (704)/*"Password"*/, new Rect(Screen.width * 0.575f, yOffset, Screen.width * 0.2f, Screen.height * 0.1f), TextAnchor.MiddleLeft, "InnerLabel").name = "PasswordLabel";
		
		// Make password input box
		passwordInputObject = MakeInputField (scrollContent, password, new Rect(Screen.width * 0.8f, yOffset, Screen.width * 0.5f, Screen.height * 0.08f), true, inputBackground);
		passwordInputObject.name = "PasswordInputField";
		passwordInputObject.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
		tabableInputObjects.Add (passwordInputObject);
		
		yOffset += Screen.height * 0.15f;
		
		// Make password confirmation label
		MakeLabel (scrollContent, MCP.Text (705)/*"Confirm Password"*/, new Rect(Screen.width * 0.575f, yOffset, Screen.width * 0.2f, Screen.height * 0.1f), TextAnchor.MiddleLeft, "InnerLabel").name = "PasswordConfirmLabel";
		
		// Make password confirmation input box
		confirmPasswordInputObject = MakeInputField (scrollContent, confirmPassword, new Rect(Screen.width * 0.8f, yOffset, Screen.width * 0.5f, Screen.height * 0.08f), true, inputBackground);
		confirmPasswordInputObject.name = "PasswordConfirmInputField";
		confirmPasswordInputObject.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
		tabableInputObjects.Add (confirmPasswordInputObject);
		
		yOffset += Screen.height * 0.15f;
		
		// Make promo code label
		MakeLabel (scrollContent, MCP.Text (706)/*"Promo Code"*/, new Rect(Screen.width * 0.575f, yOffset, Screen.width * 0.2f, Screen.height * 0.1f), TextAnchor.MiddleLeft, "InnerLabel").name = "PromoCodeLabel";
		
		// Make promo code applicable label
		GameObject promoCodeApplicableLabel = MakeLabel (scrollContent, MCP.Text (708)/*"(If applicable)"*/, new Rect(Screen.width * 0.575f, yOffset + (Screen.height * 0.05f), Screen.width * 0.2f, Screen.height * 0.1f), TextAnchor.MiddleLeft, "SmallText");
		promoCodeApplicableLabel.name = "PromoCodeApplicableLabel";
		promoCodeApplicableLabel.GetComponent<Text>().color = bmPink;
		
		// Make promo code input box
		promoCodeInputObject = MakeInputField (scrollContent, promoCode, new Rect(Screen.width * 0.8f, yOffset, Screen.width * 0.5f, Screen.height * 0.08f), false, inputBackground);
		promoCodeInputObject.name = "PromoCodeInputField";
		promoCodeInputObject.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
		tabableInputObjects.Add (promoCodeInputObject);
		
		yOffset += Screen.height * 0.15f;
		
		// Make referrer label
		MakeLabel (scrollContent, MCP.Text (707)/*"Referrer"*/, new Rect(Screen.width * 0.575f, yOffset, Screen.width * 0.2f, Screen.height * 0.1f), TextAnchor.MiddleLeft, "InnerLabel").name = "ReferrerLabel";
		
		// Make referer applicable label.
		GameObject referrerApplicableLabel = MakeLabel (scrollContent, MCP.Text (708)/*"(If applicable)"*/, new Rect(Screen.width * 0.575f, yOffset + (Screen.height * 0.05f), Screen.width * 0.2f, Screen.height * 0.1f), TextAnchor.MiddleLeft, "SmallText");
		referrerApplicableLabel.name = "ReferrerApplicableLabel";
		referrerApplicableLabel.GetComponent<Text>().color = bmPink;
		
		// Make referrer input box
		referrerInputObject = MakeInputField (scrollContent, referrer, new Rect(Screen.width * 0.8f, yOffset, Screen.width * 0.5f, Screen.height * 0.08f), false, inputBackground);
		referrerInputObject.name = "ReferrerInputField";
		referrerInputObject.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
		tabableInputObjects.Add (referrerInputObject);
		
		yOffset += Screen.height * 0.15f;
		
		// Make tap for t&c button
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			delegate
			{
				// Load t&cs.
//				LoadMenu ("EULA_Screen");
				gameObject.AddComponent<EULA>();
				gameObject.GetComponent<EULA>().sceneCanvas = localCanvas;
				localCanvas.SetActive (false);
			}
		};
		GameObject showtAndcButton = MakeButton (scrollContent, MCP.Text (709)/*"Tap here for Terms and Conditions"*/, new Rect(Screen.width * 0.575f, yOffset, Screen.width * 0.6765f, Screen.height * 0.1f), funcList, "InnerLabel");
		showtAndcButton.name = "ShowTAndCButton";
		showtAndcButton.GetComponentInChildren<Text>().alignment = TextAnchor.MiddleLeft;
		showtAndcButton.GetComponentInChildren<Text>().color = bmPink;
		// Remove the button background.
		Destroy(showtAndcButton.GetComponent<Image>());
		
		yOffset += Screen.height * 0.45f;
		
		// Make read t&c checkbox
		tAndcCheckbox = MakeCheckbox (scrollContent, new Rect(Screen.width * 1.025f, yOffset, Screen.width * 0.8f, Screen.height * 0.08f), MCP.Text (710)/*"I have read and accept the Terms and Conditions"*/, "InnerLabel");
		tAndcCheckbox.name = "TAndCCheckbox";
		tAndcCheckbox.GetComponentInChildren<Text>().color = bmPink;
		
		yOffset -= Screen.height * 0.1f;
		
		// Make create account button.
		GameObject createAccountButtonBackground = MakeImage (scrollContent, new Rect(Screen.width * 0.8f, yOffset, Screen.width * 0.5f, Screen.height * 0.1f), buttonBackground, true);
		createAccountButtonBackground.name = "CreateAccountButtonBackground";
		funcList = new List<MyVoidFunc>
		{
			CreateAccount
		};
		createAccountButton = MakeButton (scrollContent, MCP.Text (701)/*"Create Account"*/, new Rect(Screen.width * 0.8f, yOffset, Screen.width * 0.5f, Screen.height * 0.1f), funcList, "OrangeText");
		createAccountButton.name = "CreateAccountButton";
		createAccountButton.GetComponent<Button>().image = createAccountButtonBackground.GetComponent<Image>();
		createAccountButton.GetComponent<Button>().interactable = false;
		
		scrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width * 0.6f, (Screen.height * 0.9f) + yOffset);
		scrollContent.GetComponent<RectTransform>().transform.localPosition = new Vector2(0, -Screen.height * 3.1f);
		
		spinnerBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.25f, Screen.height * 0.25f, Screen.width * 0.5f, Screen.height * 0.5f), menuBackground);
		spinnerBackground.name = "SpinnerBackground";
		
		GameObject spinnerText = MakeLabel (localCanvas, MCP.Text (718)/*"Creating Account..."*/, new Rect(Screen.width * 0.3f, Screen.height * 0.35f, Screen.width * 0.4f, Screen.height * 0.2f));
		spinnerText.name = "SpinnerText";
		spinnerText.transform.SetParent (spinnerBackground.transform, true);
		
		spinner = MakeImage (localCanvas, new Rect(Screen.width * 0.5f, Screen.height * 0.4f, Screen.width * 0.1f, Screen.width * 0.1f), spinnerSprite);
		spinner.name = "Spinner";
		spinner.transform.SetParent (spinnerBackground.transform, true);
		spinner.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
		
		spinnerBackground.SetActive (false);
	}
	
	/// <summary>
	/// Creates the account.
	/// </summary>
	void CreateAccount()
	{
		if(password == confirmPassword)
		{
			// Send account information to be created.
			StartCoroutine (MCP.RegisterClient (username, firstName, lastName, email, System.DateTime.Now, password, promoCode));
			registering = true;
		}
		else
		{
			// Popup saying passwords aren't the same.
			MakePopup (localCanvas, defaultPopUpWindow, MCP.Text (711)/*"Passwords Don't Match!"*/, MCP.Text (712)/*"The confirm password does not match the initial password."*/, MCP.Text (203)/*"OK"*/, CommonTasks.DestroyParent);
		}
	}
	
	/// <summary>
	/// Updates the input strings.
	/// </summary>
	void UpdateInputStrings()
	{
		email = emailInputObject.GetComponent<InputField>().text;
		firstName = fNameInputObject.GetComponent<InputField>().text;
		lastName = lNameInputObject.GetComponent<InputField>().text;
		username = usernameInputObject.GetComponent<InputField>().text;
		password = passwordInputObject.GetComponent<InputField>().text;
		confirmPassword = confirmPasswordInputObject.GetComponent<InputField>().text;
		promoCode = promoCodeInputObject.GetComponent<InputField>().text;
		referrer = referrerInputObject.GetComponent<InputField>().text;
	}
	
	/// <summary>
	/// Checks if create account button valid.
	/// </summary>
	/// <returns><c>true</c>, if the input data is valid, <c>false</c> otherwise.</returns>
	bool CheckIfCreateAccountButtonValid()
	{
		bool valid = true;
		
		// Make sure the email is valid.
		if(email == "" || !email.Contains ("@") || !email.Contains ("."))
		{
			valid = false;
		}
		// Make sure the other fields are not empty.
		if(username == "" || password == "" || confirmPassword == "")
		{
			valid = false;
		}
		// Make sure they have accepted the terms and conditions.
		if(!tAndcCheckbox.GetComponent<Toggle>().isOn)
		{
			valid = false;
		}
		
		return valid;
	}
	
	void TabThroughInputs()
	{
		if(Input.GetKeyDown (KeyCode.Tab))
		{
			for (int i = 0; i < tabableInputObjects.Count; i++)
			{
				if(tabableInputObjects[i].GetComponent<InputField>().isFocused)
				{
					if(i < tabableInputObjects.Count - 1)
					{
						tabableInputObjects[i + 1].GetComponent<InputField>().Select();
					}
					else
					{
						tabableInputObjects[0].GetComponent<InputField>().Select();
					}
				}
			}
		}
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update()
	{
#if !UNITY_ANDROID && !UNITY_IOS
		TabThroughInputs ();
#endif
		
		// Update the stored strings to reflect the input fields.
		UpdateInputStrings();
		
		// Update the button interactability.
		if(CheckIfCreateAccountButtonValid ())
		{
			createAccountButton.GetComponent<Button>().interactable = true;
		}
		else
		{
			createAccountButton.GetComponent<Button>().interactable = false;
		}
		
		// If registering is flagged...
		if(registering)
		{
			// If the account is being created...
			if(MCP.isTransmitting)
			{
				// Activate the spinner object.
				spinnerBackground.SetActive (true);
				// Rotate spinner
				spinnerRot -= Time.deltaTime * 30f;
				spinner.transform.localEulerAngles = new Vector3(0, 0, spinnerRot);
			}
			// If the server call is finished...
			else
			{
				// Reset registering flag.
				registering = false;
				// Deactivate the spinner object.
				spinnerBackground.SetActive (false);
				
				// If the feedback from the server is success...
				if(MCP.createdUserMessage == "Success")
				{
					MCP.createdUserEmail = email;
					// Move to the account made screen.
					Application.LoadLevel ("AccountCreated_Screen");
				}
				// Otherwise...
				else
				{
					// Make a popup to inform the user.
					GameObject popup = MakePopup (localCanvas, defaultPopUpWindow, MCP.Text (715)/*"Error"*/, MCP.Text (716)/*"There has been a problem creating your account. Please try again later."*/, MCP.Text (206)/*"Close"*/, CommonTasks.DestroyParent);
					popup.name = "Popup";
					
					// If the feedback is that the username is taken, then inform the user of this.
					if(MCP.createdUserMessage == "UsernameTaken")
					{
						popup.transform.GetChild (1).gameObject.GetComponent<Text>().text = MCP.Text (717)/*"This username is already taken."*/;
					}
				}
			}
		}
	}
	
	public override void DoGUI() {}
}
