﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Unibill;
using Unibill.Impl;

[AddComponentMenu("Unibill/UnibillDemo")]
public class UpgradeScreen : MenuScreen
{
	public GameObject localCanvas;
	
	private Sprite silverSprite;
	private Sprite goldSprite;
//	private Sprite platinumSprite;
	
	private PurchasableItem[] items;
	private static bool purchaseMade = false;
	
	// Use this for initialization
	void Start ()
	{
		if (UnityEngine.Resources.Load ("unibillInventory.json") == null)
		{
			Debug.LogError("You must define your purchasable inventory within the inventory editor!");
			this.gameObject.SetActive(false);
			return;
		}
		
		if(!Unibiller.Initialised)
		{
			// We must first hook up listeners to Unibill's events.
			Unibiller.onBillerReady += onBillerReady;
			Unibiller.onTransactionsRestored += onTransactionsRestored;
			Unibiller.onPurchaseFailed += onFailed;
			Unibiller.onPurchaseCompleteEvent += onPurchased;
			Unibiller.onPurchaseDeferred += onDeferred;
			
			// Now we're ready to initialise Unibill.
			Unibiller.Initialise();
		}
		
		// iOS includes additional functionality around App receipts.
#if UNITY_IOS
		var appleExtensions = Unibiller.getAppleExtensions();
		appleExtensions.onAppReceiptRefreshed += x => {
			Debug.Log(x);
			Debug.Log("Refreshed app receipt!");
		};
		
		appleExtensions.onAppReceiptRefreshFailed += () => {
			Debug.Log("Failed to refresh app receipt.");
		};
#endif
		
		items = Unibiller.AllPurchasableItems;
		
		// Initialize the base class.
		Init();
		
		// Load Resources
		silverSprite = Resources.Load<Sprite>("GUI/SuccessCentre/success_centre_silver");
		goldSprite = Resources.Load<Sprite>("GUI/SuccessCentre/success_centre_gold");
//		platinumSprite = Resources.Load<Sprite>("GUI/SuccessCentre/success_centre_silver");
	
		// Create the GUI.
		CreateGUI();
		
		localCanvas.transform.localScale = Vector2.zero;
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Make the canvas.
		localCanvas = MakeCanvas (true);
		
		// Make the title bar.
		MakeTitleBar (localCanvas, MCP.Text (3605)/*"Increase your Goal Mapping Potential"*/);
		
		// Make the side gizmo.
		MakeSideGizmo (localCanvas, true);
		
		// Make background panel.
		GameObject backgroundPanel = MakeWindowBackground (localCanvas, new Rect(Screen.width * 0.02f, Screen.height * 0.15f, Screen.width * 0.96f, Screen.height * 0.83f), window_back);
		backgroundPanel.name = "BackgroundPanel";
		
		// Make the silver button.
		List<MyVoidFunc> funcList = new List<MyVoidFunc>()
		{
			SilverButton
		};
		GameObject silverButton = MakeIconButton (localCanvas, MCP.Text (3601)/*"Silver"*/, iconButton, silverSprite, new Rect(Screen.width * 0.225f, Screen.height * 0.25f, Screen.width * 0.2f, Screen.height * 0.275f), funcList);
		silverButton.name = "SilverButton";
		
		GameObject silverText = MakeLabel (localCanvas, MCP.Text (3603)/*"Silver gives you 7 goal maps and access to the silver resources."*/, new Rect(Screen.width * 0.225f, Screen.height * 0.575f, Screen.width * 0.2f, Screen.height * 0.3f));
		silverText.name = "SilverText";
		silverText.GetComponent<Text>().fontSize = (int)_fontSize;
		
		// Make the gold button.
		funcList = new List<MyVoidFunc>()
		{
			GoldButton
		};
		GameObject goldButton = MakeIconButton (localCanvas, MCP.Text (3602)/*"Gold"*/, iconButton, goldSprite, new Rect(Screen.width * 0.575f, Screen.height * 0.25f, Screen.width * 0.2f, Screen.height * 0.275f), funcList);
		goldButton.name = "GoldButton";
		
		GameObject goldText = MakeLabel (localCanvas, MCP.Text (3604)/*"Gold gives you 30 goal maps and access to the gold resources."*/, new Rect(Screen.width * 0.575f, Screen.height * 0.575f, Screen.width * 0.2f, Screen.height * 0.3f));
		goldText.name = "GoldText";
		goldText.GetComponent<Text>().fontSize = (int)_fontSize;
		
//		// Make the platinum button.
//		funcList = new List<MyVoidFunc>()
//		{
//			PlatinumButton
//		};
//		GameObject platinumButton = MakeIconButton (localCanvas, "Platinum", iconButton, platinumSprite, new Rect(Screen.width * 0.7f, Screen.height * 0.25f, Screen.width * 0.15f, Screen.height * 0.275f), funcList);
//		platinumButton.name = "PlatinumButton";
//		
//		GameObject platinumText = MakeLabel (localCanvas, "Platinum gives you 30 goal maps and access to the all resources.", new Rect(Screen.width * 0.7f, Screen.height * 0.6f, Screen.width * 0.15f, Screen.height * 0.3f));
//		platinumText.name = "PlatinumText";
//		platinumText.GetComponent<Text>().fontSize = (int)_fontSize;
		
		// Make the restore transactions button.
		funcList = new List<MyVoidFunc>
		{
			Unibiller.restoreTransactions
		};
		GameObject restoreButton = MakeButton (localCanvas, MCP.Text (3606)/*"Restore Transactions"*/, buttonBackground, new Rect(Screen.width * 0.3f, Screen.height * 0.88f, Screen.width * 0.4f, Screen.height * 0.06f), funcList);
		restoreButton.name = "RestoreButton";
	}
	
	/// <summary>
	/// This will be called when Unibill has finished initialising.
	/// </summary>
	private void onBillerReady(UnibillState state) {
		UnityEngine.Debug.Log("onBillerReady:" + state);
		if (state != UnibillState.CRITICAL_ERROR) {
			Debug.Log ("Available items:");
			foreach (var item in Unibiller.AllPurchasableItems) {
				if (item.AvailableToPurchase) {
					Debug.Log (string.Join (" - ",
					                        new string[] {
						item.localizedTitle,
						item.localizedDescription,
						item.isoCurrencySymbol,
						item.priceInLocalCurrency.ToString (),
						item.localizedPriceString
					}));
				}
			}
		}
	}
	
	/// <summary>
	/// This will be called after a call to Unibiller.restoreTransactions().
	/// </summary>
	private void onTransactionsRestored (bool success)
	{
		Debug.Log("Transactions restored.");
	}
	
	/// <summary>
	/// This will be called when a purchase completes.
	/// </summary>
	private void onPurchased(PurchaseEvent e)
	{
		Debug.Log("Purchase OK: " + e.PurchasedItem.Id);
		Debug.Log ("Receipt: " + e.Receipt);
		Debug.Log(string.Format ("{0} has now been purchased {1} times.",
		                         e.PurchasedItem.name,
		                         Unibiller.GetPurchaseCount(e.PurchasedItem)));
		                         
		GameObject popup = MakePopup (localCanvas, defaultPopUpWindow, MCP.Text (3607)/*"Payment Successful!"*/, MCP.Text (3608)/*"You have been upgraded to "*/ + e.PurchasedItem.name, MCP.Text (205)/*"Dismiss"*/, CommonTasks.DestroyParent);
		popup.name = "Popup";
		
		UpgradeAccount (e.PurchasedItem.name);
	}
	
	/// <summary>
	/// iOS Specific.
	/// This is called as part of Apple's 'Ask to buy' functionality,
	/// when a purchase is requested by a minor and referred to a parent
	/// for approval.
	/// 
	/// When the purchase is approved or rejected, the normal purchase events
	/// will fire.
	/// </summary>
	/// <param name="item">Item.</param>
	private void onDeferred(PurchasableItem item)
	{
		Debug.Log ("Purchase deferred blud: " + item.Id);
	}
	
	/// <summary>
	/// This will be called is an attempted purchase fails.
	/// </summary>
	private void onFailed(PurchaseFailedEvent e)
	{
		Debug.Log("Purchase failed: " + e.PurchasedItem.Id);
		Debug.Log (e.Reason);
		
		if(localCanvas == null)
		{
			localCanvas = GameObject.Find ("Canvas");
		}
		
		switch(e.Reason)
		{
		case PurchaseFailureReason.CANNOT_REPURCHASE_NON_CONSUMABLE:
			GameObject alreadyBoughtPopup = MakePopup (localCanvas, defaultPopUpWindow, MCP.Text (3609)/*"Payment Failed!"*/, MCP.Text (3610)/*"You have already payed for this. Please restore your transactions."*/, MCP.Text (205)/*"Dismiss"*/, CommonTasks.DestroyParent);
			alreadyBoughtPopup.name = "AlreadyBoughtPopup";
			UpgradeAccount (e.PurchasedItem.name);
			break;
			
		default:
			GameObject failedPopup = MakePopup (localCanvas, defaultPopUpWindow, MCP.Text (3609)/*"Payment Failed!"*/, e.Reason.ToString (), MCP.Text (205)/*"Dismiss"*/, CommonTasks.DestroyParent);
			failedPopup.name = "FailedPopup";
			break;
		}
	}
	
	void UpgradeAccount(string accountType)
	{
		if(MCP.userInfo != null && MCP.userInfo.memberProfile != null)
		{
			switch(accountType)
			{
			case "Silver":
				if(int.Parse(MCP.userInfo.memberProfile.goalMapLevel) < 1)
				{
					MCP.userInfo.memberProfile.goalMapLevel = "1";
				}
				break;
				
			case "Gold":
				if(int.Parse(MCP.userInfo.memberProfile.goalMapLevel) < 2)
				{
					MCP.userInfo.memberProfile.goalMapLevel = "2";
				}
				break;
				
			case "Platinum":
				if(int.Parse(MCP.userInfo.memberProfile.goalMapLevel) < 3)
				{
					MCP.userInfo.memberProfile.goalMapLevel = "3";
				}
				break;
			}
			
			purchaseMade = true;
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(purchaseMade)
		{
			StartCoroutine (MCP.UpdateMemberProfile());
			purchaseMade = false;
		}
	}
	
	void SilverButton()
	{
		Unibiller.initiatePurchase(items[0]);
	}
	
	void GoldButton()
	{
		Unibiller.initiatePurchase(items[1]);
	}
	
	void PlatinumButton()
	{
		Unibiller.initiatePurchase(items[2]);
	}
	
	void OnDestroy()
	{
		
	}
	
	public override void DoGUI() {}
}
