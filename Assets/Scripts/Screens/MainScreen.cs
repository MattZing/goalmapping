﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MainScreen : MenuScreen
{
	private Sprite advertSprite;
	private Sprite myProfileIcon;
	private Sprite goalMapsIcon;
	private Sprite successCentre;
	private Sprite myMessages;
	private Sprite toDoLists;
	
#if UNITY_ANDROID || UNITY_IPHONE
	private Sprite calendar;
#endif
	
	private GameObject localCanvas;
	
	private bool notificationsChecked = false;

	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		// Initialise the base.
		Init ();
		
		// Clear the previous scene states
		lastMenuScreen = new System.Collections.Generic.List<string>();
		
		// Load resources.
		advertSprite = Resources.Load<Sprite> ("bm_logo2");
		myProfileIcon = Resources.Load<Sprite> ("GUI/bm_myprofile");
		goalMapsIcon = Resources.Load<Sprite> ("GUI/bm_mygoalmaps");
		successCentre = Resources.Load<Sprite> ("GUI/bm_successcentre");
		myMessages = Resources.Load<Sprite> ("GUI/bm_mymessages");
		toDoLists = Resources.Load<Sprite> ("GUI/todo_icon");
		
#if UNITY_ANDROID || UNITY_IPHONE
		calendar = Resources.Load<Sprite> ("GUI/bm_calendar");
#endif
		
		// Get the user's member profile.
		if(MCP.userInfo != null)
		{
			StartCoroutine (MCP.GetMemberProfile ());
			StartCoroutine (MCP.GetFriends (MCP.userInfo.username));
			StartCoroutine (MCP.GetNotifications());
			
			if(MCP.clipArtBundle == null)
			{
				StartCoroutine (MCP.GetAssetBundle ("clipart.clipart", false));
			}
			
			if(MCP.userInfo != null)
			{
				StartCoroutine (MCP.GetGoalMap (MCP.userInfo.username));
			}
		}
		
		// Create the scene GUI.
		CreateGUI ();
		
		localCanvas.transform.localScale = Vector2.zero;
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Find the current canvas.
		localCanvas = MakeCanvas ();
		
		// Make the title bar.
		GameObject titleBar = MakeTitleBar (localCanvas, MCP.Text (1301)/*"Goal Mapping World"*/);
		titleBar.name = "TitleBar";

		// Make the side gizmo.
		MakeSideGizmo(localCanvas, true);

		// Make advert panel
		GameObject advertPanel = MakeImage (localCanvas, new Rect(Screen.width * 0.075f, Screen.height * 0.1525f, Screen.width * 0.2f, Screen.height * 0.6675f), advertSprite);
		advertPanel.name = "Advert Panel";

		// Make the my profile button.
		GameObject myProfileButton = MakeIconButton (localCanvas, MCP.Text (1302)/*"My Profile"*/, iconButton, myProfileIcon, new Rect(Screen.width * 0.3f, Screen.height * 0.1525f, Screen.width * 0.2f, Screen.height * 0.3f), CommonTasks.LoadLevel, "Profile_Screen");
		myProfileButton.name = "MyProfileButton";
		
		// Make the goal maps button.
		GameObject goalMapsButton = MakeIconButton (localCanvas, MCP.Text (1303)/*"My Goal Maps"*/, iconButton, goalMapsIcon, new Rect(Screen.width * 0.525f, Screen.height * 0.1525f, Screen.width * 0.2f, Screen.height * 0.3f), CommonTasks.LoadLevel, "GoalMaps_Screen");
		goalMapsButton.name = "GoalMapsButton";
		goalMapsButton.transform.GetChild (0).gameObject.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
		
		// Make the goal maps button.
		GameObject successCentreButton = MakeIconButton (localCanvas, MCP.Text (1304)/*"Success Centre"*/, iconButton, successCentre, new Rect(Screen.width * 0.75f, Screen.height * 0.1525f, Screen.width * 0.2f, Screen.height * 0.3f), CommonTasks.LoadLevel, "SuccessCentre_Screen");
		successCentreButton.name = "SuccessCentreButton";
		successCentreButton.transform.GetChild (0).gameObject.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
		
		// Make the social button.
		GameObject socialButton = MakeIconButton (localCanvas, MCP.Text (1305)/*"Messages & Friends"*/, iconButton, myMessages, new Rect(Screen.width * 0.3f, Screen.height * 0.525f, Screen.width * 0.2f, Screen.height * 0.3f), CommonTasks.LoadLevel, "Social_Screen");
		socialButton.name = "SocialButton";
		
		// Make the social button.
		GameObject taskListButton = MakeIconButton (localCanvas, MCP.Text (1307)/*"To Do"*/, iconButton, toDoLists, new Rect(Screen.width * 0.525f, Screen.height * 0.525f, Screen.width * 0.2f, Screen.height * 0.3f), CommonTasks.LoadLevel, "ToDoLists_Screen");
		taskListButton.name = "TaskListButton";
		
#if UNITY_ANDROID || UNITY_IPHONE
		// Make the calendar button.
		GameObject calendarButton = MakeIconButton (localCanvas, MCP.Text (1306)/*"Calendar"*/, iconButton, calendar, new Rect(Screen.width * 0.75f, Screen.height * 0.525f, Screen.width * 0.2f, Screen.height * 0.3f), CommonTasks.LoadLevel, "Calendar_Screen");
		calendarButton.name = "CalendarButton";
#endif
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update ()
	{
		// Debug test things.
		if(Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown (KeyCode.D))
		{
			List<string> categories = LitJson.JsonMapper.ToObject<List<string>>(MCP.userInfo.memberProfile.clipArtCategoriesOwnedJson);
			categories.Add ("Premium");
			MCP.userInfo.memberProfile.clipArtCategoriesOwnedJson = LitJson.JsonMapper.ToJson (categories);
			Debug.Log ("PremiumAdded");
		}
		if(Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown (KeyCode.U))
		{
			LoadMenu ("Upgrade_Screen");
		}
		
		// Check through the notifications for any that require feedback.
		if(!notificationsChecked && MCP.zingNotifications != null && MCP.zingNotifications.messages != null && !MCP.isTransmitting)
		{
			for(int i = 0; i < MCP.zingNotifications.messages.Count; i++)
			{
				// If the notification is a friend confirmation and is not already tripped...
				if(MCP.zingNotifications.messages[i].type == "4" && MCP.zingNotifications.messages[i].hasTripped == "0")
				{
					// Get friend's info, flag and add in update.
					StartCoroutine( MCP.GetFriendInfo(MCP.zingNotifications.messages[i].from));
					StartCoroutine (MCP.MarkNotificationAsTripped (MCP.zingNotifications.messages[i].id));
				}
			}
			
			notificationsChecked = true;
		}
		
		if(MCP.friendsListNeedsUpdating && !MCP.isTransmitting)
		{
			StartCoroutine( MCP.UpdateFriends());
		}
		
		// Store the pulled clip art asset bundle
		if(MCP.clipArtBundle == null && MCP.downloadedAssetBundle != null)
		{
			MCP.clipArtBundle = MCP.downloadedAssetBundle;
			MCP.downloadedAssetBundle = null;
			Debug.Log("ClipArtPulled: " + MCP.clipArtBundle.GetAllAssetNames().Length);
		}
	}
	
	public override void DoGUI (){}
}
