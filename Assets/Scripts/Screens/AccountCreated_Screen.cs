﻿using UnityEngine;
using System.Collections;

public class AccountCreated_Screen : MenuScreen
{
	GameObject localCanvas;
	
	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start()
	{
		Init();
		
		CreateGUI();
		
		localCanvas.transform.localScale = Vector2.zero;
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Make canvas.
		localCanvas = MakeCanvas ();
		
		// Make title
		MakeTitleBar (localCanvas, MCP.Text (401)/*"Account Created!"*/, "ScreenTitle");
		
		// Make background panel
		MakeWindowBackground (localCanvas, new Rect(Screen.width * 0.05f, Screen.height * 0.2f, Screen.width * 0.9f, Screen.height * 0.7f), window_back).name = "BackgroundPanel";
		
		// Make first paragraph label
		MakeLabel (localCanvas, MCP.Text (402)/*"You are one step away from creating your own goal maps!"*/, new Rect(Screen.width * 0.15f, Screen.height * 0.35f, Screen.width * 0.7f, Screen.height * 0.2f), TextAnchor.UpperCenter, "InnerLabel");
		
		// Make second paragraph label
		MakeLabel (localCanvas, MCP.Text (403) + MCP.createdUserEmail + MCP.Text (404)/*"You have been sent an email to *address* - click the link in the message to activate your account."*/, new Rect(Screen.width * 0.15f, Screen.height * 0.5f, Screen.width * 0.7f, Screen.height * 0.2f), TextAnchor.UpperCenter, "InnerLabel");
		
		// Make ok button
		MakeButton (localCanvas, MCP.Text (203)/*"OK"*/, buttonBackground, new Rect(Screen.width * 0.4f, Screen.height  * 0.75f, Screen.width * 0.2f, Screen.height * 0.1f), CommonTasks.LoadLevel, "Login_Screen", "OrangeText");
	}
	
	void OnDestroy()
	{
		MCP.createdUserEmail = "";
	}
	
	public override void DoGUI() {}
}
