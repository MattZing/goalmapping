﻿using UnityEngine;
using System.Collections;

public class LandingScreen : MenuScreen
{
	private GameObject localCanvas;
	
	private Sprite goalMapSprite;
	private Sprite successCentreSprite;

	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		// Initialise the base.
		Init ();
		
		// Load resources
		goalMapSprite = Resources.Load<Sprite>("GUI/goal_maps_icon");
		successCentreSprite = Resources.Load<Sprite>("GUI/success_centre_icon");
		
		// Create the scene GUI.
		CreateGUI ();
		
		localCanvas.transform.localScale = Vector2.zero;
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Find the current canvas.
		localCanvas = MakeCanvas ();

		GameObject titleBar = MakeTitleBar (localCanvas, MCP.Text (1101)/*"Landing Screen"*/);
		titleBar.name = "TitleBar";
		
		// Make the create map button.
		GameObject createMapButton = MakeIconButton (localCanvas, MCP.Text (1102)/*"Create Goal Map"*/, iconButton, goalMapSprite, new Rect(Screen.width * 0.15f, Screen.height * 0.3f, Screen.width * 0.3f, Screen.height * 0.5f), CommonTasks.LoadLevel, "Login_Screen");
		createMapButton.name = "CreateMapButton";
		
		// Make the success centre button.
		GameObject successCentreButton = MakeIconButton (localCanvas, MCP.Text (1103)/*"Success Centre"*/, iconButton, successCentreSprite, new Rect(Screen.width * 0.55f, Screen.height * 0.3f, Screen.width * 0.3f, Screen.height * 0.5f), CommonTasks.LoadLevel, "SuccessCentre_Screen");
		successCentreButton.name = "SuccessCentreButton";
	}
	
	public override void DoGUI() {}
}
