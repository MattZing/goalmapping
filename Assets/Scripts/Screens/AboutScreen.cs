using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class AboutScreen : MenuScreen
{
	public TextAsset AboutText;
	public TextAsset CreditsText;
	public TextAsset DisclaimerText;
	public TextAsset EULAtext;
	
	private GameObject localCanvas;
	private GameObject aboutTextObject;	
	private GameObject aboutScrollArea;
	
	private GUIStyle textStyle;
	private string aboutString;
	
	private enum InternalState
	{
		about,
		credits,
		disclaimer,
		viewEULA
	};
	private InternalState internalState;

	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		// Initialise the base.
		Init ();
		
		// Set default values.
		internalState = InternalState.about;
		textStyle = new GUIStyle();
		textStyle.alignment = TextAnchor.UpperLeft;
		textStyle.font = defaultFont;
		textStyle.fontSize = (int)_fontSize;
		textStyle.fontStyle = FontStyle.Normal;
		textStyle.wordWrap = true;
		
		// Create the scene GUI.
		CreateGUI ();
		
		localCanvas.transform.localScale = Vector2.zero;
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Find the current canvas.
		localCanvas = MakeCanvas ();
		
		GameObject windowBack = MakeWindowBackground (localCanvas, new Rect(Screen.width * 0.1f, Screen.height * 0.15f , Screen.width * 0.8f, Screen.height * 0.8f), window_back);
		windowBack.name = "WindowBackground";
		
		GameObject titleBar = MakeTitleBar (localCanvas, MCP.Text (301)/*"About"*/);
		titleBar.name = "TitleBar";
		
		MakeSideGizmo (localCanvas, true);
		
		// Make background for button area
		MakeImage (localCanvas, new Rect(Screen.width * 0.65f, Screen.height * 0.165f, Screen.width * 0.25f, Screen.height * 0.775f), menuBackground);
		
		// Make the about button.
		GameObject aboutButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.675f, Screen.height * 0.2f, Screen.width * 0.2f, Screen.height * 0.08f), buttonBackground, true);
		aboutButtonBackground.name = "AboutButtonBackground";
		
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			delegate {
				if(internalState != InternalState.about)
				{
					internalState = InternalState.about;
					SetText();
				}
			}
		};
		
		GameObject aboutButton = MakeButton (localCanvas, MCP.Text (301)/*"About"*/, new Rect(Screen.width * 0.675f, Screen.height * 0.2f, Screen.width * 0.2f, Screen.height * 0.08f), funcList, "InnerLabel");
		aboutButton.name = "AboutButton";
		aboutButton.GetComponent<Text>().color = bmOrange;
		aboutButton.GetComponent<Button>().image = aboutButtonBackground.GetComponent<Image>();
		
//		// Make the disclaimer button.
//		GameObject disclaimerButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.675f, Screen.height * 0.325f, Screen.width * 0.2f, Screen.height * 0.08f), buttonBackground, true);
//		disclaimerButtonBackground.name = "DisclaimerButtonBackground";
//		
//		funcList = new List<MyVoidFunc>
//		{
//			delegate {
//				if(internalState != InternalState.disclaimer)
//				{
//					internalState = InternalState.disclaimer;
//					SetText();
//				}
//			}
//		};
//		
//		GameObject disclaimerButton = MakeButton (localCanvas, MCP.Text (302)/*"Disclaimer"*/, new Rect(Screen.width * 0.675f, Screen.height * 0.325f, Screen.width * 0.2f, Screen.height * 0.08f), funcList, "InnerLabel");
//		disclaimerButton.name = "DisclaimerButton";
//		disclaimerButton.GetComponent<Text>().color = bmOrange;
//		disclaimerButton.GetComponent<Button>().image = disclaimerButtonBackground.GetComponent<Image>();
//		
//		// Make the EULA button.
//		GameObject eulaButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.675f, Screen.height * 0.45f, Screen.width * 0.2f, Screen.height * 0.08f), buttonBackground, true);
//		eulaButtonBackground.name = "eulaButtonBackground";
//		
//		funcList = new List<MyVoidFunc>
//		{
//			SetText,
//			delegate {
//				if(internalState != InternalState.viewEULA)
//				{
//					internalState = InternalState.viewEULA;
//					SetText();
//				}
//			}
//		};
//		GameObject eulaButton = MakeButton (localCanvas, MCP.Text (303)/*"EULA"*/, new Rect(Screen.width * 0.675f, Screen.height * 0.45f, Screen.width * 0.2f, Screen.height * 0.08f), funcList, "InnerLabel");
//		eulaButton.name = "EULAButton";
//		eulaButton.GetComponent<Text>().color = bmOrange;
//		eulaButton.GetComponent<Button>().image = eulaButtonBackground.GetComponent<Image>();
		
		// Make version number.
		GameObject versionNumberText = MakeLabel (localCanvas, MCP.Text (304)/*"Version: "*/ + MCP.Constants.version, new Rect(Screen.width * 0.7f, Screen.height * 0.85f, Screen.width * 0.2f, Screen.height * 0.08f), TextAnchor.MiddleLeft, "InnerLabel");
		versionNumberText.name = "VersionNumberText";
		
		// Make about text
		aboutTextObject = MakeLabel (localCanvas, "", new Rect(Screen.width * 0.13f, Screen.height * 0.18f, Screen.width * 0.49f, Screen.height * 0.7f));
		aboutTextObject.name = "AboutText";
		
		// Make a scroll rect for the help text
		aboutScrollArea = MakeScrollRect (localCanvas, new Rect(Screen.width * 0.12f, Screen.height * 0.135f, Screen.width * 0.5f, Screen.height * 0.75f), null);
		aboutScrollArea.name = "AboutScrollArea";
		aboutScrollArea.GetComponent<ScrollRect>().horizontal = false;
		
		aboutTextObject.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
		aboutTextObject.GetComponent<Text>().resizeTextForBestFit = false;
		aboutTextObject.GetComponent<Text>().fontSize = (int)_fontSize;
		aboutTextObject.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
		aboutTextObject.transform.SetParent (aboutScrollArea.transform.GetChild (0), true);
		aboutTextObject.transform.localScale = Vector3.one;
		aboutScrollArea.GetComponent<ScrollRect>().content = aboutTextObject.GetComponent<RectTransform>();
		
		// Set the initial text
		SetText ();
	}
	
	/// <summary>
	/// Sets the about text.
	/// </summary>
	void SetText()
	{
		// Get the text for this state.
		switch(internalState)
		{
		case InternalState.about:
			aboutString = AboutText.text;
			break;
		case InternalState.disclaimer:
			aboutString = DisclaimerText.text;
			break;
		case InternalState.viewEULA:
			aboutString = EULAtext.text;
			break;
		}
		
		// Get the height for the scroll rect.
		float aboutTextHeight = textStyle.CalcHeight (new GUIContent("\n" + aboutString), Screen.width * 0.49f);
		if(aboutTextHeight < Screen.height * 0.7f)
		{
			aboutTextHeight = Screen.height * 0.7f;
		}
		
		// Change the height of the scroll rect to contain all text.
		aboutTextObject.GetComponent<RectTransform>().sizeDelta = new Vector2(aboutTextObject.GetComponent<RectTransform>().sizeDelta.x, aboutTextHeight);
		
		// Reset the scroll position to the top.
		Vector3 newPos = aboutTextObject.GetComponent<RectTransform>().localPosition;
		newPos.y -= aboutTextHeight;
		aboutTextObject.GetComponent<RectTransform>().localPosition = newPos;
		
		// Add a space to the top of the string as a border for readability.
		if(MCP.siteURL.Contains ("192.168"))
		{
			aboutTextObject.GetComponent<Text>().text = "\nDevelopment Build\n";
		}
		else
		{
			aboutTextObject.GetComponent<Text>().text = "";
		}
		
		aboutTextObject.GetComponent<Text>().text += "\n" + aboutString;
		// Give the scroll rect velocity so the scroll bar updates and doesn't stay off screen.
		aboutScrollArea.GetComponent<ScrollRect>().velocity = new Vector2(0, 1000f);
	}
	
	public override void DoGUI (){}
}
