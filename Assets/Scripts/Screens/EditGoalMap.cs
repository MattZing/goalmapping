﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

//TODO: Make name editable.
public class EditGoalMap : MenuScreen
{
	private List<GameObject> editableLabels;
	private List<GameObject> editableImages;
	private GameObject localCanvas;
	private GameObject goalMapObject;
	private GameObject selectedImage;
	private GameObject loadingBackground;
	private GameObject loadingProgress;
	private GameObject loadingSpinner;
	private GameObject editCanvas;
	private GameObject stepByStepMapButton;
	private GameObject saveGoalMapButton;
	
	private Sprite[] guideSprite;
	private Sprite[] openSprite;
	private Sprite[] saveSprite;
	private Sprite defaultSprite;
	private Sprite spinnerSprite;
	
	public bool wantsEditting = false;
	private bool loading = false;
	private bool loaded = false;
	private bool saved = true;
	private bool fileBrowserOpen = false;
	private bool getGoalMaps = false;
	private MCP.GoalMap loadedGoalMap;
	
	private GameObject endDateTexButton;
	private GameObject endDatePicButton;
	private GameObject dateTimePopUpBG;
	private GameObject dateTimeCloseButtonBG;
	private GameObject dateTimeCloseButton;
	private bool showDateTimePopUp = false;

	private Texture2D plusTex;
	private Texture2D minusTex;
	private System.DateTime finishDate;

	private float middleX;
	private float canvasOffset;
	private float topY;
	private float outerWidth;
	private float outerHeight;
	private float innerWidth;
	private float innerHeight;
	private float paddingY;
	private float spinnerRot = 0;
	private float progress = 0;
	
#if !UNITY_WEBPLAYER
	private string loadedGoalMapFilePath;
#endif
	
	private const int numberOfCharsInInputs = 32;
	
	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		// Initialize the base.
		Init ();
		
		// Initialize arrays/lists.
		guideSprite = new Sprite[2];
		openSprite = new Sprite[2];
		saveSprite = new Sprite[2];
		editableLabels = new List<GameObject>();
		editableImages = new List<GameObject>();
		
		// Load resources
		guideSprite[0] = Resources.Load<Sprite>("GUI/guide_button");
		guideSprite[1] = Resources.Load<Sprite>("GUI/guide_button_active");
		openSprite[0] = Resources.Load<Sprite>("GUI/GoalMap/goalmap_openmap");
		openSprite[1] = Resources.Load<Sprite>("GUI/GoalMap/goalmap_openmap_active");
		saveSprite[0] = Resources.Load<Sprite>("GUI/Painter/paintbox_done");
		saveSprite[1] = Resources.Load<Sprite>("GUI/Painter/paintbox_done_active");
		defaultSprite = Resources.Load<Sprite>("GUI/bm_box_ltpurple");
		spinnerSprite = Resources.Load<Sprite> ("GUI/bm_rotate");
		plusTex = Resources.Load<Sprite>("GUI/GoalMap/goalmap_plus").texture;
		minusTex = Resources.Load<Sprite>("GUI/GoalMap/goalmap_minus").texture;
		
		// Create the GUI.
		CreateGUI();
		
		localCanvas.transform.localScale = Vector2.zero;

		middleX = (Screen.width * 0.475f);
		canvasOffset = localCanvas.GetComponent<RectTransform> ().position.x;
		topY = Screen.height * 0.35f;
		outerWidth = Screen.width * 0.25f;
		outerHeight = Screen.height * 0.25f;
		innerWidth = (outerWidth) * 0.9f;
		innerHeight = (outerHeight) * 0.85f;
		paddingY = ((outerHeight) - (innerHeight)) * 0.5f;
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Make the canvas.
		localCanvas = MakeCanvas (false);
		
		// Make canvas for asking if user wants to edit a picture.
		editCanvas = MakeCanvas (true);
		
		// Make the title bar.
		MakeTitleBar (localCanvas, "Edit Your Goal Maps");
		
		// Make the side gizmo.
		MakeSideGizmo (localCanvas, true);
		
		UnityEngine.UI.Navigation nav = new UnityEngine.UI.Navigation();
		nav.mode = UnityEngine.UI.Navigation.Mode.None;
		
		// Make the open goal map button.
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			OpenGuidedGoalMapScreen
		};
		stepByStepMapButton = MakeIconButton (localCanvas, MCP.Text (3401)/*"Guide"*/, null, guideSprite[0], new Rect(Screen.width * 0.85f, (Screen.height * 0.4f) - (Screen.width * 0.15f), Screen.width * 0.15f, Screen.width * 0.15f), funcList );
		stepByStepMapButton.name = "stepByStepMapButton";
		Destroy(stepByStepMapButton.GetComponent<Image>());
		// Make hover sprite
		stepByStepMapButton.GetComponent<Button>().navigation = nav;
		stepByStepMapButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		SpriteState spriteState = new SpriteState();
		stepByStepMapButton.GetComponent<Button>().targetGraphic = stepByStepMapButton.transform.GetChild (1).gameObject.GetComponent<Image>();
		spriteState.highlightedSprite = guideSprite[1];
		spriteState.pressedSprite = guideSprite[1];
		stepByStepMapButton.GetComponent<Button>().spriteState = spriteState;
		
		// Make the open goal map button.
		funcList = new List<MyVoidFunc>
		{
			OpenGoalMap
		};
		GameObject openGoalMapButton = MakeIconButton (localCanvas, MCP.Text (3101)/*"Open"*/, null, openSprite[0], new Rect(Screen.width * 0.85f, (Screen.height * 0.65f) - (Screen.width * 0.15f), Screen.width * 0.15f, Screen.width * 0.15f), funcList );
		openGoalMapButton.name = "OpenGoalMapButton";
		Destroy(openGoalMapButton.GetComponent<Image>());
		// Make hover sprite
		openGoalMapButton.GetComponent<Button>().navigation = nav;
		openGoalMapButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		openGoalMapButton.GetComponent<Button>().targetGraphic = openGoalMapButton.transform.GetChild (1).gameObject.GetComponent<Image>();
		spriteState.highlightedSprite = openSprite[1];
		spriteState.pressedSprite = openSprite[1];
		openGoalMapButton.GetComponent<Button>().spriteState = spriteState;
		
		// Make the save goal map button.
		funcList = new List<MyVoidFunc>
		{
			SaveGoalMap
		};
		saveGoalMapButton = MakeIconButton (localCanvas, MCP.Text (215)/*"Save"*/, null, saveSprite[0], new Rect(Screen.width * 0.85f, (Screen.height * 0.9f) - (Screen.width * 0.15f), Screen.width * 0.15f, Screen.width * 0.15f), funcList );
		saveGoalMapButton.name = "SaveGoalMapButton";
		Destroy(saveGoalMapButton.GetComponent<Image>());
		// Make hover sprite
		saveGoalMapButton.GetComponent<Button>().navigation = nav;
		saveGoalMapButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		saveGoalMapButton.GetComponent<Button>().targetGraphic = saveGoalMapButton.transform.GetChild (1).gameObject.GetComponent<Image>();
		spriteState.highlightedSprite = saveSprite[1];
		spriteState.pressedSprite = saveSprite[1];
		saveGoalMapButton.GetComponent<Button>().spriteState = spriteState;
		
		// Make the goal map prefab.
		goalMapObject = MakeGoalMap (localCanvas);
		
		// Modify the text object.
		goalMapObject.transform.GetChild (0).localScale = new Vector3(0.45f, 0.45f, 0.45f);
		goalMapObject.transform.GetChild (0).localPosition = new Vector3(Screen.width * 0.21f, Screen.height * 0.65f, 0);
		// Modify the image object.
		goalMapObject.transform.GetChild (1).localScale = new Vector3(0.45f, 0.45f, 0.45f);
		goalMapObject.transform.GetChild (1).localPosition = new Vector3(Screen.width * 0.63f, Screen.height * 0.65f, 0);

		// Make the background window.
		dateTimePopUpBG = MakeWindowBackground (localCanvas, new Rect (Screen.width * 0.3f, Screen.height * 0.3f, Screen.width * 0.4f, Screen.height * 0.4f), window_back); 
		dateTimePopUpBG.name = "dateTimePopUpBG";

		funcList = new List<MyVoidFunc> ()
		{
			EditStopDate
		};
		// Draw back button.
		dateTimeCloseButtonBG = MakeImage (localCanvas, new Rect( Screen.width * 0.4f, Screen.height * 0.6f, Screen.width * 0.2f, Screen.height * 0.075f ), buttonBackground, true);
		dateTimeCloseButtonBG.name = "DateTimeCloseButtonBG";

		dateTimeCloseButton = MakeButton (localCanvas, MCP.Text (2832)/*"Save"*/, new Rect( Screen.width * 0.4f, Screen.height * 0.6f, Screen.width * 0.2f, Screen.height * 0.075f ), funcList);
		dateTimeCloseButton.GetComponent<Button>().image = dateTimeCloseButtonBG.GetComponent<Image>();
		dateTimeCloseButton.GetComponent<Text> ().alignment = TextAnchor.MiddleCenter;
		dateTimeCloseButton.name = "DateTimeCloseButton";

		dateTimePopUpBG.SetActive (false);
		dateTimeCloseButton.SetActive (false);
		dateTimeCloseButtonBG.SetActive (false);

		// Make goal map editable
		MakeGoalMapElementsEditable();
		
		// Make the loading popup fade background.
		loadingBackground = MakeImage (localCanvas, new Rect(0, 0, Screen.width, Screen.height), menuBackground);
		loadingBackground.name = "LoadingBackground";
		
		// Make the loading popup background.
		GameObject loadingBackgroundPanel = MakeImage (localCanvas, new Rect(Screen.width * 0.25f, Screen.height * 0.3f, Screen.width * 0.5f, Screen.height * 0.45f), popup_back, true);
		loadingBackgroundPanel.name = "LoadingBackgroundPanel";
		loadingBackgroundPanel.transform.SetParent (loadingBackground.transform);
		
		// Make the loading text.
		GameObject loadingLabel = MakeLabel (localCanvas, MCP.Text (214)/*"Loading..."*/, new Rect(Screen.width * 0.35f, Screen.height * 0.2875f, Screen.width * 0.3f, Screen.height * 0.2f), TextAnchor.MiddleCenter, "InnerLabel");
		loadingLabel.name = "LoadingLabel";
		loadingLabel.transform.SetParent (loadingBackground.transform);
		
		loadingProgress = MakeLabel (localCanvas, "0%", new Rect(Screen.width * 0.35f, Screen.height * 0.4375f, Screen.width * 0.3f, Screen.height * 0.1f));
		loadingProgress.name = "loadingProgress";
		loadingProgress.transform.SetParent (loadingBackground.transform);
		
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				loading = false;
				MCP.pulledGoalMaps = false;
				loadedGoalMap = null;
				StopCoroutine (LoadingGoalMap());
				loadingBackground.SetActive (false);
			}
		};
		GameObject loadingCancelButton = MakeButton (localCanvas, MCP.Text (207)/*"Cancel"*/, buttonBackground, new Rect(Screen.width * 0.4f, Screen.height * 0.65f, Screen.width * 0.2f, Screen.height * 0.06f), funcList);
		loadingCancelButton.name = "LoadingCancelButton";
		loadingCancelButton.transform.SetParent (loadingBackground.transform, true);
		loadingCancelButton.transform.localScale = Vector3.one;
		
		// Make the loading spinner.
		loadingSpinner = MakeImage (localCanvas, new Rect(Screen.width * 0.5f, Screen.height * 0.45f, Screen.width * 0.08f, Screen.width * 0.08f), spinnerSprite, false);
		loadingSpinner.name = "LoadingSpinner";
		loadingSpinner.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
		loadingSpinner.transform.SetParent (loadingBackground.transform);
		
		loadingBackground.SetActive(false);
		
		// Edit picture background panel.
		GameObject editBackgroundPanel = MakeImage (editCanvas, new Rect(Screen.width * 0.2f, Screen.height * 0.2f, Screen.width * 0.6f, Screen.height * 0.6f), window_back, true);
		editBackgroundPanel.name = "EditBackgroundPanel";
		
		// Edit picture text.
		GameObject editTitleLabel = MakeLabel(editCanvas, MCP.Text (2834)/*"Would you like to edit this picture?"*/, new Rect(Screen.width * 0.35f, Screen.height * 0.35f, Screen.width * 0.3f, Screen.height * 0.15f), TextAnchor.UpperCenter);
		editTitleLabel.name = "EditTitleLabel";
		editTitleLabel.GetComponent<Text>().color = bmOrange;
		
		// Edit picture yes button.
		GameObject editYesButtonBackground = MakeImage (editCanvas, new Rect(Screen.width * 0.25f, Screen.height * 0.65f, Screen.width * 0.15f, Screen.height * 0.1f), buttonBackground, true);
		editYesButtonBackground.name = "EditYesButtonBackground";
		
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				MenuScreen.firstFrameTimer = 0;
				GameObject camObj = GameObject.FindGameObjectWithTag ("MainCamera");
				camObj.AddComponent<painter> ();
				GameObject.Find ("CanvasCamera").GetComponent<Camera>().enabled  = true;
				
				localCanvas.SetActive (true);
				editCanvas.SetActive (false);
				camObj.GetComponent<painter> ().edittingImage = true;
				camObj.GetComponent<painter> ().sceneCanvas = localCanvas;
				camObj.GetComponent<painter> ().editCanvas = editCanvas;
			}
		};
		GameObject editYesButton = MakeButton (editCanvas, MCP.Text (201)/*"Yes"*/, null, new Rect(Screen.width * 0.25f, Screen.height * 0.65f, Screen.width * 0.15f, Screen.height * 0.1f), funcList, "InnerLabel");
		editYesButton.name = "EditYesButton";
		editYesButton.transform.GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
		
		// Edit picture no button.
		GameObject editNoButtonBackground = MakeImage (editCanvas, new Rect(Screen.width * 0.6f, Screen.height * 0.65f, Screen.width * 0.15f, Screen.height * 0.1f), buttonBackground, true);
		editNoButtonBackground.name = "EditNoButtonBackground";
		
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				wantsEditting = false;
				localCanvas.SetActive (true);
				editCanvas.SetActive (false);
				
				PainterClosed ();
			}
		};
		GameObject editNoButton = MakeButton (editCanvas, MCP.Text (202)/*"No"*/, null, new Rect(Screen.width * 0.6f, Screen.height * 0.65f, Screen.width * 0.15f, Screen.height * 0.1f), funcList, "InnerLabel");
		editNoButton.name = "EditNoButton";
		editNoButton.transform.GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
		
		editCanvas.SetActive (false);
	}
	
	void MakeGoalMapElementsEditable()
	{
		// For each elemeent in gm text objects...
		for(int i = 0; i < goalMapObject.transform.GetChild (0).childCount; i++)
		{
			// For each step element...
			for(int j = 0; j < goalMapObject.transform.GetChild (0).GetChild (i).childCount; j++)
			{
				// If this is the label object, add the input field component to make it editable.
				GameObject labelObj = goalMapObject.transform.GetChild (0).GetChild (i).GetChild (j).gameObject;
				
				if(labelObj.name.Contains ("Label") && !labelObj.name.Contains ("Start") && !labelObj.name.Contains ("Stop") && !labelObj.name.Contains ("Timeline"))
				{
					labelObj.AddComponent<InputField>();
					labelObj.GetComponent<InputField>().text = labelObj.GetComponent<Text>().text;
					labelObj.GetComponent<InputField>().textComponent = labelObj.GetComponent<Text>();
					labelObj.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;
					labelObj.GetComponent<InputField>().characterLimit = numberOfCharsInInputs;
					labelObj.GetComponent<InputField>().onValueChange.AddListener (
						delegate
						{
							labelObj.GetComponent<InputField>().text = labelObj.GetComponent<InputField>().text.Replace ("\t", "");
						});
					
					editableLabels.Add (labelObj);
				}
			}
		}
		// For each elemeent in gm image objects...
		for(int i = 0; i < goalMapObject.transform.GetChild (1).childCount; i++)
		{
			// For each step element...
			for(int j = 0; j < goalMapObject.transform.GetChild (1).GetChild (i).childCount; j++)
			{
				// If this is the label object, add the input field component to make it editable.
				if(goalMapObject.transform.GetChild (1).GetChild (i).GetChild (j).gameObject.name.Contains ("Image"))
				{
					GameObject imageObj = goalMapObject.transform.GetChild (1).GetChild (i).GetChild (j).gameObject;
					
					imageObj.AddComponent<Button>();
					imageObj.GetComponent<Button>().image = imageObj.GetComponent<Image>();
					imageObj.GetComponent<Button>().onClick.AddListener(
						delegate
						{
							OpenImageEditor(imageObj);
						});
					
					editableImages.Add (imageObj);
				}
			}
		}

		
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			EditStopDate
		};
		
		endDateTexButton = MakeButton (localCanvas, "", new Rect (Screen.width * 0.2f, Screen.height * 0.48f, Screen.width * 0.1f, Screen.height * 0.075f), funcList, null);
		endDateTexButton.name = "Editable Text StopDate Button";
		
		endDatePicButton = MakeButton (localCanvas, "", new Rect (Screen.width * 0.6f, Screen.height * 0.48f, Screen.width * 0.1f, Screen.height * 0.085f), funcList, null);
		endDatePicButton.name = "Editable Pics StopDate Button";
		
		//TODO: TL (27/08/2015) - Would of been simpler to called the "FinishDate" label "FinishDateLabel" in MakeGoalMap() in MenuScreen, although I didn't know the knockon it might have.
		//		endDateLabel = goalMapObject.transform.GetChild (0).GetChild (5).FindChild ("FinishDate").gameObject;
		//		endDateLabel.AddComponent<InputField>();
		//		endDateLabel.GetComponent<InputField>().text = endDateLabel.GetComponent<Text>().text;
		//		endDateLabel.GetComponent<InputField>().textComponent = endDateLabel.GetComponent<Text>();
		//		endDateLabel.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;
		//		endDateLabel.GetComponent<InputField>().characterLimit = 10;
		//		endDateLabel.GetComponent<InputField>().onValueChange.AddListener (
		//			delegate
		//			{
		//				//endDateLabel.GetComponent<InputField>().text = endDateLabel.GetComponent<InputField>().text.Replace ("\t", "");
		//				//Regex.Replace (endDateLabel.GetComponent<InputField>().text, "[^0-9/:]", "");
		//				//EditStopDate(endDateLabel.GetComponent<InputField>().text);
		//			});
		
		editableImages.Add (endDateTexButton);
		editableImages.Add (endDatePicButton);
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update ()
	{
		if(loaded && saved)
		{
			loadingBackground.SetActive (false);
			
			// Enable the input fields for editting.
			foreach(GameObject g in editableLabels)
			{
				g.GetComponent<InputField>().interactable = true;
			}
			
			// Enable the picture editor button.
			foreach(GameObject g in editableImages)
			{
				g.GetComponent<Button>().interactable = true;
			}
		}
		else
		{
			spinnerRot -= Time.deltaTime * 75f;
			loadingSpinner.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, spinnerRot);
			loadingProgress.GetComponent<Text>().text = ((int)progress).ToString () + "%";
			
			// Disable the input field editting.
			foreach(GameObject g in editableLabels)
			{
				g.GetComponent<InputField>().interactable = false;
			}
			
			// Disable the picture editor button.
			foreach(GameObject g in editableImages)
			{
				g.GetComponent<Button>().interactable = false;
			}
		}
		
		if(!saved && !MCP.isTransmitting)
		{
			saved = true;
		}
		
		// Enable pictures.
		goalMapObject.transform.GetChild (1).gameObject.SetActive (true);
		
//#if UNITY_WEBPLAYER
		// If the goal maps have been pulled from the server...
		if(MCP.goalMaps != null && MCP.pulledGoalMaps && !fileBrowserOpen)
		{
			// If there is more than one goal map and one of them has been selected...
			if(MCP.goalMaps.Count > 0 && !loading && MCP.gmIndex != -1)
			{
				loadedGoalMap = MCP.goalMaps[MCP.gmIndex];
				
				// Populate the goal map object with the goal map.
				goalMapObject.GetComponent<PopGM_Prefab>().PopulateGoalMap(loadedGoalMap);
				
				loading = true;
			}
			else
			{
				if(!fileBrowserOpen)
				{
					loading = true;
				}
			}
			
		}
//#endif
		
		// If the goal mapping is loading, load the images from the server.
		if(loading && !fileBrowserOpen && loadedGoalMap != null)
		{
			bool emptyMap = true;
			
			if(loadedGoalMap.goals != null)
			{
				emptyMap = false;
				
				for(int j = 0; j < loadedGoalMap.goals.Count; j++)
				{
					if(loadedGoalMap.goals[j].imageRef != null)
					{
						if(loadedGoalMap.goals[j].image == null)
						{
							if(!MCP.isTransmitting)
							{
								if(MCP.pulledImages != null && MCP.pulledImages.Count > 0 && MCP.pulledImages[0] != null)
								{
									loadedGoalMap.goals[j].image = MCP.pulledImages[0];
									
									// Make sure no question marks are displayed.
									if(loadedGoalMap.goals[j].image.width < 16)
									{
										loadedGoalMap.goals[j].image = defaultSprite.texture;
									}
									
									MCP.pulledImages[0] = null;
									progress = j * 7;
								}
								else
								{
									MCP.pulledImages = new List<Texture2D>();
									
									StartCoroutine(MCP.GetImage (MCP.userInfo.username, loadedGoalMap.goals[j].imageRef));
								}
							}
						}
					}
				}
			}
			
			if(loadedGoalMap.whys != null)
			{	
				emptyMap = false;
				
				for(int j = 0; j < loadedGoalMap.whys.Count; j++)
				{
					if(loadedGoalMap.whys[j].imageRef != null)
					{
						if(!MCP.isTransmitting)
						{
							if(loadedGoalMap.whys[j].image == null)
							{
								if(MCP.pulledImages != null && MCP.pulledImages.Count > 0 && MCP.pulledImages[0] != null)
								{
									loadedGoalMap.whys[j].image = MCP.pulledImages[0];
									
									// Make sure no question marks are displayed.
									if(loadedGoalMap.whys[j].image.width < 16)
									{
										loadedGoalMap.whys[j].image = defaultSprite.texture;
									}
									
									MCP.pulledImages[0] = null;
									progress = 35 + (j * 7);
								}
								else
								{
									MCP.pulledImages = new List<Texture2D>();
									StartCoroutine(MCP.GetImage (MCP.userInfo.username, loadedGoalMap.whys[j].imageRef));
								}
							}
						}
					}
				}
			}
			
			if(loadedGoalMap.whos != null)
			{	
				emptyMap = false;
				
				for(int j = 0; j < loadedGoalMap.whos.Count; j++)
				{
					if(loadedGoalMap.whos[j].imageRef != null)
					{
						if(!MCP.isTransmitting)
						{
							if(loadedGoalMap.whos[j].image == null)
							{
								if(MCP.pulledImages != null && MCP.pulledImages.Count > 0 && MCP.pulledImages[0] != null)
								{
									loadedGoalMap.whos[j].image = MCP.pulledImages[0];
									
									// Make sure no question marks are displayed.
									if(loadedGoalMap.whos[j].image.width < 16)
									{
										loadedGoalMap.whos[j].image = defaultSprite.texture;
									}
									
									MCP.pulledImages[0] = null;
									progress = 56 + (j * 7);
								}
								else
								{
									MCP.pulledImages = new List<Texture2D>();
									StartCoroutine(MCP.GetImage (MCP.userInfo.username, loadedGoalMap.whos[j].imageRef));
								}
							}
						}
					}
				}
			}
			
			if(loadedGoalMap.hows != null)
			{	
				emptyMap = false;
				
				for(int j = 0; j < loadedGoalMap.hows.Count; j++)
				{
					if(loadedGoalMap.hows[j].imageRef != null)
					{
						if(!MCP.isTransmitting)
						{
							if(loadedGoalMap.hows[j].image == null)
							{
								if(MCP.pulledImages != null && MCP.pulledImages.Count > 0 && MCP.pulledImages[0] != null)
								{
									loadedGoalMap.hows[j].image = MCP.pulledImages[0];
									
									// Make sure no question marks are displayed.
									if(loadedGoalMap.hows[j].image.width < 16)
									{
										loadedGoalMap.hows[j].image = defaultSprite.texture;
									}
									
									MCP.pulledImages[0] = null;
									progress = 78 + (j * 7);
								}
								else
								{
									MCP.pulledImages = new List<Texture2D>();
									StartCoroutine(MCP.GetImage (MCP.userInfo.username, loadedGoalMap.hows[j].imageRef));
								}
							}
						}
					}
				}
				
				if(!MCP.isTransmitting)
				{
					loading = false;
					loaded = true;
					goalMapObject.GetComponent<PopGM_Prefab>().PopulateGoalMap(loadedGoalMap);
					finishDate = loadedGoalMap.finish;
					MCP.pulledGoalMaps = false;
				}
			}
			
			if(emptyMap)
			{
				if(!MCP.isTransmitting)
				{
					loading = false;
					loaded = true;
					goalMapObject.GetComponent<PopGM_Prefab>().PopulateGoalMap(loadedGoalMap);
					finishDate = loadedGoalMap.finish;
					MCP.pulledGoalMaps = false;
				}
			}
		}
		
		if(getGoalMaps && !MCP.isTransmitting)
		{
			if(MCP.userInfo != null)
			{
				StartCoroutine (MCP.GetGoalMap(MCP.userInfo.username));
			}
			
			getGoalMaps = false;
		}
		
		// Only activate the guide button if there is a goal map selected.
		if(loadedGoalMap == null)
		{
			stepByStepMapButton.GetComponent<Button>().interactable = false;
			saveGoalMapButton.GetComponent<Button>().interactable = false;
		}
		else
		{
			stepByStepMapButton.GetComponent<Button>().interactable = true;
			saveGoalMapButton.GetComponent<Button>().interactable = true;
		}
	}
	
	void EditStopDate()
	{
		showDateTimePopUp = !showDateTimePopUp;

		dateTimePopUpBG.SetActive (showDateTimePopUp);
		dateTimeCloseButton.SetActive (showDateTimePopUp);
		dateTimeCloseButtonBG.SetActive (showDateTimePopUp);

		if (!showDateTimePopUp) 
		{
			goalMapObject.transform.GetChild(0).GetChild(5).Find("FinishDate").GetComponent<Text>().text = finishDate.ToString( "d/M/yyyy" );
			goalMapObject.transform.GetChild(1).GetChild(5).Find("FinishDate").GetComponent<Text>().text = finishDate.ToString( "d/M/yyyy" );
			loadedGoalMap.finish = finishDate;
		}
	}

	void OpenGuidedGoalMapScreen()
	{
		LoadMenu("GuidedGoalMap_Screen");
		Debug.Log ( "Guided Goal Map has loaded map index ( " + MCP.gmIndex + " ) to edit" );
	}
		
		/// <summary>
	/// Opens a goal map.
	/// </summary>
	void OpenGoalMap()
	{
		// Reset the first frame timer.
		MenuScreen.firstFrameTimer = 0;
		fileBrowserOpen = true;
		
//		#if UNITY_WEBPLAYER
		// Add the simulated file browser script and set variables.
		Camera.main.gameObject.AddComponent<SimulateFileBrowser>().sceneCanvas = localCanvas;
//		#else
//		// Add the file browser and set variables.
//		Camera.main.gameObject.AddComponent<FileBrowser>().sceneCanvas = localCanvas;
//		Camera.main.gameObject.GetComponent<FileBrowser>().currentFilePath = Application.persistentDataPath + "/GoalMaps";
//		Camera.main.gameObject.GetComponent<FileBrowser>().upPathLimit = Application.persistentDataPath + "/GoalMaps";
//		Camera.main.gameObject.GetComponent<FileBrowser> ().allowedExtensions = new string[]{"txt"};
//		#endif
	}
	
	void FileBrowserClosed()
	{
		Debug.Log ("FileSelected");
		
		fileBrowserOpen = false;
		MenuScreen.firstFrameTimer = 0;
		
		if(MCP.gmIndex != -1)
		{
			// If editing an image...
			if(selectedImage != null)
			{
				PainterClosed();
			}
			// Otherwise, loading a goal map.
			else
			{
				StartCoroutine (LoadingGoalMap());
			}
		}
	}
	
	void PainterClosed()
	{
		if(!wantsEditting)
		{
			if(MCP.loadedTexture != null)
			{
				if(Camera.main.gameObject.GetComponent<painter>() != null)
				{
					if(!Camera.main.gameObject.GetComponent<painter>().gettingOverlay)
					{
						selectedImage.GetComponent<Image>().sprite = Sprite.Create(MCP.loadedTexture, new Rect(0, 0, MCP.loadedTexture.width, MCP.loadedTexture.height), new Vector2(0.5f, 0.5f));
						
						// Get a unique reference name for the image.
						string imageRef = "";
						
						if(MCP.userInfo != null)
						{
							imageRef = MCP.userInfo.username + System.DateTime.UtcNow.Ticks.ToString ();
						}
						
						if(selectedImage.name.Contains (":"))
						{
							selectedImage.name = selectedImage.name.Split(new char[]{':'})[0];
						}
						
						selectedImage.name += ":" + imageRef;
						
						if(MCP.userInfo != null)
						{
							// Post the image to the server.
							StartCoroutine(MCP.PostImage(MCP.userInfo.username, imageRef, MCP.loadedTexture.EncodeToPNG ()));
						}
						
						MCP.loadedTexture = null;
					}
				}
				else
				{
					selectedImage.GetComponent<Image>().sprite = Sprite.Create(MCP.loadedTexture, new Rect(0, 0, MCP.loadedTexture.width, MCP.loadedTexture.height), new Vector2(0.5f, 0.5f));
					
					// Get a unique reference name for the image.
					string imageRef = "";
					
					if(MCP.userInfo != null)
					{
						imageRef = MCP.userInfo.username + System.DateTime.UtcNow.Ticks.ToString ();
					}
					
					if(selectedImage.name.Contains (":"))
					{
						selectedImage.name = selectedImage.name.Split(new char[]{':'})[0];
					}
					
					selectedImage.name += ":" + imageRef;
					
					if(MCP.userInfo != null)
					{
						// Post the image to the server.
						StartCoroutine(MCP.PostImage(MCP.userInfo.username, imageRef, MCP.loadedTexture.EncodeToPNG ()));
					}
					
					MCP.loadedTexture = null;
				}
			}
			else
			{
//				selectedImage.GetComponent<Image>().sprite = defaultSprite;
			}
			
			if(Camera.main.gameObject.GetComponent<painter>() == null)
			{
				selectedImage = null;
			}
		}
		else if(MCP.loadedTexture != null)
		{
			localCanvas.SetActive (false);
			editCanvas.SetActive (true);
		}
	}
	
	/// <summary>
	/// Loadings the goal map.
	/// </summary>
	/// <returns>The goal map.</returns>
	private IEnumerator LoadingGoalMap()
	{
		loaded = false;
		loadingBackground.SetActive (true);
		
		MCP.GoalMap goalMap = new MCP.GoalMap();
		// Make sure the goal map is populated with the new information.
		goalMapObject.GetComponent<PopGM_Prefab>().picPrefab.subGoal4 = null;
		// Enable text.
		goalMapObject.transform.GetChild (0).gameObject.SetActive (true);
		// Enable pictures.
		goalMapObject.transform.GetChild (1).gameObject.SetActive (true);
		
		yield return null;
		
//#if UNITY_WEBPLAYER
		if(MCP.goalMaps != null && MCP.goalMaps.Count > 0)
		{
			if(MCP.gmIndex != -1)
			{
				goalMap = MCP.goalMaps[MCP.gmIndex];
			}
		}
//#else
//		if(MCP.loadedString != null && MCP.loadedString != "")
//		{
//			// Make the goal map json a goal map object.
//			goalMap = LitJson.JsonMapper.ToObject<MCP.GoalMap> (MCP.loadedString);
//		}
//#endif		
		
		// Load in the images from their byte data.
		if(goalMap.goals != null)
		{
			for(int i = 0; i < goalMap.goals.Count; i++)
			{
				if(goalMap.goals[i].imageBytes != null)
				{
					goalMap.goals[i].image = new Texture2D(512, 512);
					goalMap.goals[i].image.LoadImage (goalMap.goals[i].imageBytes);
				}
			}
		}
		if(goalMap.whys != null)
		{
			for(int i = 0; i < goalMap.whys.Count; i++)
			{
				if(goalMap.whys[i].imageBytes != null)
				{
					goalMap.whys[i].image = new Texture2D(512, 512);
					goalMap.whys[i].image.LoadImage (goalMap.whys[i].imageBytes);
				}
			}
		}
		if(goalMap.whos != null)
		{
			for(int i = 0; i < goalMap.whos.Count; i++)
			{
				if(goalMap.whos[i].imageBytes != null)
				{
					goalMap.whos[i].image = new Texture2D(512, 512);
					goalMap.whos[i].image.LoadImage (goalMap.whos[i].imageBytes);
				}
			}
		}
		if(goalMap.hows != null)
		{
			for(int i = 0; i < goalMap.hows.Count; i++)
			{
				if(goalMap.hows[i].imageBytes != null)
				{
					goalMap.hows[i].image = new Texture2D(512, 512);
					goalMap.hows[i].image.LoadImage (goalMap.hows[i].imageBytes);
				}
			}
		}
		
		// Populate the goal map object with the goal map.
		goalMapObject.GetComponent<PopGM_Prefab>().PopulateGoalMap(goalMap);
		
		loadedGoalMap = goalMap;
#if !UNITY_WEBPLAYER
//		loadedGoalMapFilePath = MCP.loadedFilePath;
#endif
		
		//		loaded = true;
	}
	
	void OpenImageEditor(GameObject imageObj)
	{
		selectedImage = imageObj;
		if((selectedImage.GetComponent<Image>().mainTexture as Texture2D).width > 8)
		{
			MCP.loadedTexture = selectedImage.GetComponent<Image>().mainTexture as Texture2D;
		}
		
		wantsEditting = true;
		Camera.main.gameObject.AddComponent<SelectImageFunction>();
	}
	
	void SaveGoalMapText(MCP.GoalMap gm)
	{
		// Get a reference to the goal parent.
		GameObject goalParent = goalMapObject.transform.GetChild (0).GetChild (4).gameObject;
		// Set goals
		for(int i = 0; i < 5; i++)
		{
			MCP.Goal g = new MCP.Goal();
			g.goal = goalParent.transform.Find ("GoalLabel" + (i + 1).ToString ()).GetComponent<Text>().text;
//			g.goal = goalParent.transform.GetChild ((i * 3) + 7).gameObject.GetComponent<Text>().text;
			
			gm.goals.Add (g);
		}
		// Get a reference to the goal parent.
		GameObject whyParent = goalMapObject.transform.GetChild (0).GetChild (3).gameObject;
		// Set goals
		for(int i = 0; i < 3; i++)
		{
			MCP.Why w = new MCP.Why();
			w.why = whyParent.transform.Find ("WhyLabel" + (i + 1).ToString ()).GetComponent<Text>().text;
//			w.why = whyParent.transform.GetChild ((i * 3) + 5).gameObject.GetComponent<Text>().text;
			
			gm.whys.Add (w);
		}
		// Get a reference to the goal parent.
		GameObject howParent = goalMapObject.transform.GetChild (0).GetChild (2).gameObject;
		// Set goals
		for(int i = 0; i < 3; i++)
		{
			MCP.How h = new MCP.How();
			h.how = howParent.transform.Find("HowLabel" + (i + 1).ToString ()).GetComponent<Text>().text;
//			h.how = howParent.transform.GetChild ((i * 3) + 5).gameObject.GetComponent<Text>().text;
			
			gm.hows.Add (h);
		}
		// Get a reference to the goal parent.
		GameObject whoParent = goalMapObject.transform.GetChild (0).GetChild (1).gameObject;
		// Set goals
		for(int i = 0; i < 3; i++)
		{
			MCP.Who w = new MCP.Who();
			w.who = whoParent.transform.Find("WhoLabel" + (i + 1).ToString ()).GetComponent<Text>().text;
//			w.who = whoParent.transform.GetChild ((i * 3) + 5).gameObject.GetComponent<Text>().text;
			
			gm.whos.Add (w);
		}
	}
	
	void SaveGoalMapImage(MCP.GoalMap gm)
	{
		// Get a reference to the goal parent.
		GameObject goalParent = goalMapObject.transform.GetChild (1).GetChild (4).gameObject;
		// Set goals
		for(int i = 0; i < 5; i++)
		{
			GameObject goalObj = goalParent.transform.GetChild (i).gameObject;
			
			// Set the image bytes.
			if(goalObj.GetComponent<Image>().sprite != defaultSprite && goalObj.GetComponent<Image>().sprite.texture.width > 8)
			{
				gm.goals[i].image = goalObj.GetComponent<Image>().sprite.texture;
				gm.goals[i].imageBytes = goalObj.GetComponent<Image>().sprite.texture.EncodeToPNG();
				
				// Set the image reference.
				if(goalObj.name.Contains (":"))
				{
					gm.goals[i].imageRef = goalObj.name.Split (new char[]{':'})[1];
				}
				else
				{
					gm.goals[i].imageRef = loadedGoalMap.goals[i].imageRef;
				}
			}
		}
		// Get a reference to the goal parent.
		GameObject whyParent = goalMapObject.transform.GetChild (1).GetChild (3).gameObject;
		// Set goals
		for(int i = 0; i < 3; i++)
		{
			GameObject whyObj = whyParent.transform.GetChild (i).gameObject;
			
			if(whyObj.GetComponent<Image>().sprite != defaultSprite && whyObj.GetComponent<Image>().sprite.texture.width > 8)
			{
				gm.whys[i].image = whyObj.GetComponent<Image>().sprite.texture;
				gm.whys[i].imageBytes = whyParent.transform.GetChild (i).gameObject.GetComponent<Image>().sprite.texture.EncodeToPNG();
			
				// Set the image reference.
				if(whyObj.name.Contains (":"))
				{
					gm.whys[i].imageRef = whyObj.name.Split (new char[]{':'})[1];
				}
				else
				{
					gm.whys[i].imageRef = loadedGoalMap.whys[i].imageRef;
				}
			}
		}
		// Get a reference to the goal parent.
		GameObject howParent = goalMapObject.transform.GetChild (1).GetChild (2).gameObject;
		// Set goals
		for(int i = 0; i < 3; i++)
		{
			GameObject howObj = howParent.transform.GetChild (i).gameObject;
			
			if(howObj.GetComponent<Image>().sprite != defaultSprite && howObj.GetComponent<Image>().sprite.texture.width > 8)
			{
				gm.hows[i].image = howObj.GetComponent<Image>().sprite.texture;
				gm.hows[i].imageBytes = howParent.transform.GetChild (i).gameObject.GetComponent<Image>().sprite.texture.EncodeToPNG();
			
				// Set the image reference.
				if(howObj.name.Contains (":"))
				{
					gm.hows[i].imageRef = howObj.name.Split (new char[]{':'})[1];
				}
				else
				{
					gm.hows[i].imageRef = loadedGoalMap.hows[i].imageRef;
				}
			}
		}
		// Get a reference to the goal parent.
		GameObject whoParent = goalMapObject.transform.GetChild (1).GetChild (1).gameObject;
		// Set goals
		for(int i = 0; i < 3; i++)
		{
			GameObject whoObj = whoParent.transform.GetChild (i).gameObject;
			
			if(whoObj.GetComponent<Image>().sprite != defaultSprite && whoObj.GetComponent<Image>().sprite.texture.width > 8)
			{
				gm.whos[i].image = whoObj.GetComponent<Image>().sprite.texture;
				gm.whos[i].imageBytes = whoParent.transform.GetChild (i).gameObject.GetComponent<Image>().sprite.texture.EncodeToPNG();
			
				// Set the image reference.
				if(whoObj.name.Contains (":"))
				{
					gm.whos[i].imageRef = whoObj.name.Split (new char[]{':'})[1];
				}
				else
				{
					gm.whos[i].imageRef = loadedGoalMap.whos[i].imageRef;
				}
			}
		}
	}
	
	MCP.GoalMap RemoveImagesFromGoalMap(MCP.GoalMap gm)
	{
		for(int i = 0; i < gm.goals.Count; i++)
		{
			gm.goals[i].image = null;
		}
		for(int i = 0; i < gm.whys.Count; i++)
		{
			gm.whys[i].image = null;
		}
		for(int i = 0; i < gm.hows.Count; i++)
		{
			gm.hows[i].image = null;
		}
		for(int i = 0; i < gm.whos.Count; i++)
		{
			gm.whos[i].image = null;
		}
		
		return gm;
	}
	
	MCP.GoalMap RemoveImagesBytesFromGoalMap (MCP.GoalMap gm)
	{
		for(int i = 0; i < gm.goals.Count; i++)
		{
			gm.goals[i].imageBytes = null;
		}
		for(int i = 0; i < gm.whys.Count; i++)
		{
			gm.whys[i].imageBytes = null;
		}
		for(int i = 0; i < gm.hows.Count; i++)
		{
			gm.hows[i].imageBytes = null;
		}
		for(int i = 0; i < gm.whos.Count; i++)
		{
			gm.whos[i].imageBytes = null;
		}
		
		return gm;
	}
	
	void SaveGoalMap()
	{
		if(loadedGoalMap == null)
		{
			return;
		}
		
		saved = false;
		
		// Reset loaded images before dealing with the goal map.
		if(MCP.pulledImages != null && MCP.pulledImages.Count > 0)
		{
			MCP.pulledImages[0] = null;
		}
		
		MCP.GoalMap goalMap = new MCP.GoalMap();
		goalMap.goals = new List<MCP.Goal>();
		goalMap.whys = new List<MCP.Why>();
		goalMap.hows = new List<MCP.How>();
		goalMap.whos = new List<MCP.Who>();
		goalMap.start = loadedGoalMap.start;
		goalMap.finish = loadedGoalMap.finish;
		goalMap.name = loadedGoalMap.name;
		goalMap.reference = loadedGoalMap.reference;
		
		// Set the text goal map strings to those set in the input fields.
		SaveGoalMapText(goalMap);
		
		// Set the image bytes and references to those set in the editable buttons.
		SaveGoalMapImage(goalMap);
		
		// Overwrite the local version of this goal map.
//		for (int i = 0; i < MCP.goalMaps.Count; i++)
//		{
//			if(MCP.goalMaps[i].reference == loadedGoalMap.reference)
//			{
//				MCP.goalMaps[i] = goalMap;
//			}
//		}
		
		goalMap = RemoveImagesFromGoalMap (goalMap);
		
//#if !UNITY_WEBPLAYER
//		// Overwrite the current local version of this goal map.
//		if(File.Exists (loadedGoalMapFilePath))
//		{
//			File.WriteAllText(loadedGoalMapFilePath, LitJson.JsonMapper.ToJson (goalMap));
//		}
//#endif	
		
		goalMap = RemoveImagesBytesFromGoalMap (goalMap);
        // Overwrite the server version of this goal map.

        if (MCP.userInfo != null)
        {
            StartCoroutine(MCP.UpdateGoalMap(goalMap));
        }
		
		// Make a popup to confirm save.
		MakePopup (localCanvas, defaultPopUpWindow, "Goal Map Saved", "", "Dismiss", CommonTasks.DestroyParent);
		
		getGoalMaps = true;
		loaded = false;
		loadingBackground.SetActive (true);
	}

	/// <summary>
	/// Gets the date and time.
	/// </summary>
	/// <returns>The date and time.</returns>
	/// <param name="area">Area.</param>
	/// <param name="date">Date.</param>
	private System.DateTime GetDateTime (Rect area, System.DateTime date, bool useButtons) // Draw a widget to get a datetime date
	{
		GUI.BeginGroup (area);
		
		Vector4 prevPadding = new Vector4();
		prevPadding.x = skin.GetStyle ("EmptyButton").padding.left;
		prevPadding.y = skin.GetStyle ("EmptyButton").padding.right;
		prevPadding.z = skin.GetStyle ("EmptyButton").padding.top;
		prevPadding.w = skin.GetStyle ("EmptyButton").padding.bottom;
		skin.GetStyle ("EmptyButton").padding.left = 8;
		skin.GetStyle ("EmptyButton").padding.right = 8;
		skin.GetStyle ("EmptyButton").padding.top = 8;
		skin.GetStyle ("EmptyButton").padding.bottom = 8;
		
		float ySpacing = area.height * 0.333f, xSpacing = area.width * 0.1666f;
		int day = date.Day, month = date.Month, year = date.Year, hour = date.Hour, minute = date.Minute, second = date.Second;
		
		TextAnchor ta = GUI.skin.label.alignment;
		GUI.skin.label.alignment = TextAnchor.MiddleCenter;
		
		if(useButtons)
		{
			// Draw day field
			//		if (DoRepeatButton (new Rect (0f, ySpacing * 2f, xSpacing, ySpacing), "-", "OrangeText"))
			if (DoRepeatButton (new Rect (0f, ySpacing * 2f, xSpacing, ySpacing), minusTex, "EmptyButton"))
			{
				// If in the current year and month, make sure the date does not go before today.
				if(year == DateTime.Now.Year && month == DateTime.Now.Month)
				{
					// Day minus
					if (--day < DateTime.Now.Day)
					{
						day = DaysInMonth (month);
					}
				}
				else
				{
					// Day minus
					if (--day < 1)
					{
						day = DaysInMonth (month);
					}
				}
			}
		}
		
		GUI.Label (new Rect (0f, ySpacing, xSpacing, ySpacing), day.ToString () + GetDayPostfix (day), "OrangeText"); // day label
		
		if(useButtons)
		{
			//		if (DoRepeatButton (new Rect (0f, 0f, xSpacing, ySpacing), "+", "OrangeText"))
			if (DoRepeatButton (new Rect (0f, 0f, xSpacing, ySpacing), plusTex, "EmptyButton"))
			{
				// If in the current year and month, make sure the date does not go before today.
				if(year == DateTime.Now.Year && month == DateTime.Now.Month)
				{
					// Day plus
					if (++day > DaysInMonth (month))
					{
						day = DateTime.Now.Day;
					}
				}
				else
				{
					// Day plus
					if (++day > DaysInMonth (month))
					{
						day = 1;
					}
				}
			}
			
			// Draw month field
			//		if (DoRepeatButton (new Rect (xSpacing, ySpacing * 2f, xSpacing, ySpacing), "-", "OrangeText"))
			if (DoRepeatButton (new Rect (xSpacing, ySpacing * 2f, xSpacing, ySpacing), minusTex, "EmptyButton"))
			{
				// If in the current year, make sure the date does not go before today.
				if(year == DateTime.Now.Year)
				{
					// Month minus
					if (--month < DateTime.Now.Month)
					{
						month = 12;
					}
					
					// Check day
					if(month == DateTime.Now.Month)
					{
						if(day < DateTime.Now.Day)
						{
							day = DateTime.Now.Day;
						}
					}
				}
				else
				{
					// Month minus
					if (--month < 1)
					{
						month = 12;
					}
				}
				
				if (day > DaysInMonth (month))
				{
					day = DaysInMonth (month);
				}
			}
		}
		
		if (month > 0 && month < 13)
		{
			GUI.Label (new Rect (xSpacing, ySpacing, xSpacing, ySpacing), months [month - 1], "OrangeText"); // month label
		}
		
		if(useButtons)
		{
			//		if (DoRepeatButton (new Rect (xSpacing, 0f, xSpacing, ySpacing), "+", "OrangeText"))
			if (DoRepeatButton (new Rect (xSpacing, 0f, xSpacing, ySpacing), plusTex, "EmptyButton"))
			{
				// If in the current year, make sure the date does not go before today.
				if(year == DateTime.Now.Year)
				{
					// Month plus
					if (++month > 12)
					{
						month = DateTime.Now.Month;
					}
					
					// Check day
					if(month == DateTime.Now.Month)
					{
						if(day < DateTime.Now.Day)
						{
							day = DateTime.Now.Day;
						}
					}
				}
				else
				{
					// Month plus
					if (++month > 12)
					{
						month = 1;
					}
				}
				
				if (day > DaysInMonth (month))
				{
					day = DaysInMonth (month);
				}
			}
			
			// Draw year field
			//		if (DoRepeatButton (new Rect (xSpacing * 2f, ySpacing * 2f, xSpacing, ySpacing), "-", "OrangeText"))
			if (DoRepeatButton (new Rect (xSpacing * 2f, ySpacing * 2f, xSpacing, ySpacing), minusTex, "EmptyButton"))
			{
				// Year minus
				if (--year < DateTime.Now.Year)
				{
					year = DateTime.Now.Year + 100;
				}
				
				// Check month
				if(year == DateTime.Now.Year)
				{
					if (month < DateTime.Now.Month)
					{
						month = DateTime.Now.Month;
					}
					
					// Check day
					if(month == DateTime.Now.Month)
					{
						if(day < DateTime.Now.Day)
						{
							day = DateTime.Now.Day;
						}
					}
					
					if (day > DaysInMonth (month))
					{
						day = DaysInMonth (month);
					}
				}
			}
		}
		
		GUI.Label (new Rect (xSpacing * 2f, ySpacing, xSpacing, ySpacing), year.ToString ("D2"), "OrangeText"); // year label
		
		if(useButtons)
		{
			//		if (DoRepeatButton (new Rect (xSpacing * 2f, 0f, xSpacing, ySpacing), "+", "OrangeText"))
			if (DoRepeatButton (new Rect (xSpacing * 2f, 0f, xSpacing, ySpacing), plusTex, "EmptyButton"))
			{
				// Year plus
				if (++year > DateTime.Now.Year + 100)
				{
					year = DateTime.Now.Year;
				}
				
				// Check month
				if(year == DateTime.Now.Year)
				{
					if (month < DateTime.Now.Month)
					{
						month = DateTime.Now.Month;
					}
					// Check day
					if(month == DateTime.Now.Month)
					{
						if(day < DateTime.Now.Day)
						{
							day = DateTime.Now.Day;
						}
					}
					
					if (day > DaysInMonth (month))
					{
						day = DaysInMonth (month);
					}
				}
			}
		}
		
		GUI.EndGroup ();
		
		skin.GetStyle ("EmptyButton").padding.left = (int)prevPadding.x;
		skin.GetStyle ("EmptyButton").padding.right = (int)prevPadding.y;
		skin.GetStyle ("EmptyButton").padding.top = (int)prevPadding.z;
		skin.GetStyle ("EmptyButton").padding.bottom = (int)prevPadding.w;
		
		GUI.skin.label.alignment = ta;
		return new System.DateTime (year, month, day, hour, minute, second);
	}

	private int DaysInMonth (int month)
	{
		// February
		if (month == 2)
		{
			return 28;
		}
		// April, June, September, November
		else if (month == 4 || month == 6 || month == 9 || month == 11)
		{
			return 30;
		}
		
		return 31;
	}

	/// <summary>
	/// Gets the day postfix.
	/// </summary>
	/// <returns>The day postfix.</returns>
	/// <param name="day">Day.</param>
	private string GetDayPostfix (int day)
	{
		if (day == 1)
		{
			return "st";
		}
		else if (day == 2)
		{
			return "nd";
		}
		else if (day == 3)
		{
			return "rd";
		}
		return "th";
	}
	
	private static string[] months =
	{
		"Jan",
		"Feb",
		"Mar",
		"Apr",
		"May",
		"Jun",
		"Jul",
		"Aug",
		"Sep",
		"Oct",
		"Nov",
		"Dec"
	};
	
	public override void DoGUI() 
	{
		if (showDateTimePopUp) 
		{
			finishDate = GetDateTime (new Rect (middleX - (Screen.width * 0.08f) + (canvasOffset * Screen.width * 0.05f), topY + (paddingY * 4.0f) - (Screen.height * 0.13f), innerWidth * 1.75f, innerHeight * 1.5f), finishDate, true);
		}
	}
}
