using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GoalMapsScreen : MenuScreen
{
	private GameObject localCanvas;
	private GameObject popupCanvas;
	
	private Sprite makeGoalMapsSprite;
	private Sprite viewGoalMapSprite;
	private Sprite editGoalMapSprite;
	
	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		// Initialise the base.
		Init ();
		
		// Load resources
		makeGoalMapsSprite = Resources.Load<Sprite>("GUI/goal_maps_icon");
		viewGoalMapSprite = Resources.Load<Sprite>("GUI/view_icon");
		editGoalMapSprite = Resources.Load<Sprite>("GUI/edit_goalmap");
		
		// Set default variables.
		MCP.gmIndex = -1;
		
		// Create the scene GUI.
		CreateGUI ();
		
		localCanvas.transform.localScale = Vector2.zero;
	}
	
	void CreateGUI()
	{
		// Find the current canvas.
		localCanvas = MakeCanvas ();

		// Make the background window.
		GameObject windowBack = MakeWindowBackground (localCanvas, new Rect(Screen.width * 0.01f, Screen.height * 0.01f , Screen.width * 0.98f, Screen.height * 0.98f), window_back);
		windowBack.name = "WindowBackground";

		// Make the title bar.
		MakeTitleBar (localCanvas, MCP.Text (1001)/*"My Goal Maps"*/);
		
		// Make the side gizmo.
		MakeSideGizmo (localCanvas, true);
		
		// Make the button to create a new goal map.
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			LoadCreateGoalMap
		};
		GameObject makeNewGMButton = MakeButton (localCanvas, "", window_back, new Rect(Screen.width * 0.075f, Screen.height * 0.3f, Screen.width * 0.25f, Screen.height * 0.5f), funcList);
		makeNewGMButton.name = "MakeNewGMButton";
		
		// Make the button to create a new goal map.
		GameObject MakeNewGMButtonImage = MakeButton (localCanvas, "", makeGoalMapsSprite, new Rect(Screen.width * 0.125f, Screen.height * 0.325f, Screen.width * 0.15f, Screen.width * 0.2f), funcList);
		MakeNewGMButtonImage.name = "MakeNewGMButtonImage";
		
		// Make the button to create a new goal map.
		GameObject makeNewGMButtonText = MakeButton (localCanvas, MCP.Text (1002)/*"Create New Goal Map"*/, null, new Rect(Screen.width * 0.075f, Screen.height * 0.68f, Screen.width * 0.25f, Screen.height * 0.1f), funcList);
		makeNewGMButtonText.name = "MakeNewGMButtonText";
		makeNewGMButtonText.transform.GetChild (0).gameObject.GetComponent<Text>().color = Color.white;
		makeNewGMButtonText.transform.GetChild (0).gameObject.GetComponent<Text>().alignment = TextAnchor.UpperCenter;
		
		// Make the button to edit goal maps.
		GameObject editGMButton = MakeButton (localCanvas, "", window_back, new Rect(Screen.width * 0.375f, Screen.height * 0.3f, Screen.width * 0.25f, Screen.height * 0.5f), CommonTasks.LoadLevel, "EditGoalMaps");
		editGMButton.name = "EditGMButton";
		
		// Make the button to edit goal maps.
		GameObject editGMButtonImage = MakeButton (localCanvas, "", editGoalMapSprite, new Rect(Screen.width * 0.425f, Screen.height * 0.35f, Screen.width * 0.15f, Screen.width * 0.175f), CommonTasks.LoadLevel, "EditGoalMaps");
		editGMButtonImage.name = "EditGMButtonImage";
		
		// Make the button to edit old goal maps.
		GameObject editGMButtonText = MakeButton (localCanvas, MCP.Text (1006)/*"Edit Goal Maps"*/, null, new Rect(Screen.width * 0.375f, Screen.height * 0.68f, Screen.width * 0.25f, Screen.height * 0.1f), CommonTasks.LoadLevel, "EditGoalMaps");
		editGMButtonText.name = "EditGMButtonText";
		editGMButtonText.transform.GetChild (0).gameObject.GetComponent<Text>().color = Color.white;
		editGMButtonText.transform.GetChild (0).gameObject.GetComponent<Text>().alignment = TextAnchor.UpperCenter;
		
		// Make the button to view goal maps.
		GameObject viewGMButton = MakeButton (localCanvas, "", window_back, new Rect(Screen.width * 0.675f, Screen.height * 0.3f, Screen.width * 0.25f, Screen.height * 0.5f), CommonTasks.LoadLevel, "ViewGoalMap_Screen");
		viewGMButton.name = "ViewGMButton";
		
		// Make the button to view goal maps.
		GameObject viewGMButtonImage = MakeButton (localCanvas, "", viewGoalMapSprite, new Rect(Screen.width * 0.725f, Screen.height * 0.325f, Screen.width * 0.15f, Screen.width * 0.2f), CommonTasks.LoadLevel, "ViewGoalMap_Screen");
		viewGMButtonImage.name = "ViewGMButtonImage";
		
		// Make the button to view old goal maps.
		GameObject viewGMButtonText = MakeButton (localCanvas, MCP.Text (1003)/*"View Goal Maps"*/, null, new Rect(Screen.width * 0.675f, Screen.height * 0.68f, Screen.width * 0.25f, Screen.height * 0.1f), CommonTasks.LoadLevel, "ViewGoalMap_Screen");
		viewGMButtonText.name = "ViewGMButtonText";
		viewGMButtonText.transform.GetChild (0).gameObject.GetComponent<Text>().color = Color.white;
		viewGMButtonText.transform.GetChild (0).gameObject.GetComponent<Text>().alignment = TextAnchor.UpperCenter;
		
		// Make the popup canvas.
		popupCanvas = MakeCanvas (true);
		
		// Make the upsell popup.
		GameObject popupObject = MakePopup (popupCanvas, new Rect(Screen.width * 0.25f, Screen.height * 0.25f, Screen.width * 0.5f, Screen.height * 0.5f), MCP.Text (1004)/*"Too Many Goal Maps"*/, MCP.Text (1005)/*"You do not currently have a high enough level to create any more Goal Maps. To get more Goal Maps, as well as other new content, continue to the website. Otherwise, click back to return."*/, MCP.Text (218)/*"Continue"*/, CommonTasks.LoadLevel, "Upgrade_Screen");
		popupObject.transform.GetChild (1).gameObject.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
		
		// Get a reference to the continue button.
		GameObject contButton = popupObject.transform.GetChild (2).gameObject;
		// Resize the continue button.
		contButton.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width * 0.15f, contButton.GetComponent<RectTransform>().sizeDelta.y);
		contButton.transform.GetChild (0).gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width * 0.15f, contButton.GetComponent<RectTransform>().sizeDelta.y);
		// Move the continue button.
		contButton.GetComponent<RectTransform>().localPosition = new Vector2(Screen.width * 0.5f * 0.6f, contButton.GetComponent<RectTransform>().localPosition.y);
		
		// Make close popup button.
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				popupCanvas.SetActive (false);
				localCanvas.SetActive (true);
			}
		};
		MakeButton(popupObject, MCP.Text (219)/*"Back"*/, buttonBackground, new Rect (Screen.width * 0.55f, Screen.height * 0.4f, Screen.width * 0.15f, Screen.height * 0.05f), funcList, "OrangeText");
		
		// Make a debug button to allow testing of the guided goal map.
		if(Debug.isDebugBuild || MCP.siteURL.Contains ("192.168.1.146"))
		{
			MakeButton(popupObject, "Debug", buttonBackground, new Rect (Screen.width * 0.875f, Screen.height * 0.55f, Screen.width * 0.1f, Screen.height * 0.05f), CommonTasks.LoadLevel, "GuidedGoalMap_Screen", "OrangeText");
		}
		
		popupCanvas.SetActive (false);
	}
	
	/// <summary>
	/// Checks if the user has run out of goal maps. If not, loads the guided goal map. Otherwise, pushes the upsell popup.
	/// </summary>
	private void LoadCreateGoalMap()
	{
		if(MCP.userInfo != null && MCP.userInfo.memberProfile != null)
		{
			// Set the max number of goal maps the user is allowed.
			int maxGoalMaps = 1;
			
			switch(MCP.userInfo.memberProfile.goalMapLevel)
			{
			case "1":			// Silver
				maxGoalMaps = 7;
				break;
				
			case "2":			// Gold
			case "3":			// Platinum
			case "4":			// Diamond
				maxGoalMaps = 30;
				break;
			}
			
			// If the user has not hit their maximum number of goal maps.
			if(int.Parse (MCP.userInfo.memberProfile.numberOfGoalMaps) < maxGoalMaps)
			{
				LoadMenu("GuidedGoalMap_Screen");
			}
			else
			{
				// Make up-sell popup.
				localCanvas.SetActive (false);
				popupCanvas.SetActive (true);
				MenuScreen.firstFrameTimer = 0;
			}
		}
		else
		{
			LoadMenu("GuidedGoalMap_Screen");
		}
	}
	
	public override void DoGUI (){}
}
