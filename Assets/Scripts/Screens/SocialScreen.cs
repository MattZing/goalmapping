using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SocialScreen : MenuScreen
{
	private GameObject localCanvas;
	
	private Sprite friendsIcon;
	private Sprite messagesIcon;
	private Sprite shareIcon;
//	private Sprite forumIcon;

	private bool notificationsChecked = false;
	
	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		// Initialise the base.
		Init ();
		
		// Load resources.
		friendsIcon = Resources.Load<Sprite> ("GUI/bm_myfriends");
		messagesIcon = Resources.Load<Sprite> ("GUI/bm_mymessages");
		shareIcon = Resources.Load<Sprite> ("GUI/bm-addphotos");
        //		forumIcon = Resources.Load<Sprite> ("GUI/bm_myprofile");

        if (MCP.userInfo != null)
        {
            StartCoroutine(MCP.GetNotifications());
        }
		
		// Create the scene GUI.
		CreateGUI ();
		
		localCanvas.transform.localScale = Vector2.zero;
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Find the current canvas.
		localCanvas = MakeCanvas ();
		
		// Make the background window.
		GameObject windowBack = MakeWindowBackground (localCanvas, new Rect(Screen.width * 0.01f, Screen.height * 0.01f , Screen.width * 0.98f, Screen.height * 0.98f), window_back);
		windowBack.name = "WindowBackground";

		// Make the title bar.
		GameObject titleBar = MakeTitleBar (localCanvas, MCP.Text (1801)/*"Messages & Friends"*/);
		titleBar.name = "TitleBar";
		
		// Make the side gizmo.
		MakeSideGizmo(localCanvas, true);

		// Make the friends button.
		GameObject friendsButton = MakeIconButton (localCanvas, MCP.Text (1802)/*"Friends"*/, iconButton, friendsIcon, new Rect(Screen.width * 0.15f, Screen.height * 0.3f, Screen.width * 0.2f, Screen.height * 0.3f), CommonTasks.LoadLevel, "Friends_Screen");
		friendsButton.name = "friendsButton";
		
		// Make the messages button.
		GameObject messagesButton = MakeIconButton (localCanvas, MCP.Text (1803)/*"Messages"*/, iconButton, messagesIcon, new Rect(Screen.width * 0.4f, Screen.height * 0.3f, Screen.width * 0.2f, Screen.height * 0.3f), CommonTasks.LoadLevel, "ClientMessages_screen");
		messagesButton.name = "MessagesButton";
		
		//TODO: Share screen button
		
		// Make the share button.
		GameObject shareButton = MakeIconButton (localCanvas, MCP.Text (1805)/*"Share"*/, iconButton, shareIcon, new Rect(Screen.width * 0.65f, Screen.height * 0.3f, Screen.width * 0.2f, Screen.height * 0.3f), CommonTasks.LoadLevel, "Share_Screen");
		shareButton.name = "ShareButton";
	}
	
	void Update()
	{
		// Check through the notifications for any that require feedback.
		if(!notificationsChecked && MCP.zingNotifications != null && MCP.zingNotifications.messages != null && !MCP.isTransmitting)
		{
			for(int i = 0; i < MCP.zingNotifications.messages.Count; i++)
			{
				// If the notification is a friend confirmation and is not already tripped...
				if(MCP.zingNotifications.messages[i].type == "4" && MCP.zingNotifications.messages[i].hasTripped == "0")
				{
					// Get friend's info, flag and add in update.
					StartCoroutine( MCP.GetFriendInfo(MCP.zingNotifications.messages[i].from));
					StartCoroutine (MCP.MarkNotificationAsTripped (MCP.zingNotifications.messages[i].id));
				}
			}
			
			notificationsChecked = true;
		}
		
		if(MCP.friendsListNeedsUpdating && !MCP.isTransmitting)
		{
			StartCoroutine( MCP.UpdateFriends());
		}
	}
	
	public override void DoGUI() {}
}
