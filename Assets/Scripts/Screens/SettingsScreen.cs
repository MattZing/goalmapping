using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

//TODO: Get list of background from inventory
//TODO: Write current track name on label
//TODO: Get list of music tracks from inventory.
//TODO: Apply background
//TODO: Apply music track

public class SettingsScreen : MenuScreen
{
	private List<GameObject> generalSettings;
	private List<GameObject> backgroundSettings;
	private List<GameObject> soundSettings;
	private List<GameObject> backgroundTrackSettings;
	private GameObject localCanvas;
	private GameObject fullScreenCheckbox;
	private GameObject soundVolumeUpButton;
	private GameObject soundVolumeDownButton;
	private GameObject soundVolumeSlider;
	private GameObject musicVolumeUpButton;
	private GameObject musicVolumeDownButton;
	private GameObject musicVolumeSlider;
	private GameObject soundEnabledCheckbox;
	private GameObject musicEnabledCheckbox;
	
	private Rect screenSize;
	private Rect leftPanelRect;
	private Rect rightPanelRect;
	private Rect topPanelRect;
	private Rect bottomPanelRect;
	private bool progressVisible;
	private bool courseVisible;
	private float logoutButtonScale;
	private float teamButtonsWidth;
	private float bottomPanelSpacer;
	private Texture backgroundImage;
	
	private bool tempStartFullScreen = false;
	
	private Sprite volumeDownSprite;
	private Sprite volumeUpSprite;
	
	private List<MCP.MyTheme> themeList;
					
	private enum InternalStates
	{
		main,
		sound,
		changeBackground,
		personal,
		chooseBackgroundTrack,
	};
	
	private InternalStates internalState;
	public bool musicEnabled;
	public float musicVolume = 1f;
	public bool soundEnabled;
	public float soundVolume = 1f;
	
	// Restore old settings when user doesn't apply settings.
	private float oldMusicVolume = 1f;
	private float oldSoundVolume = 1f;
	private bool oldMusicEnabled = false;
	private bool oldSoundEnabled = false;
	
	public List<Theme> themes;
	
	[System.Serializable]
	public class Theme
	{
		public string themeName;
		public Texture backgroundTexture;
	}
	
	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		// Initialize the base class.
		base.Init ();
		
		// Load resources.
		volumeDownSprite = Resources.Load<Sprite>("playericon_reverse");
		volumeUpSprite = Resources.Load<Sprite>("playericon_play");
		
		// Set initial variables.
		internalState = InternalStates.sound;
		
		musicEnabled = false;
		soundEnabled = true;

		musicVolume = 0f;
		soundVolume = 0.75f;
		
		if ( sndManager != null && MCP.userInfo != null )
		{
			musicEnabled = sndManager.GetBackgroundMusicEnabled();

			soundEnabled = sndManager.GetSoundEnabled();

			musicVolume = sndManager.GetBackgroundMusicVolume();
			soundVolume = sndManager.GetSoundVolume();
		}
		
		// Save old settings
		oldMusicVolume = musicVolume;
		oldSoundVolume = soundVolume;
		oldMusicEnabled = musicEnabled;
		oldSoundEnabled = soundEnabled;
		
#if UNITY_IPHONE
		tempStartFullScreen = false;
#else
		if ( PlayerPrefs.HasKey( "StartFullScreen" ))
		{
			if ( PlayerPrefs.GetInt( "StartFullScreen" ) == 1 )
			{
				tempStartFullScreen = true;
			}	
			else
			{
				tempStartFullScreen = false;
			}
		}
#endif
		
		// Initialize lists.
		generalSettings = new List<GameObject>();
		soundSettings = new List<GameObject>();
		backgroundSettings = new List<GameObject>();
		backgroundTrackSettings = new List<GameObject>();
		
		// Create the GUI.
		CreateGUI ();
		
		localCanvas.transform.localScale = Vector2.zero;
		
		// Show sound option objects.
		for(int i = 0; i < soundSettings.Count; i++)
		{
			soundSettings[i].SetActive (true);
		}
		// Hide all other objects.
		for(int i = 0; i < generalSettings.Count; i++)
		{
			generalSettings[i].SetActive (false);
		}
		for(int i = 0; i < backgroundSettings.Count; i++)
		{
			backgroundSettings[i].SetActive (false);
		}
		for(int i = 0; i < backgroundTrackSettings.Count; i++)
		{
			backgroundTrackSettings[i].SetActive (false);
		}
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	private void CreateGUI()
	{
		// Make canvas
		localCanvas = MakeCanvas ();
		
		// Make title bar
		MakeTitleBar (localCanvas, MCP.Text (1601)/*"Settings"*/).name = "SettingsTitle";
		
		// Make side gizmo
		MakeSideGizmo (localCanvas, true);
		
		// Make background panel
		MakeImage (localCanvas, new Rect(Screen.width * 0.1f, Screen.height * 0.12f, Screen.width * 0.8f, Screen.height * 0.86f), window_back, true).name = "BackgroundPanel";
		
		// Make the general options button.
		GameObject generalOptionsButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.69f, Screen.height * 0.2f, Screen.width * 0.2f, Screen.height * 0.08f), buttonBackground, true);
		generalOptionsButtonBackground.name = "GeneralOptionsButtonBackground";
		generalOptionsButtonBackground.SetActive (false);
		
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			delegate
			{
				internalState = InternalStates.main;
				
				// Show general option objects.
				for(int i = 0; i < generalSettings.Count; i++)
				{
					generalSettings[i].SetActive (true);
				}
				// Hide all other objects.
				for(int i = 0; i < backgroundSettings.Count; i++)
				{
					backgroundSettings[i].SetActive (false);
				}
				for(int i = 0; i < soundSettings.Count; i++)
				{
					soundSettings[i].SetActive (false);
				}
				for(int i = 0; i < backgroundTrackSettings.Count; i++)
				{
					backgroundTrackSettings[i].SetActive (false);
				}
			}
		};
		GameObject generalOptionsButton = MakeButton (localCanvas, MCP.Text (1602)/*"General"*/, null, new Rect(Screen.width * 0.69f, Screen.height * 0.2f, Screen.width * 0.2f, Screen.height * 0.08f), funcList, "InnerLabel");
		generalOptionsButton.name = "GeneralOptionsButton";
		generalOptionsButton.GetComponent<Button>().image = generalOptionsButtonBackground.GetComponent<Image>();
		generalOptionsButton.transform.GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
		generalOptionsButton.SetActive (false);
		
		// Make background options button
		GameObject backgroundOptionsButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.69f, Screen.height * 0.5f, Screen.width * 0.2f, Screen.height * 0.08f), buttonBackground, true);
		backgroundOptionsButtonBackground.name = "BackgroundOptionsButtonBackground";
		backgroundOptionsButtonBackground.SetActive (false);
		
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				internalState = InternalStates.changeBackground;
				
				// Show background option objects.
				for(int i = 0; i < backgroundSettings.Count; i++)
				{
					backgroundSettings[i].SetActive (true);
				}
				// Hide all other objects.
				for(int i = 0; i < generalSettings.Count; i++)
				{
					generalSettings[i].SetActive (false);
				}
				for(int i = 0; i < soundSettings.Count; i++)
				{
					soundSettings[i].SetActive (false);
				}
				for(int i = 0; i < backgroundTrackSettings.Count; i++)
				{
					backgroundTrackSettings[i].SetActive (false);
				}
			}
		};
		GameObject backgroundOptionsButton = MakeButton (localCanvas, MCP.Text (1603)/*"Background"*/, null, new Rect(Screen.width * 0.69f, Screen.height * 0.5f, Screen.width * 0.2f, Screen.height * 0.08f), funcList, "InnerLabel");
		backgroundOptionsButton.name = "BackgroundOptionsButton";
		backgroundOptionsButton.GetComponent<Button>().image = backgroundOptionsButtonBackground.GetComponent<Image>();
		backgroundOptionsButton.transform.GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
		backgroundOptionsButton.SetActive (false);
		
		// Make sound options button
		GameObject soundOptionsButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.69f, Screen.height * 0.35f, Screen.width * 0.2f, Screen.height * 0.08f), buttonBackground, true);
		soundOptionsButtonBackground.name = "SoundOptionsButtonBackground";
		soundOptionsButtonBackground.SetActive (false);
		
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				internalState = InternalStates.sound;
				
				// Show background option objects.
				for(int i = 0; i < soundSettings.Count; i++)
				{
					soundSettings[i].SetActive (true);
				}
				// Hide all other objects.
				for(int i = 0; i < generalSettings.Count; i++)
				{
					generalSettings[i].SetActive (false);
				}
				for(int i = 0; i < backgroundSettings.Count; i++)
				{
					backgroundSettings[i].SetActive (false);
				}
				for(int i = 0; i < backgroundTrackSettings.Count; i++)
				{
					backgroundTrackSettings[i].SetActive (false);
				}
			}
		};
		GameObject soundOptionsButton = MakeButton (localCanvas, MCP.Text (1604)/*"Sound"*/, null, new Rect(Screen.width * 0.69f, Screen.height * 0.35f, Screen.width * 0.2f, Screen.height * 0.08f), funcList, "InnerLabel");
		soundOptionsButton.name = "SoundOptionsButton";
		soundOptionsButton.GetComponent<Button>().image = soundOptionsButtonBackground.GetComponent<Image>();
		soundOptionsButton.transform.GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
		soundOptionsButton.SetActive (false);
		
		// Make apply button.
		GameObject applyButtonBackground = MakeImage(localCanvas, new Rect(Screen.width * 0.69f, Screen.height * 0.7f, Screen.width * 0.2f, Screen.height * 0.08f), buttonBackground, true);
		applyButtonBackground.name = "ApplyButtonBackground";
		
		funcList = new List<MyVoidFunc>
		{
			Apply
		};
		GameObject applyButton = MakeButton (localCanvas, MCP.Text (217)/*"Apply"*/, null, new Rect(Screen.width * 0.69f, Screen.height * 0.7f, Screen.width * 0.2f, Screen.height * 0.08f), funcList, "InnerLabel");
		applyButton.name = "ApplyButton";
		applyButton.GetComponent<Button>().image = applyButtonBackground.GetComponent<Image>();
		applyButton.transform.GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
		
		// Make cancel button.
		GameObject cancelButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.69f, Screen.height * 0.85f, Screen.width * 0.2f, Screen.height * 0.08f), buttonBackground, true);
		cancelButtonBackground.name = "CancelButtonBackground";
		
		funcList = new List<MyVoidFunc>
		{
			Back
		};
		GameObject cancelButton = MakeButton (localCanvas, MCP.Text (206)/*"Close"*/, null, new Rect(Screen.width * 0.69f, Screen.height * 0.85f, Screen.width * 0.2f, Screen.height * 0.08f), funcList, "InnerLabel");
		cancelButton.name = "CancelButton";
		cancelButton.GetComponent<Button>().image = cancelButtonBackground.GetComponent<Image>();
		cancelButton.transform.GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
		
		// ********* General options *******
		GameObject generalOptionsBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.12f, Screen.height * 0.14f, Screen.width * 0.56f, Screen.height * 0.82f), menuBackground, true);
		generalOptionsBackground.name = "GeneralOptionsBackground";
		generalSettings.Add (generalOptionsBackground);
		
		// Make area title label
		GameObject generalOptionsTitle = MakeLabel (localCanvas, MCP.Text (1605)/*"   General Options"*/, new Rect(Screen.width * 0.12f, Screen.height * 0.16f, Screen.width * 0.56f, Screen.height * 0.1f), TextAnchor.MiddleLeft);
		generalOptionsTitle.name = "GeneralOptionsTitle";
		generalOptionsTitle.GetComponent<Text>().fontStyle = FontStyle.Bold;
		generalSettings.Add (generalOptionsTitle);
		
		// Make full screen checkbox
		fullScreenCheckbox = MakeCheckbox (localCanvas, new Rect(Screen.width * 0.16f, Screen.height * 0.35f, Screen.width * 0.25f, Screen.height * 0.06f), MCP.Text (1612)/*"Start Full-Screen"*/);
		fullScreenCheckbox.name = "FullScreenCheckbox";
		fullScreenCheckbox.GetComponent<Toggle>().isOn = tempStartFullScreen;
		generalSettings.Add (fullScreenCheckbox);
		
		// ********* Background options *******
		// Make area title background
		GameObject backgroundOptionsTitleBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.12f, Screen.height * 0.15f, Screen.width * 0.56f, Screen.height * 0.1f), menuBackground, true);
		backgroundOptionsTitleBackground.name = "BackgroundOptionsTitleBackground";
		backgroundSettings.Add (backgroundOptionsTitleBackground);
		
		// Make area title label
		GameObject backgroundOptionsTitle = MakeLabel (localCanvas, MCP.Text (1606)/*"   Background Options"*/, new Rect(Screen.width * 0.12f, Screen.height * 0.15f, Screen.width * 0.56f, Screen.height * 0.1f), TextAnchor.MiddleLeft);
		backgroundOptionsTitle.name = "BackgroundOptionsTitle";
		backgroundSettings.Add (backgroundOptionsTitle);
		
		// Make scroll area for backgrounds
		GameObject backgroundOptionsScrollArea = MakeScrollRect (localCanvas, new Rect(Screen.width * 0.12f, Screen.height * 0.22f, Screen.width * 0.53f, Screen.height * 0.7f), null);
		backgroundOptionsScrollArea.name = "BackgroundOptionsScrollArea";
		backgroundSettings.Add (backgroundOptionsScrollArea);
		
		// ********* Sound options *******
		GameObject soundOptionsBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.12f, Screen.height * 0.14f, Screen.width * 0.56f, Screen.height * 0.82f), menuBackground, true);
		soundOptionsBackground.name = "SoundOptionsBackground";
		soundSettings.Add (soundOptionsBackground);
		
		// Make area title label
		GameObject soundOptionsTitle = MakeLabel (localCanvas, MCP.Text (1607)/*"   Sound Options"*/, new Rect(Screen.width * 0.12f, Screen.height * 0.16f, Screen.width * 0.56f, Screen.height * 0.1f), TextAnchor.MiddleLeft);
		soundOptionsTitle.name = "SoundOptionsTitle";
		soundOptionsTitle.GetComponent<Text>().fontStyle = FontStyle.Bold;
		soundSettings.Add (soundOptionsTitle);
		
		// Make sounds enabled checkbox
		soundEnabledCheckbox = MakeCheckbox (localCanvas, new Rect(Screen.width * 0.16f, Screen.height * 0.35f, Screen.width * 0.5f, Screen.height * 0.06f), MCP.Text (1608)/*"Sound Enabled"*/);
		soundEnabledCheckbox.name = "SoundEnabledCheckbox";
		soundEnabledCheckbox.GetComponent<Toggle>().isOn = soundEnabled;
		soundEnabledCheckbox.GetComponent<Toggle>().onValueChanged.AddListener(
			delegate
			{
				sndManager.SetSoundEnabled(!soundEnabled);
			}
		);
		soundSettings.Add(soundEnabledCheckbox);
		
		// Make sound volume down button
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				soundVolume -= 0.1f;
				
				if(soundVolume < 0)
				{
					soundVolume = 0;
				}
				
				soundVolumeSlider.GetComponent<Slider>().value = soundVolume * 100f;
			}
		};
		soundVolumeDownButton = MakeButton (localCanvas, "", volumeDownSprite, new Rect(Screen.width * 0.15f, Screen.height * 0.46f, Screen.width * 0.08f, Screen.height * 0.08f), funcList);
		soundVolumeDownButton.name = "SoundVolumeDownButton";
		soundSettings.Add (soundVolumeDownButton);
		
		// Make sound volume slider
		soundVolumeSlider = MakeSliderHorizontal(localCanvas, new Rect(Screen.width * 0.275f, Screen.height * 0.03f, Screen.width * 0.26f, Screen.height * 0.06f));
		soundVolumeSlider.name = "SoundVolumeSlider";
		soundVolumeSlider.GetComponent<Slider>().direction = Slider.Direction.LeftToRight;
		soundVolumeSlider.GetComponent<Slider>().maxValue = 100f;
		soundVolumeSlider.GetComponent<Slider>().minValue = 0f;
		soundVolumeSlider.GetComponent<Slider>().value = soundVolume * 100f;
		soundVolumeSlider.GetComponent<Slider>().wholeNumbers = true;
		soundSettings.Add (soundVolumeSlider);
		
		// Make sound volume up button
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				soundVolume += 0.1f;
				
				if(soundVolume > 1f)
				{
					soundVolume = 1f;
				}
				
				soundVolumeSlider.GetComponent<Slider>().value = soundVolume * 100f;
			}
		};
		soundVolumeUpButton = MakeButton (localCanvas, "", volumeUpSprite, new Rect(Screen.width * 0.575f, Screen.height * 0.46f, Screen.width * 0.08f, Screen.height * 0.08f), funcList);
		soundVolumeUpButton.name = "SoundVolumeUpButton";
		soundSettings.Add (soundVolumeUpButton);
		
		// Make music enabled checkbox
		musicEnabledCheckbox = MakeCheckbox (localCanvas, new Rect(Screen.width * 0.16f, Screen.height * 0.6f, Screen.width * 0.5f, Screen.height * 0.06f), MCP.Text (1609)/*"Music Enabled"*/);
		musicEnabledCheckbox.name = "MusicEnabledCheckbox";
		musicEnabledCheckbox.GetComponent<Toggle>().isOn = musicEnabled;
		musicEnabledCheckbox.GetComponent<Toggle>().onValueChanged.AddListener(
			delegate
			{
				sndManager.SetBackgroundMusicEnabled(!musicEnabled);
			}
		);
		soundSettings.Add(musicEnabledCheckbox);
		
		// Make music volume down button
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				musicVolume -= 0.1f;
				
				if(musicVolume < 0f)
				{
					musicVolume = 0f;
				}
				
				musicVolumeSlider.GetComponent<Slider>().value = musicVolume * 100f;
			}
		};
		musicVolumeDownButton = MakeButton (localCanvas, "", volumeDownSprite, new Rect(Screen.width * 0.15f, Screen.height * 0.72f, Screen.width * 0.08f, Screen.height * 0.08f), funcList);
		musicVolumeDownButton.name = "MusicVolumeDownButton";
		soundSettings.Add (musicVolumeDownButton);
		
		// Make music volume slider
		musicVolumeSlider = MakeSliderHorizontal(localCanvas, new Rect(Screen.width * 0.275f, Screen.height * 0.29f, Screen.width * 0.26f, Screen.height * 0.06f));
		musicVolumeSlider.name = "MusicVolumeSlider";
		musicVolumeSlider.GetComponent<Slider>().direction = Slider.Direction.LeftToRight;
		musicVolumeSlider.GetComponent<Slider>().maxValue = 100f;
		musicVolumeSlider.GetComponent<Slider>().minValue = 0f;
		musicVolumeSlider.GetComponent<Slider>().value = musicVolume * 100f;
		musicVolumeSlider.GetComponent<Slider>().wholeNumbers = true;
		soundSettings.Add (musicVolumeSlider);
		
		// Make music volume up button
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				musicVolume += 0.1f;
				
				if(musicVolume > 1f)
				{
					musicVolume = 1f;
				}
				
				musicVolumeSlider.GetComponent<Slider>().value = musicVolume * 100f;
			}
		};
		musicVolumeUpButton = MakeButton (localCanvas, "", volumeUpSprite, new Rect(Screen.width * 0.575f, Screen.height * 0.72f, Screen.width * 0.08f, Screen.height * 0.08f), funcList);
		musicVolumeUpButton.name = "MusicVolumeUpButton";
		soundSettings.Add (musicVolumeUpButton);
		
		// Make current track label
		GameObject currentTrackLabel = MakeLabel(localCanvas, MCP.Text (1610)/*"Current Track: "*/, new Rect(Screen.width * 0.14f, Screen.height * 0.78f, Screen.width * 0.5f, Screen.height * 0.06f));
		currentTrackLabel.name = "CurrentTrackLabel";
		currentTrackLabel.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
		//soundSettings.Add (currentTrackLabel);
		currentTrackLabel.SetActive (false);
		
		GameObject chooseTrackButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.16f, Screen.height * 0.85f, Screen.width * 0.2f, Screen.height * 0.1f), buttonBackground, true);
		chooseTrackButtonBackground.name = "ChooseTrackButtonBackground";
		//soundSettings.Add(chooseTrackButtonBackground);
		chooseTrackButtonBackground.SetActive (false);
		// Make choose track button
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				internalState = InternalStates.chooseBackgroundTrack;
				
				// Show background option objects.
				for(int i = 0; i < backgroundTrackSettings.Count; i++)
				{
					backgroundTrackSettings[i].SetActive (true);
				}
				// Hide all other objects.
				for(int i = 0; i < generalSettings.Count; i++)
				{
					generalSettings[i].SetActive (false);
				}
				for(int i = 0; i < backgroundSettings.Count; i++)
				{
					backgroundSettings[i].SetActive (false);
				}
				for(int i = 0; i < soundSettings.Count; i++)
				{
					soundSettings[i].SetActive (false);
				}
			}
		};
		GameObject chooseTrackButton = MakeButton (localCanvas, MCP.Text (1611)/*"Choose Track"*/ + "...", null, new Rect(Screen.width * 0.16f, Screen.height * 1.85f, Screen.width * 0.2f, Screen.height * 0.1f), funcList, "InnerLabel");
		chooseTrackButton.name = "ChooseTrackButton";
		chooseTrackButton.GetComponent<Button>().image = chooseTrackButtonBackground.GetComponent<Image>();
		chooseTrackButton.transform.GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
		//soundSettings.Add (chooseTrackButton);
		chooseTrackButton.SetActive (false);
		
		// ********* Background track selection *******
		// Make area title background
		GameObject backgroundTrackOptionsTitleBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.12f, Screen.height * 0.15f, Screen.width * 0.56f, Screen.height * 0.1f), menuBackground, true);
		backgroundTrackOptionsTitleBackground.name = "BackgroundTrackOptionsTitleBackground";
		backgroundTrackSettings.Add (backgroundTrackOptionsTitleBackground);
		
		// Make area title label
		GameObject backgroundTrackOptionsTitle = MakeLabel (localCanvas, "   " + MCP.Text (1611)/*"Choose Track"*/, new Rect(Screen.width * 0.12f, Screen.height * 1.15f, Screen.width * 0.56f, Screen.height * 0.1f), TextAnchor.MiddleLeft);
		backgroundTrackOptionsTitle.name = "BackgroundTrackOptionsTitle";
		backgroundTrackSettings.Add (backgroundTrackOptionsTitle);
		
		// Make scroll area for music tracks.
		GameObject backgroundTrackScrollArea = MakeScrollRect (localCanvas, new Rect(Screen.width * 0.12f, Screen.height * 0.22f, Screen.width * 0.53f, Screen.height * 0.68f), null);
		backgroundTrackScrollArea.name = "BackgroundTrackScrollArea";
		backgroundTrackSettings.Add (backgroundTrackScrollArea);
		
		// Hide objects not on the general options page.
		for(int i = 0; i < backgroundSettings.Count; i++)
		{
			backgroundSettings[i].SetActive (false);
		}
		for(int i = 0; i < soundSettings.Count; i++)
		{
			soundSettings[i].SetActive (false);
		}
		for(int i = 0; i < backgroundTrackSettings.Count; i++)
		{
			backgroundTrackSettings[i].SetActive (false);
		}
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update ()
	{
		// Make sure the GUI has been created.
		if(localCanvas != null)
		{
			// Update volume sliders
			soundVolume = soundVolumeSlider.GetComponent<Slider>().value / 100f;
			sndManager.SetSFXVolume(soundVolume);
			musicVolume = musicVolumeSlider.GetComponent<Slider>().value / 100f;
			sndManager.SetBackgroundMusicVolume(musicVolume);
			
			soundEnabled = soundEnabledCheckbox.GetComponent<Toggle>().isOn;
			musicEnabled = musicEnabledCheckbox.GetComponent<Toggle>().isOn;
					
			// Enable/Disable volume slider depending on checkboxes.
			soundVolumeSlider.GetComponent<Slider>().interactable = soundEnabled;
			soundVolumeUpButton.GetComponent<Button>().interactable = soundEnabled;
			soundVolumeDownButton.GetComponent<Button>().interactable = soundEnabled;
			
			musicVolumeSlider.GetComponent<Slider>().interactable = musicEnabled;
			musicVolumeUpButton.GetComponent<Button>().interactable = musicEnabled;
			musicVolumeDownButton.GetComponent<Button>().interactable = musicEnabled;
			
			// If the slider is not enabled, fade the scroll handle.
			if(soundEnabled)
			{
				soundVolumeSlider.transform.GetChild (2).gameObject.GetComponent<Image>().color = soundVolumeSlider.GetComponent<Slider>().colors.normalColor;
			}
			else
			{
				float multiplier = soundVolumeSlider.GetComponent<Slider>().colors.disabledColor.a;
				Color c = new Color(multiplier, multiplier, multiplier, 1);
				soundVolumeSlider.transform.GetChild (2).gameObject.GetComponent<Image>().color = c;
			}
			if(musicEnabled)
			{
				musicVolumeSlider.transform.GetChild (2).gameObject.GetComponent<Image>().color = musicVolumeSlider.GetComponent<Slider>().colors.normalColor;
			}
			else
			{
				float multiplier = musicVolumeSlider.GetComponent<Slider>().colors.disabledColor.a;
				Color c = new Color(multiplier, multiplier, multiplier, 1);
				musicVolumeSlider.transform.GetChild (2).gameObject.GetComponent<Image>().color = c;
			}
		}
	
		// Update the base screen.
		base.UpdateScreen ();
	}
	
	/// <summary>
	/// Overrides the base Back().
	/// </summary>
	public override void Back()
	{
		// Revert settings
		musicVolume = oldMusicVolume;
		soundVolume = oldSoundVolume;
		musicEnabled = oldMusicEnabled;
		soundEnabled = oldSoundEnabled;
		
		// If in the music selection screen...
		if ( internalState == InternalStates.chooseBackgroundTrack )
		{
			internalState = InternalStates.sound;
			
			// Show background option objects.
			for(int i = 0; i < soundSettings.Count; i++)
			{
				soundSettings[i].SetActive (true);
			}
			// Hide all other objects.
			for(int i = 0; i < generalSettings.Count; i++)
			{
				generalSettings[i].SetActive (false);
			}
			for(int i = 0; i < backgroundSettings.Count; i++)
			{
				backgroundSettings[i].SetActive (false);
			}
			for(int i = 0; i < backgroundTrackSettings.Count; i++)
			{
				backgroundTrackSettings[i].SetActive (false);
			}
		}
		// Otherwise...
		else
		{
			// Apply settings
			// Set sound.
			sndManager.SetBackgroundMusicVolume(oldMusicVolume);
			sndManager.SetBackgroundMusicEnabled (oldMusicEnabled);
			sndManager.SetSFXVolume(oldSoundVolume);
			sndManager.SetSoundEnabled (oldSoundEnabled);
			
			// Use the base back function.
			base.Back();
		}
	}
	
	/// <summary>
	/// Apply the settings.
	/// </summary>
	private void Apply()
	{
		// Save start fullscreen flag
		if(fullScreenCheckbox.GetComponent<Toggle>().isOn)
		{
			PlayerPrefs.SetInt( "StartFullScreen", 1 );
		}
		else
		{
			PlayerPrefs.SetInt( "StartFullScreen", 0 );
		}
		
		// Save new settings to the 'old' variables to prevent overwriting.
		oldMusicVolume = musicVolume;
		oldSoundVolume = soundVolume;
		oldMusicEnabled = musicEnabled;
		oldSoundEnabled = soundEnabled;
		
		// Apply the new sound settings.
		sndManager.SetBackgroundMusicVolume(oldMusicVolume);
		sndManager.SetBackgroundMusicEnabled (oldMusicEnabled);
		sndManager.SetSFXVolume(oldSoundVolume);
		sndManager.SetSoundEnabled (oldSoundEnabled);
	}
	
	/// <summary>
	/// Plays the background music.
	/// </summary>
	void PlayBackgroundMusic()
	{
		// Find the currently selected background track.
//		foreach(MCP.MyMusic track in MCP.userInfo.memberProfile.myInventory.music)
//		{
//			// Get the previously selected track.
//			if(track.selectedBackgroundMusic)
//			{
//				// Reselect it.
//				SetBackgroundMusic(track.clip);
//			}
//		}
	}
	
	/// <summary>
	/// Reverts the sound settings.
	/// </summary>
	void RevertSoundSettings()
	{
		musicEnabled = oldMusicEnabled;
		soundEnabled = oldSoundEnabled;
	
		sndManager.SetBackgroundMusicEnabled( oldMusicEnabled );
		sndManager.SetSoundEnabled( oldSoundEnabled );

		sndManager.SetBackgroundMusicVolume( oldMusicVolume );
		sndManager.SetSFXVolume( oldSoundVolume );
	}
	
	/// <summary>
	/// Sets the background music.
	/// </summary>
	/// <param name="musicPath">Music path.</param>
	/// <param name="preview">If set to <c>true</c> preview.</param>
	void SetBackgroundMusic(string musicPath, bool preview = false)
	{
		soundManager.GetComponent<AudioSource>().clip = Resources.Load(musicPath) as AudioClip;
		soundManager.GetComponent<AudioSource>().Play();
	}
	
	public override void DoGUI() {}
}
