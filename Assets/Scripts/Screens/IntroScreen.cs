﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class IntroScreen : MenuScreen
{
	private List<GameObject> mainObjects;
	private List<GameObject> videoObjects;
	private GameObject downloadingPopup;
	private GameObject downloadingSpinner;
	private GameObject localCanvas;
	private GameObject logoObject;
	private GameObject playButtonObject;
#if !UNITY_ANDROID && !UNITY_IOS
	private GameObject muteButtonObject;
#endif
	private GameObject audioPositionSliderObject;
	private GameObject currentTimeLabel;
	private GameObject totalTimeLabel;
	private GameObject volumeSliderObject;
	
	private Sprite[] playSprite;
	private Sprite[] skipSprite;
	
#if UNITY_ANDROID || UNITY_IOS
	private Sprite spinnerSprite;
#endif
	 
#if !UNITY_ANDROID && !UNITY_IOS
	private Sprite unmuteSprite;
	private Sprite muteSprite;
#endif
	
	private AudioSource waveformSource;
#if !UNITY_ANDROID && !UNITY_IOS
	private bool mute = false;
#endif
	public bool playing = true;
	
#if !UNITY_ANDROID && !UNITY_IOS
	private float timePlaying = 0;
	private MovieTexture videoClip;
#else
	private string url = MCP.serverURL +"Videos/intro-video.mp4";
#endif
	
#if UNITY_ANDROID || UNITY_IOS
	private float spinnerRot = 0f;
	private bool m_bFinish = false;
	public MediaPlayerCtrl scrMedia;
#endif

	private GameObject videoManagerObject;
	
	public InternalState internalState = InternalState.Main;
	public InternalState prevState = InternalState.Main;
	
	public enum InternalState
	{
		Main,
		Video
	}

	/// <summary>
	/// Used for initialization
	/// </summary>
	void Start ()
	{
		// Initialise the base.
		Init ();
		
		// Initialize lists
		mainObjects = new List<GameObject>();
		videoObjects = new List<GameObject>();
		playSprite = new Sprite[2];
		skipSprite = new Sprite[2];
		
		// Load the splash logo.
		playSprite[0] = Resources.Load<Sprite> ("GUI/bm_play_button");
		playSprite[1] = Resources.Load<Sprite> ("GUI/bm_play_button_active");
		skipSprite[0] = Resources.Load<Sprite> ("playericon_fastforward");
		skipSprite[1] = Resources.Load<Sprite> ("playericon_fastforward");
		
#if UNITY_ANDROID || UNITY_IOS
		spinnerSprite = Resources.Load<Sprite> ("GUI/bm_rotate");
#endif
		
#if !UNITY_ANDROID && !UNITY_IOS
		muteSprite = Resources.Load<Sprite> ("GUI/bm-nosound");
		unmuteSprite = Resources.Load<Sprite> ("GUI/bm-sound");
#endif
		
		// Make the waveform object.
		waveformSource = GetComponent<AudioSource>();
		
		// Create the scene GUI.
		CreateGUI ();
		
		localCanvas.transform.localScale = Vector2.zero;
		videoManagerObject = transform.GetChild (0).gameObject;
		
#if UNITY_ANDROID || UNITY_IOS
		StartCoroutine (PlayDeviceMovie (url));
		GetComponent<WebVideoPlayer>().enabled = false;
#else
		videoManagerObject.SetActive(false);
#endif
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Find the current canvas.
		localCanvas = MakeCanvas ();

		// Make background panel.
		MakeWindowBackground (localCanvas, new Rect(Screen.width * 0.02f, Screen.height * 0.1f, Screen.width * 0.96f, Screen.height * 0.88f), window_back).name = "BackgroundPanel";
		
//#if !UNITY_ANDROID && !UNITY_IOS
		// Make visual panel.
		GameObject visualPanelObject = MakeWindowBackground (localCanvas, new Rect(Screen.width * 0.2f, Screen.height * 0.225f, Screen.width * 0.6f, Screen.height * 0.6f), menuBackground);
		visualPanelObject.name = "VisualPanelObject";
		
		// Set the button navigation mode.
		UnityEngine.UI.Navigation nav = new UnityEngine.UI.Navigation();
		nav.mode = UnityEngine.UI.Navigation.Mode.None;
		
		// Make play/pause button
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			ControlPlay
		};
		playButtonObject = MakeButton (localCanvas, "", playSprite[0], new Rect(Screen.width * 0.439f, Screen.height * 0.4f, Screen.width * 0.1f, Screen.width * 0.1f), funcList);
		playButtonObject.name = "PlayButton";
		playButtonObject.GetComponent<Button>().navigation = nav;
		// Change the button transition to sprite swap.
		playButtonObject.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		SpriteState spriteState = new SpriteState();
		playButtonObject.GetComponent<Button>().targetGraphic = playButtonObject.transform.GetChild (0).gameObject.GetComponent<Image>();
		// Set the highlighted sprite.
		spriteState.highlightedSprite = playSprite[1];
		spriteState.pressedSprite = playSprite[1];
		playButtonObject.GetComponent<Button>().spriteState = spriteState;
		
#if !UNITY_ANDROID && !UNITY_IOS
		// Make volume slider
		volumeSliderObject = MakeSliderVertical (localCanvas, new Rect(Screen.width * 0.875f, Screen.height * 0.225f, Screen.width * 0.025f, Screen.height * 0.5f));
		volumeSliderObject.name = "VolumeSliderObject";
		volumeSliderObject.GetComponent<Slider>().maxValue = 20f;
		volumeSliderObject.GetComponent<Slider>().minValue = 0f;
		volumeSliderObject.GetComponent<Slider>().value = 10f;
		volumeSliderObject.GetComponent<Slider>().wholeNumbers = true;
		
		// Make volume/mute button
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				ControlMute();
				volumeSliderObject.GetComponent<Slider>().interactable = !volumeSliderObject.GetComponent<Slider>().interactable;
			}
		};
		muteButtonObject = MakeButton (localCanvas, "", unmuteSprite, new Rect(Screen.width * 0.8675f, Screen.height * 0.7625f, Screen.width * 0.04f, Screen.width * 0.04f), funcList);
		muteButtonObject.name = "MuteButtonObject";
#endif
		
		// Set the audio position background.
		GameObject audioPosBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.2f, Screen.height * 0.7125f, Screen.width * 0.6f, Screen.height * 0.1125f), menuBackground);
		audioPosBackground.name = "AudioPosBackground";
		
		// Make the audio position slider,
		audioPositionSliderObject = MakeSliderHorizontal (localCanvas, new Rect(Screen.width * 0.32f, Screen.height * 0.285f, Screen.width * 0.36f, Screen.height * 0.025f));
		audioPositionSliderObject.name = "AudioPositionSliderObject";
		// Set the slider attributes.
		audioPositionSliderObject.GetComponent<Slider>().maxValue = 100f;
		audioPositionSliderObject.GetComponent<Slider>().minValue = 0f;
		audioPositionSliderObject.GetComponent<Slider>().value = 0f;
		
		// Make the current time label.
		currentTimeLabel = MakeLabel (localCanvas, "00:00:00", new Rect(Screen.width * 0.205f, Screen.height * 0.7225f, Screen.width * 0.1f, Screen.height * 0.1f), "SmallText");
		currentTimeLabel.name = "CurrentTimeLabel";
		// Set the current time attributes.
		currentTimeLabel.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
		currentTimeLabel.GetComponent<Text>().resizeTextForBestFit = false;
		currentTimeLabel.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		
		// Make the total time label.
		totalTimeLabel = MakeLabel (localCanvas, "00:00:00", new Rect(Screen.width * 0.7f, Screen.height * 0.7225f, Screen.width * 0.1f, Screen.height * 0.1f), "SmallText");
		totalTimeLabel.name = "TotalTimeLabel";
		// Set the total time label.
		totalTimeLabel.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
		totalTimeLabel.GetComponent<Text>().resizeTextForBestFit = false;
		totalTimeLabel.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		
		// Make the play panel to cover the AV panel.
		funcList = new List<MyVoidFunc>
		{
			ControlPlay
		};
		GameObject playPanel = MakeButton (localCanvas, "", new Rect(Screen.width * 0.2f, Screen.height * 0.225f, Screen.width * 0.6f, Screen.height * 0.5f), funcList);
		playPanel.name = "PlayPanel";
//#else
//		List<MyVoidFunc> funcList;
//#endif
		
		funcList = new List<MyVoidFunc>
		{
			SkipVideo
		};
		GameObject skipButton = MakeButton (localCanvas, MCP.Text (218)/*"Continue"*/, buttonBackground, new Rect(Screen.width * 0.75f, Screen.height * 0.875f, Screen.width * 0.2f, Screen.height * 0.08f), funcList);
		skipButton.name = "SkipButton";
	}
	
	public void ForceSkipVideo()
	{
		//LoadMenu ("Landing_Screen");
		internalState = InternalState.Main;
		prevState = InternalState.Video;
	}
	
	void SkipVideo()
	{
#if UNITY_ANDROID || UNITY_IOS
		ForceSkipVideo();
#else
		GetComponent<WebVideoPlayer>().movieTimer = Mathf.Infinity;
		
		if(GetComponent<GUITexture>() != null && GetComponent<GUITexture>().texture != null && (GetComponent<GUITexture>().texture) as MovieTexture != null)
		{
			if(((GetComponent<GUITexture>().texture) as MovieTexture).duration < 0)
			{
				ForceSkipVideo ();
			}
		}
		else
		{
			ForceSkipVideo ();
		}
#endif
	}
	
	/// <summary>
	/// Control stopping the media.
	/// </summary>
	void ControlStop()
	{
		// Stop media and return to the beginning.
		waveformSource.Stop();
		
#if !UNITY_ANDROID && !UNITY_IOS
		// If there is a video clip playing.
		if(videoClip != null)
		{
			// Stop the video.
			videoClip.Stop ();
		}
#endif
		
		playing = false;
		
#if !UNITY_ANDROID && !UNITY_IOS
		// Change the play button sprite to play.
		playButtonObject.GetComponent<Image>().sprite = playSprite[0];
		// Add transparency to object to see the play button through it.
		Color c = GetComponent<GUITexture>().color;
		c.a = 64f / 255f;
		GetComponent<GUITexture>().color = c;
		
		timePlaying = 0;
#endif
	}
	
	/// <summary>
	/// Controls playing the media.
	/// </summary>
	public void ControlPlay()
	{
		// If the file path store is not empty...
		// Check if the media is currently playing.
		playing = !playing;
		
#if !UNITY_ANDROID && !UNITY_IOS
		// If it is playing, pause it.
		if(playing)
		{
			// Play
			waveformSource.Play ();
		}
		// Otherwise, make it play.
		else
		{
			// Pause
			waveformSource.Pause ();
		}
		
		// Toggle transparency.
		Color c = GetComponent<GUITexture>().color;
		if(playing)
		{
			c.a = 1f;
		}
		else
		{
			c.a = 64f / 255f;
		}
		GetComponent<GUITexture>().color = c;
		
		// If the video clip is not null...
		if(videoClip != null)
		{
			// If it is playing...
			if(playing)
			{
				// Unpause it.
				if(!((MovieTexture)GetComponent<GUITexture>().texture).isPlaying)
				{
					((MovieTexture)GetComponent<GUITexture>().texture).Play();
				}
//				videoClip.isPlaying = true;
			}
			// Otherwise...
			else
			{
				// Pause it
				if(((MovieTexture)GetComponent<GUITexture>().texture).isPlaying)
				{
					((MovieTexture)GetComponent<GUITexture>().texture).Pause();
				}
//				videoClip.isPlaying = false;
			}
		}
#else
		if(playing)
		{
			scrMedia.Play ();
		}
		else
		{
			scrMedia.Pause ();
		}
#endif
	}
	
	/// <summary>
	/// Controls muting the audio.
	/// </summary>
	void ControlMute()
	{
#if !UNITY_ANDROID && !UNITY_IOS
		mute = !mute;
		waveformSource.mute = mute;
		
		// If mute is now on.
		if(mute)
		{	
			// Mute sprite
			muteButtonObject.GetComponent<Image>().sprite = muteSprite;
		}
		// Otherwise...
		else
		{
			// Volume sprite
			muteButtonObject.GetComponent<Image>().sprite = unmuteSprite;
		}
#endif
	}
	
	void Update()
	{
#if !UNITY_ANDROID && !UNITY_IOS
		// Get the video clip.
		if(videoClip == null)
		{
			if(GetComponent<GUITexture>().texture != null)
			{
				videoClip = GetComponent<GUITexture>().texture as MovieTexture;
			}
		}
		else
		{
			// Disable the progress slider as there is no functionality for jumping to different parts of the video.
			audioPositionSliderObject.GetComponent<Slider>().interactable = false;
			audioPositionSliderObject.GetComponent<Slider>().value = 100f - ((waveformSource.clip.length - timePlaying) / waveformSource.clip.length * 100f);
			
			// Set the current time label text.
			timePlaying = GetComponent<WebVideoPlayer>().movieTimer;
			System.TimeSpan currentTime = new System.TimeSpan(0, 0, (int)timePlaying);
			currentTimeLabel.GetComponent<Text>().text = currentTime.ToString ();
			
			// If the clip is playing...
			if(videoClip.isPlaying)
			{
				// Set the total time label text.
				System.TimeSpan totalTime = new System.TimeSpan(0, 0, (int)GetComponent<AudioSource>().clip.length);
				totalTimeLabel.GetComponent<Text>().text = totalTime.ToString ();
			}
			
			// Check for when the clip is finished.
			if(timePlaying >= videoClip.duration && videoClip.isPlaying)
			{
				// When it is finished, handle stopping properly.
				ControlStop ();
			}
		}
		
		float volume = volumeSliderObject.GetComponent<Slider>().value * 5f;
		waveformSource.volume = volume / 100f;
#else
		if(scrMedia.GetCurrentState () == MediaPlayerCtrl.MEDIAPLAYER_STATE.PAUSED || scrMedia.GetCurrentState () == MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING)
		{
			audioPositionSliderObject.GetComponent<Slider>().interactable = false;
			
			System.TimeSpan currentTime = new System.TimeSpan((long)scrMedia.GetSeekPosition () * 10000);
			currentTimeLabel.GetComponent<Text>().text = (currentTime.ToString ()).Split (new char[]{'.'}, System.StringSplitOptions.None)[0];
			
			System.TimeSpan totalTime = new System.TimeSpan((long)scrMedia.GetDuration () * 10000);
			totalTimeLabel.GetComponent<Text>().text = (totalTime.ToString ()).Split (new char[]{'.'}, System.StringSplitOptions.None)[0];
		
			videoManagerObject.GetComponent<MeshRenderer>().enabled = true;
			videoManagerObject.transform.localScale = new Vector3(10f, 5f, 1f);
			
			if(scrMedia.GetCurrentState () == MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING)
			{
				videoManagerObject.transform.localPosition = new Vector3(0, 0.25f, 9.9f);
			}
			else if(scrMedia.GetCurrentState () == MediaPlayerCtrl.MEDIAPLAYER_STATE.PAUSED)
			{
				videoManagerObject.transform.localPosition = new Vector3(0, 0.25f, 10.1f);
			}
			
			if(downloadingPopup != null)
			{
				Destroy (downloadingPopup);
			}
		}
		else
		{
			if(downloadingPopup == null)
			{
				downloadingPopup = MakePopup(localCanvas, new Rect(Screen.width * 0.25f, Screen.height * 0.25f, Screen.width * 0.5f, Screen.height * 0.5f), MCP.Text (1908)/*"Downloading..."*/, "", "", CommonTasks.DestroyParent);
				// Remove the button from the popup
				Destroy(downloadingPopup.transform.GetChild (2).gameObject);
				
				downloadingSpinner = MakeImage (localCanvas, new Rect(Screen.width * 0.25f, Screen.height * 0.2f, Screen.width * 0.08f, Screen.width * 0.08f), spinnerSprite, false);
				downloadingSpinner.name = "LoadingSpinner";
				downloadingSpinner.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
				downloadingSpinner.transform.SetParent (downloadingPopup.transform, true);
				downloadingSpinner.transform.localScale = Vector3.one;
			}
			else
			{
				// Spin the downloading spinner
				spinnerRot -= Time.deltaTime * 75f;
				downloadingSpinner.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, spinnerRot);
				
				Vector3 newPos = downloadingPopup.transform.GetChild (1).gameObject.GetComponent<RectTransform>().localPosition;
				newPos.y = Screen.height * 0.25f;
				downloadingPopup.transform.GetChild (1).gameObject.GetComponent<RectTransform>().localPosition = newPos;
				downloadingPopup.transform.GetChild (1).gameObject.GetComponent<Text>().text = "";
			}
			
			videoManagerObject.GetComponent<MeshRenderer>().enabled = false;
		}
		
		if(m_bFinish)
		{
			scrMedia.UnLoad ();
			ForceSkipVideo();
		}
#endif
		
		// If the state has changed...
		if(internalState != prevState)
		{
			switch(internalState)
			{
			case InternalState.Main:
				// Hide the video objects.
				foreach(GameObject g in videoObjects)
				{
					g.SetActive (false);
				}
				// Show the main objects.
				foreach(GameObject g in mainObjects)
				{
					g.SetActive (true);
				}
				
				LoadMenu ("Landing_Screen");
				break;
				
			case InternalState.Video:
				// Hide the main objects.
				foreach(GameObject g in mainObjects)
				{
					g.SetActive (false);
				}
				// Show the video objects.
				foreach(GameObject g in videoObjects)
				{
					g.SetActive (true);
				}
				break;
			}
		}
		
		// Store the previous frame state.
		prevState = internalState;
	}
	
	private IEnumerator PlayDeviceMovie(string url)
	{
		playing = true;
		
#if UNITY_ANDROID || UNITY_IOS
		yield return scrMedia.Load (url);
#else
		yield return null;
#endif
	}
	
	public void VideoEnded()
	{
#if UNITY_ANDROID || UNITY_IOS
		LoadMenu ("Landing_Screen");
		// Hide the video objects.
//		foreach(GameObject g in videoObjects)
//		{
//			g.SetActive (false);
//		}
//		// Show the main objects.
//		foreach(GameObject g in mainObjects)
//		{
//			g.SetActive (true);
//		}
//		ForceSkipVideo ();
#endif
	}
	
	void OnDestroy()
	{
#if !UNITY_ANDROID && !UNITY_IOS
		if(videoClip != null)
		{
			videoClip.Stop ();
			videoClip = null;
		}
#endif
	}
	
	public override void DoGUI (){}
}
