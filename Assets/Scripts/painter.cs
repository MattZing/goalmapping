using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using LitJson;

public class painter : MenuScreen
{
	private const int numberOfUndoSteps = 3;
	
	private Vector2 dragStart;
	private Vector2 dragEnd;
	private Vector2 preDrag;
	
	public enum Tool
	{
		None,
		Brush,
		Line,
		Eraser,
		Vector
	}
	
	public enum Navigation
	{
		Brush,
		Move,
		Overlay
	}
	
	public enum OverlayState
	{
		Off,
		On,
		Move
	}
	
	public Drawing.Samples AntiAlias = Drawing.Samples.Samples4;
	public Tool tool = Tool.Brush;
	public OverlayState overlayState = OverlayState.Off;
	
	// Brush attributes.
	public float brushWidth = 3f;
	public float brushHardness = 5f;
	public float brushOpacity = 255f;
	public float oldBrushWidth = 3f;
	public float oldBrushHardness = 5f;
	public float oldBrushOpacity = 255f;
	
	public Color col = Color.white;
	public Color prevCol = Color.white;
	public GUISkin gskin;
	private Vector2 startPan = Vector2.zero;
	private float zoom = 150f;
	private float startZoom = 150f;
	private float overlayZoom = 1f;
	private float startOverlayZoom = 1f;
	public BezierPoint[] BezierPoints;
	
	private Vector2 initialSizeDelta;
	private Rect imgRect;
	private Color[,] swatchColors;
	private bool backedUp = false;
	private Texture2D[] canvasBackups;
	private Texture2D canvasTex;
	public Texture2D overlayTex;
	
	public Sprite[] drawModeSprites;
	public Sprite[] drawModeSpritesHighlighted;
	public Sprite[] brushSettingsSprite;
	public Sprite[] navigationModeSprites;
	public Sprite[] overlaySprites;
	public Sprite[] overlaySettingsSprites;
	public Sprite[] undoButton;
	public Sprite[] saveButton;
	public Sprite[] clearButton;
	public Sprite colorWheelSprite;
	public Sprite colorWheelBackgroundSprite;
	private Sprite brushPreviewSprite;
	private Sprite colorWheelCircleSprite;
	
#if !UNITY_ANDROID && !UNITY_IPHONE
	private Texture2D drawTex;
	private Texture2D eraseTex;
#endif
	
	public bool edittingImage = false;
	public bool gettingOverlay = false;
	private bool moveZoomActive = false;
	private bool clickStartedInColorCircle = false;
	private int currentDrawMode = 0;
	private float mouseTimer = 0f;
	private float startDist;
	private string saveFileName = "New_Painting";
	private bool saveDialogUp = false;
	
	public GameObject canvasCamera;
	private GameObject canvasCanvas;
	private GameObject[,] colorSwatches;
	private GameObject drawModeButton;
	private GameObject navigationModeButton;
	private GameObject overlayModeButton;
	private GameObject overlaySettingsButton;
	private GameObject newColorPreview;
	private GameObject prevColorPreview;
	private GameObject brushPreview;
	private GameObject brushSizeSlider;
	private GameObject brushSizeText;
	private GameObject brushHardnessSlider;
	private GameObject brushHardnessText;
	private GameObject brushOpacitySlider;
	private GameObject brushOpacityText;
	private GameObject canvasObject;
	private GameObject colorWheel;
	
	public GameObject sceneCanvas;
	public GameObject editCanvas;
	private GameObject localCanvas;
	private GameObject mainScreenGUI;
	private GameObject brushSettingsGUI;
	private GameObject overlayImage;
	private GameObject colorWheelCircle;
	private GameObject dialogInput;

	public Shader guideShader;
	private Vector2 guideBrushPrevEnd, guideBrushOrigStart;
	private bool guideBrushClicked = false;
	private GameObject guideBrushParent;
	private LineRenderer guideBrushLine;
	private Material guideBrushMaterial;
	
#if UNITY_ANDROID || UNITY_IPHONE
	private bool zooming = false;
#endif

	private const float overlayZ = -1000;
	
	// Used for saving Color objects to json.
	public struct TempColor
	{
		public float r, g, b, a;
		
		public TempColor (Color col)
		{
			r = col.r;
			g = col.g;
			b = col.b;
			a = col.a;
		}
	};
	
	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start()
	{
		// Initialize the base.
		Init();

		guideBrushMaterial = Resources.Load ("BrushGuideLineMat") as Material;
		guideBrushParent = new GameObject ();
		guideBrushParent.layer = 8;
		guideBrushParent.transform.position = new Vector3 (0f, 0f, 150f);
		if (guideBrushParent != null) 
		{
			guideBrushLine = guideBrushParent.AddComponent<LineRenderer> ();
			guideBrushLine.material = guideBrushMaterial;
		}

		// Initialize arrays
		drawModeSprites = new Sprite[3];
		drawModeSpritesHighlighted = new Sprite[3];
		brushSettingsSprite = new Sprite[2];
		navigationModeSprites = new Sprite[2];
		overlaySprites = new Sprite[3];
		overlaySettingsSprites = new Sprite[2];
		undoButton = new Sprite[2];
		saveButton = new Sprite[2];
		clearButton = new Sprite[2];
		
		// Load resources
		brushPreviewSprite = Resources.Load<Sprite>("BrushPreview");
		
		drawModeSprites[0] = Resources.Load<Sprite>("GUI/Painter/paintbox_drawmode1");
		drawModeSprites[1] = Resources.Load<Sprite>("GUI/Painter/paintbox_drawmode2");
		drawModeSprites[2] = Resources.Load<Sprite>("GUI/Painter/paintbox_drawmode3");
		
		drawModeSpritesHighlighted[0] = Resources.Load<Sprite>("GUI/Painter/paintbox_drawmode1_active");
		drawModeSpritesHighlighted[1] = Resources.Load<Sprite>("GUI/Painter/paintbox_drawmode2_active");
		drawModeSpritesHighlighted[2] = Resources.Load<Sprite>("GUI/Painter/paintbox_drawmode3_active");
		
		brushSettingsSprite[0] = Resources.Load<Sprite>("GUI/Painter/paintbox_settings");
		brushSettingsSprite[1] = Resources.Load<Sprite>("GUI/Painter/paintbox_settings_active");
		
		navigationModeSprites[0] = Resources.Load<Sprite>("GUI/Painter/paintbox_pinchzoom");
		navigationModeSprites[1] = Resources.Load<Sprite>("GUI/Painter/paintbox_pinchzoom_active");
		
		overlaySprites[0] = Resources.Load<Sprite>("GUI/richtext_aligncentre");
		overlaySprites[1] = Resources.Load<Sprite>("GUI/richtext_aligncentre_active");
		overlaySprites[2] = Resources.Load<Sprite>("GUI/Painter/paintbox_pinchzoom_active");
		
		overlaySettingsSprites[0] = Resources.Load<Sprite>("GUI/GoalMap/goalmap_picture");
		overlaySettingsSprites[1] = Resources.Load<Sprite>("GUI/GoalMap/goalmap_picture_active");
		
		undoButton[0] = Resources.Load<Sprite>("GUI/Painter/paintbox_undo");
		undoButton[1] = Resources.Load<Sprite>("GUI/Painter/paintbox_undo_active");
		
		saveButton[0] = Resources.Load<Sprite>("GUI/Painter/paintbox_done");
		saveButton[1] = Resources.Load<Sprite>("GUI/Painter/paintbox_done_active");
		
		clearButton[0] = Resources.Load<Sprite>("GUI/Painter/paintbox_clear");
		clearButton[1] = Resources.Load<Sprite>("GUI/Painter/paintbox_clear_active");
		
		colorWheelSprite = Resources.Load<Sprite>("colorcircle");
		colorWheelBackgroundSprite = Resources.Load<Sprite>("colorcirclebackground");
		colorWheelCircleSprite = Resources.Load<Sprite>("colorcircle_picker");
		
#if !UNITY_ANDROID && !UNITY_IPHONE
		drawTex = Resources.Load("GUI/brush") as Texture2D;
		eraseTex = Resources.Load("GUI/eraser") as Texture2D;
#endif
		
		// Get references to scene objects.
		canvasCamera = GameObject.Find ("CanvasCamera");
		
		// Create 3D GUI.
		CreateGUI ();
		
		localCanvas.transform.localScale = Vector2.zero;
		
		// Set default variables.
		col = Color.black;
		
		// Clear the canvas to the new background.
		ClearCanvas();
		
		// Setup the canvas backups.
		canvasBackups = new Texture2D[numberOfUndoSteps + 1];
		for( int i = 0; i < canvasBackups.Length; i++)
		{
			canvasBackups[i] = new Texture2D(canvasTex.width, canvasTex.height);
			canvasBackups[i].SetPixels (canvasTex.GetPixels ());
		}
	}
	
	private Color[,] SetDefaultSwatchColors()
	{
		Color[,] cols = new Color[4, 4];
		
		for(int y = 0; y < colorSwatches.GetLength (1); y++)
		{
			for(int x = 0; x < colorSwatches.GetLength (0); x++)
			{
				// Initialize each color.
				cols[x, y] = new Color();
				if(y == 0)
				{
					switch(x)
					{
					case 0:
						cols[x, y] = Color.black;
						break;
						
					case 1:
						cols[x, y] = Color.red;
						break;
						
					case 2:
						cols[x, y] = Color.green;
						break;
						
					case 3:
						cols[x, y] = Color.blue;
						break;
					}
				}
				else
				{
					// Set swatch color to white.
					cols[x, y] = Color.white;
				}
				// Make the swatch object the same color as the stored color.
				colorSwatches[x, y].GetComponent<Image>().color = cols[x, y];
			}
		}
		
		return cols;
	}
	
	/// <summary>
	/// Loads the color swatches.
	/// </summary>
	private void LoadColors()
	{
		bool notFilled = false;
		TempColor[] tc = new TempColor[16];
		
//#if UNITY_WEBPLAYER
		if(MCP.userInfo != null && MCP.userInfo.memberProfile != null)
		{
			try
			{
				// Split the json into seperate colors.
				tc = LitJson.JsonMapper.ToObject<TempColor[]>(MCP.userInfo.memberProfile.colorSwatchJson);
				
				if(tc == null)
				{
					notFilled = true;
				}
			}
			// Otherwise, initialize a new list of temp colors.
			catch(IOException e)
			{
				Debug.Log ("JsonException: " + e.Message);
				notFilled = true;
			}
		}
		else
		{
			notFilled = true;
		}
//#else
//		StreamReader reader;
//		string[] json = new string[16];
//		
//		// Make sure the swatch colors file exists.
//		if(File.Exists (Application.persistentDataPath + "/SwatchColors.txt"))
//		{
//			reader = new StreamReader(Application.persistentDataPath + "/SwatchColors.txt");
//			// Read the swatch color json.
//			string swatchColorsString = reader.ReadLine ();
//			reader.Close ();
//		
//			// If the file is a valid json...
//			try
//			{
//				// Split the json into seperate colors.
//				json = LitJson.JsonMapper.ToObject<string[]>(swatchColorsString);
//				// Set each temp color to the current color string.
//				for(int i = 0; i < 16; i++)
//				{
//					tc[i] = LitJson.JsonMapper.ToObject<TempColor>(json[i]);
//				}
//			}
//			// Otherwise, initialize a new list of temp colors.
//			catch(IOException e)
//			{
//				Debug.Log ("JsonException: " + e.Message);
//				tc = new TempColor[16];
//			}
//		}
//#endif
		
		// If the json was not valid, set the swatch colors to the default.
		if(tc == null || tc.Length == 0 || notFilled)
		{
			Debug.Log ("Empty swatch color Json. Setting to defaults.");
			
			swatchColors = SetDefaultSwatchColors ();
			
			// Save the default color swatches for next time.
			SaveColors ();
		}
		else
		{
			// Set the swatch colors to the colors stored in the file. Will set them to the defaults if these have just been set.
			swatchColors = new Color[4, 4];
			
			for(int y = 0; y < colorSwatches.GetLength (1); y++)
			{
				for(int x = 0; x < colorSwatches.GetLength (0); x++)
				{
					// Force the first color to be black.
					if(x == 0 && y == 0)
					{
						swatchColors[0, 0] = new Color(0, 0, 0, 1);
					}
					else
					{
						// Convert the values from the TempColors to Colors.
						swatchColors[x, y] = new Color(tc[x + (y * 4)].r, tc[x + (y * 4)].g, tc[x + (y * 4)].b, 1);
					}
					
					// Make the swatch object the same color as the stored color.
					colorSwatches[x, y].GetComponent<Image>().color = swatchColors[x, y];
				}
			}
		}
	}
	
	/// <summary>
	/// Converts to color swatches to a json string.
	/// </summary>
	/// <returns>The json string.</returns>
	private string SwatchColorsToJsonString()
	{
		string[] colors = new string[16];
		
		// For each color swatch...
		for(int y = 0; y < swatchColors.GetLength (1); y++)
		{
			for(int x = 0; x < swatchColors.GetLength (0); x++)
			{
				// Convert the swatch colors to TempColor objects for the json (Color objects don't work).
				TempColor c = new TempColor(swatchColors[x, y]);
				// Get the json for the color.
				colors[x + (y * 4)] = JsonMapper.ToJson (c);
			}
		}
		
		string colorsJson = "";
#if UNITY_ANDROID || UNITY_IPHONE
		colorsJson= JsonMapper.ToJson (colors);
#else
		// Make a json of all the colors from each color json.
		colorsJson = "[";
		for(int i = 0; i < colors.Length; i++)
		{
			colorsJson += colors[i];
			
			if(i != colors.Length - 1)
			{
				colorsJson += ",";
			}
		}
		colorsJson += "]";
#endif
		
		return colorsJson;
	}
	
	/// <summary>
	/// Saves the colors.
	/// </summary>
	private void SaveColors()
	{
		// Get the colors as a json.	
		string json = SwatchColorsToJsonString ();
		
#if UNITY_WEBPLAYER
		if(MCP.userInfo != null && MCP.userInfo.memberProfile != null)
		{
			MCP.userInfo.memberProfile.colorSwatchJson = json;
			StartCoroutine (MCP.UpdateMemberProfile ());
		}
#else
		string jsonPath = Application.persistentDataPath + "/SwatchColors.txt";
		
		// Open a file stream to the file to write to.
		FileStream fs = new FileStream(jsonPath, FileMode.Create, FileAccess.Write);
		
		// Write the swatch colors to file.
		StreamWriter writer = new StreamWriter(fs);
		writer.WriteLine (json);
		
		// Close the file streams.
		writer.Close ();
		fs.Close ();
		Debug.Log ("File Written");
#endif
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Get the scene camera.
		if(sceneCanvas == null)
		{
			sceneCanvas = MakeCanvas (false);
		}
		
		sceneCanvas.SetActive (false);
		
		// Make new canvas.
		localCanvas = MakeCanvas (true);
		localCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
		MenuScreen.firstFrameTimer = -5;
		
		// Make main background.
		GameObject mainBackground = MakeImage (localCanvas, new Rect(0, 0, Screen.width, Screen.height), menuBackground);
		mainBackground.name = "MainBackground";
		mainBackground.AddComponent<BoxCollider>();
		
		/* Main screen objects */
		mainScreenGUI = new GameObject();
		mainScreenGUI.name = "MainScreenGUI";
		mainScreenGUI.AddComponent<RectTransform>();
		
		mainScreenGUI.transform.SetParent (localCanvas.transform);
		mainScreenGUI.transform.localPosition = Vector3.zero;
		mainScreenGUI.transform.localScale = Vector3.one;
		mainScreenGUI.GetComponent<RectTransform>().sizeDelta = Vector2.zero;
		
		// Make the draw mode label.
		GameObject drawModeText = MakeLabel (mainScreenGUI, MCP.Text (2301)/*"Draw\nMode"*/, new Rect((Screen.width * -0.025f) - (Screen.width * 0.0f), (Screen.height * 0.175f) - (Screen.height * 0.0f), Screen.width * 0.2f, Screen.height * 0.14f), TextAnchor.MiddleCenter, "InnerLabel");
		drawModeText.name = "DrawModeText";
		drawModeText.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		
		// Make draw-mode button.
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			ChangeDrawMode
		};
		
		// Make the draw mode button.
		drawModeButton = MakeButton (mainScreenGUI, "", drawModeSprites[0], new Rect((Screen.width * 0.075f) - (Screen.height * 0.07f) - (Screen.width * 0.0f), (Screen.height * 0.075f) - (Screen.height * 0.0f), Screen.height * 0.14f, Screen.height * 0.14f), funcList);
		drawModeButton.name = "DrawModeButton";
		// Set the button navigation mode.
		UnityEngine.UI.Navigation nav = new UnityEngine.UI.Navigation();
		nav.mode = UnityEngine.UI.Navigation.Mode.None;
		drawModeButton.GetComponent<Button>().navigation = nav;
		
		drawModeButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		SpriteState spriteState = new SpriteState();
		drawModeButton.GetComponent<Button>().targetGraphic = drawModeButton.GetComponentInChildren<Image>();
		spriteState.highlightedSprite = drawModeSpritesHighlighted[0];
		spriteState.pressedSprite = drawModeSpritesHighlighted[0];
		drawModeButton.GetComponent<Button>().spriteState = spriteState;
		
		// Make the brush settings text.
		GameObject brushSettingsText = MakeLabel (mainScreenGUI, MCP.Text (2302)/*"Brush\nSettings"*/, new Rect((Screen.width * 0.025f) - (Screen.width * 0.0f), (Screen.height * 0.4f) - (Screen.height * 0.0f), Screen.width * 0.1f, Screen.height * 0.14f), TextAnchor.MiddleCenter, "InnerLabel");
		brushSettingsText.name = "BrushSettingsText";
		brushSettingsText.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		
		// Make brush settings button.
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				// Store the current color and set the preview planes.
				newColorPreview.GetComponent<Image>().color = col;
				prevCol = col;
				prevColorPreview.GetComponent<Image>().color = prevCol;
				// Enable the brush settings GUI.
				brushSettingsGUI.SetActive(true);
				// Disable the main screen GUI.
				mainScreenGUI.SetActive(false);
				canvasCamera.SetActive(false);
			}
		};
		GameObject brushSettingsButton = MakeButton (mainScreenGUI, "", brushSettingsSprite[0], new Rect((Screen.width * 0.075f) - (Screen.height * 0.07f) - (Screen.width * 0.0f), (Screen.height * 0.3f) - (Screen.height * 0.0f), Screen.height * 0.14f, Screen.height * 0.14f), funcList);
		brushSettingsButton.name = "BrushSettingsButton";
		
		// Set the button navigation mode.
		brushSettingsButton.GetComponent<Button>().navigation = nav;
		brushSettingsButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		brushSettingsButton.GetComponent<Button>().targetGraphic = brushSettingsButton.GetComponentInChildren<Image>();
		spriteState.highlightedSprite = brushSettingsSprite[1];
		spriteState.pressedSprite = brushSettingsSprite[1];
		brushSettingsButton.GetComponent<Button>().spriteState = spriteState;
		
		// Set the navigation mode text.
		GameObject navigationModeText = MakeLabel (mainScreenGUI, MCP.Text (2303)/*"Navigation\nMode"*/, new Rect((Screen.width * 0.01f) - (Screen.width * 0.0f), (Screen.height * 0.622f) - (Screen.height * 0.0f), Screen.width * 0.13f, Screen.height * 0.14f), TextAnchor.MiddleCenter, "InnerLabel");
		navigationModeText.name = "NavigationModeText";
		navigationModeText.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		
		// Make navigation mode button.
		funcList = new List<MyVoidFunc>
		{
			ChangeNavigationMode
		};
		navigationModeButton = MakeButton (mainScreenGUI, "", navigationModeSprites[0], new Rect((Screen.width * 0.075f) - (Screen.height * 0.07f) - (Screen.width * 0.0f), (Screen.height * 0.525f) - (Screen.height * 0.0f), Screen.height * 0.14f, Screen.height * 0.14f), funcList);
		navigationModeButton.name = "NavigationModeButton";
		
		// Set the navigation mode text.
		GameObject overlayModeText = MakeLabel (mainScreenGUI, MCP.Text (2304)/*"Overlay"*/, new Rect((Screen.width * 0.01f) - (Screen.width * 0.0f), (Screen.height * 0.85f) - (Screen.height * 0.0f), Screen.width * 0.13f, Screen.height * 0.14f), TextAnchor.MiddleCenter, "InnerLabel");
		overlayModeText.name = "OverlayModeText";
		overlayModeText.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		
		// Make navigation mode button.
		funcList = new List<MyVoidFunc>
		{
			ToggleOverlay
		};
		overlayModeButton = MakeButton (mainScreenGUI, "", overlaySprites[0], new Rect((Screen.width * 0.075f) - (Screen.height * 0.07f) - (Screen.width * 0.0f), (Screen.height * 0.75f) - (Screen.height * 0.0f), Screen.height * 0.14f, Screen.height * 0.14f), funcList);
		overlayModeButton.name = "OverlayModeButton";
		
		
		// Make the undo button text.
		GameObject undoButtonText = MakeLabel (mainScreenGUI, MCP.Text (2305)/*"Undo"*/, new Rect((Screen.width * 0.875f) - (Screen.width * 0.0f), (Screen.height * 0.175f) - (Screen.height * 0.0f), Screen.width * 0.1f, Screen.height * 0.14f), TextAnchor.MiddleCenter, "InnerLabel");
		undoButtonText.name = "UndoButtonText";
		undoButtonText.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		
		// Make undo button.
		funcList = new List<MyVoidFunc>
		{
			UndoAction
		};
		GameObject undoCanvasButton = MakeButton (mainScreenGUI, "", undoButton[0], new Rect((Screen.width * 0.925f) - (Screen.height * 0.07f) - (Screen.width * 0.0f), (Screen.height * 0.075f) - (Screen.height * 0.0f), Screen.height * 0.14f, Screen.height * 0.14f), funcList, "InnerLabel");
		undoCanvasButton.name = "UndoCanvasButton";
		
		// Set the button navigation mode.
		undoCanvasButton.GetComponent<Button>().navigation = nav;
		undoCanvasButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		undoCanvasButton.GetComponent<Button>().targetGraphic = undoCanvasButton.GetComponentInChildren<Image>();
		spriteState.highlightedSprite = undoButton[1];
		spriteState.pressedSprite = undoButton[1];
		undoCanvasButton.GetComponent<Button>().spriteState = spriteState;
		
		// Make the save canvas button text.
		GameObject saveCanvasText = MakeLabel (mainScreenGUI, MCP.Text (2306)/*"Done"*/, new Rect((Screen.width * 0.875f) - (Screen.width * 0.0f), (Screen.height * 0.4f) - (Screen.height * 0.0f), Screen.width * 0.1f, Screen.height * 0.14f), TextAnchor.MiddleCenter, "InnerLabel");
		saveCanvasText.name = "SaveCanvasText";
		saveCanvasText.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		
		// Make done button.
		funcList = new List<MyVoidFunc>
		{
			OpenSaveDialog
		};
		GameObject saveCanvasButton = MakeButton (mainScreenGUI, "", saveButton[0], new Rect((Screen.width * 0.925f) - (Screen.height * 0.07f) - (Screen.width * 0.0f), (Screen.height * 0.3f) - (Screen.height * 0.0f), Screen.height * 0.14f, Screen.height * 0.14f), funcList, "InnerLabel");
		saveCanvasButton.name = "SaveCanvasButton";
		// Set the button navigation mode.
		saveCanvasButton.GetComponent<Button>().navigation = nav;
		saveCanvasButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		saveCanvasButton.GetComponent<Button>().targetGraphic = saveCanvasButton.GetComponentInChildren<Image>();
		spriteState.highlightedSprite = saveButton[1];
		spriteState.pressedSprite = saveButton[1];
		saveCanvasButton.GetComponent<Button>().spriteState = spriteState;
		
		// Make the clear canvas button text.
		GameObject clearCanvasText = MakeLabel (mainScreenGUI, MCP.Text (2307)/*"Clear"*/, new Rect((Screen.width * 0.875f) - (Screen.width * 0.0f), (Screen.height * 0.622f) - (Screen.height * 0.0f), Screen.width * 0.1f, Screen.height * 0.14f), TextAnchor.MiddleCenter, "InnerLabel");
		clearCanvasText.name = "ClearCanvasText";
		clearCanvasText.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		
		// Make clear button.
		funcList = new List<MyVoidFunc>
		{
			ClearCanvas
		};
		GameObject clearCanvasButton = MakeButton (mainScreenGUI, "", clearButton[0], new Rect((Screen.width * 0.925f) - (Screen.height * 0.07f) - (Screen.width * 0.0f), (Screen.height * 0.525f) - (Screen.height * 0.0f), Screen.height * 0.14f, Screen.height * 0.14f), funcList, "InnerLabel");
		clearCanvasButton.name = "ClearCanvasButton";
		// Set the button navigation mode.
		clearCanvasButton.GetComponent<Button>().navigation = nav;
		clearCanvasButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		clearCanvasButton.GetComponent<Button>().targetGraphic = clearCanvasButton.GetComponentInChildren<Image>();
		spriteState.highlightedSprite = clearButton[1];
		spriteState.pressedSprite = clearButton[1];
		clearCanvasButton.GetComponent<Button>().spriteState = spriteState;
		
		// Set the navigation mode text.
		GameObject overlaySettingsText = MakeLabel (mainScreenGUI, MCP.Text (2308)/*"Set\nOverlay"*/, new Rect((Screen.width * 0.86f) - (Screen.width * 0.0f), (Screen.height * 0.85f) - (Screen.height * 0.0f), Screen.width * 0.13f, Screen.height * 0.14f), TextAnchor.MiddleCenter, "InnerLabel");
		overlaySettingsText.name = "OverlaySettingsText";
		overlaySettingsText.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		
		// Make navigation mode button.
		funcList = new List<MyVoidFunc>
		{
			GetOverlayImage
		};
		overlaySettingsButton = MakeButton (mainScreenGUI, "", overlaySettingsSprites[0], new Rect((Screen.width * 0.925f) - (Screen.height * 0.07f) - (Screen.width * 0.0f), (Screen.height * 0.75f) - (Screen.height * 0.0f), Screen.height * 0.14f, Screen.height * 0.14f), funcList, "InnerLabel");
		overlaySettingsButton.name = "OverlaySettingsButton";
		
		// Set the button navigation mode.
		overlaySettingsButton.GetComponent<Button>().navigation = nav;
		overlaySettingsButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		overlaySettingsButton.GetComponent<Button>().targetGraphic = overlaySettingsButton.GetComponentInChildren<Image>();
		spriteState.highlightedSprite = overlaySettingsSprites[1];
		spriteState.pressedSprite = overlaySettingsSprites[1];
		overlaySettingsButton.GetComponent<Button>().spriteState = spriteState;
		
		// Make canvas.
		canvasObject = GameObject.CreatePrimitive (PrimitiveType.Quad);
		canvasObject.name = "CanvasObject";
		
		double height, width;
		
		height = GameObject.Find ("CanvasCamera").GetComponent<Camera> ().orthographicSize * 1f;
		
		width = height * Screen.width / Screen.height;
		canvasObject.transform.position = new Vector3 (0, 0, zoom);
		canvasObject.transform.localScale = new Vector3 ((float)width, (float)height, 0.1f);
		canvasObject.layer = LayerMask.NameToLayer ("Map");
		// Make the canvas texture.
		canvasTex = new Texture2D (256, 256);
		
		if(guideShader == null)
		{
			//guideShader = Shader.Find ("Unlit/Transparent");
			guideShader = Shader.Find ("Mobile/Particles/Alpha Blended");
		}
		Material material = new Material (guideShader);
		material.color = Color.white;
		canvasObject.GetComponent<Renderer> ().sharedMaterial = material;
		canvasObject.GetComponent<Renderer> ().sharedMaterial.mainTexture = canvasTex;
		canvasObject.transform.SetParent (GameObject.Find ("CanvasCamera").transform, true);
		
		// Set the initial canvas camera position.
		canvasCamera.transform.position = new Vector3(0f, 0f, -10f);
		
		// Set the initial rect containing the canvas.
		imgRect = canvasCamera.GetComponent<Camera>().pixelRect;
		imgRect.x = (Screen.width - imgRect.width) * 0.5f;
		imgRect.y = (Screen.height - imgRect.height) * 0.5f;
		
		// Make object for the overlaying image
		overlayImage = GameObject.CreatePrimitive (PrimitiveType.Quad);
		overlayImage.name = "OverlayImage";
		
		overlayImage.transform.position = new Vector3 (0, 0, overlayZ);
		overlayImage.transform.localScale = new Vector3 ((float)width, (float)height, 0.1f);
		overlayImage.layer = LayerMask.NameToLayer ("Map");
		Destroy(overlayImage.GetComponent<MeshCollider>());
		overlayTex = Resources.Load ("GUI/star_gold") as Texture2D;
		
		Shader shader = Shader.Find ("Mobile/Particles/Multiply");
		Material overlayMaterial = new Material (shader);
		overlayImage.GetComponent<Renderer> ().sharedMaterial = overlayMaterial;
		overlayImage.GetComponent<Renderer> ().sharedMaterial.mainTexture = overlayTex;
		
		overlayImage.transform.SetParent (canvasObject.transform, true);
		Vector3 newPos = overlayImage.transform.localPosition;
		newPos.z = overlayZ;
		overlayImage.transform.localPosition = newPos;
		overlayImage.transform.localScale = new Vector3(0.5f, 0.5f, 1);
		overlayImage.SetActive (false);
		
		/* Brush settings objects */
		brushSettingsGUI = new GameObject();
		brushSettingsGUI.name = "BrushSettingsGUI";
		brushSettingsGUI.AddComponent<RectTransform>();
		
		brushSettingsGUI.transform.SetParent (localCanvas.transform);
		brushSettingsGUI.transform.localPosition = Vector3.zero;
		brushSettingsGUI.transform.localScale = Vector3.one;
		brushSettingsGUI.GetComponent<RectTransform>().sizeDelta = Vector2.zero;
		brushSettingsGUI.SetActive (false);
		
		// Make area title.
		GameObject brushSettingsTitle = MakeLabel (localCanvas, MCP.Text (2309)/*"Brush Settings"*/, new Rect((Screen.width * 0.075f) - (Screen.width * 0.0f), (Screen.height * 0.04f) - (Screen.height * 0.0f), Screen.width * 0.25f, Screen.height * 0.2f), TextAnchor.MiddleCenter, "ScreenTitle");
		brushSettingsTitle.name = "BrushSettingsTitle";
		brushSettingsTitle.GetComponent<Text>().fontStyle = FontStyle.Bold;
		brushSettingsTitle.transform.SetParent (brushSettingsGUI.transform);
		
		// Make color wheel Background.
		GameObject colorWheelBackground = MakeImage (brushSettingsGUI, new Rect((Screen.width * 0.03f) - (Screen.width * 0.0f), (Screen.height * 0.245f) - (Screen.height * 0.0f), Screen.width * 0.32f, Screen.width * 0.32f), colorWheelBackgroundSprite);
		colorWheelBackground.name = "ColorWheelBackground";
		colorWheelBackground.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
		colorWheelBackground.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0);
		colorWheelBackground.GetComponent<RectTransform>().anchorMax = new Vector2(0, 0);
		
		// Make color wheel.
		colorWheel = MakeImage (brushSettingsGUI, new Rect((Screen.width * 0.04f) - (Screen.width * 0.0f), (Screen.height * 0.26f) - (Screen.height * 0.0f), Screen.width * 0.3f, Screen.width * 0.3f), colorWheelSprite);
		colorWheel.name = "ColorWheel";
		colorWheel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
		colorWheel.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0);
		colorWheel.GetComponent<RectTransform>().anchorMax = new Vector2(0, 0);
		
		colorWheelCircle = MakeImage (localCanvas, new Rect((Screen.width * 0.2f) - (Screen.width * 0.0f), (Screen.height * 0.5f) - (Screen.height * 0.0f), Screen.width * 0.02f, Screen.width * 0.02f), colorWheelCircleSprite);
		colorWheelCircle.name = "ColorWheelCircle";
		colorWheelCircle.transform.SetParent (brushSettingsGUI.transform);
		//colorWheelCircle.SetActive (false);
		
		// Make swatch explanation text.
		GameObject swatchExplanationLabel = MakeLabel (localCanvas, MCP.Text (2318)/*""*/, new Rect((Screen.width * 0.4175f) - (Screen.width * 0.0f), (Screen.height * 0.08f) - (Screen.height * 0.0f), Screen.width * 0.425f, Screen.height * 0.15f), TextAnchor.UpperLeft);
		swatchExplanationLabel.name = "SwatchExplanationLabel";
		swatchExplanationLabel.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		swatchExplanationLabel.transform.SetParent (brushSettingsGUI.transform);

		// Make swatch background.
		GameObject swatchBackground = MakeImage (brushSettingsGUI, new Rect((Screen.width * 0.4175f) - (Screen.width * 0.0f), (Screen.height * 0.2f) - (Screen.height * 0.0f), Screen.width * 0.25f, Screen.height * 0.375f), menuBackground);
		swatchBackground.name = "SwatchBackground";
		
		// Make swatches.
		colorSwatches = new GameObject[4, 4];
		for(int y = 0; y < 4; y++)
		{
			for(int x = 0; x < 4; x++)
			{
				colorSwatches[x, y] = MakeImage (brushSettingsGUI, new Rect(Screen.width * 0.4375f + (x * Screen.width * 0.055f) - (Screen.width * 0.0f), (Screen.height * 0.24f) - (Screen.height * 0.0f) + (y * Screen.height * 0.075f), Screen.width * 0.05f, Screen.height * 0.07f), null);
				colorSwatches[x, y].name = "ColorSwatch: " + x.ToString () + ", " + y.ToString();
			}
		}
		// Load the swatch colors from file.
		LoadColors ();
		
		// Make color preview background.
		GameObject colorPreviewBackground = MakeImage (brushSettingsGUI, new Rect((Screen.width * 0.025f) - (Screen.width * 0.0f), (Screen.height * 0.84f) - (Screen.height * 0.0f), Screen.width * 0.4f, Screen.height * 0.125f), menuBackground);
		colorPreviewBackground.name = "ColorPreviewBackground";
		// Make new color preview.
		newColorPreview = MakeImage (brushSettingsGUI, new Rect((Screen.width * 0.33f) - (Screen.width * 0.0f), (Screen.height * 0.86f) - (Screen.height * 0.0f), Screen.width * 0.05f, Screen.height * 0.08f), null);
		newColorPreview.name = "NewColorPreview";
		
		Color newlySelectedColor = newColorPreview.GetComponent<Image>().color;
		newColorPreview.GetComponent<Image>().color = newlySelectedColor;
		GameObject newColorPreviewLabel = MakeLabel (brushSettingsGUI, MCP.Text (2310)/*"New"*/, new Rect((Screen.width * 0.235f) - (Screen.width * 0.0f), (Screen.height * 0.87f) - (Screen.height * 0.0f), Screen.width * 0.125f, Screen.height * 0.05f), TextAnchor.MiddleCenter, "SmallText");
		newColorPreviewLabel.name = "NewColorPreviewLabel";
		
		// Make previous color preview.
		prevColorPreview = MakeImage (brushSettingsGUI, new Rect((Screen.width * 0.07f) - (Screen.width * 0.0f), (Screen.height * 0.86f) - (Screen.height * 0.0f), Screen.width * 0.05f, Screen.height * 0.08f), null);
		prevColorPreview.name = "PrevColorPreview";
		prevColorPreview.GetComponent<Image>().color = prevCol;
		GameObject prevColorPreviewLabel = MakeLabel (brushSettingsGUI, MCP.Text (2311)/*"Current"*/, new Rect((Screen.width * 0.11f) - (Screen.width * 0.0f), (Screen.height * 0.87f) - (Screen.height * 0.0f), Screen.width * 0.125f, Screen.height * 0.05f), TextAnchor.MiddleCenter, "SmallText");
		prevColorPreviewLabel.name = "prevColorPreviewLabel";
		
		// Make brush preview background.
		GameObject brushPreviewBackground = MakeImage (brushSettingsGUI, new Rect((Screen.width * 0.675f) - (Screen.width * 0.0f), (Screen.height * 0.2f) - (Screen.height * 0.0f), Screen.width * 0.175f, Screen.height * 0.375f), menuBackground);
		brushPreviewBackground.name = "BrushPreviewBackground";
		
		// Make brush preview text.
		GameObject brushPreviewText = MakeLabel (brushSettingsGUI, MCP.Text (2312)/*"Brush\nPreview"*/, new Rect((Screen.width * 0.6625f) - (Screen.width * 0.0f), (Screen.height * 0.25f) - (Screen.height * 0.0f), Screen.width * 0.2f, Screen.height * 0.1f), TextAnchor.MiddleCenter);
		brushPreviewText.name = "BrushPreviewText";
		
		// Make brush preview.
		brushPreview = MakeImage (brushSettingsGUI, new Rect((Screen.width * 0.7f) - (Screen.width * 0.0f), (Screen.height * 0.35f) - (Screen.height * 0.0f), Screen.width * 0.125f, Screen.height * 0.175f), brushPreviewSprite);
		brushPreview.name = "BrushPreview";
		
		// Make brush size text.
		brushSizeText = MakeLabel (brushSettingsGUI, MCP.Text (2313)/*"Brush Size"*/, new Rect((Screen.width * 0.45f) - (Screen.width * 0.0f), (Screen.height * 0.6f) - (Screen.height * 0.0f), Screen.width * 0.4f, Screen.height * 0.1f), TextAnchor.MiddleLeft);
		brushSizeText.name = "BrushSizeText";
		brushSizeText.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
		
		// Make brush size slider.
		brushSizeSlider = MakeSliderHorizontal (brushSettingsGUI, new Rect((Screen.width * 0.45f) - (Screen.width * 0.0f), (Screen.height * 0.225f) - (Screen.height * 0.0f), Screen.width * 0.475f, Screen.height * 0.06f));
		brushSizeSlider.name = "BrushSizeSlider";
		brushSizeSlider.GetComponent<Slider>().wholeNumbers = true;
		brushSizeSlider.GetComponent<Slider>().minValue = 1f;
		brushSizeSlider.GetComponent<Slider>().maxValue = 10f;
		brushSizeSlider.GetComponent<Slider>().value = brushWidth;
		
//		// Make brush hardness text.
//		brushHardnessText = MakeLabel (brushSettingsGUI, MCP.Text(2314)/*"Brush Hardness"*/, new Rect(Screen.width * 0.15f, Screen.height * -0.18f, Screen.width * 0.4f, Screen.height * 0.1f), TextAnchor.MiddleLeft, "InnerLabel");
//		brushHardnessText.name = "BrushHardnessText";
//		
//		// Make brush hardness slider.
//		brushHardnessSlider = MakeSliderHorizontal (brushSettingsGUI, new Rect(Screen.width * -0.05f, Screen.height * 0.745f, Screen.width * 0.475f, Screen.height * 0.04f));
//		brushHardnessSlider.name = "BrushHardnessSlider";
//		brushHardnessSlider.GetComponent<Slider>().wholeNumbers = true;
//		brushHardnessSlider.GetComponent<Slider>().minValue = 1f;
//		brushHardnessSlider.GetComponent<Slider>().maxValue = 100f;
//		brushHardnessSlider.GetComponent<Slider>().value = 100f;
		
		// Make brush opacity text.
		brushOpacityText = MakeLabel (brushSettingsGUI, MCP.Text (2315)/*"Brush Opacity"*/, new Rect((Screen.width * 0.45f) - (Screen.width * 0.0f), (Screen.height * 0.77f) - (Screen.height * 0.0f), Screen.width * 0.4f, Screen.height * 0.1f), TextAnchor.MiddleLeft);
		brushOpacityText.name = "BrushOpacityText";
		brushOpacityText.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
		
		// Make brush opacity slider.
		brushOpacitySlider = MakeSliderHorizontal (brushSettingsGUI, new Rect((Screen.width * 0.45f) - (Screen.width * 0.0f), (Screen.height * 0.4f) - (Screen.height * 0.0f), Screen.width * 0.475f, Screen.height * 0.06f));
		brushOpacitySlider.name = "BrushOpacitySlider";
		brushOpacitySlider.GetComponent<Slider>().minValue = 0f;
		brushOpacitySlider.GetComponent<Slider>().maxValue = 255f;
		brushOpacitySlider.GetComponent<Slider>().value = 255f;
		
		// Make cancel button label.
		GameObject settingsCancelButtonLabel = MakeLabel (brushSettingsGUI, MCP.Text (207)/*"Cancel"*/, new Rect((Screen.width * 0.88f) - (Screen.width * 0.0f), (Screen.height * 0.46f) - (Screen.height * 0.0f), Screen.width * 0.08f, Screen.height * 0.08f), TextAnchor.MiddleCenter, "InnerLabel");
		settingsCancelButtonLabel.name = "SettingsCancelButtonLabel";
		settingsCancelButtonLabel.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Overflow;
		
		// Make cancel button.
		funcList = new List<MyVoidFunc>
		{
			RevertBrushChanges,
			delegate
			{
				// Enable the brush settings GUI.
				brushSettingsGUI.SetActive(false);
				
				// Disable the main screen GUI.
				mainScreenGUI.SetActive(true);
				canvasCamera.SetActive(true);
			}
		};
		GameObject settingsCancelButton = MakeButton (brushSettingsGUI, "", clearButton[0], new Rect((Screen.width * 0.88f) - (Screen.width * 0.0f), (Screen.height * 0.35f) - (Screen.height * 0.0f), Screen.width * 0.08f, Screen.width * 0.08f), funcList, "InnerLabel");
		settingsCancelButton.name = "SettingsCancelButton";
		// Set the button navigation mode.
		settingsCancelButton.GetComponent<Button>().navigation = nav;
		settingsCancelButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		settingsCancelButton.GetComponent<Button>().targetGraphic = settingsCancelButton.transform.GetChild (0).GetComponent<Image>();
		spriteState.highlightedSprite = clearButton[1];
		spriteState.pressedSprite = clearButton[1];
		settingsCancelButton.GetComponent<Button>().spriteState = spriteState;
		
		// Make done button label.
		GameObject settingsDoneButtonLabel = MakeLabel (brushSettingsGUI, MCP.Text (222)/*"Done"*/, new Rect((Screen.width * 0.88f) - (Screen.width * 0.0f), (Screen.height * 0.26f) - (Screen.height * 0.0f), Screen.width * 0.08f, Screen.height * 0.08f), TextAnchor.MiddleCenter, "InnerLabel");
		settingsDoneButtonLabel.name = "SettingsDoneButtonLabel";
		
		// Make done button.
		funcList = new List<MyVoidFunc>
		{
			SaveBrushChanges,
			delegate
			{
				// Enable the brush settings GUI.
				brushSettingsGUI.SetActive(false);
				
				// Disable the main screen GUI.
				mainScreenGUI.SetActive(true);
				canvasCamera.SetActive(true);
			}
		};
		GameObject settingsSaveButton = MakeButton (brushSettingsGUI, "", saveButton[0], new Rect((Screen.width * 0.88f) - (Screen.width * 0.0f), (Screen.height * 0.15f) - (Screen.height * 0.0f), Screen.width * 0.08f, Screen.width * 0.08f), funcList, "InnerLabel");
		settingsSaveButton.name = "SettingsSaveButton";
		// Set the button navigation mode.
		settingsSaveButton.GetComponent<Button>().navigation = nav;
		settingsSaveButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		settingsSaveButton.GetComponent<Button>().targetGraphic = settingsSaveButton.transform.GetChild (0).GetComponent<Image>();
		spriteState.highlightedSprite = saveButton[1];
		spriteState.pressedSprite = saveButton[1];
		settingsSaveButton.GetComponent<Button>().spriteState = spriteState;
	}
	
	/// <summary>
	/// Changes the draw mode.
	/// </summary>
	private void ChangeDrawMode()
	{
		if(moveZoomActive)
		{
			ChangeNavigationMode();
		}
		
		currentDrawMode++;
		if(currentDrawMode >= drawModeSprites.Length)
		{
			currentDrawMode = 0;
		}
		
		tool = (Tool)(currentDrawMode + 1);
		
		drawModeButton.GetComponent<Image>().sprite = drawModeSprites[currentDrawMode];
		
		SpriteState spriteState = new SpriteState();
		spriteState.highlightedSprite = drawModeSpritesHighlighted[currentDrawMode];
		spriteState.pressedSprite = drawModeSprites[currentDrawMode];
		spriteState.disabledSprite = drawModeSprites[currentDrawMode];
		
		drawModeButton.GetComponent<Button>().spriteState = spriteState;
		drawModeButton.GetComponent<Button>().targetGraphic = drawModeButton.GetComponentInChildren<Image>();
	}
	
	/// <summary>
	/// Changes the navigation mode.
	/// </summary>
	private void ChangeNavigationMode()
	{
		// Change the moveZoomActive flag.
		moveZoomActive = !moveZoomActive;
		
		// Change the sprite depending on whether this is active or not.
		if(moveZoomActive)
		{
			navigationModeButton.GetComponent<Image>().sprite = navigationModeSprites[1];
		}
		else
		{
			navigationModeButton.GetComponent<Image>().sprite = navigationModeSprites[0];
		}
		
		// Turn off overlay navigation.
		if(overlayState == OverlayState.Move)
		{
			// Change the overlay to stay on but not moving.
			overlayState = OverlayState.On;
			overlayImage.SetActive (true);
			overlayModeButton.GetComponent<Image>().sprite = overlaySprites[1];
		}
	}
	
	/// <summary>
	/// Toggles the overlay.
	/// </summary>
	private void ToggleOverlay()
	{
		switch(overlayState)
		{
		case OverlayState.Off:
			overlayState = OverlayState.On;
			overlayImage.SetActive (true);
			overlayModeButton.GetComponent<Image>().sprite = overlaySprites[1];
			break;
			
		case OverlayState.On:
			// Turn off the standard navigation.
			if(moveZoomActive)
			{
				ChangeNavigationMode();
			}
			
			overlayState = OverlayState.Move;
			overlayModeButton.GetComponent<Image>().sprite = overlaySprites[2];
			
			break;
		case OverlayState.Move:
			overlayState = OverlayState.Off;
			overlayImage.SetActive (false);
			overlayModeButton.GetComponent<Image>().sprite = overlaySprites[0];
			
			break;
		}
		
	}
	
	/// <summary>
	/// Undoes the action.
	/// </summary>
	private void UndoAction()
	{
		// Set the current canvas texture to the backup texture.
		canvasTex.SetPixels (canvasBackups[1].GetPixels ());
		canvasTex.Apply ();
		
		// Move each backup up the array.
		for(int i = 0; i < canvasBackups.Length - 1; i++)
		{
			if(canvasBackups[i + 1] != null)
			{
				canvasBackups[i].SetPixels(canvasBackups[i + 1].GetPixels ());
			}
		}
	}
	
	/// <summary>
	/// Opens the save dialog.
	/// </summary>
	private void OpenSaveDialog()
	{
		saveDialogUp = true;
		// Disable the canvas camera.
		canvasCamera.GetComponent<Camera>().enabled = false;
		// Make background panel.
		GameObject dialogBackgroundPanel = MakeImage (localCanvas, new Rect(Screen.width * -0.0f, Screen.height * -0.0f, Screen.width, Screen.height), menuBackground);
		dialogBackgroundPanel.name = "DialogBackgroundPanel";
		// Make panel title.
		GameObject dialogTitle = MakeLabel (localCanvas, MCP.Text (2316)/*"Save Picture"*/, new Rect(Screen.width * 0.3f, Screen.height * 0.1f, Screen.width * 0.35f, Screen.height  * 0.1f), "InnerLabel");
		dialogTitle.name = "DialogTitle";
		// Make title subtext.
		GameObject dialogText = MakeLabel (localCanvas, MCP.Text (2319)/*"Warning: Any pictures with the same name will be overwritten."*/, new Rect(Screen.width * 0.15f, Screen.height * 0.275f, Screen.width * 0.7f, Screen.height  * 0.1f), "SmallText");
		dialogText.GetComponent<Text>().alignment = TextAnchor.UpperCenter;
		dialogText.name = "DialogText";
		// Make input field for name.
		dialogInput = MakeInputField(localCanvas, saveFileName, new Rect(Screen.width * 0.2f, Screen.height * 0.4f, Screen.width * 0.6f, Screen.height  * 0.1f), false, buttonBackground);
		dialogInput.name = "DialogInput";
		
		GameObject dialogSaveButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.65f, Screen.height * 0.6f, Screen.width * 0.15f, Screen.height  * 0.1f), buttonBackground, true);
		dialogSaveButtonBackground.name = "DialogSaveButtonBackground";
		// Make save button.
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			SaveCanvas
		};
		GameObject dialogSaveButton = MakeButton (localCanvas, MCP.Text (215)/*"Save"*/, new Rect(Screen.width * 0.65f, Screen.height * 0.6f, Screen.width * 0.15f, Screen.height  * 0.1f), funcList, "InnerLabel");
		dialogSaveButton.name = "DialogSaveButton";
		dialogSaveButton.GetComponent<Text>().color = bmOrange;
		dialogSaveButton.GetComponent<Button>().image = dialogSaveButtonBackground.GetComponent<Image>();
		
		GameObject dialogDontSaveButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.425f, Screen.height * 0.6f, Screen.width * 0.15f, Screen.height  * 0.1f), buttonBackground, true);
		dialogDontSaveButtonBackground.name = "DialogDontSaveButtonBackground";
		// Make save button.
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				MCP.loadedTexture = null;
				Close ();
			}
		};
		GameObject dialogDontSaveButton = MakeButton (localCanvas, MCP.Text (2317)/*"Don't Save"*/, new Rect(Screen.width * 0.425f, Screen.height * 0.6f, Screen.width * 0.15f, Screen.height  * 0.1f), funcList, "InnerLabel");
		dialogDontSaveButton.name = "DialogDontSaveButton";
		dialogDontSaveButton.GetComponent<Text>().color = bmOrange;
		dialogDontSaveButton.GetComponent<Button>().image = dialogDontSaveButtonBackground.GetComponent<Image>();
		
		GameObject dialogCancelButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.2f, Screen.height * 0.6f, Screen.width * 0.15f, Screen.height  * 0.1f), buttonBackground, true);
		dialogCancelButtonBackground.name = "DialogCancelButtonBackground";
		
		// Make cancel button.
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				saveDialogUp = false;
				
				Destroy(dialogBackgroundPanel);
				Destroy(dialogInput.transform.parent.gameObject);
				Destroy (dialogDontSaveButtonBackground);
				Destroy(dialogDontSaveButton);
				Destroy (dialogSaveButtonBackground);
				Destroy (dialogSaveButton);
				Destroy(dialogTitle);
				Destroy(dialogText);
				Destroy (dialogCancelButtonBackground);
				
				// Reenable the canvas camera.
				canvasCamera.GetComponent<Camera>().enabled = true;
			}
		};
		GameObject dialogCancelButton = MakeButton (localCanvas, MCP.Text (207)/*"Cancel"*/, new Rect(Screen.width * 0.2f, Screen.height * 0.6f, Screen.width * 0.15f, Screen.height  * 0.1f), funcList, "InnerLabel");
		dialogCancelButton.GetComponent<Text>().color = bmOrange;
		dialogCancelButton.GetComponent<Button>().image = dialogCancelButtonBackground.GetComponent<Image>();
		dialogCancelButton.GetComponent<Button>().onClick.AddListener(
			delegate
			{
				Destroy (dialogCancelButton);
			});
	}
	
	/// <summary>
	/// Saves the canvas.
	/// </summary>
	private void SaveCanvas()
	{
		// Get the date and time to add to the file name.
		string date = System.DateTime.Now.ToShortTimeString() + "_" + System.DateTime.Now.ToShortDateString();
		// Remove invalid characters.
		date = date.Replace ('/', '-');
		date = date.Replace (" ", "");
		date = date.Replace (':', '.');
		
#if !UNITY_WEBPLAYER
		saveFileName = dialogInput.GetComponent<InputField>().text;
		
		// Save the canvas to a new file.
		string filePath = Application.persistentDataPath + "/" + saveFileName + ".png";
		// Store the file.
		byte[] file = canvasTex.EncodeToPNG ();
		
		// Initialise the file streams.
		FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write);
		BinaryWriter writer = new BinaryWriter(fs);
		// Write the file.
		writer.Write (file);
		// Close file streams.
		writer.Close ();
		fs.Close ();
#endif
		
		// Store the texture.
		MCP.loadedTexture = canvasTex;
		
		// Close the painter.
		Close ();
	}
	
	/// <summary>
	/// Clears the canvas.
	/// </summary>
	private void ClearCanvas()
	{
		// Check if there is a picture to edit to put on the background plane.
		if(MCP.loadedTexture == null)
		{
			edittingImage = false;
		}
		
		// Get the current pixels.
		if(edittingImage)
		{
			canvasTex.Resize (MCP.loadedTexture.width, MCP.loadedTexture.height);
		}
		
		// Get the canvas pixels.
		Color[] pixels = canvasTex.GetPixels();
		
		// If the user is editting an image...
		if(edittingImage)
		{
			// Make a texture2D from the loaded texture.
			Texture2D tex = new Texture2D( MCP.loadedTexture.width, MCP.loadedTexture.height, TextureFormat.RGBA32, false);
			tex.SetPixels (MCP.loadedTexture.GetPixels ());
			tex.Apply ();
			
			// Get the pixels from the loaded texture.
			Color[] loadedPixels = tex.GetPixels();
			
			// Set the correct number of pixels to the initial texture.
			for (int i = 0; i < pixels.Length; i++)
			{
				pixels[i] = loadedPixels[i];
			}
		}
		// Otherwise...
		else
		{
			// Set the correct number of pixels to white.
			for (int i = 0; i < pixels.Length; i++)
			{
				pixels[i] = Color.white;
			}
		}
		
		// Apply the new pixels to the canvas textures.
		backedUp = false;
		canvasTex.SetPixels (pixels);
		canvasTex.Apply();
	}
	
	/// <summary>
	/// Overlays the settings.
	/// </summary>
	private void GetOverlayImage()
	{
		// Disable the canvas camera.
		MenuScreen.firstFrameTimer = 0;
		canvasCamera.SetActive (false);
		
#if UNITY_WEBPLAYER
		// Add the picture gallery script and set its variables.
		Camera.main.gameObject.AddComponent<PictureGallery>();
		Camera.main.gameObject.GetComponent<PictureGallery>().sceneCanvas = localCanvas;
		Camera.main.gameObject.GetComponent<PictureGallery>().isClipArt = true;
		
		// Flag the overlay
		gettingOverlay = true;
#else
		// Add the file browser
		localCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
		Camera.main.gameObject.AddComponent<FileBrowser>();
		Camera.main.gameObject.GetComponent<FileBrowser>().currentFilePath = Application.persistentDataPath;
		Camera.main.gameObject.GetComponent<FileBrowser>().upPathLimit = Application.persistentDataPath;
		Camera.main.gameObject.GetComponent<FileBrowser>().allowedExtensions = new string[]{"jpg", "png"};
		Camera.main.gameObject.GetComponent<FileBrowser>().sceneCanvas = localCanvas;
#endif
	}
	
	/// <summary>
	/// Reverts the brush changes.
	/// </summary>
	private void RevertBrushChanges()
	{
		// Reset the brush attributes.
		brushWidth = oldBrushWidth;
		brushHardness = oldBrushHardness;
		brushOpacity = oldBrushOpacity;
		
		// Set the sliders to the old settings.
		brushSizeSlider.GetComponent<Slider>().value = brushWidth;
//		brushHardnessSlider.GetComponent<Slider>().value = brushHardness;
		brushOpacitySlider.GetComponent<Slider>().value = brushOpacity;
		
		// Reset color.
		col = prevCol;
		
		// Save any changes to the swatches.
		SaveColors ();
	}
	
	/// <summary>
	/// Saves the brush changes.
	/// </summary>
	private void SaveBrushChanges()
	{
		// Save the new brush attributes to the backup variables.
		oldBrushWidth = brushWidth;
		oldBrushHardness = brushHardness;
		oldBrushOpacity = brushOpacity;
		prevCol = col;
		
		// Save any changes to the swatches.
		SaveColors ();
	}
	
	/// <summary>
	/// Backups the canvas.
	/// </summary>
	private void BackupCanvas()
	{
		// Move the backup textures one element down the array.
		for(int i = canvasBackups.Length-1; i > 0; i--)
		{
			canvasBackups[i].SetPixels (canvasBackups[i - 1].GetPixels ());
		}
		
		// Set the first backup to the current canvas.
		canvasBackups[0].SetPixels (canvasTex.GetPixels ());
		
		// Flag the backup.
		backedUp = true;
	}
	
	/// <summary>
	/// Paints a square on the canvas.
	/// </summary>
	/// <param name="x">The x coordinate.</param>
	/// <param name="y">The y coordinate.</param>
	/// <param name="col">Color.</param>
	private void BrushSquare(int x, int y, Color col)
	{
		// Set the pixel that has been clicked to the requested color.
		Color pixel = canvasTex.GetPixel (x, y);
		
		// Make brush hardness take alpha into account.
		brushHardness = (brushOpacity / 255f) * 100f;
		pixel = Color.Lerp (pixel, col, brushHardness * 0.001f);
		
		canvasTex.SetPixel (x, y, pixel);
//		canvasTex.SetPixel (x, y, col);
		
		float edgeOffset = 0.2f;
		
		// Apply brush size.
		for(int xOff = 0; xOff < (int)brushWidth; xOff++)
		{
			for(int yOff = 0; yOff < (int)brushWidth; yOff++)
			{
				if(x + xOff < canvasTex.width * (1.0f - edgeOffset))
				{
					if(y + yOff < canvasTex.height * (1.0f - edgeOffset))
					{
						pixel = canvasTex.GetPixel (x + xOff, y + yOff);
						
						pixel = Color.Lerp (pixel, col, brushHardness * 0.001f);
						
						canvasTex.SetPixel (x + xOff, y + yOff, pixel);
//						canvasTex.SetPixel (x + xOff, y + yOff, col);
					}
					if(y - yOff > canvasTex.height * edgeOffset)
					{
						pixel = canvasTex.GetPixel (x + xOff, y - yOff);
						
						pixel = Color.Lerp (pixel, col, brushHardness * 0.001f);
						
//						canvasTex.SetPixel (x + xOff, y - yOff, col);
						canvasTex.SetPixel (x + xOff, y - yOff, pixel);
					}
				}
				if(x - xOff > canvasTex.width * edgeOffset)
				{
					if(y + yOff < canvasTex.height * (1.0f - edgeOffset))
					{
						pixel = canvasTex.GetPixel (x - xOff, y + yOff);
						
						pixel = Color.Lerp (pixel, col, brushHardness * 0.001f);
						
//						canvasTex.SetPixel (x - xOff, y + yOff, col);
						canvasTex.SetPixel (x - xOff, y + yOff, pixel);
					}
					if(y - yOff > canvasTex.height * edgeOffset)
					{
						pixel = canvasTex.GetPixel (x - xOff, y - yOff);
						
						pixel = Color.Lerp (pixel, col, brushHardness * 0.001f);
						
						canvasTex.SetPixel (x - xOff, y - yOff, pixel);
					}
				}
			}
		}
		
		pixel = Color.white;
	}
	
	/// <summary>
	/// Paints from point to point.
	/// </summary>
	/// <param name="start">Start.</param>
	/// <param name="end">End.</param>
	/// <param name="col">Color.</param>
	private void BrushPointToPoint(Vector2 start, Vector2 end, Color col)
	{
		// If the start point is not valid, return.
		if(start == Vector2.zero)
		{
			return;
		}
		
		// If the end is not valid.
		if(end == Vector2.zero)
		{
			// Set the end point to the start point.
			end = start;
		}
	
		// Get the length of the line.
		float lengthX, lengthY;
		lengthX = end.x - start.x;
		lengthY = end.y - start.y;
		
		// Draw the color through 1000 points of this line.
		for(int i = 0; i < 100; i++)
		{
			Vector2 uv = start;
			// Get the position of this step on the line.
			uv.x += (lengthX / 100 * i);
			uv.y += (lengthY / 100 * i);
			
			uv.x *= canvasTex.width;
			uv.y *= canvasTex.height;
			// Apply the brush to the canvas.
			col.a = 1;
			BrushSquare ((int)-uv.x, (int)uv.y, col);
		}
		
		// Apply the change to the texture.
		canvasTex.Apply ();
	}

	private void GuidePointToPoint(Vector2 start, Vector2 end, Color col)
	{
		Vector3 worldStart = new Vector3 (start.x, start.y, 139f);

		Vector3 worldEnd = new Vector3 (end.x, end.y, 139f);

		guideBrushParent.name = "Line Parent";
		guideBrushParent.transform.position = start;
		guideBrushLine.SetVertexCount (2);
		guideBrushLine.SetWidth (3f, 3f);
		guideBrushLine.SetPosition (0, worldStart);
		guideBrushLine.SetPosition (1, worldEnd);
	}
	
	/// <summary>
	/// Paints onto the canvas.
	/// </summary>
	/// <param name="pointer">Pointer position.</param>
	/// <param name="pressed">If set to <c>true</c> pressed.</param>
	/// <param name="held">If set to <c>true</c> held.</param>
	/// <param name="unpressed">If set to <c>true</c> unpressed.</param>
	private void DrawBrush(Vector2 pointer, bool pressed, bool held, bool unpressed)
	{
		// If not in brush settings
		if (mainScreenGUI.activeSelf)
		{
			// Null check the canvas texture and the canvas camera.
			if (canvasTex != null && GameObject.Find ("CanvasCamera") != null)
			{
				// Get the ray from camera to the screen mouse position
				Ray ray = GameObject.Find ("CanvasCamera").GetComponent<Camera> ().ScreenPointToRay (Input.mousePosition);
				RaycastHit hit;
				
				Vector2 uv = Vector2.zero;
				// If the mouse button is held or the screen is touched...
				if (held)
				{
					// Check for any collisions from the ray.
					if (Physics.Raycast (ray, out hit, 300f))
					{
						// Find the u,v coordinate of the Texture
						uv.x = -(hit.point.x - hit.collider.bounds.min.x) / hit.collider.bounds.size.x;
						uv.y = (hit.point.y - hit.collider.bounds.min.y) / hit.collider.bounds.size.y;
						
						// Set the start point.
						if(pressed)
						{
							dragStart = uv;
						}
					}
					
					if(!pressed)
					{
						// Join to the previous pixel.
						if(tool == Tool.Brush)
						{
							BrushPointToPoint (uv, preDrag, col);
						}
						if(tool == Tool.Eraser)
						{
							BrushPointToPoint (uv, preDrag, Color.white);
						}
						// If using the line tool, draw the line.
						if(tool == Tool.Line)
						{
							if(!guideBrushClicked)
							{
								guideBrushOrigStart = new Vector2(hit.point.x, hit.point.y);
								guideBrushClicked = true;
							}
							//BrushPointToPoint (preDrag, dragStart, col);
							guideBrushLine.material.color = col;
							GuidePointToPoint (guideBrushOrigStart, guideBrushPrevEnd, col);
						}
					}
					
					canvasTex.Apply ();
					guideBrushPrevEnd = new Vector2(hit.point.x, hit.point.y);
					preDrag = uv;
				}
				// When pointer is released...
				if(unpressed)
				{
					// if a line has been drawn, reset the backedUp flag.
					if(preDrag != Vector2.zero)
					{
						backedUp = false;
					}
					
					// If using the line tool, draw the line.
					if(tool == Tool.Line)
					{
						guideBrushLine.SetPosition(0, Vector3.zero);
						guideBrushLine.SetPosition(1, Vector3.zero);
						guideBrushClicked = false;
						BrushPointToPoint (preDrag, dragStart, col);
					}
					
					// Reset the positions.
					preDrag = Vector2.zero;
					dragStart = Vector2.zero;
				}
				
#if !UNITY_ANDROID && !UNITY_IPHONE
				if (Physics.Raycast (ray, out hit, 300f))
				{
					if(tool == Tool.Eraser)
					{
						Cursor.SetCursor (eraseTex, Vector2.zero, CursorMode.Auto);
					}
					else
					{
						Cursor.SetCursor (drawTex, Vector2.zero, CursorMode.Auto);
					}
				}
				else if(imgRect.Contains (Input.mousePosition))
				{
					Cursor.SetCursor (null, Vector2.zero, CursorMode.Auto);
				}
#endif
			}
		}
	}
	
	/// <summary>
	/// Moves the canvas.
	/// </summary>
	/// <param name="pointer">Pointer position.</param>
	/// <param name="pressed">If set to <c>true</c> pressed.</param>
	/// <param name="held">If set to <c>true</c> held.</param>
	/// <param name="unpressed">If set to <c>true</c> unpressed.</param>
	public void MoveCanvas(Vector2 pointer, bool pressed, bool held, bool unpressed)
	{
		// Store the current position of the camera.
		Vector3 newPos = canvasObject.transform.localPosition;
		
		// If this is the initial press.
		if(pressed)
		{
			// If the pointer is within the canvas area.
			if(imgRect.Contains (Input.mousePosition))
			{
				// Set the drag start to the initial camera position.
				dragStart = canvasObject.transform.localPosition;
				// Get the starting position of the pointer.
				startPan = pointer;
			}
			else
			{
				startPan = Vector2.zero;
			}
		}
		
		// If the mouse pointer is held...
		if(held && startPan != Vector2.zero)
		{
			// Get the offset between the current mouse position and the new mouse position.
			newPos.x = (startPan.x - pointer.x) * 0.2f;
			newPos.y = (startPan.y - pointer.y) * 0.2f;
			// Set this offset to the camera position.
			newPos = new Vector3(dragStart.x - newPos.x, dragStart.y - newPos.y, 0);
			
			// Make sure the camera does not move off the canvas.
			if(newPos.x < -(150f - zoom))
			{
				newPos.x = -(150f - zoom);
			}
			
			if(newPos.x > 150f - zoom)
			{
				newPos.x = 150f - zoom;
			}
			
			if(newPos.y < -(150f - zoom))
			{
				newPos.y = -(150f - zoom);
			}
			
			if(newPos.y > 150f - zoom)
			{
				newPos.y = 150f - zoom;
			}
			
			// Make sure the z position stays the same.
			newPos.z = zoom;
			
			// Set the new camera position.
			canvasObject.transform.localPosition = newPos;
		}
		
		// If the pointer has been released...
		if(unpressed)
		{
			// Reset the initial positions.
			dragStart = Vector2.zero;
			startPan = Vector2.zero;
		}
	}
	
	/// <summary>
	/// Zooms the canvas in using the scroll wheel.
	/// </summary>
	/// <param name="zoomAmount">Zoom amount.</param>
	public void ScrollZoomCanvas(float zoomAmount)
	{
		zoom -= zoomAmount * 10f;
		
		// Round zoom to 2dp.
		zoom = Mathf.Round (zoom * 100f) / 100f;
		
		// Keep the zoom within set bounds.
		if(zoom < 50.0f)
		{
			zoom = 50.0f;
		}
		
		if(zoom > 150f)
		{
			zoom = 150f;
		}
		
		// Set the size to the new zoom.
		Vector3 newPos = canvasObject.transform.localPosition;
		newPos.z = zoom;
		canvasObject.transform.localPosition = newPos;
	}
	
	/// <summary>
	/// Zooms the canvas.
	/// </summary>
	/// <param name="pointer">Pointer position.</param>
	/// <param name="pressed">If set to <c>true</c> pressed.</param>
	/// <param name="held">If set to <c>true</c> held.</param>
	/// <param name="unpressed">If set to <c>true</c> unpressed.</param>
	public void ZoomCanvas(Vector2 pointer, bool pressed, bool held, bool unpressed)
	{
		// If the pointer is within the canvas area.
		if(imgRect.Contains (Input.mousePosition))
		{
			// If this is the initial press.
			if(pressed)
			{
				// Set the drag start to the current pointer position.
				dragStart = pointer;
				// Set the initial zoom to the current zoom.
				startZoom = zoom;
			}
		}
		
		// If the pointer is released...
		if(unpressed)
		{
			// Reset the start position.
			dragStart = Vector2.zero;
		}
		
		// If the mouse is held and the drag began within the canvas.
		if(held && dragStart != Vector2.zero)
		{
			// Set the zoom to the difference between the current position and the initial zoom.
			zoom = startZoom + ((dragStart.y - pointer.y) / 10f);
		}
		
		// Round zoom to 2dp.
		zoom = Mathf.Round (zoom * 100f) / 100f;
		
		// Keep the zoom within set bounds.
		if(zoom < 50.0f)
		{
			zoom = 50.0f;
		}
		
		if(zoom > 150f)
		{
			zoom = 150f;
		}
		
		// Set the size to the new zoom.
		Vector3 newPos = canvasObject.transform.localPosition;
		newPos.z = zoom;
		canvasObject.transform.localPosition = newPos;
	}
	
	/// <summary>
	/// Zooms the canvas on touch devices.
	/// </summary>
	/// <param name="touches">Touches.</param>
	public void ZoomCanvasTouch(Touch[] touches)
	{
		// If the pointer is within the canvas.
		if(imgRect.Contains (touches[0].position))
		{
			// If the second touch has just begun...
			if(touches[1].phase == TouchPhase.Began)
			{
				// Set the initial distance between the touches.
				startDist = Vector2.Distance (touches[0].position, touches[1].position);
				// Set the start zoom to the current zoom.
				startZoom = zoom;
			}
		}
		// If the touch was not within the canvas...
		//if(dragStart != Vector2.zero)
		{
			// Set the zoom to the difference between the current position and the initial zoom.
			zoom = startZoom + ((startDist - Vector2.Distance (touches[0].position, touches[1].position)) / 20f);
		}
		
		// Round zoom to 2dp.
		zoom = Mathf.Round (zoom * 100f) / 100f;
		
		// Keep the zoom bound within limits.
		if(zoom < 50.0f)
		{
			zoom = 50.0f;
		}
		
		if(zoom > 150f)
		{
			zoom = 150f;
		}
		
		Vector3 newPos = canvasObject.transform.localPosition;
		newPos.z = zoom;
		canvasObject.transform.localPosition = newPos;
	}
	
	/// <summary>
	/// Moves the overlay.
	/// </summary>
	/// <param name="pointer">Pointer position.</param>
	/// <param name="pressed">If set to <c>true</c> pressed.</param>
	/// <param name="held">If set to <c>true</c> held.</param>
	/// <param name="unpressed">If set to <c>true</c> unpressed.</param>
	void MoveOverlay(Vector2 pointer, bool pressed, bool held, bool unpressed)
	{
		// Store the current position of the camera.
		Vector3 newPos = overlayImage.transform.position;
		
		// If the pointer is within the canvas area.
		if(imgRect.Contains (Input.mousePosition))
		{
			// If this is the initial press.
			if(pressed)
			{
				// Set the drag start to the initial camera position.
				dragStart = overlayImage.transform.position;
				// Get the starting position of the pointer.
				startPan = pointer;
			}
		}
		else
		{
			// If this is the initial press.
			if(pressed)
			{
				startPan = Vector2.zero;
			}
		}
		
		// If the mouse pointer is held...
		if(held && startPan != Vector2.zero)
		{
			// Get the offset between the current mouse position and the new mouse position.
			newPos.x = (startPan.x - pointer.x) * 0.2f;
			newPos.y = (startPan.y - pointer.y) * 0.2f;
			// Set this offset to the camera position.
			newPos = new Vector3(dragStart.x, dragStart.y, 0) - newPos;
			
//			float xBand = 1.0f - (Screen.width * overlayZoom);
//			float yBand = 1.0f - (Screen.height * overlayZoom);

			float xBand = overlayTex.width * 0.2f;
			float yBand = overlayTex.height * 0.1f;
			
			// Make sure the camera does not move off the canvas.
			if(newPos.x < -xBand)
			{
				newPos.x = -xBand;
			}
			
			if(newPos.x > xBand)
			{
				newPos.x = xBand;
			}
			
			if(newPos.y < -yBand)
			{
				newPos.y = -yBand;
			}
			
			if(newPos.y > yBand)
			{
				newPos.y = yBand;
			}
			
			// Set the new camera position.
			overlayImage.transform.position = newPos;
			
			newPos = overlayImage.transform.localPosition;
			newPos.z = overlayZ;
			overlayImage.transform.localPosition = newPos;
		}
		
		// If the pointer has been released...
		if(unpressed)
		{
			// Reset the initial positions.
			dragStart = Vector2.zero;
			startPan = Vector2.zero;
		}
	}
	
	/// <summary>
	/// Zooms the overlay.
	/// </summary>
	/// <param name="pointer">Pointer position.</param>
	/// <param name="pressed">If set to <c>true</c> pressed.</param>
	/// <param name="held">If set to <c>true</c> held.</param>
	/// <param name="unpressed">If set to <c>true</c> unpressed.</param>
	void ZoomOverlay(Vector2 pointer, bool pressed, bool held, bool unpressed)
	{
		// If the pointer is within the canvas area.
		if(imgRect.Contains (Input.mousePosition))
		{
			// If this is the initial press.
			if(pressed)
			{
				// Set the drag start to the current pointer position.
				dragStart = pointer;
				// Set the initial zoom to the current zoom.
				startOverlayZoom = overlayZoom;
			}
		}
		
		// If the pointer is released...
		if(unpressed)
		{
			// Reset the start position.
			dragStart = Vector2.zero;
		}
		
		// If the mouse is held and the drag began within the canvas.
		if(held && dragStart != Vector2.zero)
		{
			// Set the zoom to the difference between the current position and the initial zoom.
			overlayZoom = startOverlayZoom + ((dragStart.y - pointer.y) / 200f);
		}
		
		// Round zoom to 2dp.
		overlayZoom = Mathf.Round (overlayZoom * 100f) / 100f;
		
		// Keep the zoom within set bounds.
		if(overlayZoom < 0.05f)
		{
			overlayZoom = 0.05f;
		}
		
		if(overlayZoom > 0.5f)
		{
			overlayZoom = 0.5f;
		}
		
		// Set the size to the new zoom.
		overlayImage.transform.localScale = new Vector3(overlayZoom, overlayZoom, 1);
	}
	
	/// <summary>
	/// Zooms the overlay on touch devices.
	/// </summary>
	/// <param name="touches">Touches.</param>
	void ZoomOverlayTouch(Touch[] touches)
	{
		// If the pointer is within the canvas.
		if(imgRect.Contains (touches[0].position))
		{
			// If the second touch has just begun...
			if(touches[1].phase == TouchPhase.Began)
			{
				// Set the initial distance between the touches.
				startDist = Vector2.Distance (touches[0].position, touches[1].position);
				// Set the start zoom to the current zoom.
				startOverlayZoom = overlayZoom;
			}
		}
		
		// Set the zoom to the difference between the current position and the initial zoom.
		overlayZoom = startOverlayZoom + ((startDist - Vector2.Distance (touches[0].position, touches[1].position)) / 200f);
		
		// Round zoom to 2dp.
		overlayZoom = Mathf.Round (overlayZoom * 100f) / 100f;
		
		// Keep the zoom bound within limits.
		if(overlayZoom < 0.1f)
		{
			overlayZoom = 0.1f;
		}
		
		if(overlayZoom > 1f)
		{
			overlayZoom = 1f;
		}
		
		overlayImage.transform.localScale = new Vector3(overlayZoom, overlayZoom, 1);
	}
	
	private bool IsWithinCircle(Rect surroundingRect, Vector2 pointer)
	{
		float radius = surroundingRect.width * 0.5f;
		
		// If the pointer position is within the radius of the center point of the rect, it is within the circle.
		if((surroundingRect.center - pointer).magnitude < radius)
		{
			return true;
		}
		
		return false;
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	public void Update()
	{
		// Update the slider values.
		brushWidth = brushSizeSlider.GetComponent<Slider>().value;
		brushSizeText.GetComponent<Text>().text = MCP.Text(2313)/*"Brush Size"*/ + ": " + brushWidth;
//		brushHardness = brushHardnessSlider.GetComponent<Slider>().value;
//		brushHardnessText.GetComponent<Text>().text = MCP.Text (2314) /*"Brush Hardness"*/ + ": " + brushHardness;
		brushOpacity = brushOpacitySlider.GetComponent<Slider>().value;
		brushOpacityText.GetComponent<Text>().text = MCP.Text (2315)/*"Brush Opacity"*/ + ": " + (int)brushOpacity;
		col.a = brushOpacity / 255f;
		
		// Get the size of the visable canvas.
		imgRect = canvasCamera.GetComponent<Camera>().pixelRect;
		imgRect.x = (Screen.width - imgRect.width) * 0.5f;
		imgRect.y = (Screen.height - imgRect.height) * 0.5f;
		
		// Get mouse position.
		Vector2 mouse = Input.mousePosition;
		
		Vector2 touchPos = Vector2.zero;
		
		// Get the first touch position.
		if(Input.touches.Length > 0)
		{
			touchPos = Input.touches[0].position;
			// Scale to resolution.
			touchPos.y = Screen.height - touchPos.y;
		}
		
		// Get pointer stats.
		bool pressed = Input.GetMouseButtonDown (0);
		bool held = Input.GetMouseButton (0);
		bool unpressed = Input.GetMouseButtonUp (0);
		
		// If on the main screen.
		if(mainScreenGUI.activeSelf)
		{
			if(!saveDialogUp)
			{
				if(overlayState == OverlayState.Move)
				{
					// Move and scale the overlay image
#if UNITY_ANDROID || UNITY_IPHONE
					// If there are more than 1 touches registered.
					if(Input.touches.Length > 1)
					{
						// Flag zooming.
						zooming = true;
						// Handle zooming.
						ZoomOverlayTouch(Input.touches);
					}
					// Otherwise if there is 1 touch.
					else if(Input.touches.Length > 0)
					{
						// If zooming is not currently happening...
						if(!zooming)
						{
							// Get whether this is the initial touch.
							pressed = Input.touches[0].phase == TouchPhase.Began ? true : false;
							// Handle moving the canvas.
							MoveOverlay (Input.touches[0].position, pressed, true, false);
						}
					}
					else
					{
						// Otherwise, reset zooming flag and drag start.
						zooming = false;
						dragStart = Vector2.zero;
					}
#else
					// Handle moving the canvas.
					MoveOverlay (mouse, pressed, held, unpressed);
					// Handle zooming.
					ZoomOverlay (mouse, Input.GetMouseButtonDown (1), Input.GetMouseButton (1), Input.GetMouseButtonUp (1));
#endif
				}
				else
				{
					// Get bezier info.
					if (Input.GetKeyDown ("t"))
					{
						test ();
					}
					
					// If in move/zoom mode.
					if(moveZoomActive)
					{
#if UNITY_ANDROID || UNITY_IPHONE
						// If there are more than 1 touches registered.
						if(Input.touches.Length > 1)
						{
							// Flag zooming.
							zooming = true;
							// Handle zooming.
							ZoomCanvasTouch(Input.touches);
						}
						// Otherwise if there is 1 touch.
						else if(Input.touches.Length > 0)
						{
							// If zooming is not currently happening...
							if(!zooming)
							{
								// Get whether this is the initial touch.
								pressed = Input.touches[0].phase == TouchPhase.Began ? true : false;
								// Handle moving the canvas.
								MoveCanvas (Input.touches[0].position, pressed, true, false);
							}
						}
						else
						{
							// Otherwise, reset zooming flag and drag start.
							zooming = false;
							dragStart = Vector2.zero;
						}
#else
						// Handle moving the canvas.
						MoveCanvas (mouse, pressed, held, unpressed);
						// Handle zooming.
						ZoomCanvas (mouse, Input.GetMouseButtonDown (1), Input.GetMouseButton (1), Input.GetMouseButtonUp (1));
						ScrollZoomCanvas(Input.GetAxis ("Mouse ScrollWheel"));
#endif
					}
					else
					{
						// Set the mouse values to between 0 and 1.
						mouse.x /= (float)Screen.width;
						mouse.y /= (float)Screen.height;
						// Draw to the canvas.
						DrawBrush (mouse, pressed, held, unpressed);
					}
					
					// If the pointer has been released.
					if(unpressed)
					{
						// If the canvas has not been backed up...
						if(!backedUp)
						{
							// Backup the canvas.
							BackupCanvas ();
						}
					}
				}
			}
		}
		else
		{
			// Clear preview sprite.
			Color[] pixels = brushPreviewSprite.texture.GetPixels();
			for (int i = 0; i < pixels.Length; i++)
			{
				pixels[i] = Color.white;
			}
			
			brushPreviewSprite.texture.SetPixels (pixels);
			brushPreviewSprite.texture.Apply();
			
			// Draw new brush preview.
			Vector2 from = brushPreviewSprite.rect.center;
			Vector2 to = brushPreviewSprite.rect.center;
			
			from -= new Vector2(brushPreviewSprite.rect.width * 0.25f, 0);
			to += new Vector2(brushPreviewSprite.rect.width * 0.25f, 0);
			
			// Draw to the preview texture.
			Drawing.PaintLine (from, to, brushWidth, col, brushHardness, brushPreviewSprite.texture);
			brushPreviewSprite.texture.Apply ();
			
			// Get the rect for the color wheel.
			Rect colorWheelRect = colorWheel.GetComponent<RectTransform>().rect;
			colorWheelRect.x = (Screen.width * -0.18f) + (colorWheelRect.width * 0.75f - colorWheelRect.x);
			//colorWheelRect.y = Screen.height - (colorWheelRect.y + colorWheelRect.height) - ((colorWheelRect.height * 0.5f) + colorWheelRect.y);
			colorWheelRect.y = Screen.height - ((Screen.height * 0.25f) + (Screen.width * 0.3f));
			
			// Select the color from the color wheel.
			Color c = GUIControls.RGBCircle (Color.white, colorWheelRect, colorWheel.GetComponent<Image>().sprite.texture);
			
			if(c != Color.white)
			{
				// Set the current color to the color selected from the wheel.
				col = c;
				// Update the new color preview.
				newColorPreview.GetComponent<Image>().color = col;
			}
			
			// Reset mouse timer on initial click.
			if(pressed)
			{
				mouseTimer = 0f;
			}
			// Update the mouse timer while it is help down.
			if(held)
			{
				mouseTimer += Time.deltaTime;
				
//				if(colorWheelRect.Contains (mouse))
				//Rect wheelRect = new Rect(Screen.width * 0.04f, Screen.height * 0.34f, Screen.width * 0.3f, Screen.width * 0.3f);
				//Rect wheelRect = new Rect(Screen.width * 0.04f, (Screen.height * 0.145f) + (Screen.width * 0.15f), Screen.width * 0.3f, Screen.width * 0.3f);
				Rect wheelRect = new Rect(colorWheelRect.x, colorWheelRect.y, colorWheelRect.width, colorWheelRect.height);
//				wheelRect.x += (Screen.width * 0.00f);
				wheelRect.y = Screen.height - ((Screen.height * 0.255f) + (Screen.width * 0.3f));
				if(IsWithinCircle(wheelRect, mouse))
				{
					//colorWheelCircle.SetActive (true);
					clickStartedInColorCircle = true;
					colorWheelCircle.transform.localPosition = new Vector3(mouse.x - (Screen.width * 0.51f), mouse.y - (Screen.height * 0.5f) - (Screen.width * 0.01f), 0);
				}
				
				// If the timer goes beyond half a second...
				if(mouseTimer > 0.5f)
				{
					// Get the currently selected swatch...
					for (int y = 0; y < colorSwatches.GetLength (1); y++)
					{
						for (int x = 0; x < colorSwatches.GetLength (0); x++)
						{
							// Don't allow the first block to be overwritten.
							if(!(x == 0 && y == 0))
							{
								Rect swatchRect = new Rect(Screen.width * 0.4275f + (x * Screen.width * 0.055f), Screen.height * 0.69f - (y * Screen.height * 0.075f), Screen.width * 0.05f, Screen.height * 0.07f);
								
								// If the swatch has been clicked for over half a second, set the swatch color to the new color.
								if(swatchRect.Contains (mouse) && !clickStartedInColorCircle)
								{
									colorSwatches[x, y].GetComponent<Image>().color = col;
									swatchColors[x, y] = col;
								}
							}
						}
					}
				}
			}
			// If pointer is released...
			if(unpressed)
			{
				// Get the currently selected swatch...
				for (int y = 0; y < colorSwatches.GetLength (1); y++)
				{
					for (int x = 0; x < colorSwatches.GetLength (0); x++)
					{
						Rect swatchRect = new Rect(Screen.width * 0.4275f + (x * Screen.width * 0.055f), Screen.height * 0.69f - (y * Screen.height * 0.075f), Screen.width * 0.05f, Screen.height * 0.07f);
						
						// If the swatch has been clicked for less than half a second, select the swatch color for new color.
						if(mouseTimer < 0.5f)
						{
							if(swatchRect.Contains (mouse))
							{
								col = colorSwatches[x, y].GetComponent<Image>().color;
								// Update the new color preview.
								newColorPreview.GetComponent<Image>().color = col;
							}
						}
					}
				}
				
				// Reset mouse timer and whether the color circle was clicked.
				mouseTimer = 0f;
				clickStartedInColorCircle = false;
			}
		}
		
		// If the edit canvas is active, disable it.
		if(editCanvas != null && editCanvas.activeInHierarchy)
		{
			editCanvas.SetActive (false);
		}
	}
	
	/// <summary>
	/// Brush from p1 to p2.
	/// </summary>
	/// <param name="p1">P1.</param>
	/// <param name="p2">P2.</param>
	public void Brush (Vector2 p1, Vector2 p2)
	{
		// Set the anti-alias.
		Drawing.numSamples = AntiAlias;
		if (p2 == Vector2.zero)
		{
			p2 = p1;
		}
		
		// Set backedUp flag.
		backedUp = false;
		// Draw the desired line.
		Drawing.PaintLine (p1, p2, brushWidth, col, brushHardness, canvasTex);
		canvasTex.Apply ();
	}
	
	/// <summary>
	/// Eraser from p1 to p2.
	/// </summary>
	/// <param name="p1">P1.</param>
	/// <param name="p2">P2.</param>
	public void Eraser (Vector2 p1, Vector2 p2)
	{
		// Set the anti-alias.
		Drawing.numSamples = AntiAlias;
		if (p2 == Vector2.zero)
		{
			p2 = p1;
		}
		
		// Set backedUp flag.
		backedUp = false;
		// Erase the desired line.
		Drawing.PaintLine (p1, p2, brushWidth, Color.white, brushHardness, canvasTex);
		canvasTex.Apply ();
	}
	
	/// <summary>
	/// Test the bezier curves.
	/// </summary>
	public void test ()
	{
		float startTime = Time.realtimeSinceStartup;
		float w = 100;
		float h = 100;
		BezierPoint p1 = new BezierPoint();
		p1.Init(new Vector2(10, 0), new Vector2(5, 20), new Vector2(20, 0));
		BezierPoint p2 = new BezierPoint();
		p2.Init(new Vector2(50, 10), new Vector2(40, 20), new Vector2(60, -10));
		BezierCurve c = new BezierCurve();
		c.Init(p1.main, p1.control2, p2.control1, p2.main);
		p1.curve2 = c;
		p2.curve1 = c;
		Vector2 elapsedTime = new Vector2 ((Time.realtimeSinceStartup-startTime) * 10, 0);
		float startTime2 = Time.realtimeSinceStartup;
		
		for (int i = 0; i < w * h; i++)
		{
			Mathfx.IsNearBezier (new Vector2(Random.value * 80, Random.value * 30), p1, p2, 10);
		}
		
		Vector2 elapsedTime2 = new Vector2 ((Time.realtimeSinceStartup - startTime2) * 10, 0);
		Debug.Log ("Drawing took " + elapsedTime.ToString () + "  " + elapsedTime2.ToString ());
	}
	
	/// <summary>
	/// Close this instance.
	/// </summary>
	public void Close()
	{
		MenuScreen.firstFrameTimer = 0;
		
		// Reenable the main scene canvas.
		sceneCanvas.SetActive (true);
		
		// Disable the canvas camera.
		GameObject.Find ("CanvasCamera").GetComponent<Camera>().enabled  = false;
		
		// Reenable other scripts.
		if(GetComponent<EditGoalMap>() != null)
		{
			GetComponent<EditGoalMap>().wantsEditting = false;
		}
		
		SendMessage ("PainterClosed", SendMessageOptions.DontRequireReceiver);
		
		// Destroy all file browser game objects.
		Destroy (localCanvas);
		Destroy (canvasObject);
		
		// Remove this script from the object to close.
		DestroyImmediate (this);
	}
	
	/// <summary>
	/// Handles the file browser closing.
	/// </summary>
	public void FileBrowserClosed()
	{
		// Reenable the canvas camera.
		canvasCamera.SetActive (true);
		
		// Load in data from MCP.loadedTexture;
		overlayImage.GetComponent<Renderer>().material.mainTexture = MCP.loadedTexture;
		
		if(overlayState == OverlayState.Off)
		{
			ToggleOverlay ();
		}
		if(overlayState == OverlayState.On)
		{
			ToggleOverlay ();
		}
		
		// Reset flag
		gettingOverlay = false;
	}
	
	public override void DoGUI() {}
}
