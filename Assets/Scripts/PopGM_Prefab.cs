﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PopGM_Prefab : MonoBehaviour
{
	public PicMap picPrefab;
	public TextMap textPrefab;
	
	private MCP.GoalMap gm;
	private bool readyToPopulate = false;
	private Sprite defaultSprite;

	[System.Serializable]
	public class TextMap
	{
		public GameObject why1;
		public GameObject why2;
		public GameObject why3;
		
		public GameObject mainGoal;
		public GameObject subGoal1;
		public GameObject subGoal2;
		public GameObject subGoal3;
		public GameObject subGoal4;
		
		public GameObject who1;
		public GameObject who2;
		public GameObject who3;
		
		public GameObject how1;
		public GameObject how2;
		public GameObject how3;
		
		public GameObject start;
		public GameObject finish;
	}

	[System.Serializable]
	public class PicMap
	{
		public GameObject why1;
		public GameObject why2;
		public GameObject why3;

		public GameObject mainGoal;
		public GameObject subGoal1;
		public GameObject subGoal2;
		public GameObject subGoal3;
		public GameObject subGoal4;

		public GameObject who1;
		public GameObject who2;
		public GameObject who3;

		public GameObject how1;
		public GameObject how2;
		public GameObject how3;
		
		public GameObject start;
		public GameObject finish;
	}
	
	/// <summary>
	/// Gets the picture goal map prefab.
	/// </summary>
	void GetPicPrefab()
	{
		// Get a reference to the picture goal map parent.
		GameObject picObject = GameObject.Find ("GoalMapPicsParent");
		
		// Get a reference to the who parent.
		GameObject whoParent = picObject.transform.Find ("PicWhoParent").gameObject;
		picPrefab.who1 = whoParent.transform.GetChild (0).gameObject;
		picPrefab.who2 = whoParent.transform.GetChild (1).gameObject;
		picPrefab.who3 = whoParent.transform.GetChild (2).gameObject;
		
		// Get a reference to the how parent.
		GameObject howParent = picObject.transform.Find ("PicHowParent").gameObject;
		picPrefab.how1 = howParent.transform.GetChild (0).gameObject;
		picPrefab.how2 = howParent.transform.GetChild (1).gameObject;
		picPrefab.how3 = howParent.transform.GetChild (2).gameObject;
		
		// Get a reference to the why parent.
		GameObject whyParent = picObject.transform.Find ("PicWhyParent").gameObject;
		picPrefab.why1 = whyParent.transform.GetChild (0).gameObject;
		picPrefab.why2 = whyParent.transform.GetChild (1).gameObject;
		picPrefab.why3 = whyParent.transform.GetChild (2).gameObject;
		
		// Get a reference to the goal parent.
		GameObject goalParent = picObject.transform.Find ("PicGoalParent").gameObject;
		picPrefab.mainGoal = goalParent.transform.GetChild (0).gameObject;
		picPrefab.subGoal1 = goalParent.transform.GetChild (1).gameObject;
		picPrefab.subGoal2 = goalParent.transform.GetChild (2).gameObject;
		picPrefab.subGoal3 = goalParent.transform.GetChild (3).gameObject;
		picPrefab.subGoal4 = goalParent.transform.GetChild (4).gameObject;
		
		// Get a reference to the timeline parent.
		GameObject timelineParent = picObject.transform.Find ("PicTimelineParent").gameObject;
		picPrefab.start = timelineParent.transform.Find ("StartDate").gameObject;
		picPrefab.finish = timelineParent.transform.Find ("FinishDate").gameObject;
	}
	
	/// <summary>
	/// Gets the text goal map prefab.
	/// </summary>
	void GetTextPrefab()
	{
		// Get a reference to the text goal map parent.
		GameObject textObject = GameObject.Find ("GoalMapTextParent");
		
		// Get a reference to the who parent.
		//TODO: Get these not by name.
		GameObject whoParent = textObject.transform.Find ("TextWhoParent").gameObject;
		textPrefab.who1 = whoParent.transform.Find ("WhoLabel1").gameObject;
		textPrefab.who2 = whoParent.transform.Find ("WhoLabel2").gameObject;
		textPrefab.who3 = whoParent.transform.Find ("WhoLabel3").gameObject;
		
		// Get a reference to the how parent.
		GameObject howParent = textObject.transform.Find ("TextHowParent").gameObject;
		textPrefab.how1 = howParent.transform.Find ("HowLabel1").gameObject;
		textPrefab.how2 = howParent.transform.Find ("HowLabel2").gameObject;
		textPrefab.how3 = howParent.transform.Find ("HowLabel3").gameObject;
		
		// Get a reference to the why parent.
		GameObject whyParent = textObject.transform.Find ("TextWhyParent").gameObject;
		textPrefab.why1 = whyParent.transform.Find ("WhyLabel1").gameObject;
		textPrefab.why2 = whyParent.transform.Find ("WhyLabel2").gameObject;
		textPrefab.why3 = whyParent.transform.Find ("WhyLabel3").gameObject;
		
		// Get a reference to the goal parent.
		GameObject goalParent = textObject.transform.Find ("TextGoalParent").gameObject;
		textPrefab.mainGoal = goalParent.transform.Find ("GoalLabel1").gameObject;
		textPrefab.subGoal1 = goalParent.transform.Find ("GoalLabel2").gameObject;
		textPrefab.subGoal2 = goalParent.transform.Find ("GoalLabel3").gameObject;
		textPrefab.subGoal3 = goalParent.transform.Find ("GoalLabel4").gameObject;
		textPrefab.subGoal4 = goalParent.transform.Find ("GoalLabel5").gameObject;
		
		// Get a reference to the timeline parent.
		GameObject timelineParent = textObject.transform.Find ("TextTimelineParent").gameObject;
		textPrefab.start = timelineParent.transform.Find ("StartDate").gameObject;
		textPrefab.finish = timelineParent.transform.Find ("FinishDate").gameObject;
	}

	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		// Initialize objects.
		picPrefab = new PicMap();
		textPrefab = new TextMap();
		
		// Load resources.
		defaultSprite = Resources.Load<Sprite>("GUI/bm_box_ltpurple");
	}
	
//	private IEnumerator Populate()
//	{
//		yield return null;
//		
//		// If the prefab is ready to populate and the goal map exists...
//		if(readyToPopulate && GameObject.Find ("GoalImage5") != null)
//		{
//			// Get the picture objects.
//			GetPicPrefab ();
//			// Get the text objects.
//			GetTextPrefab ();
//			
//			// If the goal map hows exist, populate them.
//			if(gm.hows != null)
//			{
//				textPrefab.how1.GetComponent<Text>().text = gm.hows[0].how;
//				if(textPrefab.how1.GetComponent<InputField>() != null)
//				{
//					textPrefab.how1.GetComponent<InputField>().text = gm.hows[0].how;
//				}
//				
//				textPrefab.how2.GetComponent<Text>().text = gm.hows[1].how;
//				if(textPrefab.how2.GetComponent<InputField>() != null)
//				{
//					textPrefab.how2.GetComponent<InputField>().text = gm.hows[1].how;
//				}
//				
//				textPrefab.how3.GetComponent<Text>().text = gm.hows[2].how;
//				if(textPrefab.how3.GetComponent<InputField>() != null)
//				{
//					textPrefab.how3.GetComponent<InputField>().text = gm.hows[2].how;
//				}
//			}
//			
//			// If the goal map whys exist, populate them.
//			if(gm.whys != null)
//			{
//				textPrefab.why1.GetComponent<Text>().text = gm.whys[0].why;
//				if(textPrefab.why1.GetComponent<InputField>() != null)
//				{
//					textPrefab.why1.GetComponent<InputField>().text = gm.whys[0].why;
//				}
//				
//				textPrefab.why2.GetComponent<Text>().text = gm.whys[1].why;
//				if(textPrefab.why2.GetComponent<InputField>() != null)
//				{
//					textPrefab.why2.GetComponent<InputField>().text = gm.whys[1].why;
//				}
//				
//				textPrefab.why3.GetComponent<Text>().text = gm.whys[2].why;
//				if(textPrefab.why3.GetComponent<InputField>() != null)
//				{
//					textPrefab.why3.GetComponent<InputField>().text = gm.whys[2].why;
//				}
//			}
//			
//			// If the goal map whos exist, populate them.
//			if(gm.whos != null)
//			{
//				textPrefab.who1.GetComponent<Text>().text = gm.whos[0].who;
//				if(textPrefab.who1.GetComponent<InputField>() != null)
//				{
//					textPrefab.who1.GetComponent<InputField>().text = gm.whos[0].who;
//				}
//				
//				textPrefab.who2.GetComponent<Text>().text = gm.whos[1].who;
//				if(textPrefab.who2.GetComponent<InputField>() != null)
//				{
//					textPrefab.who2.GetComponent<InputField>().text = gm.whos[1].who;
//				}
//				
//				textPrefab.who3.GetComponent<Text>().text = gm.whos[2].who;
//				if(textPrefab.who3.GetComponent<InputField>() != null)
//				{
//					textPrefab.who3.GetComponent<InputField>().text = gm.whos[2].who;
//				}
//			}
//			
//			// If the goal map goals exist, populate them.
//			if(gm.goals != null)
//			{
//				textPrefab.mainGoal.GetComponent<Text>().text = gm.goals[0].goal;
//				if(textPrefab.mainGoal.GetComponent<InputField>() != null)
//				{
//					textPrefab.mainGoal.GetComponent<InputField>().text = gm.goals[0].goal;
//				}
//				
//				textPrefab.subGoal1.GetComponent<Text>().text = gm.goals[1].goal;
//				if(textPrefab.subGoal1.GetComponent<InputField>() != null)
//				{
//					textPrefab.subGoal1.GetComponent<InputField>().text = gm.goals[1].goal;
//				}
//				
//				textPrefab.subGoal2.GetComponent<Text>().text = gm.goals[2].goal;
//				if(textPrefab.subGoal2.GetComponent<InputField>() != null)
//				{
//					textPrefab.subGoal2.GetComponent<InputField>().text = gm.goals[2].goal;
//				}
//				
//				textPrefab.subGoal3.GetComponent<Text>().text = gm.goals[3].goal;
//				if(textPrefab.subGoal3.GetComponent<InputField>() != null)
//				{
//					textPrefab.subGoal3.GetComponent<InputField>().text = gm.goals[3].goal;
//				}
//				
//				textPrefab.subGoal4.GetComponent<Text>().text = gm.goals[4].goal;
//				if(textPrefab.subGoal4.GetComponent<InputField>() != null)
//				{
//					textPrefab.subGoal4.GetComponent<InputField>().text = gm.goals[4].goal;
//				}
//			}
//			
//			// Populate times.
//			textPrefab.start.GetComponent<Text>().text = gm.start.Day.ToString () + "/" + gm.start.Month.ToString () + "/" + gm.start.Year.ToString ();
//			textPrefab.finish.GetComponent<Text>().text = gm.finish.Day.ToString () + "/" + gm.finish.Month.ToString () + "/" + gm.finish.Year.ToString ();
//			
//			// If the goal map how pictures exist, populate them.
//			if(gm.hows != null)
//			{
//				if(gm.hows[0].image != null)
//				{
//					picPrefab.how1.GetComponent<Image>().sprite = Sprite.Create (gm.hows[0].image, new Rect (0, 0, gm.hows[0].image.width, gm.hows[0].image.height), new Vector2 (0.5f, 0.5f));
//				}
//				else
//				{
//					picPrefab.how1.GetComponent<Image>().sprite = defaultSprite;
//				}
//				if(gm.hows[1].image != null)
//				{
//					picPrefab.how2.GetComponent<Image>().sprite = Sprite.Create (gm.hows[1].image, new Rect (0, 0, gm.hows[1].image.width, gm.hows[1].image.height), new Vector2 (0.5f, 0.5f));
//				}
//				else
//				{
//					picPrefab.how2.GetComponent<Image>().sprite = defaultSprite;
//				}
//				if(gm.hows[2].image != null)
//				{
//					picPrefab.how3.GetComponent<Image>().sprite = Sprite.Create (gm.hows[2].image, new Rect (0, 0, gm.hows[2].image.width, gm.hows[2].image.height), new Vector2 (0.5f, 0.5f));
//				}
//				else
//				{
//					picPrefab.how3.GetComponent<Image>().sprite = defaultSprite;
//				}
//			}
//			
//			// If the goal map why pictures exist, populate them.
//			if(gm.whys != null)
//			{
//				if(gm.whys[0].image != null)
//				{
//					picPrefab.why1.GetComponent<Image>().sprite = Sprite.Create (gm.whys[0].image, new Rect (0, 0, gm.whys[0].image.width, gm.whys[0].image.height), new Vector2 (0.5f, 0.5f));
//				}
//				else
//				{
//					picPrefab.why1.GetComponent<Image>().sprite = defaultSprite;
//				}
//				if(gm.whys[1].image != null)
//				{
//					picPrefab.why2.GetComponent<Image>().sprite = Sprite.Create (gm.whys[1].image, new Rect (0, 0, gm.whys[1].image.width, gm.whys[1].image.height), new Vector2 (0.5f, 0.5f));
//				}
//				else
//				{
//					picPrefab.why2.GetComponent<Image>().sprite = defaultSprite;
//				}
//				if(gm.whys[2].image != null)
//				{
//					picPrefab.why3.GetComponent<Image>().sprite = Sprite.Create (gm.whys[2].image, new Rect (0, 0, gm.whys[2].image.width, gm.whys[2].image.height), new Vector2 (0.5f, 0.5f));
//				}
//				else
//				{
//					picPrefab.why3.GetComponent<Image>().sprite = defaultSprite;
//				}
//			}
//			
//			// If the goal map who pictures exist, populate them.
//			if(gm.whos != null)
//			{
//				if(gm.whos[0].image != null)
//				{
//					picPrefab.who1.GetComponent<Image>().sprite = Sprite.Create (gm.whos[0].image, new Rect (0, 0, gm.whos[0].image.width, gm.whos[0].image.height), new Vector2 (0.5f, 0.5f));
//				}
//				else
//				{
//					picPrefab.who1.GetComponent<Image>().sprite = defaultSprite;
//				}
//				if(gm.whos[1].image != null)
//				{
//					picPrefab.who2.GetComponent<Image>().sprite = Sprite.Create (gm.whos[1].image, new Rect (0, 0, gm.whos[1].image.width, gm.whos[1].image.height), new Vector2 (0.5f, 0.5f));
//				}
//				else
//				{
//					picPrefab.who2.GetComponent<Image>().sprite = defaultSprite;
//				}
//				if(gm.whos[2].image != null)
//				{
//					picPrefab.who3.GetComponent<Image>().sprite = Sprite.Create (gm.whos[2].image, new Rect (0, 0, gm.whos[2].image.width, gm.whos[2].image.height), new Vector2 (0.5f, 0.5f));
//				}
//				else
//				{
//					picPrefab.who3.GetComponent<Image>().sprite = defaultSprite;
//				}
//			}
//			
//			// If the goal map goal pictures exist, populate them.
//			if(gm.goals != null)
//			{
//				if(gm.goals[0] != null && gm.goals[0].image != null)
//				{
//					picPrefab.mainGoal.GetComponent<Image>().sprite = Sprite.Create (gm.goals[0].image, new Rect (0, 0, gm.goals[0].image.width, gm.goals[0].image.height), new Vector2 (0.5f, 0.5f));
//				}
//				else
//				{
//					picPrefab.mainGoal.GetComponent<Image>().sprite = defaultSprite;
//				}
//				if(gm.goals[1] != null && gm.goals[1].image != null)
//				{
//					picPrefab.subGoal1.GetComponent<Image>().sprite = Sprite.Create (gm.goals[1].image, new Rect (0, 0, gm.goals[1].image.width, gm.goals[1].image.height), new Vector2 (0.5f, 0.5f));
//				}
//				else
//				{
//					picPrefab.subGoal1.GetComponent<Image>().sprite = defaultSprite;
//				}
//				if(gm.goals[2] != null && gm.goals[2].image != null)
//				{
//					picPrefab.subGoal2.GetComponent<Image>().sprite = Sprite.Create (gm.goals[2].image, new Rect (0, 0, gm.goals[2].image.width, gm.goals[2].image.height), new Vector2 (0.5f, 0.5f));
//				}
//				else
//				{
//					picPrefab.subGoal2.GetComponent<Image>().sprite = defaultSprite;
//				}
//				if(gm.goals[3] != null && gm.goals[3].image != null)
//				{
//					picPrefab.subGoal3.GetComponent<Image>().sprite = Sprite.Create (gm.goals[3].image, new Rect (0, 0, gm.goals[3].image.width, gm.goals[3].image.height), new Vector2 (0.5f, 0.5f));
//				}
//				else
//				{
//					picPrefab.subGoal3.GetComponent<Image>().sprite = defaultSprite;
//				}
//				if(gm.goals[4] != null && gm.goals[4].image != null)
//				{
//					picPrefab.subGoal4.GetComponent<Image>().sprite = Sprite.Create (gm.goals[4].image, new Rect (0, 0, gm.goals[4].image.width, gm.goals[4].image.height), new Vector2 (0.5f, 0.5f));
//				}
//				else
//				{
//					picPrefab.subGoal4.GetComponent<Image>().sprite = defaultSprite;
//				}
//			}
//			
//			// Populate times.
//			picPrefab.start.GetComponent<Text>().text = gm.start.Day.ToString () + "/" + gm.start.Month.ToString () + "/" + gm.start.Year.ToString ();
//			picPrefab.finish.GetComponent<Text>().text = gm.finish.Day.ToString () + "/" + gm.finish.Month.ToString () + "/" + gm.finish.Year.ToString ();
//			
//			// Disable the picture goal map by default.
//			GameObject picObject = GameObject.Find ("GoalMapPicsParent");
//			picObject.SetActive (false);
//			
//			readyToPopulate = false;
//			Debug.Log ("Prefab Populated...");
//		}
//	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update ()
	{
		// If the prefab is ready to populate and the goal map exists...
		if(readyToPopulate && GameObject.Find ("GoalImage5") != null)
		{
			// Move the image object off the screen until it is loaded so the two do not cover each other.
			GameObject picObject = GameObject.Find ("GoalMapPicsParent");
//			oldPos = picObject.transform.localPosition;
//			Vector3 newPos = picObject.transform.localPosition;
//			newPos.x = Screen.width * 0.15f;
//			picObject.transform.localPosition = newPos;
			
			// Get the picture objects.
			GetPicPrefab ();
			// Get the text objects.
			GetTextPrefab ();
			
			// If the goal map hows exist, populate them.
			if(gm.hows != null)
			{
				textPrefab.how1.GetComponent<Text>().text = gm.hows[0].how;
				if(textPrefab.how1.GetComponent<InputField>() != null)
				{
					textPrefab.how1.GetComponent<InputField>().text = gm.hows[0].how;
				}
				
				textPrefab.how2.GetComponent<Text>().text = gm.hows[1].how;
				if(textPrefab.how2.GetComponent<InputField>() != null)
				{
					textPrefab.how2.GetComponent<InputField>().text = gm.hows[1].how;
				}
				
				textPrefab.how3.GetComponent<Text>().text = gm.hows[2].how;
				if(textPrefab.how3.GetComponent<InputField>() != null)
				{
					textPrefab.how3.GetComponent<InputField>().text = gm.hows[2].how;
				}
			}
			
			// If the goal map whys exist, populate them.
			if(gm.whys != null)
			{
				textPrefab.why1.GetComponent<Text>().text = gm.whys[0].why;
				if(textPrefab.why1.GetComponent<InputField>() != null)
				{
					textPrefab.why1.GetComponent<InputField>().text = gm.whys[0].why;
				}
				
				textPrefab.why2.GetComponent<Text>().text = gm.whys[1].why;
				if(textPrefab.why2.GetComponent<InputField>() != null)
				{
					textPrefab.why2.GetComponent<InputField>().text = gm.whys[1].why;
				}
				
				textPrefab.why3.GetComponent<Text>().text = gm.whys[2].why;
				if(textPrefab.why3.GetComponent<InputField>() != null)
				{
					textPrefab.why3.GetComponent<InputField>().text = gm.whys[2].why;
				}
			}
			
			// If the goal map whos exist, populate them.
			if(gm.whos != null)
			{
				textPrefab.who1.GetComponent<Text>().text = gm.whos[0].who;
				if(textPrefab.who1.GetComponent<InputField>() != null)
				{
					textPrefab.who1.GetComponent<InputField>().text = gm.whos[0].who;
				}
				
				textPrefab.who2.GetComponent<Text>().text = gm.whos[1].who;
				if(textPrefab.who2.GetComponent<InputField>() != null)
				{
					textPrefab.who2.GetComponent<InputField>().text = gm.whos[1].who;
				}
				
				textPrefab.who3.GetComponent<Text>().text = gm.whos[2].who;
				if(textPrefab.who3.GetComponent<InputField>() != null)
				{
					textPrefab.who3.GetComponent<InputField>().text = gm.whos[2].who;
				}
			}
			
			// If the goal map goals exist, populate them.
			if(gm.goals != null)
			{
				textPrefab.mainGoal.GetComponent<Text>().text = gm.goals[0].goal;
				if(textPrefab.mainGoal.GetComponent<InputField>() != null)
				{
					textPrefab.mainGoal.GetComponent<InputField>().text = gm.goals[0].goal;
				}
				
				textPrefab.subGoal1.GetComponent<Text>().text = gm.goals[1].goal;
				if(textPrefab.subGoal1.GetComponent<InputField>() != null)
				{
					textPrefab.subGoal1.GetComponent<InputField>().text = gm.goals[1].goal;
				}
				
				textPrefab.subGoal2.GetComponent<Text>().text = gm.goals[2].goal;
				if(textPrefab.subGoal2.GetComponent<InputField>() != null)
				{
					textPrefab.subGoal2.GetComponent<InputField>().text = gm.goals[2].goal;
				}
				
				textPrefab.subGoal3.GetComponent<Text>().text = gm.goals[3].goal;
				if(textPrefab.subGoal3.GetComponent<InputField>() != null)
				{
					textPrefab.subGoal3.GetComponent<InputField>().text = gm.goals[3].goal;
				}
				
				textPrefab.subGoal4.GetComponent<Text>().text = gm.goals[4].goal;
				if(textPrefab.subGoal4.GetComponent<InputField>() != null)
				{
					textPrefab.subGoal4.GetComponent<InputField>().text = gm.goals[4].goal;
				}
			}
			
			// Populate times.
			textPrefab.start.GetComponent<Text>().text = gm.start.Day.ToString () + "/" + gm.start.Month.ToString () + "/" + gm.start.Year.ToString ();
			textPrefab.finish.GetComponent<Text>().text = gm.finish.Day.ToString () + "/" + gm.finish.Month.ToString () + "/" + gm.finish.Year.ToString ();
			
			// If the goal map how pictures exist, populate them.
			if(gm.hows != null)
			{
				if(gm.hows[0].image != null)
				{
					picPrefab.how1.GetComponent<Image>().sprite = Sprite.Create (gm.hows[0].image, new Rect (0, 0, gm.hows[0].image.width, gm.hows[0].image.height), new Vector2 (0.5f, 0.5f));
				}
				else
				{
					picPrefab.how1.GetComponent<Image>().sprite = defaultSprite;
				}
				if(gm.hows[1].image != null)
				{
					picPrefab.how2.GetComponent<Image>().sprite = Sprite.Create (gm.hows[1].image, new Rect (0, 0, gm.hows[1].image.width, gm.hows[1].image.height), new Vector2 (0.5f, 0.5f));
				}
				else
				{
					picPrefab.how2.GetComponent<Image>().sprite = defaultSprite;
				}
				if(gm.hows[2].image != null)
				{
					picPrefab.how3.GetComponent<Image>().sprite = Sprite.Create (gm.hows[2].image, new Rect (0, 0, gm.hows[2].image.width, gm.hows[2].image.height), new Vector2 (0.5f, 0.5f));
				}
				else
				{
					picPrefab.how3.GetComponent<Image>().sprite = defaultSprite;
				}
			}
			
			// If the goal map why pictures exist, populate them.
			if(gm.whys != null)
			{
				if(gm.whys[0].image != null)
				{
					picPrefab.why1.GetComponent<Image>().sprite = Sprite.Create (gm.whys[0].image, new Rect (0, 0, gm.whys[0].image.width, gm.whys[0].image.height), new Vector2 (0.5f, 0.5f));
				}
				else
				{
					picPrefab.why1.GetComponent<Image>().sprite = defaultSprite;
				}
				if(gm.whys[1].image != null)
				{
					picPrefab.why2.GetComponent<Image>().sprite = Sprite.Create (gm.whys[1].image, new Rect (0, 0, gm.whys[1].image.width, gm.whys[1].image.height), new Vector2 (0.5f, 0.5f));
				}
				else
				{
					picPrefab.why2.GetComponent<Image>().sprite = defaultSprite;
				}
				if(gm.whys[2].image != null)
				{
					picPrefab.why3.GetComponent<Image>().sprite = Sprite.Create (gm.whys[2].image, new Rect (0, 0, gm.whys[2].image.width, gm.whys[2].image.height), new Vector2 (0.5f, 0.5f));
				}
				else
				{
					picPrefab.why3.GetComponent<Image>().sprite = defaultSprite;
				}
			}
			
			// If the goal map who pictures exist, populate them.
			if(gm.whos != null)
			{
				if(gm.whos[0].image != null)
				{
					picPrefab.who1.GetComponent<Image>().sprite = Sprite.Create (gm.whos[0].image, new Rect (0, 0, gm.whos[0].image.width, gm.whos[0].image.height), new Vector2 (0.5f, 0.5f));
				}
				else
				{
					picPrefab.who1.GetComponent<Image>().sprite = defaultSprite;
				}
				if(gm.whos[1].image != null)
				{
					picPrefab.who2.GetComponent<Image>().sprite = Sprite.Create (gm.whos[1].image, new Rect (0, 0, gm.whos[1].image.width, gm.whos[1].image.height), new Vector2 (0.5f, 0.5f));
				}
				else
				{
					picPrefab.who2.GetComponent<Image>().sprite = defaultSprite;
				}
				if(gm.whos[2].image != null)
				{
					picPrefab.who3.GetComponent<Image>().sprite = Sprite.Create (gm.whos[2].image, new Rect (0, 0, gm.whos[2].image.width, gm.whos[2].image.height), new Vector2 (0.5f, 0.5f));
				}
				else
				{
					picPrefab.who3.GetComponent<Image>().sprite = defaultSprite;
				}
			}
			
			// If the goal map goal pictures exist, populate them.
			if(gm.goals != null)
			{
				if(gm.goals[0] != null && gm.goals[0].image != null)
				{
					picPrefab.mainGoal.GetComponent<Image>().sprite = Sprite.Create (gm.goals[0].image, new Rect (0, 0, gm.goals[0].image.width, gm.goals[0].image.height), new Vector2 (0.5f, 0.5f));
				}
				else
				{
					picPrefab.mainGoal.GetComponent<Image>().sprite = defaultSprite;
				}
				if(gm.goals[1] != null && gm.goals[1].image != null)
				{
					picPrefab.subGoal1.GetComponent<Image>().sprite = Sprite.Create (gm.goals[1].image, new Rect (0, 0, gm.goals[1].image.width, gm.goals[1].image.height), new Vector2 (0.5f, 0.5f));
				}
				else
				{
					picPrefab.subGoal1.GetComponent<Image>().sprite = defaultSprite;
				}
				if(gm.goals[2] != null && gm.goals[2].image != null)
				{
					picPrefab.subGoal2.GetComponent<Image>().sprite = Sprite.Create (gm.goals[2].image, new Rect (0, 0, gm.goals[2].image.width, gm.goals[2].image.height), new Vector2 (0.5f, 0.5f));
				}
				else
				{
					picPrefab.subGoal2.GetComponent<Image>().sprite = defaultSprite;
				}
				if(gm.goals[3] != null && gm.goals[3].image != null)
				{
					picPrefab.subGoal3.GetComponent<Image>().sprite = Sprite.Create (gm.goals[3].image, new Rect (0, 0, gm.goals[3].image.width, gm.goals[3].image.height), new Vector2 (0.5f, 0.5f));
				}
				else
				{
					picPrefab.subGoal3.GetComponent<Image>().sprite = defaultSprite;
				}
				if(gm.goals[4] != null && gm.goals[4].image != null)
				{
					picPrefab.subGoal4.GetComponent<Image>().sprite = Sprite.Create (gm.goals[4].image, new Rect (0, 0, gm.goals[4].image.width, gm.goals[4].image.height), new Vector2 (0.5f, 0.5f));
				}
				else
				{
					picPrefab.subGoal4.GetComponent<Image>().sprite = defaultSprite;
				}
			}
			
			// Populate times.
			picPrefab.start.GetComponent<Text>().text = gm.start.Day.ToString () + "/" + gm.start.Month.ToString () + "/" + gm.start.Year.ToString ();
			picPrefab.finish.GetComponent<Text>().text = gm.finish.Day.ToString () + "/" + gm.finish.Month.ToString () + "/" + gm.finish.Year.ToString ();
			
			// Disable the picture goal map by default.
			picObject.SetActive (false);
//			picObject.transform.localPosition = oldPos;
			readyToPopulate = false;
			Debug.Log ("Prefab Populated...");
		}
	}

	/// <summary>
	/// Populates the goal map.
	/// </summary>
	/// <param name="_gm">Goal map to populate with.</param>
	public void PopulateGoalMap(MCP.GoalMap _gm)
	{
		Debug.Log ("Populating prefab...");
		
		// Set the local version of the goal map to the input one.
		gm = _gm;
		// Flag as ready to populate.
		readyToPopulate = true;
//		StartCoroutine (Populate ());
	}
}
