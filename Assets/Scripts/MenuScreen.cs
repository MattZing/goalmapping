using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

/**
*  Menu_Screen
* 
*  Abstract class to provide main menus functionality, utility functions and pop out gizmo
*  
**/
public abstract class MenuScreen : MonoBehaviour
{
	private const int screenWidth = 1024, screenHeight = 576;
	
	[SerializeField]
	protected TextAsset helpPopupText;
	protected bool showHelp = true;		// try to show help each time when loading scene

	[SerializeField]
	protected bool isHelpEnabled = true;	// is help enabled from inspector?

	protected bool tempRequestHelp = false;		// temp value for below

	[SerializeField]
	protected bool requestHelp = false;			// is help enabled in the MCP?

	protected bool hasSentData = false;

	// A 4x4 Matrix
	protected Matrix4x4 trsMatrix = Matrix4x4.identity;

	// A three dimension vector that will translate GUI coordinate system
	protected Vector3 positionVec;

	// Two booleans to determine which of the GUI buttons have been pressed
	protected bool maximize = false;
	protected bool minimize = true;
	private Sprite menuIconHome;
	protected Sprite menuIconHelp;
	private Sprite menuIconSettings;
	private Sprite menuIconLogout;
	protected Sprite backButtonSprite;
	private Sprite darkPurpleBackground;
	private Sprite mediumPurpleBackground;
	protected Sprite lightPurpleBackground;
	private Sprite blueBackground;
	private Sprite orangeBackground;
#if !UNITY_ANDROID && !UNITY_IPHONE
	private Sprite menuIconFullscreen;
#endif
	
	protected static Texture news_infoImage;
	protected static Texture news_alertImage;
	protected static Texture news_criticalImage;
	protected Texture2D mouseHandSelect;
	protected Texture2D mouseHandNormal;
	protected int mouseOverCount = 0;
	private Rect baseScreenSize;
	protected float _fontSize = 0f;
	protected float _fontSize_Small = 0f;
	protected float _fontSize_Large = 0f;
	protected float _fontSize_xLarge = 0f;
	private float Ratio = 27f;
	protected bool repeatButtonToggle;
	protected bool comingFromIdle = false;
	public static float firstFrameTimer = 0;

	// Audioclip objects accessable from anywhere
	public static AudioClip buttonClick;
	public static AudioClip moveIn;
	public static AudioClip moveOut;
	public static AudioClip negative;
	public static AudioClip popUpSpendCash;
	public static AudioClip open;
	public static AudioClip positive;
	public static AudioClip popUpOpen;
	public static AudioClip popUpCash;

	// GUI colours
	protected Color colorZingMagenta;
	protected Color colorZingGreen;
	protected Color colorZingCyan;
	protected Color colorZingOrange;
	protected Color colorZingPurple;
	protected Color colorZingLightCyan;
	protected Color bmOrange;
	protected Color bmDarkBlue;
	protected Color bmPink;
	public Rect defaultPopUpWindow;
	public Rect largePopUpWindow;
	
	[SerializeField]
	protected bool isPopUpVisible = false;
	
	protected bool widgetRight = true;
	public static GUISkin skin;
	public static List<string> lastMenuScreen = new List<string> ();
	public static GameObject soundManager;
	public static SoundManager sndManager;

	// Touch scroll
	private Touch touch;
	private float scrollVelocityY;
	private float scrollVelocityX;
	private float timeTouchPhaseEnded;
	private float inertiaDuration = 3.0f;
	public Vector2 scrollPosition;

	public GameObject canvas;
	public GameObject eventSystem;

	//New UI assets
	protected Font defaultFont;
	public Sprite header_image;
	public Sprite window_back;
	public Sprite popup_back;
	protected Sprite greyBackground;
	protected Sprite buttonBackground;
	protected Sprite sliderBackgroundHoriz;
	protected Sprite sliderForegroundHoriz;
	protected Sprite sliderBackgroundVert;
	protected Sprite sliderForegroundVert;
	protected Sprite sliderHandle;
	protected Sprite menuBackgroundButton;
	protected Sprite menuBackground;
	protected Sprite menuButton;
	protected Sprite iconButton;
	protected Sprite inputBackground;
	protected Sprite enumIconSprite;
	protected Sprite tickBoxIcon;
	protected Sprite tickBoxCheckIcon;
	private Sprite goalMapBackground;
	private Sprite menuIconAbout;

	public static Rect canvasSize;

	public delegate void MyVoidFunc ();

	public delegate void MyPopupFunc (string finalButtonText,CommonTasks task,string levelName);

	public struct PopupTemplate
	{
		public GameObject parent;
		public Rect rect;
		public string title;
		public string body;
		public string buttonText;
		public CommonTasks task;
		public string menuName;
		public string guiTitleStyle;
		public string guiBodyStyle;
	}

	public enum CommonTasks
	{
			DestroyParent,
			LoadLevel,
			MakePopup,
			OpenUrl
	};

	public class TouchScroll
	{
		public float scrollVelocityY;
		public float scrollVelocityX;
		public float timeTouchPhaseEnded;
		public float inertiaDuration = 3.0f;
	}

	public enum PopUpType
	{
		firstLaunch,
		doInsight,
		notification,
		warning,
		critical,
		friend,
		zingbitearned,
		zingbitspent,
		notenoughzingbits,
		demoOrTrialPurchaseAttempt,
		helpPopup,
		largeNotification,
		achievement
	};

	public static AudioListener audioListener;

	/// <summary>
	/// Sets the 3D GUI prefabs.
	/// </summary>
	private void SetPrefabs ()
	{
		canvas = Resources.Load ("Prefabs/Canvas") as GameObject;
		eventSystem = Resources.Load ("Prefabs/EventSystem") as GameObject;
	}

	/// <summary>
	/// Initialize this instance.
	/// </summary>
	public void Init ()
	{
		// Reset the first frame timer.
		MenuScreen.firstFrameTimer = 0;
		
		MCP.LoadTextResources( "Localization/GoalMapping_EN_GB" );
			
		if (MCP.userInfo == null && audioListener == null)
		{
			audioListener = gameObject.GetComponent<AudioListener> ();
			if (audioListener == null)
			{
				audioListener = gameObject.AddComponent <AudioListener>() as AudioListener;
			}
		}

		if (soundManager == null)
		{
			soundManager = GameObject.Instantiate (Resources.Load ("SoundManager")) as GameObject;
			sndManager = soundManager.GetComponent<SoundManager> ();
		}

		requestHelp = false;

		tempRequestHelp = requestHelp;

		menuIconHelp = Resources.Load<Sprite> ("GUI/menuicon_help");
		menuIconLogout = Resources.Load<Sprite> ("GUI/menuicon_logout");
		menuIconSettings = Resources.Load<Sprite> ("GUI/menuicon_settings");	
		darkPurpleBackground = Resources.Load<Sprite> ("GUI/bm_darkbox_rounded32px");
		mediumPurpleBackground = Resources.Load<Sprite> ("GUI/bm_box_purple");
		lightPurpleBackground = Resources.Load<Sprite> ("GUI/bm_box_ltpurple");
		blueBackground = Resources.Load<Sprite> ("GUI/bm_box_blue");
		orangeBackground = Resources.Load<Sprite> ("GUI/bm_box_orange");
		
#if !UNITY_ANDROID && !UNITY_IPHONE
		menuIconFullscreen = Resources.Load<Sprite> ("GUI/playericon_fullscreen");
#endif
		
		menuIconHome = Resources.Load<Sprite> ("GUI/menuicon_home");

		news_infoImage = Resources.Load ("news_info") as Texture;
		news_alertImage = Resources.Load<Sprite> ("news_alert").texture;
		news_criticalImage = Resources.Load ("news_critical") as Texture;
		
		menuBackground = Resources.Load<Sprite> ("menu_bg");
		goalMapBackground = Resources.Load<Sprite> ("goalmapbg_draw");
		menuIconAbout = Resources.Load<Sprite> ("GUI/menuicon_about");

		mouseHandSelect = Resources.Load ("Cursor/zingcursor_hand") as Texture2D;
		mouseHandNormal = Resources.Load ("Cursor/zingcursor_arrow") as Texture2D;

		buttonClick = Resources.Load ("Audio/ZingUpGUISounds_Default/Default_ButtonClick") as AudioClip;

		moveOut = Resources.Load ("Audio/ZingUpGUISounds_Default/Default_MoveIn") as AudioClip;
		moveIn = Resources.Load ("Audio/ZingUpGUISounds_Default/Default_MoveOut") as AudioClip;
		negative = Resources.Load ("Audio/ZingUpGUISounds_Default/Default_Negative") as AudioClip;
		popUpSpendCash = Resources.Load ("Audio/Default_Financial2") as AudioClip;
		open = Resources.Load ("Audio/ZingUpGUISounds_Default/Default_Open") as AudioClip;
		positive = Resources.Load ("Audio/ZingUpGUISounds_Default/Default_Positive") as AudioClip;
		popUpOpen = Resources.Load ("Audio/ZingUpGUISounds_Default/Default_Popup") as AudioClip;
		popUpCash = Resources.Load ("Audio/Default_Financial") as AudioClip;

		// Zing color values
		colorZingMagenta = new Color (0.9453125f, 0f, 0.38671875f);
		colorZingGreen = new Color (0.4296875f, 0.8359375f, 0f);
		colorZingCyan = new Color (0.19921875f, 0.65625f, 1f);
		colorZingOrange = new Color (1f, 0.4765625f, 0.08984375f);
		colorZingPurple = new Color (0.765625f, 0.08984375f, 0.86328125f);
		colorZingLightCyan = new Color (0.59765625f, 0.82421875f, 1f);

		// Brain Mayne color values.
		bmOrange = new Color (0.937f, 0.576f, 0f);
		bmDarkBlue = new Color (0.161f, 0.078f, 0.247f);
		bmPink = new Color (198f / 255f, 147f / 255f, 200f / 255f);

		skin = Resources.Load ("GUI/GMSkin") as GUISkin;

		header_image = Resources.Load<Sprite> ("bri-goal-mapping-logo-RGB-201406-01");
		window_back = Resources.Load<Sprite> ("GUI/bm_button_bg64px");
		popup_back = Resources.Load<Sprite> ("GUI/bm_button_bg64px_opaque");
		menuBackgroundButton = Resources.Load<Sprite> ("GUI/menu_min 1");
		menuBackground = Resources.Load<Sprite> ("menu_bg");
		buttonBackground = Resources.Load<Sprite> ("GUI/bm_buttonbg");
		sliderBackgroundHoriz = Resources.Load<Sprite> ("GUI/bm-sliderbghoriz");
		sliderBackgroundVert = Resources.Load<Sprite> ("GUI/bm-sliderbgvert");
		sliderForegroundHoriz = Resources.Load<Sprite> ("GUI/bm-sliderfillhoriz");
		sliderForegroundVert = Resources.Load<Sprite> ("GUI/bm-sliderfillvert");
		sliderHandle = Resources.Load<Sprite> ("GUI/bm_sliderhandle");
		inputBackground = Resources.Load<Sprite> ("GUI/WhiteSquare");
		menuButton = Resources.Load<Sprite> ("GUI/bm_menu4");
		backButtonSprite = Resources.Load<Sprite> ("GUI/bm_back4");
		enumIconSprite = Resources.Load<Sprite> ("GUI/bm_message_number");
		tickBoxIcon = Resources.Load<Sprite> ("tickbox");
		tickBoxCheckIcon = Resources.Load<Sprite> ("bm_tick");
		
		iconButton = Resources.Load <Sprite> ("GUI/bm_button_bg128px");

		// Set 3D GUI prefabs.
		SetPrefabs ();
		
		defaultPopUpWindow = new Rect (Screen.width * 0.25f, Screen.height * 0.25f, Screen.width * 0.5f, Screen.height * 0.5f);
		largePopUpWindow = new Rect (Screen.width * 0.125f, Screen.height * 0.125f, Screen.width * 0.75f, Screen.height * 0.75f);
		
		// Set up fonts.
		defaultFont = Resources.Load ("GUI/BM Fonts/Gotham-Book") as Font;
		skin.GetStyle ("ScreenTitle").font = defaultFont;
		_fontSize = Mathf.Min (Screen.width, Screen.height) / Ratio;
		_fontSize_Small = Mathf.Min (Screen.width, Screen.height) / 36;
		_fontSize_Large = Mathf.Min (Screen.width, Screen.height) / 19;
		_fontSize_xLarge = Mathf.Min (Screen.width, Screen.height) / 10;
			
		// Initialize layout 
		skin.textField.fontSize = (int)_fontSize;
		skin.textArea.fontSize = (int)_fontSize;
		skin.toggle.fontSize = (int)_fontSize;
		skin.label.fontSize = (int)_fontSize;	
		skin.button.fontSize = (int)_fontSize;	
		skin.box.fontSize = (int)_fontSize_Small;	
	
		for (int n = 0; n< skin.customStyles.Length; n++)
		{
			skin.customStyles [n].fontSize = (int)_fontSize;

			if (MCP.userInfo != null)
			{
				if (MCP.userInfo.themeName != null)
				{
					skin.customStyles [n].font = defaultFont;
				}
			}
		}
		
		//GMSkin styles
		skin.GetStyle ("ActionButton").fontSize = (int)_fontSize_Small;
		skin.GetStyle ("ScreenTitle").fontSize = (int)_fontSize_Large;
	}
	
	/// <summary>
	/// Handles the touch scrolling.
	/// </summary>
	public void HandleTouch ()
	{
		// If there are any touches...
		if (Input.touchCount != 1)
		{
			if (scrollVelocityY != 0.0f)
			{
				// Slow down over time
				float t = (Time.time - timeTouchPhaseEnded) / inertiaDuration;

				float frameVelocityY = Mathf.Lerp (scrollVelocityY, 0, t);
				float frameVelocityX = Mathf.Lerp (scrollVelocityX, 0, t);
				
				scrollPosition.x += frameVelocityX * Time.deltaTime;
				scrollPosition.y += frameVelocityY * Time.deltaTime;
				
				// After N seconds, we've stopped
				if (t >= 1.0f)
				{
					scrollVelocityY = 0.0f;
				}

				if (t >= 1.0f)
				{
					scrollVelocityX = 0.0f;
				}
			}
			return;
		}
		
		Touch touch = Input.touches [0];
		
		if (touch.phase == TouchPhase.Began)
		{
			scrollVelocityY = 0.0f;
			scrollVelocityX = 0.0f;
		}
		// Stop this is the touch is cancelled.
		else if (touch.phase == TouchPhase.Canceled) {}
		
		else if (touch.phase == TouchPhase.Moved)
		{
			// Dragging
			scrollPosition.x += touch.deltaPosition.x * 4.0f;
			scrollPosition.y += touch.deltaPosition.y * 4.0f;
		}
		else if (touch.phase == TouchPhase.Ended)
		{
			// Impart momentum, using last delta as the starting velocity
			scrollVelocityY = (int)(touch.deltaPosition.y / touch.deltaTime) * 4.0f;
			scrollVelocityX = (int)(touch.deltaPosition.x / touch.deltaTime) * 4.0f;
			timeTouchPhaseEnded = Time.time;
		}
	}

	/// <summary>
	/// Handles the touch scroll on specified rect.
	/// </summary>
	/// <param name="rect">Rect.</param>
	/// <param name="timeTouch">Time touch.</param>
	/// <param name="scrollVelocity">Scroll velocity.</param>
	/// <param name="scrollPosition">Scroll position.</param>
	public void HandleTouchRects (Rect rect, ref float timeTouch, ref Vector2 scrollVelocity, ref Vector2 scrollPosition)
	{
		// If there are any touches...
		if (Input.touchCount != 1)
		{
			if (scrollVelocity.x != 0.0f)
			{
				// Slow down over time
				float t = (Time.time - timeTouchPhaseEnded) / inertiaDuration;
				
				float frameVelocityX = Mathf.Lerp (scrollVelocityX, 0, t);
				scrollPosition.x -= frameVelocityX * Time.deltaTime;

				// After N seconds, we've stopped
				if (t >= 1.0f)
				{
					scrollVelocity.x = 0.0f;
				}
			}
			if (scrollVelocity.y != 0.0f)
			{
				// Slow down over time
				float t = (Time.time - timeTouchPhaseEnded) / inertiaDuration;
				float frameVelocityY = Mathf.Lerp (scrollVelocityY, 0, t);

				scrollPosition.y += frameVelocityY * Time.deltaTime;
				
				// After N seconds, we've stopped
				if (t >= 1.0f)
				{
					scrollVelocity.y = 0.0f;
				}
			}
			return;
		}
		
		Touch touch = Input.touches [0];
		Vector3 inputGuiPosition = Input.touches [0].position;
		inputGuiPosition.y = Screen.height - inputGuiPosition.y;
		
		if (rect.Contains (inputGuiPosition))
		{
			if (touch.phase == TouchPhase.Began)
			{
				scrollVelocity.y = 0.0f;
				scrollVelocity.x = 0.0f;
			}
			
			// Go no further is the touch is cancelled.
			else if (touch.phase == TouchPhase.Canceled) {}
			
			else if (touch.phase == TouchPhase.Moved)
			{
				// Dragging
				scrollPosition.x -= touch.deltaPosition.x;
				scrollPosition.y += touch.deltaPosition.y;
			}
			else if (touch.phase == TouchPhase.Ended)
			{
				// Impart momentum, using last delta as the starting velocity.
				scrollVelocity.y = -(int)(touch.deltaPosition.y / touch.deltaTime);
				scrollVelocity.x = (int)(touch.deltaPosition.x / touch.deltaTime);
				timeTouchPhaseEnded = Time.time;
			}
		}
	}

	/// <summary>
	/// Handles the touch scroll in a set rect.
	/// </summary>
	/// <returns>The touch scroll.</returns>
	/// <param name="scrollRect">Scroll rect.</param>
	public Vector2 HandleTouchScroll (Rect scrollRect)
	{
		Vector2 scrollPositionOut = new Vector2 ();

		// If there are any touches.
		if (Input.touchCount != 1)
		{
			if (scrollVelocityY != 0.0f)
			{
				// Slow down over time
				float t = (Time.time - timeTouchPhaseEnded) / inertiaDuration;
				float frameVelocityY = Mathf.Lerp (scrollVelocityY, 0, t);
				float frameVelocityX = Mathf.Lerp (scrollVelocityX, 0, t);
				
				scrollPosition.x += frameVelocityX * Time.deltaTime;
				scrollPosition.y += frameVelocityY * Time.deltaTime;
				
				// After N seconds, we've stopped
				if (t >= 1.0f)
				{
					scrollVelocityY = 0.0f;
				}

				if (t >= 1.0f)
				{
					scrollVelocityX = 0.0f;
				}
			}
	
		}
		
		Touch touch = Input.touches [0];
		
		if (touch.phase == TouchPhase.Began)
		{
			scrollVelocityY = 0.0f;
			scrollVelocityX = 0.0f;
		}
		// Go no further if the touch is cancelled
		else if (touch.phase == TouchPhase.Canceled) {}
		
		else if (touch.phase == TouchPhase.Moved)
		{
			// Dragging
			scrollPosition.x += touch.deltaPosition.x;
			scrollPosition.y += touch.deltaPosition.y;
		}
		else if (touch.phase == TouchPhase.Ended)
		{
			// Impart momentum, using last delta as the starting velocity
			scrollVelocityY = (int)(touch.deltaPosition.y / touch.deltaTime);
			scrollVelocityX = (int)(touch.deltaPosition.x / touch.deltaTime);
			timeTouchPhaseEnded = Time.time;
		}

		return scrollPositionOut;
	}

	/// <summary>
	/// Updates the screen for the side gizmo.
	/// </summary>
	public void UpdateScreen ()
	{
		if (!widgetRight)
		{
			if (maximize)
			{
				// Interpolate the current vector x component until it has the same as value the screen width
				positionVec.x = Mathf.SmoothStep (positionVec.x, Screen.width * 0.25f, Time.deltaTime * 10);
				
				// Make 'trsMatrix' a matrix that translates, rotates and scales the GUI. The position is set to positionVec, the Quaternion is set to identity and the scale is set to one.
				trsMatrix.SetTRS (positionVec, Quaternion.identity, Vector3.one);
			}
			else if (minimize)
			{
				// Interpolate the current vector x component until it reaches zero
				positionVec.x = Mathf.SmoothStep (positionVec.x, 0, Time.deltaTime * 10);
				
				//Make 'trsMatrix' a matrix that translates, rotates and scales the GUI.
				trsMatrix.SetTRS (positionVec, Quaternion.identity, Vector3.one);
			}
		}
		else
		{
			if (maximize)
			{
				// Interpolate the current vector x component until it has the same as value the screen width
				positionVec.x = Mathf.SmoothStep (positionVec.x, 0 - (Screen.width * 0.25f), Time.deltaTime * 10);
				
				// Make 'trsMatrix' a matrix that translates, rotates and scales the GUI. The position is set to positionVec, the Quaternion is set to identity and the scale is set to one.
				trsMatrix.SetTRS (positionVec, Quaternion.identity, Vector3.one);
			}
			else if (minimize)
			{
				// Interpolate the current vector x component until it reaches zero
				positionVec.x = Mathf.SmoothStep (positionVec.x, 0, Time.deltaTime * 10);
				
				// Make 'trsMatrix' a matrix that translates, rotates and scales the GUI.
				trsMatrix.SetTRS (positionVec, Quaternion.identity, Vector3.one);
			}
		}
	}

	/// <summary>
	/// Raises the GUI event.
	/// </summary>
	public virtual void OnGUI ()
	{
		GUI.skin = skin;
		GUI.matrix = trsMatrix;
	
		if (baseScreenSize.width == 0 || Screen.width != baseScreenSize.width)
		{
			Font themeFont;
			Font themeBoldFont;
			themeFont = GUI.skin.box.font;
			
			themeBoldFont = Resources.Load ("GUI/Exo/Exo-SemiBoldItalic") as Font;
			GUI.skin.GetStyle ("ScreenTitle").font = themeBoldFont;
			_fontSize = Mathf.Min (Screen.width, Screen.height) / Ratio;
			_fontSize_Small = Mathf.Min (Screen.width, Screen.height) / 36;
			_fontSize_Large = Mathf.Min (Screen.width, Screen.height) / 19;
			_fontSize_xLarge = Mathf.Min (Screen.width, Screen.height) / 10;
			skin = Resources.Load ("GUI/GMSkin") as GUISkin;
		
			// Init layout 
			GUI.skin.textField.fontSize = (int)_fontSize;
			GUI.skin.textArea.fontSize = (int)_fontSize;
			GUI.skin.toggle.fontSize = (int)_fontSize;
			GUI.skin.label.fontSize = (int)_fontSize;	
			GUI.skin.button.fontSize = (int)_fontSize;	
			GUI.skin.box.fontSize = (int)_fontSize_Small;	
		
			for (int n = 0; n< GUI.skin.customStyles.Length; n++)
			{
				GUI.skin.customStyles [n].fontSize = (int)_fontSize;

				if (MCP.userInfo != null)
				{
					if (MCP.userInfo.themeName != null)
					{
						GUI.skin.customStyles [n].font = themeFont;
					}
				}
			}
		
			baseScreenSize = new Rect (0, 0, Screen.width, Screen.height);

			//GMSkin styles
			GUI.skin.GetStyle ("ActionButton").fontSize = (int)_fontSize_Small;
			GUI.skin.GetStyle ("ScreenTitle").fontSize = (int)_fontSize;
		}
		
		// If half a second has passed since the canvas was create, change it's render mode to world space to handle fullscreen.
		if(GameObject.Find ("Canvas") != null)
		{
			if(MenuScreen.firstFrameTimer > 0.5f)
			{
				if(GameObject.Find ("Canvas").GetComponent<Canvas>().renderMode != RenderMode.WorldSpace)
				{
					GameObject.Find ("Canvas").GetComponent<Canvas>().renderMode = RenderMode.WorldSpace;
				}
			}
			else
			{
				MenuScreen.firstFrameTimer += Time.deltaTime;
			}
		}
		
		if (!comingFromIdle)
		{
#if !UNITY_ANDROID && !UNITY_IPHONE
			// If the mouse is over a button, change the cursor to a hand. Otherwise, change back to a pointer.
			if(mouseOverCount > 0)
			{
//				Cursor.SetCursor(mouseHandSelect, Vector2.zero, CursorMode.Auto);
				mouseOverCount = 0;
			}
			else
			{
//				Cursor.SetCursor(mouseHandNormal, Vector2.zero, CursorMode.Auto);
			}
#endif
		}

		// Do the child's GUI
		DoGUI ();
		// Do the end GUI
		EndGUI ();
	}

	/// <summary>
	/// Does the legacy GUI.
	/// </summary>
	public abstract void DoGUI ();

	/// <summary>
	/// Ends the GUI.
	/// </summary>
	public virtual void EndGUI ()
	{
		//To reset to GUI matrix, just make it equal to a 4x4 identity matrix
		GUI.matrix = Matrix4x4.identity;
	}
	
	/// <summary>
	/// Gets the gizmo anchor.
	/// </summary>
	/// <returns>The gizmo anchor.</returns>
	public float GetGizmoAnchor ()
	{
		Vector2 norm = new Vector2 (positionVec.x, positionVec.y);

		return norm.x * (1f / Screen.width);
	}
	
	/// <summary>
	/// Goes back to the menu.
	/// </summary>
	/// <param name="menuToLoad">Menu to load.</param>
	public void BackToMenu (string menuToLoad)
	{
		Application.LoadLevel (menuToLoad);
	}
	
	/// <summary>
	/// Loads the menu.
	/// </summary>
	/// <param name="menuToLoad">Menu to load.</param>
	public void LoadMenu (string menuToLoad)
	{
		if (lastMenuScreen.Count == 0 || menuToLoad != lastMenuScreen [lastMenuScreen.Count - 1]) // Check that we are not duplicating the last scene on the stack
		{
			// Stop the current level being added if it is selected.
			if(menuToLoad != Application.loadedLevelName)
			{
				lastMenuScreen.Add (Application.loadedLevelName);
			}
		}

		Application.LoadLevel (menuToLoad);
	}
	
	/// <summary>
	/// Go back to the previous screen.
	/// </summary>
	public virtual void Back ()
	{
		if (lastMenuScreen.Count > 0)
		{
			int index = lastMenuScreen.Count - 1;
			string menuToLoad = lastMenuScreen [index];
			lastMenuScreen.RemoveAt (index);

			BackToMenu (menuToLoad);
		}
	}
	
	/// <summary>
	/// Gets the the day postfix.
	/// </summary>
	/// <returns>The day postfix.</returns>
	/// <param name="dayNumber">Day number.</param>
	private string GetDayPostfix(int dayNumber)
	{
		switch(dayNumber)
		{
		case 1:
			return "st";
			
		case 2:
			return "nd";
			
		case 3:
			return "rd";
			
		default:
			return "th";
		}
	}
	
	/// <summary>
	/// Gets the month string.
	/// </summary>
	/// <returns>The month string.</returns>
	/// <param name="monthNumber">Month number.</param>
	private string GetMonthString(int monthNumber)
	{
		switch(monthNumber)
		{
		case 1:
			return "Jan";
			
		case 2:
			return "Feb";
			
		case 3:
			return "Mar";
			
		case 4:
			return "Apr";
			
		case 5:
			return "May";
			
		case 6:
			return "Jun";
			
		case 7:
			return "Jul";
			
		case 8:
			return "Aug";
			
		case 9:
			return "Sep";
			
		case 10:
			return "Oct";
			
		case 11:
			return "Nov";
			
		case 12:
			return "Dec";
		}
		
		return "Jan";
	}
	
	/// <summary>
	/// Gets the date string in a universal way.
	/// </summary>
	/// <returns>The date string.</returns>
	/// <param name="date">Date.</param>
	public string GetDateString(System.DateTime date)
	{
		string dateString = "";
		
		// Add day.
		dateString += date.Day.ToString ();
		// Add day postfix.
		dateString += GetDayPostfix(date.Day);
		
		dateString += " ";
		
		// Add the month.
		dateString += GetMonthString (date.Month);
		
		dateString += " ";
		
		dateString += date.Year.ToString();
		
		return dateString;
	}
	
	/// <summary>
	/// Makes the canvas.
	/// </summary>
	/// <returns>The canvas.</returns>
	/// <param name="isNew">If set to <c>true</c> is new.</param>
	public GameObject MakeCanvas (bool isNew = false)
	{
		// Find the current canvas.
		GameObject localCanvas = GameObject.Find ("Canvas");
		
		// If the canvas does not exist, create one.
		if (localCanvas == null || isNew)
		{
			localCanvas = (GameObject)Instantiate (canvas);
			localCanvas.name = "Canvas";
		}
		
//		localCanvas.GetComponent<RectTransform> ().sizeDelta = new Vector2(Screen.width, Screen.height);
//		localCanvas.GetComponent<RectTransform> ().position = new Vector3(-52.5f, 30f, localCanvas.GetComponent<RectTransform> ().position.z);
		
		localCanvas.GetComponent<Canvas> ().renderMode = RenderMode.ScreenSpaceCamera;
		localCanvas.GetComponent<Canvas> ().worldCamera = GetComponent<Camera>();
		localCanvas.GetComponent<Canvas> ().planeDistance = 10;
		
//		localCanvas.transform.position = new Vector3(-52.5f, 30f, localCanvas.GetComponent<RectTransform> ().position.z);

		MenuScreen.canvasSize = new Rect(0, 0, Screen.width, Screen.height);

		// Find the curret event system.
		GameObject localEventSystem = GameObject.Find ("EventSystem");
		
		// If the event system does not exist, create one.
		if (localEventSystem == null)
		{
			localEventSystem = GameObject.Find ("EventSystem(Clone)");
			
			// If the event system does not exist, create one.
			if (localEventSystem == null)
			{
				localEventSystem = (GameObject)Instantiate (eventSystem);
				eventSystem.name = "EventSystem";
			}
		}
		
		return localCanvas;
	}

	/// <summary>
	/// Toggles fullscreen.
	/// </summary>
	public void ToggleFullscreen ()
	{
//		GameObject localCanvas = GameObject.Find ("Canvas");
		//		localCanvas.GetComponent<Canvas> ().renderMode = RenderMode.WorldSpace;
		
		#if UNITY_STANDALONE
		if(Screen.fullScreen)
		{
			Screen.SetResolution( screenWidth, screenHeight, false );
		}
		else
		{
			Screen.fullScreen = true;
		}
		#endif
		
		Debug.Log ("Toggling fullscreen");
//		Screen.fullScreen = !Screen.fullScreen;

		sndManager.PlayOneShotSound (moveOut);
	}

	/// <summary>
	/// Moves the side gizmo.
	/// </summary>
	public void MoveSideGizmo ()
	{
		GameObject localCanvas = GameObject.Find ("Canvas");

		localCanvas.GetComponent<Canvas> ().renderMode = RenderMode.WorldSpace;
		localCanvas.GetComponent<Animator> ().speed = 5;

		if (minimize)
		{
			// Move gizmo on screen.
			localCanvas.GetComponent<Animator> ().SetTrigger ("SlideOn");
		}
		else
		{
			// Move gizmo off screen.
			localCanvas.GetComponent<Animator> ().SetTrigger ("SlideOff");
		}

		minimize = !minimize;
	}

	/// <summary>
	/// Makes the title bar.
	/// </summary>
	/// <returns>The title bar.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="title">Title.</param>
	/// <param name="guiStyle">GUI style.</param>
	public GameObject MakeTitleBar (GameObject parent, string title, string guiStyle = null)
	{
		// Instantiate scroll area.
		GameObject instText = new GameObject ();//(GameObject)Instantiate(text);
		// Set scroll area parent.
		instText.AddComponent<Text> ();
		instText.transform.SetParent (parent.transform);
		instText.GetComponent<RectTransform> ().anchorMin = new Vector2 (0f, 1f);
		instText.GetComponent<RectTransform> ().anchorMax = new Vector2 (0f, 1f);
		instText.GetComponent<RectTransform> ().pivot = Vector2.zero;

		// Set scroll area position.
		instText.transform.localPosition = new Vector2 (Screen.width * -0.4f, Screen.height * 0.35f);
		instText.transform.localScale = Vector3.one;
		// Set scroll area size. Scroll area starts at full screen so modify to make smaller.
		Vector2 sizeModifier = new Vector2 (Screen.width * 0.8f, Screen.height * 0.15f);
		instText.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		// Set text
		instText.GetComponent<Text> ().text = title;
		
		if (guiStyle != null && guiStyle != "" && skin.GetStyle (guiStyle) != null)
		{
			instText.GetComponent<Text> ().alignment = skin.GetStyle (guiStyle).alignment;
			instText.GetComponent<Text> ().color = skin.GetStyle (guiStyle).normal.textColor;
			instText.GetComponent<Text> ().fontSize = skin.GetStyle (guiStyle).fontSize;
			if (skin.GetStyle (guiStyle).font != null)
			{
				instText.GetComponent<Text> ().font = skin.GetStyle (guiStyle).font;
			}
			else
			{
				instText.GetComponent<Text> ().font = defaultFont;
			}

		}
		else
		{
			// Set text size.
			instText.GetComponent<Text> ().fontSize = (int)_fontSize_Large;
			// Set text alignment.
			instText.GetComponent<Text> ().resizeTextForBestFit = false;
			instText.GetComponent<Text> ().font = defaultFont;
			instText.GetComponent<Text> ().fontStyle = FontStyle.Bold;
			instText.GetComponent<Text> ().alignment = TextAnchor.MiddleCenter;
		}

		instText.name = "Title";
		
		return instText;
	}
	
	/// <summary>
	/// Makes the side gizmo.
	/// </summary>
	/// <returns>The side gizmo.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="includeSideMenu">If set to <c>true</c> include side menu.</param>
	/// <param name="backEnabled">If set to <c>true</c> back enabled.</param>
	public GameObject MakeSideGizmo (GameObject parent, bool includeSideMenu, bool backEnabled = true)
	{
        return new GameObject();
		GameObject sideGizmo = new GameObject ();
		sideGizmo.name = "SideGizmo";
		
		// Back button.
		List<MyVoidFunc> funcList;
		
		if (includeSideMenu)
		{
			// Side menu button.
			funcList = new List<MyVoidFunc>
			{
				MoveSideGizmo
			};
			GameObject sideMenuButton = MakeImageButton (parent, menuButton, new Rect (Screen.width * 0.105f + (Screen.width * 0.4f), Screen.height * 0.125f, Screen.width * 0.065f, Screen.width * 0.065f), funcList);
			sideMenuButton.name = "SideMenuButton";
			sideMenuButton.transform.SetParent(sideGizmo.transform, true);
			sideMenuButton.transform.localScale = Vector3.one;
			// Side menu background.
			GameObject sideGizmoBackground = MakeImage (parent, new Rect (Screen.width - (Screen.width * 0.4f), Screen.height * 0.1f, Screen.width * 0.26f, Screen.height), menuBackground);
			sideGizmoBackground.name = "SideGizmoBackground";
			sideGizmoBackground.transform.SetParent (sideGizmo.transform, true);
			sideGizmoBackground.transform.localScale = Vector3.one;
			
			float yOffset = Screen.height * 0.25f;
			
			// Home button icon.
			GameObject homeButtonIcon = MakeImage (parent, new Rect (Screen.width * 1.001f - (Screen.width * 0.4f), yOffset, Screen.width * 0.05f, Screen.width * 0.05f), menuIconHome);
			homeButtonIcon.name = "HomeButtonIcon";
			homeButtonIcon.transform.SetParent(sideGizmo.transform);
			homeButtonIcon.transform.localScale = Vector3.one;
			// Home button.
			GameObject homeButton = MakeGizmoButton (parent, MCP.Text (2701)/*"Home"*/, menuBackground, new Rect (Screen.width * 1.0f - (Screen.width * 0.34f), yOffset, Screen.width * 0.2f, Screen.height * 0.08f), CommonTasks.LoadLevel, "Main_Screen", "PopUpContent");
			homeButton.name = "HomeButton";
			homeButton.transform.SetParent(sideGizmo.transform);
			homeButton.transform.localScale = Vector3.one;
			
			yOffset += Screen.height * 0.1f;
			
			// Settings button icon.
			GameObject settingsButtonIcon = MakeImage (parent, new Rect (Screen.width * 1.001f - (Screen.width * 0.4f), yOffset, Screen.width * 0.05f, Screen.width * 0.05f), menuIconSettings);
			settingsButtonIcon.name = "SettingsButtonIcon";
			settingsButtonIcon.transform.SetParent(sideGizmo.transform);
			settingsButtonIcon.transform.localScale = Vector3.one;
			// Settings button.
			GameObject settingsButton = MakeGizmoButton (parent, MCP.Text (2702)/*"Settings"*/, menuBackground, new Rect (Screen.width * 1.0f - (Screen.width * 0.34f), yOffset, Screen.width * 0.2f, Screen.height * 0.08f), CommonTasks.LoadLevel, "Settings_Screen", "PopUpContent");
			settingsButton.name = "SettingsButton";
			settingsButton.transform.SetParent(sideGizmo.transform);
			settingsButton.transform.localScale = Vector3.one;
			
//			yOffset += Screen.height * 0.1f;
//			
//			// Help button icon.
//			GameObject helpButtonIcon = MakeImage (parent, new Rect (Screen.width * 1.001f, yOffset, Screen.width * 0.05f, Screen.width * 0.05f), menuIconHelp);
//			helpButtonIcon.name = "HelpButtonIcon";
//			helpButtonIcon.transform.SetParent(sideGizmo.transform);
//			helpButtonIcon.transform.localScale = Vector3.one;
//			// Help button.
//			GameObject helpButton = MakeGizmoButton (parent, MCP.Text(2703)/*"Help"*/, menuBackground, new Rect (Screen.width * 1.0f, yOffset, Screen.width * 0.2f, Screen.height * 0.08f), CommonTasks.LoadLevel, "FAQ_Screen", "PopUpContent");
//			helpButton.name = "HelpButton";
//			helpButton.transform.SetParent(sideGizmo.transform);
//			helpButton.transform.localScale = Vector3.one;
//			
//			yOffset += Screen.height * 0.1f;
//			
//			// Contact button icon.
//			GameObject contactButtonIcon = MakeImage (parent, new Rect (Screen.width * 1.001f, yOffset, Screen.width * 0.05f, Screen.width * 0.05f), menuIconContact);
//			contactButtonIcon.name = "ContactButtonIcon";
//			contactButtonIcon.transform.SetParent (sideGizmo.transform);
//			contactButtonIcon.transform.localScale = Vector3.one;
//			// Contact button.
//			GameObject contactButton = MakeGizmoButton (parent, MCP.Text(2704)/*"Contact"*/, menuBackground, new Rect (Screen.width * 1.0f, yOffset, Screen.width * 0.2f, Screen.height * 0.08f), CommonTasks.LoadLevel, "Contact_Screen", "PopUpContent");
//			contactButton.name = "ContactButton";
//			contactButton.transform.SetParent (sideGizmo.transform);
//			contactButton.transform.localScale = Vector3.one;
			
#if UNITY_ANDROID || UNITY_IPHONE
#else
			yOffset += Screen.height * 0.1f;
			
			// Full screen button icon.
			GameObject fullscreenButtonIcon = MakeImage (parent, new Rect (Screen.width * 1.001f - (Screen.width * 0.4f), yOffset, Screen.width * 0.05f, Screen.height * 0.08f), menuIconFullscreen);
			fullscreenButtonIcon.name = "FullscreenButtonIcon";
			fullscreenButtonIcon.transform.SetParent(sideGizmo.transform);
			fullscreenButtonIcon.transform.localScale = Vector3.one;
			// Full screen button.
			funcList = new List<MyVoidFunc>
			{
				ToggleFullscreen
			};
			GameObject fullscreenButton = MakeGizmoButton (parent, MCP.Text(2705)/*"Fullscreen"*/, menuBackground,new Rect (Screen.width * 1.0f - (Screen.width * 0.34f), yOffset, Screen.width * 0.2f, Screen.height * 0.08f), funcList, "PopUpContent" );
			fullscreenButton.name = "FullscreenButton";
			fullscreenButton.transform.SetParent(sideGizmo.transform);
			fullscreenButton.transform.localScale = Vector3.one;
#endif
			
			yOffset += Screen.height * 0.1f;
			
			// About button icon.
			GameObject aboutButtonIcon = MakeImage (parent, new Rect (Screen.width * 1.001f - (Screen.width * 0.4f), yOffset, Screen.width * 0.05f, Screen.width * 0.05f), menuIconAbout);
			aboutButtonIcon.name = "AboutButtonIcon";
			aboutButtonIcon.transform.SetParent(sideGizmo.transform);
			aboutButtonIcon.transform.localScale = Vector3.one;
			// About button.
			GameObject aboutButton = MakeGizmoButton (parent, MCP.Text(2706)/*"About"*/, menuBackground, new Rect (Screen.width * 1.0f - (Screen.width * 0.34f), yOffset, Screen.width * 0.2f, Screen.height * 0.08f), CommonTasks.LoadLevel, "About_Screen", "PopUpContent");
			aboutButton.name = "AboutButton";
			aboutButton.transform.SetParent(sideGizmo.transform);
			aboutButton.transform.localScale = Vector3.one;
			
			yOffset += Screen.height * 0.1f;
			
			// Logout button icon.
			GameObject logoutButtonIcon = MakeImage (parent, new Rect (Screen.width * 1.001f - (Screen.width * 0.4f), yOffset, Screen.width * 0.05f, Screen.width * 0.05f), menuIconLogout);
			logoutButtonIcon.name = "LogoutButtonIcon";
			logoutButtonIcon.transform.SetParent(sideGizmo.transform);
			logoutButtonIcon.transform.localScale = Vector3.one;
			// Logout button.
			GameObject logoutButton = MakeGizmoButton (parent, MCP.Text(2707)/*"Logout"*/, menuBackground, new Rect (Screen.width * 1.0f - (Screen.width * 0.34f), yOffset, Screen.width * 0.2f, Screen.height * 0.08f), CommonTasks.LoadLevel, "Login_Screen", "PopUpContent");
			logoutButton.name = "LogoutButton";
			logoutButton.transform.SetParent(sideGizmo.transform);
			logoutButton.transform.localScale = Vector3.one;
			
		}
		
		if (backEnabled) 
		{
			// Back button.
			funcList = new List<MyVoidFunc>
			{
				Back
			};
			GameObject backButton = MakeImageButton (parent, backButtonSprite, new Rect (Screen.width * 0.025f, Screen.height * 0.025f, Screen.width * 0.06f, Screen.width * 0.06f), funcList);
			backButton.name = "BackButton";
			
			if (Application.loadedLevelName == "Main_Screen" && backEnabled)
			{
				Destroy (backButton);
			}
		}
		
		// Set position of sideGizmo
		sideGizmo.transform.SetParent(parent.transform);
		sideGizmo.transform.localPosition = new Vector3 (Screen.width * 0.4f - (Screen.width * 0.5f), Screen.height * -0.4f, 0);
		sideGizmo.transform.localScale = Vector4.one;
		return sideGizmo;
	}
	
	/// <summary>
	/// Makes the text goal map panels.
	/// </summary>
	/// <param name="parent">Parent.</param>
	private void MakeTextGoalMapPanels(GameObject parent)
	{
		// Make the why panels.
		GameObject why1PanelObject = MakeImage (parent, new Rect(Screen.width * 0.475f, Screen.height * -0.05f, Screen.width * 0.025f, Screen.height * 0.5f), menuBackground);
		why1PanelObject.name = "Why1PanelObject";
		why1PanelObject.transform.eulerAngles = new Vector3(0, 0, 60f);
		why1PanelObject.transform.SetParent (parent.transform);
		
		GameObject why2PanelObject = MakeImage (parent, new Rect(Screen.width * 0.49f, Screen.height * 0.25f, Screen.width * 0.025f, Screen.height * 0.8f), menuBackground);
		why2PanelObject.name = "Why2PanelObject";
		why2PanelObject.transform.SetParent (parent.transform);
		
		GameObject why3PanelObject = MakeImage (parent, new Rect(Screen.width * 0.525f, Screen.height * -0.0f, Screen.width * 0.025f, Screen.height * 0.4f), menuBackground);
		why3PanelObject.name = "Why3PanelObject";
		why3PanelObject.transform.eulerAngles = new Vector3(0, 0, -60f);
		why3PanelObject.transform.SetParent (parent.transform);
		
		// Make the goal panels.
		GameObject goal2PanelObject = MakeImage (parent, new Rect(Screen.width * 0.2f, Screen.height * 0.45f, Screen.width * 0.2f, Screen.height * 0.05f), menuBackground);
		goal2PanelObject.name = "goal2PanelObject";
		goal2PanelObject.transform.SetParent (parent.transform);
		
		GameObject goal3PanelObject = MakeImage (parent, new Rect(Screen.width * 0.2f, Screen.height * 0.6f, Screen.width * 0.2f, Screen.height * 0.05f), menuBackground);
		goal3PanelObject.name = "goal3PanelObject";
		goal3PanelObject.transform.SetParent (parent.transform);
		
		GameObject goal4PanelObject = MakeImage (parent, new Rect(Screen.width * 0.575f, Screen.height * 0.45f, Screen.width * 0.2f, Screen.height * 0.05f), menuBackground);
		goal4PanelObject.name = "goal4PanelObject";
		goal4PanelObject.transform.SetParent (parent.transform);
		
		GameObject goal5PanelObject = MakeImage (parent, new Rect(Screen.width * 0.575f, Screen.height * 0.6f, Screen.width * 0.2f, Screen.height * 0.05f), menuBackground);
		goal5PanelObject.name = "goal5PanelObject";
		goal5PanelObject.transform.SetParent (parent.transform);
		
		// Make the who panels.
		GameObject who1PanelObject = MakeImage (parent, new Rect(Screen.width * 0.27f, Screen.height * 1.025f, Screen.width * 0.2f, Screen.height * 0.05f), menuBackground);
		who1PanelObject.name = "who1PanelObject";
		who1PanelObject.transform.SetParent (parent.transform);
		
		GameObject who2PanelObject = MakeImage (parent, new Rect(Screen.width * 0.27f, Screen.height * 1.275f, Screen.width * 0.2f, Screen.height * 0.05f), menuBackground);
		who2PanelObject.name = "Who2PanelObject";
		who2PanelObject.transform.SetParent (parent.transform);
		
		GameObject who3PanelObject = MakeImage (parent, new Rect(Screen.width * 0.27f, Screen.height * 1.525f, Screen.width * 0.2f, Screen.height * 0.05f), menuBackground);
		who3PanelObject.name = "Who3PanelObject";
		who3PanelObject.transform.SetParent (parent.transform);
		
		// Make the how panels.
		GameObject how1PanelObject = MakeImage (parent, new Rect(Screen.width * 0.54f, Screen.height * 1.025f, Screen.width * 0.2f, Screen.height * 0.05f), menuBackground);
		how1PanelObject.name = "How1PanelObject";
		how1PanelObject.transform.SetParent (parent.transform);
		
		GameObject how2PanelObject = MakeImage (parent, new Rect(Screen.width * 0.54f, Screen.height * 1.275f, Screen.width * 0.2f, Screen.height * 0.05f), menuBackground);
		how2PanelObject.name = "How2PanelObject";
		how2PanelObject.transform.SetParent (parent.transform);
		
		GameObject how3PanelObject = MakeImage (parent, new Rect(Screen.width * 0.54f, Screen.height * 1.525f, Screen.width * 0.2f, Screen.height * 0.05f), menuBackground);
		how3PanelObject.name = "How3PanelObject";
		how3PanelObject.transform.SetParent (parent.transform);
	}
	
	/// <summary>
	/// Makes the pic goal map panels.
	/// </summary>
	/// <param name="parent">Parent.</param>
	private void MakePicGoalMapPanels(GameObject parent)
	{
		GameObject picGoalMapBackground = MakeImage (parent, new Rect(Screen.width * 0.2f, Screen.height * 0.2f, Screen.width * 0.6f, Screen.height * 1.6f), goalMapBackground);
		picGoalMapBackground.name = "PicGoalMapBackground";
	}
	
	private Sprite GetElementBackgroundSprite(string label)
	{
		if(label.ToLower().Contains ("goal"))
		{
			if(label.ToLower().Contains ("1"))
			{
				return orangeBackground;
			}
			
			return lightPurpleBackground;
		}
		if(label.ToLower().Contains ("why"))
		{
			return darkPurpleBackground;
		}
		if(label.ToLower().Contains ("who"))
		{
			return blueBackground;
		}
		if(label.ToLower().Contains ("how"))
		{
			return mediumPurpleBackground;
		}
		
		return lightPurpleBackground;
	}
	
	/// <summary>
	/// Makes the goal map element.
	/// </summary>
	/// <param name="parent">Parent.</param>
	/// <param name="rect">Rect.</param>
	/// <param name="label">Label.</param>
	private void MakeGoalMapElement(GameObject parent, Rect rect, string label)
	{
		// Split the label to get info for object name in heirachy
		string[] splitLabel = label.Split (new char[]{' '});	// Expecting string as "Element #" eg "Who 1".
		if(splitLabel.Length < 2)
		{
			splitLabel = new string[2];
			splitLabel[0] = label;
			splitLabel[1] = "";
		}
		
		GameObject backgroundObject = MakeImage (parent, rect, darkPurpleBackground, true);
		backgroundObject.name = splitLabel[0] + "Background" + splitLabel[1];
		GameObject imageObject = MakeImage (parent, new Rect(rect.x + (Screen.width * 0.01f), rect.y + (Screen.height * 0.01f), rect.width - (Screen.width * 0.02f), rect.height - (Screen.height * 0.02f)), GetElementBackgroundSprite(label), false);
		imageObject.name = splitLabel[0] + "Image" + splitLabel[1];
		GameObject labelObject = MakeLabel (parent, label, new Rect(rect.x + (Screen.width * 0.015f), rect.y + (Screen.height * 0.01f), rect.width - (Screen.width * 0.03f), rect.height - (Screen.height * 0.02f)), TextAnchor.MiddleCenter, "InnerLabel");
		labelObject.name = splitLabel[0] + "Label" + splitLabel[1];
		
		if(label.ToLower ().Contains (MCP.Text (2711).ToLower ()))
		{
			labelObject.GetComponent<Text>().color = bmDarkBlue;
		}
		else if(label.ToLower ().Contains (MCP.Text (2714).ToLower ()) || label.ToLower ().Contains (MCP.Text (2713).ToLower()))
		{
			labelObject.GetComponent<Text>().color = bmDarkBlue;
		}
		else
		{
			labelObject.GetComponent<Text>().color = Color.white;
		}
		
		labelObject.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Wrap;
		labelObject.GetComponent<Text>().resizeTextForBestFit = true;
		labelObject.GetComponent<Text>().resizeTextMaxSize = (int)_fontSize_Large;
		labelObject.GetComponent<Text>().resizeTextMaxSize = (int)_fontSize_Small;

		backgroundObject.transform.SetParent (parent.transform);
		imageObject.transform.SetParent (parent.transform);
		labelObject.transform.SetParent (parent.transform);
	}
	
	/// <summary>
	/// Makes the text goal map who elements.
	/// </summary>
	/// <returns>The text goal map who elements.</returns>
	private GameObject MakeTextGoalMapWhoElements()
	{
		GameObject textWhoParent = new GameObject();
		textWhoParent.name = "TextWhoParent";
		
		MakeGoalMapElement (textWhoParent, new Rect(Screen.width * 0.1f, Screen.height * 0.95f, Screen.width * 0.2f, Screen.height * 0.2f), MCP.Text (2708)/*"Who*/ + " 1");
		
		MakeGoalMapElement (textWhoParent, new Rect(Screen.width * 0.1f, Screen.height * 1.2f, Screen.width * 0.2f, Screen.height * 0.2f), MCP.Text (2708)/*"Who"*/ + " 2");
		
		MakeGoalMapElement (textWhoParent, new Rect(Screen.width * 0.1f, Screen.height * 1.45f, Screen.width * 0.2f, Screen.height * 0.2f), MCP.Text (2708)/*"Who"*/ + " 3");
		
		return textWhoParent;
	}
	
	/// <summary>
	/// Makes the pic goal map who elements.
	/// </summary>
	/// <returns>The pic goal map who elements.</returns>
	private GameObject MakePicGoalMapWhoElements()
	{
		GameObject picWhoParent = new GameObject();
		picWhoParent.name = "PicWhoParent";
		
		GameObject picWho1Image = MakeImage (picWhoParent, new Rect(Screen.width * 0.11f, Screen.height * 0.97f, Screen.width * 0.2f, Screen.height * 0.2f), lightPurpleBackground, true);
		picWho1Image.name = "WhoImage1";
		
		GameObject picWho2Image = MakeImage (picWhoParent, new Rect(Screen.width * 0.11f, Screen.height * 1.2f, Screen.width * 0.2f, Screen.height * 0.2f), lightPurpleBackground, true);
		picWho2Image.name = "WhoImage2";
		
		GameObject picWho3Image = MakeImage (picWhoParent, new Rect(Screen.width * 0.11f, Screen.height * 1.43f, Screen.width * 0.2f, Screen.height * 0.2f), lightPurpleBackground, true);
		picWho3Image.name = "WhoImage3";
		
		return picWhoParent;
	}
	
	/// <summary>
	/// Makes the text goal map how elements.
	/// </summary>
	/// <returns>The text goal map how elements.</returns>
	public GameObject MakeTextGoalMapHowElements()
	{
		GameObject textHowParent = new GameObject();
		textHowParent.name = "TextHowParent";
		
		MakeGoalMapElement (textHowParent, new Rect(Screen.width * 0.7f, Screen.height * 0.95f, Screen.width * 0.2f, Screen.height * 0.2f), MCP.Text (2709)/*"How"*/ + " 1");
		
		MakeGoalMapElement (textHowParent, new Rect(Screen.width * 0.7f, Screen.height * 1.2f, Screen.width * 0.2f, Screen.height * 0.2f), MCP.Text (2709)/*"How"*/ + " 2");
		
		MakeGoalMapElement (textHowParent, new Rect(Screen.width * 0.7f, Screen.height * 1.45f, Screen.width * 0.2f, Screen.height * 0.2f), MCP.Text (2709)/*"How"*/ + " 3");
		
		return textHowParent;
	}
	
	/// <summary>
	/// Makes the pic goal map how elements.
	/// </summary>
	/// <returns>The pic goal map how elements.</returns>
	public GameObject MakePicGoalMapHowElements()
	{
		GameObject picHowParent = new GameObject();
		picHowParent.name = "PicHowParent";
		
		GameObject picHow1Image = MakeImage (picHowParent, new Rect(Screen.width * 0.655f, Screen.height * 0.97f, Screen.width * 0.2f, Screen.height * 0.2f), lightPurpleBackground, true);
		picHow1Image.name = "HowImage1";
		
		GameObject picHow2Image = MakeImage (picHowParent, new Rect(Screen.width * 0.655f, Screen.height * 1.2f, Screen.width * 0.2f, Screen.height * 0.2f), lightPurpleBackground, true);
		picHow2Image.name = "HowImage2";
		
		GameObject picHow3Image = MakeImage (picHowParent, new Rect(Screen.width * 0.655f, Screen.height * 1.43f, Screen.width * 0.2f, Screen.height * 0.2f), lightPurpleBackground, true);
		picHow3Image.name = "HowImage3";
		
		return picHowParent;
	}
	
	/// <summary>
	/// Makes the text goal map why elements.
	/// </summary>
	/// <returns>The text goal map why elements.</returns>
	private GameObject MakeTextGoalMapWhyElements()
	{
		GameObject textWhyParent = new GameObject();
		textWhyParent.name = "TextWhyParent";
		
		MakeGoalMapElement (textWhyParent, new Rect(Screen.width * 0.15f, Screen.height * 0.1f, Screen.width * 0.2f, Screen.height * 0.2f), MCP.Text (2710)/*"Why"*/ + " 1");
		
		MakeGoalMapElement (textWhyParent, new Rect(Screen.width * 0.4f, Screen.height * 0.05f, Screen.width * 0.2f, Screen.height * 0.2f), MCP.Text (2710)/*"Why"*/ + " 2");
		
		MakeGoalMapElement (textWhyParent, new Rect(Screen.width * 0.65f, Screen.height * 0.1f, Screen.width * 0.2f, Screen.height * 0.2f), MCP.Text (2710)/*"Why"*/ + " 3");
		
		return textWhyParent;
	}
	
	/// <summary>
	/// Makes the pic goal map why elements.
	/// </summary>
	/// <returns>The pic goal map why elements.</returns>
	private GameObject MakePicGoalMapWhyElements()
	{
		GameObject picWhyParent = new GameObject();
		picWhyParent.name = "PicWhyParent";
		
		GameObject picWhy1Image = MakeImage (picWhyParent, new Rect(Screen.width * 0.08f, Screen.height * 0.1f, Screen.width * 0.2f, Screen.height * 0.2f), lightPurpleBackground, true);
		picWhy1Image.name = "WhyImage1";
		
		GameObject picWhy2Image = MakeImage (picWhyParent, new Rect(Screen.width * 0.4f, Screen.height * 0.01f, Screen.width * 0.2f, Screen.height * 0.2f), lightPurpleBackground, true);
		picWhy2Image.name = "WhyImage2";
		
		GameObject picWhy3Image = MakeImage (picWhyParent, new Rect(Screen.width * 0.7f, Screen.height * 0.1f, Screen.width * 0.2f, Screen.height * 0.2f), lightPurpleBackground, true);
		picWhy3Image.name = "WhyImage3";
		
		return picWhyParent;
	}
	
	/// <summary>
	/// Makes the text goal map goal elements.
	/// </summary>
	/// <returns>The text goal map goal elements.</returns>
	private GameObject MakeTextGoalMapGoalElements()
	{
		GameObject textGoalParent = new GameObject();
		textGoalParent.name = "TextGoalParent";
		
		MakeGoalMapElement (textGoalParent, new Rect(Screen.width * 0.4f, Screen.height * 0.4f, Screen.width * 0.2f, Screen.height * 0.3f), MCP.Text (2711)/*"Goal"*/ + " 1");
		
		MakeGoalMapElement (textGoalParent, new Rect(Screen.width * 0.1f, Screen.height * 0.35f, Screen.width * 0.2f, Screen.height * 0.2f), MCP.Text (2711)/*"Goal"*/ + " 2");
		
		MakeGoalMapElement (textGoalParent, new Rect(Screen.width * 0.1f, Screen.height * 0.55f, Screen.width * 0.2f, Screen.height * 0.2f), MCP.Text (2711)/*"Goal"*/ + " 3");
		
		MakeGoalMapElement (textGoalParent, new Rect(Screen.width * 0.7f, Screen.height * 0.35f, Screen.width * 0.2f, Screen.height * 0.2f), MCP.Text (2711)/*"Goal"*/ + " 4");
		
		MakeGoalMapElement (textGoalParent, new Rect(Screen.width * 0.7f, Screen.height * 0.55f, Screen.width * 0.2f, Screen.height * 0.2f), MCP.Text (2711)/*"Goal"*/ + " 5");
		
		return textGoalParent;
	}
	
	/// <summary>
	/// Makes the pic goal map goal elements.
	/// </summary>
	/// <returns>The pic goal map goal elements.</returns>
	private GameObject MakePicGoalMapGoalElements()
	{
		GameObject picGoalParent = new GameObject();
		picGoalParent.name = "PicGoalParent";
		
		GameObject picGoal1Image = MakeImage (picGoalParent, new Rect(Screen.width * 0.4f, Screen.height * 0.45f, Screen.width * 0.2f, Screen.height * 0.2f), lightPurpleBackground, true);
		picGoal1Image.name = "GoalImage1";
		
		GameObject picGoal2Image = MakeImage (picGoalParent, new Rect(Screen.width * 0.05f, Screen.height * 0.375f, Screen.width * 0.2f, Screen.height * 0.2f), lightPurpleBackground, true);
		picGoal2Image.name = "GoalImage2";
		
		GameObject picGoal3Image = MakeImage (picGoalParent, new Rect(Screen.width * 0.08f, Screen.height * 0.65f, Screen.width * 0.2f, Screen.height * 0.2f), lightPurpleBackground, true);
		picGoal3Image.name = "GoalImage3";
		
		GameObject picGoal4Image = MakeImage (picGoalParent, new Rect(Screen.width * 0.735f, Screen.height * 0.375f, Screen.width * 0.2f, Screen.height * 0.2f), lightPurpleBackground, true);
		picGoal4Image.name = "GoalImage4";
		
		GameObject picGoal5Image = MakeImage (picGoalParent, new Rect(Screen.width * 0.7f, Screen.height * 0.65f, Screen.width * 0.2f, Screen.height * 0.2f), lightPurpleBackground, true);
		picGoal5Image.name = "GoalImage5";
		
		return picGoalParent;
	}
	
	/// <summary>
	/// Makes the text goal map timeline elements.
	/// </summary>
	/// <returns>The text goal map timeline elements.</returns>
	private GameObject MakeTextGoalMapTimelineElements()
	{
		GameObject textTimelineParent = new GameObject();
		textTimelineParent.name = "TextTimelineParent";
		
		GameObject timelineBackgroundObject = MakeImage (textTimelineParent, new Rect(Screen.width * 0.5f, Screen.height * 1.175f, Screen.width * 0.6f, Screen.height * 0.15f), darkPurpleBackground, true);
		timelineBackgroundObject.name = "TimelineBackground";
		GameObject imageObject = MakeImage (textTimelineParent, new Rect(Screen.width * 0.5f, Screen.height * 1.15f, Screen.width * 0.575f, Screen.height * 0.13f), lightPurpleBackground, false);
		imageObject.name = "TimelineImage";
		GameObject labelObject = MakeLabel (textTimelineParent, MCP.Text (2712)/*"Timeline"*/, new Rect(Screen.width * 0.5f, Screen.height * 1.075f, Screen.width * 0.4f, Screen.height * 0.13f), TextAnchor.MiddleCenter, "InnerLabel");
		labelObject.name = "TimelineLabel";
		labelObject.GetComponent<Text>().color = bmDarkBlue;
		
		// Rotate the timeline box.
		timelineBackgroundObject.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
		timelineBackgroundObject.transform.eulerAngles = new Vector3(0, 0, 90f);
		imageObject.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
		imageObject.transform.eulerAngles = new Vector3(0, 0, 90f);
		labelObject.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
		labelObject.transform.eulerAngles = new Vector3(0, 0, 90f);
		
		timelineBackgroundObject.transform.SetParent (textTimelineParent.transform);
		imageObject.transform.SetParent (textTimelineParent.transform);
		labelObject.transform.SetParent (textTimelineParent.transform);
		
		MakeGoalMapElement (textTimelineParent, new Rect(Screen.width * 0.4f, Screen.height * 0.75f, Screen.width * 0.2f, Screen.height * 0.15f), MCP.Text (2714)/*"Stop"*/);
		GameObject finishDateLabel = MakeLabel (textTimelineParent, "-" + MCP.Text (2715)/*date*/ + "-", new Rect(Screen.width * 0.4f, Screen.height * 0.79f, Screen.width * 0.2f, Screen.height * 0.15f), "InnerLabel");
		finishDateLabel.name = "FinishDate";
		finishDateLabel.GetComponent<Text>().color = bmDarkBlue;
		
		MakeGoalMapElement (textTimelineParent, new Rect(Screen.width * 0.4f, Screen.height * 1.6f, Screen.width * 0.2f, Screen.height * 0.15f), MCP.Text (2713)/*"Start"*/);
		GameObject startDateLabel = MakeLabel (textTimelineParent, "-" + MCP.Text (2715)/*date*/ + "-", new Rect(Screen.width * 0.4f, Screen.height * 1.64f, Screen.width * 0.2f, Screen.height * 0.15f), "InnerLabel");
		startDateLabel.name = "StartDate";
		startDateLabel.GetComponent<Text>().color = bmDarkBlue;
		
		return textTimelineParent;
	}
	
	/// <summary>
	/// Makes the pic goal map timeline elements.
	/// </summary>
	/// <returns>The pic goal map timeline elements.</returns>
	private GameObject MakePicGoalMapTimelineElements()
	{
		GameObject picTimelineParent = new GameObject();
		picTimelineParent.name = "PicTimelineParent";
		
		GameObject labelObject = MakeLabel (picTimelineParent, MCP.Text (2712)/*"Timeline"*/, new Rect(Screen.width * 0.5f, Screen.height * 1.08f, Screen.width * 0.2f, Screen.height * 0.13f), TextAnchor.MiddleCenter, "InnerLabel");
		labelObject.name = "TimelineLabel";
		labelObject.GetComponent<Text>().color = bmDarkBlue;
		
		// Rotate the timeline label.
		labelObject.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
		labelObject.transform.eulerAngles = new Vector3(0, 0, 90f);
		labelObject.transform.SetParent (picTimelineParent.transform);
		
		GameObject startDateLabel = MakeLabel (picTimelineParent, MCP.Text (2713)/*"Start"*/, new Rect(Screen.width * 0.4f, Screen.height * 1.6525f, Screen.width * 0.2f, Screen.height * 0.15f), "InnerLabel");
		startDateLabel.name = "StartDateLabel";
		startDateLabel.GetComponent<Text>().color = bmDarkBlue;
		
		GameObject startDateText = MakeLabel (picTimelineParent, "-" + MCP.Text (2715)/*date*/ + "-", new Rect(Screen.width * 0.4f, Screen.height * 1.69f, Screen.width * 0.2f, Screen.height * 0.15f), "InnerLabel");
		startDateText.name = "StartDate";
		startDateText.GetComponent<Text>().color = bmDarkBlue;
		
		GameObject finishDateLabel = MakeLabel (picTimelineParent, MCP.Text (2714)/*"Stop"*/, new Rect(Screen.width * 0.4f, Screen.height * 0.73f, Screen.width * 0.2f, Screen.height * 0.15f), "InnerLabel");
		finishDateLabel.name = "FinishDateLabel";
		finishDateLabel.GetComponent<Text>().color = bmDarkBlue;
		
		GameObject finishDateText = MakeLabel (picTimelineParent, "-" + MCP.Text (2715)/*date*/ + "-", new Rect(Screen.width * 0.4f, Screen.height * 0.765f, Screen.width * 0.2f, Screen.height * 0.15f), "InnerLabel");
		finishDateText.name = "FinishDate";
		finishDateText.GetComponent<Text>().color = bmDarkBlue;
		
		return picTimelineParent;
	}
	
	/// <summary>
	/// Makes the goal map.
	/// </summary>
	/// <returns>The goal map.</returns>
	/// <param name="parent">Parent.</param>
	public GameObject MakeGoalMap(GameObject parent)
	{
		GameObject goalMapParent = new GameObject();
		goalMapParent.name = "GoalMapParent";
		goalMapParent.transform.SetParent(parent.transform);
		
		// Text
		GameObject goalMapTextParent = new GameObject();
		goalMapTextParent.name = "GoalMapTextParent";
		goalMapTextParent.transform.SetParent (goalMapParent.transform);
		
		// Connecting panels
		GameObject textPanelParent = new GameObject();
		textPanelParent.name = "TextPanelParent";
		textPanelParent.transform.SetParent (goalMapTextParent.transform);
		
		MakeTextGoalMapPanels (textPanelParent);
		
		// Who - left
		GameObject textWhoParent = MakeTextGoalMapWhoElements ();
		textWhoParent.name = "TextWhoParent";
		textWhoParent.transform.SetParent (goalMapTextParent.transform);
		// How - right
		GameObject textHowParent = MakeTextGoalMapHowElements();
		textHowParent.name = "TextHowParent";
		textHowParent.transform.SetParent (goalMapTextParent.transform);
		// Whys - top
		GameObject textWhyParent = MakeTextGoalMapWhyElements();
		textWhyParent.name = "TextWhyParent";
		textWhyParent.transform.SetParent (goalMapTextParent.transform);
		// Goals - middle
		GameObject textGoalParent = MakeTextGoalMapGoalElements();
		textGoalParent.name = "TextGoalParent";
		textGoalParent.transform.SetParent (goalMapTextParent.transform);
		// Timeline - bottom
		GameObject textTimelineParent = MakeTextGoalMapTimelineElements();
		textTimelineParent.name = "TextTimelineParent";
		textTimelineParent.transform.SetParent (goalMapTextParent.transform);
		
		
		// Pics
		GameObject goalMapPicsParent = new GameObject();
		goalMapPicsParent.name = "GoalMapPicsParent";
		goalMapPicsParent.transform.SetParent (goalMapParent.transform);
		
		// Connecting panels
		GameObject picPanelParent = new GameObject();
		picPanelParent.name = "PicPanelParent";
		picPanelParent.transform.SetParent (goalMapPicsParent.transform);
		
		MakePicGoalMapPanels (picPanelParent);
		
		// Who - left
		GameObject picWhoParent = MakePicGoalMapWhoElements ();
		picWhoParent.name = "PicWhoParent";
		picWhoParent.transform.SetParent (goalMapPicsParent.transform);
		// How - right
		GameObject picHowParent = MakePicGoalMapHowElements();
		picHowParent.name = "PicHowParent";
		picHowParent.transform.SetParent (goalMapPicsParent.transform);
		// Whys - top
		GameObject picWhyParent = MakePicGoalMapWhyElements();
		picWhyParent.name = "PicWhyParent";
		picWhyParent.transform.SetParent (goalMapPicsParent.transform);
		// Goals - middle
		GameObject picGoalParent = MakePicGoalMapGoalElements();
		picGoalParent.name = "PicGoalParent";
		picGoalParent.transform.SetParent (goalMapPicsParent.transform);
		// Timeline - bottom
		GameObject picTimelineParent = MakePicGoalMapTimelineElements();
		picTimelineParent.name = "PicTimelineParent";
		picTimelineParent.transform.SetParent (goalMapPicsParent.transform);
		
		goalMapParent.transform.localScale = Vector3.one;
		goalMapParent.AddComponent<PopGM_Prefab>();
		
		return goalMapParent;
	}
	
	/// <summary>
	/// Makes a label.
	/// </summary>
	/// <returns>The label.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="title">Title.</param>
	/// <param name="rect">Rect.</param>
	/// <param name="guiStyle">GUI style.</param>
	public GameObject MakeLabel (GameObject parent, string title, Rect rect, string guiStyle = null)
	{
		// Create the label.
		GameObject instText = new GameObject ();
		instText.AddComponent<Text> ();
		
		// Set label parent.
		instText.transform.SetParent (parent.transform, true);
		
		// Set label position.
		instText.GetComponent<RectTransform> ().anchorMin = new Vector2 (0f, 1f);
		instText.GetComponent<RectTransform> ().anchorMax = new Vector2 (0f, 1f);
		instText.GetComponent<RectTransform> ().pivot = Vector2.zero;

		instText.transform.localPosition = new Vector2 (rect.x - (Screen.width * 0.5f), -(rect.y + rect.height) + (Screen.height * 0.5f));
		instText.transform.localScale = Vector3.one;
		
		// Set label size.
		Vector2 sizeModifier = new Vector2 (rect.width, rect.height);
		instText.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		
		// Set text
		instText.GetComponent<Text> ().text = title;
		if (guiStyle != null && guiStyle != "" && skin.GetStyle (guiStyle) != null)
		{
			instText.GetComponent<Text> ().alignment = skin.GetStyle (guiStyle).alignment;
			instText.GetComponent<Text> ().color = skin.GetStyle (guiStyle).normal.textColor;
			instText.GetComponent<Text> ().fontSize = skin.GetStyle (guiStyle).fontSize;
			if (skin.GetStyle (guiStyle).font != null)
			{
				instText.GetComponent<Text> ().font = skin.GetStyle (guiStyle).font;
			}
			else
			{
				instText.GetComponent<Text> ().font = defaultFont;
			}
		}
		else
		{
			// Set text size.
			instText.GetComponent<Text> ().fontSize = (int)_fontSize;
			
			// Set text alignment.
			instText.GetComponent<Text> ().resizeTextForBestFit = false;
			instText.GetComponent<Text> ().font = defaultFont;
			instText.GetComponent<Text> ().alignment = TextAnchor.UpperCenter;
		}

		return instText;
	}

	/// <summary>
	/// Makes a label.
	/// </summary>
	/// <returns>The label.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="title">Title.</param>
	/// <param name="rect">Rect.</param>
	/// <param name="anchor">Anchor.</param>
	/// <param name="guiStyle">GUI style.</param>
	public GameObject MakeLabel (GameObject parent, string title, Rect rect, TextAnchor anchor, string guiStyle = null)
	{
		// Create the label.
		GameObject instText = new GameObject ();
		instText.AddComponent<Text> ();
		
		// Set label parent.
		instText.GetComponent<Text> ().font = defaultFont;
		instText.GetComponent<Text> ().alignment = anchor;
		instText.transform.SetParent (parent.transform, true);
		
		// Set label position.
		instText.GetComponent<RectTransform> ().anchorMin = new Vector2 (0f, 1f);
		instText.GetComponent<RectTransform> ().anchorMax = new Vector2 (0f, 1f);
		instText.GetComponent<RectTransform> ().pivot = Vector2.zero;

		instText.transform.localPosition = new Vector2 (rect.x - (Screen.width * 0.5f), -rect.y - rect.height + (Screen.height * 0.5f));
		instText.transform.localScale = Vector3.one;
		
		// Set label size.
		Vector2 sizeModifier = new Vector2 (rect.width, rect.height);
		instText.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		
		// Set text
		instText.GetComponent<Text> ().text = title;
		
		if (guiStyle != null && guiStyle != "" && skin.GetStyle (guiStyle) != null)
		{
			instText.GetComponent<Text> ().color = skin.GetStyle (guiStyle).normal.textColor;
			instText.GetComponent<Text> ().fontSize = skin.GetStyle (guiStyle).fontSize;
			if (skin.GetStyle (guiStyle).font != null)
			{
				instText.GetComponent<Text> ().font = skin.GetStyle (guiStyle).font;
			}
			else
			{
				instText.GetComponent<Text> ().font = defaultFont;
			}
		}
		else
		{
			// Set text size.
			instText.GetComponent<Text> ().fontSize = (int)_fontSize;
			// Set text alignment.
			instText.GetComponent<Text> ().resizeTextForBestFit = false;
			instText.GetComponent<Text> ().font = defaultFont;
		}

		return instText;
	}

	/// <summary>
	/// Makes the action button.
	/// </summary>
	/// <returns>The action button.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="buttonText">Button text.</param>
	/// <param name="rect">Rect.</param>
	/// <param name="func">Func.</param>
	/// <param name="i">The index.</param>
	/// <param name="guiStyle">GUI style.</param>
	public GameObject MakeActionButton (GameObject parent, string buttonText, Rect rect, Func<int,bool> func, int i, string guiStyle = null)
	{
		// Create the button.
		GameObject instButton = new GameObject ();
		instButton.name = buttonText;
		
		// Set button name.
		instButton.AddComponent<Image> ();
		instButton.AddComponent<Button> ();
		instButton.AddComponent<HoverPointer>();
		
		// Create the button text object.
		GameObject text = new GameObject ();
		text.AddComponent<Text> ();
		text.name = "ActionButtonText";
		text.transform.SetParent (instButton.transform);
		// Set the button text attributes.
		text.GetComponent<Text> ().text = buttonText;
		text.GetComponent<Text> ().font = defaultFont;
		text.GetComponent<Text> ().color = bmOrange;
		text.GetComponent<Text> ().alignment = TextAnchor.MiddleCenter;
		// Set the button attributes.
		instButton.GetComponent<RectTransform> ().anchorMin = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().anchorMax = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().pivot = Vector2.zero;
		instButton.GetComponent<Image> ().type = Image.Type.Sliced;
		instButton.GetComponent<Image> ().sprite = buttonBackground;
		// Set button parent.
		instButton.transform.SetParent (parent.transform, true);

		// Set button position.
		instButton.transform.localPosition = new Vector2 (rect.x - (Screen.width * 0.5f), parent.GetComponent<RectTransform> ().rect.height - (rect.y + rect.height) - (Screen.height * 0.5f));
		instButton.transform.localScale = Vector3.one;

		// Set the button size.
		Vector2 sizeModifier = new Vector2 (rect.width, rect.height);
		instButton.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		text.GetComponent<RectTransform>().sizeDelta = sizeModifier;
		
		if (guiStyle != null && guiStyle != "" && skin.GetStyle (guiStyle) != null)
		{
			Text textTmp = text.GetComponent<Text> ();
			textTmp.alignment = skin.GetStyle (guiStyle).alignment;
			textTmp.color = skin.GetStyle (guiStyle).normal.textColor;
			textTmp.fontSize = skin.GetStyle (guiStyle).fontSize;
			
			if (skin.GetStyle (guiStyle).font != null)
			{
				textTmp.font = skin.GetStyle (guiStyle).font;
			}
			else
			{
				textTmp.GetComponent<Text> ().font = defaultFont;
			}
			
			if(skin.GetStyle (guiStyle).normal.background != null)
			{
				instButton.GetComponent<Image> ().sprite = Sprite.Create (skin.GetStyle (guiStyle).normal.background, new Rect(0,0,skin.GetStyle (guiStyle).normal.background.width,skin.GetStyle (guiStyle).normal.background.height),new Vector2(0.5f,0.5f));
			}
			else
			{
				instButton.GetComponent<Image> ().enabled = false;
			}
		}
		else
		{
			Text textTmp = text.GetComponent<Text> ();
			
			textTmp.font = defaultFont;
			textTmp.fontSize = (int)_fontSize;
			textTmp.resizeTextForBestFit = false;
			textTmp.fontStyle = FontStyle.Bold;
		}
		
		// Reset button listeners.
		instButton.GetComponent<Button> ().onClick.RemoveAllListeners ();
		// Add onClick listeners to button.
		instButton.GetComponent<Button> ().onClick.AddListener (
			delegate
			{
				func.Invoke (i);
				
				// Make clicking noise when clicked
				if (sndManager != null)
				{	
					sndManager.PlaySound (buttonClick);
				}
			});

		return instButton;
	}
	
	/// <summary>
	/// Makes an action button.
	/// </summary>
	/// <returns>The action button.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="icon">Icon.</param>
	/// <param name="rect">Rect.</param>
	/// <param name="func">Func.</param>
	/// <param name="i">The index.</param>
	/// <param name="guiStyle">GUI style.</param>
	public GameObject MakeActionButton (GameObject parent, Sprite icon, Rect rect, Func<int,bool> func, int i, string guiStyle = null)
	{
		// Create the button.
		GameObject instButton = new GameObject ();

		// Set button name.
		instButton.AddComponent<Image> ();
		instButton.AddComponent<Button> ();
		instButton.AddComponent<HoverPointer>();
		
		instButton.GetComponent<RectTransform> ().anchorMin = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().anchorMax = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().pivot = Vector2.zero;
		instButton.GetComponent<Image> ().type = Image.Type.Sliced;
		instButton.GetComponent<Image> ().sprite = buttonBackground;
		// Set button parent.
		instButton.transform.SetParent (parent.transform, true);
		
		// Set button position.
		instButton.transform.localPosition = new Vector2 (rect.x - (Screen.width * 0.5f), parent.GetComponent<RectTransform> ().rect.height - (rect.y + rect.height) - (Screen.height * 0.5f));
		instButton.transform.localScale = Vector3.one;
	
		// Set button size.
		Vector2 sizeModifier = new Vector2 (rect.width, rect.height);
		instButton.GetComponent<RectTransform> ().sizeDelta = sizeModifier;

		instButton.GetComponent<Image>().sprite = icon;

		// Reset button listeners.
		instButton.GetComponent<Button> ().onClick.RemoveAllListeners ();
		// Add onClick listeners to button.
		instButton.GetComponent<Button> ().onClick.AddListener (
			delegate
			{
				func.Invoke (i);
				
				// Make clicking noise when clicked
				if (sndManager != null)
				{	
					sndManager.PlaySound (buttonClick);
				}
			});
		
		return instButton;
	}

	/// <summary>
	/// Makes a button.
	/// </summary>
	/// <returns>The button.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="buttonText">Button text.</param>
	/// <param name="rect">Rect.</param>
	/// <param name="functionNames">Function names.</param>
	/// <param name="finalButtonText">Final button text.</param>
	/// <param name="task">Task.</param>
	/// <param name="levelName">Level name.</param>
	/// <param name="guiStyle">GUI style.</param>
	public GameObject MakeButton (GameObject parent, string buttonText, Rect rect, List<MyPopupFunc> functionNames, string finalButtonText, CommonTasks task, string levelName = "", string guiStyle = null)
	{
		// Create the button.
		GameObject instButton = new GameObject ();
		instButton.name = buttonText;
		
		// Set button name.
		instButton.AddComponent<Text> ();
		instButton.AddComponent<Button> ();
		instButton.AddComponent<HoverPointer>();
		
		// Create the button image.
		GameObject image = new GameObject ();
		image.AddComponent<Image> ();
		image.transform.SetParent (instButton.transform);
		// Set the button attributes.
		instButton.GetComponent<Text> ().resizeTextForBestFit = false;
		instButton.GetComponent<Text> ().fontSize = (int)_fontSize;
		instButton.GetComponent<Text> ().fontStyle = FontStyle.Bold;
		instButton.GetComponent<Text> ().text = buttonText;
		instButton.GetComponent<Text> ().font = defaultFont;
		instButton.GetComponent<Text> ().color = bmOrange;

		instButton.GetComponent<RectTransform> ().anchorMin = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().anchorMax = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().pivot = Vector2.zero;

		// Set button parent.
		instButton.transform.SetParent (parent.transform, true);

		// Set button position.
		instButton.transform.localPosition = new Vector2 (rect.x - (Screen.width * 0.5f), rect.y - rect.height + (Screen.height * 0.5f));
		instButton.transform.localScale = Vector3.one;

		// Set the button size.
		Vector2 sizeModifier = new Vector2 (rect.width, rect.height);
		instButton.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		image.GetComponent<RectTransform> ().sizeDelta = sizeModifier;

		// Reset button listeners.
		instButton.GetComponent<Button> ().onClick.RemoveAllListeners ();
		
		// Add onClick listeners to button.
		instButton.GetComponent<Button> ().onClick.AddListener (
			delegate()
			{
				for (int i = 0; i < functionNames.Count; i++)
				{
					functionNames [i] (finalButtonText, task, levelName);
				}
				
				// Make clicking noise when clicked
				if (sndManager != null)
				{
					sndManager.PlaySound (buttonClick);
				}
			});
	
		// Apply the GUI style.
		if (guiStyle != null && guiStyle != "" && skin.GetStyle (guiStyle) != null)
		{
			Text buttonTextObj = instButton.GetComponent<Text> ();
			buttonTextObj.alignment = skin.GetStyle (guiStyle).alignment;
			buttonTextObj.color = skin.GetStyle (guiStyle).normal.textColor;
			buttonTextObj.fontSize = skin.GetStyle (guiStyle).fontSize;
			if (skin.GetStyle (guiStyle).font != null)
			{
				buttonTextObj.font = skin.GetStyle (guiStyle).font;
			}
			else
			{
				buttonTextObj.GetComponent<Text> ().font = defaultFont;
			}
		}
		// Otherwise, just set to defaults.
		else
		{
			Text buttonTextObj = instButton.GetComponent<Text> ();

			buttonTextObj.font = defaultFont;
		}

		return instButton;
	}

	/// <summary>
	/// Makes a button.
	/// </summary>
	/// <returns>The button.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="buttonText">Button text.</param>
	/// <param name="content">Background content.</param>
	/// <param name="rect">Rect.</param>
	/// <param name="task">Task.</param>
	/// <param name="menuName">Menu name.</param>
	/// <param name="guiStyle">GUI style.</param>
	public GameObject MakeButton (GameObject parent, string buttonText, Sprite content, Rect rect, CommonTasks task, string menuName = "", string guiStyle = null)
	{
		// Instantiate button.
		GameObject instButton = new GameObject ();
		instButton.name = buttonText;
		
		// Set button name.
		instButton.AddComponent<Image> ();
		instButton.AddComponent<Button> ();
		instButton.AddComponent<HoverPointer>();
		
		// Make the text object for the button.
		GameObject text = new GameObject ();
		text.name = "Text Component";
		text.AddComponent<Text> ();
		text.transform.SetParent (instButton.transform);
		text.GetComponent<Text> ().resizeTextForBestFit = false;
		text.GetComponent<Text> ().fontSize = (int)_fontSize;
		text.GetComponent<Text> ().fontStyle = FontStyle.Bold;
		text.GetComponent<Text> ().text = buttonText;
		text.GetComponent<Text> ().font = defaultFont;
		text.GetComponent<Text> ().color = bmOrange;
		text.GetComponent<Text> ().alignment = TextAnchor.MiddleCenter;
		
		instButton.GetComponent<RectTransform> ().anchorMin = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().anchorMax = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().pivot = Vector2.zero;

		instButton.transform.SetParent (parent.transform, true);
		
		// Set button position.
		instButton.transform.localPosition = new Vector2 (rect.x - (Screen.width * 0.5f), -rect.y - rect.height + (Screen.height * 0.5f));
		instButton.transform.localScale = Vector3.one;

		// Set the button size.
		Vector2 sizeModifier = new Vector2 (rect.width, rect.height);
		instButton.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		sizeModifier = new Vector2 (rect.width * 0.9f, rect.height * 0.9f);
		text.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		
		// Set sprite.
		if (content != null)
		{
			instButton.GetComponent<Image> ().type = Image.Type.Sliced;
			instButton.GetComponent<Image> ().sprite = content;
		}
		else
		{
			instButton.GetComponent<Image> ().enabled = false;
		}
		
		// Apply the GUI style.
		if (guiStyle != null && guiStyle != "" && skin.GetStyle (guiStyle) != null)
		{
			Text textTmp = text.GetComponent<Text> ();
			textTmp.alignment = skin.GetStyle (guiStyle).alignment;
			textTmp.color = skin.GetStyle (guiStyle).normal.textColor;
			textTmp.fontSize = skin.GetStyle (guiStyle).fontSize;
			
			if (skin.GetStyle (guiStyle).font != null)
			{
				textTmp.font = skin.GetStyle (guiStyle).font;
				textTmp.color = skin.GetStyle (guiStyle).normal.textColor;
			}
			else
			{
				textTmp.GetComponent<Text> ().font = defaultFont;
			}
			if(skin.GetStyle (guiStyle).normal.background != null)
			{
				instButton.GetComponent<Image> ().sprite = Sprite.Create (skin.GetStyle (guiStyle).normal.background, new Rect(0,0,skin.GetStyle (guiStyle).normal.background.width,skin.GetStyle (guiStyle).normal.background.height),new Vector2(0.5f,0.5f));
			}
		}
		// Otherwise, set to defaults.
		else
		{
			Text textTmp = text.GetComponent<Text> ();
			
			textTmp.font = defaultFont;
			textTmp.fontSize = (int)_fontSize;
			textTmp.resizeTextForBestFit = false;
		}
		
		// Reset button listeners.
		instButton.GetComponent<Button> ().onClick.RemoveAllListeners ();
		// Add onClick listeners to button.
		instButton.GetComponent<Button> ().onClick.AddListener (() =>
		{
			switch (task)
			{
			case CommonTasks.DestroyParent:
				Destroy (parent);
				break;
				
			case CommonTasks.LoadLevel:
				LoadMenu (menuName);
				break;
				
			case CommonTasks.OpenUrl:
				Application.OpenURL(menuName);
				break;
			}
			
			// Make clicking noise when clicked
			if (sndManager != null)
			{
				sndManager.PlaySound (buttonClick);
			}
		});
	
		return instButton;
	}
	
	/// <summary>
	/// Makes an icon button.
	/// </summary>
	/// <returns>The icon button.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="buttonText">Button text.</param>
	/// <param name="content">Background content.</param>
	/// <param name="icon">Icon.</param>
	/// <param name="rect">Rect.</param>
	/// <param name="task">Task.</param>
	/// <param name="menuName">Menu name.</param>
	/// <param name="guiStyle">GUI style.</param>
	public GameObject MakeIconButton (GameObject parent, string buttonText, Sprite content, Sprite icon, Rect rect, CommonTasks task, string menuName = "", string guiStyle = null)
	{
		// Instantiate button.
		GameObject instButton = new GameObject ();
		instButton.name = buttonText;
		instButton.AddComponent<Image> ();
		instButton.AddComponent<Button> ();
		instButton.AddComponent<HoverPointer>();
		
		// Create the text object for the button.
		GameObject text = new GameObject ();
		instButton.transform.SetParent(parent.transform, true);
		text.name = "Text Component";
		text.AddComponent<Text> ();
		text.transform.SetParent(instButton.transform);
		text.transform.localPosition = new Vector3 (0, rect.height * -0.3f, 0f);
		
		text.GetComponent<Text> ().resizeTextForBestFit = false;
		text.GetComponent<Text> ().fontSize = (int)_fontSize;
		text.GetComponent<Text> ().text = buttonText;
		text.GetComponent<Text> ().font = defaultFont;
		text.GetComponent<Text> ().fontStyle = FontStyle.Bold;
		text.GetComponent<Text> ().alignment = TextAnchor.MiddleCenter;
		instButton.GetComponent<RectTransform> ().anchorMin = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().anchorMax = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().pivot = new Vector2(0f, 1f);
		
		// Set button position.
		instButton.transform.localPosition = new Vector2 (rect.x - (Screen.width * 0.5f), -rect.y + (Screen.height * 0.5f));
		instButton.transform.localScale = Vector3.one;
		
		// Set button size.
		Vector2 sizeModifier = new Vector2 (rect.width, rect.height);
		instButton.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		sizeModifier = new Vector2 (rect.width * 0.9f, rect.height * 0.25f);
		text.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		
		// Make image object for the button icon.
		GameObject iconObj = MakeImage (parent, new Rect (rect.x + (rect.width * 0.5f) - (rect.height * 0.3f), rect.y + (rect.height * 0.15f), rect.height * 0.6f, rect.height * 0.5f), icon);
		iconObj.name = "Icon";
		iconObj.transform.SetParent (instButton.transform, true);
		
		// Set sprite.
		if (content != null)
		{
			instButton.GetComponent<Image> ().type = Image.Type.Sliced;
			instButton.GetComponent<Image> ().sprite = content;
		}
		
		// Reset button listeners.
		instButton.GetComponent<Button> ().onClick.RemoveAllListeners ();
		// Add onClick listeners to button.
		instButton.GetComponent<Button> ().onClick.AddListener (() =>
			{
				switch (task)
				{
				case CommonTasks.DestroyParent:
					Destroy (parent);
					break;
				case CommonTasks.LoadLevel:
					LoadMenu (menuName);
					break;
				}
				
				// Make clicking noise when clicked
				if (sndManager != null)
				{	
					sndManager.PlaySound (buttonClick);
				}
			});
		
		return instButton;
	}
	
	/// <summary>
	/// Makes an icon button.
	/// </summary>
	/// <returns>The icon button.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="buttonText">Button text.</param>
	/// <param name="content">Content.</param>
	/// <param name="icon">Icon.</param>
	/// <param name="rect">Rect.</param>
	/// <param name="funcList">Func list.</param>
	/// <param name="guiStyle">GUI style.</param>
	public GameObject MakeIconButton (GameObject parent, string buttonText, Sprite content, Sprite icon, Rect rect, List<MyVoidFunc> funcList, string guiStyle = null)
	{
		// Instantiate button.
		GameObject instButton = new GameObject ();
		instButton.name = buttonText;
		instButton.AddComponent<Image> ();
		instButton.AddComponent<Button> ();
		instButton.AddComponent<HoverPointer>();
		
		// Make the text object for the button.
		GameObject text = new GameObject ();
		instButton.transform.SetParent(parent.transform, true);
		text.name = "Text Component";
		text.AddComponent<Text> ();
		text.transform.SetParent(instButton.transform);
		text.transform.localPosition = new Vector3 (0, rect.height * -0.3f, 0f);
		
		text.GetComponent<Text> ().resizeTextForBestFit = false;
		text.GetComponent<Text> ().fontSize = (int)_fontSize;
		text.GetComponent<Text> ().fontStyle = FontStyle.Bold;
		text.GetComponent<Text> ().text = buttonText;
		text.GetComponent<Text> ().font = defaultFont;
		text.GetComponent<Text> ().alignment = TextAnchor.MiddleCenter;
		instButton.GetComponent<RectTransform> ().anchorMin = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().anchorMax = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().pivot = new Vector2(0f, 1f);
		
		// Set button position.
		instButton.transform.localPosition = new Vector2 (rect.x - (Screen.width * 0.5f), -rect.y + (Screen.height * 0.5f));
		instButton.transform.localScale = Vector3.one;
		
		// Make the image object for the icon.
		if(icon != null)
		{
			GameObject iconObj = MakeImage (parent, new Rect (rect.x + (rect.width * 0.5f) - (rect.height * 0.25f), rect.y + (rect.height * 0.15f), rect.height * 0.5f, rect.height * 0.5f), icon);
			iconObj.name = "Icon";
			iconObj.transform.SetParent (instButton.transform, true);
		}
		
		// Set the button size.
		Vector2 sizeModifier = new Vector2 (rect.width, rect.height);
		instButton.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		sizeModifier = new Vector2 (rect.width * 0.9f, rect.height * 0.25f);
		text.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		
		// Set sprite.
		if (content != null)
		{
			instButton.GetComponent<Image> ().type = Image.Type.Sliced;
			instButton.GetComponent<Image> ().sprite = content;
		}
		else
		{
			instButton.GetComponent<Image> ().enabled = false;
		}
		
		// Reset button listeners.
		instButton.GetComponent<Button> ().onClick.RemoveAllListeners ();
		// Add onClick listeners to button.
		instButton.GetComponent<Button> ().onClick.AddListener (() =>
		    {
				for (int i = 0; i < funcList.Count; i++)
				{
					funcList[i]();
				}
				
				// Make clicking noise when clicked
				if (sndManager != null)
				{
					sndManager.PlaySound (buttonClick);
				}
			});
		
		return instButton;
	}
	
	/// <summary>
	/// Makes a gizmo button.
	/// </summary>
	/// <returns>The gizmo button.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="buttonText">Button text.</param>
	/// <param name="content">Background content.</param>
	/// <param name="rect">Rect.</param>
	/// <param name="task">Task.</param>
	/// <param name="menuName">Menu name.</param>
	/// <param name="guiStyle">GUI style.</param>
	public GameObject MakeGizmoButton (GameObject parent, string buttonText, Sprite content, Rect rect, CommonTasks task, string menuName = "", string guiStyle = null)
	{
		// Instantiate button.
		GameObject instButton = new GameObject ();
		instButton.name = buttonText;
		instButton.AddComponent<Image> ();
		instButton.AddComponent<Button> ();
		instButton.AddComponent<HoverPointer>();
		
		// Make the text object for the button.
		GameObject text = new GameObject ();
		text.name = "Text Component";
		text.AddComponent<Text> ();
		text.transform.SetParent(instButton.transform);
		text.GetComponent<Text> ().fontSize = (int)_fontSize;
		text.GetComponent<Text> ().text = buttonText;
		text.GetComponent<Text> ().font = defaultFont;
		text.GetComponent<Text> ().resizeTextForBestFit = false;
		text.GetComponent<Text> ().alignment = TextAnchor.MiddleLeft;
		instButton.GetComponent<RectTransform> ().anchorMin = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().anchorMax = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().pivot = Vector2.zero;

		// Set button parent.
		instButton.transform.SetParent(parent.transform, true);
		
		// Set button position.
		instButton.transform.localPosition = new Vector2 (rect.x - (Screen.width * 0.5f), -rect.y - rect.height + (Screen.height * 0.5f));
		instButton.transform.localScale = Vector3.one;
		
		// Set the button size.
		Vector2 sizeModifier = new Vector2 (rect.width, rect.height);
		instButton.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		sizeModifier = new Vector2 (rect.width * 0.9f, rect.height * 0.9f);
		text.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		
		// Set sprite.
		if (content != null)
		{
			instButton.GetComponent<Image> ().type = Image.Type.Sliced;
			instButton.GetComponent<Image> ().sprite = content;
		}
		
		// Reset button listeners.
		instButton.GetComponent<Button> ().onClick.RemoveAllListeners ();
		// Add onClick listeners to button.
		instButton.GetComponent<Button> ().onClick.AddListener (() =>
			{
				switch (task)
				{
				case CommonTasks.DestroyParent:
						Destroy (parent);
						break;
				case CommonTasks.LoadLevel:
						LoadMenu (menuName);
						break;
				}
				
				// Make clicking noise when clicked
				if (sndManager != null)
				{
					sndManager.PlaySound (buttonClick);
				}
			});

		return instButton;
	}
	
	/// <summary>
	/// Makes a gizmo button.
	/// </summary>
	/// <returns>The gizmo button.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="buttonText">Button text.</param>
	/// <param name="content">Background content.</param>
	/// <param name="rect">Rect.</param>
	/// <param name="functionNames">Function names.</param>
	/// <param name="guiStyle">GUI style.</param>
	public GameObject MakeGizmoButton (GameObject parent, string buttonText, Sprite content, Rect rect, List<MyVoidFunc> functionNames, string guiStyle = null)
	{
		// Instantiate button.
		GameObject instButton = new GameObject ();
		instButton.name = buttonText;
		instButton.AddComponent<Image> ();
		instButton.AddComponent<Button> ();
		instButton.AddComponent<HoverPointer>();
		
		// Make the text object for the button.
		GameObject text = new GameObject ();
		text.name = "Text Component";
		text.AddComponent<Text> ();
		text.transform.SetParent(instButton.transform);
		text.GetComponent<Text> ().resizeTextForBestFit = false;
		text.GetComponent<Text> ().fontSize = (int)_fontSize;
		text.GetComponent<Text> ().text = buttonText;
		text.GetComponent<Text> ().font = defaultFont;
		text.GetComponent<Text> ().alignment = TextAnchor.MiddleLeft;
		instButton.GetComponent<RectTransform> ().anchorMin = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().anchorMax = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().pivot = Vector2.zero;
		
		// Set button parent.
		instButton.transform.SetParent(parent.transform, true);
		
		// Set button position.
		instButton.transform.localPosition = new Vector2 (rect.x - (Screen.width * 0.5f), -rect.y - rect.height + (Screen.height * 0.5f));
		instButton.transform.localScale = Vector3.one;
		
		// Set the button size.
		Vector2 sizeModifier = new Vector2 (rect.width, rect.height);
		instButton.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		sizeModifier = new Vector2 (rect.width * 0.9f, rect.height * 0.9f);
		text.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		
		// Set sprite.
		if (content != null)
		{
			instButton.GetComponent<Image> ().type = Image.Type.Sliced;
			instButton.GetComponent<Image> ().sprite = content;
		}
		
		// Reset button listeners.
		instButton.GetComponent<Button> ().onClick.RemoveAllListeners ();
		// Add onClick listeners to button.
		instButton.GetComponent<Button> ().onClick.AddListener (() =>
			{
				for (int i = 0; i < functionNames.Count; i++)
				{
					functionNames [i] ();
				}
				
				// Make clicking noise when clicked
				if (sndManager != null)
				{
					sndManager.PlaySound (buttonClick);
				}
			});
		
		return instButton;
	}
	
	/// <summary>
	/// Makes an image button.
	/// </summary>
	/// <returns>The image button.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="image">Image.</param>
	/// <param name="rect">Rect.</param>
	/// <param name="functionNames">Function names.</param>
	/// <param name="sliced">If set to <c>true</c> sliced.</param>
	/// <param name="guiStyle">GUI style.</param>
	public GameObject MakeImageButton (GameObject parent, Sprite image, Rect rect, List<MyVoidFunc> functionNames, bool sliced = true, string guiStyle = null)
	{
		// Instantiate button.
		GameObject instButton = new GameObject ();

		// Set button name.
		instButton.AddComponent<Image> ();
		instButton.AddComponent<Button> ();
		instButton.AddComponent<HoverPointer>();
		
		// Set whether the button is sliced.
		if (sliced)
		{
			instButton.GetComponent<Image> ().type = Image.Type.Sliced;
		}

		if (image != null)
		{
			instButton.GetComponent<Image> ().sprite = image;
		}
		else
		{
			Debug.Log ("Null Texture");
		}

		instButton.GetComponent<RectTransform> ().anchorMin = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().anchorMax = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().pivot = Vector2.zero;

		// Set button parent.
		instButton.transform.SetParent (parent.transform, true);

		// Set button position.
		instButton.transform.localPosition = new Vector2 (rect.x - (Screen.width * 0.5f), -rect.y - rect.height + (Screen.height * 0.5f));
		instButton.transform.localScale = Vector3.one;

		// Set the button size.
		Vector2 sizeModifier = new Vector2 (rect.width, rect.height);
		instButton.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		
		// Reset button listeners.
		instButton.GetComponent<Button> ().onClick.RemoveAllListeners ();
		// Add onClick listeners to button.
		instButton.GetComponent<Button> ().onClick.AddListener (() =>
			{
				for (int i = 0; i < functionNames.Count; i++)
				{
					functionNames [i] ();
				}
				
				// Make clicking noise when clicked
				if (sndManager != null)
				{	
					sndManager.PlaySound (buttonClick);
				}
			});

		return instButton;
	}
	
	/// <summary>
	/// Makes a button.
	/// </summary>
	/// <returns>The button.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="buttonText">Button text.</param>
	/// <param name="rect">Rect.</param>
	/// <param name="functionNames">Function names.</param>
	/// <param name="guiStyle">GUI style.</param>
	public GameObject MakeButton (GameObject parent, string buttonText, Rect rect, List<MyVoidFunc> functionNames, string guiStyle = null)
	{
		// Instantiate button.
		GameObject instButton = new GameObject ();
		instButton.name = buttonText;
		instButton.AddComponent<Text> ();
		instButton.AddComponent<Button> ();
		instButton.AddComponent<HoverPointer>();
		
		instButton.GetComponent<Text> ().resizeTextForBestFit = false;
		instButton.GetComponent<Text> ().fontSize = (int)_fontSize;
		instButton.GetComponent<Text> ().fontStyle = FontStyle.Bold;
		instButton.GetComponent<Text> ().text = buttonText;
		instButton.GetComponent<Text> ().font = defaultFont;
		instButton.GetComponent<Text> ().color = bmOrange;
		
		instButton.GetComponent<RectTransform> ().anchorMin = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().anchorMax = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().pivot = Vector2.zero;
		
		// Set button parent.
		instButton.transform.SetParent (parent.transform, true);
		
		// Set button position.
		instButton.transform.localPosition = new Vector2 (rect.x - (Screen.width * 0.5f), -rect.y - rect.height + (Screen.height * 0.5f));
		instButton.transform.localScale = Vector3.one;
		
		// Set the button size.
		Vector2 sizeModifier = new Vector2 (rect.width, rect.height);
		instButton.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		
		// Reset button listeners.
		instButton.GetComponent<Button> ().onClick.RemoveAllListeners ();
		// Add onClick listeners to button.
		instButton.GetComponent<Button> ().onClick.AddListener (() =>
			{
				for (int i = 0; i < functionNames.Count; i++)
				{
					functionNames [i] ();
				}
				
				// Make clicking noise when clicked
				if (sndManager != null)
				{
					
					sndManager.PlaySound (buttonClick);
				}
			});
		
		// Apply the GUi style.
		if (guiStyle != null && guiStyle != "" && skin.GetStyle (guiStyle) != null)
		{
			Text buttonTextObj = instButton.GetComponent<Text> ();
			buttonTextObj.alignment = skin.GetStyle (guiStyle).alignment;
			buttonTextObj.color = skin.GetStyle (guiStyle).normal.textColor;
			buttonTextObj.fontSize = skin.GetStyle (guiStyle).fontSize;
			if (skin.GetStyle (guiStyle).font != null)
			{
				buttonTextObj.font = skin.GetStyle (guiStyle).font;
			}
			else
			{
				buttonTextObj.GetComponent<Text> ().font = defaultFont;
			}
		}
		// Otherwise, set to the default.
		else
		{
			Text buttonTextObj = instButton.GetComponent<Text> ();
			
			buttonTextObj.font = defaultFont;
		}
		
		return instButton;
	}
	
	/// <summary>
	/// Makes a button.
	/// </summary>
	/// <returns>The button.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="buttonText">Button text.</param>
	/// <param name="content">Background content.</param>
	/// <param name="rect">Rect.</param>
	/// <param name="functionNames">Function names.</param>
	/// <param name="guiStyle">GUI style.</param>
	public GameObject MakeButton (GameObject parent, string buttonText, Sprite content, Rect rect, List<MyVoidFunc> functionNames, string guiStyle = null)
	{
		// Instantiate button.
		GameObject instButton = new GameObject ();
		instButton.name = buttonText;
		// Set button name.
		instButton.AddComponent<Image> ();
		instButton.AddComponent<Button> ();
		instButton.AddComponent<HoverPointer>();
		
		// Make the text object for the button.
		GameObject text = new GameObject ();
		text.AddComponent<Text> ();
		text.transform.SetParent (instButton.transform);
		instButton.GetComponent<Button>().targetGraphic = instButton.GetComponent<Image>();
		
		text.GetComponent<Text> ().resizeTextForBestFit = false;
		text.GetComponent<Text> ().fontSize = (int)_fontSize;
		text.GetComponent<Text> ().fontStyle = FontStyle.Bold;
		text.GetComponent<Text> ().text = buttonText;
		text.GetComponent<Text> ().font = defaultFont;
		text.GetComponent<Text> ().alignment = TextAnchor.MiddleCenter;
		text.GetComponent<Text> ().color = bmOrange;
		
		instButton.GetComponent<RectTransform> ().anchorMin = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().anchorMax = new Vector2 (0f, 1f);
		instButton.GetComponent<RectTransform> ().pivot = Vector2.zero;

		// Set button parent.
		instButton.transform.SetParent (parent.transform, true);

		// Set button position.
		instButton.transform.localPosition = new Vector2 (rect.x - (Screen.width * 0.5f), -rect.y - rect.height + (Screen.height * 0.5f));
		instButton.transform.localScale = Vector3.one;
		
		// Set the button size.
		Vector2 sizeModifier = new Vector2 (rect.width, rect.height);
		instButton.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		text.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		
		// Set sprite.
		if (content != null)
		{
			instButton.GetComponent<Image> ().type = Image.Type.Sliced;
			instButton.GetComponent<Image> ().sprite = content;
		}
		else
		{
			Destroy (instButton.GetComponent<Image> ());
		}
		
		// Reset button listeners.
		instButton.GetComponent<Button> ().onClick.RemoveAllListeners ();
		// Add onClick listeners to button.
		instButton.GetComponent<Button> ().onClick.AddListener (() =>
			{
				for (int i = 0; i < functionNames.Count; i++)
				{
					functionNames [i] ();
				}
				
				// Make clicking noise when clicked
				if (sndManager != null)
				{
					
					sndManager.PlaySound (buttonClick);
				}
			});
		
		// Apply the GUI style.
		if (guiStyle != null && guiStyle != "" && skin.GetStyle (guiStyle) != null)
		{
			Text buttonTextObj = text.GetComponent<Text> ();
			buttonTextObj.alignment = skin.GetStyle (guiStyle).alignment;
			buttonTextObj.color = skin.GetStyle (guiStyle).normal.textColor;
			buttonTextObj.fontSize = skin.GetStyle (guiStyle).fontSize;
			if (skin.GetStyle (guiStyle).font != null)
			{
				buttonTextObj.font = skin.GetStyle (guiStyle).font;
			}
			else
			{
				buttonTextObj.GetComponent<Text> ().font = defaultFont;
			}
		}
		// Otherwise, set to the defaults.
		else
		{
			Text buttonTextObj = text.GetComponent<Text> ();

			buttonTextObj.font = defaultFont;
		}

		return instButton;
	}
	
	/// <summary>
	/// Makes a scroll rect.
	/// </summary>
	/// <returns>The scroll rect.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="position">Position.</param>
	/// <param name="content">Content.</param>
	public GameObject MakeScrollRect (GameObject parent, Vector2 position, ref GameObject content)
	{
		// Scroll Rect
		GameObject scrollThing = new GameObject ();
		scrollThing.AddComponent<ScrollRect> ();
		scrollThing.GetComponent<ScrollRect>().horizontal = false;
		scrollThing.name = "ScrollThing";
		scrollThing.GetComponent<RectTransform> ().anchorMax = new Vector2 (0, 1f);
		scrollThing.GetComponent<RectTransform> ().anchorMin = new Vector2 (0, 1f);
		scrollThing.GetComponent<RectTransform> ().pivot = new Vector2 (0, 0);
		scrollThing.GetComponent<ScrollRect>().scrollSensitivity = 25f;

		scrollThing.transform.SetParent (parent.transform, false);
		scrollThing.GetComponent<RectTransform>().localPosition = new Vector2(position.x, -scrollThing.GetComponent<RectTransform>().rect.height - position.y);
		
		// Make the mask for the scroll area.
		GameObject scrollStuff = MakeWindowBackground (scrollThing, new Rect (0, 0, MenuScreen.canvasSize.width * 0.8f, MenuScreen.canvasSize.height * 0.6f), window_back);
		scrollStuff.name = "ScrollStuff";
		scrollStuff.AddComponent<Mask> ();
		
		// Make an object to hold the scroll content.
		GameObject page1content = new GameObject ();// MakeWindowBackground (scrollStuff, new Rect(MenuScreen.canvasSize.width * 0.01f, MenuScreen.canvasSize.height * 0.01f , MenuScreen.canvasSize.width * 0.8f, MenuScreen.canvasSize.height * 0.7f), window_back);
		page1content.AddComponent<RectTransform> ();
		page1content.transform.SetParent (scrollStuff.transform, false);
		page1content.transform.localPosition = new Vector3(0, 0, 0);
		page1content.name = "Page 1 - content";
		content.transform.SetParent(page1content.transform, false);
		
		// Vertical scrollbar
		GameObject scroller = new GameObject ();
		scroller.name = "Scroller";
		scroller.AddComponent<Scrollbar> ();
		scroller.GetComponent<RectTransform> ().anchorMax = new Vector2 (0, 1f);
		scroller.GetComponent<RectTransform> ().anchorMin = new Vector2 (0, 1f);
		scroller.GetComponent<RectTransform> ().pivot = new Vector2 (0, 0);

		scroller.transform.SetParent (parent.transform, false);

		scroller.GetComponent<RectTransform>().localPosition = new Vector2(parent.GetComponent<RectTransform>().rect.width - scroller.GetComponent<RectTransform>().rect.width - position.x, -scrollThing.GetComponent<RectTransform>().rect.height - position.y);
		
		scroller.GetComponent<Scrollbar> ().direction = Scrollbar.Direction.TopToBottom;
		
		// Make an image for the scroller.
		GameObject scrollerImage = MakeImage (scroller, new Rect (0, 0, 1f, 1f), sliderHandle, true);		
	
		scroller.GetComponent<Scrollbar> ().targetGraphic = scrollerImage.GetComponent<Graphic> ();
		GameObject handleRect = MakePanel (scroller,new Rect (0, 0, 50f, 200f));
		handleRect.name = "HandleRect";
		
		scroller.GetComponent<Scrollbar> ().handleRect = handleRect.GetComponent<RectTransform>();
	
		scrollThing.GetComponent<ScrollRect> ().verticalScrollbar = scroller.GetComponent<Scrollbar> ();
		scrollThing.GetComponent<ScrollRect> ().content = page1content.GetComponent<RectTransform> ();
	
		return scrollThing;
	}
	
	/// <summary>
	/// Makes a scroll rect.
	/// </summary>
	/// <returns>The scroll rect.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="rect">Rect.</param>
	/// <param name="content">Content.</param>
	/// <param name="vertical">If set to <c>true</c> vertical.</param>
	/// <param name="horizontal">If set to <c>true</c> horizontal.</param>
	public GameObject MakeScrollRect (GameObject parent, Rect rect, GameObject content, bool vertical = true, bool horizontal = false)
	{
		// Scroll Rect
		GameObject scrollThing = new GameObject ();
		scrollThing.AddComponent<ScrollRect> ();

		scrollThing.name = "ScrollThing";
		
		// Set the scroll rect size.
		Vector2 sizeModifier = new Vector2 (rect.width, rect.height * 0.945f);
		scrollThing.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		
		scrollThing.GetComponent<RectTransform> ().anchorMax = new Vector2 (0, 1f);
		scrollThing.GetComponent<RectTransform> ().anchorMin = new Vector2 (0, 1f);
		scrollThing.GetComponent<RectTransform> ().pivot = new Vector2 (0, 0);
		scrollThing.GetComponent<ScrollRect>().scrollSensitivity = 25f;
		
		scrollThing.transform.SetParent (parent.transform, true);
		scrollThing.GetComponent<RectTransform> ().localPosition = new Vector2 (rect.x - (Screen.width * 0.5f), -rect.y - (rect.height * 1.05f) + (Screen.height * 0.5f));//rect.y + (parent.GetComponent<RectTransform>().rect.height - rect.height)  );
		scrollThing.transform.localScale = Vector3.one;
		
		// Make the mask for the scroll rect.
		GameObject scrollStuff = MakeWindowBackground (scrollThing, new Rect (0 + (Screen.width * 0.5f), -rect.height + (Screen.height * 0.5f), rect.width, rect.height), window_back);
		scrollStuff.name = "ScrollStuff";
		scrollStuff.AddComponent<Mask> ();
		scrollStuff.transform.localScale = Vector3.one;

		scrollThing.GetComponent<ScrollRect> ().content = scrollStuff.GetComponent<RectTransform> ();

		// Make an object for the scroll content.
		GameObject page1content = MakePanel (scrollStuff, new Rect(0,0, rect.width, rect.height));
		page1content.name = "Content";

		page1content.GetComponent<RectTransform> ().anchorMax = new Vector2 (0, 1f);
		page1content.GetComponent<RectTransform> ().anchorMin = new Vector2 (0, 1f);
		page1content.GetComponent<RectTransform> ().pivot = new Vector2 (0, -1);
		
		page1content.transform.SetParent (scrollStuff.transform);
		
		// If this is a vertical scroll rect, move the content to the top of the scroll rect.
		if(vertical)
		{
			page1content.GetComponent<RectTransform>().transform.localPosition = new Vector2(0, -rect.height);
		}
		else
		{
			page1content.GetComponent<RectTransform>().transform.localPosition = new Vector2(0, Screen.height * -0.625f);
		}
		page1content.transform.localScale = Vector3.one;
		
		// Vertical scrollbar
		if(vertical)
		{
			// Make the background of the scroller.
			GameObject vScrollerBackground = MakeImage (scrollThing, new Rect(rect.width * 0.985f + (Screen.width * 0.5f), -rect.height * 1.0f + (Screen.height * 0.5f), rect.width * 0.08f, rect.height), sliderBackgroundVert);
			vScrollerBackground.name = "vScrollerBackground";
			
			// Make the scroller object.
			GameObject vScroller = new GameObject();
			vScroller.name = "VScroller";
			vScroller.AddComponent<Scrollbar> ();
			vScroller.GetComponent<RectTransform> ().pivot = new Vector2 (0, 0);
			vScroller.transform.SetParent (scrollThing.transform, true);
			vScroller.transform.localPosition = new Vector2 (rect.width * 0.985f, 0);
			vScroller.GetComponent<Scrollbar> ().direction = Scrollbar.Direction.BottomToTop;
			sizeModifier = new Vector2 (rect.width * 0.08f, rect.height);
			
			vScroller.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
			
			// Make the image for the scroller.
			GameObject vScrollerImage = MakeImage (vScroller, new Rect (0 + (Screen.width * 0.5f), -rect.height * 1.0f + (Screen.height * 0.5f), 0, 0), sliderForegroundVert, true);
			
			vScroller.GetComponent<Scrollbar> ().targetGraphic = vScrollerImage.GetComponent<Graphic> ();
			
			vScroller.GetComponent<Scrollbar> ().handleRect = vScrollerImage.GetComponent<RectTransform> ();
			vScroller.transform.localScale = Vector3.one;
			
			scrollThing.GetComponent<ScrollRect> ().verticalScrollbar = vScroller.GetComponent<Scrollbar> ();
		}
		
		// Horizontal scrollbar.
		if(horizontal)
		{
			// Make the scroller background.
			GameObject hScrollerBackground = MakeImage (scrollThing, new Rect(Screen.width * 0.5f, Screen.height * 0.5f, rect.width, rect.width * 0.1f), sliderBackgroundHoriz);
			hScrollerBackground.name = "hScrollerBackground";
			
			// Make the scroller object.
			GameObject hScroller = new GameObject();
			hScroller.name = "HScroller";
			hScroller.AddComponent<Scrollbar> ();
			hScroller.GetComponent<RectTransform> ().pivot = new Vector2 (0, 0);
			hScroller.transform.SetParent (scrollThing.transform, true);
			hScroller.transform.localPosition = new Vector2 (0, rect.width * -0.1f);
			hScroller.GetComponent<Scrollbar> ().direction = Scrollbar.Direction.LeftToRight;
			sizeModifier = new Vector2 (rect.width, rect.width * 0.1f);
			
			hScroller.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
			
			// Make the scroller image.
			GameObject hScrollerImage = MakeImage (hScroller, new Rect (Screen.width * 0.5f, rect.width * -0.1f + (Screen.height * 0.5f), 0, 0), sliderForegroundHoriz, true);
			
			hScroller.GetComponent<Scrollbar> ().targetGraphic = hScrollerImage.GetComponent<Graphic> ();
			
			hScroller.GetComponent<Scrollbar> ().handleRect = hScrollerImage.GetComponent<RectTransform> ();
			hScroller.transform.localScale = Vector3.one;
			
			scrollThing.GetComponent<ScrollRect> ().horizontalScrollbar = hScroller.GetComponent<Scrollbar> ();
		}
		
		scrollThing.GetComponent<ScrollRect> ().content = page1content.GetComponent<RectTransform> ();
		
		// Set the content to be a child of this scroll rect
		if(content != null)
		{
			content.transform.SetParent(page1content.transform, true);
			content.transform.localScale = Vector3.one;
		}

		return scrollThing;
	}
	
	/// <summary>
	/// Makes a panel.
	/// </summary>
	/// <returns>The panel.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="rect">Rect.</param>
	public GameObject MakePanel (GameObject parent, Rect rect)
	{
		// Make panel object.
		GameObject panelObject = new GameObject ();
		panelObject.AddComponent<RectTransform> ();

		panelObject.name = "Panel";
		panelObject.GetComponent<RectTransform> ().anchorMax = new Vector2 (0, 1f);
		panelObject.GetComponent<RectTransform> ().anchorMin = new Vector2 (0, 1f);
		panelObject.GetComponent<RectTransform> ().pivot = Vector2.zero;

		panelObject.transform.SetParent (parent.transform, true);
		panelObject.GetComponent<RectTransform> ().localPosition = new Vector2 (rect.x, rect.y);
		panelObject.transform.localScale = Vector3.one;

		if (parent.GetComponent<RectTransform> () != null)
		{
			panelObject.transform.localPosition = new Vector2 (parent.GetComponent<RectTransform> ().rect.x + rect.x - (Screen.width * 0.5f), -(rect.y + rect.height) + (Screen.height * 0.5f));
		}
		else
		{
			panelObject.transform.localPosition = new Vector2 (rect.x, - (rect.y + rect.height));
		}
		
		// Set the panel size.
		Vector2 sizeModifier = new Vector2 (rect.width , rect.height );
		panelObject.GetComponent<RectTransform> ().sizeDelta = sizeModifier;

		return panelObject;
	}
	
	/// <summary>
	/// Makes an image.
	/// </summary>
	/// <returns>The image.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="rect">Rect.</param>
	/// <param name="content">Content.</param>
	/// <param name="isSliced">If set to <c>true</c> is sliced.</param>
	public GameObject MakeImage (GameObject parent, Rect rect, Sprite content, bool isSliced = true)
	{
		// Instantiate scroll area.
		GameObject instImage = new GameObject ();
		instImage.name = "Image";
		instImage.AddComponent<Image> ();
		
		// Set image parent.
		try
		{
			instImage.transform.SetParent (parent.transform, true);
		}
		catch(Exception e)
		{
			Debug.Log ("E: " + e);
		}

		instImage.GetComponent<RectTransform> ().anchorMin = new Vector2 (0f, 1f);
		instImage.GetComponent<RectTransform> ().anchorMax = new Vector2 (0f, 1f);
		instImage.GetComponent<RectTransform> ().pivot = Vector2.zero;

		// Set button position.
		instImage.transform.localPosition = new Vector2 (rect.x - (Screen.width * 0.5f), -rect.y - rect.height + (Screen.height * 0.5f));

		instImage.transform.localScale = Vector3.one;

		// Set if the image is sliced.
		if(isSliced)
		{
			instImage.GetComponent<Image> ().type = Image.Type.Sliced;
		}

		// Set the image size.
		Vector2 sizeModifier = new Vector2 (rect.width, rect.height);
		instImage.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		
		// Set sprite.
		if (content != null)
		{
			instImage.GetComponent<Image> ().sprite = content;
		}

		return instImage;
	}
	
	/// <summary>
	/// Makes a window background.
	/// </summary>
	/// <returns>The window background.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="rect">Rect.</param>
	/// <param name="content">Content.</param>
	public GameObject MakeWindowBackground (GameObject parent, Rect rect, Sprite content)
	{
		// Instantiate scroll area.
		GameObject instImage = new GameObject ();
		instImage.name = "Image";
		instImage.AddComponent<Image> ();
		
		// Set scroll area parent.
		instImage.GetComponent<RectTransform> ().anchorMin = new Vector2 (0f, 1f);
		instImage.GetComponent<RectTransform> ().anchorMax = new Vector2 (0f, 1f);
		instImage.GetComponent<RectTransform> ().pivot = Vector2.zero;

		// Set image parent.
		instImage.transform.SetParent (parent.transform, true);

		// Set image position.
		instImage.transform.localPosition = new Vector2 (rect.x - (Screen.width * 0.5f), -(rect.y + rect.height) + (Screen.height * 0.5f));
		instImage.transform.localScale = Vector3.one;

		// Set image size.
		Vector2 sizeModifier = new Vector2 (rect.width, rect.height);
		instImage.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		
		// Set sprite.
		if (content != null)
		{
			instImage.GetComponent<Image> ().type = Image.Type.Sliced;
			instImage.GetComponent<Image> ().sprite = content;
		}

		return instImage;
	}
	
	/// <summary>
	/// Makes a checkbox.
	/// </summary>
	/// <returns>The checkbox.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="rect">Rect.</param>
	/// <param name="label">Label.</param>
	/// <param name="guiStyle">GUI style.</param>
	public GameObject MakeCheckbox (GameObject parent, Rect rect, string label, string guiStyle = null)
	{
		// Instantiate checkbox.
		GameObject instCheckbox = new GameObject ();
		instCheckbox.AddComponent<RectTransform> ();
		instCheckbox.AddComponent<Toggle> ();
		instCheckbox.AddComponent<Image> ();
		instCheckbox.AddComponent<HoverPointer>();
		
		// Set checkbox parent.
		instCheckbox.transform.SetParent (parent.transform, true);
		
		// Set checkbox position
		instCheckbox.transform.localPosition = new Vector2 (rect.x - (Screen.width * 0.5f), -rect.y - rect.height + (Screen.height * 0.5f));
		instCheckbox.transform.localScale = Vector3.one;
		
		instCheckbox.GetComponent<RectTransform> ().anchorMin = new Vector2 (0f, 1f);
		instCheckbox.GetComponent<RectTransform> ().anchorMax = new Vector2 (0f, 1f);
		instCheckbox.GetComponent<RectTransform> ().pivot = Vector2.zero;
		
		// Set checkbox size.
		Vector2 sizeModifier = new Vector2 (rect.height, rect.height);
		instCheckbox.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		
		// Set sprite.
		instCheckbox.GetComponent<Image> ().sprite = tickBoxIcon;
		instCheckbox.GetComponent<Toggle> ().toggleTransition = Toggle.ToggleTransition.None;
		
		// Make the image for the checkbox.
		GameObject instImage = MakeImage (instCheckbox, new Rect(Screen.width * 0.5f, -rect.height + (Screen.height * 0.5f), rect.height, rect.height), tickBoxCheckIcon, false);
		instImage.name = "CheckboxImage";
		instCheckbox.GetComponent<Toggle> ().graphic = instImage.GetComponent<Image>();
		
		// Make clicking noise when clicked. Check against frame timer to stop this clicking on screen init.
		instCheckbox.GetComponent<Toggle> ().onValueChanged.AddListener(
			delegate
			{
				if (sndManager != null && MenuScreen.firstFrameTimer > 0.1f)
				{
					sndManager.PlaySound (buttonClick);
				}
			});
		
		// Make label.
		GameObject instLabel = MakeLabel (instCheckbox, " " + label, new Rect(rect.height * 1.05f + (Screen.width * 0.5f), -rect.height + (Screen.height * 0.5f), rect.width, rect.height), guiStyle);
		instLabel.name = "CheckboxLabel";
		instLabel.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
		instLabel.GetComponent<Text>().resizeTextForBestFit = false;
		instLabel.GetComponent<Text>().fontSize = (int)_fontSize;
		
		return instCheckbox;
	}
	
	/// <summary>
	/// Makes a popup.
	/// </summary>
	/// <returns>The popup.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="rect">Rect.</param>
	/// <param name="title">Title.</param>
	/// <param name="body">Body.</param>
	/// <param name="buttonText">Button text.</param>
	/// <param name="task">Task.</param>
	/// <param name="menuName">Menu name.</param>
	/// <param name="guiTitleStyle">GUI title style.</param>
	/// <param name="guiBodyStyle">GUI body style.</param>
	public GameObject MakePopup (GameObject parent, Rect rect, string title, string body, string buttonText, CommonTasks task, string menuName = "", string guiTitleStyle = null, string guiBodyStyle = null)
	{
		// Make the popup object.
		GameObject instPopup = MakeImage (parent, new Rect(rect.x, rect.y, rect.width, rect.height), popup_back, true);
		instPopup.AddComponent<Popup>();
		
		// Make title.
		GameObject titleObj = MakeLabel (instPopup, title, new Rect (rect.width * 0.025f + (Screen.width * 0.5f), (rect.height * 0.05f) + (Screen.height * 0.0f), rect.width * 0.95f, rect.height * 0.2f));
		// Set text size.
		titleObj.GetComponent<Text> ().fontSize = (int)_fontSize_xLarge;
		// Set text style.
		titleObj.GetComponent<Text> ().fontStyle = FontStyle.Bold;
		// Set text alignment.
		titleObj.GetComponent<Text> ().alignment = TextAnchor.UpperCenter;

		// Make body.
		GameObject bodyObj = MakeLabel (instPopup, body, new Rect (rect.width * 0.025f + (Screen.width * 0.5f), (rect.height * 0.3f) + (Screen.height * 0.0f), rect.width * 0.95f, rect.height * 0.4f));
		titleObj.GetComponent<Text> ().fontSize = (int)_fontSize_Large;
		// Set text alignment.
		bodyObj.GetComponent<Text> ().alignment = TextAnchor.MiddleCenter;

		// Make close popup button.
		MakeButton(instPopup, buttonText, buttonBackground, new Rect (rect.width * 0.375f + (Screen.width * 0.5f), rect.height * -0.2f + (Screen.height * 0.5f), rect.width * 0.25f, rect.height * 0.1f), task, menuName, "OrangeText");

		instPopup.GetComponent<Popup> ().titleObj = titleObj;
		instPopup.GetComponent<Popup> ().bodyObj = bodyObj;

		return instPopup;
	}
	
	/// <summary>
	/// Makes a popup.
	/// </summary>
	/// <returns>The popup.</returns>
	/// <param name="popupTemp">Popup temp.</param>
	public GameObject MakePopup (PopupTemplate popupTemp)
	{
		// Makes the popup.
		GameObject instPopup = MakeImage (popupTemp.parent, new Rect(popupTemp.rect.x, popupTemp.rect.y, popupTemp.rect.width, popupTemp.rect.height), popup_back, true);
		instPopup.AddComponent<Popup>();
		
		// Make title.
		GameObject titleObj = MakeLabel (instPopup, popupTemp.title, new Rect (popupTemp.rect.width * 0.025f + (Screen.width * 0.5f), popupTemp.rect.height * 0.05f + (Screen.height * 0.0f), popupTemp.rect.width * 0.95f, popupTemp.rect.height * 0.2f));
		// Set text size.
		titleObj.GetComponent<Text> ().fontSize = (int)_fontSize_xLarge;
		// Set text style.
		titleObj.GetComponent<Text> ().fontStyle = FontStyle.Bold;
		// Set text alignment.
		titleObj.GetComponent<Text> ().alignment = TextAnchor.UpperCenter;
		
		// Make body.
		GameObject bodyObj = MakeLabel (instPopup, popupTemp.body, new Rect (popupTemp.rect.width * 0.025f + (Screen.width * 0.5f), popupTemp.rect.height * 0.3f + (Screen.height * 0.0f), popupTemp.rect.width * 0.95f, popupTemp.rect.height * 0.4f));
		titleObj.GetComponent<Text> ().fontSize = (int)_fontSize_Large;
		// Set text alignment.
		bodyObj.GetComponent<Text> ().alignment = TextAnchor.UpperCenter;
		
		// Make close popup button.
		GameObject buttonObj = MakeButton(instPopup, popupTemp.buttonText, buttonBackground, new Rect (popupTemp.rect.width * 0.375f, popupTemp.rect.height * -0.2f, popupTemp.rect.width * 0.25f, popupTemp.rect.height * 0.1f), popupTemp.task, popupTemp.menuName, "OrangeText");
		buttonObj.name = "ButtonObject";
		
		instPopup.GetComponent<Popup> ().titleObj = titleObj;
		instPopup.GetComponent<Popup> ().bodyObj = bodyObj;
		
		return instPopup;
	}

	/// <summary>
	/// Makes a popup.
	/// </summary>
	/// <returns>The popup.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="rect">Rect.</param>
	/// <param name="title">Title.</param>
	/// <param name="body">Body.</param>
	/// <param name="buttonText">Button text.</param>
	/// <param name="task">Task.</param>
	/// <param name="menuName">Menu name.</param>
	/// <param name="guiTitleStyle">GUI title style.</param>
	/// <param name="guiBodyStyle">GUI body style.</param>
	public GameObject MakePopup (GameObject parent, Rect rect, string title, string body, string buttonText, List<MyVoidFunc> func, string guiTitleStyle = null, string guiBodyStyle = null)
	{
		// Make the popup object.
		GameObject instPopup = MakeImage (parent, new Rect(rect.x, rect.y, rect.width, rect.height), popup_back, true);
		instPopup.AddComponent<Popup>();
		
		// Make title.
		GameObject titleObj = MakeLabel (instPopup, title, new Rect (rect.width * 0.025f + (Screen.width * 0.5f), (rect.height * 0.05f) + (Screen.height * 0.0f), rect.width * 0.95f, rect.height * 0.2f));
		// Set text size.
		titleObj.GetComponent<Text> ().fontSize = (int)_fontSize_xLarge;
		// Set text style.
		titleObj.GetComponent<Text> ().fontStyle = FontStyle.Bold;
		// Set text alignment.
		titleObj.GetComponent<Text> ().alignment = TextAnchor.UpperCenter;
		
		// Make body.
		GameObject bodyObj = MakeLabel (instPopup, body, new Rect (rect.width * 0.025f + (Screen.width * 0.5f), (rect.height * 0.3f) + (Screen.height * 0.5f), rect.width * 0.95f, rect.height * 0.4f));
		titleObj.GetComponent<Text> ().fontSize = (int)_fontSize_Large;
		// Set text alignment.
		bodyObj.GetComponent<Text> ().alignment = TextAnchor.MiddleCenter;
		
		// Make close popup button.
		MakeButton(instPopup, buttonText, buttonBackground, new Rect (rect.width * 0.375f + (Screen.width * 0.5f), rect.height * -0.2f + (Screen.height * 0.5f), rect.width * 0.25f, rect.height * 0.1f), func, "OrangeText");
		
		instPopup.GetComponent<Popup> ().titleObj = titleObj;
		instPopup.GetComponent<Popup> ().bodyObj = bodyObj;
		
		return instPopup;
	}

	/// <summary>
	/// Makes a multi step popup.
	/// </summary>
	/// <returns>The multi popup.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="rect">Rect.</param>
	/// <param name="title">Title.</param>
	/// <param name="body">Body.</param>
	/// <param name="buttonText">Button text.</param>
	/// <param name="finalButtonText">Final button text.</param>
	/// <param name="task">Task.</param>
	/// <param name="menuName">Menu name.</param>
	/// <param name="guiTitleStyle">GUI title style.</param>
	/// <param name="guiBodyStyle">GUI body style.</param>
	public GameObject MakeMultiPopup (GameObject parent, Rect rect, string title, string[] body, string buttonText, string finalButtonText, CommonTasks task, string menuName = "", string guiTitleStyle = null, string guiBodyStyle = null)
	{
		GameObject instPopup = new GameObject ();
		instPopup.AddComponent<RectTransform>();
		instPopup.AddComponent<Popup>();
		
		// Set popup parent.
		instPopup.transform.SetParent (parent.transform);
		instPopup.transform.localScale = Vector3.one;
		instPopup.transform.localPosition = new Vector3(rect.x - (Screen.width * 0.5f), rect.y - rect.height + (Screen.height * 0.5f), 0);
		
		instPopup.GetComponent<RectTransform> ().anchorMin = new Vector2 (0f, 1f);
		instPopup.GetComponent<RectTransform> ().anchorMax = new Vector2 (0f, 1f);
		instPopup.GetComponent<RectTransform> ().pivot = Vector2.zero;
		
		// Set popup size.
		Vector2 sizeModifier = new Vector2 (rect.width, rect.height);
		instPopup.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		
		// Make title.
		GameObject titleObj = MakeLabel (instPopup, title, new Rect (0.0f, rect.height * 0.01f, rect.width, rect.height * 0.2f));
		
		// Set text size.
		titleObj.GetComponent<Text> ().fontSize = (int)_fontSize_xLarge;
		
		// Set text style.
		titleObj.GetComponent<Text> ().fontStyle = FontStyle.Bold;
		
		// Set text alignment.
		titleObj.GetComponent<Text> ().alignment = TextAnchor.UpperCenter;

		// Make body.
		if (body.Length <= 0)
		{
			body = new string[]{""};
		}
		
		GameObject bodyObj = MakeLabel (instPopup, body[0], new Rect (0.0f, rect.height * 0.3f, rect.width, rect.height * 0.4f), body [0]);
		// Set text alignment.
		bodyObj.GetComponent<Text> ().alignment = TextAnchor.UpperCenter;
		// Set the multitext for the popup.
		instPopup.GetComponent<Popup>().multiLayerText = body;

		// Make button. Set the required rect for the button.
		Rect buttonRect = new Rect (rect.width * 0.4f, rect.height * 0.2f, rect.width * 0.2f, rect.height * 0.1f);

		List<MyPopupFunc> funcList = new List<MyPopupFunc>
		{
			instPopup.GetComponent<Popup>().NextPopupLayer
		};
	
		GameObject buttonObj = MakeButton (instPopup, buttonText, buttonRect, funcList, finalButtonText, task, menuName);

		instPopup.GetComponent<Popup> ().titleObj = titleObj;
		
		if (bodyObj != null)
		{
			instPopup.GetComponent<Popup> ().bodyObj = bodyObj;
		}
		
		instPopup.GetComponent<Popup> ().buttonObj = buttonObj;

		return instPopup;
	}
	
	/// <summary>
	/// Makes an input field.
	/// </summary>
	/// <returns>The input field.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="buttonText">Button text.</param>
	/// <param name="rect">Rect.</param>
	/// <param name="isPassword">If set to <c>true</c> is password.</param>
	/// <param name="backGround">Back ground.</param>
	public GameObject MakeInputField (GameObject parent, string buttonText, Rect rect, bool isPassword, Sprite backGround = null)
	{
		GameObject inputFieldBackground;
		
		// Set the field background.
		if(backGround != null)
		{
			inputFieldBackground = MakeImage (parent, new Rect (0, 0, rect.width, rect.height), backGround);
		}
		// Otherwise, give it the default background.
		else
		{
			inputFieldBackground = MakeImage (parent, new Rect (0, 0, rect.width, rect.height), inputBackground);
		}

		// Make the field background sliced.
		inputFieldBackground.GetComponent<Image> ().type = Image.Type.Sliced;
		inputFieldBackground.transform.SetParent (parent.transform, true);

		// Instantiate input field.
		GameObject inputField = new GameObject ();

		inputField.AddComponent<Text> ();
		inputField.AddComponent<InputField> ();
		inputField.AddComponent<HoverPointer>();
		
		if(buttonText == null)
		{
			buttonText = "";
		}
		
		// Set button name.
		inputField.GetComponent<InputField> ().text = buttonText;
	                                 
		inputField.name = "Text";

		// Set whether this is a password field.
		if (isPassword)
		{
			inputField.GetComponent<InputField> ().inputType = InputField.InputType.Password;
		}

		inputField.GetComponent<Text> ().resizeTextForBestFit = false;
		inputField.GetComponent<Text> ().fontSize = (int)_fontSize_Large;
		inputField.GetComponent<Text> ().text = buttonText;
		inputField.GetComponent<Text> ().color = bmDarkBlue;
		inputField.GetComponent<Text> ().font = defaultFont;

		inputField.GetComponent<InputField> ().textComponent = inputField.GetComponent<Text> ();
		inputField.GetComponent<InputField>().targetGraphic = inputFieldBackground.GetComponent<Image>();
		
		// Set the background fading.
		ColorBlock cb = new ColorBlock();
		cb.colorMultiplier = 1;
		cb.disabledColor = new Color(1, 1, 1, 0.4f);
		cb.highlightedColor = new Color(1, 1, 1, 1f);
		cb.normalColor = new Color(1, 1, 1, 0.8f);
		cb.pressedColor = new Color(1, 1, 1, 1f);
		inputField.GetComponent<InputField>().colors = cb;
		
		// Set field position.
		inputFieldBackground.GetComponent<RectTransform> ().anchorMin = new Vector2 (0f, 1f);
		inputFieldBackground.GetComponent<RectTransform> ().anchorMax = new Vector2 (0f, 1f);
		inputFieldBackground.GetComponent<RectTransform> ().pivot = Vector2.zero;
		
		inputFieldBackground.transform.localPosition = new Vector2 (rect.x - (Screen.width * 0.5f), -rect.y - rect.height + (Screen.height * 0.5f));
		inputFieldBackground.transform.localScale = Vector3.one;
		
		// Set the field size.
		Vector2 sizeModifier = new Vector2 (rect.width, rect.height);
		inputFieldBackground.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		sizeModifier.x *= 0.98f;
		sizeModifier.y *= 0.9f;
		inputField.GetComponent<RectTransform> ().sizeDelta = sizeModifier;
		inputField.transform.SetParent (inputFieldBackground.transform, false);

		return inputField; 
	}
	
	/// <summary>
	/// Makes a horizontal slider.
	/// </summary>
	/// <returns>The horizontal slider.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="rect">Rect.</param>
	public GameObject MakeSliderHorizontal (GameObject parent, Rect rect)
	{
		// Horizontal scrollbar
		GameObject scroller = new GameObject();
		scroller.name = "Scroller";
		scroller.AddComponent<Slider>();
		scroller.transform.SetParent (parent.transform);
		
		// Set scroller position.
		scroller.GetComponent<RectTransform>().anchorMax = new Vector2(0,1f);
		scroller.GetComponent<RectTransform>().anchorMin = new Vector2(0,1f);
		scroller.GetComponent<RectTransform>().pivot = new Vector2(0,0);
		
		scroller.transform.localScale = Vector3.one;
		scroller.transform.localPosition = new Vector2(rect.x - (Screen.width * 0.5f), -rect.y - (Screen.height * 0.0f));
		scroller.GetComponent<Slider>().direction = Slider.Direction.LeftToRight;
		
		// Set scroller size.
		Vector2 sizeModifier = new Vector2(rect.width, rect.height);
		scroller.GetComponent<RectTransform>().sizeDelta = sizeModifier;
		
		// Make the scroller background image.
		GameObject scrollerBackground = MakeImage (scroller, new Rect(-rect.height * 0.25f + (Screen.width * 0.5f), -rect.height * 2f + (Screen.height * 0.5f), rect.width + (rect.height * 0.5f), rect.height * 3f), sliderBackgroundHoriz);
		scrollerBackground.name = "ScrollerBackground";
		
		// Make the scroller foreground image.
		GameObject scrollerForeground = MakeImage (scroller, new Rect(-rect.height * 0.25f + (Screen.width * 0.5f), -rect.height * 2f + (Screen.height * 0.5f), rect.width + (rect.height * 0.5f), rect.height * 3f), sliderForegroundHoriz);
		scrollerForeground.name = "ScrollerForeground";
		
		// Change the foreground size on value change.
		scroller.GetComponent<Slider>().onValueChanged.AddListener(
			delegate
			{
				scrollerForeground.GetComponent<RectTransform>().sizeDelta = new Vector2((((scroller.GetComponent<Slider>().value - scroller.GetComponent<Slider>().minValue) / (scroller.GetComponent<Slider>().maxValue - scroller.GetComponent<Slider>().minValue) * rect.width)/* + (rect.width * 0.1f)*/), scrollerForeground.GetComponent<RectTransform>().sizeDelta.y);
			});
		
		// Make the image for the scroller.
		GameObject scrollerImage = MakeImage (scroller, new Rect(-rect.height + (Screen.width * 0.5f), -rect.height * 1.5f + (Screen.height * 0.5f), rect.height * 2.0f, rect.height), sliderHandle);
		scrollerImage.name = "ScrollerImage";
		scrollerImage.GetComponent<Image>().preserveAspect = true;
		
		scroller.GetComponent<Slider>().image = scrollerForeground.GetComponent<Image>();
		scroller.GetComponent<Slider>().handleRect = scrollerImage.GetComponent<RectTransform>();
		
		return scroller;
	}
	
	/// <summary>
	/// Makes a vertical slider.
	/// </summary>
	/// <returns>The vertical slider.</returns>
	/// <param name="parent">Parent.</param>
	/// <param name="rect">Rect.</param>
	public GameObject MakeSliderVertical (GameObject parent, Rect rect)
	{
		// Vertical scrollbar
		GameObject scroller = new GameObject();
		scroller.name = "Scroller";
		scroller.AddComponent<Slider>();
		scroller.transform.SetParent (parent.transform);
		
		// Set slider position.
		scroller.GetComponent<RectTransform>().anchorMax = new Vector2(0, 1f);
		scroller.GetComponent<RectTransform>().anchorMin = new Vector2(0, 1f);
		scroller.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
		
		scroller.transform.localScale = Vector3.one;
		scroller.transform.localPosition = new Vector2(rect.x - (Screen.width * 0.5f), -(Screen.height * 0.0f) - rect.y);
		scroller.GetComponent<Slider>().direction = Slider.Direction.BottomToTop;
		
		// Set slider size.
		Vector2 sizeModifier = new Vector2(rect.width, rect.height);
		scroller.GetComponent<RectTransform>().sizeDelta = sizeModifier;
		
		// Make slider background image.
		GameObject scrollerBackground = MakeImage (scroller, new Rect(rect.width * -1f + (Screen.width * 0.5f), -rect.height * 1.0f + (Screen.height * 0.5f), rect.width * 3.0f, rect.height), sliderBackgroundVert);
		scrollerBackground.name = "ScrollerBackground";
		
		// Make slider foreground image.
		GameObject scrollerForeground = MakeImage (scroller, new Rect(rect.width * -1f + (Screen.width * 0.5f), -rect.height * 1.0f + (Screen.height * 0.5f), rect.width * 3.0f, rect.height), sliderForegroundVert);
		scrollerForeground.name = "ScrollerForeground";
		
		// Change the foreground size on value change.
		scroller.GetComponent<Slider>().onValueChanged.AddListener(
			delegate
			{
				scrollerForeground.GetComponent<RectTransform>().sizeDelta = new Vector2(scrollerForeground.GetComponent<RectTransform>().sizeDelta.x, ((scroller.GetComponent<Slider>().value - scroller.GetComponent<Slider>().minValue) / (scroller.GetComponent<Slider>().maxValue - scroller.GetComponent<Slider>().minValue)) * (rect.height));
			});
		
		// Make the scroller image.
		GameObject scrollerImage = MakeImage (scroller, new Rect(-rect.width * 0.5f + (Screen.width * 0.5f), -rect.height - (rect.width * 1.0f) + (Screen.height * 0.5f), rect.width, rect.width * 2.0f), sliderHandle);
		scrollerImage.name = "ScrollerImage";
		scrollerImage.GetComponent<Image>().preserveAspect = true;
		
		scroller.GetComponent<Slider>().image = scrollerForeground.GetComponent<Image>();
		scroller.GetComponent<Slider>().handleRect = scrollerImage.GetComponent<RectTransform>();
		
		return scroller;
	}
	
	/// <summary>
	/// Toggles the repeat button.
	/// </summary>
	/// <returns>The button toggle.</returns>
	IEnumerator RepeatButtonToggle ()
	{
		yield return new WaitForSeconds (0.125f);
		repeatButtonToggle = false;
	}
	
	/// <summary>
	/// Does a repeat button.
	/// </summary>
	/// <returns><c>true</c>, if repeat button was done, <c>false</c> otherwise.</returns>
	/// <param name="rect">Rect.</param>
	/// <param name="image">Image.</param>
	public bool DoRepeatButton (Rect rect, Texture image)
	{
		// If mouse is in the button area, show a hand instead of a pointer.
		if(rect.Contains(Event.current.mousePosition))
		{
			mouseOverCount++;
		}
		
		// Draw the repeat button.
		if (GUI.RepeatButton (rect, image))
		{
			if (!repeatButtonToggle)
			{
				repeatButtonToggle = true;
				StartCoroutine (RepeatButtonToggle ());
				sndManager.PlayOneShotSound (buttonClick);
				return true;
			}
		}
		
		return false;
	}
	
	/// <summary>
	/// Does a repeat button.
	/// </summary>
	/// <returns><c>true</c>, if repeat button was done, <c>false</c> otherwise.</returns>
	/// <param name="rect">Rect.</param>
	/// <param name="text">Text.</param>
	/// <param name="style">Style.</param>
	public bool DoRepeatButton (Rect rect, string text, GUIStyle style)
	{
		// If mouse is in the button area, show a hand instead of a pointer.
		if(rect.Contains(Event.current.mousePosition))
		{
			mouseOverCount++;
		}
		
		// Draws the repeat button.
		if (GUI.RepeatButton (rect, text, style))
		{
			if (!repeatButtonToggle)
			{
				repeatButtonToggle = true;
				StartCoroutine (RepeatButtonToggle ());
				sndManager.PlayOneShotSound (buttonClick);
				return true;
			}
		}
		
		return false;
	}
	
	/// <summary>
	/// Does a repeat button.
	/// </summary>
	/// <returns><c>true</c>, if repeat button was done, <c>false</c> otherwise.</returns>
	/// <param name="rect">Rect.</param>
	/// <param name="image">Image.</param>
	/// <param name="style">Style.</param>
	public bool DoRepeatButton (Rect rect, Texture image, GUIStyle style)
	{
		// If mouse is in the button area, show a hand instead of a pointer.
		if(rect.Contains(Event.current.mousePosition))
		{
			mouseOverCount++;
		}
		
		// Draws the repeat button.
		if (GUI.RepeatButton (rect, image, style))
		{
			if (!repeatButtonToggle)
			{
				repeatButtonToggle = true;
				StartCoroutine (RepeatButtonToggle ());
				sndManager.PlayOneShotSound (buttonClick);
				return true;
			}
		}
		
		return false;
	}
}

