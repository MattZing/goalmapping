using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class WebVideoPlayer : MenuScreen
{
	public WWW wwwData;
	public float movieTimer = 0;
	
	private GameObject localCanvas;
	private GameObject downloadingPopup;
	private GameObject downloadingSpinner;
	
	private Sprite spinnerSprite;
	
#if !UNITY_IPHONE && !UNITY_ANDROID
	private string url = MCP.serverURL +"Videos/intro-video.ogg";
#else
	private string url = MCP.serverURL +"Videos/intro-video.mp4";
#endif
	private bool restartMusic = false;
	private string videoFile;
	private bool prevFocus = true;
#if !UNITY_IPHONE && !UNITY_ANDROID
	private bool movieTexSet = false;
	private bool textureShowing = false;
	private MovieTexture tempMovieTex;
#endif
	private Rect screenSize;
	
	private bool isDownloading = false;
	
//	private float downloadProgress = 0;
	private float spinnerRot = 0;
//	private Rect loadingBox;
//#endif
	
	void Start() 
	{	
		Init ();
	
		spinnerSprite = Resources.Load<Sprite> ("GUI/bm_rotate");
		
		GetComponent<GUITexture>().pixelInset = new Rect(Screen.width * 0.71f, Screen.height * -0.2f, Screen.width * -0.42f, Screen.height * -0.54f);
		GetComponent<GUITexture>().enabled = false;

		if ( MenuScreen.sndManager != null )
		{
			try
			{
				restartMusic = MenuScreen.sndManager.GetBackgroundMusicEnabled();
				MenuScreen.sndManager.SetBackgroundMusicEnabled( false );
			}
			catch(System.Exception e)
			{
				Debug.Log ("Error: " + e);
			}
		}
	
		if ( MCP.userInfo != null ) 
		{
//			url = MCP.siteURL + "Videos/SWB.mp4";//" + MCP.webURLToPlay;
//			videoFile = Application.persistentDataPath + "/" + MCP.webURLToPlay;
			Debug.Log ("Video URL = " +  MCP.webURLToPlay);
		}
		
		localCanvas = MakeCanvas (true);
		
		wwwData = new WWW( url.Replace (" ", "%20") );
		
//#if UNITY_IOS || UNITY_ANDROID
		if(MCP.userInfo == null)
		{
//			url = MCP.siteURL + "Videos/SWB.mp4";//" + MCP.webURLToPlay;
//			videoFile = Application.persistentDataPath + "/intro-video.ogg";
//			videoFile = url;
		}
		StartCoroutine(DownloadAndPlayVideo());
//#endif
	}

	
	IEnumerator DownloadAndPlayVideo ()
	{
//		if( !System.IO.File.Exists( videoFile ) )
		{
//#if UNITY_IOS || UNITY_ANDROID
			isDownloading = true;
//#endif
#if !UNITY_ANDROID && !UNITY_IPHONE
			WWW www = new WWW( url );
			
			while(!www.isDone )
			{
#if UNITY_IOS || UNITY_ANDROID
//				downloadProgress = www.progress;
#endif
				yield return www.isDone;
			}
#endif
			
#if UNITY_IOS|| UNITY_ANDROID
			yield return new WaitForSeconds(1);
#endif

#if !UNITY_IPHONE
//			if( www != null && www.isDone && www.error == null )
			{
				//FileStream stream = new FileStream( videoFile, FileMode.Create );
				//stream.Write( www.bytes, 0, www.bytes.Length );
				//stream.Close();
			}
#endif
		}
#if UNITY_IOS|| UNITY_ANDROID
//		StartCoroutine(PlayStreamingVideo("file://" + videoFile));
		if(!MCP.siteURL.Contains ("192.168.1.146"))
		{
			downloadingPopup.SetActive (false);
			
			try
			{
				StartCoroutine(PlayStreamingVideo(url));
			}
			catch(System.Exception e)
			{
				Debug.Log (e);
			}
		}
		else
		{
			if(GetComponent<IntroScreen>() != null)
			{
				GetComponent<IntroScreen>().ForceSkipVideo ();
			}
			
			yield return new WaitForEndOfFrame();
		}
#endif
	}

	private IEnumerator PlayStreamingVideo(object URL)
	{
#if UNITY_IOS|| UNITY_ANDROID
		Handheld.PlayFullScreenMovie((string)URL, Color.black, FullScreenMovieControlMode.CancelOnInput);
#endif
		yield return new WaitForEndOfFrame();
		Debug.Log("Video playback completed.");
		if(GetComponent<IntroScreen>() != null)
		{
			GetComponent<IntroScreen>().VideoEnded ();
		}

		this.enabled = false;
	}
	
	void Update() 
	{
		if(wwwData != null && wwwData.isDone)
		{
#if !UNITY_IOS && !UNITY_ANDROID
	//		Texture t = GetComponent<GUITexture>().texture;
			if(wwwData.error != null)
			{
				Debug.Log ("Error: " + wwwData.error);
				Debug.Log ("Error: " + wwwData.text);
				LoadMenu ("Landing_Screen");
			}
			else
			{
				if(!movieTexSet)
				{
					GetComponent<GUITexture>().texture = wwwData.GetMovieTexture();
					
					tempMovieTex = GetComponent<GUITexture>().texture as MovieTexture;
					
					GetComponent<AudioSource>().clip = tempMovieTex.audioClip;
					GetComponent<GUITexture>().color = new Color(0, 0, 0, 0);
					movieTexSet = true;
					
					// Hide the main screen GUI.
					GetComponent<IntroScreen>().internalState = IntroScreen.InternalState.Video;
				}
			}
			
			if(movieTexSet && tempMovieTex.duration < 0)
			{
				//				Debug.Log ("Error: " + wwwData.text);
				//					GetComponent<IntroScreen>().ForceSkipVideo ();
//				LoadMenu ("Landing_Screen");
			}
			
			if(tempMovieTex != null && tempMovieTex.duration > 0 && !textureShowing)
			{
				GetComponent<GUITexture>().color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
				textureShowing = true;
			}
			
			if(GetComponent<GUITexture>().texture == null)
			{
				GetComponent<GUITexture>().enabled = false;
			}
			else
			{
				GetComponent<GUITexture>().enabled = true;
			}
			
			if(movieTexSet)
			{		
				if ( tempMovieTex != null )
				{	
					if ( !tempMovieTex.isPlaying && tempMovieTex.isReadyToPlay && movieTimer < 0.2f )
					{
	//					tempMovieTex.Play();
						GetComponent<IntroScreen>().ControlPlay ();
						isDownloading = false;
						Destroy (downloadingPopup);
					}
				}
			}
			
			if(GetComponent<IntroScreen>().playing)// && movieTimer < m.duration)
			{
				movieTimer += Time.deltaTime;
			}
			
	//		if(m.isReadyToPlay && movieTimer > m.duration)
			if(tempMovieTex != null && tempMovieTex.isReadyToPlay && movieTimer > GetComponent<AudioSource>().clip.length)
			{
				// Show the main screen GUI.
				GetComponent<IntroScreen>().internalState = IntroScreen.InternalState.Main;
				// Hide the video and this script.
				tempMovieTex.Stop();
				GetComponent<GUITexture>().enabled = false;
				this.enabled = false;
			}
			
			if(movieTimer > 5 && !tempMovieTex.isPlaying)
			{
				GetComponent<IntroScreen>().ForceSkipVideo ();
			}
#endif	
		}
	}
	
	public override void DoGUI()
	{
		if (screenSize.width == 0 || (Screen.width != screenSize.width)) 
		{
			screenSize = new Rect (0, 0, Screen.width, Screen.height);
//#if UNITY_IOS|| UNITY_ANDROID
//			loadingBox = new Rect(Screen.width * 0.25f,Screen.height * 0.25f,Screen.width * 0.5f,Screen.height * 0.7f);
//#endif
		}
		
		if(isDownloading)
		{
			if(downloadingPopup == null)
			{
				downloadingPopup = MakePopup(localCanvas, new Rect(Screen.width * 0.25f, Screen.height * 0.25f, Screen.width * 0.5f, Screen.height * 0.5f), MCP.Text (1908)/*"Downloading..."*/, "", "", CommonTasks.DestroyParent);
				// Remove the button from the popup
				Destroy(downloadingPopup.transform.GetChild (2).gameObject);
				
				downloadingSpinner = MakeImage (localCanvas, new Rect(Screen.width * 0.5f, Screen.height * 0.45f, Screen.width * 0.08f, Screen.width * 0.08f), spinnerSprite, false);
				downloadingSpinner.name = "LoadingSpinner";
				downloadingSpinner.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
				downloadingSpinner.transform.SetParent (downloadingPopup.transform);
			}
			else
			{
				// Spin the downloading spinner
				spinnerRot -= Time.deltaTime * 75f;
				downloadingSpinner.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, spinnerRot);
				
				Vector3 newPos = downloadingPopup.transform.GetChild (1).gameObject.GetComponent<RectTransform>().localPosition;
				newPos.y = Screen.height * 0.25f;
				downloadingPopup.transform.GetChild (1).gameObject.GetComponent<RectTransform>().localPosition = newPos;
#if UNITY_ANDROID || UNITY_IOS
				downloadingPopup.transform.GetChild (1).gameObject.GetComponent<Text>().text = "";
#else
				downloadingPopup.transform.GetChild (1).gameObject.GetComponent<Text>().text = (wwwData.progress * 100f).ToString ("f2") + "%";
#endif
			}
		}
	}
	
	void OnApplicationFocus(bool focusStatus)
	{
		if(focusStatus != prevFocus)
		{
			// If the focus has just become active...
			if (focusStatus)
			{	
				if(GetComponent<GUITexture>() != null)
				{
					GetComponent<GUITexture>().enabled = false;
				}
			}
		}
		
		prevFocus = focusStatus;
	}
	
	void OnDestroy()
	{
		if(GetComponent<GUITexture>() != null)
		{
#if !UNITY_ANDROID && !UNITY_IPHONE
			if(GetComponent<GUITexture>().texture != null && (GetComponent<GUITexture>().texture as MovieTexture) != null && (GetComponent<GUITexture>().texture as MovieTexture).duration > 0)
			{
				(GetComponent<GUITexture>().texture as MovieTexture).Stop ();
			}
#endif
			
			GetComponent<GUITexture>().texture = null;
		}
		
		if ( restartMusic && MenuScreen.sndManager != null )
		{
			MenuScreen.sndManager.SetBackgroundMusicEnabled( true );
		}
	}
}
