﻿using UnityEngine;
using System.Collections;

public class MyZingTeam_Screen : MenuScreen
{
	//A 4x4 Matrix

	private Rect screenSize;
	private Rect topPanelRect;
	private Rect bottomPanelRect;
	
	//private Texture messageIconImage;
	
	private float logoutButtonScale;
	//private float teamButtonsWidth;
	//private float bottomPanelSpacer;

	// Use this for initialization
	void Start ()
	{
		base.Init ();
		StartCoroutine (MCP.GetMessages ());
		
		if (MCP.userInfo != null)
		{
			if(MCP.userInfo.mentorProfile != null)
			{
				StartCoroutine (MCP.GetMentorImage ());	
			}
		}
	}

	// Update is called once per frame
	void Update ()
	{
		base.UpdateScreen ();
		//If the 'next' boolean is true
	}

	public override void DoGUI ()
	{
		//DoTitleBar ("My ZingUp Team");
	//	DoSideGizmo ();

		if (screenSize.width == 0 || Screen.width != screenSize.width)
		{
			screenSize = new Rect (0, 0, Screen.width, Screen.height);
			bottomPanelRect = new Rect (screenSize.width * 0.11f, screenSize.height * 0.1f, screenSize.width * 0.78f, screenSize.height - (screenSize.height * 0.5f));

			topPanelRect = new Rect (screenSize.width * 0.11f, (screenSize.height * 0.1f) + (screenSize.height - (screenSize.height * 0.475f)), screenSize.width * 0.78f, screenSize.height * 0.35f);

			//teamButtonsWidth = (topPanelRect.width * 0.3f);
			//bottomPanelSpacer = topPanelRect.height * 0.175f;
		}

		GUI.Box (bottomPanelRect, "", "GlassBox");
		GUI.BeginGroup (bottomPanelRect);
		GUI.Box (new Rect (bottomPanelRect.width * 0.05f, bottomPanelRect.height * 0.05f, bottomPanelRect.width * 0.87f, bottomPanelRect.height * 0.7f), "", "PopOutGizmo_Header");
		scrollPosition = GUI.BeginScrollView (new Rect (bottomPanelRect.width * 0.05f, bottomPanelRect.height * 0.0585f, bottomPanelRect.width * 0.915f, bottomPanelRect.height * 0.66f), scrollPosition, new Rect (0, 0, bottomPanelRect.width * 0.87f, 2000), false, true);
		float offset = 0.0f;

		if (MCP.zingMessages != null)
		{
			byte flipFlop = 0;

			foreach (MCP.ZingMessage message in MCP.zingMessages.messages)
			{
				if (flipFlop == 0)
				{
					GUI.Box (new Rect (0, offset, bottomPanelRect.width * 0.87f, bottomPanelRect.width * 0.0325f), "", "MessageBoxWhite");
					GUI.Box (new Rect (bottomPanelRect.width * 0.03f, offset, bottomPanelRect.width * 0.135f, bottomPanelRect.width * 0.0325f), message.date.ToShortDateString (), "MessageBoxBlue");
					GUI.Box (new Rect (bottomPanelRect.width * 0.175f, offset, bottomPanelRect.width * 0.685f, bottomPanelRect.width * 0.0325f), message.message, "MessageBoxBlue");
					offset += bottomPanelRect.width * 0.0325f;
					flipFlop = 1;
				}
				else
				{
					GUI.Box (new Rect (0, offset, bottomPanelRect.width * 0.87f, bottomPanelRect.width * 0.0325f), "", "MessageBoxBlue");
					GUI.Box (new Rect (bottomPanelRect.width * 0.03f, offset, bottomPanelRect.width * 0.135f, bottomPanelRect.width * 0.0325f), message.date.ToShortDateString (), "MessageBoxBlue");
					GUI.Box (new Rect (bottomPanelRect.width * 0.175f, offset, bottomPanelRect.width * 0.685f, bottomPanelRect.width * 0.0325f), message.message, "MessageBoxBlue");
					offset += bottomPanelRect.width * 0.0325f;
					flipFlop = 0;
				}
				
				switch (int.Parse (message.type))
				{
				case 0:
					GUI.DrawTexture (new Rect (0, offset - bottomPanelRect.width * 0.0325f, bottomPanelRect.width * 0.03f, bottomPanelRect.width * 0.03f), news_infoImage, ScaleMode.StretchToFill, true, 10.0F);
					break;
				case 1:
					GUI.DrawTexture (new Rect (0, offset - bottomPanelRect.width * 0.0325f, bottomPanelRect.width * 0.03f, bottomPanelRect.width * 0.03f), news_alertImage, ScaleMode.StretchToFill, true, 10.0F);
					break;
				case 2:
					GUI.DrawTexture (new Rect (0, offset - bottomPanelRect.width * 0.0325f, bottomPanelRect.width * 0.03f, bottomPanelRect.width * 0.03f), news_criticalImage, ScaleMode.StretchToFill, true, 10.0F);
					break;
				}
			}
		}
		GUI.EndScrollView ();
		//if (DoButton (new Rect ((bottomPanelRect.width * 0.5f) - (bottomPanelRect.width * 0.2f), bottomPanelRect.height * 0.8f, bottomPanelRect.width * 0.4f, bottomPanelRect.height * 0.15f), "View Message Archive", "RedButton"))
		//{
		//	LoadMenu ("ArchivedMessages_Screen");
		//}
		//GUI.DrawTexture (new Rect ((bottomPanelRect.width * 0.5f) - (bottomPanelRect.width * 0.23f), bottomPanelRect.height * 0.8f, Screen.width * 0.04f, Screen.width * 0.04f), messageIconImage, ScaleMode.ScaleToFit);

		GUI.EndGroup ();

		GUI.Box (topPanelRect, "", "GlassBox");

		GUI.BeginGroup (topPanelRect);
		GUI.Label (new Rect (0, 0, screenSize.width * 0.25f, screenSize.width * 0.04f), "Team Toolbox");

		// Top Row
		//if (DoButton (new Rect ((topPanelRect.width * 0.33f) - teamButtonsWidth, bottomPanelSpacer * 1.5f, teamButtonsWidth, bottomPanelSpacer), "Team Map", "RedButton"))
		//{
		//	LoadMenu ("MLM_Test");
		//}
		//GUI.DrawTexture (new Rect ((topPanelRect.width * 0.335f) - teamButtonsWidth, bottomPanelSpacer * 1.5f, bottomPanelSpacer, bottomPanelSpacer), teamMapIconImage, ScaleMode.ScaleToFit);

		//if (DoButton (new Rect ((topPanelRect.width * 0.5f) - (teamButtonsWidth * 0.5f), bottomPanelSpacer * 1.5f, teamButtonsWidth, bottomPanelSpacer), "Mentor Profile", "RedButton"))
		//{
		//	LoadMenu ("MentorProfileViewer_Screen");	
	//	}
		//GUI.DrawTexture (new Rect ((topPanelRect.width * 0.5f) - (teamButtonsWidth * 0.48f), bottomPanelSpacer * 1.5f, bottomPanelSpacer, bottomPanelSpacer), mentorProfileIconImage, ScaleMode.ScaleToFit);

		//if (DoButton (new Rect ((topPanelRect.width - (topPanelRect.width * 0.33f)), bottomPanelSpacer * 1.5f, teamButtonsWidth, bottomPanelSpacer), "Add Client", "RedButton"))
		//{
		//	LoadMenu ("AddClient_Screen");
		//}
		//GUI.DrawTexture (new Rect ((topPanelRect.width - (topPanelRect.width * 0.325f)), bottomPanelSpacer * 1.5f, bottomPanelSpacer, bottomPanelSpacer), addClientIconImage, ScaleMode.ScaleToFit);

		// Bottom row
		//if (DoButton (new Rect ((topPanelRect.width * 0.33f) - teamButtonsWidth, bottomPanelSpacer * 3.5f, teamButtonsWidth, bottomPanelSpacer), "Mentor Zone", "RedButton"))
		//{
		//	LoadMenu ("MentorZone_Screen");
		//}
		//GUI.DrawTexture (new Rect ((topPanelRect.width * 0.335f) - teamButtonsWidth, bottomPanelSpacer * 3.5f, bottomPanelSpacer, bottomPanelSpacer), scheduleIconImage, ScaleMode.ScaleToFit);
		//Inactive overlay
		//GUI.Box (new Rect ((topPanelRect.width * 0.33f) - teamButtonsWidth, bottomPanelSpacer * 3.5f, teamButtonsWidth, bottomPanelSpacer), "", "Inactive_overlay");
		//if (DoButton (new Rect ((topPanelRect.width * 0.5f) - (teamButtonsWidth * 0.5f), bottomPanelSpacer * 3.5f, teamButtonsWidth, bottomPanelSpacer), "Team Sheet", "RedButton"))
		//{
		//	LoadMenu ("TeamSheet_Screen");
		//}
		//GUI.DrawTexture (new Rect ((topPanelRect.width * 0.5f) - (teamButtonsWidth * 0.48f), bottomPanelSpacer * 3.5f, bottomPanelSpacer, bottomPanelSpacer), teamSheetIconImage, ScaleMode.ScaleToFit);

		//if (DoButton (new Rect ((topPanelRect.width - (topPanelRect.width * 0.33f)), bottomPanelSpacer * 3.5f, teamButtonsWidth, bottomPanelSpacer), "Resources", "RedButton"))
		//{
		//	LoadMenu ("TeamResources_Screen");
		//}
		//GUI.DrawTexture (new Rect ((topPanelRect.width - (topPanelRect.width * 0.325f)), bottomPanelSpacer * 3.5f, bottomPanelSpacer, bottomPanelSpacer), resourcesIconImage, ScaleMode.ScaleToFit);

		GUI.EndGroup ();
	}
}
