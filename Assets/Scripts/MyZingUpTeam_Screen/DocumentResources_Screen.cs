using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DocumentResources_Screen : MenuScreen
{

	public List<DocumentURL> documentURLS;
	private Rect screenSize;
	
	private Rect topPanel;
	private Texture closeIconImage;
	//private Texture networkFeedBackground;
	
	private float logoutButtonScale;

	[System.Serializable]
	public class DocumentURL
	{
		public string title;
		public string url;
		public bool isDownloaded;
	}
	
	// Use this for initialization
	void Start ()
	{
		base.Init ();
		
		//videoURLS = new List<VideoURL>();

		closeIconImage = Resources.Load ("menuicon_logout") as Texture;
	}
	
	// Update is called once per frame
	void Update ()
	{
		base.UpdateScreen ();
		//If the 'next' boolean is true
		
		
	}
	
	public override void DoGUI ()
	{
		
		
		//DoTitleBar ("ZingTeam - Document Resources");
		//DoSideGizmo ();
		
		
		
		if (screenSize.width == 0 || Screen.width != screenSize.width) {
			screenSize = new Rect (0, 0, Screen.width, Screen.height);
			topPanel = new Rect (screenSize.width * 0.11f, screenSize.height * 0.1f, screenSize.width * 0.78f, screenSize.height * 0.875f);
			
			
			
		}
		
		
		
		GUI.Box (topPanel, "", "GlassBox");
		GUI.BeginGroup (topPanel);
		GUI.Label (new Rect (0, 0, screenSize.width * 0.25f, screenSize.width * 0.04f), "Document Library");
		scrollPosition = GUI.BeginScrollView (new Rect (topPanel.width * 0.05f, topPanel.height * 0.1f, topPanel.width * 0.90f, topPanel.height * 0.8f), scrollPosition, new Rect (0, 0, topPanel.width * 0.87f, 2000), false, true);
		float offset = 0.0f;
		
		byte flipFlop = 0;
		foreach (DocumentURL vid in documentURLS) {
			
			
			if (flipFlop == 0) {
				
				GUI.Box (new Rect (topPanel.width * 0.03f, offset, topPanel.width * 0.685f, topPanel.width * 0.0325f), vid.title, "MessageBoxWhite");
				GUI.Box (new Rect (topPanel.width * 0.73f, offset, topPanel.width * 0.135f, topPanel.width * 0.0325f), "Download", "MessageBoxWhite");
				offset += topPanel.width * 0.0325f;
				flipFlop = 1;
			} else {
				GUI.Box (new Rect (topPanel.width * 0.03f, offset, topPanel.width * 0.685f, topPanel.width * 0.0325f), vid.title, "MessageBoxBlue");
				GUI.Box (new Rect (topPanel.width * 0.73f, offset, topPanel.width * 0.135f, topPanel.width * 0.0325f), "Download", "MessageBoxBlue");
				offset += topPanel.width * 0.0325f;
				flipFlop = 0;
			}
		}
		
		GUI.EndScrollView ();
	//	if (DoButton (new Rect (topPanel.width - (topPanel.width * 0.145f), topPanel.height * 0.925f, topPanel.width * 0.135f, topPanel.width * 0.0325f), "Close", "BlueButton")) {
		//	Application.LoadLevel ("TeamResources_Screen");
		//}
		
		GUI.DrawTexture (new Rect (topPanel.width - (topPanel.width * 0.185f), topPanel.height * 0.925f, topPanel.width * 0.135f, topPanel.width * 0.0325f), closeIconImage, ScaleMode.ScaleToFit);
		
		GUI.EndGroup ();
		
		
		
	}
	
	
	
}