using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VideoResources_Screen : MenuScreen
{
	public List<VideoURL> videoURLS;
	private Rect screenSize;
	private Rect topPanel;
	private Texture closeIconImage;
	//private Texture networkFeedBackground;
	private float logoutButtonScale;
	//private bool isDownloading = false;
	//private bool hasPlayed = false;

	[System.Serializable]
	public class VideoURL
	{
		public string title;
		public string url;
		public bool isDownloaded;
	}

	// Use this for initialization
	void Start ()
	{
		base.Init ();

		//videoURLS = new List<VideoURL>();
		closeIconImage = Resources.Load ("GUI/menuicon_logout") as Texture;
	}
	
	// Update is called once per frame
	void Update ()
	{
		base.UpdateScreen ();
		//If the 'next' boolean is true
		if (MCP.userInfo != null)
		{
			/*if(MCP.downloadComplete && !hasPlayed)
			{
				#if UNITY_ANDROID || UNITY_IPHONE
				Handheld.PlayFullScreenMovie ("file://"+ Application.persistentDataPath + "/Science3.mp4", Color.black, FullScreenMovieControlMode.Full);
	
	
				#endif
	
				Debug.Log ("Trying to play video");
				//hasPlayed = true;
				//isDownloading = false;
			}*/
			if (MCP.downloadComplete)
			{
				//isDownloading = false;
			}
		}
	}

	public override void DoGUI ()
	{
		//	DoTitleBar ("ZingTeam - Video Resources");
		//	DoSideGizmo ();
	
		if (screenSize.width == 0 || Screen.width != screenSize.width)
		{
			screenSize = new Rect (0, 0, Screen.width, Screen.height);
			topPanel = new Rect (screenSize.width * 0.11f, screenSize.height * 0.1f, screenSize.width * 0.78f, screenSize.height * 0.875f);
		}



		GUI.Box (topPanel, "", "GlassBox");
		//if(DoButton (new Rect (topPanel.width * 0.03f, 0, topPanel.width * 0.685f, topPanel.width * 0.0325f), "Test", "MessageBoxWhite"))
		//{


		//	Handheld.PlayFullScreenMovie ("triangle_lucy.mp4", Color.black, FullScreenMovieControlMode.Full);
	
	
		//	#endif
		//}
		GUI.BeginGroup (topPanel);
		GUI.Label (new Rect (0, 0, screenSize.width * 0.25f, screenSize.width * 0.04f), "Video Library");
		scrollPosition = GUI.BeginScrollView (new Rect (topPanel.width * 0.05f, topPanel.height * 0.1f, topPanel.width * 0.90f, topPanel.height * 0.75f), scrollPosition, new Rect (0, 0, topPanel.width * 0.87f, 2000), false, true);
		//float offset = 0.0f;

		//byte flipFlop = 0;
		/*foreach (VideoURL vid in videoURLS)
		{
			if (flipFlop == 0)
			{
				//	if (DoButton (new Rect (topPanel.width * 0.03f, offset, topPanel.width * 0.685f, topPanel.width * 0.0325f), vid.title, "MessageBoxWhite")) {
							
			//MCP.webURLToPlay = MCP.serverURL + vid.url;
			//LoadMenu("WebVideoPlayer");
				//				Debug.Log ("Play vid.");
						
		//}
		/*

		if(MCP.userInfo != null&&isDownloading)
		{
			GUI.Box (new Rect (topPanel.width * 0.03f, offset, topPanel.width * 0.685f, topPanel.width * 0.0325f), MCP.downLoadProgess, "MessageBoxWhite");
			GUI.Box(new Rect (topPanel.width * 0.73f, offset, topPanel.width * 0.135f, topPanel.width * 0.0325f), "Downloading", "MessageBoxWhite");
		}
		else
		{
			if(System.IO.File.Exists(Application.persistentDataPath + "/Science3.mp4"))
			{

			if(DoButton (new Rect (topPanel.width * 0.03f, offset, topPanel.width * 0.685f, topPanel.width * 0.0325f), "Science3.mp4", "MessageBoxWhite"))
			{
				
					string vidPath = "file://"+ Application.persistentDataPath + "/Science3.mp4";
				Handheld.PlayFullScreenMovie (vidPath, Color.black, FullScreenMovieControlMode.Full);
				
				
				
				Debug.Log ("Play vid.");
			}
			}
			else
			{
				GUI.Box (new Rect (topPanel.width * 0.03f, offset, topPanel.width * 0.685f, topPanel.width * 0.0325f), "Not found!", "MessageBoxWhite");
			}
		if(DoButton(new Rect (topPanel.width * 0.73f, offset, topPanel.width * 0.135f, topPanel.width * 0.0325f), "Download", "MessageBoxWhite"))
		{
			if(MCP.userInfo != null && !isDownloading)
			{
				isDownloading = true;
				StartCoroutine(MCP.TestDownload());
			}
		}

		}*/

		/*		offset += topPanel.width * 0.0325f;
				flipFlop = 1;
			}
			else
			{
//				if (DoButton (new Rect (topPanel.width * 0.03f, offset, topPanel.width * 0.685f, topPanel.width * 0.0325f), vid.title, "MessageBoxBlue")) {
//					
//					MCP.webURLToPlay = MCP.serverURL + vid.url;
//					LoadMenu("WebVideoPlayer");
//					Debug.Log ("Play vid.");
//					
//				} 
				GUI.Box (new Rect (topPanel.width * 0.73f, offset, topPanel.width * 0.135f, topPanel.width * 0.0325f), "Download", "MessageBoxBlue");
				offset += topPanel.width * 0.0325f;
				flipFlop = 0;
			}
		}*/

		GUI.EndScrollView ();
//			if (DoButton (new Rect (topPanel.width - (topPanel.width * 0.145f), topPanel.height * 0.925f, topPanel.width * 0.135f, topPanel.width * 0.0325f), "Close", "BlueButton")) {
//					Application.LoadLevel ("TeamResources_Screen");
//			}

		GUI.DrawTexture (new Rect (topPanel.width - (topPanel.width * 0.185f), topPanel.height * 0.925f, topPanel.width * 0.135f, topPanel.width * 0.0325f), closeIconImage, ScaleMode.ScaleToFit);

		GUI.EndGroup ();
	}

}