﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SelectImageFunction : MenuScreen
{
	public GameObject editCanvas;
	private GameObject localCanvas;
	private GameObject sceneCanvas;
	
	private Sprite cameraSprite;
	private Sprite clipartSprite;
	private Sprite photosSprite;
	private Sprite painterSprite;

	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		// Initialize the base class.
		Init ();
		
		// Load resources.
		cameraSprite = Resources.Load<Sprite> ("GUI/bm_camera");
		clipartSprite = Resources.Load<Sprite> ("GUI/bm_clipart");
		photosSprite = Resources.Load<Sprite> ("GUI/bm-addphotos");
		painterSprite = Resources.Load<Sprite> ("GUI/bm_paintbox");
		
		// Create the GUI.
		CreateGUI ();
		
		localCanvas.transform.localScale = Vector2.zero;
		
		MenuScreen.firstFrameTimer = -1;
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Get the current canvas.
		sceneCanvas = MakeCanvas (false);
		sceneCanvas.SetActive(false);
		
		// Make a new the canvas.
		localCanvas = MakeCanvas(true);
		localCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
		
		// Title
		GameObject titleBarObject = MakeTitleBar (localCanvas, MCP.Text (2401)/*"Choose Image"*/);
		titleBarObject.name = "TitleBarObject";
		
		// Description.
		GameObject descriptionTextObject = MakeLabel (localCanvas, MCP.Text (2402)/*"Would you like to draw your goal, or select an image from a file on your device?"*/, new Rect(Screen.width * 0.1f, 0, Screen.width * 0.8f, Screen.height * 0.4f), TextAnchor.MiddleCenter, "InnerLabel");
		descriptionTextObject.name = "DescriptionTextObject";
		
		float xPos = Screen.width * 0.025f;
		
#if UNITY_IOS
//		xPos = Screen.width * 0.1f;
#endif
		
		// Button to go to painter.
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			OpenPainter
		};
		GameObject openPainterButtonBackground = MakeButton (localCanvas, "", iconButton, new Rect(xPos, Screen.height * 0.4f, Screen.width * 0.2f, Screen.height * 0.3f), funcList);
		openPainterButtonBackground.name = "OpenPainterButtonBackground";
		openPainterButtonBackground.GetComponent<Image>().type = Image.Type.Sliced;
		
		// Painter button label
		GameObject openPainterLabel = MakeLabel (localCanvas, MCP.Text (2403)/*"Paint Package"*/, new Rect(xPos + (Screen.width * 0.025f), Screen.height * 0.52f, Screen.width * 0.15f, Screen.height * 0.2f), TextAnchor.MiddleCenter, "InnerLabel");
		openPainterLabel.name = "OpenPainterLabel";
		openPainterLabel.transform.SetParent (openPainterButtonBackground.transform, true);
		openPainterLabel.transform.localScale = Vector3.one;
		// painter graphic.
		GameObject openPainterButton = MakeButton (localCanvas, "", painterSprite, new Rect(xPos + (Screen.width * 0.025f), Screen.height * 0.4f, Screen.width * 0.15f, Screen.height * 0.2f), funcList);
		openPainterButton.name = "OpenPainterButton";
		
		// Button to go to painter.
		funcList = new List<MyVoidFunc>
		{
			OpenPainterToEdit
		};
		GameObject openPainterEditButtonBackground = MakeButton (localCanvas, "Edit Current", iconButton, new Rect(xPos, Screen.height * 0.7f, Screen.width * 0.2f, Screen.height * 0.1f), funcList);
		openPainterEditButtonBackground.name = "OpenPainterEditButtonBackground";
		openPainterEditButtonBackground.GetComponent<Image>().type = Image.Type.Sliced;
		// Set to inactive if there is no image to edit.
		if(MCP.loadedTexture == null || MCP.loadedTexture.width < 16)
		{
			openPainterEditButtonBackground.GetComponent<Button>().interactable = false;
			Color c = openPainterEditButtonBackground.transform.GetChild (0).gameObject.GetComponent<Text>().color;
			c.a = 0.4f;
			openPainterEditButtonBackground.transform.GetChild (0).gameObject.GetComponent<Text>().color = c;
		}
		
		xPos = Screen.width * 0.275f;
		
		#if UNITY_IOS
//		xPos = Screen.width * 0.4f;
		#endif
		
		// Button to open file.
		funcList = new List<MyVoidFunc>
		{
			OpenFileBrowser
		};
		GameObject openFileBrowserButtonBackground = MakeButton (localCanvas, "", iconButton, new Rect(xPos, Screen.height * 0.4f, Screen.width * 0.2f, Screen.height * 0.3f), funcList);
		openFileBrowserButtonBackground.name = "OpenFileBrowserButtonBackground";
		openFileBrowserButtonBackground.GetComponent<Image>().type = Image.Type.Sliced;
		
		GameObject openFileLabel = MakeLabel (localCanvas, MCP.Text (2404)/*"Add Photos"*/, new Rect(xPos + (Screen.width * 0.025f), Screen.height * 0.52f, Screen.width * 0.15f, Screen.height * 0.2f), TextAnchor.MiddleCenter, "InnerLabel");
		openFileLabel.name = "OpenFileLabel";
		openFileLabel.transform.SetParent (openFileBrowserButtonBackground.transform, true);
		openFileLabel.transform.localScale = Vector3.one;
		// File browser graphic.
		GameObject openFileButton = MakeButton (localCanvas, "", photosSprite, new Rect(xPos + (Screen.width * 0.025f), Screen.height * 0.4f, Screen.width * 0.15f, Screen.height * 0.2f), funcList);
		openFileButton.name = "OpenFileButton";
		
		xPos = Screen.width * 0.525f;
		
		#if UNITY_IOS
//		xPos = Screen.width * 0.7f;
		#endif
		
		// Button to open clipart.
		funcList = new List<MyVoidFunc>
		{
			OpenClipArt
		};
		GameObject openClipArtButtonBackground = MakeButton (localCanvas, "", iconButton, new Rect(xPos, Screen.height * 0.4f, Screen.width * 0.2f, Screen.height * 0.3f), funcList);
		openClipArtButtonBackground.name = "OpenClipArtButtonBackground";
		openClipArtButtonBackground.GetComponent<Image>().type = Image.Type.Sliced;
		
		GameObject openClipArtLabel = MakeLabel (localCanvas, MCP.Text (2405)/*"Clip Art"*/, new Rect(xPos + (Screen.width * 0.025f), Screen.height * 0.52f, Screen.width * 0.15f, Screen.height * 0.2f), TextAnchor.MiddleCenter, "InnerLabel");
		openClipArtLabel.name = "OpenClipArtLabel";
		openClipArtLabel.transform.SetParent (openClipArtButtonBackground.transform, true);
		openClipArtLabel.transform.localScale = Vector3.one;
		// Clip art graphic.
		GameObject openClipArtButton = MakeButton (localCanvas, "", clipartSprite, new Rect(xPos + (Screen.width * 0.025f), Screen.height * 0.4f, Screen.width * 0.15f, Screen.height * 0.2f), funcList);
		openClipArtButton.name = "OpenClipArtButton";
		
//#if !UNITY_IOS
		// Button to open camera.
		funcList = new List<MyVoidFunc>
		{
			OpenCamera
		};
		GameObject openCameraButtonBackground = MakeButton (localCanvas, "", iconButton, new Rect(Screen.width * 0.775f, Screen.height * 0.4f, Screen.width * 0.2f, Screen.height * 0.3f), funcList);
		openCameraButtonBackground.name = "OpenCameraButtonBackground";
		openCameraButtonBackground.GetComponent<Image>().type = Image.Type.Sliced;
		
		GameObject openCameraLabel = MakeLabel (localCanvas, MCP.Text (2406)/*"Camera"*/, new Rect(Screen.width * 0.8f, Screen.height * 0.52f, Screen.width * 0.15f, Screen.height * 0.2f), TextAnchor.MiddleCenter, "InnerLabel");
		openCameraLabel.name = "OpenCameraLabel";
		openCameraLabel.transform.SetParent (openCameraButtonBackground.transform, true);
		openCameraLabel.transform.localScale = Vector3.one;
		// Camera graphic.
		GameObject openCameraButton = MakeButton (localCanvas, "", cameraSprite, new Rect(Screen.width * 0.8f, Screen.height * 0.4f, Screen.width * 0.15f, Screen.height * 0.2f), funcList);
		openCameraButton.name = "OpenCameraButton";
//#endif
		
		// Button to go back
		GameObject backButtonBackgroundObject = MakeImage (localCanvas, new Rect(Screen.width * 0.825f, Screen.height * 0.85f, Screen.width * 0.15f, Screen.height * 0.1f), buttonBackground, true);
		backButtonBackgroundObject.name = "BackButtonBackgroundObject";
		
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				MCP.loadedTexture = null;
				Back();
			}
		};
		GameObject backButtonObject = MakeButton (localCanvas, MCP.Text (219)/*"Back"*/, new Rect(Screen.width * 0.825f, Screen.height * 0.85f, Screen.width * 0.15f, Screen.height * 0.1f), funcList);
		backButtonObject.name = "BackButtonObject";
		backButtonObject.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
		backButtonObject.GetComponent<Button>().image = backButtonBackgroundObject.GetComponent<Image>();
	}
	
	/// <summary>
	/// Opens the file browser.
	/// </summary>
	public void OpenFileBrowser()
	{
		// Reactivate the scene canvas so it can be seen by the file browser/picture gallery.
		sceneCanvas.SetActive (true);
		// Get a reference to the camera object.
		GameObject camObj = GameObject.FindGameObjectWithTag ("MainCamera");
		
#if UNITY_WEBPLAYER
		// Add the picture gallery script.
		camObj.AddComponent<PictureGallery> ();
		// Set the picture gallery variables.
		camObj.GetComponent<PictureGallery>().sceneCanvas = sceneCanvas;
#else
		// Add the file browser script.
		camObj.AddComponent<FileBrowser> ();
		// Set the file browser variables.
		camObj.GetComponent<FileBrowser> ().currentFilePath = Application.persistentDataPath;
		camObj.GetComponent<FileBrowser> ().upPathLimit = Application.persistentDataPath;
		camObj.GetComponent<FileBrowser> ().allowedExtensions = new string[]{"png", "jpg"};
		camObj.GetComponent<FileBrowser>().sceneCanvas = sceneCanvas;
#endif	
		// Close this script.
		Close ();
	}
	
	/// <summary>
	/// Opens the painter.
	/// </summary>
	public void OpenPainter()
	{
		// Reset the first frame timer.
		MenuScreen.firstFrameTimer = 0;
		// Reset loaded texture.
		MCP.loadedTexture = null;
		
		// Reactivate the scene canvas so it can be seen by the painter.
		sceneCanvas.SetActive (true);
		// Get a reference to the camera object.
		GameObject camObj = GameObject.FindGameObjectWithTag ("MainCamera");
		// Add the painter component.
		camObj.AddComponent<painter> ();
		// Set the painter variables.
		camObj.GetComponent<painter>().sceneCanvas = sceneCanvas;
		camObj.GetComponent<painter>().editCanvas = editCanvas;
		// enable the canvas camera.
		GameObject.Find ("CanvasCamera").GetComponent<Camera>().enabled  = true;
		
		camObj.GetComponent<painter> ().edittingImage = false;
		
		// Close this script.
		Close ();
	}

	/// <summary>
	/// Opens the painter to edit the current image.
	/// </summary>
	public void OpenPainterToEdit()
	{
		// Reset the first frame timer.
		MenuScreen.firstFrameTimer = 0;
		
		// Reactivate the scene canvas so it can be seen by the painter.
		sceneCanvas.SetActive (true);
		// Get a reference to the camera object.
		GameObject camObj = GameObject.FindGameObjectWithTag ("MainCamera");
		// Add the painter component.
		camObj.AddComponent<painter> ();
		// Set the painter variables.
		camObj.GetComponent<painter>().sceneCanvas = sceneCanvas;
		camObj.GetComponent<painter>().editCanvas = editCanvas;
		// enable the canvas camera.
		GameObject.Find ("CanvasCamera").GetComponent<Camera>().enabled  = true;
		
		camObj.GetComponent<painter> ().edittingImage = true;
		
		// Close this script.
		Close ();
	}
		
	/// <summary>
	/// Opens the clip art.
	/// </summary>
	public void OpenClipArt()
	{
		// Reactivate the scene canvas so it can be seen by the painter.
		sceneCanvas.SetActive (true);
		// Get a reference to the camera object.
		GameObject camObj = GameObject.FindGameObjectWithTag ("MainCamera");
		
//#if UNITY_WEBPLAYER
		// Add the picture gallery script.
		camObj.AddComponent<PictureGallery> ();
		// Set the picture gallery variables.
		camObj.GetComponent<PictureGallery>().sceneCanvas = sceneCanvas;
		camObj.GetComponent<PictureGallery>().isClipArt = true;
//#else
//		// Add the file browser script.
//		camObj.AddComponent<FileBrowser> ();
//		
//		// Set current file path to the app data path.
//		if(!System.IO.Directory.Exists (Application.persistentDataPath + "/clipart"))
//		{
//			System.IO.Directory.CreateDirectory (Application.persistentDataPath + "/clipart");
//		}
//		
//		// Set the file browser variables.
//		camObj.GetComponent<FileBrowser> ().currentFilePath = Application.persistentDataPath + "/clipart";
//		camObj.GetComponent<FileBrowser> ().upPathLimit = Application.persistentDataPath + "/clipart";
//		camObj.GetComponent<FileBrowser> ().allowedExtensions = new string[]{"png", "jpg"};
//		camObj.GetComponent<FileBrowser>().sceneCanvas = sceneCanvas;
//#endif		
		// Close this script.
		Close ();
	}
	
	/// <summary>
	/// Opens the camera.
	/// </summary>
	public void OpenCamera()
	{
		// Reactivate the scene canvas so it can be seen by the camera scene.
		sceneCanvas.SetActive (true);
		// Get a referene to the camera object.
		GameObject camObj = GameObject.FindGameObjectWithTag ("MainCamera");
		// Add the camera script to the camera.
		camObj.AddComponent<WebcamTextureTest> ();
		// Set the camera variables.
		camObj.GetComponent<WebcamTextureTest> ().sceneCanvas = sceneCanvas;
		// Close this script.
		Close ();
	}
	
	/// <summary>
	/// Overrides the base Back.
	/// </summary>
	public override void Back()
	{
		// Reenable the main script.
		sceneCanvas.SetActive (true);
		// Make the guided goal map process this as a FileBrowserClosed call.
		SendMessage ("FileBrowserClosed", SendMessageOptions.DontRequireReceiver);
		// Close this script.
		Close();
	}
	
	/// <summary>
	/// Close this instance.
	/// </summary>
	private void Close()
	{
		// Reset the first frame timer.
		MenuScreen.firstFrameTimer = 0;
		// Destroy this scripts game objects.
		Destroy (localCanvas);
		// Destroy this script.
		Destroy (this);
	}
	
	public override void DoGUI(){}
}
