using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ViewGoalMap_Screen : MenuScreen
{
	private List<GameObject> hideForScreenshot;
	private GameObject localCanvas;
	private GameObject goalMapParentObject;
	private GameObject goalMapObject;
	private GameObject changeTextPicGoalMapButton;
	private GameObject toggleShowingBothTreesButton;
	//private GameObject saveScreenshotFileName;
	private GameObject screenshotTitle;
	private GameObject loadingBackground;
	private GameObject loadingProgress;
	private GameObject loadingSpinner;
	
	private List<Texture2D> backgroundTextures;
	private Sprite[] zoomInSprite;
	private Sprite[] zoomOutSprite;
	private Sprite[] rotateSprite;
	private Sprite[] changeTextPicSprite;
	private Sprite[] showOneOrTwoSprite;
	private Sprite[] screenshotSprite;
	private Sprite[] loadBackgroundSprite;
	private Sprite[] loadGoalMapSprite;
	private Sprite spinnerSprite;
	private Sprite defaultSprite;
	
#if !UNITY_ANDROID && !UNITY_IPHONE
	private byte[] screenshot;
#endif
	public bool fileBrowserOpen = false;
	private MCP.GoalMap goalMap;
	private bool gmRotated = false;
	private bool showBoth = true;
	private bool readyToScreenshot = false;
	private bool screenshotTaken = false;
	private bool openingBackground = false;
	private bool loaded = false;
	private bool loading = false;
	private float screenshotTimer = 0f;
	private float zoom = -2f;
	private float startZoom;
	private float startDist;
	private float progress = 0;
	private int currentBackgroundIndex = 0;
	private Rect screenSize;
	private Vector2 startPan;
	private Vector2 dragStart;
	private Vector3 showBothPos;
	private Vector3 imageTreePos;
	private string screenshotFileName = MCP.Text (2501);/*"My Goal Map";*/
	private float spinnerRot;
	private Texture mainBGText;
	public Texture[] screenshotBGTextures;

	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		// Initialize the base class.
		Init ();
		
		// Initialise lists/arrays
		backgroundTextures = new List<Texture2D>();
		zoomInSprite = new Sprite[2];
		zoomOutSprite = new Sprite[2];
		rotateSprite = new Sprite[2];
		changeTextPicSprite = new Sprite[4];
		showOneOrTwoSprite = new Sprite[2];
		screenshotSprite = new Sprite[2];
		loadBackgroundSprite = new Sprite[2];
		loadGoalMapSprite = new Sprite[2];
		
		// If the pulled images list is not initialized, initalize it.
		if(MCP.pulledImages == null)
		{
			MCP.pulledImages = new List<Texture2D>();
		}
		
		// Load resources.
		zoomInSprite[0] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_plus");
		zoomInSprite[1] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_plus_active");
		zoomOutSprite[0] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_minus");
		zoomOutSprite[1] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_minus_active");
		rotateSprite[0] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_orientation");
		rotateSprite[1] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_orientation_active");
		changeTextPicSprite[0] = Resources.Load<Sprite> ("GUI/GoalMap/bm_left_brain");
		changeTextPicSprite[1] = Resources.Load<Sprite> ("GUI/GoalMap/bm_left_brain_active");
		changeTextPicSprite[2] = Resources.Load<Sprite> ("GUI/GoalMap/bm_right_brain");
		changeTextPicSprite[3] = Resources.Load<Sprite> ("GUI/GoalMap/bm_right_brain_active");
		showOneOrTwoSprite[0] = Resources.Load<Sprite> ("GUI/GoalMap/bm_whole_brain");
		showOneOrTwoSprite[1] = Resources.Load<Sprite> ("GUI/GoalMap/bm_whole_brain_active");
		screenshotSprite[0] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_screenshot");
		screenshotSprite[1] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_screenshot_active");
		loadBackgroundSprite[0] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_openbg");
		loadBackgroundSprite[1] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_openbg_active");
		loadGoalMapSprite[0] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_openmap");
		loadGoalMapSprite[1] = Resources.Load<Sprite> ("GUI/GoalMap/goalmap_openmap_active");
		spinnerSprite = Resources.Load<Sprite> ("GUI/bm_rotate");
		defaultSprite = Resources.Load<Sprite>("GUI/bm_box_ltpurple");
		
		backgroundTextures.Add (Resources.Load<Texture2D>("GUI/bm_background1_2048_4"));
		backgroundTextures.Add (Resources.Load<Texture2D>("bm_video_placeholder"));
		backgroundTextures.Add (Resources.Load<Texture2D>("GUI/gamesbg_curves"));
		
		// Initialize variables.
		hideForScreenshot = new List<GameObject>();
		goalMap = new MCP.GoalMap();
		
		// Try to get the background texture.
		try
		{
			mainBGText = GameObject.Find ("BackgroundCamera").GetComponent<BackgroundPlane>().backgroundTexture;
		}
		// If this has failed, instantiate the background panel and get the texture from it.
		catch(System.Exception e)
		{
			Debug.Log ("Error: " + e.Message);
			GameObject backgroundCamera = Instantiate (Resources.Load ("Prefabs/BackgroundCamera") as GameObject);
			backgroundCamera.name = "BackgroundCamera";
			mainBGText = backgroundCamera.GetComponent<BackgroundPlane>().backgroundTexture;
		}
		
		if( !MCP.viewingFriendGM)
		{
			MCP.gmIndex = 0;
			
			if(MCP.userInfo != null)
			{
				StartCoroutine (MCP.GetGoalMap (MCP.userInfo.username));
			}
		}
		
		// Create the GUI.
		CreateGUI ();
		
		localCanvas.transform.localScale = Vector2.zero;
		
		// Store the position of the goal maps when both should be showing.
		showBothPos = goalMapObject.transform.localPosition;
		showBothPos.x = Screen.width * 0.17f;
		showBothPos.y = Screen.height * 0.615f;
		imageTreePos = goalMapObject.transform.GetChild (1).localPosition;
		imageTreePos.x = goalMapObject.transform.localPosition.x + (Screen.width * 0.05f);
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Find the current canvas.
		localCanvas = MakeCanvas ();
		
		localCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
		firstFrameTimer = 0;
		
		// Make a different canvas for the goal map.
		goalMapParentObject = MakePanel (localCanvas, new Rect(Screen.width * 0.65f, Screen.height * 0.15f, Screen.width * 0.7f, Screen.height * 0.825f));
		goalMapParentObject.name = "GMCanvas";
		goalMapParentObject.AddComponent<Mask>();
		goalMapParentObject.GetComponent<Mask>().showMaskGraphic = false;
		goalMapParentObject.AddComponent<Image>();
		
		// Make the side gizmo.
		GameObject sideGizmo = MakeSideGizmo (localCanvas, true, true);
		hideForScreenshot.Add (sideGizmo);
		hideForScreenshot.Add (GameObject.Find ("BackButton"));
		
		UnityEngine.UI.Navigation nav = new UnityEngine.UI.Navigation();
		nav.mode = UnityEngine.UI.Navigation.Mode.None;
		
		// Make the zoom in button.
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			ZoomIn
		};
		GameObject zoomInButton = MakeIconButton (localCanvas, MCP.Text (2504)/*"Zoom In*"*/, null, zoomInSprite[0], new Rect(Screen.width * 0.025f, Screen.height * 0.225f, Screen.width * 0.1f, Screen.width * 0.1f), funcList);
		zoomInButton.name = "ZoomInButton";
		zoomInButton.transform.GetChild (0).gameObject.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		zoomInButton.transform.GetChild (0).gameObject.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Overflow;
		zoomInButton.transform.GetChild (0).gameObject.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
		// Make hover sprite
		zoomInButton.GetComponent<Button>().navigation = nav;
		zoomInButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		SpriteState spriteState = new SpriteState();
		zoomInButton.GetComponent<Button>().targetGraphic = zoomInButton.transform.GetChild (1).gameObject.GetComponent<Image>();
		spriteState.highlightedSprite = zoomInSprite[1];
		spriteState.pressedSprite = zoomInSprite[1];
		zoomInButton.GetComponent<Button>().spriteState = spriteState;
		hideForScreenshot.Add (zoomInButton);
		
		// Make the zoom out button.
		funcList = new List<MyVoidFunc>
		{
			ZoomOut
		};
		GameObject zoomOutButton = MakeIconButton (localCanvas, MCP.Text (2505)/*"Zoom Out*"*/, null, zoomOutSprite[0], new Rect(Screen.width * 0.025f, Screen.height * 0.4f, Screen.width * 0.1f, Screen.width * 0.1f), funcList);
		zoomOutButton.name = "ZoomOutButton";
		zoomOutButton.transform.GetChild (0).gameObject.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		zoomOutButton.transform.GetChild (0).gameObject.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Overflow;
		zoomOutButton.transform.GetChild (0).gameObject.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
		// Make hover sprite
		zoomOutButton.GetComponent<Button>().navigation = nav;
		zoomOutButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		zoomOutButton.GetComponent<Button>().targetGraphic = zoomOutButton.transform.GetChild (1).gameObject.GetComponent<Image>();
		spriteState.highlightedSprite = zoomOutSprite[1];
		spriteState.pressedSprite = zoomOutSprite[1];
		zoomOutButton.GetComponent<Button>().spriteState = spriteState;
		hideForScreenshot.Add (zoomOutButton);
		
		// Make the rotate goal map button.
		funcList = new List<MyVoidFunc>
		{
			RotateGoalMap
		};
		GameObject rotateGoalMapButton = MakeIconButton (localCanvas, MCP.Text (2506)/*"Rotate*"*/, null, rotateSprite[0], new Rect(Screen.width * 0.025f, Screen.height * 0.575f, Screen.width * 0.1f, Screen.width * 0.1f), funcList);
		rotateGoalMapButton.name = "RotateGoalMapButton";
		rotateGoalMapButton.transform.GetChild (0).gameObject.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		rotateGoalMapButton.transform.GetChild (0).gameObject.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Overflow;
		rotateGoalMapButton.transform.GetChild (0).gameObject.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
		// Make hover sprite
		rotateGoalMapButton.GetComponent<Button>().navigation = nav;
		rotateGoalMapButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		rotateGoalMapButton.GetComponent<Button>().targetGraphic = rotateGoalMapButton.transform.GetChild (1).gameObject.GetComponent<Image>();
		spriteState.highlightedSprite = rotateSprite[1];
		spriteState.pressedSprite = rotateSprite[1];
		rotateGoalMapButton.GetComponent<Button>().spriteState = spriteState;
		hideForScreenshot.Add (rotateGoalMapButton);
		
		// Make the change text/picture button.
		funcList = new List<MyVoidFunc>
		{
			ChangeTextPicture
		};
		changeTextPicGoalMapButton = MakeIconButton (localCanvas, MCP.Text (2507)/*"Switch*"*/, null, changeTextPicSprite[0], new Rect(Screen.width * 0.025f, Screen.height * 0.75f, Screen.width * 0.1f, Screen.width * 0.1f), funcList);
		changeTextPicGoalMapButton.name = "ChangeTextPicGoalMapButton";
		changeTextPicGoalMapButton.transform.GetChild (0).gameObject.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		changeTextPicGoalMapButton.transform.GetChild (0).gameObject.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Overflow;
		changeTextPicGoalMapButton.transform.GetChild (0).gameObject.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
		// Make hover sprite
		changeTextPicGoalMapButton.GetComponent<Button>().navigation = nav;
		changeTextPicGoalMapButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		changeTextPicGoalMapButton.GetComponent<Button>().targetGraphic = changeTextPicGoalMapButton.transform.GetChild (1).gameObject.GetComponent<Image>();
		spriteState.highlightedSprite = changeTextPicSprite[1];
		spriteState.pressedSprite = changeTextPicSprite[1];
		changeTextPicGoalMapButton.GetComponent<Button>().spriteState = spriteState;
		hideForScreenshot.Add (changeTextPicGoalMapButton);
		
		// Make the input field for the save screenshot name.
//		saveScreenshotFileName = MakeInputField (localCanvas, screenshotFileName, new Rect(Screen.width * 0.1f, Screen.height * 0.02f, Screen.width * 0.8f, Screen.height * 0.125f), false, inputBackground);
//		saveScreenshotFileName.GetComponent<Text>().color = bmDarkBlue;
//		saveScreenshotFileName.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
//		hideForScreenshot.Add (saveScreenshotFileName.transform.parent.gameObject);
		
		screenshotTitle = MakeLabel (localCanvas, screenshotFileName, new Rect(Screen.width * 0.1f, Screen.height * 0.02f, Screen.width * 0.8f, Screen.height * 0.125f));
		screenshotTitle.name = "ScreenshotTitle";
		
		// Make the toggle show both button.
		funcList = new List<MyVoidFunc>
		{
			ToggleShowBoth
		};
		toggleShowingBothTreesButton = MakeIconButton (localCanvas, MCP.Text (2508)/*"Show Both*"*/, null, showOneOrTwoSprite[1], new Rect(Screen.width * 0.875f, Screen.height * 0.225f, Screen.width * 0.1f, Screen.width * 0.1f), funcList, "InnerLabel");
		toggleShowingBothTreesButton.name = "ToggleShowingBothTreesButton";
		toggleShowingBothTreesButton.transform.GetChild (0).gameObject.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		toggleShowingBothTreesButton.transform.GetChild (0).gameObject.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Overflow;
		toggleShowingBothTreesButton.transform.GetChild (0).gameObject.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
		// Make hover sprite
		toggleShowingBothTreesButton.GetComponent<Button>().navigation = nav;
		toggleShowingBothTreesButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		toggleShowingBothTreesButton.GetComponent<Button>().targetGraphic = toggleShowingBothTreesButton.transform.GetChild (1).gameObject.GetComponent<Image>();
		spriteState.highlightedSprite = showOneOrTwoSprite[1];
		spriteState.pressedSprite = showOneOrTwoSprite[0];
		toggleShowingBothTreesButton.GetComponent<Button>().spriteState = spriteState;
		hideForScreenshot.Add (toggleShowingBothTreesButton);
	
		// Make the screenshot goal map button.
		funcList = new List<MyVoidFunc>
		{
			ScreenshotGoalMap
		};
		GameObject screenshotGoalMapButton = MakeIconButton (localCanvas, MCP.Text (2509)/*"Screenshot*"*/, null, screenshotSprite[0], new Rect(Screen.width * 0.875f, Screen.height * 0.4f, Screen.width * 0.1f, Screen.width * 0.1f), funcList);
		screenshotGoalMapButton.name = "ScreenshotGoalMapButton";
		screenshotGoalMapButton.transform.GetChild (0).gameObject.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		screenshotGoalMapButton.transform.GetChild (0).gameObject.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Overflow;
		screenshotGoalMapButton.transform.GetChild (0).gameObject.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
		// Make hover sprite
		screenshotGoalMapButton.GetComponent<Button>().navigation = nav;
		screenshotGoalMapButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		spriteState = new SpriteState();
		screenshotGoalMapButton.GetComponent<Button>().targetGraphic = screenshotGoalMapButton.transform.GetChild (1).gameObject.GetComponent<Image>();
		spriteState.highlightedSprite = screenshotSprite[1];
		spriteState.pressedSprite = screenshotSprite[1];
		screenshotGoalMapButton.GetComponent<Button>().spriteState = spriteState;
		hideForScreenshot.Add (screenshotGoalMapButton);

		if( !MCP.viewingFriendGM )
		{
			// Make the open background file button.
			funcList = new List<MyVoidFunc>
			{
				CycleBackgroundImage
			};
			GameObject loadBackgroundButton = MakeIconButton (localCanvas, MCP.Text (2510)/*"Background*"*/, null, loadBackgroundSprite[0], new Rect(Screen.width * 0.875f, Screen.height * 0.575f, Screen.width * 0.1f, Screen.width * 0.1f), funcList);
			loadBackgroundButton.name = "LoadBackgroundButton";
			loadBackgroundButton.transform.GetChild (0).gameObject.GetComponent<Text>().fontSize = (int)_fontSize_Small;
			loadBackgroundButton.transform.GetChild (0).gameObject.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Overflow;
			loadBackgroundButton.transform.GetChild (0).gameObject.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
			// Make hover sprite
			loadBackgroundButton.GetComponent<Button>().navigation = nav;
			loadBackgroundButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
			
			spriteState = new SpriteState();
			loadBackgroundButton.GetComponent<Button>().targetGraphic = loadBackgroundButton.transform.GetChild (1).gameObject.GetComponent<Image>();
			spriteState.highlightedSprite = loadBackgroundSprite[1];
			spriteState.pressedSprite = loadBackgroundSprite[1];
			loadBackgroundButton.GetComponent<Button>().spriteState = spriteState;
			hideForScreenshot.Add (loadBackgroundButton);
			
			// Make the load goal map button.
			funcList = new List<MyVoidFunc>
			{
				LoadGoalMap
			};
			GameObject loadGoalMapButton = MakeIconButton (localCanvas, MCP.Text (2511)/*"Goal Map*"*/, null, loadGoalMapSprite[0], new Rect(Screen.width * 0.875f, Screen.height * 0.75f, Screen.width * 0.1f, Screen.width * 0.1f), funcList);
			loadGoalMapButton.name = "LoadGoalMapButton";
			loadGoalMapButton.transform.GetChild (0).gameObject.GetComponent<Text>().fontSize = (int)_fontSize_Small;
			loadGoalMapButton.transform.GetChild (0).gameObject.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Overflow;
			loadGoalMapButton.transform.GetChild (0).gameObject.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
			// Make hover sprite
			loadGoalMapButton.GetComponent<Button>().navigation = nav;
			loadGoalMapButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
			
			spriteState = new SpriteState();
			loadGoalMapButton.GetComponent<Button>().targetGraphic = loadGoalMapButton.transform.GetChild (1).gameObject.GetComponent<Image>();
			spriteState.highlightedSprite = loadGoalMapSprite[1];
			spriteState.pressedSprite = loadGoalMapSprite[1];
			loadGoalMapButton.GetComponent<Button>().spriteState = spriteState;
			hideForScreenshot.Add (loadGoalMapButton);
		}
		
		// Make the goal map object.
		goalMapObject = MakeGoalMap (goalMapParentObject);
		goalMapObject.name = "GoalMapPrefab";
		
		goalMapParentObject.transform.SetAsLastSibling ();
		goalMapObject.transform.localPosition = new Vector3(Screen.width * 0.85f, Screen.height * 1.0f, 0);
		goalMapObject.transform.localScale = Vector3.one * 0.5f;
		GameObject.Find ("GoalMapPicsParent").SetActive (false);
		
		// Make the loading popup fade background.
		loadingBackground = MakeImage (localCanvas, new Rect(0, 0, Screen.width, Screen.height), menuBackground);
		loadingBackground.name = "LoadingBackground";
		
		// Make the loading popup background.
		GameObject loadingBackgroundPanel = MakeImage (localCanvas, new Rect(Screen.width * 0.25f, Screen.height * 0.3f, Screen.width * 0.5f, Screen.height * 0.45f), popup_back, true);
		loadingBackgroundPanel.name = "LoadingBackgroundPanel";
		loadingBackgroundPanel.transform.SetParent (loadingBackground.transform);
		
		// Make the loading text.
		GameObject loadingLabel = MakeLabel (localCanvas, MCP.Text (214)/*"Loading..."*/, new Rect(Screen.width * 0.35f, Screen.height * 0.2875f, Screen.width * 0.3f, Screen.height * 0.2f), TextAnchor.MiddleCenter, "InnerLabel");
		loadingLabel.name = "LoadingLabel";
		loadingLabel.transform.SetParent (loadingBackground.transform);
		
		loadingProgress = MakeLabel (localCanvas, "0%", new Rect(Screen.width * 0.35f, Screen.height * 0.4375f, Screen.width * 0.3f, Screen.height * 0.1f));
		loadingProgress.name = "loadingProgress";
		loadingProgress.transform.SetParent (loadingBackground.transform);
		
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				loading = false;
				MCP.pulledGoalMaps = false;
				StopCoroutine (LoadingGoalMap());
				loadingBackground.SetActive (false);
			}
		};
		GameObject loadingCancelButton = MakeButton (localCanvas, MCP.Text (207)/*"Cancel"*/, buttonBackground, new Rect(Screen.width * 0.4f, Screen.height * 0.65f, Screen.width * 0.2f, Screen.height * 0.06f), funcList);
		loadingCancelButton.name = "LoadingCancelButton";
		loadingCancelButton.transform.SetParent (loadingBackground.transform, true);
		loadingCancelButton.transform.localScale = Vector3.one;
		
		// Make the loading spinner.
		loadingSpinner = MakeImage (localCanvas, new Rect(Screen.width * 0.5f, Screen.height * 0.45f, Screen.width * 0.08f, Screen.width * 0.08f), spinnerSprite, false);
		loadingSpinner.name = "LoadingSpinner";
		loadingSpinner.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
		loadingSpinner.transform.SetParent (loadingBackground.transform);
	}
	
	/// <summary>
	/// Screenshots the goal map.
	/// </summary>
	public void ScreenshotGoalMap()
	{
		// Set the title label text to the input field.
//		screenshotTitle.GetComponent<Text>().text = saveScreenshotFileName.GetComponent<Text>().text;
		
		// Hide the obejcts that aren't wanted in the screenshot.
		foreach (GameObject go in hideForScreenshot)
		{
            if(go != null)
            {
                go.SetActive(false);
            }
		}
		
		// Show the title label object.
		screenshotTitle.SetActive (true);
		
		// Flag as ready to screenshot.
		readyToScreenshot = true;
	}
	
	/// <summary>
	/// Changes between the text goal map and the picture goal map.
	/// </summary>
	public void ChangeTextPicture()
	{
		// If both goal maps aren't showing...
		if(!showBoth)
		{
			// Enable/disable text.
			goalMapObject.transform.GetChild (0).gameObject.SetActive (!goalMapObject.transform.GetChild (0).gameObject.activeSelf);
			// Disable/enable pictures.
			goalMapObject.transform.GetChild (1).gameObject.SetActive (!goalMapObject.transform.GetChild (1).gameObject.activeSelf);
			
			// If the text map is enabled...
			if(goalMapObject.transform.GetChild (0).gameObject.activeSelf)
			{
				// Set the button sprite to select image sprite.
				changeTextPicGoalMapButton.transform.GetChild (1).gameObject.GetComponent<Image>().sprite = changeTextPicSprite[0];
				
				// Make hover sprite
				UnityEngine.UI.Navigation nav = new UnityEngine.UI.Navigation();
				nav.mode = UnityEngine.UI.Navigation.Mode.None;
				
				changeTextPicGoalMapButton.GetComponent<Button>().navigation = nav;
				changeTextPicGoalMapButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
				
				SpriteState spriteState = new SpriteState();
				changeTextPicGoalMapButton.GetComponent<Button>().targetGraphic = changeTextPicGoalMapButton.transform.GetChild (1).gameObject.GetComponent<Image>();
				spriteState.highlightedSprite = changeTextPicSprite[1];
				spriteState.pressedSprite = changeTextPicSprite[1];
				changeTextPicGoalMapButton.GetComponent<Button>().spriteState = spriteState;
			}
			// Otherwise...
			else
			{
				// Set the button sprite to select text sprite.
				changeTextPicGoalMapButton.transform.GetChild (1).gameObject.GetComponent<Image>().sprite = changeTextPicSprite[2];
				
				// Make hover sprite
				UnityEngine.UI.Navigation nav = new UnityEngine.UI.Navigation();
				nav.mode = UnityEngine.UI.Navigation.Mode.None;
				
				changeTextPicGoalMapButton.GetComponent<Button>().navigation = nav;
				changeTextPicGoalMapButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
				
				SpriteState spriteState = new SpriteState();
				changeTextPicGoalMapButton.GetComponent<Button>().targetGraphic = changeTextPicGoalMapButton.transform.GetChild (1).gameObject.GetComponent<Image>();
				spriteState.highlightedSprite = changeTextPicSprite[3];
				spriteState.pressedSprite = changeTextPicSprite[3];
				changeTextPicGoalMapButton.GetComponent<Button>().spriteState = spriteState;
			}
			
			// Move both trees back to the center
			Vector3 newPos = goalMapObject.transform.localPosition;
			if(gmRotated)
			{
				newPos.x = screenSize.width * 0.275f;
				newPos.y = screenSize.height * 0.45f;
			}
			else
			{
				newPos.x = screenSize.width * 0.35f;
				newPos.y = screenSize.height * 0.615f;
			}
			goalMapObject.transform.localPosition = newPos;
			
			// Move the image tree to the right
			newPos = goalMapObject.transform.GetChild (1).localPosition;
			newPos.x = 0;
			goalMapObject.transform.GetChild (1).localPosition = newPos;
		}
	}
	
	/// <summary>
	/// Moves the goal map.
	/// </summary>
	/// <param name="pointer">Pointer position.</param>
	/// <param name="pressed">If set to <c>true</c> pressed.</param>
	/// <param name="held">If set to <c>true</c> held.</param>
	/// <param name="unpressed">If set to <c>true</c> unpressed.</param>
	/// <param name="controlRect">Rect that movement can begin in.</param>
	void MoveGoalMap(Vector2 pointer, bool pressed, bool held, bool unpressed, Rect controlRect)
	{
		// Store the current position of the camera.
		Vector3 newPos = goalMapObject.transform.localPosition;
		
		// If this is the initial press.
		if(pressed)
		{
			// If the pointer is within the area the user can start moving...
			if(controlRect.Contains (pointer))
			{
				// Set the drag start to the initial camera position.
				dragStart = goalMapObject.transform.localPosition;
				// Get the starting position of the pointer.
				startPan = pointer;
			}
			// Otherwise, reset the start pan.
			else
			{
				startPan = Vector2.zero;
			}
		}
		
		// If the mouse pointer is held...
		if(held && startPan != Vector2.zero)
		{
			// Get the offset between the current mouse position and the new mouse position.
			newPos.x = (startPan.x - pointer.x);
			newPos.y = (startPan.y - pointer.y);
			// Set this offset to the camera position.
			newPos = new Vector3(dragStart.x, dragStart.y, 0) - (newPos / 1f);
			
			// Make sure the camera does not move off the canvas. Change depending on rotation.
			if(gmRotated)
			{
				if(newPos.x < screenSize.width * -0.2f)
				{
					newPos.x = screenSize.width * -0.2f;
				}
				
				if(newPos.x > screenSize.width * 0.8f)
				{
					newPos.x = screenSize.width * 0.8f;
				}
				
				if(newPos.y < screenSize.height * -0.0f)
				{
					newPos.y = screenSize.height * -0.0f;
				}
				
				if(newPos.y > screenSize.height * 1.0f)
				{
					newPos.y = screenSize.height * 1.0f;
				}
			}
			else
			{
				if(newPos.x < screenSize.width * 0.0f)
				{
					newPos.x = screenSize.width * 0.0f;
				}
				
				if(newPos.x > screenSize.width * 0.8f)
				{
					newPos.x = screenSize.width * 0.8f;
				}
				
				if(newPos.y < screenSize.height * 0.0f)
				{
					newPos.y = screenSize.height * 0.0f;
				}
				
				if(newPos.y > screenSize.height * 1.35f)
				{
					newPos.y = screenSize.height * 1.35f;
				}
			}
			
			// Set the new camera position.
			goalMapObject.transform.localPosition = newPos;
		}
		
		// If the pointer has been released...
		if(unpressed)
		{
			// Reset the initial positions.
			dragStart = Vector2.zero;
			startPan = Vector2.zero;
		}
	}
	
	/// <summary>
	/// Rotates the goal map.
	/// </summary>
	public void RotateGoalMap()
	{
		// If both goal maps aren't being shown.
		if(!showBoth)
		{
			gmRotated = !gmRotated;
			
			// If it is currently rotated...
			if(gmRotated)
			{
				// Rotate to 90 degrees.
				goalMapObject.transform.eulerAngles = new Vector3(0, 0, 90f);
			}
			// Otherwise
			else
			{
				// Rotate to 0 degrees.
				goalMapObject.transform.eulerAngles = new Vector3(0, 0, 0f);
			}
			
			// Update position to make sure it fits now it's rotated.
			Vector3 newPos = goalMapObject.transform.localPosition;
			
			if(gmRotated)
			{
				newPos.x = screenSize.width * 0.25f;
				newPos.y = screenSize.height * 0.4f;
			}
			else
			{
				newPos.x = screenSize.width * 0.35f;
				newPos.y = screenSize.height * 0.6f;
			}
			
			// Set the new camera position.
			goalMapObject.transform.localPosition = newPos;
		}
	}
	
	/// <summary>
	/// Zooms the goal map out.
	/// </summary>
	public void ZoomOut()
	{
		zoom -= 2.0f;
		
		// Round zoom to 2dp.
		zoom = Mathf.Round (zoom * 100f) / 100f;
		
		// Keep the zoom bound within limits.
		if(zoom < -6.0f)
		{
			zoom = -6.0f;
		}
		
		if(zoom > 100f)
		{
			zoom = 100f;
		}
		
		// Set the size to the new zoom.
		goalMapObject.transform.localScale = new Vector3(0.5f + (zoom * 0.01f), 0.5f + (zoom * 0.01f), 1);
	}
	
	/// <summary>
	/// Zooms the goal map in.
	/// </summary>
	public void ZoomIn()
	{
		zoom += 2.0f;
		
		// Round zoom to 2dp.
		zoom = Mathf.Round (zoom * 100f) / 100f;
		
		// Keep the zoom bound within limits.
		if(zoom < -10.0f)
		{
			zoom = -10.0f;
		}
		
		if(zoom > 100f)
		{
			zoom = 100f;
		}
		
		// Set the size to the new zoom.
		goalMapObject.transform.localScale = new Vector3(0.5f + (zoom * 0.01f), 0.5f + (zoom * 0.01f), 1);
	}
	
	/// <summary>
	/// Zooms the goal map.
	/// </summary>
	/// <param name="pointer">Pointer position.</param>
	/// <param name="pressed">If set to <c>true</c> pressed.</param>
	/// <param name="held">If set to <c>true</c> held.</param>
	/// <param name="unpressed">If set to <c>true</c> unpressed.</param>
	/// <param name="clickArea">Area that the zoom can begin from.</param>
	public void ZoomGoalMap(Vector2 pointer, bool pressed, bool held, bool unpressed, Rect clickArea)
	{
		// If this is the initial press.
		if(pressed)
		{
			// Set the drag start to the current pointer position.
			dragStart = pointer;
			// Set the initial zoom to the current zoom.
			startZoom = zoom;
		}
		
		// If the pointer is released...
		if(unpressed)
		{
			// Reset the start position.
			dragStart = Vector2.zero;
		}
		
		// If the mouse is held and the drag began within the canvas.
		if(held && dragStart != Vector2.zero)
		{
			// Set the zoom to the difference between the current position and the initial zoom.
			zoom = startZoom - ((dragStart.y - pointer.y)) * 0.1f;
		}
		
		// Round zoom to 2dp.
		zoom = Mathf.Round (zoom * 100f) / 100f;
		
		// Keep the zoom bound within limits.
		if(zoom < -10.0f)
		{
			zoom = -10.0f;
		}
		
		if(zoom > 100f)
		{
			zoom = 100f;
		}
		
		// Set the size to the new zoom.
		goalMapObject.transform.localScale = new Vector3(0.5f + (zoom * 0.01f), 0.5f + (zoom * 0.01f), 1);
	}
	
	/// <summary>
	/// Zooms the goal map.
	/// </summary>
	/// <param name="touches">Touches.</param>
	/// <param name="touchArea">Area the zoom can begin from.</param>
	void ZoomGoalMap(Touch[] touches, Rect touchArea)
	{
		// If the second touch has just begun...
		if(touches[1].phase == TouchPhase.Began)
		{
			// Set the initial distance between the touches.
			startDist = Vector2.Distance (touches[0].position, touches[1].position);
			// Set the start zoom to the current zoom.
			startZoom = zoom;
		}
		// Set the zoom to the difference between the current position and the initial zoom.
		zoom = (startZoom - ((startDist - Vector2.Distance (touches[0].position, touches[1].position)) * 0.05f));
		
		// Round zoom to 2dp.
		zoom = Mathf.Round (zoom * 100f) / 100f;
		
		// Keep the zoom bound within limits.
		if(zoom < -10.0f)
		{
			zoom = -10.0f;
		}
		
		if(zoom > 100f)
		{
			zoom = 100f;
		}
		
		goalMapObject.transform.localScale = new Vector3(0.5f + (zoom * 0.01f), 0.5f + (zoom * 0.01f), 1);
	}
	
	public void ScrollZoomCanvas(float zoomAmount)
	{
		zoom += zoomAmount * 10f;
		
		// Round zoom to 2dp.
		zoom = Mathf.Round (zoom * 100f) / 100f;
		
		// Keep the zoom within set bounds.
		if(zoom < -10.0f)
		{
			zoom = -10.0f;
		}
		
		if(zoom > 100f)
		{
			zoom = 100f;
		}
		
		goalMapObject.transform.localScale = new Vector3(0.5f + (zoom * 0.01f), 0.5f + (zoom * 0.01f), 1);
	}
	
	/// <summary>
	/// Loads the goal map.
	/// </summary>
	void LoadGoalMap()
	{
		openingBackground = false;
		// Reset the first frame timer.
		MenuScreen.firstFrameTimer = 0;
		
//#if UNITY_WEBPLAYER
		// Add the simulated file browser script and set variables.
		Camera.main.gameObject.AddComponent<SimulateFileBrowser>().sceneCanvas = localCanvas;
		fileBrowserOpen = true;
//#else
//		// Add the file browser and set variables.
//		Camera.main.gameObject.AddComponent<FileBrowser>().sceneCanvas = localCanvas;
//		Camera.main.gameObject.GetComponent<FileBrowser>().currentFilePath = Application.persistentDataPath + "/GoalMaps";
//		Camera.main.gameObject.GetComponent<FileBrowser>().upPathLimit = Application.persistentDataPath + "/GoalMaps";
//		Camera.main.gameObject.GetComponent<FileBrowser> ().allowedExtensions = new string[]{"txt"};
//#endif
	}
	
	/// <summary>
	/// Loads the goal map.
	/// </summary>
	/// <param name="goalMap">Goal map.</param>
	void LoadGoalMap(MCP.GoalMap goalMap)
	{
		screenshotTitle.GetComponent<Text>().text = goalMap.name;
		
		showBoth = true;
		
		// Populate the goal map object with the loading goal map.
		goalMapObject.GetComponent<PopGM_Prefab>().PopulateGoalMap(goalMap);
		
		// Reset the map type label.
		changeTextPicGoalMapButton.transform.GetChild (1).gameObject.GetComponent<Image>().sprite = changeTextPicSprite[0];
		
		// Make hover sprite
		UnityEngine.UI.Navigation nav = new UnityEngine.UI.Navigation();
		nav.mode = UnityEngine.UI.Navigation.Mode.None;
		
		changeTextPicGoalMapButton.GetComponent<Button>().navigation = nav;
		changeTextPicGoalMapButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		SpriteState spriteState = new SpriteState();
		changeTextPicGoalMapButton.GetComponent<Button>().targetGraphic = changeTextPicGoalMapButton.transform.GetChild (1).gameObject.GetComponent<Image>();
		spriteState.highlightedSprite = changeTextPicSprite[1];
		spriteState.pressedSprite = changeTextPicSprite[1];
		changeTextPicGoalMapButton.GetComponent<Button>().spriteState = spriteState;
		
		// If show both is flagged, back sure both are showing.
		if(showBoth)
		{
			// Enable text.
			goalMapObject.transform.GetChild (0).gameObject.SetActive (true);
			// Enable pictures.
			goalMapObject.transform.GetChild (1).gameObject.SetActive (true);
		}
	}
	
	/// <summary>
	/// Loadings the goal map.
	/// </summary>
	/// <returns>The goal map.</returns>
	private IEnumerator LoadingGoalMap()
	{
		loaded = false;
		loadingBackground.SetActive (true);
		
		goalMap = new MCP.GoalMap();
		// Make sure the goal map is populated with the new information.
		goalMapObject.GetComponent<PopGM_Prefab>().picPrefab.subGoal4 = null;
		// Enable text.
		goalMapObject.transform.GetChild (0).gameObject.SetActive (true);
		// Enable pictures.
		goalMapObject.transform.GetChild (1).gameObject.SetActive (true);
		
		yield return null;
		
		if(MCP.loadedString != null && MCP.loadedString != "")
		{
			// Make the goal map json a goal map object.
			goalMap = LitJson.JsonMapper.ToObject<MCP.GoalMap> (MCP.loadedString);
			
			// Load in the images from their byte data.
			for(int i = 0; i < goalMap.goals.Count; i++)
			{
				if(goalMap.goals[i].imageBytes != null)
				{
					goalMap.goals[i].image = new Texture2D(512, 512);
					goalMap.goals[i].image.LoadImage (goalMap.goals[i].imageBytes);
				}
			}
			for(int i = 0; i < goalMap.whys.Count; i++)
			{
				if(goalMap.whys[i].imageBytes != null)
				{
					goalMap.whys[i].image = new Texture2D(512, 512);
					goalMap.whys[i].image.LoadImage (goalMap.whys[i].imageBytes);
				}
			}
			for(int i = 0; i < goalMap.whos.Count; i++)
			{
				if(goalMap.whos[i].imageBytes != null)
				{
					goalMap.whos[i].image = new Texture2D(512, 512);
					goalMap.whos[i].image.LoadImage (goalMap.whos[i].imageBytes);
				}
			}
			for(int i = 0; i < goalMap.hows.Count; i++)
			{
				if(goalMap.hows[i].imageBytes != null)
				{
					goalMap.hows[i].image = new Texture2D(512, 512);
					goalMap.hows[i].image.LoadImage (goalMap.hows[i].imageBytes);
				}
			}
			
			// Populate the goal map ibject with the goal map.
			goalMapObject.GetComponent<PopGM_Prefab>().PopulateGoalMap(goalMap);
			
			// Reset the map type label.
			changeTextPicGoalMapButton.transform.GetChild (1).gameObject.GetComponent<Image>().sprite = changeTextPicSprite[0];
			
			// Make hover sprite
			UnityEngine.UI.Navigation nav = new UnityEngine.UI.Navigation();
			nav.mode = UnityEngine.UI.Navigation.Mode.None;
			
			changeTextPicGoalMapButton.GetComponent<Button>().navigation = nav;
			changeTextPicGoalMapButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
			
			SpriteState spriteState = new SpriteState();
			changeTextPicGoalMapButton.GetComponent<Button>().targetGraphic = changeTextPicGoalMapButton.transform.GetChild (1).gameObject.GetComponent<Image>();
			spriteState.highlightedSprite = changeTextPicSprite[1];
			spriteState.pressedSprite = changeTextPicSprite[1];
			changeTextPicGoalMapButton.GetComponent<Button>().spriteState = spriteState;
			
			// If show both is flagged, make sure it is showing both.
			if(showBoth)
			{
				// Enable text.
				goalMapObject.transform.GetChild (0).gameObject.SetActive (true);
				// Enable pictures.
				goalMapObject.transform.GetChild (1).gameObject.SetActive (true);
			}
		}
		else
		{
			// Enable pictures.
			goalMapObject.transform.GetChild (1).gameObject.SetActive (false);
		}
		
//#if !UNITY_WEBPLAYER
//		loaded = true;
//#endif
		
		showBoth = false;
		ToggleShowBoth ();
	}
	
	/// <summary>
	/// Process the file browser closing.
	/// </summary>
	public void FileBrowserClosed()
	{
		// If the user was trying to open a background...
		if(openingBackground)
		{
			// Make this the loaded texture.
			GameObject.Find ("BackgroundCamera").GetComponent<BackgroundPlane>().SetBackgroundTexture(MCP.loadedTexture);
			loaded = true;
		}
		// Otherwise...
		else
		{
			// Load in the goal map.
			StartCoroutine (LoadingGoalMap ());
		}
	}
	
	void OnEnable ()
	{
		// call backs
		ScreenshotManager.OnScreenshotTaken += ScreenshotTaken;
		ScreenshotManager.OnScreenshotSaved += ScreenshotSaved;	
	}
	
	void OnDisable ()
	{
		ScreenshotManager.OnScreenshotTaken -= ScreenshotTaken;
		ScreenshotManager.OnScreenshotSaved -= ScreenshotSaved;	
	}
	
	void ScreenshotTaken(Texture2D image)
	{
//		console.text += "\nScreenshot has been taken and is now saving...";
//		screenshot.sprite = Sprite.Create(image, new Rect(0, 0, image.width, image.height), new Vector2(.5f, .5f));
//		screenshot.color = Color.white;
//		ui.alpha = 1;
	}
	
	void ScreenshotSaved(string path)
	{
//		console.text += "\nScreenshot finished saving to " + path;
	}
	
	private IEnumerator TakeScreenshot()
	{
		yield return new WaitForEndOfFrame();
		
#if UNITY_ANDROID || UNITY_IPHONE
		string filepath = "" + screenshotTitle.GetComponent<Text>().text;
		ScreenshotManager.SaveScreenshot(filepath, "GoalMapping", "png");
#else
		
		Texture2D screenGrab = new Texture2D(Screen.width, Screen.height);
		screenGrab.ReadPixels (new Rect(0, 0, Screen.width, Screen.height), 0, 0);
		screenGrab.Apply ();
		
		screenshot = screenGrab.EncodeToPNG ();
#endif
	}
	
	private void OpenEmailer(GameObject popupParent)
	{
#if !UNITY_ANDROID && !UNITY_IPHONE
		if(popupParent != null)
		{
			Destroy (popupParent);
		}
		
		gameObject.AddComponent<Mailer>();
		gameObject.GetComponent<Mailer>().sceneCanvas = localCanvas;
		gameObject.GetComponent<Mailer>().screenshot = screenshot;
#endif
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update ()
	{
		if(screenSize.width == 0 || screenSize.width != Screen.width || screenSize.height == 0 || screenSize.height != Screen.height)
		{
			screenSize = new Rect(0, 0, Screen.width, Screen.height);
		}
		
		Rect imgRect = new Rect(Screen.width * 0.15f, Screen.height * 0.025f, Screen.width * 0.7f, Screen.height * 0.85f);
		
		// If the goal map is loaded, then stop showing the loading popup.
		if(loaded)
		{
			loadingBackground.SetActive (false);
		}
		// Otherwise, make the loading spinner spin.
		else
		{
			spinnerRot -= Time.deltaTime * 75f;
			loadingSpinner.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, spinnerRot);
			loadingProgress.GetComponent<Text>().text = ((int)progress).ToString () + "%";
			
			// Allow access when not logged in.
			if(!MCP.viewingFriendGM && MCP.userInfo == null)
			{
				loaded = true;
			}
		}
		
		// If both are showing...
		if(showBoth)
		{
			// Enable text.
			goalMapObject.transform.GetChild (0).gameObject.SetActive (true);
			// Enable pictures.
			goalMapObject.transform.GetChild (1).gameObject.SetActive (true);
			
			// Move both trees to the side
			goalMapObject.transform.localPosition = showBothPos;
			// Move the image tree to the right
			goalMapObject.transform.GetChild (1).localPosition = imageTreePos;
			
			// Fully zoom out.
			goalMapObject.transform.localScale = new Vector3(0.4f, 0.4f, 1);
		}
		else
		{
#if UNITY_ANDROID || UNITY_IPHONE
			// If there are 2 or more touches...
			if(Input.touches.Length > 1)
			{
				// Process zooming the goal map.
				ZoomGoalMap (Input.touches, imgRect);
			}
			// Otherwise, if there is a touch...
			else if(Input.touches.Length > 0)
			{
				// Process moving the goal map.
				MoveGoalMap (Input.touches[0].position, Input.touches[0].phase == TouchPhase.Began ? true : false, true, Input.touches[0].phase == TouchPhase.Ended ? true : false, imgRect);
			}
#else
			// Process moving the goal map.
			MoveGoalMap (Input.mousePosition, Input.GetMouseButtonDown (0), Input.GetMouseButton (0), Input.GetMouseButtonUp (0), imgRect);
			// Process zooming the goal map.
			ZoomGoalMap (Input.mousePosition, Input.GetMouseButtonDown (1), Input.GetMouseButton (1), Input.GetMouseButtonUp (1), imgRect);
			ScrollZoomCanvas(Input.GetAxis ("Mouse ScrollWheel"));
#endif
		}
		
		// Set the screenshot name to the input field.
		screenshotFileName = screenshotTitle.GetComponent<Text>().text;
		
		// If the screenshot has been flagged as ready...
		if(readyToScreenshot)
		{
			// Update the screenshot timer.
			screenshotTimer += Time.deltaTime;
			
			// If the screenshot hasn't been taken and the scene has had the time to hide all objects...
			if(!screenshotTaken && screenshotTimer > 0.5f)
			{	
				// Take the screenshot.
				StartCoroutine(TakeScreenshot());
//				Application.CaptureScreenshot (screenshotFileName + ".png");
				screenshotTaken = true;
			}
				
			// After enough time has passed to take the screenshot...
			if(screenshotTimer > 1f)
			{
				// Make a popup to confirm screenshot.
#if UNITY_ANDROID || UNITY_IPHONE
				MakePopup (localCanvas, defaultPopUpWindow, MCP.Text(2502)/*"Screenshot Taken"*/, MCP.Text(2503)/*"Screenshot saved to Gallery"*/, MCP.Text(205)/*"Dismiss"*/, CommonTasks.DestroyParent);
#else
				GameObject popupObj = MakePopup (localCanvas, defaultPopUpWindow, MCP.Text(2502)/*"Screenshot Taken"*/, MCP.Text(2512)/*"Would you like to send this screenshot as an email?"*/, MCP.Text(202)/*"No"*/, CommonTasks.DestroyParent);
				GameObject popupNoButton = popupObj.transform.GetChild (2).gameObject;
				// Resize the no button.
				popupNoButton.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width * 0.15f, popupNoButton.GetComponent<RectTransform>().sizeDelta.y);
				popupNoButton.transform.GetChild (0).gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width * 0.15f, popupNoButton.GetComponent<RectTransform>().sizeDelta.y);
				// Move the no button.
				popupNoButton.GetComponent<RectTransform>().localPosition = new Vector2(Screen.width * 0.5f * 0.1f, popupNoButton.GetComponent<RectTransform>().localPosition.y);
				
				List<MyVoidFunc> funcList = new List<MyVoidFunc>
				{
					delegate
					{
						OpenEmailer(popupObj);
					}
				};
				GameObject popupYesButton = MakeButton (localCanvas, MCP.Text (201)/*Yes*/, buttonBackground, new Rect(Screen.width * 0.04f, Screen.height * 0.15f, Screen.width * 0.15f, Screen.height * 0.05f), funcList);
				popupYesButton.name = "PopupYesButton";
				popupYesButton.transform.SetParent (popupObj.transform, true);
				popupYesButton.transform.localScale = Vector3.one;
#endif
				
				// Reactivate the objects that are hidden for the screenshot.
				foreach (GameObject go in hideForScreenshot)
				{
					go.SetActive (true);
				}
				
				// Reset flags
				screenshotTaken = false;
				readyToScreenshot = false;
				screenshotTimer = 0f;
			}
		}
		
//#if UNITY_WEBPLAYER
		// If the goal maps have been pulled from the server...
		if(MCP.goalMaps != null && MCP.pulledGoalMaps && !fileBrowserOpen)
		{
			// If there is more than one goal map and one of them has been selected...
			if(MCP.goalMaps.Count > 0 && !loading && MCP.gmIndex != -1)
			{
				goalMap = MCP.goalMaps[MCP.gmIndex];
				LoadGoalMap(goalMap);
				loading = true;
			}
			else
			{
				if(!fileBrowserOpen)
				{
					loading = true;
				}
			}
		}
//#endif
		
		// If the goal mapping is loading, load the images from the server.
		if(loading && !fileBrowserOpen)
		{
			bool emptyMap = true;
			
			if(goalMap.goals != null)
			{
				emptyMap = false;
				
				for(int j = 0; j < goalMap.goals.Count; j++)
				{
					if(goalMap.goals[j].imageRef != null)
					{
						if(goalMap.goals[j].image == null)
						{
							if(!MCP.isTransmitting)
							{
								if(MCP.pulledImages != null && MCP.pulledImages.Count > 0 && MCP.pulledImages[0] != null)
								{
									goalMap.goals[j].image = MCP.pulledImages[0];
								
									if(goalMap.goals[j].image.width < 16)
									{
										goalMap.goals[j].image = defaultSprite.texture;	
									}
									
									MCP.pulledImages[0] = null;
									progress = j * 7;
								}
								else
								{
									MCP.pulledImages.Clear ();
									StartCoroutine(MCP.GetImage (MCP.currentGoalMapUser, goalMap.goals[j].imageRef));
								}
							}
						}
					}
				}
			}
			
			if(goalMap.whys != null)
			{	
				emptyMap = false;
				
				for(int j = 0; j < goalMap.whys.Count; j++)
				{
					if(goalMap.whys[j].imageRef != null)
					{
						if(!MCP.isTransmitting)
						{
							if(goalMap.whys[j].image == null)
							{
								if(MCP.pulledImages != null && MCP.pulledImages.Count > 0 && MCP.pulledImages[0] != null)
								{
									goalMap.whys[j].image = MCP.pulledImages[0];
								
									if(goalMap.whys[j].image.width < 16)
									{
										goalMap.whys[j].image = defaultSprite.texture;	
									}
									
									MCP.pulledImages[0] = null;
									progress = 35 + (j * 7);
								}
								else
								{
									MCP.pulledImages.Clear ();
									StartCoroutine(MCP.GetImage (MCP.currentGoalMapUser, goalMap.whys[j].imageRef));
								}
							}
						}
					}
				}
			}
			
			if(goalMap.whos != null)
			{	
				emptyMap = false;
				
				for(int j = 0; j < goalMap.whos.Count; j++)
				{
					if(goalMap.whos[j].imageRef != null)
					{
						if(!MCP.isTransmitting)
						{
							if(goalMap.whos[j].image == null)
							{
								if(MCP.pulledImages != null && MCP.pulledImages.Count > 0 && MCP.pulledImages[0] != null)
								{
									goalMap.whos[j].image = MCP.pulledImages[0];
								
									if(goalMap.whos[j].image.width < 16)
									{
										goalMap.whos[j].image = defaultSprite.texture;	
									}
									
									MCP.pulledImages[0] = null;
									progress = 56 + (j * 7);
								}
								else
								{
									MCP.pulledImages.Clear ();
									StartCoroutine(MCP.GetImage (MCP.currentGoalMapUser, goalMap.whos[j].imageRef));
								}
							}
						}
					}
				}
			}
			
			if(goalMap.hows != null)
			{	
				emptyMap = false;
				
				for(int j = 0; j < goalMap.hows.Count; j++)
				{
					if(goalMap.hows[j].imageRef != null)
					{
						if(!MCP.isTransmitting)
						{
							if(goalMap.hows[j].image == null)
							{
								if(MCP.pulledImages != null && MCP.pulledImages.Count > 0 && MCP.pulledImages[0] != null)
								{
									goalMap.hows[j].image = MCP.pulledImages[0];
									
									if(goalMap.hows[j].image.width < 16)
									{
										goalMap.hows[j].image = defaultSprite.texture;	
									}
									
									MCP.pulledImages[0] = null;
									progress = 78 + (j * 7);
								}
								else
								{
									MCP.pulledImages.Clear ();
									StartCoroutine(MCP.GetImage (MCP.currentGoalMapUser, goalMap.hows[j].imageRef));
								}
							}
						}
					}
				}
				
				if(!MCP.isTransmitting)
				{
					loading = false;
					loaded = true;
					goalMapObject.GetComponent<PopGM_Prefab>().PopulateGoalMap(goalMap);
					MCP.pulledGoalMaps = false;
				}
			}
			
			if(emptyMap)
			{
				if(!MCP.isTransmitting)
				{
					loading = false;
					loaded = true;
					goalMapObject.GetComponent<PopGM_Prefab>().PopulateGoalMap(goalMap);
					MCP.pulledGoalMaps = false;
				}
			}
		}
	}
	
	/// <summary>
	/// Toggles showing both text and image goal maps.
	/// </summary>
	private void ToggleShowBoth()
	{
		// If the goal map is rottated, rotate it back.
		if(gmRotated)
		{
			RotateGoalMap();
		}
		
		showBoth = !showBoth;
		
		if(showBoth)
		{
			// Enable/disable text.
			goalMapObject.transform.GetChild (0).gameObject.SetActive (true);
			// Disable/enable pictures.
			goalMapObject.transform.GetChild (1).gameObject.SetActive (true);
			
			// Show the highlighted sprite while this is selected
			UnityEngine.UI.Navigation nav = new UnityEngine.UI.Navigation();
			nav.mode = UnityEngine.UI.Navigation.Mode.None;
			
			toggleShowingBothTreesButton.GetComponent<Button>().navigation = nav;
			toggleShowingBothTreesButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
			
			SpriteState spriteState = new SpriteState();
			toggleShowingBothTreesButton.transform.GetChild (1).gameObject.GetComponent<Image>().sprite = showOneOrTwoSprite[1];
			toggleShowingBothTreesButton.GetComponent<Button>().targetGraphic = toggleShowingBothTreesButton.transform.GetChild (1).gameObject.GetComponent<Image>();
			spriteState.highlightedSprite = showOneOrTwoSprite[1];
			spriteState.pressedSprite = showOneOrTwoSprite[0];
			toggleShowingBothTreesButton.GetComponent<Button>().spriteState = spriteState;
			
			// Set text/pic label to text.
			changeTextPicGoalMapButton.transform.GetChild (1).gameObject.GetComponent<Image>().sprite = changeTextPicSprite[0];
			
			// Make hover sprite
			changeTextPicGoalMapButton.GetComponent<Button>().navigation = nav;
			changeTextPicGoalMapButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
			
			spriteState = new SpriteState();
			changeTextPicGoalMapButton.GetComponent<Button>().targetGraphic = changeTextPicGoalMapButton.transform.GetChild (1).gameObject.GetComponent<Image>();
			spriteState.highlightedSprite = changeTextPicSprite[1];
			spriteState.pressedSprite = changeTextPicSprite[1];
			changeTextPicGoalMapButton.GetComponent<Button>().spriteState = spriteState;
		}
		else
		{
			// Reverse the moves
			Vector3 newPos = goalMapObject.transform.GetChild (0).localPosition;
			newPos.x = 0;
			goalMapObject.transform.GetChild (0).localPosition = newPos;
			
			newPos = goalMapObject.transform.localPosition;
			newPos.x = screenSize.width * 0.35f;
			goalMapObject.transform.localPosition = newPos;
			
			// Reset zoom;
			ZoomOut();
			
			// Enable/disable text.
			goalMapObject.transform.GetChild (0).gameObject.SetActive (true);
			// Disable/enable pictures.
			goalMapObject.transform.GetChild (1).gameObject.SetActive (false);
			
			// Show the highlighted sprite while this is selected
			UnityEngine.UI.Navigation nav = new UnityEngine.UI.Navigation();
			nav.mode = UnityEngine.UI.Navigation.Mode.None;
			
			toggleShowingBothTreesButton.GetComponent<Button>().navigation = nav;
			toggleShowingBothTreesButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
			
			SpriteState spriteState = new SpriteState();
			toggleShowingBothTreesButton.transform.GetChild (1).gameObject.GetComponent<Image>().sprite = showOneOrTwoSprite[0];
			toggleShowingBothTreesButton.GetComponent<Button>().targetGraphic = toggleShowingBothTreesButton.transform.GetChild (1).gameObject.GetComponent<Image>();
			spriteState.highlightedSprite = showOneOrTwoSprite[1];
			spriteState.pressedSprite = showOneOrTwoSprite[1];
			toggleShowingBothTreesButton.GetComponent<Button>().spriteState = spriteState;
		}
	}
	
	private void CycleBackgroundImage()
	{
		currentBackgroundIndex++;
		
		if(currentBackgroundIndex > backgroundTextures.Count - 1)
		{
			currentBackgroundIndex = 0;
		}
		
		// Make this the loaded texture.
		GameObject.Find ("BackgroundCamera").GetComponent<BackgroundPlane>().SetBackgroundTexture(backgroundTextures[currentBackgroundIndex]);
	}
	
	/// <summary>
	/// Opens the background file.
	/// </summary>
	private void OpenBackgroundFile()
	{
		openingBackground = true;
		// Reset the first frame timer.
		MenuScreen.firstFrameTimer = 0;
		
#if UNITY_WEBPLAYER
		// Add the picture gallery script to the camera and sets the variables.
		Camera.main.gameObject.AddComponent<PictureGallery>().sceneCanvas = localCanvas;
#else
		// Add the file browser script to the camera and set the variables.
		Camera.main.gameObject.AddComponent<FileBrowser>().sceneCanvas = localCanvas;
		Camera.main.gameObject.GetComponent<FileBrowser>().currentFilePath = Application.persistentDataPath;
		Camera.main.gameObject.GetComponent<FileBrowser>().upPathLimit = Application.persistentDataPath;
		Camera.main.gameObject.GetComponent<FileBrowser> ().allowedExtensions = new string[]{"png", "jpg"};
#endif
	}

	public void OnDestroy()
	{
		// Reset the goal map index.
		MCP.gmIndex = -1;
		// Reset the background
		if(GameObject.Find ("BackgroundCamera") != null)
		{
			GameObject.Find ("BackgroundCamera").GetComponent<BackgroundPlane>().SetBackgroundTexture(mainBGText);
		}
	}
	
	public override void DoGUI() {}
}
