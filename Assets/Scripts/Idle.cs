﻿using UnityEngine;
using System.Collections;
using System;

public class Idle : MenuScreen
{
	private bool focusRegained = false;
	private bool tripPopUp = false;
	private bool showDays = false, showHours = false;
	private float focusedIdleTime = 0f;
	private DateTime timeLostFocus;
	private DateTime prevClickTime;
	private bool prevFocus = true;
	
	// 1 hour = 60 minutes * 60 seconds.
	private const float ALLOWED_IDLE_TIME = 3600f;
	//private const float ALLOWED_IDLE_TIME = 5f;

	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		// Initialize the base class.
		Init ();
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update ()
	{
		// Only update when the user is logged in.
		if(MCP.loginSuccess)
		{
			// Add to the idle timer.
			focusedIdleTime += Time.deltaTime;
			
#if UNITY_IPHONE || UNITY_ANDROID
			// If there are touches, then reset the timers.
			if(Input.touchCount > 0)
			{
				focusedIdleTime = 0f;
				prevClickTime = DateTime.UtcNow;
			}
#else
			// If the mouse has been clicked, then reset the timers.
			if(Input.GetMouseButtonDown (0))
			{
				focusedIdleTime = 0f;
				prevClickTime = DateTime.UtcNow;
			}
#endif
			// If the idle timer goes over the allowed amount, trip the popup.
			if(focusedIdleTime > ALLOWED_IDLE_TIME)
			{
				tripPopUp = true;
			}
		}
	}
	
	/// <summary>
	/// Check whether an hour has passed since this program was focused.
	/// </summary>
	/// <returns><c>true</c>, if hour was passed, <c>false</c> otherwise.</returns>
	private bool HourPassed()
	{
		// Change to a timespan to make sure an hour has passed.
		TimeSpan timePassed = new TimeSpan(DateTime.UtcNow.Ticks - timeLostFocus.Ticks);
		
		// If the time since going inactive is over an hour, return true.
		if(timePassed.Hours > 1)
		{
			return true;
		}
		
		// If the time since going inactive is over a day, return true.
		if(timePassed.Days > 0)
		{
			return true;
		}
		
		return false;
	}
	
	/// <summary>
	/// Checks whether the day has changed since the program lost focus.
	/// </summary>
	/// <returns><c>true</c>, if the day was different, <c>false</c> otherwise.</returns>
	private bool DifferentDay()
	{
		int days;
		
		
		if(tripPopUp)
		{
			days = DateTime.UtcNow.Day - prevClickTime.Day;
		}
		else
		{
			days = DateTime.UtcNow.Day - timeLostFocus.Day;
		}
		
		// If one or more days have passed, return true.
		if(days != 0)
		{
			return true;
		}
		
		return false;
	}
	
	/// <summary>
	/// Gets the number of hours passed.
	/// </summary>
	/// <returns>The number of hours.</returns>
	private int GetNumberOfHours()
	{
		// Get the time since the program lost focus.
		TimeSpan timePassed = new TimeSpan(DateTime.UtcNow.Ticks - timeLostFocus.Ticks);
		// Get the hours from this time.
		int hours = timePassed.Hours;
		// Return them.
		return hours;
	}
	
	/// <summary>
	/// Gets the number of days passed.
	/// </summary>
	/// <returns>The number of days.</returns>
	private int GetNumberOfDays()
	{
		// Get the time since the program lost focus.
		TimeSpan timePassed = new TimeSpan(DateTime.UtcNow.Ticks - timeLostFocus.Ticks);
		// Get the days from this time.
		int days = timePassed.Days;
		// Return them.
		return days;
	}
	
	/// <summary>
	/// Gets the inactive text.
	/// </summary>
	/// <returns>The inactive text.</returns>
	private string GetInactiveText()
	{
		string text = "You have been inactive for ";
		string hoursText = "", daysText = "";
		
		// Get the number of days the user has been inactive.
		int numberOfDays = GetNumberOfDays();
		// If they have been inactive for more than a day, add the day value to the string.
		if(showDays || numberOfDays > 0)
		{
			daysText += numberOfDays.ToString () + " days and ";
			
			// Force hours to show if there is more than a day idle.
			showHours = true;
		}
		
		// Get the number of hours the user has been inactive.
		int numberOfHours = GetNumberOfHours();
		// If they have been inactive for more than an hour, add the hour value to the string.
		if(showHours || numberOfHours > 0)
		{
			hoursText += GetNumberOfHours().ToString () + " hours.";
		}
		
		// Return the popup text
		return text + daysText + hoursText;
	}
	
	/// <summary>
	/// Displays the inactive pop up.
	/// </summary>
	private void DisplayInactivePopUp()
	{
		/*string inactiveText;
		if(tripPopUp)
		{
			inactiveText = "You have been logged out for being idle.";
		}
		else
		{
			inactiveText = GetInactiveText ();
		}
		
		// Display the time the user has been away.
		if(DoPopUpBox ("Inactive", inactiveText, PopUpType.largeNotification))
		{
			focusRegained = false;
			focusedIdleTime = 0f;
			tripPopUp = false;
			
			MCP.loginSuccess = false;
			
			if (sndManager != null) {
				Destroy (sndManager);
				//sndManager.SetMusicEnabled( false );
				//sndManager.SetSoundEnabled( false );
			}
			
			LoadMenu ("Login_Screen");
		}*/
	}
	
	/// <summary>
	/// Do the legacy GUI.
	/// </summary>
	public override void DoGUI()
	{
		// Make sure the idle script does not overwrite the cursor switch.
		comingFromIdle = true;
		
		// If focus has just been regained...
		if(focusRegained)
		{
			// If more than an hour has passed...
			if(HourPassed ())
			{
				// Display the inactive popup.
				DisplayInactivePopUp ();
				
				// If it is a different day, then don't let the day trip.
				if(DifferentDay())
				{
					MCP.hasDayTripped = false;
				}
			}
		}
		
		// If enough time has passed to trip the popup...
		if(tripPopUp)
		{
			// Display the inactive popup.
			DisplayInactivePopUp ();
			
			// If it is a different day, then don't let the day trip.
			if(DifferentDay())
			{
				MCP.hasDayTripped = false;
			}
		}
	}
	
	/// <summary>
	/// Raises the application focus event.
	/// </summary>
	/// <param name="isFocus">If set to <c>true</c> is focus.</param>
	public void OnApplicationFocus(bool isFocus)
	{
		// Only check if the user is logged in.
		if(MCP.loginSuccess)
		{
			// If the focus status has changed...
			if(isFocus != prevFocus)
			{
				// If the focus has just become active...
				if (isFocus)
				{
					focusRegained = true;
					
					prevFocus = isFocus;
				}
				// Otherwise...
				else
				{
					// Log the time focus is lost.
					timeLostFocus = DateTime.UtcNow;
					
					prevFocus = isFocus;
				}
			}
		}
	}
}
