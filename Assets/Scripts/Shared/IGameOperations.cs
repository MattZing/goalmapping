using System;

namespace Games.Shared
{
		public interface IGameOperations
		{
			// confirm window trigger ?
			void Restart();
			void Pause();
			void Resume();
			// if needs to submit score
			void Quit();
			
			void ShiftContentLeft();
			void ShiftContentRight();
		}
}

