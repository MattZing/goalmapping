﻿using UnityEngine;
using System.Collections;

namespace Games.Shared {
	public class PanelButton : MonoBehaviour {
		
		private GameObject button_menu = null;
		private GameObject button_bg = null;
		
		// proper alignment, better than Start
		void Awake() {
			if (button_bg == null) {
				button_bg = GameObject.CreatePrimitive(PrimitiveType.Quad);
				button_bg.GetComponent<Renderer>().material.mainTexture = Resources.Load<Texture2D>("Shared/Textures/header_bg");
				button_bg.GetComponent<Renderer>().material.shader = Shader.Find("Sprites/Default");
				button_bg.GetComponent<Renderer>().material.color = new Color(1f,1f,1f,0.81f);
				button_bg.transform.parent = this.transform;
				button_bg.GetComponent<Collider>().enabled = false;
			}
			
			if (button_menu == null) {
				button_menu = GameObject.CreatePrimitive(PrimitiveType.Quad);
				button_menu.GetComponent<Renderer>().material.mainTexture = Resources.Load<Texture2D>("Shared/Textures/menuicon_normal");
				button_menu.GetComponent<Renderer>().material.shader = Shader.Find("Sprites/Default");
				button_menu.GetComponent<Renderer>().material.color = new Color(1f,1f,1f,1f);
				button_menu.transform.position = new Vector3(0f,0f,-1f);
				button_menu.transform.parent = this.transform;
				button_menu.GetComponent<Collider>().enabled = false;
			}
		}
		
		// Use this for initialization
		void Start () {
			
		}
		
		// Update is called once per frame
		void Update () {
			
		}
	}
}