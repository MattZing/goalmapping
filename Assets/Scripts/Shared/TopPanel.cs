﻿using UnityEngine;
using System.Collections;

namespace Games.Shared {
	public class TopPanel : MonoBehaviour {
		private GameObject topBarQuad;
		
		private GameObject quad_level;
		private GameObject quad_scores;
		
		public bool last_state;
		
		private GameObject button_menu;
		
		// Use this for initialization
		void Start () {
			topBarQuad = GameObject.CreatePrimitive(PrimitiveType.Quad);
			
			string path = "Shared/Textures/header_bg";
			
			topBarQuad.GetComponent<Renderer>().material.mainTexture = Resources.Load<Texture2D>(path);
			topBarQuad.GetComponent<Renderer>().material.shader = Shader.Find ("Sprites/Default");
			
			topBarQuad.transform.localScale = new Vector3(16.5f,1f,1f);
			topBarQuad.GetComponent<Renderer>().material.color = new Color(1f,1f,1f,0.75f);
			topBarQuad.transform.parent = this.transform;
			topBarQuad.name = "QuadBgPanel";
			
			quad_level = TextMeshFactory("Exo-MediumItalic","quad_level_text");
			quad_scores = TextMeshFactory("Exo-Medium","quad_scores_text");
			
			quad_level.transform.localPosition = new Vector3(-8.5f,4.75f,-1f);
			quad_scores.transform.localPosition = new Vector3(5f,0.28f,-1f);
			quad_scores.transform.parent = this.transform;
			
			button_menu = GameObject.CreatePrimitive(PrimitiveType.Quad);
			button_menu.AddComponent<PanelButton>();
			button_menu.name = "ButtonMenuTopRight";
			button_menu.GetComponent<Renderer>().material.mainTexture = Resources.Load<Texture2D>("Shared/Textures/header_bg");
			button_menu.GetComponent<Renderer>().material.shader = Shader.Find("Sprites/Default");
			
			button_menu.transform.localPosition = new Vector3(9.15f,0f,-1f);
			button_menu.transform.parent = this.transform;
			MeshCollider collider = button_menu.GetComponent<MeshCollider>() as MeshCollider;
			collider.enabled = true;
			
			InsertTextMesh(quad_level, "Level 3/12");
			InsertTextMesh(quad_scores, "Scores : 000000");
			
			SetActive(last_state);
		}
		
		// Update is called once per frame
		void Update () {
			
		}
		
		void OnGUI() {
			// GUI.Label (new Rect (Screen.width-100, Screen.height-30, 100,30 ), "Scores : "+scores, new GUIStyle());
			// GUI.Label (new Rect (Screen.width-100, Screen.height-30, 100,30 ), "Scores : "+scores, new GUIStyle());
		}
		
		public void UpdateScores (int s) {
			string scores = PrettyScores(s); 
			InsertTextMesh(quad_scores, "Scores: "+scores);
		}
		
		public void UpdateLevel(int level_num, int total_levels) {
			InsertTextMesh(quad_level, "Level "+level_num.ToString()+" / "+total_levels);
		}
		
		private string PrettyScores(int scores) {
			string pretty_scores = scores.ToString();
			
			for (int i=0;i<6-pretty_scores.Length;i++) {
				pretty_scores = "0"+pretty_scores;
			}
			
			return pretty_scores;
		}
		
		private GameObject TextMeshFactory(string font, string name) {
			GameObject text = GameObject.CreatePrimitive(PrimitiveType.Quad);
			text.name = name;
			TextMesh textMesh = text.AddComponent<TextMesh>() as TextMesh;
			MeshRenderer renderer = text.GetComponent<MeshRenderer>();
			
			renderer.material = Resources.Load<Material>("Shared/Fonts/"+font);
			textMesh.text = "00000000";
			Font exo_medium = Resources.Load<Font>("Shared/Fonts/"+font);
			textMesh.font = exo_medium;
			textMesh.fontSize = 35;
			text.transform.localScale = new Vector3(0.11f,0.11f,0f);
			return text;
		}
		
		private void InsertTextMesh(GameObject text, string val) {
			TextMesh textmesh = text.GetComponent<TextMesh>() as TextMesh;
			textmesh.text = val;
		}
		
		public void SetActive(bool b) {
			topBarQuad.SetActive(b);
			quad_level.SetActive(b);
			quad_scores.SetActive(b);
			button_menu.SetActive(b);
		}
	}
}