using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using LitJson;

/**
*  FAQ_Screen
* 
*  Unity GUI script to display frequently asked questions
**/
public class Glossary_Screen : MenuScreen
{
	private GameObject localCanvas;
	private GameObject glossaryTextObject;
	private GameObject glossaryScrollArea;
	
	/** Private variables **/
	private Rect screenSize;
	private Rect leftPanelRect;
	private Rect rightPanelRect;
	
	private float logoutButtonScale;
	private Touch touch;
	private float bottomPanelSpacer;
	private float scrollVelocity;
	public Glossary_Entries glossary_entries;
	private List<Glossary_Entry> search_entries;
	private GUIStyle textStyle_Head;
	private GUIStyle textStyle;
	private string searchString = "";
	public TextAsset jsonFile;

	public Glossary_Entry entries;

	public class Glossary_Entries
	{
		public Glossary_Entry[] entries;
	}

	public class Glossary_Entry : System.IComparable
	{
		public string title;
		public string description;
		
		public int CompareTo (object obj)
		{
			Glossary_Entry toCompare = obj as Glossary_Entry;
			
			if ( toCompare != null )
			{
				return toCompare.CompareTo( title );
			}
				
			return 0;
		}
	}

	/** Internal states **/
	private enum InternalState
	{
		letter,
		search
	}
	
	// All cannot be displayed on the new GUI due to too many characters
	private enum LetterState
	{
		A,
		B,
		C,
		D,
		E,
		F,
		G,
		H,
		I,
		J,
		K,
		L,
		M,
		N,
		O,
		P,
		Q,
		R,
		S,
		T,
		U,
		V,
		W,
		X,
		Y,
		Z
	};

	private LetterState letterState;
	
	private string seconds;
	private string minutes;
	
	private string interval_seconds;
	private string interval_minutes;

	// Use this for initialization
	void Start ()
	{
		// Initialize the base class.
		base.Init ();

		// Initialize lists.
		search_entries = new List<Glossary_Entry> ();
		
		// Set the start state.
		letterState = LetterState.A;
		PickLetterSearchEntries(letterState);
		
		// Set variable defaults.
		glossary_entries = JsonMapper.ToObject<Glossary_Entries>(jsonFile.text);
		
		textStyle_Head = new GUIStyle();
		textStyle_Head.alignment = TextAnchor.UpperLeft;
		textStyle_Head.font = defaultFont;
		textStyle_Head.fontSize = (int)_fontSize_Large;
		textStyle_Head.wordWrap = true;
		
		textStyle = new GUIStyle();
		textStyle.alignment = TextAnchor.UpperLeft;
		textStyle.font = defaultFont;
		textStyle.fontSize = (int)_fontSize;
		textStyle.wordWrap = true;
		
		// Create the GUI.
		CreateGUI();
		
		localCanvas.transform.localScale = Vector2.zero;
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Make canvas
		localCanvas = MakeCanvas ();
		
		// Make title bar
		MakeTitleBar (localCanvas, MCP.Text (2201)/*"Glossary"*/, "ScreenTitle");
		
		// Make side gizmo
		MakeSideGizmo (localCanvas, true, true);
		
		// Make background panel
		MakeImage (localCanvas, new Rect(Screen.width * 0.1f, Screen.height * 0.15f, Screen.width * 0.8f, Screen.height * 0.8f), window_back, true).name = "BackgroundPanel";
		
		// Make search area label
		MakeLabel (localCanvas, MCP.Text (2202)/*"Search: "*/, new Rect(Screen.width * 0.125f, Screen.height * 0.17f, Screen.width * 0.225f, Screen.height * 0.06f), TextAnchor.MiddleLeft, "SmallText").name = "SearchAreaLabel";
		
		// Make search area input field
		GameObject searchAreaInput = MakeInputField (localCanvas, searchString, new Rect(Screen.width * 0.112f, Screen.height * 0.225f, Screen.width * 0.23f, Screen.height * 0.08f), false, menuBackground);
		searchAreaInput.name = "SearchAreaInput";
		searchAreaInput.GetComponent<Text>().color = bmOrange;
		
		// Make search button
		GameObject searchButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.1275f, Screen.height * 0.32f, Screen.width * 0.2f, Screen.height * 0.08f), buttonBackground, true);
		searchButtonBackground.name = "SearchButtonBackground";
		
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			delegate
			{
				// Clear the current search entries.
				search_entries.Clear();
				// Force the search input to lower to make the searching easier.
				string lowerSearch = searchAreaInput.GetComponent<Text>().text.ToLower ();
				
				// If the search field is not empty...
				if(lowerSearch != "")
				{
					// For each entry in the glossary...
					foreach (Glossary_Entry entry in glossary_entries.entries) 
					{
						// Get the entry string in ower case for easier searching.
						string entryLower = entry.title.ToLower ();
						
						// If the entry title contains the search term, then add it to the results list.
						if (entryLower.Contains (lowerSearch)) 
						{
							search_entries.Add (entry);
							continue;
						}
						
						// If the entry description contains the search term, then add it to the results list.
						entryLower = entry.description.ToLower ();
						
						if (entryLower.Contains (lowerSearch)) 
						{
							search_entries.Add (entry);
						}
					}
					
					// Set the viewable text to show the found terms.
					SetText(search_entries);
				}
			}
		};
		GameObject searchButton = MakeButton (localCanvas, MCP.Text (221)/*"Search"*/, new Rect(Screen.width * 0.1275f, Screen.height * 0.32f, Screen.width * 0.2f, Screen.height * 0.08f), funcList, "InnerLabel");
		searchButton.name = "SearchButton";
		searchButton.GetComponent<Button>().image = searchButtonBackground.GetComponent<Image>();
		searchButton.GetComponent<Text>().color = bmOrange;
		
		// Make search letter buttons
		MakeLetterButtons ();
		
		// Make glossary text scroll area
		glossaryScrollArea = MakeScrollRect (localCanvas, new Rect(Screen.width * 0.365f, Screen.height * 0.125f, Screen.width * 0.5f, Screen.height * 0.75f), null);
		glossaryScrollArea.name = "GlossaryScrollArea";
		
		// Make glossary text
		glossaryTextObject = MakeLabel (localCanvas, MCP.Text (208)/*"Coming Soon"*/, new Rect(Screen.width * 0.375f, Screen.height * 0.1885f, Screen.width * 0.48f, Screen.height * 0.7f), TextAnchor.UpperLeft);
		glossaryTextObject.name = "GlossaryTextObject";
		// Set the glossary object attributes.
		glossaryTextObject.transform.SetParent (glossaryScrollArea.transform.GetChild(0), true);
		glossaryTextObject.transform.localScale = Vector3.one;
		glossaryTextObject.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
		glossaryTextObject.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
		glossaryTextObject.GetComponent<Text>().resizeTextForBestFit = false;
		glossaryTextObject.GetComponent<Text>().fontSize = (int)_fontSize;
		glossaryScrollArea.GetComponent<ScrollRect>().horizontal = false;
		glossaryScrollArea.GetComponent<ScrollRect>().content = glossaryTextObject.GetComponent<RectTransform>();
		
		// Set the viewable text to show the default terms.
		SetText(search_entries);
	}
	
	// Update is called once per frame
	void Update()
	{
		// Update the base screen.
		base.UpdateScreen ();
		// Handle the touch scrolling.
		base.HandleTouch ();
	}
	
	/// <summary>
	/// Picks the search entries by letter.
	/// </summary>
	/// <param name="letter">Letter.</param>
	void PickLetterSearchEntries( LetterState letter )
	{
		// Clear the current search entries.
		search_entries.Clear ();
		
		// For each glossary entry...
		foreach( Glossary_Entry entry in glossary_entries.entries )
		{
			// If the first letter of the entry title is the same as the selected letter...
			if (entry.title.ToUpper() [0] == letter.ToString().ToUpper ()[0]) 		
			{
				// Add it to the results list.
				search_entries.Add ( entry );
			}
		}
		
		// Sort the list alphabetically.
		search_entries.Sort();
	}
	
	/// <summary>
	/// Sets the text.
	/// </summary>
	/// <param name="glossaryEntry">Glossary entries.</param>
	void SetText(List<Glossary_Entry> glossaryEntry)
	{
		// Start the glossary text with a space for readability.
		string glossaryText = "\n";
		
		// Add each entry to the glossary text with breaks for readability.
		foreach (Glossary_Entry entry in glossaryEntry)
		{
			glossaryText += "<b>" + entry.title + "</b>" + "\n\n" + entry.description + "\n\n";
		}
		
		// If there have been no entries added, display No entries found.
		if(glossaryText == "" || glossaryText == "\n")
		{
			glossaryText = MCP.Text (2203)/*"No entries found for "*/ + letterState.ToString ();
		}
		
		// Set the text object to the glossary text.
		glossaryTextObject.GetComponent<Text>().text = glossaryText;
		
		// Work out the height of the glossary text.
		float height = textStyle.CalcHeight (new GUIContent(glossaryText), Screen.width * 0.48f);
		if(height < Screen.height * 0.7f)
		{
			height = Screen.height * 0.7f;
		}
		// Make the text object big enough to contain all the text.
		glossaryTextObject.GetComponent<RectTransform>().sizeDelta = new Vector2(glossaryTextObject.GetComponent<RectTransform>().sizeDelta.x, height);
		
		// Set content position to start scrolled to the top.
		Vector3 newPos = glossaryTextObject.GetComponent<RectTransform>().localPosition;
		newPos.y = -height;
		glossaryTextObject.GetComponent<RectTransform>().localPosition = newPos;
		
		// Add velocity to stop scroll bar going off the top.
		glossaryScrollArea.GetComponent<ScrollRect>().velocity = new Vector2(0, 1000f);
	}
	
	/// <summary>
	/// Sets the text.
	/// </summary>
	/// <param name="glossaryEntry">Glossary entry.</param>
	void SetText(Glossary_Entry[] glossaryEntry)
	{
		// Start the glossary text with a space for readability.
		string glossaryText = "\n";
		
		// Add each entry to the glossary text with breaks for readability.
		foreach (Glossary_Entry entry in glossaryEntry)
		{
			glossaryText += "<b>" + entry.title + "</b>" + "\n\n" + entry.description + "\n\n";
		}
		
		// If there have been no entries added, display No entries found.
		if(glossaryText == "" || glossaryText == "\n")
		{
			glossaryText = MCP.Text (2203)/*"No entries found for "*/ + letterState.ToString ();
		}
		
		// Set the text object to the glossary text.
		glossaryTextObject.GetComponent<Text>().text = glossaryText;
		
		// Work out the height of the glossary text.
		float height = textStyle.CalcHeight (new GUIContent(glossaryText), Screen.width * 0.48f);
		if(height < Screen.height * 0.7f)
		{
			height = Screen.height * 0.7f;
		}
		// Make the text object big enough to contain all the text.
		glossaryTextObject.GetComponent<RectTransform>().sizeDelta = new Vector2(glossaryTextObject.GetComponent<RectTransform>().sizeDelta.x, height);
	}
	
	/// <summary>
	/// Makes the letter buttons.
	/// </summary>
	public void MakeLetterButtons ()
	{
		float xPos = Screen.width * 0.11f;
		float yPos = Screen.height * 0.425f;
		int count = 0;
		
		LetterState[] letters = new LetterState[LetterState.GetValues(typeof(LetterState)).Length];
		LetterState.GetValues(typeof(LetterState)).CopyTo(letters, 0);
		
		for (int i = 0; i < letters.Length; i++)
		{
			// Make the letter button.
			GameObject letterButtonBackground = MakeImage(localCanvas, new Rect(xPos, yPos, Screen.width * 0.05f, Screen.height * 0.06f), buttonBackground, true);
			letterButtonBackground.name = "LetterButtonBackground";
			
			List<MyVoidFunc> funcList = new List<MyVoidFunc>{};
			GameObject letterButton = MakeButton(localCanvas, letters[i].ToString (), new Rect(xPos, yPos, Screen.width * 0.05f, Screen.height * 0.06f), funcList, "InnerLabel");
			letterButton.name = "LetterButton" + letters[i].ToString ();
			letterButton.GetComponent<Text>().color = bmOrange;
			letterButton.GetComponent<Button>().image = letterButtonBackground.GetComponent<Image>();
			letterButton.GetComponent<Button>().onClick.AddListener(
				delegate
				{
					LetterButtonClicked (letterButton.name[letterButton.name.Length-1]);
				});
			
			count++;
			// Move the letter positions down ever 4 letters.
			if(count > 3)
			{
				// Reset x
				xPos = Screen.width * 0.11f;
				// Increment y
				yPos += Screen.height * 0.07f;
				// Reset count
				count = 0;
			}
			else
			{
				// Increment x
				xPos += Screen.width * 0.06f;
			}
		}
	}
	
	/// <summary>
	/// Processes the letter button click.
	/// </summary>
	/// <param name="letter">Letter.</param>
	void LetterButtonClicked(char letter)
	{
		// Get the letter state for this letter.
		foreach(LetterState ls in LetterState.GetValues(typeof(LetterState)))
		{
			if(ls.ToString () == letter.ToString ())
			{
				letterState = ls;
			}
		}
		
		// Get the glossary entries for this letter.
		PickLetterSearchEntries( letterState );
		// Set the text to these entries.
		SetText(search_entries);
	}
	
	public override void DoGUI() {}
}





