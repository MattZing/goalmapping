﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MLM_Simulation : MonoBehaviour
{

		//public MasterNode masterNode;
		
		public static int nodeIndex = 0;
		private string nodeToEdit = "";

		
		public class MasterNode
		{
				public static List<Node> nodeList;
		}

		public class Node
		{
				public int primaryKey;
				public int secondaryKey;
				public GameObject ob;

				public void SetPrimaryKey (int key)
				{
						primaryKey = key;
				}

				public int GetPrimaryKey ()
				{
						return primaryKey;
				}

				public void SetSecondaryKey (int key)
				{
						secondaryKey = key;
				}

				public int GetSecondaryKey ()
				{
						return secondaryKey;
				}

				public void AddClient ()
				{
						nodeIndex++;
						Node currentNode = new Node ();
						currentNode.SetPrimaryKey (nodeIndex);
						currentNode.SetSecondaryKey (primaryKey);
						MasterNode.nodeList.Add (currentNode);
						Visualize (currentNode, nodeIndex, GetSecondaryKey ());
				}

				public void Visualize (Node curNode, int pK, int sK)
				{
						
						foreach (Node node in MasterNode.nodeList) {
								
								if (node.GetPrimaryKey () == pK) {
										curNode.ob = GameObject.CreatePrimitive (PrimitiveType.Sphere);
										curNode.ob.name = "Pk = " + pK + " Sk = " + sK;
										curNode.ob.transform.parent = ob.transform;
										curNode.ob.transform.localPosition = new Vector3 (1/nodeIndex, -sK - 1, 0);
										
												
						
								}
						}
				}


		}
		
		void OnDrawGizmos ()
		{
				Gizmos.color = Color.yellow;
				//Node firstNode = MasterNode.nodeList [0];

				foreach (Node node in MasterNode.nodeList) {
						Node lastNode = node;
						foreach (Node subNode in MasterNode.nodeList) {
								if (lastNode.GetPrimaryKey () == subNode.GetSecondaryKey ()) {
										Gizmos.DrawLine (lastNode.ob.transform.position, subNode.ob.transform.position);
								}
								lastNode = node;
						}
				}
		}

		void Start ()
		{
				//masterNode = new MasterNode();
				MasterNode.nodeList = new List<Node> ();
				Node mNode = new Node ();
				mNode.ob = GameObject.CreatePrimitive (PrimitiveType.Sphere) as GameObject;

				mNode.SetPrimaryKey (0);
				MasterNode.nodeList.Add (mNode);
				
				
				
		}

		void OnGUI ()
		{
				nodeToEdit = GUI.TextField (new Rect (150, 0, 50, 50), nodeToEdit, 10);
				if (GUI.Button (new Rect (0, 0, 150, 50), "Add Client")) {
						List<Node> tmpList = new List<Node> ();
						foreach (Node node in MasterNode.nodeList) {
								if (node.GetPrimaryKey () == int.Parse (nodeToEdit)) {
										tmpList.Add (node);
								}
						}
						foreach (Node node in tmpList) {
								node.AddClient ();
						}
				}
		}

		// Update is called once per frame
		void Update ()
		{
	
		}
}
