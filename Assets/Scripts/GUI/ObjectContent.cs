﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ObjectContent : MonoBehaviour
{
	public List<GameObject> contentObject;
	
	public void SetText(string text, TextAnchor alignment = TextAnchor.MiddleCenter)
	{
		for (int i = 0; i < contentObject.Count; i++)
		{
			if(contentObject[i].GetComponent<Text>() != null)
			{
				contentObject[i].GetComponent<Text>().text = text;
				
				contentObject[i].GetComponent<Text>().alignment = alignment;
			}
		}
	}
	
	public void SetSprite(Sprite sprite, bool scaled = true)
	{
		for (int i = 0; i < contentObject.Count; i++)
		{
			if(contentObject[i].GetComponent<Image>() != null)
			{
				// Set sprite.
				contentObject[i].GetComponent<Image>().sprite = sprite;
				if(!scaled)
				{
					// Set image size.
					contentObject[i].GetComponent<RectTransform>().sizeDelta = new Vector2(sprite.rect.width, sprite.rect.height);
				}
			}
		}
	}
	
	public void SetSprite(Sprite sprite, Rect rect, bool scaled = true)
	{
		for (int i = 0; i < contentObject.Count; i++)
		{
			if(contentObject[i].GetComponent<Image>() != null)
			{
				// Set sprite.
				contentObject[i].GetComponent<Image>().sprite = sprite;
				if(!scaled)
				{
					// Set image size.
					contentObject[i].GetComponent<RectTransform>().sizeDelta = new Vector2(sprite.rect.width, sprite.rect.height);
				}
				
				//contentObject[i].transform.position -= new Vector3(rect.x, rect.y, 0);
				contentObject[i].GetComponent<RectTransform>().sizeDelta = new Vector2(-rect.width, -rect.height);
				contentObject[i].transform.Translate(Vector3.up * -rect.y);
			}
		}
	}
	
	public void SetGameObject(GameObject go)
	{
		for(int i = 0; i < contentObject.Count; i++)
		{
			contentObject[i] = go;
		}
	}
	
	private float GetLowestChildEdge(RectTransform[] childRectTransforms)
	{
		float lowestBound = 0;
		
		for (int i = 1; i < childRectTransforms.Length; i++)
		{
			// BoundPosition is the LocalPosition added to the Rect.Height
			if(childRectTransforms[i].localPosition.y - childRectTransforms[i].rect.height < lowestBound)
			{
				lowestBound = childRectTransforms[i].localPosition.y - (childRectTransforms[i].rect.height * 0.5f);
			}
		}
		
		return lowestBound ;
	}
	
	/// <summary>
	/// Resets the size of the parent object to include all content.
	/// </summary>
	public void ResizeContent(bool horizontal = true, bool vertical = true)
	{
		// Make a new object for the scroll view to avoid scaling content.
		GameObject newScrollContent = new GameObject();
		newScrollContent.AddComponent<RectTransform>();
		newScrollContent.name = "NewScrollContent";
		newScrollContent.transform.SetParent(transform);
		
		// Get the transforms of all children.
		RectTransform[] childObjects = contentObject[0].transform.GetComponentsInChildren<RectTransform>();
		
		// Start with a 0,0 scroll object.
		newScrollContent.GetComponent<RectTransform>().localPosition = Vector2.zero;
		newScrollContent.GetComponent<RectTransform>().localScale = Vector3.one;
		Debug.Log ("Rect: " + GetComponent<RectTransform>().rect);
		float height = GetComponent<RectTransform>().rect.height - GetLowestChildEdge (childObjects);
		newScrollContent.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 1);
		newScrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().rect.width, height);
		
		// Change the objects' parent to the new content parent object.
		for(int i = 1; i < childObjects.Length; i++)
		{
			if(childObjects[i].transform.parent == contentObject[0].transform)
			{
				childObjects[i].SetParent(newScrollContent.transform);
			}
		}
		
		// Destroy the old object.
		Destroy(contentObject[0]);
		// Add the new content object to the scroll functionality.
		contentObject[0] = newScrollContent;
		GetComponent<ScrollRect>().content = contentObject[0].GetComponent<RectTransform>();
	}
}
