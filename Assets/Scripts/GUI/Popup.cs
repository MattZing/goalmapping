﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Popup : MonoBehaviour
{
	public GameObject titleObj;
	public GameObject bodyObj;
	public GameObject buttonObj;
	
	public string[] multiLayerText;
	
	private int currentTextLayer = 0;
	
	/// <summary>
	/// Nexts the popup layer.
	/// </summary>
	/// <returns><c>true</c> if there is a next popup layer, <c>false</c> if this was the last screen.</returns>
	public void NextPopupLayer(string finalButtonText, MenuScreen.CommonTasks task, string levelName = "")
	{
		currentTextLayer++;
		if(currentTextLayer < multiLayerText.Length)
		{
			// Move to the next body text.
			bodyObj.GetComponent<Text>().text = (multiLayerText[currentTextLayer]);
			if(currentTextLayer == multiLayerText.Length - 1)
			{
				buttonObj.GetComponent<ObjectContent>().SetText(finalButtonText);
			}
		}
		else
		{
			// Do the task.
			switch(task)
			{
			case MenuScreen.CommonTasks.DestroyParent:
				Destroy(gameObject);
				break;
			case MenuScreen.CommonTasks.LoadLevel:
				Application.LoadLevel(levelName);
				break;
			}
		}
	}
	
	public void SetTitle(string _title)
	{
		titleObj.GetComponent<ObjectContent>().SetText(_title);
	}
	
	public void SetBody(string _body)
	{
		bodyObj.GetComponent<ObjectContent>().SetText(_body);
	}
	
	public void SetBody(string[] _body)
	{
		bodyObj.GetComponent<ObjectContent>().SetText(_body[0]);
		multiLayerText = _body;
	}
	
	public void SetButtonText(string _buttonText)
	{
		buttonObj.GetComponent<ObjectContent>().SetText(_buttonText);
	}
}
