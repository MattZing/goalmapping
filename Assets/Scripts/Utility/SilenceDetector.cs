﻿using UnityEngine;
using System.Collections;

public class SilenceDetector : MonoBehaviour 
{
	[SerializeField]
	public bool silence = false;											// the private flag for this class
	public float silenceThreshold = 0.0125f;								// the threshold for a 'silent' sample
	public int consecutiveSamplesLimit = 11025;								// the limit of samples to check ( 0.25s @ 44100Hz )
	private int consecutiveSamples = 0;										// count how many samples are below the threshold
	public float avg = 0f;

	void Start () 
	{
	}

	public void OnAudioFilterRead( float[] data, int channels )
	{
		for ( int i = 0; i < data.Length; i++ )
		{
			if ( data[i] < silenceThreshold )
				consecutiveSamples++;
			else
				consecutiveSamples = 0;
		}

		silence = (consecutiveSamples > consecutiveSamplesLimit);
	}
}
