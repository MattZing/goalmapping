using UnityEngine;
using System.Collections;

[RequireComponent (typeof (GUITexture))]
[RequireComponent (typeof (AudioSource))]

public class DrawWaveform : MonoBehaviour
{
	public Texture2D texture;
	
	public bool colorIsGreen = false;
	public bool isDemoCam = false;
	private int width = 512;
	private int height = 512;
	private Color backgroundColor = new Color( 0f, 0f, 0f, 0f );
	private Color waveformColor = Color.white;
	private int size = 1024;
	
	private Color[] blank;
	private float[] samples;
	
	/// <summary>
	/// Used for initialization.
	/// </summary>
	public void Start()
	{
		samples = new float[size];
		
		// Create the texture and assign to the guiTexture:
		texture = new Texture2D(width, height);
		GetComponent<GUITexture>().texture = texture;
		
		// If the color is not green...
		if (!colorIsGreen) 
		{
			
			Vector3 textureScale = new Vector3 (0.6f, 0.5f, 1f);
			GetComponent<GUITexture>().transform.localScale = textureScale;
			
			Vector3 texturePos = new Vector3 (0.5f, 0.48f, 0f);
			GetComponent<GUITexture>().transform.localPosition = texturePos;
		} 
		// Otherwise, if this is in the demo cam...
		else if (colorIsGreen && isDemoCam)
		{
			Vector3 textureScale = new Vector3 (0.73f, 0.59f, 1f);
			GetComponent<GUITexture>().transform.localScale = textureScale;
			
			Vector3 texturePos = new Vector3 (0.41f, 0.62f, 0f);
			GetComponent<GUITexture>().transform.position = texturePos;
		}
		// Otherwise...
		else 
		{	
			Vector3 textureScale = new Vector3 (0.55f, 0.65f, 1f);
			GetComponent<GUITexture>().transform.localScale = textureScale;
			
			Vector3 texturePos = new Vector3 (0.44f, 0.61f, 0f);
			GetComponent<GUITexture>().transform.position = texturePos;
		}
		
		// Create a 'blank screen' image
		blank = new Color[width * height];
		
		// If the color is green...
		if (colorIsGreen) 
		{
			// Set the waveform color to green.
			waveformColor = Color.green;
			// Se tthe background color to black.
			backgroundColor = Color.black;
		}
		else
		{
			// Set the waveform color to orange.
			waveformColor = new Color(239f / 255f, 147f / 255f, 0f / 255f);
			// Set the background color to a transparent grey.
			backgroundColor = new Color(0.2f, 0.2f, 0.2f, 0.5f);
		}
		
		// Set each blank to the background color.
		for( var i = 0; i < blank.Length; i++ )
		{
			blank[i] = backgroundColor;  
		}
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update()
	{
		// clear the texture
		texture.SetPixels( blank, 0 );
			
		// If the audio source is playing...
		if ( GetComponent<AudioSource>().isPlaying )
		{
			// Get samples from channel 0 (left)
			GetComponent<AudioSource>().GetOutputData( samples, 0 );
		
			// Draw the waveform
			for( var i = 0; i < size; i++ )
			{
				texture.SetPixel( (int)(( width * (i)) / size ), (int)(height * (samples[i] + 1f) * 0.5f), waveformColor );
				texture.SetPixel( (int)(( width * (i)) / size ) + 1, (int)(height * (samples[i] + 1f) * 0.5f), waveformColor );
				texture.SetPixel( (int)(( width * (i)) / size ), (int)(height * (samples[i] + 1f) * 0.5f) + 1, waveformColor );
				texture.SetPixel( (int)(( width * (i)) / size ) + 1, (int)(height * (samples[i] + 1f) * 0.5f) + 1, waveformColor );
			}
		}
		
		// Upload to the graphics card.
		texture.Apply();
	}
}
