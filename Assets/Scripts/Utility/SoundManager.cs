﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MusicObject
{
	public AudioSource source;
	public AudioClip clip;
	public float volume;				// this volume will be 0-1 normalised
	public float targetVolume;			// the target volume of this structure
	
	public MusicObject()
	{
		source = new AudioSource();		// audio source volume will be the real volume accounting for the scale
		clip = new AudioClip();			// allocate the audioclip
		volume = 0f;					// set initial clip volume to 0 for pre fade
	}
}

public class Progress
{
	public float startTime;
	public float duration;
	public float startValue;
	public float targetValue;
	public float currentValue;
	public float progress;
}

public class SoundEntry								// used to keep track of sound sources, clips, volume, pan
{
	public int sourceID;							// id into the audioSource array
	public Progress volume;							// volume progress object
	public Progress pan;							// pan progress object
	public Progress pitch;							// pitch progress object
	public AudioClip clip;							// the AudioClip object reference
	public AudioSource source;						// the AudioSource object reference
	public bool killAfterFade = false;				// flag to kill the object after it has faded
	public bool looping = false;					// is this sound looping
	public int priority = 50;						// used for note stealing
	
	/// <summary>
	/// Starts the volume fade.
	/// </summary>
	/// <param name="target">Target volume.</param>
	/// <param name="duration">Duration of fade.</param>
	public void StartVolumeFade( float target, float duration )
	{
		// Set volume variables.
		volume.startValue = volume.currentValue;
		volume.targetValue = target;
		volume.duration = duration;
		volume.startTime = Time.time;
	}
	
	/// <summary>
	/// Starts the pitch sweep.
	/// </summary>
	/// <param name="target">Target pitch.</param>
	/// <param name="duration">Duration of pitch sweep.</param>
	public void StartPitchSweep( float target, float duration )
	{
		// Set pitch varialbes.
		pitch.startValue = pitch.currentValue;
		pitch.targetValue = target;
		pitch.duration = duration;
		pitch.startTime = Time.time;
	}
	
	/// <summary>
	/// Starts the pan fade.
	/// </summary>
	/// <param name="target">Target pan.</param>
	/// <param name="duration">Duration of pan fade.</param>
	public void StartPanFade( float target, float duration )
	{
		// Set pan variables.
		pan.startValue = pan.currentValue;
		pan.targetValue = target;
		pan.duration = duration;
		pan.startTime = Time.time;
	}
	
	// Property setters / getters
	public float Volume 								// volume property
	{
		get { return volume.currentValue; }
		set { volume.currentValue = source.volume = volume.targetValue = value; }
	}
	
	public float Pitch
	{
		get { return pitch.currentValue; }
		set { pitch.currentValue = source.pitch = pitch.targetValue = value; }
	}
	
	public float Pan
	{
		get { return pan.currentValue; }
		set { pan.currentValue = pan.targetValue = source.panStereo = value; }
	}
	
	public bool Looping
	{
		get { return looping; }
		set { looping = source.loop = value; }
	}
}

public class SoundManager : MonoBehaviour
{
		private MusicObject[] musics;

		private AudioClip[] defaultMusic;

	SilenceDetector silenceDetector = null;
		
		//private int musicEnabled = 1;
		private int backgroundMusicEnabled = 1;
		private int activityMusicEnabled = 1;
		private int soundEnabled = 1;
		private int audioPolyphony = 3;
		private AudioSource[] sfxSource;
		
		private int musicIndex = 0;
		public float musicVolScale = 1f;
		
		public float backgroundMusicVolScale = 1f;
		
		public float activityMusicVolScale = 1f;
		
		private List<SoundEntry> soundEntries = new List<SoundEntry>();									// list of sound entry objects
		private float updateTime = 0.1f;																// time to wait between updates
		
		IEnumerator UpdateSounds()
		{
			foreach( SoundEntry se in soundEntries )
			{
				// Update volume fades
				se.volume.progress = ( Time.time - se.volume.startTime ) / se.volume.duration;
				se.volume.currentValue = Mathf.Lerp( se.volume.startValue , se.volume.targetValue, se.volume.progress );
				se.source.volume = se.volume.currentValue;															// You might not need to set the source objects volume
				
				// Update pitch sweeps
				se.pitch.progress = ( Time.time - se.pitch.startTime ) / se.pitch.duration;
				se.pitch.currentValue = Mathf.Lerp( se.pitch.startValue, se.pitch.targetValue, se.pitch.progress );
				se.source.pitch = se.pitch.currentValue;																// set the source object pitch here
				
				// Update pan sweeps
				se.pan.progress = ( Time.time - se.pan.startTime ) / se.pan.duration;
				se.pan.currentValue = Mathf.Lerp( se.pan.startValue, se.pan.targetValue, se.pan.progress );
				se.source.panStereo = se.pan.currentValue;																	// set the source object pan here
				
				// check to see if this sound should be killed
				yield return new WaitForSeconds( updateTime );											// breathe
			}
		}
		
		/// <summary>
		/// Used to initialize.
		/// </summary>
		void Start ()
		{
			// Stop this being destroyed on scene change.
			DontDestroyOnLoad( this );
			
			musicIndex = 0;
			musics = new MusicObject[2];
			
			// Set up the music objects.
			for ( int i = 0; i < 2; i++ )
			{
				musics[i] = new MusicObject();
				musics[i].source = gameObject.AddComponent<AudioSource>(  ) as AudioSource;
				musics[i].source.loop = true;
			}
			
			// Set up the sfx objects.
			sfxSource = new AudioSource[audioPolyphony];
			for ( int i = 0; i < audioPolyphony; i++ )
			{
				sfxSource[i] = gameObject.AddComponent<AudioSource>(  ) as AudioSource;
			}
			
			defaultMusic = new AudioClip[2];
			
			// Get the user's music selection from inventory.
			defaultMusic[0] = Resources.Load( "Audio/EmptySpace" ) as AudioClip;
			defaultMusic[1] = Resources.Load( "Audio/EmptySpace" ) as AudioClip;
			
			if ( !defaultMusic[0] || !defaultMusic[1])
			{
				Debug.Log( "Couldn't load default music clip" );
			}
			
			foreach(MusicObject mo in musics)
			{
				// Set music object variables.
				mo.volume = 1f;
				mo.source.volume = mo.volume * backgroundMusicVolScale; 
				mo.clip = defaultMusic[0];
				mo.source.clip = defaultMusic[0];
			}
			
			// Get the user settings related to sound.
			if ( PlayerPrefs.HasKey( "BackgroundMusicEnabled" )) 
			{
				backgroundMusicEnabled = PlayerPrefs.GetInt( "BackgroundMusicEnabled" );
				
				if ( backgroundMusicEnabled == 1 )
				{
					if ( PlayerPrefs.HasKey("BackgroundMusicVolume"))
					{
						SetBackgroundMusicVolume( PlayerPrefs.GetFloat( "BackgroundMusicVolume" ) );
					}
				}
			}
			
			if ( PlayerPrefs.HasKey( "SoundEnabled" ))
			{
				soundEnabled = PlayerPrefs.GetInt("SoundEnabled");
			}
			
			if ( PlayerPrefs.HasKey( "SoundVolume" ))
			{
				SetSFXVolume( PlayerPrefs.GetFloat("SoundVolume"));
			}
			
			// Update the sounds.
			StartCoroutine( UpdateSounds() );

			silenceDetector = gameObject.AddComponent<SilenceDetector>();
		}

	public bool IsSilent()
	{
		bool silent = true;
		
		if ( silenceDetector != null )
		{
			silent = silenceDetector.silence;
		}
		
		return silent;
	}
		
		/// <summary>
		/// Gets the free SFX source.
		/// </summary>
		/// <returns>The free SFX source.</returns>
		private AudioSource GetFreeSFXSource()
		{
			for ( int i = 0; i < sfxSource.Length; i++ )
			{
				if ( !sfxSource[i].isPlaying )
				{
					return sfxSource[i];
				}
			}
			
			return sfxSource[0];
		}
		
		/// <summary>
		/// Raises the level was loaded event.
		/// </summary>
		void OnLevelWasLoaded()
		{
			if(musicIndex == 1)
			{
				musicIndex = 0;
				StartBackgroundMusic();
			}
		}
		
		/// <summary>
		/// Sets the background music track.
		/// </summary>
		/// <param name="trackPath">The track path.</param>
		public void SetBackgroundMusicTrack( string trackPath)
		{
			// Load the track from the path.
			defaultMusic[0] = Resources.Load(trackPath) as AudioClip;
			
			// If it is not null...
			if ( musics[0] != null && defaultMusic[0] != null )
			{
				// If it is not the same as the current music track...
				if(musics[0].clip != defaultMusic[0])
				{
					// Set the music clip to the new clip.
					musics[0].clip = defaultMusic[0];
					musics[0].source.clip = defaultMusic[0];
					// If music is enabled, start the new background music.
					if(backgroundMusicEnabled == 1)
					{
						StartBackgroundMusic();
					}
				}
			}
		}
		
		/// <summary>
		/// Sets the background music volume.
		/// </summary>
		/// <param name="volume">Volume.</param>
		public void SetBackgroundMusicVolume( float volume )
		{
			backgroundMusicVolScale = volume;
			
			musics[0].source.volume = musics[0].volume * backgroundMusicVolScale;
			
			// Store the music volume in the player's preferences.
			PlayerPrefs.SetFloat( "BackgroundMusicVolume", volume );
		}
		
		/// <summary>
		/// Sets the SFX volume.
		/// </summary>
		/// <param name="volume">Volume.</param>
		public void SetSFXVolume( float volume )
		{
			for ( int i = 0; i < sfxSource.Length; i++ )
			{
				sfxSource[i].volume = volume;
			
				// Store the sfx volume in the player's preferences.
				PlayerPrefs.SetFloat( "SoundVolume", volume );
			}
		}
		
		/// <summary>
		/// Starts the background music.
		/// </summary>
		public void StartBackgroundMusic()
		{
			// If the background music is enabled...
			if ( GetBackgroundMusicEnabled() )
			{	
				// Play the music.
				musics[0].source.Play();
			}
		}
		
		/// <summary>
		/// Stops the background music.
		/// </summary>
		public void StopBackgroundMusic()
		{
			// Stop the music.
			musics[0].source.Stop();
		}
		
		/// <summary>
		/// Gets the background music volume.
		/// </summary>
		/// <returns>The background music volume.</returns>
		public float GetBackgroundMusicVolume()
		{
			return backgroundMusicVolScale;
		}
		
		/// <summary>
		/// Gets the sound volume.
		/// </summary>
		/// <returns>The sound volume.</returns>
		public float GetSoundVolume()
		{
			float volume = 0f;
			
			if ( sfxSource[0] != null )
			{
				volume = sfxSource[0].volume;										
			}
				
			return volume;
		}
		
		/// <summary>
		/// Sound effect one shot functions
		/// </summary>
		/// <param name="clip">The audio clip.</param>
		/// <param name="rndPitch">If set to <c>true</c> random pitch.</param>
		public void PlayOneShotSound( AudioClip clip, bool rndPitch = true )
		{
			AudioSource src = GetFreeSFXSource();
			src.pitch = 1f;
			
			if ( rndPitch )
			{
				src.pitch = Random.Range( 0.99f, 1.01f ); // 50 cents variation
			}
		
			// If the sound is enabled...
			if ( soundEnabled == 1 )
			{
				// Play the sound.
				src.PlayOneShot( clip );
			}
		}
		
		/// <summary>
		/// Plays the one shot sound, overriding others.
		/// </summary>
		/// <param name="clip">Clip.</param>
		/// <param name="rndPitch">If set to <c>true</c> random pitch.</param>
		public void PlayOneShotSoundOverride( AudioClip clip, bool rndPitch = true )
		{
			AudioSource src = GetFreeSFXSource();
			src.pitch = 1f;
			
			if ( rndPitch )
			{
				src.pitch = Random.Range( 0.99f, 1.01f ); // 50 cents variation
			}
			
			src.PlayOneShot( clip );
		}
	
		/// <summary>
		/// Plays the sound.
		/// </summary>
		/// <param name="clip">Audio clip to play.</param>
		/// <param name="rndPitch">If set to <c>true</c> random pitch.</param>
		public void PlaySound( AudioClip clip, bool rndPitch = true )
		{
			AudioSource src = GetFreeSFXSource();
			src.pitch = 1f;
			
			if ( rndPitch )
			{
				src.pitch = Random.Range( 0.99f, 1.01f ); // 50 cents variation
			}
			
			// If the sound is enabled...
			if ( soundEnabled == 1 )
			{
				// Set the source clip.
				src.clip = clip;
				// Play the clip.
				src.Play();
			}
		}
		
		/// <summary>
		/// Plays the sound looped.
		/// </summary>
		/// <param name="clip">Audio clip to play.</param>
		public void PlaySoundLooped( AudioClip clip )
		{
			AudioSource src = GetFreeSFXSource();
			
			// If the sound is enabled...
			if ( soundEnabled == 1 )
			{
				// Set the source clip.
				src.clip = clip;
				// Turn on loop.
				src.loop = true;
				// Play the sound.
				src.Play();
			}
		}
		
		/// <summary>
		/// Stops the looped sound.
		/// </summary>
		/// <param name="clip">Audio clip to stop.</param>
		public void StopLoopedSound( AudioClip clip )
		{
			// If the clip is playing, stop it.
			for ( int i = 0; i < sfxSource.Length; ++i )
			{
				if ( sfxSource[i].clip == clip )
				{
					Debug.Log( "Found looping sound and stopping" );
					sfxSource[i].loop = false;
					sfxSource[i].Stop();
					break;
				}
			}
		}
		
		/// <summary>
		/// Determines whether this sound clip is currently playing.
		/// </summary>
		/// <returns><c>true</c> if this instance is sound playing the specified clip; otherwise, <c>false</c>.</returns>
		/// <param name="clip">Clip.</param>
		public bool IsSoundPlaying( AudioClip clip )
		{			
			// Loop through each current sound...
			for ( int i = 0; i < sfxSource.Length; ++i )
			{
				// If it contains the clip that is input...
				if ( sfxSource[i].clip == clip )
				{
					// If it is playing...
					if ( sfxSource[i].isPlaying )
					{
						return true;
					}
				}
			}
			
			// Otherwise...
			return false;
		}

		public float SoundTime( AudioClip clip )
		{			
			// Loop through each current sound...
			for ( int i = 0; i < sfxSource.Length; ++i )
			{
				// If it contains the clip that is input...
				if ( sfxSource[i].clip == clip )
				{
					// If it is playing...
					if ( sfxSource[i].isPlaying )
					{
						return sfxSource[i].time;
					}
				}
			}
			
			// Otherwise...
			return 0.0f;
		}


		/// <summary>
		/// Stops the sound.
		/// </summary>
		/// <param name="clip">Audio clip to stop.</param>
		public void StopSound( AudioClip clip )
		{
			// Loop through each current sound...
			for ( int i = 0; i < sfxSource.Length; ++i )
			{
				// If it contains the clip that is input...
				if ( sfxSource[i] != null && sfxSource[i].clip == clip )
				{
					// If it is playing...
					if ( sfxSource[i].isPlaying )
					{
						// Stop it.
						sfxSource[i].Stop();
						break;
					}
				}
			}
		}
		
		/// <summary>
		/// Sets the background music enabled.
		/// </summary>
		/// <param name="enabled">If set to <c>true</c>, enable music.</param>
		public void SetBackgroundMusicEnabled( bool enabled )
		{
			if( enabled )
			{
				backgroundMusicEnabled = 1;
				// Store the preference.
				PlayerPrefs.SetInt( "BackgroundMusicEnabled", 1 );
				// Start the music
				StartBackgroundMusic();
			}
			
			else
			{
				backgroundMusicEnabled = 0;
				// Store the preference.
				PlayerPrefs.SetInt( "BackgroundMusicEnabled", 0 );
				// Stop the music.
				StopBackgroundMusic();
			}
		}
	
		/// <summary>
		/// Gets the background music enabled.
		/// </summary>
		/// <returns><c>true</c>, if background music is enabled, <c>false</c> otherwise.</returns>
		public bool GetBackgroundMusicEnabled()
		{
			if ( backgroundMusicEnabled == 0 )
			{
				return false;
			}
			
			return true;
		}
		
		/// <summary>
		/// Sets the sound enabled.
		/// </summary>
		/// <param name="enabled">If set to <c>true</c>, enable sound.</param>
		public void SetSoundEnabled( bool enabled )
		{
			if ( enabled )
			{
				// Enable the sound.
				if ( soundEnabled == 0 )
				{
					soundEnabled = 1;
				}
				
				// Store the preference.
				PlayerPrefs.SetInt( "SoundEnabled", 1 );
			}
				
			else
			{
				// Disable the sound.
				if ( soundEnabled == 1 )
				{
					soundEnabled = 0;
				}
			
				// Store the preference.
				PlayerPrefs.SetInt( "SoundEnabled", 0 );
			}
		}
		
		/// <summary>
		/// Gets wether the sound is enabled.
		/// </summary>
		/// <returns><c>true</c>, if sound enabled was gotten, <c>false</c> otherwise.</returns>
		public bool GetSoundEnabled()
		{
			if ( soundEnabled == 1 )
			{
				return true;
			}
			
			return false;
		}
		
	/// <summary>
	/// Gets a value indicating whether the sound is muted.
	/// </summary>
	/// <value><c>true</c> if the sound is muted; otherwise, <c>false</c>.</value>
	public bool IsMuted
	{
		get
		{
			return (backgroundMusicEnabled == 0 && activityMusicEnabled == 0 && soundEnabled == 0);
		}
	}
}
