﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TestScreen : MenuScreen
{
	GameObject localCanvas;
	GameObject goalMapObject;
	
//	Sprite testSprite;
	
	AudioClip testClip;
	MCP.GoalMap goalMap;
	string successDir = "assets/resources/successcentre/";
	
	// Use this for initialization
	void Start ()
	{
		Init ();
		
//		testSprite = Resources.Load<Sprite> ("GUI/Buttons/bm_button_bg128px");
		StartCoroutine(MCP.GetAssetBundle("bronzeresources01.bronze", false));
		
		CreateGUI ();
	}
	
	void CreateGUI()
	{
		localCanvas = MakeCanvas ();
		localCanvas.name = "Canvas";
		
//		string s = "hrbthztthztfhdthz";
		
//		List<MyPopupFunc> funcList = new List<MyPopupFunc>
//		{
//			delegate
//			{
//				DoStateButtonID(0);
//			}
//		};
//		
//		//MakeButton (GameObject parent, string buttonText, Rect rect, List<MyPopupFunc> functionNames, string finalButtonText, CommonTasks task, string levelName = "", string guiStyle = null)
//		GameObject image = MakeLabel(localCanvas, "Button", new Rect(Screen.width * 0.25f, Screen.height * 0.125f, Screen.width * 0.5f, Screen.height * 0.5f), TextAnchor.UpperLeft);
////		GameObject scrollArea = MakeScrollRect (localCanvas, new Rect(Screen.width * 0.25f, Screen.height * 0.25f, Screen.width * 0.5f, Screen.height * 0.5f), image);
//		
////		image.transform.SetParent (scrollArea.transform.GetChild (0).GetChild (0), true);
//		image.transform.localScale = Vector3.one;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(testClip == null && MCP.downloadedAssetBundle != null)
		{
			testClip = MCP.downloadedAssetBundle.LoadAsset ((successDir + "2. Listen - Process - The 7 Empowering Questions.mp3").ToLower()) as AudioClip;
			
			sndManager.PlaySound (testClip, false);
		}
	}
	
	bool DoStateButtonID (int i)
	{
		Debug.Log ("I: " + i);
		/*sigBox.SetActive (false);
		gameObject.GetComponent<WebVideoPlayer> ().enabled = false;
		Debug.Log ("Button " + i + " clicked.");
		if (i == 0)
			Camera.main.GetComponent<GUITexture> ().enabled = true;
		else
			Camera.main.GetComponent<GUITexture> ().enabled = false;
		
		switch (i)
		{
		case (int)InternalState.page1video:	
			gameObject.GetComponent<WebVideoPlayer> ().enabled = false;
			break;
		case (int)InternalState.page2text:
			
			Debug.Log ("page2goals length = " + page2goals.Count);
			for (int x=0; x<numberOfGoals; x++) {
				Debug.Log ("page1Goal : Goal = " + page1goals [x].GetComponent<Text> ().text);
				goalMap.goals [x].goal = page1goals [x].GetComponent<Text> ().text;
				Debug.Log ("Goal map: Goals count = " + goalMap.goals.Count);
				page2goals [x].GetComponent<Text> ().text = goalMap.goals [x].goal;
			}
			
			break;
			
		case (int)InternalState.page3display:
			for (int x=0; x<numberOfGoals; x++) {
				page3goals [x].GetComponent<Text> ().text = goalMap.goals [x].goal;
			}
			
			break;
			
		case (int)InternalState.page6draw:
			
			why1label.GetComponent<Text>().text = goalMap.whys [0].why;
			why2label.GetComponent<Text> ().text = goalMap.whys [1].why;
			why3label.GetComponent<Text> ().text = goalMap.whys [2].why;
			break;
			
		case (int)InternalState.page12draw:
			sigBox.SetActive (true);
			break;
			
		case (int)InternalState.exit:
			StartCoroutine(SaveGoalMap());
			break;
		}
		ChangeState (i);
		*/
		return true;
	}	
	
	public override void DoGUI() {}
}
