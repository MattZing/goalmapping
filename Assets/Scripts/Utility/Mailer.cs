﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Mailer : MenuScreen
{
	public GameObject sceneCanvas;
	private GameObject localCanvas;
	private GameObject emailToObject;
	private GameObject emailFromObject;
	private GameObject emailSubjectObject;
	private GameObject emailMessageObject;
	private GameObject popup;
	private GameObject popupSpinner;
	
	private Sprite spinnerSprite;
	
	public byte[] screenshot;
	private bool messageSending = false;
	private float spinnerRot = 0;
	
	/// <summary>
	/// Used to initialize this script.
	/// </summary>
	void Start ()
	{
		// Initialize the base class.
		Init();	
		
		// Load resources
		spinnerSprite = Resources.Load<Sprite> ("GUI/bm_rotate");
		
		// Create the 3D GUI
		CreateGUI();
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	private void CreateGUI()
	{
		// Get the scene canvas if not already set, then hide it.
		if(sceneCanvas == null)
		{
			sceneCanvas = MakeCanvas (false);
		}
		sceneCanvas.SetActive (false);
		
		// Make the local canvas.
		localCanvas = MakeCanvas (true);
		
		// Make background window
		GameObject backgroundWindow = MakeWindowBackground (localCanvas, new Rect(Screen.width * 0.1f, Screen.height * 0.15f, Screen.width * 0.8f, Screen.height * 0.8f), window_back);
		backgroundWindow.name = "BackgroundWindow";
		
		float xLeftPos = Screen.width * 0.12f;
		float xRightPos = Screen.width * 0.3f;
		float yOffset = Screen.height * 0.19f;
		
		// Make from label
		GameObject fromLabelObject = MakeLabel (localCanvas, MCP.Text (3702)/*"From: "*/, new Rect(xLeftPos, yOffset, Screen.width * 0.15f, Screen.height * 0.08f), TextAnchor.MiddleRight);
		fromLabelObject.name = "FromLabelObject";
		
		// Make from input field
		emailFromObject = MakeInputField(localCanvas, "", new Rect(xRightPos, yOffset, Screen.width * 0.58f, Screen.height * 0.08f), false, inputBackground);
		emailFromObject.name = "EmailFromObject";
		if(MCP.userInfo != null)
		{
			emailFromObject.GetComponent<InputField>().text = MCP.userInfo.email;
			emailFromObject.GetComponent<InputField>().interactable = false;
		}
		
		yOffset += Screen.height * 0.1f;
		
		// Make to label
		GameObject toLabelObject = MakeLabel (localCanvas, MCP.Text (3701)/*"To: "*/, new Rect(xLeftPos, yOffset, Screen.width * 0.15f, Screen.height * 0.08f), TextAnchor.MiddleRight);
		toLabelObject.name = "ToLabelObject";
		
		// Make to input field
		emailToObject = MakeInputField(localCanvas, "", new Rect(xRightPos, yOffset, Screen.width * 0.58f, Screen.height * 0.08f), false, inputBackground);
		emailToObject.name = "EmailToObject";
		
		yOffset += Screen.height * 0.1f;
		
		// Make subject label
		GameObject subjectLabelObject = MakeLabel (localCanvas, MCP.Text (3703)/*"Subject: "*/, new Rect(xLeftPos, yOffset, Screen.width * 0.15f, Screen.height * 0.08f), TextAnchor.MiddleRight);
		subjectLabelObject.name = "SubjectLabelObject";
		
		// Make subject input field
		emailSubjectObject = MakeInputField(localCanvas, "", new Rect(xRightPos, yOffset, Screen.width * 0.58f, Screen.height * 0.08f), false, inputBackground);
		emailSubjectObject.name = "EmailSubjectObject";
		
		yOffset += Screen.height * 0.1f;
		
		// Make message label
		GameObject messageLabelObject = MakeLabel (localCanvas, MCP.Text (3704)/*"Message: "*/, new Rect(xLeftPos, yOffset, Screen.width * 0.15f, Screen.height * 0.08f), TextAnchor.MiddleRight);
		messageLabelObject.name = "MessageLabelObject";
		
		// Make message input label
		emailMessageObject = MakeInputField(localCanvas, "", new Rect(xRightPos, yOffset, Screen.width * 0.58f, Screen.height * 0.28f), false, inputBackground);
		emailMessageObject.name = "EmailMessageObject";
		emailMessageObject.GetComponent<InputField>().lineType = InputField.LineType.MultiLineNewline;
		
		yOffset += Screen.height * 0.1f;
		
		// Make cancel button
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			Close
		};
		GameObject cancelButton = MakeButton (localCanvas, "Cancel", buttonBackground, new Rect(Screen.width * 0.2f, Screen.height * 0.825f, Screen.width * 0.15f, Screen.height * 0.08f), funcList);
		cancelButton.name = "CancelButton";
		
		// Make send button
		funcList = new List<MyVoidFunc>
		{
			SendMail
		};
		GameObject sendButton = MakeButton (localCanvas, "Send", buttonBackground, new Rect(Screen.width * 0.65f, Screen.height * 0.825f, Screen.width * 0.15f, Screen.height * 0.08f), funcList);
		sendButton.name = "SendButton";
		
		//DEBUG:
		if(Debug.isDebugBuild)
		{
//			emailToObject.GetComponent<InputField>().text = "matt@zingup.com";
//			emailFromObject.GetComponent<InputField>().text = "matt@zingup.com";
//			emailSubjectObject.GetComponent<InputField>().text = "Goal Map";
//			emailMessageObject.GetComponent<InputField>().text = "A Goal Map";
		}
		
		// Make the loading popup fade background.
		popup = MakeImage (localCanvas, new Rect(0, 0, Screen.width, Screen.height), menuBackground);
		popup.name = "LoadingBackground";
		
		// Make the loading popup background.
		GameObject popupPanel = MakeImage (localCanvas, new Rect(Screen.width * 0.25f, Screen.height * 0.3f, Screen.width * 0.5f, Screen.height * 0.45f), popup_back, true);
		popupPanel.name = "LoadingBackgroundPanel";
		popupPanel.transform.SetParent (popup.transform);
		
		// Make the loading text.
		GameObject loadingLabel = MakeLabel (localCanvas, MCP.Text (3705)/*"Sending..."*/, new Rect(Screen.width * 0.35f, Screen.height * 0.35f, Screen.width * 0.3f, Screen.height * 0.2f), TextAnchor.MiddleCenter, "InnerLabel");
		loadingLabel.name = "LoadingLabel";
		loadingLabel.transform.SetParent (popup.transform);
		
		// Make the loading spinner.
		popupSpinner = MakeImage (localCanvas, new Rect(Screen.width * 0.5f, Screen.height * 0.5f, Screen.width * 0.08f, Screen.width * 0.08f), spinnerSprite, false);
		popupSpinner.name = "LoadingSpinner";
		popupSpinner.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
		popupSpinner.transform.SetParent (popup.transform);
		popup.SetActive (false);
	}
	
	/// <summary>
	/// Sends the message.
	/// </summary>
	void SendMail()
	{
		string to = emailToObject.GetComponent<InputField>().text;
		string from = emailFromObject.GetComponent<InputField>().text;
		string subject = emailSubjectObject.GetComponent<InputField>().text;
		string message = emailMessageObject.GetComponent<InputField>().text;
		
		StartCoroutine (MCP.SendMail(to, from, subject, message, screenshot));
		messageSending = true;
		popup.SetActive (true);
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update ()
	{
		if(messageSending)
		{
			if(!MCP.isTransmitting)
			{
				Close ();
			}
			else
			{
				spinnerRot -= Time.deltaTime * 75f;
				popupSpinner.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, spinnerRot);
			}
		}
	}
	
	/// <summary>
	/// Close this script and returns to the main script.
	/// </summary>
	void Close()
	{
		// Reactivate the main scene canvas.
		sceneCanvas.SetActive (true);
		// Remove all objects relating to this script.
		Destroy (localCanvas);
		// Remove this script.
		Destroy (this);
	}
	
	public override void DoGUI() {}
}
