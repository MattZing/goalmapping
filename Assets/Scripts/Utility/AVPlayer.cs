using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

//TODO: Make audio work on standalone

public class AVPlayer : MenuScreen
{
	public GameObject videoManagerObject;
	private GameObject localCanvas;
	private GameObject downloadingPopup;
	private GameObject downloadingSpinner;
#if !UNITY_ANDROID && !UNITY_IPHONE
	private GameObject fullscreenCanvas;
#endif
	public GameObject sceneCanvas;
	private GameObject sectionTitleObject;
	private GameObject mediaTitleObject;
	private GameObject visualPanelObject;
	private GameObject playButtonObject;
	private GameObject muteButtonObject;
	private GameObject volumeSliderObject;
	private GameObject audioPosBackground;
	
	private GameObject audioPositionSliderObject;
	private GameObject currentTimeLabel;
	private GameObject totalTimeLabel;
	private GameObject playPanel;
	private GameObject noMediaSelectedText;
	private GameObject waveformObject;
	
#if !UNITY_ANDROID && !UNITY_IPHONE
	private GameObject fullscreenButton;
	private GameObject fullscreenButtonLabel;
	private GameObject exitFullscreenButton;
	private GameObject exitFullscreenButtonLabel;
	private GameObject fsPlayButtonObject;
	private GameObject fsAudioPosBackground;
	private GameObject fsCurrentTimeLabel;
	private GameObject fsAudioPositionSliderObject;
	private GameObject fsTotalTimeLabel;
	private GameObject fsVolumeSliderObject;
	private GameObject fsMuteButtonObject;
	private GameObject fsPlayPanel;
#endif
	
	private Sprite[] playSprite;
	private Sprite[] folderSprite;
	private Sprite[] fullscreenSprite;
	private Sprite[] exitFullscreenSprite;
	private Sprite muteSprite;
	private Sprite unmuteSprite;
	private Sprite spinnerSprite;
	
	public string currentAssetPack;
	public string openFolderPath = "";
	private bool playing = false;
	private bool stopped = false;
	private bool fastforwarding = false;
	private bool rewinding = false;
	private bool mute = false;
	private bool fileLoading = false;
	private bool fileLoaded = false;
	private bool fullscreen = false;
	private float volume = 50f;
	private float timePlaying = 0f;
	private float prevSliderPos = 0;
	private float spinnerRot = 0;
	private Rect screenSize;
	private string filePathStore = "";
#if UNITY_IOS
	private float downloadPercent = 0;
#endif
	
#if UNITY_ANDROID || UNITY_IPHONE
	private WWW hs_get;
#endif
	
	private AudioClip audioClip;
#if !UNITY_ANDROID && !UNITY_IPHONE
	private bool initialPlay = true;
	private MovieTexture videoClip;
#endif
	private bool playedMedia = false;
	
#if UNITY_ANDROID || UNITY_IPHONE
	public bool m_bFinish = false;
	
	public MediaPlayerCtrl scrMedia;
#endif
	
	private AudioSource waveformSource;
	
	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start()
	{
		// Initialize the base class.
		Init ();
		
		// Initialize arrays/lists
		folderSprite = new Sprite[2];
		fullscreenSprite = new Sprite[2];
		exitFullscreenSprite = new Sprite[2];
		playSprite = new Sprite[2];
		
		// Load resources
		playSprite[0] = Resources.Load<Sprite> ("GUI/bm_play_button");
		playSprite[1] = Resources.Load<Sprite> ("GUI/bm_play_button_active");
		muteSprite = Resources.Load<Sprite> ("GUI/bm-nosound");
		unmuteSprite = Resources.Load<Sprite> ("GUI/bm-sound");
		spinnerSprite = Resources.Load<Sprite> ("GUI/bm_rotate");
		folderSprite[0] = Resources.Load<Sprite>("GUI/bm_folder");
		folderSprite[1] = Resources.Load<Sprite>("GUI/bm_folder_active");
		fullscreenSprite[0] = Resources.Load<Sprite>("GUI/bm_fullscreen");
		fullscreenSprite[1] = Resources.Load<Sprite>("GUI/bm_fullscreen_active");
		exitFullscreenSprite[0] = Resources.Load<Sprite>("GUI/bm_fullscreenexit");
		exitFullscreenSprite[1] = Resources.Load<Sprite>("GUI/bm_fullscreenexit_active");
		
		// Make the waveform object.
		waveformObject = new GameObject();
		waveformObject.name = "Waveform";
		waveformObject.transform.localPosition = new Vector3(0, -0.05f, 0);
		waveformSource = waveformObject.AddComponent<AudioSource>();
		waveformObject.AddComponent<GUITexture>();
//		waveformObject.AddComponent<DrawWaveform>();
//		waveformObject.SetActive (false);
		
		audioClip = MCP.loadedAudio;
		
		sndManager.StopBackgroundMusic ();
		
#if UNITY_ANDROID || UNITY_IPHONE
		scrMedia = videoManagerObject.GetComponent<MediaPlayerCtrl>();
		scrMedia.OnEnd += OnEnd;
#endif
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Get the scene canvas
		sceneCanvas.GetComponent<Canvas>().renderMode = RenderMode.WorldSpace;
		
		// Disable the background scene
		sceneCanvas.SetActive (false);
		
		// Make new canvas for av player
		localCanvas = MakeCanvas (true);
		localCanvas.name = "Canvas";
		localCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
		MenuScreen.firstFrameTimer = -2;
		
		// Make background panel.
		MakeWindowBackground (localCanvas, new Rect(Screen.width * 0.02f, Screen.height * 0.15f, Screen.width * 0.96f, Screen.height * 0.83f), window_back).name = "BackgroundPanel";
		
		sectionTitleObject = MakeTitleBar (localCanvas, "Success Centre - " + currentAssetPack);
		sectionTitleObject.name = "SectionTitleObject";
		
		// Make back button side gizmo.
		MakeSideGizmo (localCanvas, false, true);
		
		// Make visual panel.
		visualPanelObject = MakeWindowBackground (localCanvas, new Rect(Screen.width * 0.2f, Screen.height * 0.275f, Screen.width * 0.6f, Screen.height * 0.6f), menuBackground);
		visualPanelObject.name = "VisualPanelObject";
		
		// Set the button navigation mode.
		UnityEngine.UI.Navigation nav = new UnityEngine.UI.Navigation();
		nav.mode = UnityEngine.UI.Navigation.Mode.None;
		
		// Make play/pause button
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			ControlPlay
		};
		playButtonObject = MakeButton (localCanvas, "", playSprite[0], new Rect(Screen.width * 0.439f, Screen.height * 0.5f, Screen.width * 0.1f, Screen.width * 0.1f), funcList);
		playButtonObject.name = "PlayButton";
		playButtonObject.GetComponent<Button>().navigation = nav;
		// Change the button transition to sprite swap.
		playButtonObject.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		SpriteState spriteState = new SpriteState();
		playButtonObject.GetComponent<Button>().targetGraphic = playButtonObject.transform.GetChild (0).gameObject.GetComponent<Image>();
		// Set the highlighted sprite.
		spriteState.highlightedSprite = playSprite[1];
		spriteState.pressedSprite = playSprite[1];
		playButtonObject.GetComponent<Button>().spriteState = spriteState;
		
		// Make volume slider
		volumeSliderObject = MakeSliderVertical (localCanvas, new Rect(Screen.width * 0.875f, Screen.height * 0.275f, Screen.width * 0.025f, Screen.height * 0.5f));
		volumeSliderObject.name = "VolumeSliderObject";
		volumeSliderObject.GetComponent<Slider>().maxValue = 20f;
		volumeSliderObject.GetComponent<Slider>().minValue = 0f;
		volumeSliderObject.GetComponent<Slider>().value = 10f;
		volumeSliderObject.GetComponent<Slider>().wholeNumbers = true;
		
		// Make volume/mute button
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				ControlMute();
				volumeSliderObject.GetComponent<Slider>().interactable = !mute;
#if !UNITY_ANDROID && !UNITY_IOS
				fsVolumeSliderObject.GetComponent<Slider>().interactable = !mute;
#endif
			}
		};
		muteButtonObject = MakeButton (localCanvas, "", unmuteSprite, new Rect(Screen.width * 0.8675f, Screen.height * 0.8125f, Screen.width * 0.04f, Screen.width * 0.04f), funcList);
		muteButtonObject.name = "MuteButtonObject";
		
		// Set the audio position background.
		audioPosBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.2f, Screen.height * 0.7625f, Screen.width * 0.6f, Screen.height * 0.1125f), menuBackground);
		audioPosBackground.name = "AudioPosBackground";
		
		// Make the audio position slider,
		audioPositionSliderObject = MakeSliderHorizontal (localCanvas, new Rect(Screen.width * 0.32f, Screen.height * 0.335f, Screen.width * 0.36f, Screen.height * 0.025f));
		audioPositionSliderObject.name = "AudioPositionSliderObject";
		// Set the slider attributes.
		audioPositionSliderObject.GetComponent<Slider>().maxValue = 100f;
		audioPositionSliderObject.GetComponent<Slider>().minValue = 0f;
		
		if(waveformSource != null && waveformSource.clip != null)
		{
			audioPositionSliderObject.GetComponent<Slider>().value = timePlaying * 100 / waveformSource.clip.length;
		}
		else
		{
			audioPositionSliderObject.GetComponent<Slider>().value = 0;
		}
		
		// Make the current time label.
		currentTimeLabel = MakeLabel (localCanvas, "00:00:00", new Rect(Screen.width * 0.205f, Screen.height * 0.7725f, Screen.width * 0.1f, Screen.height * 0.1f), "SmallText");
		currentTimeLabel.name = "CurrentTimeLabel";
		// Set the current time attributes.
		currentTimeLabel.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
		currentTimeLabel.GetComponent<Text>().resizeTextForBestFit = false;
		currentTimeLabel.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		
		// Make the total time label.
		totalTimeLabel = MakeLabel (localCanvas, "00:00:00", new Rect(Screen.width * 0.7f, Screen.height * 0.7725f, Screen.width * 0.1f, Screen.height * 0.1f), "SmallText");
		totalTimeLabel.name = "TotalTimeLabel";
		// Set the total time label.
		totalTimeLabel.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
		totalTimeLabel.GetComponent<Text>().resizeTextForBestFit = false;
		totalTimeLabel.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		
		// Make open file button.
		funcList = new List<MyVoidFunc>
		{
			OpenFileBrowser
		};
		GameObject openButton = MakeButton (localCanvas, "", folderSprite[0], new Rect(Screen.width * 0.075f, Screen.height * 0.275f, Screen.width * 0.08f, Screen.width * 0.08f), funcList, "InnerLabel");
		openButton.name = "OpenButton";
		
		// Make the open button label.
		GameObject openButtonLabel = MakeLabel(localCanvas, MCP.Text (3101)/*"OPEN"*/, new Rect(Screen.width * 0.065f, Screen.height * 0.41f, Screen.width * 0.1f, Screen.height * 0.06f), TextAnchor.MiddleCenter);
		openButtonLabel.name = "OpenButtonLabel";
		
		openButton.GetComponent<Button>().navigation = nav;
		// Set the button transition to sprite swap.
		openButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		spriteState = new SpriteState();
		openButton.GetComponent<Button>().targetGraphic = openButton.GetComponent<Image>();
		// Set the highlighted sprite.
		spriteState.highlightedSprite = folderSprite[1];
		spriteState.pressedSprite = folderSprite[1];
		openButton.GetComponent<Button>().spriteState = spriteState;
		
		// Make the title bar.
		mediaTitleObject = MakeLabel(localCanvas, "", new Rect(Screen.width * 0.2f, Screen.height * 0.4f, Screen.width * 0.6f, Screen.height * 0.2f));
		mediaTitleObject.name = "MediaTitleObject";
		
		// Make the play panel to cover the AV panel.
		funcList = new List<MyVoidFunc>
		{
			ControlPlay
		};
		playPanel = MakeButton (localCanvas, "", new Rect(Screen.width * 0.2f, Screen.height * 0.275f, Screen.width * 0.6f, Screen.height * 0.5f), funcList);
		playPanel.name = "playPanel";
		playPanel.SetActive (false);
		
		// Make text to show that no media is loaded.
		noMediaSelectedText = MakeLabel (localCanvas, "No media selected", new Rect(Screen.width * 0.3f, Screen.height * 0.45f, Screen.width * 0.4f, Screen.height * 0.2f));
		noMediaSelectedText.name = "NoMediaSelectedText";
		
#if !UNITY_ANDROID && !UNITY_IPHONE
		// Make open file button.
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				ToggleAVFullscreen();
			}
		};
		fullscreenButton = MakeButton (localCanvas, "", fullscreenSprite[0], new Rect(Screen.width * 0.075f, Screen.height * 0.64f, Screen.width * 0.08f, Screen.width * 0.08f), funcList, "InnerLabel");
		fullscreenButton.name = "FullscreenButton";
		// Set the button transition to sprite swap.
		fullscreenButton.GetComponent<Button>().navigation = nav;
		
		if(videoClip == null)
		{
			fullscreenButton.GetComponent<Button>().interactable = false;
		}
		
		// Make the full screen button label.
		fullscreenButtonLabel = MakeLabel(localCanvas, MCP.Text (3102)/*"FULL\nSCREEN"*/, new Rect(Screen.width * 0.04f, Screen.height * 0.775f, Screen.width * 0.15f, Screen.height * 0.1f), TextAnchor.MiddleCenter);
		fullscreenButtonLabel.name = "FullscreenButtonLabel";
		// Set the button transition to sprite swap.
		fullscreenButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		spriteState = new SpriteState();
		fullscreenButton.GetComponent<Button>().targetGraphic = fullscreenButton.GetComponent<Image>();
		spriteState.highlightedSprite = fullscreenSprite[1];
		spriteState.pressedSprite = fullscreenSprite[1];
		fullscreenButton.GetComponent<Button>().spriteState = spriteState;
		
		// Make the full screen canvas.
		fullscreenCanvas = MakeCanvas (true);
		
		// Make exit full screen button
		exitFullscreenButton = MakeButton (fullscreenCanvas, "", exitFullscreenSprite[0], new Rect(Screen.width * 0.02f, Screen.height * 0.02f, Screen.width * 0.08f, Screen.width * 0.08f), funcList, "InnerLabel");
		exitFullscreenButton.name = "ExitFullscreenButton";
		
		// Make the exit fullscreen button label.
		exitFullscreenButtonLabel = MakeLabel(fullscreenCanvas, "EXIT", new Rect(Screen.width * 0.02f, Screen.height * 0.15f, Screen.width * 0.08f, Screen.height * 0.1f), TextAnchor.MiddleCenter);
		exitFullscreenButtonLabel.name = "ExitFullscreenButtonLabel";
		exitFullscreenButtonLabel.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		// Set the button transition to sprite swap.
		exitFullscreenButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		spriteState = new SpriteState();
		exitFullscreenButton.GetComponent<Button>().targetGraphic = exitFullscreenButton.GetComponent<Image>();
		spriteState.highlightedSprite = exitFullscreenSprite[1];
		spriteState.pressedSprite = exitFullscreenSprite[1];
		exitFullscreenButton.GetComponent<Button>().spriteState = spriteState;
		
		// Make play button
		funcList = new List<MyVoidFunc>
		{
			ControlPlay
		};
		fsPlayButtonObject = MakeButton (fullscreenCanvas, "", playSprite[0], new Rect(Screen.width * 0.45f, (Screen.height * 0.5f) - (Screen.width * 0.05f), Screen.width * 0.1f, Screen.width * 0.1f), funcList);
		fsPlayButtonObject.name = "PlayButton";
		fsPlayButtonObject.GetComponent<Button>().navigation = nav;
		// Set the button transition to sprite swap.
		fsPlayButtonObject.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		spriteState = new SpriteState();
		fsPlayButtonObject.GetComponent<Button>().targetGraphic = fsPlayButtonObject.transform.GetChild (0).gameObject.GetComponent<Image>();
		spriteState.highlightedSprite = playSprite[1];
		spriteState.pressedSprite = playSprite[1];
		fsPlayButtonObject.GetComponent<Button>().spriteState = spriteState;
		
		// Make progress background
		fsAudioPosBackground = MakeImage (fullscreenCanvas, new Rect(0, Screen.height * 0.8875f, Screen.width * 0.6f, Screen.height * 0.1125f), menuBackground);
		audioPosBackground.name = "AudioPosBackground";
		
		// Make current progress label
		fsCurrentTimeLabel = MakeLabel (fullscreenCanvas, "00:00:00", new Rect(Screen.width * 0.005f, Screen.height * 0.8875f, Screen.width * 0.1f, Screen.height * 0.1f), "SmallText");
		fsCurrentTimeLabel.name = "fsCurrentTimeLabel";
		fsCurrentTimeLabel.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
		fsCurrentTimeLabel.GetComponent<Text>().resizeTextForBestFit = false;
		fsCurrentTimeLabel.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		
		// Make current progress slider
		fsAudioPositionSliderObject = MakeSliderHorizontal (fullscreenCanvas, new Rect(Screen.width * 0.1f, Screen.height * 0.45f, Screen.width * 0.4f, Screen.height * 0.025f));
		fsAudioPositionSliderObject.name = "fsAudioPositionSliderObject";
		fsAudioPositionSliderObject.GetComponent<Slider>().maxValue = 100f;
		fsAudioPositionSliderObject.GetComponent<Slider>().minValue = 0f;
		
		if(waveformSource != null && waveformSource.clip != null)
		{
			fsAudioPositionSliderObject.GetComponent<Slider>().value = timePlaying * 100 / waveformSource.clip.length;
		}
		else
		{
			fsAudioPositionSliderObject.GetComponent<Slider>().value = 0;
		}
		
		// Make total progress label
		fsTotalTimeLabel = MakeLabel (fullscreenCanvas, "00:00:00", new Rect(Screen.width * 0.5125f, Screen.height * 0.8875f, Screen.width * 0.1f, Screen.height * 0.1f), "SmallText");
		fsTotalTimeLabel.name = "fsTotalTimeLabel";
		fsTotalTimeLabel.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
		fsTotalTimeLabel.GetComponent<Text>().resizeTextForBestFit = false;
		fsTotalTimeLabel.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		
		// Make volume/mute button
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				ControlMute();
				volumeSliderObject.GetComponent<Slider>().interactable = !mute;
				fsVolumeSliderObject.GetComponent<Slider>().interactable = !mute;
			}
		};
		fsMuteButtonObject = MakeButton (fullscreenCanvas, "", unmuteSprite, new Rect(Screen.width * 0.65f, Screen.height * 0.905f, Screen.width * 0.04f, Screen.width * 0.04f), funcList);
		fsMuteButtonObject.name = "fsMuteButtonObject";
		
		// Make volume slider
		fsVolumeSliderObject = MakeSliderHorizontal (fullscreenCanvas, new Rect(Screen.width * 0.7f, Screen.height * 0.45f, Screen.width * 0.25f, Screen.height * 0.025f));
		fsVolumeSliderObject.name = "fsVolumeSliderObject";
		fsVolumeSliderObject.GetComponent<Slider>().maxValue = 20f;
		fsVolumeSliderObject.GetComponent<Slider>().minValue = 0f;
		fsVolumeSliderObject.GetComponent<Slider>().value = 10f;
		fsVolumeSliderObject.GetComponent<Slider>().wholeNumbers = true;
		
		// Make the full screen play panel.
		funcList = new List<MyVoidFunc>
		{
			ControlPlay
		};
		fsPlayPanel = MakeButton (fullscreenCanvas, "", new Rect(0, 0, Screen.width, Screen.height), funcList);
		fsPlayPanel.name = "playPanel";
		fsPlayPanel.SetActive (false);
		
		fullscreenCanvas.SetActive (false);
#endif
	}
	
	/// <summary>
	/// Control stopping the media.
	/// </summary>
	void ControlStop()
	{
		// Stop media and return to the beginning.
		waveformSource.Stop();
		
#if !UNITY_ANDROID && !UNITY_IPHONE
		// If there is a video clip playing.
		if(videoClip != null)
		{
			// Stop the video.
			videoClip.Stop ();
		}
#endif
		
		stopped = true;
		playing = false;
		// Change the play button sprite to play.
		playButtonObject.GetComponent<Image>().sprite = playSprite[0];
		
#if !UNITY_ANDROID && !UNITY_IPHONE
		// Add transparency to object to see the play button through it.
		Color c = waveformObject.GetComponent<GUITexture>().color;
//		c.a = 64f / 255f;
		c.a = 32f / 255f;
		waveformObject.GetComponent<GUITexture>().color = c;
#endif

		timePlaying = 0;
	}
	
	/// <summary>
	/// Control rewinding the media.
	/// </summary>
	void ControlRewind()
	{
#if !UNITY_ANDROID && !UNITY_IPHONE
		// Make the media rewind.
		waveformSource.Pause ();
#endif

		playing = true;
		rewinding = !rewinding;
		// Change the play button sprite to play.
		playButtonObject.transform.GetChild (0).GetComponent<Image>().sprite = playSprite[0];
	}
	
	/// <summary>
	/// Controls playing the media.
	/// </summary>
	void ControlPlay()
	{
		// Check if the media is currently playing.
		playing = !playing;
		
		// If it is playing, pause it.
		if(playing)
		{
			// Play
			waveformSource.Play ();
			stopped = false;
		}
		// Otherwise, make it play.
		else
		{
			// Pause
			waveformSource.Pause ();
		}
#if !UNITY_ANDROID && !UNITY_IPHONE
		
		// Remove transparency.
		Color c = waveformObject.GetComponent<GUITexture>().color;
		//c.a = 64f / 255f;
		c.a = 32f / 255f;
		waveformObject.GetComponent<GUITexture>().color = c;
#endif
		
#if !UNITY_ANDROID && !UNITY_IPHONE
		// If the video clip is not null...
		if(videoClip != null)
		{
			// If it is playing...
			if(videoClip.isPlaying)
			{
				// Pause it
				videoClip.Pause();
				// Play the clip.
				waveformSource.Pause();
				stopped = true;
				playing = false;
			}
			// Otherwise...
			else
			{
				// Unpause it.
				videoClip.Play();
				// Set the waveform's clip.
				waveformSource.clip = videoClip.audioClip;
				// Play the clip.
				waveformSource.Play();
				stopped = false;
				playing = true;
			}
		}
#else
		// Move behind the GUI when paused.
		if(playing)
		{
			scrMedia.Play ();
		}
		else
		{
			scrMedia.Pause();
		}
#endif
		
		// Reset other flags.
		fastforwarding = false;
		rewinding = false;
	}
	
	/// <summary>
	/// Controls fast forwarding the media.
	/// </summary>
	void ControlFastForward()
	{
#if !UNITY_ANDROID && !UNITY_IPHONE
		// Make the media fast forward.
		waveformSource.Pause ();
#endif
		
		playing = true;
		fastforwarding = !fastforwarding;
		// Change the play button sprite to play.
		playButtonObject.transform.GetChild (0).GetComponent<Image>().sprite = playSprite[0];
	}
	
	/// <summary>
	/// Controls muting the audio.
	/// </summary>
	void ControlMute()
	{
		mute = !mute;
		
		waveformSource.mute = mute;
		
		// If mute is now on.
		if(mute)
		{	
			// Mute sprite
			muteButtonObject.GetComponent<Image>().sprite = muteSprite;
#if !UNITY_ANDROID && !UNITY_IPHONE
			fsMuteButtonObject.GetComponent<Image>().sprite = muteSprite;
#endif
		}
		// Otherwise...
		else
		{
			// Volume sprite
			muteButtonObject.GetComponent<Image>().sprite = unmuteSprite;
#if !UNITY_ANDROID && !UNITY_IPHONE
			fsMuteButtonObject.GetComponent<Image>().sprite = unmuteSprite;
#endif
		}
	}
	
	/// <summary>
	/// Opens the file browser.
	/// </summary>
	void OpenFileBrowser()
	{
		waveformSource.Stop ();
		
#if UNITY_ANDROID || UNITY_IPHONE
		if(hs_get != null)
		{
			hs_get.Dispose ();
			hs_get = null;
		}
		
		videoManagerObject.transform.localScale = new Vector3(9f, 9f, 9f);
		scrMedia.Stop ();
		scrMedia.UnLoad();
		m_bFinish = true;
		videoManagerObject.SetActive (false);
#endif

		if(MCP.mediaDownload != null)
		{
			MCP.mediaDownload.Dispose ();
			MCP.mediaDownload = null;
		}
		
		fileLoading = false;
		Destroy (downloadingPopup);
		
		// Reset the waveform time.
		waveformSource.time = 0;
		timePlaying = 0;
		
		localCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
		GameObject camObj = GameObject.FindGameObjectWithTag ("MainCamera");
		
		MCP.audioFilePath = "";
		MCP.videoFilePath = "";
		
		// If loading from an asset pack...
		if(currentAssetPack != "")
		{
			// Add the simulated file browser script to the camera object.
			camObj.AddComponent<SimulateFileBrowser> ();
			camObj.GetComponent<SimulateFileBrowser> ().sceneCanvas = localCanvas;
			
#if UNITY_ANDROID || UNITY_IPHONE
			camObj.GetComponent<SimulateFileBrowser> ().allowedExtensions = new string[]{"xm", "it", "wav", "mp3", "wmv", "mp4"};
			camObj.GetComponent<SimulateFileBrowser> ().assetPack = currentAssetPack;
#else
			camObj.GetComponent<SimulateFileBrowser> ().allowedExtensions = new string[]{"xm", "it", "wav", "mp3", "ogg"};
			camObj.GetComponent<SimulateFileBrowser> ().assetPack = currentAssetPack;
#endif
		}
		// Otherwise...
		else
		{
			// Add the file browser script to the camera object.
			camObj.AddComponent<FileBrowser> ();
			
			// Set current file path to the app data path.
			if(!System.IO.Directory.Exists (Application.persistentDataPath + "/audio"))
			{
				System.IO.Directory.CreateDirectory (Application.persistentDataPath + "/audio");
			}
			
			// Reset the first frame timer.
			MenuScreen.firstFrameTimer = 0;
			
			// Set the file browser variables.
			camObj.GetComponent<FileBrowser> ().currentFilePath = openFolderPath;
			camObj.GetComponent<FileBrowser> ().upPathLimit = openFolderPath;
			camObj.GetComponent<FileBrowser> ().sceneCanvas = localCanvas;
#if UNITY_ANDROID || UNITY_IPHONE
			camObj.GetComponent<FileBrowser> ().allowedExtensions = new string[]{"xm", "it", "wav", "mp3", "wmv", "mp4"};
#else
			camObj.GetComponent<FileBrowser> ().allowedExtensions = new string[]{"xm", "it", "wav", "mp3", "ogg"};
#endif
		}
		
#if !UNITY_ANDROID && !UNITY_IPHONE
		waveformObject.SetActive(false);
#endif
	}
	
	/// <summary>
	/// Fades the GUI elements.
	/// </summary>
	/// <returns>The GUI elements.</returns>
	/// <param name="alpha">Alpha.</param>
	private float FadeGUIElements(float alpha)
	{
		alpha *= 255f;
		
		if(alpha < 255f)
		{
			alpha += Time.deltaTime * 50f;
		}
		else
		{
			alpha = 255f;
		}
		
		return alpha / 255f;
	}
	
	/// <summary>
	/// Enables the fullscreen GUI elements.
	/// </summary>
	private void EnableFSGUIElements()
	{
#if !UNITY_ANDROID && !UNITY_IPHONE
		fsVolumeSliderObject.SetActive (true);
		fsMuteButtonObject.SetActive (true);
		fsTotalTimeLabel.SetActive (true);
		fsAudioPositionSliderObject.SetActive(true);
		fsCurrentTimeLabel.SetActive (true);
		fsAudioPosBackground.SetActive (true);
		fsPlayButtonObject.SetActive (true);
		exitFullscreenButtonLabel.SetActive (true);
		exitFullscreenButton.SetActive (true);
#endif
	}
	
	/// <summary>
	/// Disables the fullscreen GUI elements.
	/// </summary>
	private void DisableFSGUIElements()
	{
#if !UNITY_ANDROID && !UNITY_IPHONE
		fsVolumeSliderObject.SetActive (false);
		fsMuteButtonObject.SetActive (false);
		fsTotalTimeLabel.SetActive (false);
		fsAudioPositionSliderObject.SetActive(false);
		fsCurrentTimeLabel.SetActive (false);
		fsAudioPosBackground.SetActive (false);
		fsPlayButtonObject.SetActive (false);
		exitFullscreenButtonLabel.SetActive (false);
		exitFullscreenButton.SetActive (false);
#endif
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update()
	{
		if(screenSize.width == 0 || screenSize.height == 0 || screenSize.width != Screen.width || screenSize.height != Screen.height)
		{
			screenSize = new Rect(0, 0, Screen.width, Screen.height);
			
			Init ();
			
			if(localCanvas != null)
			{
				Destroy (localCanvas);
			}
			
#if !UNITY_ANDROID && !UNITY_IOS
			if(fullscreenCanvas != null)
			{
				Destroy (fullscreenCanvas);
			}
#endif
			
			CreateGUI ();
			
			localCanvas.transform.localScale = Vector2.zero;
			
			if(fullscreen)
			{
				waveformObject.GetComponent<GUITexture>().pixelInset = new Rect(0, 0, 0, 0);
			}
			else
			{
				waveformObject.GetComponent<GUITexture>().pixelInset = new Rect(screenSize.width * 0.71f, screenSize.height * 0.8f, screenSize.width * -0.42f, screenSize.height * -0.54f);
			}
			
			// Make sure values persist.
			volumeSliderObject.GetComponent<Slider>().interactable = !mute;
			volumeSliderObject.GetComponent<Slider>().value = volume * 0.2f;
			
#if !UNITY_ANDROID && !UNITY_IOS
			fsVolumeSliderObject.GetComponent<Slider>().interactable = !mute;
			fsVolumeSliderObject.GetComponent<Slider>().value = volume * 0.2f;
#endif
			
			// If mute is now on.
			if(mute)
			{	
				// Mute sprite
				muteButtonObject.GetComponent<Image>().sprite = muteSprite;
#if !UNITY_ANDROID && !UNITY_IPHONE
				fsMuteButtonObject.GetComponent<Image>().sprite = muteSprite;
#endif
			}
			// Otherwise...
			else
			{
				// Volume sprite
				muteButtonObject.GetComponent<Image>().sprite = unmuteSprite;
#if !UNITY_ANDROID && !UNITY_IPHONE
				fsMuteButtonObject.GetComponent<Image>().sprite = unmuteSprite;
#endif
			}
			
			//string currentTime = "";
			System.TimeSpan currentTime = new System.TimeSpan(0, 0, (int)timePlaying);
			currentTimeLabel.GetComponent<Text>().text = currentTime.ToString ();
#if !UNITY_ANDROID && !UNITY_IPHONE
			fsCurrentTimeLabel.GetComponent<Text>().text = currentTime.ToString ();
#endif
			if(waveformSource != null && waveformSource.clip != null)
			{
				System.TimeSpan totalTime = new System.TimeSpan(0, 0, (int)waveformSource.clip.length);
				//string currentTime = "";
				totalTimeLabel.GetComponent<Text>().text = totalTime.ToString ();
#if !UNITY_ANDROID && !UNITY_IPHONE
				fsTotalTimeLabel.GetComponent<Text>().text = totalTime.ToString ();
#endif
			}
		}
		
#if !UNITY_ANDROID && !UNITY_IOS
		// If there is no media selected, show the no media selected text.
		if(waveformSource.clip == null)
		{
			noMediaSelectedText.SetActive (true);
		}
		else
		{
			noMediaSelectedText.SetActive (false);
		}
#endif

		// Make sure the fullscreen is consistant.
		if(fullscreen && !Screen.fullScreen && MenuScreen.firstFrameTimer > 0.1f)
		{
			if(!Application.isEditor)
			{
				ToggleAVFullscreen();
			}
		}
		
#if !UNITY_ANDROID && !UNITY_IPHONE
		if(GetComponent<SimulateFileBrowser>() == null)
		{
			if(fullscreen)
			{
				fullscreenCanvas.SetActive (true);
				localCanvas.SetActive (false);
			}
			else
			{
				localCanvas.SetActive (true);
				fullscreenCanvas.SetActive (false);
			}
		}
#endif
	
		if(!fileLoading)
		{
			// Update the volume from the slider. Keep the slider moving in steps of 5.
			if(fullscreen)
			{
#if !UNITY_ANDROID && !UNITY_IPHONE
				volume = fsVolumeSliderObject.GetComponent<Slider>().value * 5f;
#endif
			}
			else
			{
				volume = volumeSliderObject.GetComponent<Slider>().value * 5f;
			}
			
			waveformSource.volume = volume / 100f;
			
			// Update media progression slider.
			if(waveformSource.clip != null)
			{
				if(fullscreen)
				{
#if !UNITY_ANDROID && !UNITY_IPHONE
					if(prevSliderPos == fsAudioPositionSliderObject.GetComponent<Slider>().value)
					{
						fsAudioPositionSliderObject.GetComponent<Slider>().value = 100f - ((waveformSource.clip.length - timePlaying) / waveformSource.clip.length * 100f);
					}
					else
					{
						if(fsAudioPositionSliderObject.GetComponent<Slider>().value * 0.01f * waveformSource.clip.length < waveformSource.clip.length)
						{
							timePlaying = fsAudioPositionSliderObject.GetComponent<Slider>().value * 0.01f * waveformSource.clip.length;
							waveformSource.time = fsAudioPositionSliderObject.GetComponent<Slider>().value * 0.01f * waveformSource.clip.length;
						}
						else
						{
							timePlaying = waveformSource.clip.length;
							waveformSource.time = waveformSource.clip.length;
						}
					}
					
					prevSliderPos = fsAudioPositionSliderObject.GetComponent<Slider>().value;
#endif
				}
				else
				{
					// Make the slider position update as the media plays.
					if(prevSliderPos == audioPositionSliderObject.GetComponent<Slider>().value)
					{
						audioPositionSliderObject.GetComponent<Slider>().value = 100f - ((waveformSource.clip.length - timePlaying) / waveformSource.clip.length * 100f);
					}
					else
					{
						if(audioPositionSliderObject.GetComponent<Slider>().value * 0.01f * waveformSource.clip.length < waveformSource.clip.length)
						{
							timePlaying = audioPositionSliderObject.GetComponent<Slider>().value * 0.01f * waveformSource.clip.length;
							waveformSource.time = audioPositionSliderObject.GetComponent<Slider>().value * 0.01f * waveformSource.clip.length;
						}
						else
						{
							timePlaying = waveformSource.clip.length;
							waveformSource.time = waveformSource.clip.length;
						}
					}
					
					// Store the slider position to allow the user to click on the slider to change the position of the media.
					prevSliderPos = audioPositionSliderObject.GetComponent<Slider>().value;
				}
			}
			
			// Update the time playing when the audio is playing.
			if(playing && !stopped && GetComponent<SimulateFileBrowser>() == null)
			{
				timePlaying += Time.deltaTime;
			}
			
			// Check for when the clip is finished.
			if ( !playedMedia && waveformSource.clip != null)
			{
				if(timePlaying >= waveformSource.clip.length && waveformSource.clip.length > -1)
				{
					// When it is finished, handle stopping properly.
					ControlStop ();
				}
			}
			else
			{
				// If the audio clip is not null and is not playing...
				if(audioClip != null && !waveformSource.isPlaying)
				{
					// Set the waveform's clip.
					waveformSource.clip = audioClip;
					// Play the clip.
					waveformSource.Play();
					// Flag the clip as playing.
					playing = true;
					stopped = false;
					timePlaying = 0;
				}
			}
		}
		
		if(fileLoading)
		{
			if(downloadingPopup == null)
			{
				List<MyVoidFunc> funcList = new List<MyVoidFunc>
				{
					delegate
					{
						fileLoading = false;
						fileLoaded = true;
						
						if(MCP.mediaDownload != null)
						{
							MCP.mediaDownload.Dispose ();
							MCP.mediaDownload = null;
						}
						
						MCP.loadedAudio = null;
#if !UNITY_ANDROID && !UNITY_IOS
						MCP.loadedVideo = null;
#else
						scrMedia.UnLoad ();
#endif
						Destroy (downloadingPopup);
					}
				};
				downloadingPopup = MakePopup(localCanvas, defaultPopUpWindow, MCP.Text (1908)/*"Downloading..."*/, "", MCP.Text (207)/*"Cancel"*/, funcList);
				
				Vector3 newPos = downloadingPopup.transform.GetChild (2).localPosition;
				newPos.y -= (Screen.height * 0.03f);
				downloadingPopup.transform.GetChild (2).localPosition = newPos;
				
				downloadingSpinner = MakeImage (localCanvas, new Rect(Screen.width * 0.5f, Screen.height * 0.54f, Screen.height * 0.075f, Screen.height * 0.075f), spinnerSprite, false);
				downloadingSpinner.name = "LoadingSpinner";
				downloadingSpinner.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
				downloadingSpinner.transform.SetParent (downloadingPopup.transform);
				waveformObject.GetComponent<GUITexture>().enabled = false;
			}
			else
			{
				// Spin the downloading spinner
				spinnerRot -= Time.deltaTime * 75f;
				downloadingSpinner.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, spinnerRot);
				
				Vector3 newPos = downloadingPopup.transform.GetChild (1).gameObject.GetComponent<RectTransform>().localPosition;
				newPos.y = Screen.height * 0.125f;
				downloadingPopup.transform.GetChild (1).gameObject.GetComponent<RectTransform>().localPosition = newPos;
				
				Vector2 newScale = downloadingPopup.transform.GetChild (1).gameObject.GetComponent<RectTransform>().sizeDelta;
				newScale.y = Screen.height * 0.325f;
				downloadingPopup.transform.GetChild (1).gameObject.GetComponent<RectTransform>().sizeDelta = newScale;
				
#if UNITY_ANDROID|| UNITY_IOS
				if(hs_get != null)
				{
					downloadingPopup.transform.GetChild (1).gameObject.GetComponent<Text>().text = (hs_get.progress * 100f).ToString ("f2") + "%";
				}
				#endif

				if(MCP.mediaDownload != null)

//#if !UNITY_IOS
				{
					if(MCP.mediaDownload.error != null)
					{	
						MCP.mediaDownload.Dispose ();
						MCP.mediaDownload = null;
						
						MCP.loadedAudio = null;
#if !UNITY_ANDROID && !UNITY_IOS
						MCP.loadedVideo = null;
#endif
						fileLoading = false;
						fileLoaded = false;
						
						Destroy(downloadingPopup);

						if(Debug.isDebugBuild)
						{
                            string error = string.Empty;

                            if(MCP.mediaDownload != null)
                            {
                                error = MCP.mediaDownload.error;
                            }

							downloadingPopup = MakePopup (localCanvas, defaultPopUpWindow, MCP.Text (1908)/*"Downloading..."*/, MCP.Text (1909)/*"Download Failed"*/ + error, MCP.Text (205)/*"Dismiss"*/, CommonTasks.DestroyParent);
						}
						else
						{
							downloadingPopup = MakePopup (localCanvas, defaultPopUpWindow, MCP.Text (1908)/*"Downloading..."*/, MCP.Text (1909)/*"Download Failed"*/, MCP.Text (205)/*"Dismiss"*/, CommonTasks.DestroyParent);
						}
					}
					else
					{
						string warningString = MCP.Text (3104);/*"If the download percentage does not change after 2 minutes, please cancel the download and try again*";*/
						downloadingPopup.transform.GetChild (1).gameObject.GetComponent<Text>().text = warningString + "\n" + (MCP.mediaDownload.progress * 100f).ToString ("f2") + "%";
					}
				}
#if UNITY_ANDROID || UNITY_IOS
				else if(scrMedia.GetCurrentState () == MediaPlayerCtrl.MEDIAPLAYER_STATE.ERROR)
				{
					MCP.loadedAudio = null;
					fileLoading = false;
					fileLoaded = false;
					
					Destroy(downloadingPopup);
					downloadingPopup = MakePopup (localCanvas, defaultPopUpWindow, MCP.Text (1908)/*"Downloading..."*/, MCP.Text (1909)/*"Download Failed"*/, MCP.Text (205)/*"Dismiss"*/, CommonTasks.DestroyParent);
				}
//#endif
#endif
			}
		
			if(MCP.loadedAudio != null)
			{
				filePathStore = MCP.loadedFilePath;
				fileLoading = false;
				fileLoaded = true;
				spinnerRot = 0;
				timePlaying = 0;
				waveformObject.GetComponent<GUITexture>().enabled = true;
				Destroy (downloadingPopup);
			}
			
#if !UNITY_ANDROID && !UNITY_IOS
			if(MCP.loadedVideo != null)
			{
				fileLoading = false;
				fileLoaded = true;
				spinnerRot = 0;
				timePlaying = 0;
				Destroy (downloadingPopup);
			}
#else
			// Otherwise, if the media is video...
			else if(MCP.videoFilePath != null && MCP.videoFilePath != "")
			{
				fileLoaded = false;
				
				//TODO: Make the cancel button work on video.
				// Tell the device to play the movie.
				//StartCoroutine (PlayHandheldMovie(MCP.videoFilePath));
				StartCoroutine (PlayDeviceMovie(MCP.videoFilePath));
				
				// Store the filepath for replaying.
				filePathStore = MCP.videoFilePath;
				
				// Reset the filepath to stop auto-replaying.
				MCP.videoFilePath = "";
				timePlaying = 0;
				noMediaSelectedText.SetActive (false);
				mediaTitleObject.SetActive (true);
			}

			if(!m_bFinish && audioClip == null)
			{
				videoManagerObject.SetActive (true);
				
				if(scrMedia.GetCurrentState () != MediaPlayerCtrl.MEDIAPLAYER_STATE.PAUSED && scrMedia.GetCurrentState () != MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING)
				{
					videoManagerObject.GetComponent<MeshRenderer>().enabled = false;
				}
				else
				{
					if(downloadingPopup != null)
					{
						fileLoading = false;
						fileLoaded = true;
						Destroy (downloadingPopup);
						
						// Force an initial pause and play to stop it restarting after the first pause.
						scrMedia.Pause();
						playing = false;
						ControlPlay ();
					}
				}
			}
#endif
		}
		
#if UNITY_ANDROID || UNITY_IOS
		if(scrMedia.GetCurrentState () == MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING)
		{
			videoManagerObject.GetComponent<MeshRenderer>().enabled = true;
			// Resize the video object
			videoManagerObject.transform.localScale = new Vector3(10.5f, 5.25f, 1);
			videoManagerObject.transform.localPosition = new Vector3(0, -0.25f, 9.9f);
		}
		else if(scrMedia.GetCurrentState () == MediaPlayerCtrl.MEDIAPLAYER_STATE.PAUSED)
		{
			videoManagerObject.transform.localPosition = new Vector3(0, -0.25f, 10.1f);
		}
#endif
		
		if(fileLoaded)
		{
#if UNITY_ANDROID || UNITY_IPHONE
			if(m_bFinish)
			{
				scrMedia.Stop ();
				videoManagerObject.SetActive (false);
			}
			
			mediaTitleObject.GetComponent<Text>().text = MCP.mediaName;
			
			if(scrMedia.GetCurrentState () == MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING)
			{
				mediaTitleObject.SetActive (false);
			}
			else if(scrMedia.GetCurrentState () == MediaPlayerCtrl.MEDIAPLAYER_STATE.PAUSED)
			{
				mediaTitleObject.SetActive (true);
			}
#endif
			
			// If the media is audio...
			if(MCP.loadedAudio != null)
			{
				fileLoaded = false;
				// Set the audio clip to the loaded audio.
				audioClip = MCP.loadedAudio;
				// Reset the audio path.
				MCP.audioFilePath = "";
				
				waveformObject.SetActive (true);
				noMediaSelectedText.SetActive (false);
				
#if !UNITY_ANDROID && !UNITY_IPHONE
				// Activate the fullscreen button.
				fullscreenButton.GetComponent<Button>().interactable = false;
				// Return button to color transition to allow it to fade on disable.
				fullscreenButton.GetComponent<Button>().transition = Selectable.Transition.ColorTint;
#endif
			}
			
#if !UNITY_ANDROID && !UNITY_IPHONE
			// Otherwise, if the media is video...
			else if(MCP.loadedVideo != null)
			{
				fileLoaded = false;
				// Set the video clip to the loaded video.
				videoClip = MCP.loadedVideo;
				// Reset the video path
				MCP.videoFilePath = "";
				// Set the title of the screen to the name of the media,
//				mediaTitleObject.GetComponent<Text>().text = MCP.mediaName;
				// Set the texture of the waveform to the video clip.
				waveformObject.GetComponent<GUITexture>().texture = videoClip;
				videoClip.Stop ();
				// Set the audio clip.
				waveformSource.clip = videoClip.audioClip;
				waveformSource.Stop ();
				// Play the clip.
//				waveformSource.Play();
				// Activate the fullscreen button.
				fullscreenButton.GetComponent<Button>().interactable = true;
				// Return button to sprite transition to allow it to fade on disable.
				fullscreenButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
			}
#endif
		}
		
#if !UNITY_ANDROID && !UNITY_IPHONE
		// If media is playing, disable the play button.
		if(playing || (audioClip == null && videoClip == null))
		{
			playButtonObject.SetActive (false);
		}
#else
		// If media is playing, disable the play button.
		if(playing || (audioClip == null && filePathStore == ""))
		{
			playButtonObject.SetActive (false);
		}
#endif
		else
		{
			playButtonObject.SetActive (true);
		}
		
		// Update the playing time labels
		if(audioClip != null)
		{
			// Move the progress bar to the centre.
			SetProgressBarPosition(true);
			
			// Enable the progress slider.
			audioPositionSliderObject.GetComponent<Slider>().interactable = true;
#if !UNITY_ANDROID && !UNITY_IPHONE
			fsAudioPositionSliderObject.GetComponent<Slider>().interactable = true;
#endif
			
			System.TimeSpan currentTime = new System.TimeSpan(0, 0, (int)timePlaying);
			//string currentTime = "";
			currentTimeLabel.GetComponent<Text>().text = currentTime.ToString ();
#if !UNITY_ANDROID && !UNITY_IPHONE
			fsCurrentTimeLabel.GetComponent<Text>().text = currentTime.ToString ();
#endif
			System.TimeSpan totalTime = new System.TimeSpan(0, 0, (int)audioClip.length);
			//string currentTime = "";
			totalTimeLabel.GetComponent<Text>().text = totalTime.ToString ();
#if !UNITY_ANDROID && !UNITY_IPHONE
			fsTotalTimeLabel.GetComponent<Text>().text = totalTime.ToString ();
#endif
		} 
#if UNITY_ANDROID || UNITY_IPHONE
		else if(scrMedia.GetCurrentState () == MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING || scrMedia.GetCurrentState () == MediaPlayerCtrl.MEDIAPLAYER_STATE.PAUSED)
		{
			audioPositionSliderObject.GetComponent<Slider>().interactable = false;
			
			System.TimeSpan currentTime = new System.TimeSpan((long)scrMedia.GetSeekPosition () * 10000);
			currentTimeLabel.GetComponent<Text>().text = (currentTime.ToString ()).Split (new char[]{'.'}, System.StringSplitOptions.None)[0];
		
			System.TimeSpan totalTime = new System.TimeSpan((long)scrMedia.GetDuration () * 10000);
			totalTimeLabel.GetComponent<Text>().text = (totalTime.ToString ()).Split (new char[]{'.'}, System.StringSplitOptions.None)[0];
		}
#endif
		
		playPanel.SetActive (true);
		
#if !UNITY_ANDROID && !UNITY_IPHONE
		// If the media is a video clip...
		if(videoClip != null)
		{
			if(videoClip.isReadyToPlay && initialPlay)
			{
				initialPlay = false;
				ControlPlay ();
			}
			
			// Make sure the progress bar is in the right place.
			SetProgressBarPosition(false);
			
			// Disable the progress slider as there is no functionality for jumping to different parts of the video.
			audioPositionSliderObject.GetComponent<Slider>().interactable = false;
			fsAudioPositionSliderObject.GetComponent<Slider>().interactable = false;
			
			// Set the current time label text.
			System.TimeSpan currentTime = new System.TimeSpan(0, 0, (int)timePlaying);
			currentTimeLabel.GetComponent<Text>().text = currentTime.ToString ();
			fsCurrentTimeLabel.GetComponent<Text>().text = currentTime.ToString ();
			
			// If the clip is playing...
			if(videoClip.isPlaying)
			{
				// Set the total time label text.
				System.TimeSpan totalTime = new System.TimeSpan(0, 0, (int)videoClip.duration);
				totalTimeLabel.GetComponent<Text>().text = totalTime.ToString ();
				fsTotalTimeLabel.GetComponent<Text>().text = totalTime.ToString ();
			}
			
			// Check for when the clip is finished.
			if(timePlaying >= videoClip.duration && videoClip.isPlaying)
			{
				// When it is finished, handle stopping properly.
				ControlStop ();
			}
		}
		
		// If fullscreen is being used...
		if(fullscreen)
		{
#if !UNITY_ANDROID && !UNITY_IOS
			// Close when esc is pressed
			if(Input.GetKeyDown (KeyCode.Escape))
			{
				ToggleAVFullscreen();
			}
#endif
			
			// If the media is currently playing...
			if(playing)
			{
				// If playing then fade out with a timer. Increase alpha of waveforeObject for easy fade.
				Color col = waveformObject.GetComponent<GUITexture>().color;
				col.a = FadeGUIElements (col.a);
				waveformObject.GetComponent<GUITexture>().color = col;
				
				// If the alpha is high enough, then disable the gui except the pause.
				if(col.a > 80f / 255f)
				{
					// Enable the invis play button
					fsPlayPanel.SetActive (true);
				}
				if(col.a > 230f / 255f)
				{
					DisableFSGUIElements();
				}
			}
			// Otherwise...
			else
			{
				// If paused then set make the waveform seethrough for GUI elements.
				Color col = waveformObject.GetComponent<GUITexture>().color;
				//col.a = 64f / 255f;
				col.a = 32f / 255f;
				waveformObject.GetComponent<GUITexture>().color = col;
				
				// Reenable GUI.
				EnableFSGUIElements();
				// Disable the invis play button
				fsPlayPanel.SetActive (false);
			}
		}
		// Otherwise...
		else
		{
			// If the media is currently playing...
			if(playing)
			{
				// Activate the play panel.
				playPanel.SetActive (true);
				// Deactivate the play button.
				playButtonObject.SetActive (false);
				// Set the waveform alpha back to full.
				Color col = waveformObject.GetComponent<GUITexture>().color;
				col.a = 1.0f;
				waveformObject.GetComponent<GUITexture>().color = col;
			}
			// Otherwise...
			else
			{
				// If some media is loaded...
				if(audioClip != null || videoClip != null)
				{
					// Activate the play button.
					playButtonObject.SetActive (true);
					playButtonObject.GetComponent<Button>().interactable = true;
					// Activate the play panel.
					playPanel.SetActive (true);
				}
				// Otherwise.
				else
				{
					// Make the play button uninteractable.
					playButtonObject.GetComponent<Button>().interactable = false;
				}
				// Set the waveform to transparent so the play button can be seen, and it is visually clear that the video is over.
				Color col = waveformObject.GetComponent<GUITexture>().color;
				col.a = 64f/255f;
				waveformObject.GetComponent<GUITexture>().color = col;
			}
		}
#endif
	}
	
	/// <summary>
	/// Close this script and returns to the main scene.
	/// </summary>
	private void Close()
	{
		// Stop audio
		ControlStop ();
		// Reset all MCP audio
		MCP.audioFilePath = "";
		MCP.videoFilePath = "";
		
		MCP.loadedAudio = null;
		
		waveformSource.clip = null;
#if !UNITY_ANDROID && !UNITY_IPHONE
		MCP.loadedVideo = null;
#endif
		MCP.mediaName = "";
		MCP.bronzeAssetBundle = null;
		MCP.silverAssetBundle = null;
		MCP.goldAssetBundle = null;
		MCP.downloadedAssetBundle = null;
		
#if UNITY_ANDROID || UNITY_IPHONE
		scrMedia.Stop ();
#endif
		videoManagerObject.SetActive (false);
		
		sceneCanvas.SetActive (true);
		// Remove all objects relating to this script.
		Destroy (localCanvas);
		Destroy (waveformObject);
		// Remove this script.
		Destroy (this);
	}
	
	private void SetProgressBarPosition(bool audioClip)
	{
		if(audioClip)
		{
			// Move to the centre of the screen.
			// Set the audio position background.
			audioPosBackground.GetComponent<RectTransform>().localPosition = new Vector3(audioPosBackground.GetComponent<RectTransform>().localPosition.x, Screen.height * -0.365f, 0); //new Rect(Screen.width * 0.2f, Screen.height * 0.7625f, Screen.width * 0.6f, Screen.height * 0.1125f)
			
			// Make the audio position slider,
			audioPositionSliderObject.GetComponent<RectTransform>().localPosition = new Vector3(audioPositionSliderObject.GetComponent<RectTransform>().localPosition.x, Screen.height * -0.335f, 0); //new Rect(Screen.width * 0.3f, Screen.height * 0.335f, Screen.width * 0.4f, Screen.height * 0.025f)
			
			// Make the current time label.
			currentTimeLabel.GetComponent<RectTransform>().localPosition = new Vector3(currentTimeLabel.GetComponent<RectTransform>().localPosition.x, Screen.height * -0.375f, 0);// new Rect(Screen.width * 0.205f, Screen.height * 0.7725f, Screen.width * 0.1f, Screen.height * 0.1f)
			
			// Make the total time label.
			totalTimeLabel.GetComponent<RectTransform>().localPosition = new Vector3(totalTimeLabel.GetComponent<RectTransform>().localPosition.x, Screen.height * -0.375f, 0);// new Rect(Screen.width * 0.7125f, Screen.height * 0.7725f, Screen.width * 0.1f, Screen.height * 0.1f)
			
#if !UNITY_ANDROID && !UNITY_IPHONE
			waveformObject.GetComponent<GUITexture>().enabled = false;
#endif
			
			playPanel.GetComponent<RectTransform>().localPosition = new Vector3(playPanel.GetComponent<RectTransform>().localPosition.x, Screen.height * -0.275f, playPanel.GetComponent<RectTransform>().localPosition.z);
			playPanel.GetComponent<RectTransform>().sizeDelta = new Vector2(playPanel.GetComponent<RectTransform>().sizeDelta.x, Screen.height * 0.5f);
		}
		else
		{
			// Return to default.
			// Set the audio position background.
			audioPosBackground.GetComponent<RectTransform>().localPosition = new Vector3(audioPosBackground.GetComponent<RectTransform>().localPosition.x, Screen.height * -0.365f, 0); //new Rect(Screen.width * 0.2f, Screen.height * 0.7625f, Screen.width * 0.6f, Screen.height * 0.1125f)
			
			// Make the audio position slider,
			audioPositionSliderObject.GetComponent<RectTransform>().localPosition = new Vector3(audioPositionSliderObject.GetComponent<RectTransform>().localPosition.x, Screen.height * -0.335f, 0); //new Rect(Screen.width * 0.3f, Screen.height * 0.335f, Screen.width * 0.4f, Screen.height * 0.025f)
			
			// Make the current time label.
			currentTimeLabel.GetComponent<RectTransform>().localPosition = new Vector3(currentTimeLabel.GetComponent<RectTransform>().localPosition.x, Screen.height * -0.375f, 0);// new Rect(Screen.width * 0.205f, Screen.height * 0.7725f, Screen.width * 0.1f, Screen.height * 0.1f)
			
			// Make the total time label.
			totalTimeLabel.GetComponent<RectTransform>().localPosition = new Vector3(totalTimeLabel.GetComponent<RectTransform>().localPosition.x, Screen.height * -0.375f, 0);// new Rect(Screen.width * 0.7125f, Screen.height * 0.7725f, Screen.width * 0.1f, Screen.height * 0.1f)
			
#if !UNITY_ANDROID && !UNITY_IPHONE
			waveformObject.GetComponent<GUITexture>().enabled = true;
#endif
			
			playPanel.GetComponent<RectTransform>().localPosition = new Vector3(playPanel.GetComponent<RectTransform>().localPosition.x, Screen.height * -0.275f, playPanel.GetComponent<RectTransform>().localPosition.z);
			playPanel.GetComponent<RectTransform>().sizeDelta = new Vector2(playPanel.GetComponent<RectTransform>().sizeDelta.x, Screen.height * 0.5f);
		}
	}
	
#if UNITY_ANDROID || UNITY_IPHONE
	/// <summary>
	/// Make the movie download, save, then play from device.
	/// </summary>
	/// <returns>A pause to allow cleanup.</returns>
	/// <param name="movie">Movie path.</param>
	private IEnumerator PlayHandheldMovie(string movie)
	{
		// Yield to give this some breathing room to try and cut down on iOS crashing.
		yield return new WaitForFixedUpdate();
		
		// If the video has not already been downloaded, download it.
		string filename = (movie.Split (new char[]{'/'})[movie.Split (new char[]{'/'}).Length - 1]).Replace (" ", "%20");
		
#if UNITY_IOS
		bool fileExists = System.IO.File.Exists (Application.persistentDataPath + "/" + filename);
#else
		bool fileExists = System.IO.File.Exists (Application.persistentDataPath + "/" + filename);
#endif
		mediaTitleObject.GetComponent<Text>().text = filename.Replace ("%20", " ");
		
#if UNITY_IOS
		if(!fileExists)
		{
			uint contentLength;
			int n = 0;
			int read = 0;
			
			System.Net.Sockets.NetworkStream networkStream;
			System.IO.FileStream fileStream;
			System.Net.Sockets.Socket client;
			
//			movie = MCP.serverURL + ("Videos/intro-video.mp4");
			string filepath = movie.Replace (MCP.siteURL, "");
			string host = MCP.siteURL.Replace ("http://", "").TrimEnd (new char[]{'/'});
			string query = "GET " + "/" + filepath.Replace (" ", "%20") + " HTTP/1.1\r\n" +
				"Host: " + host + "\r\n" +
					"User-Agent: undefined\r\n" +
					"Connection: close\r\n" + "\r\n";
			
			Debug.Log ("Query: " + query);
			
			client = new System.Net.Sockets.Socket(System.Net.Sockets.AddressFamily.InterNetwork, System.Net.Sockets.SocketType.Stream, System.Net.Sockets.ProtocolType.IP);
			
			client.Connect (host, 80);
			
			networkStream = new System.Net.Sockets.NetworkStream(client);
			
			byte[] bytes = System.Text.Encoding.Default.GetBytes (query);
			
			networkStream.Write (bytes, 0, bytes.Length);
			
			System.IO.BinaryReader bReader = new System.IO.BinaryReader(networkStream, System.Text.Encoding.Default);
			
			yield return bReader;
			
			string response = "";
			string line;
			char c;
			
			do
			{
				line = "";
				c = '\u0000';
				while(true)
				{
					c = bReader.ReadChar ();
					
					if(c == '\r')
					{
						break;
					}
					
					line += c;
				}
				
				c = bReader.ReadChar ();
				
				response += line + "\r\n";
			}
			while(line.Length > 0);
			
			Debug.Log ("Response: " + response);
			
			System.Text.RegularExpressions.Regex reContentLength = new System.Text.RegularExpressions.Regex(@"(?<=Content-Length:\s)\d+", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
			
			contentLength = uint.Parse (reContentLength.Match (response).Value);
			
			try
			{
				fileStream = new System.IO.FileStream(Application.persistentDataPath + "/" + filename, System.IO.FileMode.Create);
			}
			catch(System.Exception e)
			{
				sectionTitleObject.GetComponent<Text>().text = e.ToString();
				fileStream = new System.IO.FileStream(Application.persistentDataPath, System.IO.FileMode.Open);
			}
			
			yield return new WaitForFixedUpdate();
			
			byte[] buffer = new byte[4 * 1024];
			
			while (n < contentLength)
			{
				yield return null;
				
				if(networkStream.DataAvailable)
				{
					read = networkStream.Read (buffer, 0, buffer.Length);
					n += read;
					fileStream.Write (buffer, 0, read);
				}
				
				downloadPercent = ((float)n / (float)contentLength) * 100f;
				Debug.Log ("Downloaded " + n + " of " + contentLength + "bytes...");
			}
			
			fileStream.Flush ();
			fileStream.Close ();
			
			client.Close ();
			
			Debug.Log ("Finished");
		}
		
		//TODO: If not work try removing path.
		movie = "file:// " + Application.persistentDataPath + "/" + filename;
#else
		if(!fileExists)
		{
			hs_get = new WWW(movie);
			
			yield return hs_get;
			
			if (hs_get.error != null)
			{
				Debug.Log ("GetVideoFile(): ERROR: " + hs_get.error);
				
				if(hs_get != null)
				{
					hs_get.Dispose ();
					hs_get = null;
				}
				fileLoading = false;
				Destroy(downloadingPopup);
				downloadingPopup = MakePopup (localCanvas, defaultPopUpWindow, MCP.Text (1908)/*"Downloading..."*/, MCP.Text (1909)/*"Download Failed"*/, MCP.Text (205)/*"Dismiss"*/, CommonTasks.DestroyParent);
			}
			else
			{
				Debug.Log ("GetVideoFile(): Success");
				
				downloadingPopup.transform.GetChild (0).gameObject.GetComponent<Text>().text = MCP.Text (3103)/*"Saving...*"*/;
				
				if(!fileExists)
				{
					System.IO.FileStream fs = System.IO.File.Create (Application.persistentDataPath + "/" + filename);
					fs.Write (hs_get.bytes, 0, hs_get.bytes.Length);
					fs.Close ();
				}
				
				yield return new WaitForFixedUpdate();
				yield return new WaitForFixedUpdate();
			}
		}
		
		movie = /*"file://" + */Application.persistentDataPath + "/" + filename;
#endif
		
		Handheld.PlayFullScreenMovie (movie);
		fileLoading = false;
		Destroy (downloadingPopup);
		hs_get = null;
		
		yield return new WaitForSeconds(2.0f);
	}
#endif

	private IEnumerator PlayDeviceMovie(string url)
	{
		playing = true;
		
#if UNITY_ANDROID || UNITY_IPHONE
		yield return scrMedia.Load (url);
#else
		yield return 0;
#endif
	}
	
	/// <summary>
	/// Processes the files loaded by the file browser.
	/// </summary>
	public void FileBrowserClosed()
	{
		// Reset variables
		timePlaying = 0;
		playedMedia = false;
		stopped = false;
//		waveformSource.clip = null;
//		waveformObject.GetComponent<DrawWaveform>().Start();
		
#if !UNITY_ANDROID && !UNITY_IPHONE
		initialPlay = true;
		
		if(currentAssetPack == "")
		{
			MCP.loadedVideo = null;
		}
		if(videoClip != null)
		{
			videoClip.Stop ();
		}
		videoClip = null;
		
		// Remove transparency.
		Color c = waveformObject.GetComponent<GUITexture>().color;
		c.a = 128f / 255f;
		waveformObject.GetComponent<GUITexture>().color = c;
#endif
		// If there is new media selected...
		if(MCP.audioFilePath != "" || MCP.videoFilePath != "")
		{
			MCP.loadedAudio = null;
			
#if !UNITY_ANDROID && !UNITY_IPHONE
			MCP.loadedVideo = null;
#endif
		}
		// Otherwise, if the file browser was just closed...
		else
		{
			// Load the video path and reset the time playing.
			fileLoaded = true;
#if UNITY_ANDROID || UNITY_IOS
			fileLoading = true;
#endif
			MCP.videoFilePath = filePathStore;
			filePathStore = "";
			timePlaying = 0;
			
#if !UNITY_ANDROID && !UNITY_IPHONE
			// Activate the fullscreen button.
			fullscreenButton.GetComponent<Button>().interactable = false;
			// Return button to color transition to allow it to fade on disable.
			fullscreenButton.GetComponent<Button>().transition = Selectable.Transition.ColorTint;
#endif
		}
		
		waveformObject.SetActive(true);
		waveformSource.clip = null;
		
		// If the media is audio...
		if(MCP.audioFilePath != null && MCP.audioFilePath != "")
		{
			audioClip = null;
			fileLoading = true;
			fileLoaded = false;
			filePathStore = MCP.audioFilePath;
			
#if !UNITY_ANDROID && !UNITY_IPHONE
			waveformObject.SetActive(false);
#endif
			// Load the audio file.
			StartCoroutine (MCP.GetAudioFile());
		}
		// If the media is video...
		if(MCP.videoFilePath != null && MCP.videoFilePath != "")
		{
			audioClip = null;
			fileLoading = true;
			fileLoaded = false;
			filePathStore = MCP.videoFilePath;
			
#if UNITY_ANDROID || UNITY_IPHONE
			m_bFinish = false;
#endif
			
#if !UNITY_ANDROID && !UNITY_IPHONE
			// Load the video file.
			StartCoroutine (MCP.GetVideoFile());
#endif
		}
	}
	
	/// <summary>
	/// Toggles the fullscreen for the AV player.
	/// </summary>
	private void ToggleAVFullscreen()
	{
		// Reset the first frame timer.
		MenuScreen.firstFrameTimer = 0;
		
#if UNITY_EDITOR
		screenSize = new Rect(0,0,0,0);
#endif
		
		// If Screen.fullscreen is not on, activate it.
		if(!Screen.fullScreen && !fullscreen && !Application.isEditor)
		{
			Screen.fullScreen = true;
			ToggleFullscreen ();
		}
		
		fullscreen = !fullscreen;
		
		if(fullscreen)
		{
			// Disable normal canvas.
			localCanvas.SetActive (false);
			
#if !UNITY_ANDROID && !UNITY_IPHONE
			// Activate full screen canvas
			fullscreenCanvas.SetActive (true);
			fullscreenCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
			MenuScreen.firstFrameTimer = -1f;
#endif
			
			if(!Application.isEditor)
			{
//				ToggleFullscreen ();
			}
			
#if !UNITY_ANDROID && !UNITY_IPHONE			
			// Move the waveform object to the center of the screen.
			waveformObject.transform.position = new Vector3(0.5f, 0.5f, 0);
			waveformObject.transform.localScale = Vector3.one;
			waveformObject.GetComponent<GUITexture>().pixelInset = new Rect(0, 0, 0, 0);
			
			// Set the variable objects to the non fullscreen values.
			fsAudioPositionSliderObject.GetComponent<Slider>().value = audioPositionSliderObject.GetComponent<Slider>().value;
			fsVolumeSliderObject.GetComponent<Slider>().value = volumeSliderObject.GetComponent<Slider>().value;
#endif
		}
		else
		{
#if !UNITY_ANDROID && !UNITY_IPHONE
			if(!Application.isEditor)
			{
				Screen.fullScreen = false;
				ToggleFullscreen ();
			}
			
			// Disable full screen canvas.
			fullscreenCanvas.SetActive (false);
#endif
			
			// Activate normal canvas.
			localCanvas.SetActive (true);
			localCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
			MenuScreen.firstFrameTimer = -1f;
			
#if !UNITY_ANDROID && !UNITY_IPHONE
			// Move the waveform object to slightly above the centre of the screen.
			waveformObject.transform.position = new Vector3(0.0f, -0.05f, 0);
//			waveformObject.transform.localScale = new Vector3(0.6f, 0.5f, 1);
			waveformObject.transform.localScale = Vector3.one;
			waveformObject.GetComponent<GUITexture>().pixelInset = new Rect(screenSize.width * 0.71f, screenSize.height * 0.8f, screenSize.width * -0.42f, screenSize.height * -0.54f);
			
			// Set the non-fullscreen variable objects to the fullscreen values.
			audioPositionSliderObject.GetComponent<Slider>().value = fsAudioPositionSliderObject.GetComponent<Slider>().value;
			volumeSliderObject.GetComponent<Slider>().value = fsVolumeSliderObject.GetComponent<Slider>().value;
#endif
		}
	}
	
	public override void DoGUI()
	{
#if !UNITY_ANDROID && !UNITY_IPHONE
		//TODO: Move this to update
		if(videoClip != null)
		{
			if(!videoClip.isPlaying && videoClip.isReadyToPlay && !stopped)
			{
				videoClip.Play ();
			}
			else if(!stopped)
			{
				playing = true;
			}
		}
#endif
	}
	
	void OnEnd()
	{
#if UNITY_ANDROID || UNITY_IPHONE
		m_bFinish = true;
#endif
	}
	
	void OnDestroy()
	{
		sndManager.StartBackgroundMusic();
	}
	
	void OnApplicationFocus(bool isFocused)
	{
#if UNITY_ANDROID
		GetComponent<SuccessCentreScreen>().enabled = true;
		this.enabled = false;
#endif
	}
	
	/// <summary>
	/// Overrides the base Back().
	/// </summary>
	public override void Back ()
	{
		Close ();
	}
}
