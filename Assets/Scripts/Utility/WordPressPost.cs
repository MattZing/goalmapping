﻿using UnityEngine;
using System.Collections;

public class WordPressPost
{
	public class Author
	{
		public int id;
		public string slug;
		public string name;
		public string first_name;
		public string last_name;
		public string nickname;
		public string url;
		public string description;
	}
	
	public class Category
	{
		public int id;
		public string slug;
		public string title;
		public string description;
		public int parent;
		public int post_count;
	}
	
	public class Tag
	{
		public int id;
		public string slug;
		public string title;
		public string description;
		public int post_count;
	}
	
	public class CustomField
	{
		public int id;
		public string slug;
		public string title;
		public string description;
		public int parent;
		public int post_count;
	}
	
	public class Comment
	{
		public int id;
		public string name;
		public string url;
		public System.DateTime date;
		public string content;
		public int parent;
		public Author author;
		public System.DateTime latestChildDate;
	}
	
	public class Attachment
	{
		public class Image
		{
			public class Thumbnail
			{
				public string url;
				public int width;
				public int height;
			}
	
			public class Medium
			{
				public string url;
				public int width;
				public int height;
			}
	
			public class Large
			{
				public string url;
				public int width;
				public int height;
			}
	
			public class Full
			{
				public string url;
				public int width;
				public int height;
			}
			
			public Thumbnail thumbnail;
			public Medium medium;
			public Large large;
			public Full full;
		}
		
		public int id;
		public string url;
		public string slug;
		public string title;
		public string description;
		public string caption;
		public int parent;
		public string mime_type;
		public Image image;
	}
	
	public int id;
	public string type;
	public string slug;
	public string url;
	public string status;
	public string title;
	public string title_plain;
	public string content;
	public string excerpt;
	public System.DateTime date;
	public System.DateTime modified;
	public Category[] categories;
	public Tag[] tags;
	public Author author;
	public Comment[] comments;
	public Attachment[] attachments;
	public int comment_count;
	public string comment_status;
	public string thumbnail;
	public CustomField custom_fields; // Array of custom taxonomy objects (these resemble Category or Tag response objects, depending on whether the taxonomy is hierarchical)	
}