﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class WebcamTextureTest : MenuScreen
{
	public GameObject sceneCanvas;
	private GameObject localCanvas;
	private GameObject webcamImageObject;
	private GameObject noCamFoundText;
	private GameObject saveButton;

	public bool mirrored = false;
	private Sprite webcamSprite;
	private WebCamTexture webcamTexture;
	private WebCamDevice[] devices;
	string backCamName = "";
	string frontCamName = "";
	private bool camSet = false;

	private const int camSize = 128;

	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		// Initialize the base class.
		Init ();
		
		// Get the connected devices.
		devices = WebCamTexture.devices;
		
//		// If the user has not authorized the webcam, ask them to.
#if !UNITY_IPHONE
		if(!Application.HasUserAuthorization (UserAuthorization.WebCam))
		{
			Application.RequestUserAuthorization(UserAuthorization.WebCam);
		}
#endif
//		// Otherwise, try and get webcams.
//		else
//#endif
//		{
//			for( int i = 0 ; i < devices.Length ; i++ )
//			{
//				if (devices[i].isFrontFacing)
//				{
//					frontCamName = devices[i].name;
//				}
//				else
//				{
//					backCamName = devices[i].name;
//				}
//			}
//		}
//		
//		webcamTexture = new WebCamTexture(backCamName);//, camSize, camSize, 24);
//		// If there is a valid front camera...
//		if(frontCamName != "")
//		{
//			// Flag the camera as set.
//			camSet = true;
//			mirrored = false;
//			// Set the webcamTexture to the front cam.
//			webcamTexture = new WebCamTexture(frontCamName);//, camSize, camSize, 24);
//		}
//		// Otherwise, if there is a valid back camera...
//		else if(backCamName != "")
//		{
//			// Flag the camera as set.
//			camSet = true;
//			mirrored = true;
//			// Set the webcamTexture to the back cam.
//			webcamTexture = new WebCamTexture(backCamName);//, camSize, camSize, 24);
//		}
//		
//		// If there is a device and the camera is set.
//		if(devices.Length > 0 && camSet)
//		{
//			// Reset camSet flag.
//			camSet = false;
//			
//			// If the texture is not playing...
//			if(!webcamTexture.isPlaying)
//			{
//				// Start it playing.
//				webcamTexture.Play ();
//			}
//		}
		
		// Create the GUI.
		CreateGUI();
		
		localCanvas.transform.localScale = Vector2.zero;
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Disable the main scene canvas.
		sceneCanvas.SetActive (false);
		
		// Make a new canvas for this script.
		localCanvas = MakeCanvas (true);
		
		MakeSideGizmo (localCanvas, false, true);
		
		// Create a sprite from the webcam texture.
//		Texture2D spriteTex = new Texture2D(webcamTexture.width, webcamTexture.height);
//		if(webcamTexture != null && webcamTexture.isPlaying)
//		{
//			spriteTex.SetPixels (webcamTexture.GetPixels ());
//			spriteTex.Apply ();
//		}
//		webcamSprite = Sprite.Create (spriteTex, new Rect(0, 0, spriteTex.width, spriteTex.height), new Vector2(0.5f, 0.5f));
		webcamSprite = iconButton;
		
		// Show the current camera view. Allow users to press it to save picture.
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			SavePicture
		};
		//webcamImageObject = MakeButton (localCanvas, "", webcamSprite, new Rect((Screen.width * 0.5f) - (spriteTex.width * 0.5f), Screen.height * 0.1f, spriteTex.width, spriteTex.height), funcList);
		webcamImageObject = MakeButton (localCanvas, "", webcamSprite, new Rect(Screen.width * 0.325f, Screen.height * 0.1f, Screen.width * 0.35f, Screen.height * 0.4f), funcList);
		webcamImageObject.name = "WebcamImageObject";
		
		// Make a seperate button to save, just in case.
		GameObject saveButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.4f, Screen.height * 0.8f, Screen.width * 0.2f, Screen.height * 0.1f), buttonBackground, true);
		saveButtonBackground.name = "SaveButtonBackground";
		
		saveButton = MakeButton (localCanvas, "Save", null, new Rect(Screen.width * 0.4f, Screen.height * 0.8f, Screen.width * 0.2f, Screen.height * 0.1f), funcList, "InnerLabel");
		saveButton.name = "SaveButton";
		saveButton.GetComponent<Button>().image = saveButton.GetComponent<Image>();
		saveButton.transform.GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
		
		// Make no camera found text.
		noCamFoundText = MakeLabel (localCanvas, MCP.Text (3001)/* "No Camera Found"*/, new Rect(Screen.width * 0.25f, Screen.height * -0.005f, Screen.width * 0.5f, Screen.height * 0.5f), TextAnchor.MiddleCenter, "InnerLabel");
		noCamFoundText.name = "NoCamFoundText";
		noCamFoundText.SetActive (false);
	}
	
	/// <summary>
	/// Saves the picture.
	/// </summary>
	public void SavePicture()
	{
		// If there is a valid webcam texture...
		if(webcamTexture != null && webcamTexture.isPlaying)
		{
			// Store the texture.
			Texture2D tex = new Texture2D(webcamSprite.texture.width, webcamSprite.texture.height);
			
			// If mirrored, flip pixel x positions
			if(webcamImageObject.GetComponent<RectTransform>().localScale.x < 0)
			{
				for(int x = 0; x < webcamSprite.texture.width; x++)
				{
					for(int y = 0; y < webcamSprite.texture.height; y++)
					{
						try
						{
							tex.SetPixel (webcamSprite.texture.width - x - 1, y, webcamSprite.texture.GetPixel (x, y));
						}
						catch(System.Exception e)
						{
							Debug.Log ("SetPixelError: " + e);
						}
					}
				}
			}
			else
			{
				for(int x = 0; x < webcamSprite.texture.width; x++)
				{
					for(int y = 0; y < webcamSprite.texture.height; y++)
					{
						try
						{
							tex.SetPixel (x, y, webcamSprite.texture.GetPixel (x, y));
						}
						catch(System.Exception e)
						{
							Debug.Log ("SetPixelError: " + e);
						}
					}
				}
			}
			
			tex.Apply();
			MCP.loadedTexture = tex;
		}
		else
		{
			MCP.loadedTexture = null;
		}
		
		// Close the webcam.
		Close ();
	}
	
	/// <summary>
	/// Close this instance.
	/// </summary>
	void Close()
	{
		// If the webcam texture is working, stop it.
		if(webcamTexture != null && webcamTexture.isPlaying)
		{
			webcamTexture.Stop ();
		}
		
		MenuScreen.firstFrameTimer = 0;
		
		// Reenable other scripts.
		sceneCanvas.SetActive (true);
		SendMessage ("FileBrowserClosed", SendMessageOptions.DontRequireReceiver);
		// Destroy all file browser game objects.
		Destroy (localCanvas);
		
		// Remove this script from the object to close.
		Destroy (this);
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update ()
	{
		if(devices.Length == 0 || !Application.HasUserAuthorization (UserAuthorization.WebCam))
		{
			// Change the save button to go back.
			saveButton.transform.GetChild (0).gameObject.GetComponent<Text>().text = "Back";
			// Show no camera found.
			noCamFoundText.SetActive (true);
			webcamImageObject.SetActive (false);
		}
		else
		{
			// If the camera hasn't been set...
			if(!camSet)
			{
				// Ask the user for authorization.
	#if !UNITY_IPHONE
				if(Application.HasUserAuthorization (UserAuthorization.WebCam))
	#endif
				{
					// Make sure a webcam texture is not running when the new devices are set.
					if(webcamTexture != null && webcamTexture.isPlaying)
					{
						webcamTexture.Stop ();
					}
					
					for( int i = 0 ; i < devices.Length ; i++ )
					{
						if (devices[i].isFrontFacing)
						{
							frontCamName = devices[i].name;
						}
						else
						{
							backCamName = devices[i].name;
						}
					}
					
					// If there is a valid front camera...
					if(frontCamName != "")
					{
						camSet = true;
						mirrored = false;
						webcamTexture = new WebCamTexture(frontCamName);//, camSize, camSize, 24);
					}
					// If there is a valid back camera...
					else if(backCamName != "")
					{
						camSet = true;
						mirrored = true;
						webcamTexture = new WebCamTexture(backCamName);//, camSize, camSize, 24);
						
						// Mirror the take photo object.
						Vector3 newScale = webcamImageObject.GetComponent<RectTransform>().localScale;
						newScale.x = -1;
						webcamImageObject.GetComponent<RectTransform>().localScale = newScale;
						Vector3 newPos = webcamImageObject.GetComponent<RectTransform>().localPosition;
						newPos.x = newPos.x + webcamImageObject.GetComponent<RectTransform>().rect.width;
						webcamImageObject.GetComponent<RectTransform>().localPosition = newPos;
					}
					
					// If there are any devices...
					if(devices.Length > 0)
					{
						// If this device is not currently playing...
						if(!webcamTexture.isPlaying)
						{
							// Play the webcam texture.
							webcamTexture.Play ();
						}
					}
					
					// Create a sprite from the webcam texture.
//					Texture2D spriteTex = new Texture2D(webcamTexture.width, webcamTexture.height);
//					if(webcamTexture != null && webcamTexture.isPlaying)
//					{
//						spriteTex.SetPixels (webcamTexture.GetPixels ());
//						spriteTex.Apply ();
//					}
//					webcamSprite = Sprite.Create (spriteTex, new Rect(0, 0, spriteTex.width, spriteTex.height), new Vector2(0.5f, 0.5f));
//					
//					webcamImageObject.GetComponent<Image>().sprite = webcamSprite;
				}
			}
			else if(devices.Length > 0)
			{
				// If the webcam texture is valid and playing...
				if(webcamTexture != null && webcamTexture.isPlaying)
				{
					if(webcamTexture.width > 32)
					{
						// Change the button text and change the panel texture.
						saveButton.transform.GetChild (0).gameObject.GetComponent<Text>().text = "Save";
	
						// Create a sprite from the webcam texture.
						Texture2D spriteTex = new Texture2D(webcamTexture.width, webcamTexture.height);
	
						spriteTex.SetPixels(webcamTexture.GetPixels ());
						spriteTex.Apply ();
	
						webcamSprite = Sprite.Create (spriteTex, new Rect(0, 0, spriteTex.width, spriteTex.height), new Vector2(0.5f, 0.5f));
						
						webcamImageObject.GetComponent<Image>().sprite = webcamSprite;
	
						// Show the camera.
						noCamFoundText.SetActive (false);
						webcamImageObject.SetActive (true);
					}
				}
			}
		}
	}
	
	public override void Back ()
	{
		if(webcamTexture != null && webcamTexture.isPlaying)
		{
			webcamTexture.Stop ();
		}
		
		webcamTexture = null;
		devices = null;
		Close ();
	}
	
	private void OnDestroy()
	{
		if(webcamTexture != null && webcamTexture.isPlaying)
		{
			webcamTexture.Stop ();
		}
		
		webcamTexture = null;
		devices = null;
	}
	
	public override void DoGUI() {}
}
