using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StaticSpriteSheet : MonoBehaviour
{	
	/*  ZingMonster Colours
	 *  green: 110 214 0
		yellow: 242 186 29
		orange: 240 94 21
		magenta: 242 0 99
		purple: 196 23 221
		blue: 0 116 221
	*/
	
	public GameObject backgroundQuad;
	private Renderer _myRenderer;
	public Sprite backTexGreen;
	public Sprite backTexYellow;
	public Sprite backTexOrange;
	public Sprite backTexMagenta;
	public Sprite backTexPurple;
	public Sprite backTexBlue;
	public AvatarSheet[] avatarSheet;
	public Sprite selectedAvatar;
	public Sprite selectedBackground;
	
	public int _uvTieX = 1;
	public int _uvTieY = 1;
	private Vector2 offset;
	private int currentSheet;
	public List<MonsterColor> monsterColors;
	
	[System.Serializable]
	public class AvatarSheet
	{
		public Texture sheetTexture;
		public int tilesX;
		public int tilesY;
	}
	
	[System.Serializable]
	public class MonsterColor
	{
		public string color;
		public int r, g, b;
		public Sprite backgroundTex;

		public Color GetColor ()
		{
			float normalR = 0.0f;
			float normalG = 0.0f;
			float normalB = 0.0f;
			if (r != 0)
			{
				normalR = r / 512f;
			}
			if (g != 0)
			{
				normalG = g / 512f;
			}
			if (b != 0)
			{
				normalB = b / 512f;
			}

			return new Color (normalR, normalG, normalB);
		}
	}
	
	/// <summary>
	/// Used to initialize.
	/// </summary>
	void Start ()
	{	
		currentSheet = avatarSheet.Length - 1;
		PickSprite( 0, 0, 0, currentSheet );
		
		// overwrite with saved avatar image if MCP is available
		if ( MCP.userInfo != null )
		{
			if ( MCP.userInfo.myAvatar != null )
			{
				PickSprite (MCP.userInfo.myAvatar.indexX, MCP.userInfo.myAvatar.indexY, MCP.userInfo.myAvatar.indexColor, MCP.userInfo.myAvatar.sheetIndex);
			}
		}
		
		monsterColors = new List<MonsterColor> ();
		MonsterColor newColor = new MonsterColor ();
		newColor.color = "Green";
		newColor.r = 110;
		newColor.g = 214;
		newColor.b = 0;
		newColor.backgroundTex = backTexGreen;
		monsterColors.Add (newColor);
		newColor = new MonsterColor ();
		newColor.color = "Yellow";
		newColor.r = 242;
		newColor.g = 186;
		newColor.b = 29;
		newColor.backgroundTex = backTexYellow;
		monsterColors.Add (newColor);
		newColor = new MonsterColor ();
		newColor.color = "Orange";
		newColor.r = 240;
		newColor.g = 94;
		newColor.b = 21;
		newColor.backgroundTex = backTexOrange;
		monsterColors.Add (newColor);
		newColor = new MonsterColor ();
		newColor.color = "Magenta";
		newColor.r = 242;
		newColor.g = 0;
		newColor.b = 99;
		newColor.backgroundTex = backTexMagenta;
		monsterColors.Add (newColor);
		newColor = new MonsterColor ();
		newColor.color = "Purple";
		newColor.r = 196;
		newColor.g = 23;
		newColor.b = 221;
		newColor.backgroundTex = backTexPurple;
		monsterColors.Add (newColor);
		newColor = new MonsterColor ();
		newColor.color = "Blue";
		newColor.r = 0;
		newColor.g = 116;
		newColor.b = 221;
		newColor.backgroundTex = backTexBlue;
		monsterColors.Add (newColor);
	}

	/// <summary>
	/// Picks the sprite.
	/// </summary>
	/// <param name="indexX">Index x.</param>
	/// <param name="indexY">Index y.</param>
	/// <param name="monsterColor">Monster color.</param>
	/// <param name="sheetIndex">Sheet index.</param>
	public void PickSprite (int indexX, int indexY, int monsterColor, int sheetIndex)
	{
		int x, y = 0;
		currentSheet = sheetIndex;
		x = Mathf.FloorToInt (indexX * (avatarSheet[currentSheet].sheetTexture.width / avatarSheet[currentSheet].tilesX));
		y = Mathf.FloorToInt (indexY * (avatarSheet[currentSheet].sheetTexture.height / avatarSheet[currentSheet].tilesY));
		
		int width = Mathf.FloorToInt ((avatarSheet[currentSheet].sheetTexture.width /avatarSheet[currentSheet].tilesX));
		int height = Mathf.FloorToInt ((avatarSheet[currentSheet].sheetTexture.height / avatarSheet[currentSheet].tilesY));
		
		Color[] pix = (avatarSheet[currentSheet].sheetTexture as Texture2D).GetPixels (x, y, width, height);
		int index = 0;
		
		foreach (Color tmp in pix)
		{
			Color newColor;
			if (tmp.a != 0)
			{
				newColor.r = monsterColors [monsterColor].r / 255f;
				newColor.g = monsterColors [monsterColor].g / 255f;
				newColor.b = monsterColors [monsterColor].b / 255f;
				newColor.a = tmp.a;
				pix [index] = newColor;
			}

			index++;
		}

#if UNITY_IOS
		Texture2D destTex = new Texture2D (width, height, TextureFormat.RGBA32, false);
#else
		Texture2D destTex = new Texture2D (width, height, TextureFormat.RGBA32, false);
#endif
		destTex.SetPixels (pix);
		destTex.Apply ();
		selectedBackground = monsterColors [monsterColor].backgroundTex;

		Sprite destSprite = Sprite.Create (destTex, new Rect(0, 0, width, height), new Vector2(0.5f, 0.5f));
		selectedAvatar = destSprite;
	}
	
	/// <summary>
	/// Picks the sprite image.
	/// </summary>
	/// <param name="indexX">Index x.</param>
	/// <param name="indexY">Index y.</param>
	/// <param name="sheetIndex">Sheet index.</param>
	public void PickSpriteImage (int indexX, int indexY, int sheetIndex)
	{
		int x,y =0;
		currentSheet = sheetIndex;
		
		x = Mathf.FloorToInt (indexX * (avatarSheet[currentSheet].sheetTexture.width / avatarSheet[currentSheet].tilesX));
		y = Mathf.FloorToInt (indexY * (avatarSheet[currentSheet].sheetTexture.height / avatarSheet[currentSheet].tilesY));
		
		int width = Mathf.FloorToInt ((avatarSheet[currentSheet].sheetTexture.width /avatarSheet[currentSheet].tilesX));
		int height = Mathf.FloorToInt ((avatarSheet[currentSheet].sheetTexture.height / avatarSheet[currentSheet].tilesY));
		
		Color[] pix = (avatarSheet[currentSheet].sheetTexture as Texture2D).GetPixels (x, y, width, height);

#if UNITY_IOS
		Texture2D destTex = new Texture2D (width, height, TextureFormat.RGBA32, false);
#else
		Texture2D destTex = new Texture2D (width, height, TextureFormat.RGBA32, false);
#endif
		destTex.SetPixels (pix);
		destTex.Apply ();

		Sprite destSprite = Sprite.Create (destTex, new Rect(x, y, width, height), new Vector2(0.5f, 0.5f));
		selectedAvatar = destSprite;
	}
	
	/// <summary>
	/// Gets the number of sprite sheets.
	/// </summary>
	/// <returns>The number sprite sheets.</returns>
	public int GetNumSpriteSheets()
	{
		return avatarSheet.Length;
	}
	
	/// <summary>
	/// Gets the number of X tiles.
	/// </summary>
	/// <returns>The X tile count.</returns>
	public int GetXTileCount()
	{
		return avatarSheet[currentSheet].tilesX;
	}
	
	/// <summary>
	/// Gets the number of Y tiles.
	/// </summary>
	/// <returns>The Y tile count.</returns>
	public int GetYTileCount()
	{
		return avatarSheet[currentSheet].tilesY;
	}

}