﻿using UnityEngine;
using System.Collections;

public class GUIControls : MonoBehaviour
{	
	/// <summary>
	/// The RGB circle.
	/// </summary>
	/// <returns>The color from the circle.</returns>
	/// <param name="c">Color.</param>
	/// <param name="rect">Rect.</param>
	/// <param name="colorCircle">Color circle texture.</param>
	public static Color RGBCircle (Color c, Rect rect, Texture2D colorCircle)
	{
		// It is much easier to work with HSB colours in this case
		HSBColor hsb = new HSBColor (c);
		
		// Get the centre point of the texture.
		Vector2 cp = new Vector2 (rect.x + (rect.width / 2f), rect.y + (rect.height / 2f));
		
		// On mouse button...
		if (Input.GetMouseButton (0))
		{
			// Get the position of the click.
			Vector2 InputVector = Vector2.zero;
			InputVector.x = cp.x - Input.mousePosition.x;
			InputVector.y = Input.mousePosition.y - cp.y;
			
			float hyp = Mathf.Sqrt( (InputVector.x * InputVector.x) + (InputVector.y * InputVector.y) );
			
			// If the position is off of the texture...
			if (hyp <= rect.width / 2 + 5)
			{
				// Clamp it to the position of the texture.
				hyp = Mathf.Clamp (hyp, 0, rect.width / 2);
				
				// Get the angle of the input vector.
				float a = Vector3.Angle(new Vector3(-1, 0, 0), InputVector);
				
				// If the input vector is below 0...
				if (InputVector.y < 0)
				{
					// Continue around from 360 degrees.
					a = 360 - a;
				}
				
				hsb.h = a / 360;
				hsb.s = hyp / (rect.width / 2);
			}
		}
		
		// Return the color is the Color format.
		c = hsb.ToColor ();
		return c;
	}
}
