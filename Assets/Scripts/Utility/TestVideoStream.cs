﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TestVideoStream : MonoBehaviour
{	
	List<string> videoURLs = new List<string>();
	public bool m_bFinish = false;
	
	public MediaPlayerCtrl scrMedia;
	
	// Use this for initialization
	void Start ()
	{
//		string videoFolder = MCP.serverURL + "Videos/Test/";
		videoURLs.Add ("http://www.zingup.com/GoalCortex0.0.1.1/Videos/intro-video.mp4");
//		videoURLs.Add (videoFolder + "Test1.mp4");
//		videoURLs.Add (videoFolder + "Test2.mp4");
//		videoURLs.Add (videoFolder + "Test3.mp4");
//		videoURLs.Add (MCP.serverURL + "Videos/intro-video.mp4");
		scrMedia.OnEnd += OnEnd;
	}
	
	IEnumerator LoadMedia(string filepath)
	{
		yield return scrMedia.Load (filepath);
	}
	
	// Update is called once per frame
	void Update ()
	{
		// Resize the video object
		transform.GetChild (0).transform.localScale = new Vector3(10, 8, 1);
		transform.GetChild (0).transform.localPosition = new Vector3(0, 0.5f, 10);
	}
	
	void OnGUI()
	{
		if(GUI.Button (new Rect(Screen.width * 0.4f, Screen.height * 0.88f, Screen.width * 0.2f, Screen.height * 0.1f), "Load"))
		{
			StartCoroutine(LoadMedia(videoURLs[0]));
			m_bFinish = false;
		}
		
		GUIStyle style = new GUIStyle();
		style.alignment = TextAnchor.MiddleCenter;
		style.fontSize = 20;
		style.normal.textColor = Color.white;
		
		if(scrMedia.GetCurrentState () == MediaPlayerCtrl.MEDIAPLAYER_STATE.ERROR)
		{
			GUI.Label (new Rect(Screen.width * 0.4f, Screen.height * 0.01f, Screen.width * 0.2f, Screen.height * 0.1f), "Error", style);
		}
	}
	
	void OnEnd()
	{
		m_bFinish = true;
	}
}
