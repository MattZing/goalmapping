﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class HoverPointer : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
#if !UNITY_ANDROID && !UNITY_IPHONE
	private Texture2D buttonTex;
	private Texture2D inputTex;
#endif
	
	// Use this for initialization
	void Start ()
	{
#if !UNITY_ANDROID && !UNITY_IPHONE
		buttonTex = Resources.Load ("Cursor/zingcursor_hand") as Texture2D;
		inputTex = Resources.Load ("Cursor/zingcursor_input") as Texture2D;
		Cursor.SetCursor (null, Vector2.zero, CursorMode.Auto);
#endif
	}
	
	public void OnPointerEnter(PointerEventData eventData)
	{	
#if !UNITY_ANDROID && !UNITY_IPHONE
		if(eventData != null)
		{
			if(GetComponent<Button>() != null)
			{
				Cursor.SetCursor (buttonTex, Vector2.zero, CursorMode.Auto);
			}
			if(GetComponent<InputField>() != null)
			{
				Cursor.SetCursor (inputTex, Vector2.zero, CursorMode.Auto);
			}
		}
#endif
	}
	
	public void OnPointerExit(PointerEventData eventData)
	{
#if !UNITY_ANDROID && !UNITY_IPHONE
		Cursor.SetCursor (null, Vector2.zero, CursorMode.Auto);
#endif
	}
}
