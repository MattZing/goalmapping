﻿using UnityEngine;
using System.Collections;

public class PlayMovie : MonoBehaviour {
	#if UNITY_ANDROID || UNITY_IPHONE
	public string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, "triangle_lucy.mp4");
	#else

	#endif

	public string result = "";

#if UNITY_EDITOR && !UNITY_ANDROID && !UNITY_IPHONE
	public MovieTexture _texture;
#endif
	
	void Start() {
		#if UNITY_ANDROID 
		StartCoroutine(CoroutinePlayMovie());
		//Handheld.PlayFullScreenMovie ("triangle_lucy.mp4", Color.black, FullScreenMovieControlMode.Full);

		#endif

#if UNITY_EDITOR


		//GameObject vidPlayer = GameObject.CreatePrimitive(PrimitiveType.Quad).renderer.material.mainTexture = _texture;


		//_texture.Play();
		#endif

		#if UNITY_IPHONE
		Handheld.PlayFullScreenMovie ("triangle_lucy.mp4", Color.black, FullScreenMovieControlMode.Full);
		#endif
	}

	
	
	
	protected IEnumerator CoroutinePlayMovie() { 
		#if UNITY_ANDROID || UNITY_IPHONE
		if (filePath.Contains("://")) {
			WWW www = new WWW(filePath);
			yield return www;
			result = www.text;
		} else
		{

			
			Handheld.PlayFullScreenMovie (filePath, Color.black, FullScreenMovieControlMode.CancelOnInput);
			


		}
		#endif
		yield return new WaitForSeconds(2.0f); //Allow time for Unity to pause execution while the movie plays. 

		//Application.LoadLevel("MainMenu"); 
		
	}
}