using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class EULA : MenuScreen
{
	public GameObject sceneCanvas;
	private GameObject localCanvas;
	
	public string sceneTitle;
	public TextAsset EULAtext;
	public string agreementText;
	private Vector2 scrollPosition2;
	private Texture logoutButtonImage;
	private float logoutButtonScale;
	private bool hasAgreed; 

	// touch scroll
	private Touch touch;

	public enum AgreementType
	{
		EULA,
		Disclaimer
	};
	public AgreementType agreementType;
	
	// Use this for initialization
	void Start()
	{
		Init ();
		
		EULAtext = Resources.Load ("HelpText/EULA ZingUp Version 1.x") as TextAsset;
		
		CreateGUI ();
	}
	
	void CreateGUI()
	{
		if(sceneCanvas == null)
		{
			sceneCanvas = MakeCanvas (false);
		}
		
		localCanvas = MakeCanvas (true);
		
		GameObject windowBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.02f, Screen.height * 0.15f, Screen.width * 0.96f, Screen.height * 0.83f), window_back);
		windowBackground.name = "WindowBackground";
		
		MakeSideGizmo (localCanvas, false);
		
		GameObject scrollRect = MakeScrollRect (localCanvas, new Rect(Screen.width * 0.1f, Screen.height * 0.15f, Screen.width * 0.8f, Screen.height * 0.75f), null);
		scrollRect.name = "ScrollRect";
		scrollRect.GetComponent<ScrollRect>().horizontal = false;
		
		float textHeight = Screen.height * 9.0f;
		
		GameObject eulaText = MakeLabel (localCanvas, "\n\n" + EULAtext.text, new Rect(Screen.width * 0.1f, Screen.height * 0.17f, Screen.width * 0.8f, textHeight));
		eulaText.name = "eulaText";
		eulaText.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
		
		GameObject scrollContent = scrollRect.transform.GetChild (0).GetChild (0).gameObject;
		eulaText.transform.SetParent (scrollContent.transform, true);
		eulaText.transform.localScale = Vector3.one;
		scrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(scrollContent.GetComponent<RectTransform>().sizeDelta.x, textHeight);
		scrollContent.GetComponent<RectTransform>().localPosition = new Vector3(0, -textHeight * 2, 0);
		scrollRect.GetComponent<ScrollRect>().velocity = new Vector2(0, 100f);
	}

	void Update()
	{
		HandleTouch();
	}
	
	public override void Back ()
	{
		sceneCanvas.SetActive (true);
		Destroy(localCanvas);
		Destroy(this);
	}
	
	public override void DoGUI() {}
}
