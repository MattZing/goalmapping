﻿using UnityEngine;
using System.Collections;
using System;
public class TestPushNotifications : MonoBehaviour {

	void Start()
	{
#if UNITY_IOS
		// schedule notification to be delivered in 10 seconds
		UnityEngine.iOS.LocalNotification notif = new UnityEngine.iOS.LocalNotification();
		notif.fireDate = DateTime.Now.AddSeconds(10);
		notif.alertAction = "ZingUp Test Notification";
		notif.alertBody = "Hello!";
		notif.hasAction = true;
		notif.repeatInterval = UnityEngine.iOS.CalendarUnit.Day;
		notif.applicationIconBadgeNumber = notif.applicationIconBadgeNumber + 1;
		UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(notif);
#endif
		Debug.Log( "Notification Push" );
	}
	void Update()
	{
		#if UNITY_IOS
		if (UnityEngine.iOS.NotificationServices.localNotificationCount > 0) {
			Debug.Log("LocalNotification: "+UnityEngine.iOS.NotificationServices.localNotifications[0].alertBody);
			UnityEngine.iOS.NotificationServices.ClearLocalNotifications();
		}
		#endif
	}
}
