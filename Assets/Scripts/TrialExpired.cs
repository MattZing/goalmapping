﻿using UnityEngine;
using System.Collections;

public class TrialExpired : MenuScreen
{
	private Rect popUpWindow;
	private Texture popUpTex;
	private Texture buttonTex;
	private Texture fadeBackground;
	private GUIStyle buttonStyle;

	// Use this for initialization
	void Start ()
	{
		// Initialize the base class.
		Init ();
		
		// Initialize GUIStyles.
		buttonStyle = new GUIStyle ();
		buttonStyle.alignment = TextAnchor.MiddleCenter;
		
		// Load resources.
		popUpTex = Resources.Load ("GUI/app_popup_bg") as Texture;
		fadeBackground = Resources.Load ("menu_bg") as Texture;
		buttonTex = Resources.Load ("GUI/LL_Blue") as Texture;
		
		// Set default values.
		popUpWindow = new Rect( Screen.width * 0.125f, Screen.height * 0.125f, Screen.width * 0.75f, Screen.height * 0.75f );
	}

	/// <summary>
	/// Override the base DoGUI.
	/// </summary>
	public override void DoGUI()
	{
		// Draw a popup to process this.
		DoPopUpMessage (popUpWindow);
	}
	
	/// <summary>
	/// Does the pop up message.
	/// </summary>
	/// <param name="rect">For the popup.</param>
	void DoPopUpMessage(Rect rect)
	{
		GUI.DrawTexture( new Rect( 0, 0, popUpWindow.height * 0.15f, popUpWindow.height * 0.15f ), news_infoImage, ScaleMode.StretchToFill, true, 10f );
		// Draw background.	
		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), fadeBackground, ScaleMode.StretchToFill);

		GUI.DrawTexture ( popUpWindow, popUpTex, ScaleMode.StretchToFill);

		GUI.Label (new Rect (rect.x + (rect.width * 0.1f), rect.y + (rect.height * 0.15f), rect.width * 0.8f, rect.height * 0.2f),
		           "You have completed the first month of you ZingUp course. We would love to hear how you got on.", "PopUpText");
		GUI.Label (new Rect (rect.x + (rect.width * 0.1f), rect.y + (rect.height * 0.32f), rect.width * 0.8f, rect.height * 0.2f),
		           "Most people start to notice significant changes at the three month stage, but you may have started to experience benefits already.", "PopUpText");
		GUI.Label (new Rect (rect.x + (rect.width * 0.1f), rect.y + (rect.height * 0.55f), rect.width * 0.8f, rect.height * 0.2f),
		           "Imagine what you could achieve if you completed your entire ZingUp course. Please let us know how you got on.", "PopUpText");

		// Draw button background.
		GUI.DrawTexture (new Rect (rect.x + (rect.width * 0.17f), rect.y + (rect.height * 0.78f), rect.width * 0.23f, rect.height * 0.1f), buttonTex);

		// Draw button to leave feedback.
		GUI.DrawTexture (new Rect (rect.x + (rect.width * 0.59f), rect.y + (rect.height * 0.78f), rect.width * 0.25f, rect.height * 0.1f), buttonTex);
	}
}
