﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;

//TODO: Make sure the new list of strings plays properly on non-ios devices.

public class SimulateFileBrowser : MenuScreen
{
	public Sprite fileSprite;
	public Sprite folderSprite;
	public Sprite upFolderSprite;
	public Sprite refreshSprite;
	public Sprite highlightedBackground;
	private Sprite rotationSprite;
	public object calledByScript;
	
	private List<GameObject> fileBrowserObjects;
	private List<GameObject> currentFiles;
	public GameObject sceneCanvas;
	private GameObject localCanvas;
	private GameObject currentPathObject;
	private GameObject folderScrollArea;
	private GameObject openButtonObject;
	private GameObject noFilesFoundLabel;
	private GameObject popup;
	private GameObject openingPopup;
	private GameObject openingSpinner;
	
	public bool videoOverride = false;		// Used to make sure an ogg is opened as a video
	private bool updateVelocity = false;
	private float clickTimer;
	private float offsetY = 0;
	private float spinnerRot = 0;
	public string[] allowedExtensions;
	public string assetPack;
	//private AssetBundle selectedAssetPack;
	private List<SuccessCentreScreen.Media> currentMediaList;
	
	enum FileType
	{
		File,
		Folder
	}
	
	struct FolderObject
	{
		public string path;
		public FileType fileType;
		public Rect rect;
		public GameObject gameObject;
		
		public FolderObject(string p, FileType ft)
		{
			path = p;
			fileType = ft;
			rect = new Rect(0, 0, 0, 0);
			gameObject = null;
		}
	}

	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		// Initialize the base class.
		Init ();
		
		// Initialise lists.
		currentFiles = new List<GameObject>();
		fileBrowserObjects = new List<GameObject>();
		
		// Set variable defaults.
		clickTimer = 0f;
		
		// Load resources.
		fileSprite = Resources.Load<Sprite>("GUI/File-8x");
		folderSprite = Resources.Load<Sprite>("GUI/Folder");
		upFolderSprite = Resources.Load<Sprite>("left_arrow");
		refreshSprite = Resources.Load<Sprite>("waiting_symbol");
		highlightedBackground = Resources.Load<Sprite>("box");
		rotationSprite = Resources.Load<Sprite>("GUI/bm_rotate");
		
		// Get the goal maps.
		if(MCP.userInfo != null && (assetPack == null || assetPack == ""))
		{
			StartCoroutine (MCP.GetGoalMap (MCP.userInfo.username));
		}
		
		// Create the GUI.
		MakeGUI ();
		
		localCanvas.transform.localScale = Vector2.zero;
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void MakeGUI()
	{
		// Disable the main scene canvas.
		if(sceneCanvas == null)
		{
			sceneCanvas = GameObject.Find ("Canvas");
		}
		sceneCanvas.SetActive (false);
		
		// Make a canvas for the scene.
		localCanvas = MakeCanvas(true);
		localCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
		MenuScreen.firstFrameTimer = -2;
		
		// Draw background.
		GameObject windowBack = MakeWindowBackground (localCanvas, new Rect(Screen.width * 0.01f, Screen.height * 0.01f , Screen.width * 0.98f, Screen.height * 0.98f), window_back);
		windowBack.name = "WindowBackground";
		
		// Draw window background.
		GameObject backgroundObject = MakeImage (localCanvas, new Rect(0, 0, Screen.width, Screen.height), menuBackground);
		backgroundObject.name = "BackgroundObject";
		fileBrowserObjects.Add (backgroundObject);
		
		// Create scroll area for files.
		folderScrollArea = MakeScrollRect (localCanvas, new Rect(Screen.width * 0.1f, Screen.height * 0.125f, Screen.width * 0.8f, Screen.height * 0.65f), null);
		folderScrollArea.name = "FolderScrollArea";
		folderScrollArea.GetComponent<ScrollRect>().horizontal = false;
		fileBrowserObjects.Add (folderScrollArea);
		
		// Draw back button.
		GameObject closeButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.8f, Screen.height * 0.87f, Screen.width * 0.15f, Screen.height * 0.08f), buttonBackground, true);
		closeButtonBackground.name = "CloseButtonBackground";
		fileBrowserObjects.Add (closeButtonBackground);
		
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			Close
		};
		GameObject closeButtonObject = MakeButton (localCanvas, MCP.Text (206)/*"Close"*/, new Rect(Screen.width * 0.8f, Screen.height * 0.87f, Screen.width * 0.15f, Screen.height * 0.08f), funcList, "SmallText");
		closeButtonObject.name = "CloseButtonObject";
		closeButtonObject.GetComponent<Button>().image = closeButtonBackground.GetComponent<Image>();
		closeButtonObject.GetComponent<Text> ().alignment = TextAnchor.MiddleCenter;
		closeButtonObject.GetComponent<Text> ().color = bmOrange;
		fileBrowserObjects.Add (closeButtonObject);
	}
	
	private IEnumerator OpenFile(int index)
	{
		// Make a popup to feedback that the file is opening.
		openingPopup = MakePopup (localCanvas, defaultPopUpWindow, MCP.Text (2102)/*"Opening..."*/, "", "", CommonTasks.DestroyParent);
		openingPopup.name = "OpeningPopup";
		
		// Remove the button
		Destroy(openingPopup.transform.GetChild (2).gameObject);
		
		// Make a spinner for this loading screen.
		openingSpinner = MakeImage (localCanvas, new Rect(Screen.width * 0.0f, Screen.height * -0.1f, Screen.width * 0.08f, Screen.width * 0.08f), rotationSprite, false);
		openingSpinner.name = "LoggingInSpinner";
		
		// Move the pivot to the center of the object.
		openingSpinner.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
		openingSpinner.transform.SetParent (openingPopup.transform);
		
		yield return null;
		
		if(assetPack != null && assetPack != "")
		{
//			if(selectedAssetPack.GetAllAssetNames ()[index].Contains(".ogg") || selectedAssetPack.GetAllAssetNames ()[index].Contains(".mp4"))
			if(currentMediaList[index].type == SuccessCentreScreen.Media.Type.Video)
			{
				//TODO: Put file path into MCP if on device
#if !UNITY_ANDROID && !UNITY_IPHONE
//				MCP.loadedVideo = selectedAssetPack.LoadAsset(selectedAssetPack.GetAllAssetNames ()[index]) as MovieTexture;
//				MCP.mediaName = MCP.loadedVideo.name;
				//TODO: Load video
				MCP.mediaName = currentMediaList[index].displayName;
				MCP.videoFilePath = currentMediaList[index].url[0];
				MCP.loadedAudio = null;
				MCP.loadedVideo = null;
#else
//				MCP.loadedFilePath = selectedAssetPack.GetAllAssetNames ()[index];
//				MCP.mediaName = MCP.loadedFilePath.Split(new char[]{'/'})[MCP.loadedFilePath.Split(new char[]{'/'}).Length - 1];
				MCP.loadedFilePath = currentMediaList[index].url[0];
				MCP.videoFilePath = currentMediaList[index].url[0];
				MCP.mediaName = currentMediaList[index].displayName;
				MCP.audioFilePath = "";
				MCP.loadedAudio = null;
#endif
			}
			else
			{
//				MCP.loadedAudio = selectedAssetPack.LoadAsset<AudioClip>(selectedAssetPack.GetAllAssetNames ()[index]);
//				MCP.mediaName = MCP.loadedAudio.name;
				MCP.loadedFilePath = currentMediaList[index].url[0];
				MCP.audioFilePath = currentMediaList[index].url[0];
				MCP.mediaName = currentMediaList[index].displayName;
				MCP.videoFilePath = "";
				MCP.loadedAudio = null;
				
#if !UNITY_ANDROID && !UNITY_IPHONE
				MCP.loadedVideo = null;
#endif
			}
		}
		else if(MCP.goalMaps != null)
		{
			MCP.gmIndex = index;
			
			MCP.pulledGoalMaps = true;
		}
		
		// Close this script.
		Close ();
	}
	
	/// <summary>
	/// Open the specified index.
	/// </summary>
	/// <param name="index">Index.</param>
	bool Open(int index)
	{
		// If the index is valid...
		if(index != -1)
		{
			StartCoroutine (OpenFile (index));
			
			return true;
		}
		
		return false;
	}
	
	/// <summary>
	/// Close this instance.
	/// </summary>
	void Close()
	{
		// Reenable the main scene canvas.
		sceneCanvas.SetActive (true);
		
		// Reenable other scripts.
		SendMessage ("FileBrowserClosed", SendMessageOptions.DontRequireReceiver);
		
		if(GetComponent<ViewGoalMap_Screen>() != null)
		{
			GetComponent<ViewGoalMap_Screen>().fileBrowserOpen = false;
		}
		
		// Destroy all file browser game objects.
		Destroy (localCanvas);
		
		// Remove this script from the object to close.
		Destroy (this);
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update ()
	{
		// Update the timer.
		clickTimer += Time.deltaTime;
		
		// Limit timers.
		if(clickTimer > 2f)
		{
			clickTimer = 2f;
		}
		
		// If the lists of folders and files is empty...
		if(currentFiles.Count <= 0)
		{
			// Get a reference to the content of the scroll rect.
			GameObject scrollContent = folderScrollArea.transform.GetChild (0).GetChild (0).gameObject;
			scrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(1f, Screen.height * 0.7f);
			
			offsetY = Screen.height * -1.2f;
			// Make the folders
			int index = 0;
			
			// Make the files.
			if(GetComponent<AVPlayer>() != null && assetPack != null && assetPack != "")
			{
//				selectedAssetPack = MCP.downloadedAssetBundle;
				
				currentMediaList = GetComponent<SuccessCentreScreen>().bronzeMedia;
				
				switch(assetPack)
				{
				case "Bronze":
//					selectedAssetPack = MCP.bronzeAssetBundle;
					break;
					
				case "Silver":
//					selectedAssetPack = MCP.silverAssetBundle;
					currentMediaList = GetComponent<SuccessCentreScreen>().silverMedia;
					break;
					
				case "Gold":
//					selectedAssetPack = MCP.goldAssetBundle;
					currentMediaList = GetComponent<SuccessCentreScreen>().goldMedia;
					break;
				}
				
//				if(selectedAssetPack.GetAllAssetNames().Length == 0)
				if(currentMediaList.Count == 0)
				{
					if(noFilesFoundLabel == null)
					{
						noFilesFoundLabel = MakeLabel (localCanvas, MCP.Text (2103)/*"No files found"*/, new Rect(Screen.width * -0.25f, Screen.height * 1.2f, Screen.width * 0.5f, Screen.height * 0.2f), TextAnchor.MiddleCenter, "InnerLabel");
						noFilesFoundLabel.name = "NoFilesFoundLabel";
						noFilesFoundLabel.transform.SetParent (scrollContent.transform, true);
						noFilesFoundLabel.transform.localScale = Vector3.one;
						fileBrowserObjects.Add (noFilesFoundLabel);
					}
				}
				else
				{
					// For each asset in pack...
//					foreach(string s in selectedAssetPack.GetAllAssetNames())
					for(int i = 0; i < currentMediaList.Count; i++)
					{
						// Make a rect for the position of the folder.
						Rect fileRect = new Rect(Screen.width * 0.51f, offsetY, Screen.width * 0.78f, Screen.height * 0.07f);
						// Make an image object for the folder background.
						GameObject backgroundObject = MakeActionButton (scrollContent, menuBackground, fileRect, Open, index);
						backgroundObject.name = "FileBackgroundObject";
						backgroundObject.GetComponent<Image>().type = Image.Type.Sliced;
						fileBrowserObjects.Add (backgroundObject);
						
						// Make an image object to show it's a file.
						GameObject fileIdentifier = MakeImage (backgroundObject, new Rect(fileRect.width * 0.01f + (Screen.width * 0.5f), -fileRect.height + (Screen.height * 0.5f), fileRect.width * 0.0625f, fileRect.height), fileSprite);
						fileIdentifier.name = "FileIdentifier";
						fileIdentifier.transform.SetParent (backgroundObject.transform);
						fileBrowserObjects.Add (fileIdentifier);
						// Make a label to show the file's name.
						GameObject fileLabel = MakeLabel (backgroundObject, currentMediaList[i].displayName, new Rect(fileRect.width * 0.075f + (Screen.width * 0.5f), -fileRect.height + (Screen.height * 0.5f), fileRect.width * 0.89f, fileRect.height), TextAnchor.MiddleLeft, "InnerLabel");
						fileLabel.name = "FileLabel";
						fileLabel.transform.SetParent (backgroundObject.transform);
						fileBrowserObjects.Add (fileLabel);
						currentFiles.Add (fileLabel);
						
						// Add the requred offset for the next object.
						offsetY += Screen.height * 0.1f;
						index++;
					}
				}
			}
			else if(MCP.goalMaps != null)
			{
				if(MCP.goalMaps.Count == 0)
				{
					if(noFilesFoundLabel == null)
					{
						noFilesFoundLabel = MakeLabel (localCanvas, MCP.Text (2103)/*"No files found"*/, new Rect(Screen.width * -0.25f, Screen.height * -0.3f, Screen.width * 0.5f, Screen.height * 0.2f), TextAnchor.MiddleCenter, "InnerLabel");
						noFilesFoundLabel.name = "NoFilesFoundLabel";
						noFilesFoundLabel.transform.SetParent (scrollContent.transform, true);
						noFilesFoundLabel.transform.localScale = Vector3.one;
						fileBrowserObjects.Add (noFilesFoundLabel);
					}
				}
				else
				{
					// For each goal map...
					foreach(MCP.GoalMap gm in MCP.goalMaps)
					{
						// Make a rect for the position of the folder.
						Rect fileRect = new Rect(Screen.width * 0.51f, offsetY, Screen.width * 0.78f, Screen.height * 0.07f);
						// Make an image object for the folder background.
						GameObject backgroundObject = MakeActionButton (scrollContent, menuBackground, fileRect, Open, index);
						backgroundObject.name = "FileBackgroundObject";
						backgroundObject.GetComponent<Image>().type = Image.Type.Sliced;
						fileBrowserObjects.Add (backgroundObject);
						
						// Make an image object to show it's a folder.
						GameObject fileIdentifier = MakeImage (backgroundObject, new Rect(fileRect.width * 0.01f + (Screen.width * 0.5f), -fileRect.height + (Screen.height * 0.5f), fileRect.width * 0.0625f, fileRect.height), fileSprite);
						fileIdentifier.name = "FileIdentifier";
						fileIdentifier.transform.SetParent (backgroundObject.transform);
						fileBrowserObjects.Add (fileIdentifier);
						// Make a label to show the folder's name.
						GameObject fileLabel = MakeLabel (backgroundObject, gm.name, new Rect(fileRect.width * 0.075f + (Screen.width * 0.5f), -fileRect.height + (Screen.height * 0.5f), fileRect.width * 0.89f, fileRect.height), TextAnchor.MiddleLeft, "InnerLabel");
						fileLabel.name = "FileLabel";
						fileLabel.transform.SetParent (backgroundObject.transform);
						fileBrowserObjects.Add (fileLabel);
						currentFiles.Add (fileLabel);
						
						// Add the requred offset for the next object.
						offsetY += Screen.height * 0.1f;
						index++;
					}
					
					// Remove the no files found text.
					if(noFilesFoundLabel != null)
					{
						Destroy (noFilesFoundLabel);
					}
				}
			}
			
			// If the files have just been made...
			if(index > 0)
			{
				// Make the scroll content size large enough to contain all of the content.
				if(offsetY + (Screen.height * 0.6f) > Screen.height * 0.6f)
				{
//					scrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(folderScrollArea.GetComponent<RectTransform>().sizeDelta.x, offsetY + (Screen.height * 0.675f)/* - (Screen.height * 0.7f)*/);
					scrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(folderScrollArea.GetComponent<RectTransform>().sizeDelta.x, offsetY + (Screen.height * 1.175f)/* - (Screen.height * 0.7f)*/);
				}
				else
				{
					scrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(folderScrollArea.GetComponent<RectTransform>().sizeDelta.x, Screen.height * 0.6f);
				}
				
				// Move the scroll content to start at the far left.
				if(offsetY >= Screen.height * -0.3f)
				{
					scrollContent.GetComponent<RectTransform>().transform.localPosition = new Vector2(0, (Mathf.Abs (offsetY) * -4) - (Screen.height * 0.3f) * 1000f);
				}
				else
				{
					scrollContent.GetComponent<RectTransform>().transform.localPosition = new Vector2(0, Screen.height * -0.3f);
				}
				
				// Make sure the scroll bar is in the right place.
				updateVelocity = true;
				index = 0;
			}
		}
		else if(updateVelocity)
		{
			// Make sure the scroll bar is in the right place.
			folderScrollArea.GetComponent<ScrollRect>().velocity = new Vector2(0, -100f);
			folderScrollArea.transform.GetChild (2).gameObject.GetComponent<Scrollbar>().size = 1;
			
			updateVelocity = false;
			
			// Reset offsetY.
			offsetY = 0;
		}
		
		if(openingPopup != null)
		{
			spinnerRot -= Time.deltaTime * 75f;
			openingSpinner.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, spinnerRot);
		}
	}
	
	public override void DoGUI () {}
}
