using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ClientMessages_Screen : MenuScreen
{
	private List<GameObject> mainScreenGUI;
	private List<GameObject> readMessageGUI;
	private List<GameObject> sendMessageGUI;
	private List<GameObject> messageObjects;
	private List<GameObject> viewFriendGMGUI;
	private GameObject localCanvas;
	private GameObject scrollAreaObject;
	private GameObject readReplyButton;
	private GameObject readSentFromTextbox;
	private GameObject readSubjectTextbox;
	private GameObject readMessageTextbox;
	private GameObject sendSendToTextbox;
	private GameObject sendSubjectTextbox;	
	private GameObject sendMessageTextbox;
	private GameObject sendBoldButton;
	private GameObject sendItalicButton;
	private GameObject sendAlignLeftButton;
	private GameObject sendAlignCentreButton;
	private GameObject sendAlignRightButton;
	private GameObject sendSendMessageButton;
	private GameObject noMessageFoundLabel;
	private GameObject popup;
	
	public List<MyMessages> friend;
	private Rect screenSize;
	private Rect topPanelRect;
	private Rect leftPanelRect;
	private Rect rightPanelRect;
	private bool posUpdated = false;
	private float teamButtonsWidth;
	private float bottomPanelSpacer;
	private float elementHeight;
	private MCP.ZingMessage selectedMessage;
	public GameObject gameInformationObject;
	private string acceptedFriendName;
	private string sendMessage = "";
	private GUIStyle sendMessageStyle;
#if !UNITY_ANDROID && !UNITY_IPHONE
	private TextEditor editor;
#else
	private TouchScreenKeyboard tsk;
#endif

#if UNITY_EDITOR
//	private int startSelectPos = 0;
//	private int currentSelectPos = 0;
#endif
	
	private Sprite[] boldSprite;
	private Sprite[] italicSprite;
	private Sprite[] underlineSprite;
	private Sprite[] fontBiggerSprite;
	private Sprite[] fontSmallerSprite;
	private Sprite[] leftAlignSprite;
	private Sprite[] centreAlignSprite;
	private Sprite[] rightAlignSprite;
	private Sprite[] refreshSprite;
	
//#if UNITY_ANDROID || UNITY_IPHONE
	private bool makeBold = false;		// Used on device
	private bool makeItalic = false;	// Used on device
//#endif
	
	private enum InternalState
	{
		displayPMs,
		sendPM,
		readPM,
		sentPopup,
	};
	
	private InternalState internalState;
	
	[System.Serializable]
	public class MyMessages
	{
		public string name;
		public string date;
		public string course;
		public string contact;
		public string email;
		public string phoneNumber;
	}

	public bool goalMapUpdated = false;
	
	private bool autoRefreshMessages = false;
	
	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		// Initialize the base class.
		base.Init ();

		// Set default variables.
		internalState = InternalState.displayPMs;
		mainScreenGUI = new List<GameObject>();
		readMessageGUI = new List<GameObject>();
		sendMessageGUI = new List<GameObject>();
		messageObjects = new List<GameObject>();
		viewFriendGMGUI = new List<GameObject>();
		boldSprite = new Sprite[2];
		italicSprite = new Sprite[2];
		underlineSprite = new Sprite[2];
		fontBiggerSprite = new Sprite[2];
		fontSmallerSprite = new Sprite[2];
		leftAlignSprite = new Sprite[2];
		centreAlignSprite = new Sprite[2];
		rightAlignSprite = new Sprite[2];
		refreshSprite = new Sprite[2];
		sendMessageStyle = new GUIStyle();
		sendMessageStyle.alignment = TextAnchor.UpperLeft;
		sendMessageStyle.font = defaultFont;
		sendMessageStyle.fontSize = (int)_fontSize;
		sendMessageStyle.normal.background = menuBackground.texture;
		sendMessageStyle.normal.textColor = bmOrange;
		sendMessageStyle.fontStyle = FontStyle.Normal;
		sendMessageStyle.richText = true;
		sendMessageStyle.clipping = TextClipping.Clip;
		sendMessageStyle.wordWrap = true;
		
		// Load resources.
		boldSprite[0] = Resources.Load<Sprite>("GUI/richtext_bold");
		boldSprite[1] = Resources.Load<Sprite>("GUI/richtext_bold_active");
		italicSprite[0] = Resources.Load<Sprite>("GUI/richtext_italic");
		italicSprite[1] = Resources.Load<Sprite>("GUI/richtext_italic_active");
		underlineSprite[0] = Resources.Load<Sprite>("GUI/richtext_underline");
		underlineSprite[1] = Resources.Load<Sprite>("GUI/richtext_underline_active");
		fontBiggerSprite[0] = Resources.Load<Sprite>("GUI/richtext_sizeincrease");
		fontBiggerSprite[1] = Resources.Load<Sprite>("GUI/richtext_sizeincrease_active");
		fontSmallerSprite[0] = Resources.Load<Sprite>("GUI/richtext_sizedecrease");
		fontSmallerSprite[1] = Resources.Load<Sprite>("GUI/richtext_sizedecrease_active");
		leftAlignSprite[0] = Resources.Load<Sprite>("GUI/richtext_alignleft");
		leftAlignSprite[1] = Resources.Load<Sprite>("GUI/richtext_alignleft_active");
		centreAlignSprite[0] = Resources.Load<Sprite>("GUI/richtext_aligncentre");
		centreAlignSprite[1] = Resources.Load<Sprite>("GUI/richtext_aligncentre_active");
		rightAlignSprite[0] = Resources.Load<Sprite>("GUI/richtext_alignright");
		rightAlignSprite[1] = Resources.Load<Sprite>("GUI/richtext_alignright_active");
		refreshSprite[0] = Resources.Load<Sprite>("GUI/refresh");
		refreshSprite[1] = Resources.Load<Sprite>("GUI/refresh_active");
		
#if UNITY_ANDROID || UNITY_IOS
		tsk = new TouchScreenKeyboard("", TouchScreenKeyboardType.Default, true, true, false, false, "");
		tsk.active = false;
#endif
		
		// Create the GUI.
		CreateGUI ();
		
		localCanvas.transform.localScale = Vector2.zero;
		
		// Get the messages from server
		if(MCP.userInfo != null)
		{
			MCP.zingMessages = new MCP.ZingMessages();
			MCP.zingMessages.messages = new List<MCP.ZingMessage>();
			StartCoroutine (MCP.GetMessages ());
		}
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Make the canvas.
		localCanvas = MakeCanvas ();
		
		// Make the side gizmo.
		MakeSideGizmo (localCanvas, true, true);
		
		// Make background.
		MakeImage (localCanvas, new Rect(Screen.width * 0.01f, Screen.height * 0.15f, Screen.width * 0.98f, Screen.height * 0.83f), window_back, true).name = "BackgroundPanel";
		
		/* Make the main screen gui.*/
		// Make title.
		GameObject mainTitle = MakeTitleBar (localCanvas, MCP.Text (2901)/*"Messages"*/);
		mainTitle.name = "MainTitle";
		mainScreenGUI.Add (mainTitle);
		
		// Make scroll area.
		scrollAreaObject = MakeScrollRect (localCanvas, new Rect(Screen.width * 0.1f, Screen.height * 0.3f, Screen.width * 0.8f, Screen.height  * 0.5f), null);
		scrollAreaObject.name = "ScrollAreaObject";
		scrollAreaObject.GetComponent<ScrollRect>().horizontal = false;
		scrollAreaObject.GetComponent<ScrollRect>().velocity = new Vector2(0, 1000f);
		mainScreenGUI.Add (scrollAreaObject);
		
		// Make column header background
		GameObject columnHeaderBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.1f, Screen.height * 0.24f, Screen.width * 0.8f, Screen.height * 0.09f), window_back, true);
		columnHeaderBackground.name = "ColumnHeaderBackground";
		mainScreenGUI.Add (columnHeaderBackground);
		
		// Make date header
		GameObject dateHeaderLabel = MakeLabel (localCanvas, MCP.Text (2902)/*"Date"*/, new Rect(Screen.width * 0.15f, Screen.height * 0.24f, Screen.width * 0.15f, Screen.height * 0.1f), TextAnchor.MiddleLeft, "InnerLabel");
		dateHeaderLabel.name = "DateHeaderLabel";
		mainScreenGUI.Add (dateHeaderLabel);
		
		// Make from header
		GameObject fromHeaderLabel = MakeLabel (localCanvas, MCP.Text (210)/*"From"*/, new Rect(Screen.width * 0.335f, Screen.height * 0.24f, Screen.width * 0.15f, Screen.height * 0.1f), TextAnchor.MiddleLeft, "InnerLabel");
		fromHeaderLabel.name = "FromHeaderLabel";
		mainScreenGUI.Add (fromHeaderLabel);
		
		// Make subject header
		GameObject subjectHeaderLabel = MakeLabel (localCanvas, MCP.Text (211)/*"Subject"*/, new Rect(Screen.width * 0.525f, Screen.height * 0.24f, Screen.width * 0.375f, Screen.height * 0.1f), TextAnchor.MiddleLeft, "InnerLabel");
		subjectHeaderLabel.name = "SubjectHeaderLabel";
		mainScreenGUI.Add (subjectHeaderLabel);
		
		// Make refresh button
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			RefreshMessages
		};
		GameObject refreshButton = MakeButton (localCanvas, "", refreshSprite[0], new Rect(Screen.width * 0.8f, Screen.height * 0.245f, Screen.height * 0.08f, Screen.height * 0.08f), funcList);
		refreshButton.name = "RefreshButton";
		
		UnityEngine.UI.Navigation nav = new UnityEngine.UI.Navigation();
		nav.mode = UnityEngine.UI.Navigation.Mode.None;
		// Make hover sprite
		refreshButton.GetComponent<Button>().navigation = nav;
		refreshButton.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		SpriteState spriteState = new SpriteState();
		refreshButton.GetComponent<Button>().targetGraphic = refreshButton.GetComponent<Image>();
		spriteState.highlightedSprite = refreshSprite[1];
		spriteState.pressedSprite = refreshSprite[1];
		refreshButton.GetComponent<Button>().spriteState = spriteState;
		
		mainScreenGUI.Add (refreshButton);
		
		// Make new message button.
		funcList = new List<MyVoidFunc>
		{
			NewMessage
		};
		GameObject newMessageButton = MakeButton (localCanvas, MCP.Text (2909)/*"New"*/, buttonBackground, new Rect(Screen.width * 0.7f, Screen.height * 0.86f, Screen.width * 0.2f, Screen.height * 0.08f), funcList);
		newMessageButton.name = "NewMessageButton";
		mainScreenGUI.Add (newMessageButton);
		
		noMessageFoundLabel = MakeLabel (localCanvas, "No Messages Found", new Rect(Screen.width * 0.3f, Screen.height * 0.45f, Screen.width * 0.4f, Screen.height * 0.2f), TextAnchor.MiddleCenter);
		noMessageFoundLabel.name = "NoMessageFoundLabel";
		noMessageFoundLabel.transform.SetParent (scrollAreaObject.transform.GetChild (0).GetChild (0));
		noMessageFoundLabel.transform.localScale = Vector3.one;
		mainScreenGUI.Add (noMessageFoundLabel);
		
		/* Make the read message GUI */
		// Make title
		ColorBlock cb = new ColorBlock();
		cb.colorMultiplier = 1f;
		cb.fadeDuration = 0.1f;
		cb.normalColor = Color.white;
		cb.disabledColor = Color.white;
		cb.highlightedColor = Color.white;
		cb.pressedColor = Color.white;
		
		GameObject readTitle = MakeTitleBar (localCanvas, MCP.Text (2901)/*"Messages"*/, "ScreenTitle");
		readTitle.name = "ReadTitle";
		readMessageGUI.Add (readTitle);
		
		// Make sent from label
		GameObject readSentFromLabel = MakeLabel (localCanvas, MCP.Text (2903)/*"Sent from: "*/, new Rect(Screen.width * 0.05f, Screen.height * 0.17f, Screen.width * 0.15f, Screen.height * 0.1f), TextAnchor.MiddleLeft, "InnerLabel");
		readSentFromLabel.name = "SentFromLabel";
		readMessageGUI.Add (readSentFromLabel);
		// Make sent from textbox
		string readSentFrom = "Cortex";
		readSentFromTextbox = MakeInputField (localCanvas, readSentFrom, new Rect(Screen.width * 0.2f, Screen.height * 0.18f, Screen.width * 0.75f, Screen.height * 0.08f), false, menuBackground);
		readSentFromTextbox.name = "SentFromTextbox";
		readSentFromTextbox.GetComponent<Text>().color = bmOrange;
		readSentFromTextbox.GetComponent<InputField>().interactable = false;
		readSentFromTextbox.GetComponent<InputField>().colors = cb;
		readMessageGUI.Add (readSentFromTextbox.transform.parent.gameObject);
		
		// Make subject label
		GameObject readSubjectLabel = MakeLabel (localCanvas, MCP.Text (211)/*"Subject"*/ + ": ", new Rect(Screen.width * 0.05f, Screen.height * 0.3f, Screen.width * 0.15f, Screen.height * 0.1f), TextAnchor.MiddleLeft, "InnerLabel");
		readSubjectLabel.name = "SubjectLabel";
		readMessageGUI.Add (readSubjectLabel);
		// Make subject textbox
		string readSubject = "";
		readSubjectTextbox = MakeInputField (localCanvas, readSubject, new Rect(Screen.width * 0.2f, Screen.height * 0.315f, Screen.width * 0.75f, Screen.height * 0.08f), false, menuBackground);
		readSubjectTextbox.name = "SubjectTextbox";
		readSubjectTextbox.GetComponent<Text>().color = bmOrange;
		readSubjectTextbox.GetComponent<InputField>().interactable = false;
		readSubjectTextbox.GetComponent<InputField>().colors = cb;
		readMessageGUI.Add (readSubjectTextbox.transform.parent.gameObject);
		
		// Make message label
		GameObject readMessageLabel = MakeLabel (localCanvas, MCP.Text (2904)/*"Message: "*/, new Rect(Screen.width * 0.05f, Screen.height * 0.45f, Screen.width * 0.15f, Screen.height * 0.1f), TextAnchor.MiddleLeft, "InnerLabel");
		readMessageLabel.name = "MessageLabel";
		readMessageGUI.Add (readMessageLabel);
		// Make message textbox
		string readMessage = "";
		readMessageTextbox = MakeInputField (localCanvas, readMessage, new Rect(Screen.width * 0.2f, Screen.height * 0.45f, Screen.width * 0.75f, Screen.height * 0.375f), false, menuBackground);
		readMessageTextbox.name = "MessageTextbox";
		readMessageTextbox.GetComponent<Text>().color = bmOrange;
		readMessageTextbox.GetComponent<Text>().resizeTextForBestFit = false;
		readMessageTextbox.GetComponent<Text>().fontSize = (int)_fontSize;
		readMessageTextbox.GetComponent<InputField>().interactable = false;
		readMessageTextbox.GetComponent<InputField>().lineType = InputField.LineType.MultiLineNewline;
		readMessageTextbox.GetComponent<InputField>().colors = cb;
		readMessageGUI.Add (readMessageTextbox.transform.parent.gameObject);
		
		// Make back button background.
		GameObject readBackButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.15f, Screen.height * 0.86f, Screen.width * 0.2f, Screen.height * 0.08f), buttonBackground, true);
		readBackButtonBackground.name = "ReadBackButtonBackground";
		readMessageGUI.Add (readBackButtonBackground);
		// Make back button
		funcList = new List<MyVoidFunc>
		{
			Back
		};
		GameObject readBackButton = MakeButton (localCanvas, MCP.Text (219)/*"Back"*/, null, new Rect(Screen.width * 0.15f, Screen.height * 0.86f, Screen.width * 0.2f, Screen.height * 0.08f), funcList, "OrangeText");
		readBackButton.name = "ReadBackButton";
		readMessageGUI.Add (readBackButton);
		
		// Make delete button background.
		GameObject readDeleteButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.4f, Screen.height * 0.86f, Screen.width * 0.2f, Screen.height * 0.08f), buttonBackground, true);
		readDeleteButtonBackground.name = "ReadDeleteButtonBackground";
		readMessageGUI.Add (readDeleteButtonBackground);
		// Make delete button
		funcList = new List<MyVoidFunc>
		{
			DeleteMessage
		};
		GameObject readDeleteButton = MakeButton (localCanvas, MCP.Text (2905)/*"Delete"*/, null, new Rect(Screen.width * 0.4f, Screen.height * 0.86f, Screen.width * 0.2f, Screen.height * 0.08f), funcList, "OrangeText");
		readDeleteButton.name = "ReadDeleteButton";
		readMessageGUI.Add (readDeleteButton);
		
		// Make reply button background.
		GameObject readReplyButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.65f, Screen.height * 0.86f, Screen.width * 0.2f, Screen.height * 0.08f), buttonBackground, true);
		readReplyButtonBackground.name = "ReadReplyButtonBackground";
		readMessageGUI.Add (readReplyButtonBackground);
		// Make reply button
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				// Change functionality depending on message type.
				switch(selectedMessage.type)
				{
				case "3":		// Friend request
					AcceptFriend();
					break;
				case "20":		// View goal map
					ViewGoalMap();
					break;
							
				default:
					ReplyToMessage();
					break;
				}
			}
		};
		readReplyButton = MakeButton (localCanvas, MCP.Text (2906)/*"Reply"*/, null, new Rect(Screen.width * 0.65f, Screen.height * 0.86f, Screen.width * 0.2f, Screen.height * 0.08f), funcList, "OrangeText");
		readReplyButton.name = "ReadReplyButton";
		readMessageGUI.Add (readReplyButton);
		
		/* Make the send message GUI */
		// Make send message title
		GameObject sendTitle = MakeTitleBar (localCanvas, MCP.Text (2907)/*"New Message"*/, "ScreenTitle");
		sendTitle.name = "SendTitle";
		sendMessageGUI.Add (sendTitle);
		
		// Make sent from label
		GameObject sendSendToLabel = MakeLabel (localCanvas, MCP.Text (2908)/*"Send to: "*/, new Rect(Screen.width * 0.05f, Screen.height * 0.17f, Screen.width * 0.15f, Screen.height * 0.1f), TextAnchor.MiddleLeft, "InnerLabel");
		sendSendToLabel.name = "SendToLabel";
		sendMessageGUI.Add (sendSendToLabel);
		// Make sent from textbox
		sendSendToTextbox = MakeInputField (localCanvas, "", new Rect(Screen.width * 0.2f, Screen.height * 0.18f, Screen.width * 0.75f, Screen.height * 0.08f), false, menuBackground);
		sendSendToTextbox.name = "SendToTextbox";
		sendSendToTextbox.GetComponent<Text>().color = bmOrange;
		sendSendToTextbox.GetComponent<InputField>().interactable = false;
		sendMessageGUI.Add (sendSendToTextbox.transform.parent.gameObject);
		
		// Make subject label
		GameObject sendSubjectLabel = MakeLabel (localCanvas, MCP.Text (211)/*"Subject"*/ + ": ", new Rect(Screen.width * 0.05f, Screen.height * 0.3f, Screen.width * 0.15f, Screen.height * 0.1f), TextAnchor.MiddleLeft, "InnerLabel");
		sendSubjectLabel.name = "SendSubjectLabel";
		sendMessageGUI.Add (sendSubjectLabel);
		// Make subject textbox
		string sendSubject = "";
		sendSubjectTextbox = MakeInputField (localCanvas, sendSubject, new Rect(Screen.width * 0.2f, Screen.height * 0.315f, Screen.width * 0.75f, Screen.height * 0.08f), false, menuBackground);
		sendSubjectTextbox.name = "SendSubjectTextbox";
		sendSubjectTextbox.GetComponent<Text>().color = bmOrange;
		sendMessageGUI.Add (sendSubjectTextbox.transform.parent.gameObject);
		
		// Make message label
		GameObject sendMessageLabel = MakeLabel (localCanvas, MCP.Text (2904)/*"Message: "*/, new Rect(Screen.width * 0.05f, Screen.height * 0.45f, Screen.width * 0.15f, Screen.height * 0.1f), TextAnchor.MiddleLeft, "InnerLabel");
		sendMessageLabel.name = "SendMessageLabel";
		sendMessageGUI.Add (sendMessageLabel);
		
		// Make bold button
		funcList = new List<MyVoidFunc>
		{
			ControlBold
		};
		sendBoldButton = MakeButton (localCanvas, "", boldSprite[0], new Rect(Screen.width * 0.125f, Screen.height * 0.86f, Screen.width * 0.05f, Screen.width * 0.05f), funcList);
		sendBoldButton.name = "SendBoldButton";
		sendMessageGUI.Add (sendBoldButton);
		
		// Make italic button
		funcList = new List<MyVoidFunc>
		{
			ControlItalic
		};
		sendItalicButton = MakeButton (localCanvas, "", italicSprite[0], new Rect(Screen.width * 0.2f, Screen.height * 0.86f, Screen.width * 0.05f, Screen.width * 0.05f), funcList);
		sendItalicButton.name = "SendItalicButton";
		sendMessageGUI.Add (sendItalicButton);
		
		// Make font bigger button
		funcList = new List<MyVoidFunc>
		{
			ControlFontBigger
		};
		GameObject sendFontBiggerButton = MakeButton (localCanvas, "", fontBiggerSprite[0], new Rect(Screen.width * 0.3f, Screen.height * 0.86f, Screen.width * 0.05f, Screen.width * 0.05f), funcList);
		sendFontBiggerButton.name = "SendFontBiggerButton";
		sendMessageGUI.Add (sendFontBiggerButton);
		
		// Make font smaller button
		funcList = new List<MyVoidFunc>
		{
			ControlFontSmaller
		};
		GameObject sendFontSmallerButton = MakeButton (localCanvas, "", fontSmallerSprite[0], new Rect(Screen.width * 0.375f, Screen.height * 0.86f, Screen.width * 0.05f, Screen.width * 0.05f), funcList);
		sendFontSmallerButton.name = "SendFontSmallerButton";
		sendMessageGUI.Add (sendFontSmallerButton);
		
		// Make left align button
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				ControlAlign (TextAnchor.UpperLeft);
			}
		};
		sendAlignLeftButton = MakeButton (localCanvas, "", leftAlignSprite[1], new Rect(Screen.width * 0.475f, Screen.height * 0.86f, Screen.width * 0.05f, Screen.width * 0.05f), funcList);
		sendAlignLeftButton.name = "SendAlignLeftButton";
		sendMessageGUI.Add (sendAlignLeftButton);
		
		// Make centre align button
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				ControlAlign (TextAnchor.UpperCenter);
			}
		};
		sendAlignCentreButton = MakeButton (localCanvas, "", centreAlignSprite[0], new Rect(Screen.width * 0.55f, Screen.height * 0.86f, Screen.width * 0.05f, Screen.width * 0.05f), funcList);
		sendAlignCentreButton.name = "SendAlignCentreButton";
		sendMessageGUI.Add (sendAlignCentreButton);
		
		// Make right align button
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
				ControlAlign (TextAnchor.UpperRight);
			}
		};
		sendAlignRightButton = MakeButton (localCanvas, "", rightAlignSprite[0], new Rect(Screen.width * 0.625f, Screen.height * 0.86f, Screen.width * 0.05f, Screen.width * 0.05f), funcList);
		sendAlignRightButton.name = "SendAlignRightButton";
		sendMessageGUI.Add (sendAlignRightButton);
		
		// Make send button background
		GameObject sendSendMessageButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.7f, Screen.height * 0.86f, Screen.width * 0.2f, Screen.height * 0.08f), buttonBackground, true);
		sendSendMessageButtonBackground.name = "SendSendMessageButtonBackground";
		sendMessageGUI.Add (sendSendMessageButtonBackground);
		// Make send button
		funcList = new List<MyVoidFunc>
		{
			SendUserMessage
		};
		sendSendMessageButton = MakeButton (localCanvas, MCP.Text (213)/*"Send"*/, null, new Rect(Screen.width * 0.7f, Screen.height * 0.86f, Screen.width * 0.2f, Screen.height * 0.08f), funcList, "InnerLabel");
		sendSendMessageButton.name = "SendSendMessageButton";
		sendSendMessageButton.GetComponent<Button>().image = sendSendMessageButtonBackground.GetComponent<Image>();
		sendSendMessageButton.transform.GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
		sendMessageGUI.Add (sendSendMessageButton);
		
		for(int i = 0; i < readMessageGUI.Count; i++)
		{
			readMessageGUI[i].SetActive (false);
		}
		for(int i = 0; i < sendMessageGUI.Count; i++)
		{
			sendMessageGUI[i].SetActive (false);
		}
	}
	
	/// <summary>
	/// Returns whether the message begins with an alignment tag.
	/// </summary>
	/// <returns><c>true</c>, if the message begins with alignment tag, <c>false</c> otherwise.</returns>
	private bool BeginsWithAlignmentTag(string message)
	{
		if(message.Length > 2)
		{
			if(message[0] == '<' && message[2] == '>')
			{
				char[] validTags = new char[]{'r', 'e', 'l'};
				
				for(int i = 0; i < validTags.Length; i++)
				{
					if(message[1] == validTags[i])
					{
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	/// <summary>
	/// Sets the message from info.
	/// </summary>
	void SetMessageFromInfo()
	{
		// Set from text
		readSentFromTextbox.GetComponent<InputField>().text = selectedMessage.from;
		
		// Set subject text

		bool recordNumber = false;
		string gmIndexString = "";
		for( int i = 0; i < selectedMessage.subject.Length; i++ )
		{
			if( selectedMessage.subject[i] == '>' )
			{
				gmIndexString = gmIndexString + selectedMessage.subject[i];
				recordNumber = false;
				break;
			}
			
			if( recordNumber )
			{
				gmIndexString = gmIndexString + selectedMessage.subject[i];
			}
			
			if( selectedMessage.subject[i] == '<' )
			{
				gmIndexString = gmIndexString + selectedMessage.subject[i];
				recordNumber = true;
			}
		}

		string tempString = "";
		
		if( gmIndexString != "" )
		{
			tempString = selectedMessage.subject.Replace( gmIndexString, "" );
		}
		else
		{
			tempString = selectedMessage.subject;
		}

		readSubjectTextbox.GetComponent<InputField>().text = tempString;
		
		// If the message beigns with an alignment tag...
		if(BeginsWithAlignmentTag(selectedMessage.message))
		{
			// Set the alignment of the text from the first 3 characters.
			switch(selectedMessage.message[1])
			{
			case 'l':
				readMessageTextbox.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
				break;
			case 'e':
				readMessageTextbox.GetComponent<Text>().alignment = TextAnchor.UpperCenter;
				break;
			case 'r':
				readMessageTextbox.GetComponent<Text>().alignment = TextAnchor.UpperRight;
				break;
			}
			
			// Set the message text, removing the tag for the alignment.
			readMessageTextbox.GetComponent<InputField>().text = selectedMessage.message.Substring (3);
		}
		// Otherwise, if there are any characters in the message string...
		else if(selectedMessage.message.Length > 0)
		{
			// Set the text to the message.
			readMessageTextbox.GetComponent<InputField>().text = selectedMessage.message;
		}
		// Otherwise, if the message is empty, just use an empty string.
		else
		{
			readMessageTextbox.GetComponent<InputField>().text = "";
		}
	}
	
	/// <summary>
	/// Sets the message to info.
	/// </summary>
	void SetMessageToInfo()
	{
		// Set from text
		sendSendToTextbox.GetComponent<InputField>().text = selectedMessage.from;
		
		// Set subject text
		sendSubjectTextbox.GetComponent<InputField>().text = "RE: " + selectedMessage.subject;
	}
	
	/// <summary>
	/// Creates the message object.
	/// </summary>
	/// <returns>The message object.</returns>
	/// <param name="currentMessage">Current message.</param>
	/// <param name="yOffset">Y offset.</param>
	private GameObject CreateMessageObject(MCP.ZingMessage currentMessage, float yOffset)
	{
		// Make background image as parent.
		GameObject backgroundImage = MakeImage (localCanvas, new Rect(Screen.width * -0.4f, (Screen.height * -0.05f) + (Screen.height * 0.1f * (yOffset - 1f)), Screen.width * 0.8f, Screen.height * 0.1f), menuBackground);
		backgroundImage.name = "BackgroundImage";
		// Make the background invisible
		Destroy(backgroundImage.GetComponent<Image>());
		
		// Make date label.
		string dateString = GetDateString (currentMessage.date);
		
		GameObject dateLabel = MakeLabel (localCanvas, dateString, new Rect(Screen.width * -0.38f, (Screen.height * -0.05f) + (Screen.height * 0.1f * (yOffset - 1f)), Screen.width * 0.2f, Screen.height * 0.06f));
		dateLabel.name = "DateLabel";
		dateLabel.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
		dateLabel.transform.SetParent (backgroundImage.transform, true);
		
		// Make from label.
		GameObject fromLabel = MakeLabel (localCanvas, currentMessage.from, new Rect(Screen.width * -0.16f, (Screen.height * -0.05f) + (Screen.height * 0.1f * (yOffset - 1f)), Screen.width * 0.15f, Screen.height * 0.06f));
		fromLabel.name = "FromLabel";
		fromLabel.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
		fromLabel.transform.SetParent (backgroundImage.transform, true);
		
		// Make subject label.

		bool recordNumber = false;
		string gmIndexString = "";
		for( int i = 0; i < currentMessage.subject.Length; i++ )
		{
			if( currentMessage.subject[i] == '>' )
			{
				gmIndexString = gmIndexString + currentMessage.subject[i];
				recordNumber = false;
				break;
			}
			
			if( recordNumber )
			{
				gmIndexString = gmIndexString + currentMessage.subject[i];
			}
			
			if( currentMessage.subject[i] == '<' )
			{
				gmIndexString = gmIndexString + currentMessage.subject[i];
				recordNumber = true;
			}
		}

		string tempString = "";

		if( gmIndexString != "" )
		{
			tempString = currentMessage.subject.Replace( gmIndexString, "" );
		}
		else
		{
			tempString = currentMessage.subject;
		}

		GameObject subjectLabel = MakeLabel (localCanvas, tempString, new Rect(Screen.width * 0.02f, (Screen.height * -0.05f) + (Screen.height * 0.1f * (yOffset - 1f)), Screen.width * 0.375f, Screen.height * 0.06f));

		subjectLabel.name = "SubjectLabel";
		subjectLabel.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
		subjectLabel.transform.SetParent (backgroundImage.transform, true);
		
		// Make message button.
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			delegate
			{
				// Set selected message.
				selectedMessage = currentMessage;
				
				// Change the current state
				internalState = InternalState.readPM;
				// Disable the displayPM objects.
				for(int i = 0; i < readMessageGUI.Count; i++)
				{
					readMessageGUI[i].SetActive (true);
				}
				// Enable the readPM objects.
				for(int i = 0; i < mainScreenGUI.Count; i++)
				{
					mainScreenGUI[i].SetActive (false);
				}
				
				SetMessageFromInfo();
				
				// Process different message types.
				switch(selectedMessage.type)
				{
				case "3":
					readReplyButton.transform.GetChild (0).gameObject.GetComponent<Text>().text = MCP.Text (609)/*"Accept"*/;
					break;
					
				case "4":
					readReplyButton.transform.GetChild (0).gameObject.GetComponent<Text>().text = MCP.Text (2906)/*"Reply"*/;
					// Get friend's info, flag and add in update.
					StartCoroutine( MCP.GetFriendInfo(selectedMessage.from));
					break;
					
				case "20":
					readReplyButton.transform.GetChild (0).gameObject.GetComponent<Text>().text = MCP.Text (610)/*"View GoalMap"*/;
					break;
					
				default:
					readReplyButton.transform.GetChild (0).gameObject.GetComponent<Text>().text = MCP.Text (2906)/*"Reply"*/;
					break;
				}
			}
		};
		GameObject messageButton = MakeButton (localCanvas, "", null, new Rect(Screen.width * -0.4f, (Screen.height * -0.05f) + (Screen.height * 0.1f * (yOffset - 1f)), Screen.width * 0.8f, Screen.height * 0.1f), funcList);
		messageButton.name = "MessageButton";
		messageButton.transform.SetParent (backgroundImage.transform, true);
		
		return backgroundImage;
	}
	
	/// <summary>
	/// Refreshs the messages.
	/// </summary>
	private void RefreshMessages()
	{
		// Destroy the current message objects.
		for(int i = messageObjects.Count - 1; i >= 0; i--)
		{
			mainScreenGUI.Remove (messageObjects[i]);
			Destroy (messageObjects[i]);
			messageObjects.RemoveAt (i);
		}
		
		// Clear the message list in MCP
		MCP.zingMessages = new MCP.ZingMessages();
		MCP.zingMessages.messages = new List<MCP.ZingMessage>();
		
		// Get the messages from the server.
		if(MCP.userInfo != null)
		{
			StartCoroutine (MCP.GetMessages ());
		}
		
		//		scrollAreaObject.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform>().localScale = Vector3.one;
		scrollAreaObject.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform>().localPosition = new Vector3(0, Screen.height * -0.5f, 0);
		// Give the scroll area velocity to avoid having the scroll bar off the screen.
		scrollAreaObject.GetComponent<ScrollRect>().velocity = new Vector2(0, 1000f);
		scrollAreaObject.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(scrollAreaObject.GetComponent<RectTransform>().sizeDelta.x, Screen.height * 0.5f);
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update ()
	{
		if(MCP.userInfo != null && MCP.friendsListNeedsUpdating)
		{
			// Accept the friend.
			StartCoroutine (MCP.AcceptFriend ( acceptedFriendName ));
			MCP.friendsListNeedsUpdating = false;
		}
		
		// If the state is in the sent popup...
		switch(internalState)
		{
		case InternalState.sentPopup:
			// If the popup has been destroyed.
			if(popup == null)
			{
				// Change state back to showing messages.
				internalState = InternalState.displayPMs;
				
				// Reenable the main screen GUI.
				for(int i = 0; i < mainScreenGUI.Count; i++)
				{
					mainScreenGUI[i].SetActive (true);
				}
			}
			break;
			
		case InternalState.displayPMs:
			selectedMessage = null;
			break;

		case InternalState.readPM:
			if( !MCP.isTransmitting )
			{
				readReplyButton.GetComponent<Button>().interactable = true;
			}
			else
			{
				readReplyButton.GetComponent<Button>().interactable = false;
			}
			break;
			
		case InternalState.sendPM:
			// If there is a selected user, don't allow the user to change the send to username.
			if(selectedMessage == null)
			{
				sendSendToTextbox.GetComponent<InputField>().interactable = true;
			}
			else
			{
				sendSendToTextbox.GetComponent<InputField>().interactable = false;
			}
			
			// If there is no send to username, don't let the usersend their message.
			if(sendSendToTextbox.GetComponent<Text>().text == "")
			{
				sendSendMessageButton.GetComponent<Button>().interactable = false;
			}
			else
			{
				sendSendMessageButton.GetComponent<Button>().interactable = true;
			}
			
//#if (!UNITY_ANDROID && !UNITY_IPHONE) || UNITY_EDITOR
#if UNITY_ANDROID || UNITY_IPHONE
			if(makeBold)
			{
				// Stop the message becoming the bold tag when all is selected and removed.
				if(sendMessage == "<b>")
				{
					sendMessage = "";
				}
				
				// Turn off bold if the tags are not in the message string.
				if(!sendMessage.Contains("<b>"))
				{
					ControlBold();
				}
				// Make sure that if the bold tag is opened, that it is closed. Used to counter selecting the end of the message and removing the closing tag.
				else
				{
					if(!sendMessage.Contains("</b>"))
					{
						sendMessage += "</b>";
					}	
				}
			}
			
			if(makeItalic)
			{
				// Stop the message becoming the italic tag when all is selected and removed.
				if(sendMessage == "<i>")
				{
					sendMessage = "";
				}
				
				// Turn off italic if the tags are not in the message string.
				if(!sendMessage.Contains("<i>"))
				{
					ControlItalic ();
				}
				// Make sure that if the itslic tag is opened, that it is closed. Used to counter selecting the end of the message and removing the closing tag.
				else
				{
					if(!sendMessage.Contains("</i>"))
					{
						sendMessage += "</i>";
					}	
				}
			}
#endif
			break;
		}
		
		if( autoRefreshMessages && !MCP.isTransmitting )
		{
			// Show readPM objects
			for(int i = 0; i < mainScreenGUI.Count; i++)
			{
				if(mainScreenGUI[i] != null)
				{
					mainScreenGUI[i].SetActive (true);
				}
			}
			RefreshMessages();
			autoRefreshMessages = false;
		}
		
		// Get a reference to the scroll content object.
		GameObject scrollContent = scrollAreaObject.transform.GetChild(0).GetChild(0).gameObject;
		
		// If the messages are not currently created
		if(messageObjects.Count <= 0)
		{
			if(MCP.zingMessages != null && MCP.zingMessages.messages != null)
			{
				if(MCP.zingMessages.messages.Count > 0)
				{
					// Make a message object for each message
					for(int i = 0; i < MCP.zingMessages.messages.Count; i++)
					{
						GameObject message = CreateMessageObject (MCP.zingMessages.messages[i], i);
						message.name = "MessageObject" + i;
						message.transform.SetParent (scrollContent.transform, true);
						message.transform.localScale = Vector3.one;
						
						messageObjects.Add (message);
						mainScreenGUI.Add (message);
					}
					
					// Change the scroll area height
					if(((messageObjects.Count + 1) * Screen.height * 0.1f) > Screen.height * 0.5f)
					{
						scrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(scrollAreaObject.GetComponent<RectTransform>().sizeDelta.x, ((messageObjects.Count + 1) * Screen.height * 0.1f));
					}
					else
					{
						scrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(scrollAreaObject.GetComponent<RectTransform>().sizeDelta.x, Screen.height * 0.5f);
					}
					
					// Flag the need for a position change.
					posUpdated = false;
					
					scrollAreaObject.GetComponent<ScrollRect>().velocity = new Vector2(0, 1000f);
				}
			}
		}
		else if(!posUpdated)
		{	
			// Change the position of the scroll content to the top.
			scrollContent.GetComponent<RectTransform>().transform.localPosition = new Vector2(0, -((messageObjects.Count + 1) * Screen.height * 0.1f) - (Screen.height * 1.5f));
			
			// Flag that the position is updated.
			posUpdated = true;
		}
		
		// Show or hide the no messages found label as required.
		if(MCP.zingMessages != null && MCP.zingMessages.messages != null && MCP.zingMessages.messages.Count > 0)
		{
			// Hide no messages found.
			noMessageFoundLabel.SetActive(false);
		}
		else
		{
			// Display no messages found.
			noMessageFoundLabel.SetActive(true);
		}

		if( goalMapUpdated && !MCP.isTransmitting )
		{
			LoadMenu("ViewGoalMap_Screen");
		}
		
		// Update the base screen.
		base.UpdateScreen ();
	}
	
	/// <summary>
	/// Makes a new message.
	/// </summary>
	private void NewMessage()
	{
		// Set new state;
		internalState = InternalState.sendPM;
		
		// Hide read message objects
		for(int i = 0; i < mainScreenGUI.Count; i++)
		{
			if(mainScreenGUI[i] != null)
			{
				mainScreenGUI[i].SetActive (false);
			}
		}
		// Show send message objects.
		for(int i = 0; i < sendMessageGUI.Count; i++)
		{
			sendMessageGUI[i].SetActive (true);
		}
	}
	
	/// <summary>
	/// Deletes the message.
	/// </summary>
	private void DeleteMessage()
	{
		// Send the current message to delete in MCP.
		StartCoroutine(MCP.DeleteMessage (selectedMessage.id));
		// Return to the display message screen.
		Back ();
	}
	
	/// <summary>
	/// Replies to message.
	/// </summary>
	private void ReplyToMessage()
	{
		// Set the reply's username and subject.
		SetMessageToInfo ();
		
		// Set new state;
		internalState = InternalState.sendPM;
		
		// Hide read message objects
		for(int i = 0; i < readMessageGUI.Count; i++)
		{
			readMessageGUI[i].SetActive (false);
		}
		// Show send message objects.
		for(int i = 0; i < sendMessageGUI.Count; i++)
		{
			sendMessageGUI[i].SetActive (true);
		}
	}
	
	private void AcceptFriend()
	{
		// Accept the friend.
		//StartCoroutine (MCP.AcceptFriend (selectedMessage.from));
		
		// Set state back to displayPMs
		internalState = InternalState.sentPopup;
		// Hide current screen objects
		for(int i = 0; i < readMessageGUI.Count; i++)
		{
			readMessageGUI[i].SetActive (false);
		}
		
		List<MyVoidFunc> delMessageFunc = new List<MyVoidFunc>
		{
			delegate
			{
				acceptedFriendName = selectedMessage.from;
				
				StartCoroutine( MCP.GetFriendInfo( acceptedFriendName ) );

				DeleteMessage();
			}
		};
		
		// Make message sent popup.
		//popup = MakePopup (localCanvas, defaultPopUpWindow, "", MCP.Text (2910)/*"Request Accepted"*/, MCP.Text (609)/*"Accept"*/, CommonTasks.DestroyParent );
		popup = MakePopup (localCanvas, defaultPopUpWindow, "", MCP.Text (2910)/*"Request Accepted"*/, MCP.Text (609)/*"Accept"*/, delMessageFunc );
	}
	
	private void ViewGoalMap()
	{
		//Show the GM screen and display the friends chosen GM
		StartCoroutine( MCP.GetGoalMap( selectedMessage.from ) );
		
		bool recordNumber = false;
		string gmIndexString = "";
		for( int i = 0; i < selectedMessage.subject.Length; i++ )
		{
			if( selectedMessage.subject[i] == '>' )
			{
				recordNumber = false;
				break;
			}
			
			if( recordNumber )
			{
				gmIndexString = gmIndexString + selectedMessage.subject[i];
			}
			
			if( selectedMessage.subject[i] == '<' )
			{
				recordNumber = true;
			}
		}
		
		MCP.gmIndex = int.Parse( gmIndexString );
		MCP.viewingFriendGM = true;
		
		// Make the button to view goal maps.
		goalMapUpdated = true;
	}
	
	/// <summary>
	/// Controls the bold.
	/// </summary>
	private void ControlBold()
	{
//#if UNITY_ANDROID || UNITY_IPHONE
//#if !UNITY_EDITOR
		makeBold = !makeBold;
		
		if(makeBold)
		{
			// Change the bold sprite to the highlighted version.
			sendBoldButton.GetComponent<Image>().sprite = boldSprite[1];
			
			// Add bold flags around message
			sendMessage = "<b>" + sendMessage + "</b>";
		}
		else
		{
			// Change the bold sprite to the unhighlighted version.
			sendBoldButton.GetComponent<Image>().sprite = boldSprite[0];
			
			// Make this remove bold flags within the substring.
			sendMessage = sendMessage.Replace("<b>", "");
			sendMessage = sendMessage.Replace("</b>", "");
		}
//#else
//		// Don't allow change if there is no selection.
//		if(startSelectPos == currentSelectPos)
//		{
//			return;
//		}
//		
//		// If there are already bold within the selection, remove them
//		if(startSelectPos > currentSelectPos)
//		{
//			if(sendMessage.Substring (currentSelectPos, startSelectPos - currentSelectPos).Contains("<b>") &&
//			   sendMessage.Substring (currentSelectPos, startSelectPos - currentSelectPos).Contains("</b>"))
//			{
//				string smSub = sendMessage.Substring (currentSelectPos, startSelectPos - currentSelectPos);
//				sendMessage = sendMessage.Remove (currentSelectPos, startSelectPos - currentSelectPos);
//				
//				// Make this remove bold flags within the substring.
//				smSub = smSub.Replace("<b>", "");
//				smSub = smSub.Replace("</b>", "");
//				
//				sendMessage = sendMessage.Insert (currentSelectPos, smSub);
//				return;
//			}
//		}
//		else
//		{
//			if(sendMessage.Substring (startSelectPos, currentSelectPos - startSelectPos).Contains("<b>") &&
//			   sendMessage.Substring (startSelectPos, currentSelectPos - startSelectPos).Contains("</b>"))
//			{
//				string smSub = sendMessage.Substring (startSelectPos, currentSelectPos - startSelectPos);
//				sendMessage = sendMessage.Remove (startSelectPos, currentSelectPos - startSelectPos);
//				
//				// Make this remove bold flags within the substring.
//				smSub = smSub.Replace("<b>", "");
//				smSub = smSub.Replace("</b>", "");
//				
//				sendMessage = sendMessage.Insert (startSelectPos, smSub);
//				return;
//			}
//		}
//		
//		// Otherwise, add bold flags around selection.
//		// Stop nested flags of same type. Do opposite flags to unbold/italic the middle bit.
//		if(startSelectPos > currentSelectPos)
//		{
//			bool nested = false;
//			// Go back through and check for open bold flag.
//			for(int i = currentSelectPos; i > 1; i--)
//			{ 
//				if(sendMessage[i] == '>' && sendMessage[i-1] == 'b')
//				{
//					// If there is a close bold flag then break the loop
//					if(i < 0 || sendMessage[i-2] != '/')
//					{
//						nested = true;
//					}
//					break;
//				}
//			}
//			
//			if(nested)
//			{
//				sendMessage = sendMessage.Insert (currentSelectPos, "</b>");
//				sendMessage = sendMessage.Insert (startSelectPos + 4, "<b>");
//			}
//			else
//			{
//				sendMessage = sendMessage.Insert (currentSelectPos, "<b>");
//				sendMessage = sendMessage.Insert (startSelectPos + 3, "</b>");
//			}
//		}
//		else
//		{
//			bool nested = false;
//			// Go back through and check for open bold flag.
//			for(int i = startSelectPos; i > 1; i--)
//			{ 
//				if(sendMessage[i] == '>' && sendMessage[i-1] == 'b')
//				{
//					// If there is a close bold flag then break the loop
//					if(i < 0 || sendMessage[i-2] != '/')
//					{
//						nested = true;
//					}
//					break;
//				}
//			}
//			
//			if(nested)
//			{
//				sendMessage = sendMessage.Insert (startSelectPos, "</b>");
//				sendMessage = sendMessage.Insert (currentSelectPos + 4, "<b>");
//			}
//			else
//			{
//				sendMessage = sendMessage.Insert (startSelectPos, "<b>");
//				sendMessage = sendMessage.Insert (currentSelectPos + 3, "</b>");
//			}
//		}
//		
//		startSelectPos = 0;
//		currentSelectPos = 0;
//#endif
	}
	
	/// <summary>
	/// Controls the italic.
	/// </summary>
	private void ControlItalic()
	{
//#if UNITY_ANDROID || UNITY_IPHONE
//#if !UNITY_EDITOR
		makeItalic = !makeItalic;
		
		if(makeItalic)
		{
			// Change the italic sprite to the highlighted version.
			sendItalicButton.GetComponent<Image>().sprite = italicSprite[1];
			
			// Add italic flags around message
			sendMessage = "<i>" + sendMessage + "</i>";
		}
		else
		{
			// Change the italic sprite to the unhighlighted version.
			sendItalicButton.GetComponent<Image>().sprite = italicSprite[0];
			
			// Make this remove italic flags.
			sendMessage = sendMessage.Replace("<i>", "");
			sendMessage = sendMessage.Replace("</i>", "");
		}
//#else
//		// Don't allow change if there is no selection.
//		if(startSelectPos == currentSelectPos)
//		{
//			return;
//		}
//		
//		// If there are already bold within the selection, remove them
//		if(startSelectPos > currentSelectPos)
//		{
//			if(sendMessage.Substring (currentSelectPos, startSelectPos - currentSelectPos).Contains("<i>") &&
//			   sendMessage.Substring (currentSelectPos, startSelectPos - currentSelectPos).Contains("</i>"))
//			{
//				string smSub = sendMessage.Substring (currentSelectPos, startSelectPos - currentSelectPos);
//				sendMessage = sendMessage.Remove (currentSelectPos, startSelectPos - currentSelectPos);
//				
//				// Make this remove bold flags within the substring.
//				smSub = smSub.Replace("<i>", "");
//				smSub = smSub.Replace("</i>", "");
//				
//				sendMessage = sendMessage.Insert (currentSelectPos, smSub);
//				return;
//			}
//		}
//		else
//		{
//			if(sendMessage.Substring (startSelectPos, currentSelectPos - startSelectPos).Contains("<i>") &&
//			   sendMessage.Substring (startSelectPos, currentSelectPos - startSelectPos).Contains("</i>"))
//			{
//				string smSub = sendMessage.Substring (startSelectPos, currentSelectPos - startSelectPos);
//				sendMessage = sendMessage.Remove (startSelectPos, currentSelectPos - startSelectPos);
//				
//				// Make this remove bold flags within the substring.
//				smSub = smSub.Replace("<i>", "");
//				smSub = smSub.Replace("</i>", "");
//				
//				sendMessage = sendMessage.Insert (startSelectPos, smSub);
//				return;
//			}
//		}
//		
//		// Otherwise, add bold flags around selection.
//		// Stop nested flags of same type. Do opposite flags to unbold/italic the middle bit.
//		if(startSelectPos > currentSelectPos)
//		{
//			bool nested = false;
//			// Go back through and check for open bold flag.
//			for(int i = currentSelectPos; i > 1; i--)
//			{ 
//				if(sendMessage[i] == '>' && sendMessage[i-1] == 'i')
//				{
//					// If there is a close bold flag then break the loop
//					if(i < 0 || sendMessage[i-2] != '/')
//					{
//						nested = true;
//					}
//					break;
//				}
//			}
//			
//			if(nested)
//			{
//				sendMessage = sendMessage.Insert (currentSelectPos, "</i>");
//				sendMessage = sendMessage.Insert (startSelectPos + 4, "<i>");
//			}
//			else
//			{
//				sendMessage = sendMessage.Insert (currentSelectPos, "<i>");
//				sendMessage = sendMessage.Insert (startSelectPos + 3, "</i>");
//			}
//		}
//		else
//		{
//			bool nested = false;
//			// Go back through and check for open bold flag.
//			for(int i = startSelectPos; i > 1; i--)
//			{ 
//				if(sendMessage[i] == '>' && sendMessage[i-1] == 'i')
//				{
//					// If there is a close bold flag then break the loop
//					if(i < 0 || sendMessage[i-2] != '/')
//					{
//						nested = true;
//					}
//					break;
//				}
//			}
//			
//			if(nested)
//			{
//				sendMessage = sendMessage.Insert (startSelectPos, "</i>");
//				sendMessage = sendMessage.Insert (currentSelectPos + 4, "<i>");
//			}
//			else
//			{
//				sendMessage = sendMessage.Insert (startSelectPos, "<i>");
//				sendMessage = sendMessage.Insert (currentSelectPos + 3, "</i>");
//			}
//		}
//		
//		startSelectPos = 0;
//		currentSelectPos = 0;
//#endif
	}
	
	/// <summary>
	/// Controls the font bigger.
	/// </summary>
	private void ControlFontBigger()
	{
		// Limit the font size.
		if(sendMessageStyle.fontSize < 64)
		{
			sendMessageStyle.fontSize = sendMessageStyle.fontSize + 2;
		}
	}
	
	/// <summary>
	/// Controls the font smaller.
	/// </summary>
	private void ControlFontSmaller()
	{
		// Limit the font size.
		if(sendMessageStyle.fontSize > 8)
		{
			sendMessageStyle.fontSize = sendMessageStyle.fontSize - 2;
		}
	}
	
	/// <summary>
	/// Controls the alignment.
	/// </summary>
	/// <param name="alignment">Alignment.</param>
	private void ControlAlign(TextAnchor alignment)
	{
		// Set send message textbox alignment to alignment
		sendMessageStyle.alignment = alignment;
		
		// Change the alignment button highlights to only highlight the selected one.
		switch(alignment)
		{
		case TextAnchor.UpperLeft:
			sendAlignLeftButton.GetComponent<Image>().sprite = leftAlignSprite[1];
			sendAlignCentreButton.GetComponent<Image>().sprite = centreAlignSprite[0];
			sendAlignRightButton.GetComponent<Image>().sprite = rightAlignSprite[0];
			break;
		case TextAnchor.UpperCenter:
			sendAlignLeftButton.GetComponent<Image>().sprite = leftAlignSprite[0];
			sendAlignCentreButton.GetComponent<Image>().sprite = centreAlignSprite[1];
			sendAlignRightButton.GetComponent<Image>().sprite = rightAlignSprite[0];
			break;
		case TextAnchor.UpperRight:
			sendAlignLeftButton.GetComponent<Image>().sprite = leftAlignSprite[0];
			sendAlignCentreButton.GetComponent<Image>().sprite = centreAlignSprite[0];
			sendAlignRightButton.GetComponent<Image>().sprite = rightAlignSprite[1];
			break;
		}
	}
	
	/// <summary>
	/// Sends the user message.
	/// </summary>
	private void SendUserMessage()
	{
		// Get the message info from input boxes
		MCP.ZingMessage m = new MCP.ZingMessage();
		m.date = System.DateTime.Now;
		if(MCP.userInfo != null)
		{
			m.from = MCP.userInfo.username;
		}
		m.message = sendMessage;
		m.subject = sendSubjectTextbox.GetComponent<Text>().text;
		m.username = sendSendToTextbox.GetComponent<Text>().text;
		m.type = "0";
		
		// Add the textbox font size tags to the message.
		string fontSize = sendMessageStyle.fontSize.ToString ();
		m.message = "<size=" + fontSize + ">" + m.message + "</size>";
		
		// Add a tag for the alignment
		switch(sendMessageStyle.alignment)
		{
		case TextAnchor.UpperLeft:
			m.message = "<l>" + m.message;
			break;
		case TextAnchor.UpperCenter:
			m.message = "<e>" + m.message;
			break;
		case TextAnchor.UpperRight:
			m.message = "<r>" + m.message;
			break;
		}
		
		// Send message.
		if(MCP.userInfo != null)
		{
			StartCoroutine (MCP.PostPrivateMessage (m.username, m.from, m.subject, m.message));
		}
		
		// Reset message textboxes.
		sendSubjectTextbox.GetComponent<InputField>().text = "";
		sendSendToTextbox.GetComponent<InputField>().text = "";
		sendMessage = "";
		ControlAlign(TextAnchor.UpperLeft);
		
		// Set state back to displayPMs
		internalState = InternalState.sentPopup;
		// Hide current screen objects
		for(int i = 0; i < sendMessageGUI.Count; i++)
		{
			sendMessageGUI[i].SetActive (false);
		}
		
		// Make message sent popup.
		popup = MakePopup (localCanvas, defaultPopUpWindow, "", MCP.Text (2911)/*"Message Sent"*/, MCP.Text (609)/*"Dismiss"*/, CommonTasks.DestroyParent);
	}
	
	/// <summary>
	/// Overrides the base Back().
	/// </summary>
	public override void Back ()
	{
#if UNITY_ANDROID || UNITY_IOS
		tsk.active = false;
#endif
		
		switch(internalState)
		{
		case InternalState.displayPMs:
			base.Back ();
			break;
			
		case InternalState.readPM:
			internalState = InternalState.displayPMs;
			// Hide current screen objects
			for(int i = 0; i < readMessageGUI.Count; i++)
			{
				readMessageGUI[i].SetActive (false);
			}

			for(int i = 0; i < viewFriendGMGUI.Count; i++)
			{
				viewFriendGMGUI[i].SetActive(false);
			}
			// Show displayPM objects
			for(int i = 0; i < mainScreenGUI.Count; i++)
			{
				if(mainScreenGUI[i] != null)
				{
					mainScreenGUI[i].SetActive (true);
				}
			}
			break;
			
		case InternalState.sendPM:
			if(selectedMessage == null)
			{
				internalState = InternalState.displayPMs;
				// Hide current screen objects
				for(int i = 0; i < sendMessageGUI.Count; i++)
				{
					sendMessageGUI[i].SetActive (false);
				}
				// Show readPM objects
				for(int i = 0; i < mainScreenGUI.Count; i++)
				{
					if(mainScreenGUI[i] != null)
					{
						mainScreenGUI[i].SetActive (true);
					}
				}
			}
			else
			{
				internalState = InternalState.readPM;
				// Hide current screen objects
				for(int i = 0; i < sendMessageGUI.Count; i++)
				{
					sendMessageGUI[i].SetActive (false);
				}
				// Show readPM objects
				for(int i = 0; i < readMessageGUI.Count; i++)
				{
					readMessageGUI[i].SetActive (true);
				}
			}
			
			// Reset send message.
			sendSubjectTextbox.GetComponent<InputField>().text = "";
			sendSendToTextbox.GetComponent<InputField>().text = "";
			sendMessage = "";
			ControlAlign(TextAnchor.UpperLeft);
			break;

		case InternalState.sentPopup:
			Destroy(popup);
			internalState = InternalState.displayPMs;
			autoRefreshMessages = true;
			break;
			
		}
	}	
	
	/// <summary>
	/// Control codes for changing alignment or selecting all text..
	/// </summary>
	private void ControlKeys()
	{
		if(Event.current.keyCode == KeyCode.A)
		{
			// Select all text
#if !UNITY_ANDROID && !UNITY_IPHONE
			editor.SelectAll ();
#endif
		}
		
		if(Event.current.keyCode == KeyCode.R)
		{
			// Right align
			ControlAlign (TextAnchor.UpperRight);
		}
		if(Event.current.keyCode == KeyCode.E)
		{
			// Centre align
			ControlAlign (TextAnchor.UpperCenter);
		}
		if(Event.current.keyCode == KeyCode.L)
		{
			// Left align
			ControlAlign (TextAnchor.UpperLeft);
		}
	}
	
	/// <summary>
	/// Do the legacy GUI.
	/// </summary>
	public override void DoGUI()
	{
		// If the current internal state is sendPM.
		if(internalState == InternalState.sendPM)
		{
			// Name the message to send text area so it can be manipulated.
			GUI.SetNextControlName ("SendMessageTextbox");
	
//#if (UNITY_ANDROID || UNITY_IPHONE) && !UNITY_EDITOR
#if UNITY_ANDROID || UNITY_IPHONE
			if(TouchScreenKeyboard.visible)
			{
				// If the text contains bold or italic handles.
				if(sendMessage.Contains("<b>") || sendMessage.Contains ("<i>"))
				{
//					editor.pos = 0;
					
					// Hide the text handles.
					sendMessage = sendMessage.Replace("<b>", "");
					sendMessage = sendMessage.Replace("</b>", "");
					sendMessage = sendMessage.Replace("<i>", "");
					sendMessage = sendMessage.Replace("</i>", "");
				}
			}
#endif
			float sendMessageX = (Screen.width * 0.2f) + (Screen.width * 0.075f * localCanvas.GetComponent<RectTransform>().localPosition.x);
			
			// Make a text area for the message to send.
			sendMessage = GUI.TextArea(new Rect(sendMessageX, Screen.height * 0.49f, Screen.width * 0.75f, Screen.height * 0.35f), sendMessage, sendMessageStyle);
			
#if UNITY_ANDROID || UNITY_IPHONE
			// If the keyboard is not visible...
			if(!TouchScreenKeyboard.visible)
			{
				// Replace bold flags.
				if(makeBold && !sendMessage.Contains ("<b>"))
				{
					sendMessage = "<b>" + sendMessage + "</b>";
				}
				// Replace italic flags.
				if(makeItalic && !sendMessage.Contains ("<i>"))
				{
					sendMessage = "<i>" + sendMessage + "</i>";
				}
			}
#else
			
			//TODO: Make sure this worksd with updated Unity
			if(!Input.GetMouseButton (0))
			{
				// Get a reference to the text editor attached to the message text area.
				editor = (TextEditor)GUIUtility.GetStateObject (typeof(TextEditor), GUIUtility.keyboardControl);
				
				// If the navigation hits the start of a flag...
				int startEPos = editor.cursorIndex;
				if( startEPos > 0 && startEPos < sendMessage.Length && sendMessage[startEPos - 1] == '<' && (sendMessage[startEPos] == 'b' || sendMessage[startEPos] == 'i' || sendMessage[startEPos] == '/'))
				{
					// Move to the start of the next word
					editor.cursorIndex += 2;
					if(sendMessage[startEPos] == '/')
					{
						editor.cursorIndex++;
						// If shift is not held, move the selected position as well.
						if(Event.current.keyCode != KeyCode.LeftShift && Event.current.keyCode != KeyCode.RightShift)
						{
							editor.selectIndex++;
						}
						else
						{
							editor.selectIndex--;
						}
					}
					editor.cursorIndex++;
					// If shift is not held, move the selected position as well.
					if(Event.current.keyCode != KeyCode.LeftShift && Event.current.keyCode != KeyCode.RightShift)
					{
						editor.selectIndex += 3;
					}
					else
					{
						editor.selectIndex -= 3;
					}
				}
				if( startEPos > 0 && startEPos < sendMessage.Length && sendMessage[startEPos] == '>' && (sendMessage[startEPos - 1] == 'b' || sendMessage[startEPos - 1] == 'i' || sendMessage[startEPos - 2] == '/'))
				{
					// Move to the start of the next word
					editor.cursorIndex -= 2;
					if(sendMessage[startEPos - 2] == '/')
					{
						editor.cursorIndex--;
						// If shift is not held, move the selected position as well.
						if(Event.current.keyCode != KeyCode.LeftShift && Event.current.keyCode != KeyCode.RightShift)
						{
							editor.selectIndex--;
						}
						else
						{
							editor.selectIndex++;
						}
					}
					editor.cursorIndex--;
					// If shift is not held, move the selected position as well.
					if(Event.current.keyCode != KeyCode.LeftShift && Event.current.keyCode != KeyCode.RightShift)
					{
						editor.selectIndex -= 3;
					}
					else
					{
						editor.selectIndex += 3;
					}
				}
				
				// Remove bold and italic flags in a single press of backspace or delete.
				if(Input.inputString == "\b")
				{
					if(editor.cursorIndex > 1 && editor.cursorIndex <= sendMessage.Length)
					{
						if(sendMessage[editor.cursorIndex-1] == 'b')
						{
							if(sendMessage[editor.cursorIndex-2] == '<')
							{
								sendMessage = sendMessage.Remove (editor.cursorIndex - 2, 2);
								editor.cursorIndex -= 2;
								editor.selectIndex = editor.cursorIndex;
								
								// Remove this flag's partner.
								for(int i = editor.cursorIndex; i < sendMessage.Length; i++)
								{
									if(i >= 2)
									{
										if(sendMessage[i-1] == 'b' && sendMessage[i-2] == '/')
										{
											sendMessage = sendMessage.Remove (i - 3, 4);
											ControlBold();
											break;
										}
									}
								}
							}
							else if(editor.cursorIndex > 2 && sendMessage[editor.cursorIndex-2] == '/')
							{
								sendMessage = sendMessage.Remove (editor.cursorIndex - 3, 3);
								editor.cursorIndex -= 3;
								editor.selectIndex = editor.cursorIndex;
								
								// Go back and remove this flag's partner.
								for(int i = editor.cursorIndex; i >= 0; i--)
								{
									if(i >= 2)
									{
										if(sendMessage[i-1] == 'b' && sendMessage[i-2] == '<')
										{
											sendMessage = sendMessage.Remove (i - 2, 3);
											editor.cursorIndex -= 3;
											editor.selectIndex = editor.cursorIndex;
											ControlBold();
											break;
										}
									}
								}
							}
						}
						else if (sendMessage[editor.cursorIndex-1] == 'i')
						{
							if(sendMessage[editor.cursorIndex-2] == '<')
							{
								sendMessage = sendMessage.Remove (editor.cursorIndex - 2, 2);
								editor.cursorIndex -= 2;
								editor.selectIndex = editor.cursorIndex;
								
								// Remove this flag's partner.
								for(int i = editor.cursorIndex; i < sendMessage.Length; i++)
								{
									if(i >= 2)
									{
										if(sendMessage[i-1] == 'i' && sendMessage[i-2] == '/')
										{
											sendMessage = sendMessage.Remove (i - 3, 4);
											ControlItalic();
											break;
										}
									}
								}
							}
							else if(editor.cursorIndex > 2 && sendMessage[editor.cursorIndex-2] == '/')
							{
								sendMessage = sendMessage.Remove (editor.cursorIndex - 3, 3);
								editor.cursorIndex -= 3;
								editor.selectIndex = editor.cursorIndex;
								
								// Go back and remove this flag's partner.
								for(int i = editor.cursorIndex; i >= 0; i--)
								{
									if(i >= 2)
									{
										if(sendMessage[i-1] == 'i' && sendMessage[i-2] == '<')
										{
											sendMessage = sendMessage.Remove (i - 2, 3);
											editor.cursorIndex -= 3;
											editor.selectIndex = editor.cursorIndex;
											ControlItalic();
											break;
										}
									}
								}
							}
						}
					}
				}
				
				if(Event.current.keyCode == KeyCode.Delete)
				{
					if(editor.cursorIndex >= 0 && editor.cursorIndex < sendMessage.Length - 2)
					{
						if(sendMessage[editor.cursorIndex] == 'b')
						{
							if(sendMessage[editor.cursorIndex+1] == '>')
							{
								sendMessage = sendMessage.Remove (editor.cursorIndex, 2);
								// Delete this flag's pair.
								for(int i = editor.cursorIndex; i < sendMessage.Length; i++)
								{
									if(i >= 2)
									{
										if(sendMessage[i-1] == 'b' && sendMessage[i-2] == '/')
										{
											sendMessage = sendMessage.Remove (i - 3, 4);
											ControlBold();
											break;
										}
									}
								}
							}
						}
						else if(sendMessage[editor.cursorIndex] == 'i')
						{
							if(sendMessage[editor.cursorIndex+1] == '>')
							{
								sendMessage = sendMessage.Remove (editor.cursorIndex, 2);
								// Delete this flag's pair.
								for(int i = editor.cursorIndex; i < sendMessage.Length; i++)
								{
									if(i >= 2)
									{
										if(sendMessage[i-1] == 'i' && sendMessage[i-2] == '/')
										{
											sendMessage = sendMessage.Remove (i - 3, 4);
											ControlItalic();
											break;
										}
									}
								}
							}
						}
						else if(sendMessage[editor.cursorIndex] == '/')
						{
							if(editor.cursorIndex+1 < sendMessage.Length && sendMessage[editor.cursorIndex+1] == 'b')
							{
								sendMessage = sendMessage.Remove (editor.cursorIndex, 3);
								
								// Go back and delete this flag's pair.
								for(int i = editor.cursorIndex; i >= 0; i--)
								{
									if(i >= 2)
									{
										if(sendMessage[i-1] == 'b' && sendMessage[i-2] == '<')
										{
											sendMessage = sendMessage.Remove (i - 2, 3);
											editor.cursorIndex -= 3;
											editor.selectIndex = editor.cursorIndex;
											ControlBold();
											break;
										}
									}
								}
							}
							if(editor.cursorIndex+1 < sendMessage.Length && sendMessage[editor.cursorIndex+1] == 'i')
							{
								sendMessage = sendMessage.Remove (editor.cursorIndex, 3);
								
								// Go back and delete this flag's pair.
								for(int i = editor.cursorIndex; i >= 0; i--)
								{
									if(i > 0 && sendMessage[i-1] == 'i' && sendMessage[i-2] == '<')
									{
										sendMessage = sendMessage.Remove (i - 2, 3);
										editor.cursorIndex -= 3;
										editor.selectIndex = editor.cursorIndex;
										ControlItalic();
										break;
									}
								}
							}
						}
					}
				}
				
//#if UNITY_EDITOR
//				if(editor.pos != 0 || editor.pos != editor.selectPos)
//				{
//					currentSelectPos = editor.pos;
//				}
//				if(editor.selectPos != 0 || editor.pos != editor.selectPos)
//				{
//					startSelectPos = editor.selectPos;
//				}
//#endif
				
				// If the control key is held...
				if(Event.current.control)
				{
					// Process keyboard shotcuts.
					ControlKeys ();
				}
			}
#endif
		}
	}
}
