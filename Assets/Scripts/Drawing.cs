﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Drawing : MonoBehaviour
{
	public enum Samples
	{
		None,
		Samples2,
		Samples4,
		Samples8,
		Samples16,
		Samples32,
		RotatedDisc
	};
	
	public static Samples numSamples = Samples.Samples4;
	
	/// <summary>
	/// Draws a line.
	/// </summary>
	/// <returns>The texture with the new line.</returns>
	/// <param name="from">From point.</param>
	/// <param name="to">To point.</param>
	/// <param name="w">The width of the line.</param>
	/// <param name="col">Line color.</param>
	/// <param name="tex">Texture to draw on.</param>
	public static Texture2D DrawLine(Vector2 from, Vector2 to, float w, Color col, Texture2D tex)
	{
		return DrawLine(from, to, w, col, tex, false, Color.black, 0);
	}
	
	/// <summary>
	/// Draws the line.
	/// </summary>
	/// <returns>The texture with the new line.</returns>
	/// <param name="from">From point.</param>
	/// <param name="to">To point.</param>
	/// <param name="w">The width of the line.</param>
	/// <param name="col">Line color.</param>
	/// <param name="tex">Texture to draw on.</param>
	/// <param name="stroke">If set to <c>true</c> stroke.</param>
	/// <param name="strokeCol">Stroke col.</param>
	/// <param name="strokeWidth">Stroke width.</param>
	public static Texture2D DrawLine(Vector2 from, Vector2 to, float w, Color col, Texture2D tex, bool stroke, Color strokeCol, float strokeWidth)
	{
		//It is important to round the numbers otherwise it will mess up with the texture width
		w = Mathf.Round (w);
		strokeWidth = Mathf.Round (strokeWidth);
		
		// Get the pixel edges.
		float extent = w + strokeWidth;
		float stY = Mathf.Clamp (Mathf.Min (from.y, to.y) - extent, 0, tex.height);//This is the topmost Y value
		float stX =  Mathf.Clamp (Mathf.Min (from.x, to.x) - extent, 0, tex.width);
		float endY = Mathf.Clamp (Mathf.Max (from.y, to.y) + extent, 0, tex.height);
		float endX = Mathf.Clamp (Mathf.Max (from.x, to.x) + extent, 0, tex.width);//This is the rightmost Y value
		
		// Get the stroke edges.
		strokeWidth = strokeWidth/2;
		float strokeInner = (w - strokeWidth) * (w - strokeWidth);
		float strokeOuter = (w + strokeWidth) * (w + strokeWidth);
		float strokeOuter2 = (w + strokeWidth + 1) * (w + strokeWidth + 1);
		float sqrW = w * w;//It is much faster to calculate with squared values
		
		float lengthX = endX - stX;
		float lengthY = endY - stY;
		Vector2 start = new Vector2 (stX, stY);
		Color[] pixels = tex.GetPixels ((int)stX, (int)stY, (int)lengthX, (int)lengthY, 0);//Get all pixels
		
		// Loop through the pixels.
		for (int y = 0; y < (int)lengthY; y++)
		{
			for (int x = 0; x < (int)lengthX; x++)
			{
				// Get the pixel position.
				Vector2 p = new Vector2 (x, y) + start;
				Vector2 center = p + new Vector2(0.5f, 0.5f);
				
				// Get the squared distance from the center of the pixels to the nearest point on the line.
				float dist = (center - Mathfx.NearestPointStrict(from, to, center)).sqrMagnitude;
				if (dist <= strokeOuter2)
				{
					Vector2[] samples = Sample (p);
					Color c = Color.black;
					Color pc = pixels[y * (int)lengthX + x];
					
					//Loop through the samples
					for (int i = 0; i < samples.Length; i++)
					{
						//The squared distance from the sample to the line
						dist = (samples[i] - Mathfx.NearestPointStrict(from, to, samples[i])).sqrMagnitude;
						
						if (stroke)
						{
							if (dist <= strokeOuter && dist >= strokeInner)
							{
								c += strokeCol;
							}
							else if (dist < sqrW)
							{
								c += col;
							}
							else
							{
								c += pc;
							}
						}
						else
						{
							// If the distance is smaller than the width of the line...
							if (dist < sqrW)
							{
								c += col;
							}
							// Otherwise, set it to be the original colour.
							else
							{
								c += pc;
							}
						}
					}
					
					//Get the avarage colour.
					c /= samples.Length;
					pixels[y * (int)lengthX + x] = c;
				}
			}
		}
		
		// Set and apply the new texture pixels.
		tex.SetPixels ((int)stX, (int)stY, (int)lengthX, (int)lengthY, pixels, 0);
		tex.Apply ();
		
		return tex;
	}
	
	/// <summary>
	/// Paint at the specified position, with the specified radius, color, and hardness on the specified texture.
	/// </summary>
	/// <param name="pos">Position.</param>
	/// <param name="rad">Radius.</param>
	/// <param name="col">Color.</param>
	/// <param name="hardness">Hardness.</param>
	/// <param name="tex">Texture to draw on.</param>
	public static Texture2D Paint(Vector2 pos, float rad, Color col, float hardness, Texture2D tex)
	{
		// Get the pixels effected.
		Vector2 start = new Vector2 (Mathf.Clamp (pos.x - rad, 0, tex.width), Mathf.Clamp (pos.y - rad, 0, tex.height));
		Vector2 end = new Vector2 (Mathf.Clamp (pos.x + rad, 0, tex.width), Mathf.Clamp (pos.y + rad, 0, tex.height));
		float widthX = Mathf.Round (end.x - start.x);
		float widthY = Mathf.Round (end.y - start.y);
		float sqrRad = (rad + 1) * (rad + 1);
		
		// Get the texture pixels.
		Color[] pixels = tex.GetPixels ((int)start.x, (int)start.y, (int)widthX, (int)widthY, 0);
		
		// For each pixel the be drawn on...
		for (int y = 0; y < widthY; y++)
		{
			for (int x = 0; x < widthX; x++)
			{
				// Get the pixel position.
				Vector2 p = new Vector2 (x, y) + start;
				Vector2 center = p + new Vector2(0.5f, 0.5f);
				
				// Get the distance from the position to the square radius.
				float dist = (center - pos).sqrMagnitude;
				// If the distance is greater than the square radius, continue to the next pixel.
				if (dist > sqrRad)
				{
					continue;
				}
				
				// Handle sampling.
				Vector2[] samples = Sample (p);
				Color c = Color.black;
				
				for (int i = 0; i < samples.Length; i++)
				{
					dist = Mathfx.GaussFalloff (Vector2.Distance(samples[i], pos), rad) * hardness;
					if (dist > 0)
					{
						c += Color.Lerp (pixels[y * (int)widthX + x], col, dist);
					}
					else
					{
						c += pixels[y * (int)widthX + x];
					}
				}
				c /= samples.Length;
				
				pixels[y * (int)widthX + x] = c;
			}
		}
		
		// Set the new texture pixels.
		tex.SetPixels ((int)start.x, (int)start.y, (int)widthX, (int)widthY, pixels, 0);
		return tex;
	}
	
	/// <summary>
	/// Paints the line onto the texture.
	/// </summary>
	/// <returns>The texture with the new line.</returns>
	/// <param name="from">From point.</param>
	/// <param name="to">To point.</param>
	/// <param name="rad">Radius.</param>
	/// <param name="col">Color.</param>
	/// <param name="hardness">Hardness.</param>
	/// <param name="tex">Texture to draw on.</param>
	public static Texture2D PaintLine(Vector2 from, Vector2 to, float rad, Color col, float hardness, Texture2D tex)
	{
		// Get the area affected by this click.
		float extent = rad;
		float stY = Mathf.Clamp (Mathf.Min (from.y, to.y) - extent, 0, tex.height);
		float stX =  Mathf.Clamp (Mathf.Min (from.x, to.x) - extent, 0, tex.width);
		float endY = Mathf.Clamp (Mathf.Max (from.y, to.y) + extent, 0, tex.height);
		float endX = Mathf.Clamp (Mathf.Max (from.x, to.x) + extent, 0, tex.width);
		
		float lengthX = endX - stX;
		float lengthY = endY - stY;
		
		float sqrRad = (rad + 1) * (rad + 1);
		// Get the texture's pixels.
		Color[] pixels = tex.GetPixels ((int)stX, (int)stY, (int)lengthX, (int)lengthY, 0);
		Vector2 start = new Vector2 (stX, stY);
		
		// For each selected pixel...
		for (int y = 0; y < (int)lengthY; y++)
		{
			for (int x = 0; x < (int)lengthX; x++)
			{
				// Get the pixel position.
				Vector2 p = new Vector2 (x, y) + start;
				Vector2 center = p + new Vector2(0.5f, 0.5f);
				
				float dist = (center - Mathfx.NearestPointStrict(from, to, center)).sqrMagnitude;
				if (dist > sqrRad)
				{
					continue;
				}
				
				dist = Mathfx.GaussFalloff (Mathf.Sqrt(dist), rad) * hardness;
				
				Color c;
				
				if (dist > 0)
				{
					try
					{
						c = Color.Lerp (pixels[y * (int)lengthX + x], col, dist);
					}
					catch (System.Exception e)
					{
						Debug.Log ("Drawing Error: " + e.Message);
						float pos = y * (int)lengthX + x;
						pos++;
						c = Color.white;
					}
				}
				else
				{
					if(pixels.Length > (y * (int)lengthX + x) - 1)
					{
						c = pixels[y * (int)lengthX + x];
					}
					else
					{
						c = Color.white;
					}
				}
				
				pixels[y * (int)lengthX + x] = c;
			}
		}
		
		// Set the textures new pixels.
		tex.SetPixels ((int)start.x, (int)start.y, (int)lengthX, (int)lengthY, pixels, 0);
		return tex;
	}
	
	/// <summary>
	/// Draws the bezier.
	/// </summary>
	/// <param name="points">Bezier points.</param>
	/// <param name="rad">Radius.</param>
	/// <param name="col">Color.</param>
	/// <param name="tex">Texture to draw on.</param>
	public static void DrawBezier (BezierPoint[] points, float rad, Color col, Texture2D tex)
	{
		// It is important to round the numbers otherwise it will mess up with the texture width.
		rad = Mathf.Round (rad);
		
		// If there are 1 or fewer point, return.
		if (points.Length <= 1)
		{
			return;
		}
		
		Vector2 topleft = new Vector2(Mathf.Infinity, Mathf.Infinity);
		Vector2 bottomright = new Vector2(0, 0);
		
		// For each point...
		for (int i = 0; i < points.Length - 1; i++)
		{
			// Work out the curve from the points.
			BezierCurve curve = new BezierCurve();
			curve.Init(points[i].main, points[i].control2, points[i + 1].control1, points[i + 1].main);
			points[i].curve2 = curve;
			points[i + 1].curve1 = curve;
			
			topleft.x = Mathf.Min (topleft.x, curve.rect.x);
			
			topleft.y = Mathf.Min (topleft.y, curve.rect.y);
			
			bottomright.x = Mathf.Max (bottomright.x, curve.rect.x + curve.rect.width);
			
			bottomright.y = Mathf.Max (bottomright.y, curve.rect.y + curve.rect.height);
		}
		
		topleft -= new Vector2(rad, rad);
		bottomright += new Vector2(rad, rad);
		
		Vector2 start = new Vector2 (Mathf.Clamp (topleft.x, 0, tex.width), Mathf.Clamp (topleft.y, 0, tex.height));
		Vector2 width = new Vector2 (Mathf.Clamp (bottomright.x - topleft.x, 0, tex.width - start.x), Mathf.Clamp (bottomright.y - topleft.y, 0, tex.height - start.y));
		
		// Get the texture's pixels.
		Color[] pixels = tex.GetPixels ((int)start.x, (int)start.y, (int)width.x, (int)width.y, 0);
		
		// For each selected pixel...
		for (int y = 0; y < width.y; y++)
		{
			for (int x = 0; x < width.x; x++)
			{
				Vector2 p = new Vector2(x + start.x, y + start.y);
				if (!Mathfx.IsNearBeziers (p, points, rad + 2))
				{
					continue;
				}
				
				Vector2[] samples = Sample (p);
				Color c = Color.black;
				//Previous pixel color.
				Color pc = pixels[y * (int)width.x + x];
				
				for (int i = 0; i < samples.Length; i++)
				{
					if (Mathfx.IsNearBeziers (samples[i], points,rad))
					{
						c += col;
					}
					else
					{
						c += pc;
					}
				}
				
				c /= samples.Length;
				
				pixels[y * (int)width.x + x] = c;
			}
		}
		
		// Set and apply the new pixels to the texture.
		tex.SetPixels ((int)start.x, (int)start.y, (int)width.x, (int)width.y, pixels, 0);
		tex.Apply ();
	}
	
	/// <summary>
	/// Sample the specified point.
	/// </summary>
	/// <param name="p">P.</param>
	public static Vector2[] Sample (Vector2 p)
	{
		switch (numSamples)
		{
		case Samples.None :
			return new Vector2[]
				{
					p + new Vector2(0.5f, 0.5f)
				};
		case Samples.Samples2 :
			return new Vector2[]
				{
					p + new Vector2(0.25f, 0.5f), p + new Vector2(0.75f, 0.5f)
				};
		case Samples.Samples4 : 
			//return [p+Vector2(0.25,0.25),p+Vector2(0.75,0.25), p+Vector2(0.25,0.75),p+Vector2(0.75,0.75)];
			return new Vector2[]
				{
			        /*p+Vector2(0,0),
					p+Vector2(1,0),
					p+Vector2(0,1),
					p+Vector2(1,1)*/
			        
			        p + new Vector2(0.25f, 0.5f),
			        p + new Vector2(0.75f, 0.5f),
			        p + new Vector2(0.5f, 0.25f),
			        p + new Vector2(0.5f, 0.75f)
			    };
			    
		case Samples.Samples8 : 
			return new Vector2[]
				{
			        
			        /*p+Vector2(0,0),
					p+Vector2(1,0),
					p+Vector2(0,1),
					p+Vector2(1,1),*/
			        
			        /*p+Vector2(0.25,0.25),
					p+Vector2(0.75,0.25),
					p+Vector2(0.25,0.75),
					p+Vector2(0.75,0.75)*/
			        
			        p + new Vector2(0.25f, 0.5f),
			        p + new Vector2(0.75f, 0.5f),
			        p + new Vector2(0.5f, 0.25f),
			        p + new Vector2(0.5f, 0.75f),
			        
			        p + new Vector2(0.25f, 0.25f),
			        p + new Vector2(0.75f, 0.25f),
			        p + new Vector2(0.25f, 0.75f),
			        p + new Vector2(0.75f, 0.75f)
			        
			        /*p+Vector2(0.2,0.25),
					p+Vector2(0.4,0.25),
					p+Vector2(0.6,0.25),
					p+Vector2(0.8,0.25),
					
					p+Vector2(0.2,0.75),
					p+Vector2(0.4,0.75),
					p+Vector2(0.6,0.75),
					p+Vector2(0.8,0.75)*/
		        };
		case Samples.Samples16 : 
			return new Vector2[]
				{   
			        p + new Vector2(0, 0),
			        p + new Vector2(0.3f, 0),
			        p + new Vector2(0.7f, 0),
			        p + new Vector2(1f, 0),
			        
			        p + new Vector2(0, 0.3f),
			        p + new Vector2(0.3f, 0.3f),
			        p + new Vector2(0.7f, 0.3f),
			        p + new Vector2(1f, 0.3f),
			        
			        p + new Vector2(0, 0.7f),
			        p + new Vector2(0.3f, 0.7f),
			        p + new Vector2(0.7f, 0.7f),
			        p + new Vector2(1f, 0.7f),
			        
			        p + new Vector2(0, 1f),
			        p + new Vector2(0.3f, 1f),
			        p + new Vector2(0.7f, 1f),
			        p + new Vector2(1f, 1f)
		        };
		case Samples.Samples32 :
			return new Vector2[]
				{
			        p + new Vector2(0, 0),
			        p + new Vector2(1f, 0),
			        p + new Vector2(0, 1f),
			        p + new Vector2(1f, 1f),
			        
			        p + new Vector2(0.2f, 0.2f),
			        p + new Vector2(0.4f, 0.2f),
			        p + new Vector2(0.6f, 0.2f),
			        p + new Vector2(0.8f, 0.2f),
			        
			        p + new Vector2(0.2f, 0.4f),
			        p + new Vector2(0.4f, 0.4f),
			        p + new Vector2(0.6f, 0.4f),
			        p + new Vector2(0.8f, 0.4f),
			        
			        p + new Vector2(0.2f, 0.6f),
			        p + new Vector2(0.4f, 0.6f),
			        p + new Vector2(0.6f, 0.6f),
			        p + new Vector2(0.8f, 0.6f),
			        
			        p + new Vector2(0.2f, 0.8f),
			        p + new Vector2(0.4f, 0.8f),
			        p + new Vector2(0.6f, 0.8f),
			        p + new Vector2(0.8f, 0.8f),
			        
			        p + new Vector2(0.5f, 0),
			        p + new Vector2(0.5f, 1f),
			        p + new Vector2(0, 0.5f),
			        p + new Vector2(1f, 0.5f),
			        
			        p + new Vector2(0.5f, 0.5f)   
		        };
		case Samples.RotatedDisc :
			return new Vector2[]
				{
			        p + new Vector2(0, 0),
			        p + new Vector2(1f, 0),
			        p + new Vector2(0, 1f),
			        p + new Vector2(1f, 1f),
			        
			        p + new Vector2(0.5f, 0.5f) + new Vector2(0.258f, 0.965f),//Sin (75°) && Cos (75°)
			        p + new Vector2(0.5f, 0.5f) + new Vector2(-0.965f, -0.258f),
			        p + new Vector2(0.5f, 0.5f) + new Vector2(0.965f, 0.258f),
			        p + new Vector2(0.5f, 0.5f) + new Vector2(0.258f, -0.965f)
			    };
		default:
			goto case Samples.None;
		}
	}
}

public class BezierPoint : MonoBehaviour
{
	public Vector2 main;
	public Vector2 control1;//Think of as left
	public Vector2 control2;//Right
	//var rect : Rect;
	public BezierCurve curve1;//Left
	public BezierCurve curve2;//Right
	
	/// <summary>
	/// Init the specified middle, left and right points.
	/// </summary>
	/// <param name="m">Middle point.</param>
	/// <param name="l">Left point.</param>
	/// <param name="r">Right point.</param>
	public void Init (Vector2 m, Vector2 l, Vector2 r)
	{
		main = m;
		control1 = l;
		control2 = r;
		
		/*var topleft : Vector2 = Vector2(Mathf.Infinity,Mathf.Infinity);
		var bottomright : Vector2 = Vector2(Mathf.NegativeInfinity,Mathf.NegativeInfinity);
		
		topleft.x = Mathf.Min (topleft.x,main.x);
		topleft.x = Mathf.Min (topleft.x,control1.x);
		topleft.x = Mathf.Min (topleft.x,control2.x);
		
		topleft.y = Mathf.Min (topleft.y,main.y);
		topleft.y = Mathf.Min (topleft.y,control1.y);
		topleft.y = Mathf.Min (topleft.y,control2.y);
		
		bottomright.x = Mathf.Max (bottomright.x,main.x);
		bottomright.x = Mathf.Max (bottomright.x,control1.x);
		bottomright.x = Mathf.Max (bottomright.x,control2.x);
		
		bottomright.y = Mathf.Max (bottomright.y,main.y);
		bottomright.y = Mathf.Max (bottomright.y,control1.y);
		bottomright.y = Mathf.Max (bottomright.y,control2.y);
		
		rect = Rect (topleft.x,topleft.y,bottomright.x-topleft.x,bottomright.y-topleft.y);*/
	}
}

public class BezierCurve : MonoBehaviour
{
	public Vector2[] points;
	public float aproxLength;
	public Rect rect;
	
	public Vector2 Get(float t)
	{
		int t2 = (int)Mathf.Round (t * (points.Length - 1));
		return points[t2];
	}
	
	/// <summary>
	/// Init the specified points.
	/// </summary>
	/// <param name="p0">Point 0.</param>
	/// <param name="p1">Point 1.</param>
	/// <param name="p2">Point 2.</param>
	/// <param name="p3">Point 3.</param>
	public void Init (Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3)
	{	
		Vector2 topleft = new Vector2(Mathf.Infinity, Mathf.Infinity);
		Vector2 bottomright = new Vector2(Mathf.NegativeInfinity, Mathf.NegativeInfinity);
		
		topleft.x = Mathf.Min (topleft.x, p0.x);
		topleft.x = Mathf.Min (topleft.x, p1.x);
		topleft.x = Mathf.Min (topleft.x, p2.x);
		topleft.x = Mathf.Min (topleft.x, p3.x);
		
		topleft.y = Mathf.Min (topleft.y, p0.y);
		topleft.y = Mathf.Min (topleft.y, p1.y);
		topleft.y = Mathf.Min (topleft.y, p2.y);
		topleft.y = Mathf.Min (topleft.y, p3.y);
		
		bottomright.x = Mathf.Max (bottomright.x, p0.x);
		bottomright.x = Mathf.Max (bottomright.x, p1.x);
		bottomright.x = Mathf.Max (bottomright.x, p2.x);
		bottomright.x = Mathf.Max (bottomright.x, p3.x);
		
		bottomright.y = Mathf.Max (bottomright.y, p0.y);
		bottomright.y = Mathf.Max (bottomright.y, p1.y);
		bottomright.y = Mathf.Max (bottomright.y, p2.y);
		bottomright.y = Mathf.Max (bottomright.y, p3.y);
		
		rect = new Rect (topleft.x, topleft.y, bottomright.x - topleft.x, bottomright.y - topleft.y);
		
		List<Vector2> ps = new List<Vector2>();
		
		Vector2 point1 = Mathfx.CubicBezier (0, p0, p1, p2, p3);
		Vector2 point2 = Mathfx.CubicBezier (0.05f, p0, p1, p2, p3);
		Vector2 point3 = Mathfx.CubicBezier (0.1f, p0, p1, p2, p3);
		Vector2 point4 = Mathfx.CubicBezier (0.15f, p0, p1, p2, p3);
		
		Vector2 point5 = Mathfx.CubicBezier (0.5f, p0, p1, p2, p3);
		Vector2 point6 = Mathfx.CubicBezier (0.55f, p0, p1, p2, p3);
		Vector2 point7 = Mathfx.CubicBezier (0.6f, p0, p1, p2, p3);
		
		aproxLength = Vector2.Distance (point1,point2) + Vector2.Distance (point2,point3) + Vector2.Distance (point3,point4)  + Vector2.Distance (point5,point6)  + Vector2.Distance (point6,point7);
		
		Debug.Log (Vector2.Distance (point1,point2) + "     " + Vector2.Distance (point3,point4) + "   " + Vector2.Distance (point6,point7));
		aproxLength *= 4;
		
		//Double the amount of points since the aproximation is quite bad.
		float a2 = 0.5f / aproxLength;
		for (float i = 0; i < 1; i += a2)
		{
			ps.Add (Mathfx.CubicBezier (i, p0, p1, p2, p3));
		}
		
		points = ps.ToArray ();
	}
}