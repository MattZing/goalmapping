using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Friends_Screen : MenuScreen
{
	private const float refreshRate = 10f;
	
	private List<GameObject> friendObjects;
	private GameObject localCanvas;
	private GameObject friendsScrollArea;
	private GameObject messageToObj;
	private GameObject messageSubjectObj;
	private GameObject messageTextObj;
	private GameObject sendButton;
	private GameObject popup;
	private GameObject friendNameInputObj;
	private GameObject displayContactsBackground;
	
	private bool notificationsChecked = false;
	private bool sendingMessage = false;
	private float refreshTimer = refreshRate - 0.5f;
	private Rect screenSize;
	private Rect topPanelRect;
	private Texture avatarBackground;
	private Sprite teamAvatarImage;
	private string messageSubject = "";

	private MCP.Friend selectedFriend;
	
	private float usernameOffsetX = 0.1f;
	private float usernameWidth = 0.25f;
	private float realnameOffsetX = 0.35f;
	private float realnameWidth = 0.25f;
	private float contactOffsetX = 0.675f;
	private float contactWidth = 0.2f;

	private Dictionary<int, GameObject> internalScenes;
	
	private enum InternalState
	{
		displayContacts = 0,
		contactContact = 1,
		sendPM = 2,
		loadFriendProfile = 3
	};
	
	private InternalState internalState;
	public GameObject avatarPrefab;
	public float tempVar = 75f;
	
	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		// Initialise the base.
		Init ();
		
		friendObjects = new List<GameObject>();

		// Load resources
		teamAvatarImage = Resources.Load <Sprite>("icon_mentorprofile");
		
		// Set default variables.
		internalState = InternalState.displayContacts;

		internalScenes = new Dictionary<int, GameObject>();
		
		if(MCP.userInfo != null)
		{
			// Get the user's friends.
			StartCoroutine (MCP.GetFriends (MCP.userInfo.username));
		}
	}
	
	void RefreshFriends()
	{
		Debug.Log ("Refreshing");
		if(!MCP.isTransmitting)
		{
			// Pull the friends list
			if(refreshTimer < 15f)
			{
				if(MCP.userInfo != null)
				{
					StartCoroutine (MCP.GetNotifications ());
					notificationsChecked = false;
				}

				refreshTimer = 20f;
				return;
			}
			else
			{
				if(!notificationsChecked && MCP.zingNotifications != null && MCP.zingNotifications.messages != null && !MCP.isTransmitting)
				{
					for(int i = 0; i < MCP.zingNotifications.messages.Count; i++)
					{
						// If the notification is a friend confirmation and is not already tripped...
						if(MCP.zingNotifications.messages[i].type == "4" && MCP.zingNotifications.messages[i].hasTripped == "0")
						{
							// Get friend's info, flag and add in update.
							StartCoroutine( MCP.GetFriendInfo(MCP.zingNotifications.messages[i].from));
							StartCoroutine (MCP.MarkNotificationAsTripped (MCP.zingNotifications.messages[i].id));
							
							MCP.friendsListNeedsUpdating = true;
						}
						
						if(MCP.friendsListNeedsUpdating)
						{
							// Destroy current friend objects
							for(int j = friendObjects.Count - 1; j >= 0; j--)
							{
								Destroy (friendObjects[j]);
								friendObjects.RemoveAt (j);
							}
							
							// Generate the new objects from the new list.
							CreateFriendObjects();
						}
					}
					
					notificationsChecked = true;
				}
				
				refreshTimer = 0;
			}
		}
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update()
	{
		refreshTimer += Time.deltaTime;
		
		if(refreshTimer > refreshRate)
		{
			RefreshFriends();
		}
		
		if(MCP.friendsListNeedsUpdating && !MCP.isTransmitting)
		{
			StartCoroutine( MCP.UpdateFriends());
		}
		
		switch(internalState)
		{
		case InternalState.contactContact:
			// If there is no message to send, disable the button.
			if(messageToObj.GetComponent<Text>().text == "" || messageTextObj.GetComponent<Text>().text == "")
			{
				sendButton.GetComponent<Button>().interactable = false;
			}
			// Otherwise, activate the button.
			else
			{
				sendButton.GetComponent<Button>().interactable = true;
			}
			break;
			
		case InternalState.sendPM:
			// If the popup has been destroyed...
			if(popup == null)
			{
				// Change back to contacts script.
				ChangeState (0);
				// Reset flags.
				sendingMessage = false;
			}
			break;
			
		case InternalState.loadFriendProfile:
			
			List<GameObject> activeGameObjects = new List<GameObject>( internalScenes.Values );
			
			for( int i = 0; i < activeGameObjects.Count; i++ )
			{
				activeGameObjects[i].SetActive( false );
			}
			
			if( !MCP.isTransmitting )
			{
				LoadMenu("Profile_Screen");
			}
			
			break;
		}
		
		if(popup != null && !sendingMessage)
		{
			switch(MCP.messageFromServer)
			{
			case "User found, request sent":
				// If the server has returned that the username is valid, change the popup text to say so.
				popup.transform.GetChild (1).gameObject.GetComponent<Text>().text = MCP.Text (2009)/*"Request Sent"*/;
				break;
				
			case "User not found":
				// If the server has returned that the username is not valid, change the popup text to say so.
				popup.transform.GetChild (1).gameObject.GetComponent<Text>().text = MCP.Text (2604)/*"User not found"*/;
				break;
			
			default:
				// If the server has returned that the username is valid, change the popup text to say so.
				popup.transform.GetChild (1).gameObject.GetComponent<Text>().text = MCP.Text (2013)/*"Sending Request..."*/;
				break;
			}
		}
		// Otherwise, reset username box and messageFromServer after adding a friend.
		else
		{
			if(MCP.messageFromServer == "User found, request sent")
			{
				friendNameInputObj.GetComponent<InputField>().text = "";
				MCP.messageFromServer = "";
			}
		}
	}
	
	/// <summary>
	/// Do the legacy GUI.
	/// </summary>
	public override void DoGUI ()
	{
		if ((screenSize.width == 0 || Screen.width != screenSize.width) && MCP.userInfo != null && MCP.userInfo.friend_list != null && MCP.userInfo.friend_list.friends != null)
		{
			screenSize = new Rect (0, 0, Screen.width, Screen.height);
			topPanelRect = new Rect (Screen.width * 0.15f, Screen.height * 0.14f, Screen.width , Screen.height );
			
			// If the GUI has not aready been created, create the scene GUI.
			if(localCanvas == null)
			{
				CreateGUI ();
				
				// Generate the new objects from the new list.
				CreateFriendObjects();
				
				localCanvas.transform.localScale = Vector2.zero;
			}
		}
		
		if(MCP.friendsListNeedsUpdating)
		{
			StartCoroutine (MCP.UpdateFriends ());
			
			MCP.friendsListNeedsUpdating = false;
		}
	}
	
	void CreateFriendObjects()
	{
		// If the user has any friends...
		if(MCP.userInfo.friend_list.friends.Count > 0)
		{
			float offset = topPanelRect.height * 0.175f;
			int i = 0;
			
			GameObject scrollContent = friendsScrollArea.transform.GetChild (0).GetChild (0).gameObject;
			
			// Make the friends scroll area big enough for the content.
			scrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(scrollContent.GetComponent<RectTransform>().sizeDelta.x, topPanelRect.height * 0.5f);
			
			// Reset the scroll content position to start at the top.
			Vector3 newPos = friendsScrollArea.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform>().localPosition;
			newPos.y = -(topPanelRect.height * 0.45f);
			friendsScrollArea.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform>().localPosition = newPos;
			
			foreach (MCP.Friend vid in MCP.userInfo.friend_list.friends)
			{
				GameObject profileButton = MakeActionButton( displayContactsBackground, window_back, new Rect(topPanelRect.width * 0.01f, offset - (topPanelRect.height * 0f), topPanelRect.width * 0.7f, topPanelRect.height * 0.05f), DoProfileButtonID, i );
				profileButton.name = "profileButton";
				profileButton.transform.SetParent (friendsScrollArea.transform.GetChild (0).GetChild (0), true);
				profileButton.transform.localScale = Vector3.one;
				friendObjects.Add (profileButton);
				
				// Make an avatar image for this friend.
				GameObject avatarImage = MakeImage (displayContactsBackground, new Rect (topPanelRect.width * 0.035f, offset - (topPanelRect.height * 0.81f), topPanelRect.width * 0.0325f, topPanelRect.height * 0.05f), teamAvatarImage);
				avatarImage.name = "avImage";
				avatarImage.transform.SetParent (profileButton.transform, true);
				avatarImage.transform.localScale = Vector3.one;
				friendObjects.Add (avatarImage);
				
				// Make a label for the username.
				GameObject name = MakeLabel(displayContactsBackground,vid.username , new Rect (topPanelRect.width * usernameOffsetX, offset - (Screen.height * 0.81f), topPanelRect.width * usernameWidth, topPanelRect.height * 0.05f));
				name.name = "Name"; //the name
				name.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
				name.transform.SetParent (profileButton.transform, true);
				name.transform.localScale = Vector3.one;
				name.GetComponent<Text>().resizeTextForBestFit = false;
				name.GetComponent<Text>().fontSize = (int)_fontSize;
				friendObjects.Add (name);
				
				// Make a label for the real name.
				GameObject real = MakeLabel(displayContactsBackground,vid.firstName + " " + vid.lastName, new Rect (topPanelRect.width * realnameOffsetX, offset - (Screen.height * 0.81f), topPanelRect.width * realnameWidth, topPanelRect.height * 0.05f));
				real.name = "Name"; //the name
				real.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
				real.transform.SetParent (profileButton.transform, true);
				real.transform.localScale = Vector3.one;
				real.GetComponent<Text>().resizeTextForBestFit = false;
				real.GetComponent<Text>().fontSize = (int)_fontSize;
				friendObjects.Add (real);
				
				// Make a button to contact this friend.
				GameObject contactButton = MakeActionButton (displayContactsBackground, MCP.Text (2004)/*"Contact"*/,  new Rect (topPanelRect.width * contactOffsetX, offset, topPanelRect.width * contactWidth * 0.95f, topPanelRect.height * 0.05f), DoContactButtonID, i);
				contactButton.name = "ContactButton";
				contactButton.transform.GetChild (0).gameObject.GetComponent<Text>().fontStyle = FontStyle.Bold;
				contactButton.transform.SetParent (profileButton.transform, true);
				contactButton.transform.localScale = Vector3.one;
				friendObjects.Add (contactButton);
				
				offset += topPanelRect.width * 0.05f;			
				
				i++;
			}
			
			if(offset > (topPanelRect.height * 0.7f))
			{
				// Make the friends scroll area big enough for the content.
				scrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(scrollContent.GetComponent<RectTransform>().sizeDelta.x, offset);
				
				// Reset the scroll content position to start at the top.
				newPos = friendsScrollArea.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform>().localPosition;
				newPos.y = -offset - (Screen.height * 1.5f);
				friendsScrollArea.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform>().localPosition = newPos;
			}
		}
		else
		{
			//TODO: Make a newgui object
			GUI.Label (topPanelRect, MCP.Text (2006)/*"No friends."*/);
		}
	}
	
	/// <summary>
	/// Creates the 3D GUI.
	/// </summary>
	void CreateGUI()
	{
		// Find the current canvas.
		localCanvas = MakeCanvas ();
		
		// Make title bar.
		GameObject titleBar = MakeTitleBar (localCanvas, "Friends");
		titleBar.name = "TitleBar";
		
		// Make side gizmo.
		MakeSideGizmo(localCanvas, true);
		
		// *******  Display contacts - state view *******
		displayContactsBackground = MakeWindowBackground (localCanvas, new Rect(topPanelRect.width * 0.02f, topPanelRect.height * 0.15f, (Screen.width * 0.98f) - (topPanelRect.width * 0.02f), (Screen.height * 0.98f) - (topPanelRect.height * 0.17f)), window_back);
		displayContactsBackground.name = "Display Contacts";
		
		// Make username label.
		GameObject nameLabel = MakeLabel(localCanvas, MCP.Text (2002)/*"Username"*/, new Rect (topPanelRect.width * (usernameOffsetX - 0.04f), topPanelRect.y + (topPanelRect.height * 0.05f), topPanelRect.width * usernameWidth, topPanelRect.height * 0.05f));
		nameLabel.name = "Name"; //the name
		nameLabel.GetComponent<Text>().color = bmPink;
		nameLabel.GetComponent<Text>().fontStyle = FontStyle.Bold;
		nameLabel.transform.SetParent (displayContactsBackground.transform, true);
		
		// Make real name label. 
		GameObject realLabel = MakeLabel(localCanvas, MCP.Text (2003)/*"Realname"*/, new Rect (topPanelRect.width * (realnameOffsetX), topPanelRect.y + (topPanelRect.height * 0.05f), topPanelRect.width * realnameWidth, topPanelRect.height * 0.05f));
		realLabel.name = "RealName"; //the real name
		realLabel.GetComponent<Text>().color = bmPink;
		realLabel.GetComponent<Text>().fontStyle = FontStyle.Bold;
		realLabel.transform.SetParent (displayContactsBackground.transform, true);
		
		// Make contact button label.
		GameObject contactLabel = MakeLabel(localCanvas, MCP.Text (2004)/*"Contact"*/, new Rect (topPanelRect.width * (contactOffsetX + 0.015f), topPanelRect.y + (topPanelRect.height * 0.05f), topPanelRect.width * contactWidth, topPanelRect.height * 0.05f));
		contactLabel.name = "Contact"; //the contact button
		contactLabel.GetComponent<Text>().color = bmPink;
		contactLabel.GetComponent<Text>().fontStyle = FontStyle.Bold;
		contactLabel.transform.SetParent (displayContactsBackground.transform, true);
		
		// Make scroll area for the friends list.
		friendsScrollArea = MakeScrollRect (localCanvas, new Rect(topPanelRect.width * 0.02f, topPanelRect.height * 0.225f, (Screen.width * 0.98f) - (topPanelRect.width * 0.075f), (Screen.height * 0.88f) - (topPanelRect.height * 0.3f)), null);
		friendsScrollArea.name = "FriendsScrollArea";
		friendsScrollArea.GetComponent<ScrollRect>().horizontal = false;
		friendsScrollArea.transform.SetParent (displayContactsBackground.transform, true);
		friendsScrollArea.transform.GetChild (0).gameObject.GetComponent<Mask>().showMaskGraphic = false;
		
		GameObject addFriendLabel = MakeLabel (localCanvas, MCP.Text (2011)/*"Add Friend"*/, new Rect(Screen.width * 0.05f, Screen.height * 0.85f, Screen.width * 0.2f, Screen.height * 0.08f));
		addFriendLabel.name = "AddFriendLabel";
		addFriendLabel.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
		addFriendLabel.transform.SetParent (displayContactsBackground.transform, true);
		
		friendNameInputObj = MakeInputField (localCanvas, "", new Rect(Screen.width * 0.25f, Screen.height * 0.85f, Screen.width * 0.45f, Screen.height * 0.08f), false);
		friendNameInputObj.name = "FriendNameInputObj";
//		friendNameInputObj.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
		friendNameInputObj.transform.parent.SetParent (displayContactsBackground.transform, true);
		
		// Make new message button.
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			AddFriend
		};
		GameObject newMessageButton = MakeButton (localCanvas, MCP.Text (2010)/*"Add"*/, buttonBackground, new Rect(Screen.width * 0.7125f, Screen.height * 0.85f, Screen.width * 0.2f, Screen.height * 0.08f), funcList);
		newMessageButton.name = "NewMessageButton";
		newMessageButton.transform.SetParent (displayContactsBackground.transform, true);

		internalScenes.Add ((int)InternalState.displayContacts, displayContactsBackground);
		// ******* End Display contacts *******

		// ******* Begin Contact friend *******
		GameObject contactContactsBackground = MakeWindowBackground (localCanvas, new Rect(topPanelRect.width * 0.15f, topPanelRect.height * 0.15f, (MenuScreen.canvasSize.width * 0.98f) - (topPanelRect.width * 0.3f), (MenuScreen.canvasSize.height * 0.98f) - (topPanelRect.height * 0.3f)), window_back);
		contactContactsBackground.name = "Contact Contact";
		
		Rect areaRect = contactContactsBackground.GetComponent<RectTransform>().rect;
		// Make the button to go back to the friends list.
		GameObject backButton = MakeActionButton (contactContactsBackground, MCP.Text (219)/*"Back"*/,  new Rect (areaRect.width * 0.05f, areaRect.height * 0.8885f, topPanelRect.width * 0.15f, topPanelRect.height * 0.06f), ChangeState, (int)InternalState.displayContacts);
		backButton.name = "BackButton";

		// Make the label for Send to
		GameObject sendTo = MakeLabel(contactContactsBackground, MCP.Text (2006)/*"Send to:"*/ , new Rect (areaRect.width * 0.01f, (topPanelRect.height * 0.0675f) - (Screen.height * 0.675f), areaRect.width * 0.2f, topPanelRect.height * 0.06f));
		sendTo.name = "Send To"; //the name
		sendTo.GetComponent<Text>().resizeTextForBestFit = false;
		sendTo.GetComponent<Text>().fontSize = (int)_fontSize;
		
		// Make an input field for the recipient.
		messageToObj = MakeInputField (contactContactsBackground, messageSubject, new Rect (areaRect.width * 0.275f, (topPanelRect.height * 0.05f) - (Screen.height * 0.675f), areaRect.width * 0.66f, Screen.height * 0.08f), false);
		messageToObj.name = "messageTo";
//		messageToObj.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;

		// Make the subject label
		GameObject subjectObj = MakeLabel(contactContactsBackground, MCP.Text (211)/*"Subject"*/ + ":" , new Rect (areaRect.width * 0.01f, (topPanelRect.height * 0.175f) - (Screen.height * 0.675f), areaRect.width * 0.2f, topPanelRect.width * 0.0325f));
		subjectObj.name = "Subject"; //the name
		subjectObj.GetComponent<Text>().resizeTextForBestFit = false;
		subjectObj.GetComponent<Text>().fontSize = (int)_fontSize;
		
		// Make the input field for the subject.
		messageSubjectObj = MakeInputField (contactContactsBackground, messageSubject, new Rect (areaRect.width * 0.275f, (topPanelRect.height * 0.13f) - (Screen.height * 0.65f), areaRect.width * 0.66f, Screen.height * 0.08f), false);
		messageSubjectObj.name = "MessageSubject";
//		messageSubjectObj.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;

		// Make the label for the message.
		GameObject messageObj = MakeLabel(contactContactsBackground, MCP.Text (2007)/*"Message:"*/ , new Rect (areaRect.width * 0.01f, (topPanelRect.height * 0.3f) - (Screen.height * 0.675f), areaRect.width * 0.2f, topPanelRect.width * 0.0325f));
		messageObj.name = "Message"; //the name
		messageObj.GetComponent<Text>().resizeTextForBestFit = false;
		messageObj.GetComponent<Text>().fontSize = (int)_fontSize;
		
		// Make the input field for the message.
		messageTextObj = MakeInputField (contactContactsBackground, messageSubject, new Rect (areaRect.width * 0.275f, (topPanelRect.height * 0.21f) - (Screen.height * 0.6f), areaRect.width * 0.66f, topPanelRect.height * 0.3f), false);
		messageTextObj.name = "MessageText";
		messageTextObj.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;
		
		// Make the send button.
		GameObject sendButtonBackground = MakeImage (contactContactsBackground, new Rect(areaRect.width * 0.7175f, (areaRect.height * 0.775f) - (Screen.height * 0.6f), topPanelRect.width * 0.15f, topPanelRect.height * 0.06f), buttonBackground, true);
		sendButtonBackground.name = "SendButtonBackground";
		
		funcList = new List<MyVoidFunc>
		{
			SendPrivateMessage
		};
		sendButton = MakeButton (contactContactsBackground, MCP.Text (213)/*"Send"*/, null, new Rect(areaRect.width * 0.7175f, (areaRect.height * 0.775f) - (Screen.height * 0.6f), topPanelRect.width * 0.15f, topPanelRect.height * 0.06f), funcList);
		sendButton.name = "SendButton";
		sendButton.transform.GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
		sendButton.transform.GetChild (0).gameObject.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
		sendButton.GetComponent<Button>().image = sendButtonBackground.GetComponent<Image>();

		internalScenes.Add ((int)InternalState.contactContact, contactContactsBackground);
		contactContactsBackground.SetActive(false);

		// ******* End contact contacts ***** 
	}

	/// <summary>
	/// Action button delegate.
	/// </summary>
	/// <returns><c>true</c>.</returns>
	/// <param name="i">The index.</param>
	bool DoContactButtonID(int i)
	{
		// Change the suit.
		ChangeState((int)InternalState.contactContact);
		
		// Set the sent to text to the friend's name.
		messageToObj.GetComponent<InputField>().text = MCP.userInfo.friend_list.friends[i].username;

		return true;
	}
	
	bool DoProfileButtonID( int i )
	{
		// Load friends profile using i
		StartCoroutine(MCP.GetFriendMemberProfile( MCP.userInfo.friend_list.friends[i].username ));
		
		// Set flag to true for update loop
		
		// Possible change state
		ChangeState((int)InternalState.loadFriendProfile);
		
		return true;
	}
	
	/// <summary>
	/// Changes the state.
	/// </summary>
	/// <returns><c>true</c>, if state was changed, <c>false</c> otherwise.</returns>
	/// <param name="stateIn">State in.</param>
	bool ChangeState(int stateIn)
	{
		InternalState enumState = (InternalState)stateIn;
		GameObject internalScene;
		
		internalScenes.TryGetValue((int)internalState, out internalScene);
		
		if(internalScene != null)
		{
			internalScene.SetActive(false);
		}
		
		// Exit stuff here 
		internalState = enumState;
		
		// Begin stuff
		internalScenes.TryGetValue((int)enumState, out internalScene);
		if(internalScene != null)
		{
			internalScene.SetActive(true);
		}
		return true;
	}
	
	/// <summary>
	/// Sends the private message.
	/// </summary>
	public void SendPrivateMessage()
	{
		// Post the message.
		StartCoroutine (MCP.PostPrivateMessage(messageToObj.GetComponent<Text>().text, MCP.userInfo.username, messageSubjectObj.GetComponent<Text>().text, messageTextObj.GetComponent<Text>().text));
		
		// Change the state to popup.
		ChangeState (2);
		
		// Make the message sent popup.
		popup = MakePopup (localCanvas, defaultPopUpWindow, "", MCP.Text (2008)/*"Message Sent"*/, MCP.Text (205)/*"Dismiss"*/, CommonTasks.DestroyParent);
		
		// Flag that a message is being sent instead of a request.
		sendingMessage = true;
		MCP.messageFromServer = "";
		messageTextObj.GetComponent<InputField>().text = "";
		messageSubjectObj.GetComponent<InputField>().text = "";
	}
	
	public void AddFriend()
	{
		string username = friendNameInputObj.GetComponent<Text>().text;
		
		bool alreadyFriend = false;
		
		if(MCP.userInfo != null)
		{
			foreach(MCP.Friend f in MCP.userInfo.friend_list.friends)
			{
				if(f.username == username)
				{
					alreadyFriend = true;
				}
			}
		}
		else
		{
			alreadyFriend = true;
		}
		
		if(alreadyFriend)
		{
			MakePopup (localCanvas, defaultPopUpWindow, "", MCP.Text (2014)/*"Already a Friend"*/, MCP.Text (205)/*"Dismiss"*/, CommonTasks.DestroyParent);
		}
		else
		{
			// Send the friend request to the required user.
			StartCoroutine (MCP.AddFriend (username));
			// Make the request sent popup.
			popup = MakePopup (localCanvas, defaultPopUpWindow, "", MCP.Text (2013)/*"Sending Request..."*/, MCP.Text (205)/*"Dismiss"*/, CommonTasks.DestroyParent);
			
			// Reset variables
			sendingMessage = false;
			MCP.messageFromServer = "";
		}
	}
}
