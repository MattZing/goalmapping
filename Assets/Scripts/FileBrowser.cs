﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class FileBrowser : MenuScreen
{
	public Sprite fileSprite;
	public Sprite folderSprite;
	public Sprite[] upFolderSprite;
	public Sprite refreshSprite;
	public Sprite highlightedBackground;
	private Sprite warningSprite;
	public object calledByScript;
	
	public GameObject sceneCanvas;
	private GameObject localCanvas;
	private GameObject currentPathObject;
	private GameObject folderScrollArea;
	private GameObject noFilesFoundLabel;

	private List<GameObject> fileBrowserObjects;
	private List<GameObject> currentFolders;
	private List<GameObject> currentFiles;
	private List<FolderObject> currentFolderObjects;
	
	private bool updateVelocity = false;
	public string currentFilePath;
	public string upPathLimit;
	private float clickTimer;
	public string[] allowedExtensions;
	
	private string[] textExtensions = new string[]{"txt"};
	private string[] audioExtensions = new string[]{"xm", "it", "wav", "mp3"};
	private string[] videoExtensions = new string[]{"ogg", "mp4", "wmv"};
	
	enum FileType
	{
		File,
		Folder
	}
	
	struct FolderObject
	{
		public string path;
		public FileType fileType;
		public Rect rect;
		public GameObject gameObject;
		
		public FolderObject(string p, FileType ft)
		{
			path = p;
			fileType = ft;
			rect = new Rect(0, 0, 0, 0);
			gameObject = null;
		}
	}

	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		Init ();
		
		// Initialise lists.
		currentFolders = new List<GameObject>();
		currentFiles = new List<GameObject>();
		currentFolderObjects = new List<FolderObject>();
		fileBrowserObjects = new List<GameObject>();
		upFolderSprite = new Sprite[2];
		
		// Set variable defaults.
		clickTimer = 0f;
		if(upPathLimit == null)
		{
			upPathLimit = "";
		}
		
		// Load resources.
		fileSprite = Resources.Load<Sprite>("GUI/File-8x");
		folderSprite = Resources.Load<Sprite>("GUI/Folder");
		upFolderSprite[0] = Resources.Load<Sprite>("GUI/filebrowser_up");
		upFolderSprite[1] = Resources.Load<Sprite>("GUI/filebrowser_up_active");
		refreshSprite = Resources.Load<Sprite>("waiting_symbol");
		highlightedBackground = Resources.Load<Sprite>("box");
		warningSprite = Resources.Load<Sprite>("news_alert");
		
		// Create the GUI.
		MakeGUI ();
		
		localCanvas.transform.localScale = Vector2.zero;
	}
	
	/// <summary>
	/// Create the 3D GUI.
	/// </summary>
	void MakeGUI()
	{
		// Disable the main scene canvas.
		sceneCanvas.SetActive (false);
		
		// Make a canvas for the scene.
		localCanvas = MakeCanvas(true);
		localCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
		
		// Draw background.
		GameObject windowBack = MakeWindowBackground (localCanvas, new Rect(Screen.width * 0.01f, Screen.height * 0.01f , Screen.width * 0.98f, Screen.height * 0.98f), window_back);
		windowBack.name = "WindowBackground";
		fileBrowserObjects.Add (windowBack);
		
		// Draw window background.
		GameObject backgroundObject = MakeImage (localCanvas, new Rect(0, 0, Screen.width, Screen.height), menuBackground);
		backgroundObject.name = "BackgroundObject";
		fileBrowserObjects.Add (backgroundObject);
		
		// Draw up folder button.
		List<MyVoidFunc> funcList = new List<MyVoidFunc>
		{
			GoUpFolder
		};
		GameObject upFolderButtonObject = MakeButton (localCanvas, "", upFolderSprite[0], new Rect(Screen.width * 0.1f, Screen.height * 0.05f, Screen.width * 0.04f, Screen.width * 0.04f), funcList, "SmallText");
		upFolderButtonObject.name = "UpFolderButtonObject";
		fileBrowserObjects.Add (upFolderButtonObject);
		// Make hover sprite
		UnityEngine.UI.Navigation nav = new UnityEngine.UI.Navigation();
		nav.mode = UnityEngine.UI.Navigation.Mode.None;
		upFolderButtonObject.GetComponent<Button>().navigation = nav;
		upFolderButtonObject.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
		
		SpriteState spriteState = new SpriteState();
		upFolderButtonObject.GetComponent<Button>().targetGraphic = upFolderButtonObject.GetComponent<Image>();
		spriteState.highlightedSprite = upFolderSprite[1];
		spriteState.pressedSprite = upFolderSprite[1];
		upFolderButtonObject.GetComponent<Button>().spriteState = spriteState;
		
		// Draw up folder button.
		funcList = new List<MyVoidFunc>
		{
			RefreshFileList
		};
		GameObject refreshButtonObject = MakeButton (localCanvas, "", refreshSprite, new Rect(Screen.width * 0.895f, Screen.height * 0.05f, Screen.width * 0.04f, Screen.height * 0.06f), funcList, "SmallText");
		refreshButtonObject.name = "RefreshButtonObject";
		fileBrowserObjects.Add (refreshButtonObject);
		
		// Draw the current path background.
		GameObject currentPathBackgroundObject = MakeImage (localCanvas, new Rect(Screen.width * 0.15f, Screen.height * 0.05f, Screen.width * 0.74f, Screen.height * 0.071f), menuBackground);
		currentPathBackgroundObject.name = "CurrentPathBackgroundObject";
		fileBrowserObjects.Add (currentPathBackgroundObject);
		
		// Draw the current path. Limit it to stop it going clipping.
		string s = "";
		if(currentFilePath.Length <= 32)
		{
			s = currentFilePath;
		}
		else
		{
			s = "..." + currentFilePath.Substring(currentFilePath.Length - 32);
		}
		
		currentPathObject = MakeLabel (localCanvas, s, new Rect(Screen.width * 0.155f, Screen.height * 0.01f, Screen.width * 0.695f, Screen.height * 0.1f), TextAnchor.UpperLeft, "SmallText");
		currentPathObject.name = "CurrentPathObject";
		currentPathObject.GetComponent<RectTransform>().pivot = new Vector2(0f, 0.5f);
		currentPathObject.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Overflow;
		fileBrowserObjects.Add (currentPathObject);
		
		// Create scroll area for files.
		folderScrollArea = MakeScrollRect (localCanvas, new Rect(Screen.width * 0.1f, Screen.height * 0.125f, Screen.width * 0.8f, Screen.height * 0.65f), null);
		folderScrollArea.name = "FolderScrollArea";
		folderScrollArea.GetComponent<ScrollRect>().horizontal = false;
		fileBrowserObjects.Add (folderScrollArea);
		
#if UNITY_ANDROID || UNITY_IPHONE
		bool openingImage = false;
		
		// Check for whether you are trying to open an image.
		foreach(string str in allowedExtensions)
		{
			if(str == "png" || str == "jpg")
			{
				openingImage = true;
			}
		}
		
		// If you are trying to open an image...
		if(openingImage)
		{
			// Make the switch to server button.
			GameObject switchToServerButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.1f, Screen.height * 0.87f, Screen.width * 0.3f, Screen.height * 0.08f), buttonBackground, true);
			switchToServerButtonBackground.name = "SwitchToServerButtonBackground";
			fileBrowserObjects.Add (switchToServerButtonBackground);
			
			funcList = new List<MyVoidFunc>
			{
				delegate
				{
					// Reenable the main scene canvas.
					sceneCanvas.SetActive (true);
					
					// Destroy all file browser game objects.
					localCanvas.SetActive (false);
					Destroy (localCanvas);
					
					// Reset the first frame timer.
					MenuScreen.firstFrameTimer = 0;
					
					// Add the picture gallery script and set the scene canvas.
					GameObject.FindGameObjectWithTag ("MainCamera").AddComponent<PictureGallery> ().sceneCanvas = sceneCanvas;
					
					// Remove this script from the object to close.
					Destroy (this);
				}
			};
			GameObject switchToServerButton = MakeButton (localCanvas, MCP.Text(2101)/*"Switch To Web"*/, null, new Rect(Screen.width * 0.1f, Screen.height * 0.87f, Screen.width * 0.3f, Screen.height * 0.08f), funcList, "SmallText");
			switchToServerButton.name = "SwitchToServerButton";
			switchToServerButton.GetComponent<Button>().image = switchToServerButtonBackground.GetComponent<Image>();
			switchToServerButton.transform.GetChild (0).gameObject.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
			switchToServerButton.transform.GetChild (0).gameObject.GetComponent<Text>().color = bmOrange;
			fileBrowserObjects.Add (switchToServerButton);
		}
#endif
		
		// Make warning icon.
		GameObject warningIcon = MakeImage(localCanvas, new Rect(Screen.width * 0.411f, Screen.height * 0.885f, Screen.height * 0.05f, Screen.height * 0.05f), warningSprite);
		warningIcon.name = "WarningIcon";
		
		// Make warning message about file size.
		GameObject warningLabel = MakeLabel (localCanvas, MCP.Text (2104)/*"Warning: large files will slow the app down."*/, new Rect((Screen.width * 0.42f) + (Screen.height * 0.05f), Screen.height * 0.8725f, (Screen.width * 0.325f) - (Screen.height * 0.05f), Screen.height * 0.08f), TextAnchor.MiddleLeft);
		warningLabel.name = "WarningLabel";
		warningLabel.GetComponent<Text>().fontSize = (int)_fontSize_Small;
		warningLabel.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
		
		// Make the close file browser button.
		GameObject closeButtonBackground = MakeImage (localCanvas, new Rect(Screen.width * 0.75f, Screen.height * 0.87f, Screen.width * 0.15f, Screen.height * 0.08f), buttonBackground, true);
		closeButtonBackground.name = "CloseButtonBackground";
		fileBrowserObjects.Add (closeButtonBackground);
		// Draw back button.
		funcList = new List<MyVoidFunc>
		{
			delegate
			{
//				if(GetComponent<GuidedGoalMap_Screen>() != null)
//				{
//					GetComponent<GuidedGoalMap_Screen>().wantsEditting = false;
//				}
				
				MCP.loadedTexture = null;
				
				Close();
			}
		};
		GameObject closeButtonObject = MakeButton (localCanvas, MCP.Text (206)/*"Close"*/, new Rect(Screen.width * 0.75f, Screen.height * 0.87f, Screen.width * 0.15f, Screen.height * 0.08f), funcList, "SmallText");
		closeButtonObject.name = "CloseButtonObject";
		closeButtonObject.GetComponent<Button>().image = closeButtonBackground.GetComponent<Image>();
		closeButtonObject.GetComponent<Text> ().alignment = TextAnchor.MiddleCenter;
		closeButtonObject.GetComponent<Text> ().color = bmOrange;
		fileBrowserObjects.Add (closeButtonObject);
	}
	
	/// <summary>
	/// Refreshes the file list.
	/// </summary>
	private void RefreshFileList()
	{
		// Destroy the current file objects.
		foreach(GameObject g in currentFiles)
		{
			DestroyImmediate (g);
		}
		currentFiles = new List<GameObject>();
		
		// Destroy the current folder objects.
		foreach(GameObject g in currentFolders)
		{
			DestroyImmediate (g);
		}
		currentFiles = new List<GameObject>();
		currentFolders = new List<GameObject>();
		currentFolderObjects = new List<FolderObject>();
		
		fileBrowserObjects.RemoveAll (item => item == null);
		
		// Update path label.
		if(currentFilePath.Length <= 32)
		{
			currentPathObject.GetComponent<Text>().text = currentFilePath;
		}
		else
		{
			currentPathObject.GetComponent<Text>().text = "..." + currentFilePath.Substring(currentFilePath.Length - 32);
		}
		
		// Reset scroll area size.
		folderScrollArea.transform.GetChild (0).GetChild (0).gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(folderScrollArea.GetComponent<RectTransform>().sizeDelta.x, Screen.height * 0.7f);
		folderScrollArea.GetComponent<ScrollRect>().velocity = new Vector2(0, -100f);
	}
	
	/// <summary>
	/// Go up a folder.
	/// </summary>
	void GoUpFolder()
	{
		string prevFolderPath = "";
		string[] splitFilePath = currentFilePath.Split (new char[]{'/', '\\'});
		
		// Stop path from going too far up.
		if(/*currentFilePath == Application.persistentDataPath || */splitFilePath.Length <= 2)
		{
			return;
		}
		
		if(upPathLimit != "" && currentFilePath == upPathLimit)
		{
			//return;
		}
		
		// Add the file path string back together, ignoring the last entry.
		for(int i = 0; i < splitFilePath.Length - 1; i++)
		{
			//prevFolderPath += splitFilePath[i] + "\\";
			prevFolderPath += splitFilePath[i] + "/";
		}
		
		// Open this new folder.
		OpenFolder (prevFolderPath);
	}
	
	/// <summary>
	/// Loads the file data.
	/// </summary>
	/// <returns>The file data.</returns>
	/// <param name="path">File path.</param>
	private byte[] LoadFileData(string path)
	{
		FileStream fs = new FileStream(path, FileMode.Open);
		BinaryReader reader = new BinaryReader(fs);
		
		byte[] data = reader.ReadBytes ((int)reader.BaseStream.Length);
		
		// Close file streams.
		reader.Close ();
		fs.Close ();
		
		return data;
	}
	
	/// <summary>
	/// Loads the text file data.
	/// </summary>
	/// <returns>The text file data.</returns>
	/// <param name="path">File path.</param>
	private string LoadTextFileData(string path)
	{
		FileStream fs = new FileStream(path, FileMode.Open);
		StreamReader reader = new StreamReader(fs);
		
		// Read the file data.
		string data = reader.ReadLine ();
		
		// Close file streams.
		reader.Close ();
		fs.Close ();
		
		return data;
	}
	
	/// <summary>
	/// Loads the audio file data.
	/// </summary>
	/// <returns>The audio file data.</returns>
	/// <param name="path">File path.</param>
	private byte[] LoadAudioFileData(string path)
	{
		FileStream fs = new FileStream(path, FileMode.Open);
		BinaryReader reader = new BinaryReader(fs);
		
		// Read the file data.
		byte[] data = reader.ReadBytes ((int)reader.BaseStream.Length);
		
		// Close file streams.
		reader.Close ();
		fs.Close ();
		
		return data;
	}
	
	/// <summary>
	/// Opens the file.
	/// </summary>
	/// <param name="path">File path.</param>
	void OpenFile(string path)
	{
		// Get the file extension.
		string[] splitPath = path.Split (new char[]{'.'});
		
		// Check if this is a valid file type.
		bool validFile = false;
		
		bool textFile = false;
		bool audioFile = false;
		bool videoFile = false;
		
		// Make sure this file is of a type that is allowed.
		foreach(string s in allowedExtensions)
		{
			if(splitPath[splitPath.Length - 1] == s)
			{
				validFile = true;
			}
		}
			
		// Check to see if this is a text file.
		foreach(string s in textExtensions)
		{
			// Get the kind of file that has been selected.
			if(s == splitPath[splitPath.Length-1])
			{
				textFile = true;
			}
		}
		// If it is not a text file...
		if(!textFile)
		{
			// Check to see if this is an audio file.
			foreach(string s in audioExtensions)
			{
				if(s == splitPath[splitPath.Length-1])
				{
					audioFile = true;
				}
			}
			// If it is not an audio file...
			if(!audioFile)
			{
				// Check to see if it is a video file.
				foreach(string s in videoExtensions)
				{
					if(s == splitPath[splitPath.Length-1])
					{
						videoFile = true;
					}
				}
			}
		}
		
		// If it is a text file...
		if(textFile)
		{
			// Load the text  data from the file.
			string fileString = LoadTextFileData (path);
			// Store the text data.
			MCP.loadedString = fileString;
			MCP.loadedFilePath = path;
			// Close the file browser.
			Close ();
		}
		// Otherwise, if it is an audio file...
		else if(audioFile)
		{
			int index = path.IndexOf ("/");
			string filePath = "file://" + path.Insert (index, "/");
			
			// Store the audio file path.
			MCP.audioFilePath = filePath;
			// Get the name of the media from the end of the file path.
			MCP.mediaName = filePath.Split (new char[]{'/'})[filePath.Split (new char[]{'/'}).Length-1];
			
			// Close the file browser.
			Close ();
		}
		// Otherwise, if it is a video file...
		else if(videoFile)
		{
			int index = path.IndexOf ("/");
			string filePath = "file://" + path.Insert (index, "/");
			
			// Store the video path.
			MCP.videoFilePath = filePath;
			// Get the name of the media from the end of the file path.
			MCP.mediaName = filePath.Split (new char[]{'/'})[filePath.Split (new char[]{'/'}).Length-1];
			
			// Close the file browser.
			Close ();
		}
		// Otherwise...
		else
		{
			byte[] fileData;
			
			// If this is a valid file...
			if(validFile)
			{	
				// Pull the bytes from the file into an array.
				fileData = LoadFileData (path);
	
				// Assume the data is an image, and create a texture.
				Texture2D tex = new Texture2D(2, 2);
				tex.LoadImage(fileData);
				
				// Store the texture.
				MCP.loadedTexture = tex;
				
				// Close the file browser.
				Close ();
			}
		}
	}
	
	/// <summary>
	/// Opens the folder.
	/// </summary>
	/// <param name="path">File path.</param>
	void OpenFolder(string path)
	{
		// Set the new file path.
		string newFilePath = path;
		newFilePath = newFilePath.TrimEnd(new char[]{'/', '\\'});
		// Set the current file path to the new filepath.
		currentFilePath = newFilePath;
		// Refresh the displayed files by destroying the current objects.
		RefreshFileList ();
	}
	
	/// <summary>
	/// Open the file at the specified index.
	/// </summary>
	/// <param name="index">Index.</param>
	bool Open(int index)
	{
		// If the index is not set to invalid...
		if(index != -1)
		{
			// If the path is for a folder...
			if(currentFolderObjects[index].fileType == FileType.Folder)
			{
				// Open the folder.
				OpenFolder (currentFolderObjects[index].path);
				return true;
			}
			// Otherwise...
			else
			{	
				// Open the file.
				OpenFile (currentFolderObjects[index].path);
				return true;
			}

		}
		
		return false;
	}
	
	/// <summary>
	/// Clean up and close the file browser.
	/// </summary>
	void Close()
	{
		// Reenable the main scene canvas.
		sceneCanvas.SetActive (true);
		
		// Reenable other scripts.
		SendMessage ("FileBrowserClosed", SendMessageOptions.DontRequireReceiver);
		
		// Destroy all file browser game objects.
		Destroy (localCanvas);
		
		// Remove this script from the object to close.
		Destroy (this);
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update ()
	{
		// Update the timer.
		clickTimer += Time.deltaTime;
		
		// Limit timers.
		if(clickTimer > 2f)
		{
			clickTimer = 2f;
		}
		
		// Get the list of folders and files within the current directory.
		string[] containedFolders = System.IO.Directory.GetDirectories (currentFilePath);
		string[] containedFiles = System.IO.Directory.GetFiles(currentFilePath);
		
		// Remove the double slash created by Directory functions.
		for(int i = 0; i < containedFolders.Length; i++)
		{
			containedFolders[i] = containedFolders[i].Replace("//", "/");
			containedFolders[i] = containedFolders[i].Replace("\\\\", "\\");
		}
		for(int i = 0; i < containedFiles.Length; i++)
		{
			containedFiles[i] = containedFiles[i].Replace("//", "/");
			containedFiles[i] = containedFiles[i].Replace("\\", "/");
			containedFiles[i] = containedFiles[i].Replace("\\\\", "/");
		}
		
		GameObject scrollContent = folderScrollArea.transform.GetChild (0).GetChild (0).gameObject;
		
		// If the lists of folders and files is empty...
		if(currentFolders.Count <= 0 && currentFiles.Count <= 0)
		{
			// Get a referemce to the scroll content, and resize it to contain the required content.
//			scrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(1f, Screen.height * 0.6f);
			
			float offsetY = Screen.height * -1.1f;
			// Make the folders
			int index = 0;
			
			// For each folder found...
			foreach(string folderPath in containedFolders)
			{
				FolderObject folder = new FolderObject(folderPath, FileType.Folder);
				// Make a rect for the position of the folder.
				Rect folderRect = new Rect(Screen.width * 0.51f, offsetY, Screen.width * 0.78f, Screen.height * 0.07f);
				// Store this rect in the folder to use for collision.
				folder.rect = folderRect;
				
				// Make an image object for the folder background.
				GameObject backgroundObject = MakeActionButton(scrollContent,  menuBackground, folderRect, Open, index);
				backgroundObject.name = "FolderBackgroundObject";
				backgroundObject.GetComponent<Image>().type = Image.Type.Sliced;
				fileBrowserObjects.Add (backgroundObject);
				
				// Store this object for the reference object for the folder, due to it covering the whole area of the folder objects. Also useful to change color to show selected file/folder.
				folder.gameObject = backgroundObject;
				// Make an image object to show it's a folder.
				GameObject folderIdentifier = MakeImage (backgroundObject, new Rect(folderRect.width * 0.01f + (Screen.width * 0.5f), -folderRect.height + (Screen.height * 0.5f), folderRect.width * 0.0625f, folderRect.height), folderSprite);
				folderIdentifier.name = "FolderIdentifier";
				folderIdentifier.transform.SetParent (backgroundObject.transform);
				fileBrowserObjects.Add (folderIdentifier);
				// Make a label to show the folder's name.
				string[] folderPathSplit = folderPath.Split (new char[]{'/', '\\'});
				string folderName = folderPathSplit[folderPathSplit.Length - 1];
				GameObject folderLabel = MakeLabel (backgroundObject, folderName, new Rect(folderRect.width * 0.085f + (Screen.width * 0.5f), -folderRect.height + (Screen.height * 0.5f), folderRect.width * 0.89f, folderRect.height), TextAnchor.MiddleLeft, "InnerLabel");
				folderLabel.name = "FolderLabel";
				folderLabel.transform.SetParent (backgroundObject.transform);
				fileBrowserObjects.Add (folderLabel);
				
				// Add this folder object to the lists.
				currentFolders.Add (folder.gameObject);
				currentFolderObjects.Add (folder);
				// Add the requred offset for the next object.
				offsetY += Screen.height * 0.1f;
				index++;
			}
			
			// For each file found...
			foreach(string filePath in containedFiles)
			{
				// Check if this file is one that is of a specified format,
				string[] splitFilePath = filePath.Split (new char[]{'.'}, System.StringSplitOptions.RemoveEmptyEntries);
				bool validFile = false;
				
				foreach(string s in allowedExtensions)
				{
					if(splitFilePath[splitFilePath.Length-1].Contains (s))
					{
						validFile = true;
					}
				}
				
				// If it is a valid file, create a file object for it.
				if(validFile)
				{
					FolderObject file = new FolderObject(filePath, FileType.File);
					// Make a rect for the position of the folder.
					Rect fileRect = new Rect(Screen.width * 0.51f, offsetY, Screen.width * 0.78f, Screen.height * 0.07f);
					// Store this rect in the folder to use for collision.
					file.rect = fileRect;
					// Make an image object for the folder background.
					GameObject backgroundObject = MakeActionButton (scrollContent, menuBackground, fileRect, Open, index);
					backgroundObject.name = "FileBackgroundObject";
					backgroundObject.GetComponent<Image>().type = Image.Type.Sliced;
					fileBrowserObjects.Add (backgroundObject);
					
					// Store this object for the reference object for the folder, due to it covering the whole area of the folder objects. Also useful to change color to show selected file/folder.
					file.gameObject = backgroundObject;
					// Make an image object to show it's a folder.
					GameObject fileIdentifier = MakeImage (backgroundObject, new Rect(fileRect.width * 0.01f + (Screen.width * 0.5f), -fileRect.height + (Screen.height * 0.5f), fileRect.width * 0.0625f, fileRect.height), fileSprite);
					fileIdentifier.name = "FileIdentifier";
					fileIdentifier.transform.SetParent (backgroundObject.transform);
					fileBrowserObjects.Add (fileIdentifier);
					// Make a label to show the folder's name.
					string[] filePathSplit = filePath.Split (new char[]{'/', '\\'});
					string fileName = filePathSplit[filePathSplit.Length - 1];
					GameObject fileLabel = MakeLabel (backgroundObject, fileName, new Rect(fileRect.width * 0.085f + (Screen.width * 0.5f), -fileRect.height + (Screen.height * 0.5f), fileRect.width * 0.89f, fileRect.height), TextAnchor.MiddleLeft, "InnerLabel");
					fileLabel.name = "FileLabel";
					fileLabel.transform.SetParent (backgroundObject.transform);
					fileBrowserObjects.Add (fileLabel);
					
					// Add this folder object to the lists.
					currentFiles.Add (file.gameObject);
					currentFolderObjects.Add (file);
					// Add the requred offset for the next object.
					offsetY += Screen.height * 0.1f;
					index++;
				}
			}
			
			if(index > 0)
			{
				// Set the scroll content size to hold all of the folder and file objects.
				if(offsetY + (Screen.height * 0.6f) > Screen.height * 0.6f)
				{
					scrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(folderScrollArea.GetComponent<RectTransform>().sizeDelta.x, offsetY + (Screen.height * 0.675f)/* - (Screen.height * 0.7f)*/);
				}
				else
				{
					scrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(folderScrollArea.GetComponent<RectTransform>().sizeDelta.x, Screen.height * 0.6f);
				}
				
				// Move the scroll content to start at the far left.
				if(offsetY >= Screen.height * -0.3f)
				{
					scrollContent.GetComponent<RectTransform>().transform.localPosition = new Vector2(0, (Mathf.Abs (offsetY) * -4) - (Screen.height * 0.3f) * 1000f);
				}
				else
				{
					scrollContent.GetComponent<RectTransform>().transform.localPosition = new Vector2(0, Screen.height * -0.3f);
				}
				
				// Make sure the scroll bar is in the right place.
				updateVelocity = true;
				index = 0;
			}
			
			// Reset offsetY.
			offsetY = 0;
		}
		else if(updateVelocity)
		{
			// Make sure the scroll bar is in the right place.
			folderScrollArea.GetComponent<ScrollRect>().velocity = new Vector2(0, -100f);
			folderScrollArea.transform.GetChild (2).gameObject.GetComponent<Scrollbar>().size = 1;
			
			updateVelocity = false;
		}
		
		// If this directory is empty...
		if(currentFolders.Count + currentFiles.Count == 0)
		{
			if(noFilesFoundLabel == null)
			{
				noFilesFoundLabel = MakeLabel (localCanvas, "No files found", new Rect(Screen.width * -0.25f, Screen.height * -0.3f, Screen.width * 0.5f, Screen.height * 0.2f), TextAnchor.MiddleCenter, "InnerLabel");
				noFilesFoundLabel.name = "NoFilesFoundLabel";
				noFilesFoundLabel.transform.SetParent (scrollContent.transform, true);
				noFilesFoundLabel.transform.localScale = Vector3.one;
				fileBrowserObjects.Add (noFilesFoundLabel);
			}
		}
		else
		{
			// Remove the no files found text.
			if(noFilesFoundLabel != null)
			{
				Destroy (noFilesFoundLabel);
			}
		}
	}
	
	public override void DoGUI () {}
}
