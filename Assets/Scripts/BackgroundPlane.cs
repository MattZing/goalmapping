﻿using UnityEngine;
using System.Collections;

public class BackgroundPlane : MonoBehaviour
{
	public GameObject backGroundPlane;
	
	public Texture backgroundTexture;
	public Material backgroundMat;
	
	private double height, width;
	
	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start ()
	{
		if (MCP.userInfo != null && backgroundTexture == null)
		{
			if (MCP.userInfo.background == null)
			{
				backgroundTexture = Resources.Load ("app_bg2") as Texture;
			}
			else
			{
				backgroundTexture = MCP.userInfo.background;
			}
		}
		else
		{
			if (backgroundTexture != null)
			{

			}
			else
			{
				backgroundTexture = Resources.Load ("app_bg2") as Texture;
			}
		}
	
		backGroundPlane = GameObject.CreatePrimitive (PrimitiveType.Quad);
		backGroundPlane.GetComponent<Renderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
		backGroundPlane.GetComponent<Renderer>().receiveShadows = false;
		backGroundPlane.GetComponent<Renderer>().sharedMaterial = backgroundMat;
		backGroundPlane.GetComponent<Renderer>().material.mainTexture = backgroundTexture;
		
		backGroundPlane.transform.parent = transform;
		backGroundPlane.transform.GetComponent<MeshCollider> ().enabled = false;
		backGroundPlane.name = "BackgroundPlane";
		backGroundPlane.layer = backGroundPlane.transform.parent.gameObject.layer;
		
		if (Camera.main != null)
		{
			height = Camera.main.orthographicSize * 2.0;
		}
		else if (GameObject.Find ("MainCamera") != null)
		{
			height = GameObject.Find ("MainCamera").gameObject.GetComponent<Camera>().orthographicSize * 2.0f;
		}
		
		width = height * Screen.width / Screen.height;
		backGroundPlane.transform.position = new Vector3 (backGroundPlane.transform.position.x, GetComponent<Camera>().transform.position.y, 10.0f);
		backGroundPlane.transform.localScale = new Vector3 ((float)width, (float)height, 0.1f);
	}
	
	/// <summary>
	/// Sets the background texture.
	/// </summary>
	/// <param name="tex">Texture.</param>
	public void SetBackgroundTexture( Texture tex )
	{
		if ( tex != null )
		{
			backGroundPlane.GetComponent<Renderer>().material.mainTexture = tex;
		}
	}
}
