﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using LitJson;

public class MCP : MonoBehaviour
{
	public bool zingJunior;
	public static bool isJunior;
	private static string cortexVersion;
	private const  int secondsInDay = 86400; 
	private static string playerName;
	private static double playerGUID;

    //	public static string siteURL = "http://192.168.1.146/";
    //public static string siteURL = "http://www.zingup.com/";
    public static string siteURL = "http://www.withzing.com/";

    // Dallas server
    //	public static string siteURL = "http://166.78.60.33/";
    // Hong Kong server
    //	public static string siteURL = "http://119.9.78.39/";

    public static string serverURL = siteURL+"GoalCortex"+Constants.version+"/"; 

	public static string secretKey = "21019216816";
	public static bool hasDayTripped = false;
	public static string checkLoginURL = serverURL + "checkLogin.php?"; //be sure to add a ? to your url
	public static string checkVersionURL = serverURL + "checkVersion.php";
	public static string register_url = siteURL + "GoalWeb/Billing/BMRegister.php"; //be sure to add a ? to your url
	public static string reportURL = "http://localhost/unity_test/display.php";
	public static string messagesURL = serverURL + "Messages/Inbox.php?";
	public static string notificationsURL = serverURL + "Notifications/GetNotifications.php?";
	public static string notificationReadURL = serverURL + "Notifications/DeleteNotification.php?";
	public static string notificationTrippedURL = serverURL + "Notifications/TripNotification.php?";
	public static string messageTrippedURL = serverURL + "Messages/TripMessage.php?";
	public static string messageDeleteURL = serverURL + "Messages/DeleteMessage.php?";
	public static string submitSuggestionURL = serverURL + "Suggestions/SubmitSuggestion.php?";
	public static string submitBugURL = serverURL + "Suggestions/SubmitBug.php?";
	public static string sendPMURL = serverURL + "Messages/sendPM.php?";
	public static string sendNotURL = serverURL + "Messages/sendNot.php?";
	
	// Friends
	public static string addFriend_URL = serverURL + "Friends/AddFriend.php";
	public static string getFriends_URL = serverURL + "Friends/GetFriends.php?";
	public static string getFriendInfo_URL = serverURL + "Friends/GetFriendInfo.php?";
	public static string acceptFriend_URL = serverURL + "Friends/AcceptFriend.php?";
	public static string updateFriends_URL = serverURL + "Friends/UpdateFriends.php?";
	
	public static string mentorProfileURL = serverURL + "MentorProfile/get_app_profile.php?id=";
	public static string updateMemberProfileURL = serverURL + "MemberProfile/UpdateMemberProfile.php?";
	public static string updatePurchaseURL = serverURL + "MemberProfile/UpdatePurchase.php?";
	public static string updateMemberPrefsURL = serverURL + "MemberProfile/UpdateMemberPrefs.php?id=";
	public static string memberProfileURL = serverURL + "MemberProfile/GetMemberProfile.php?id=";
	public static string updateMemberStreakURL = serverURL + "MemberProfile/UpdateMemberStreak.php?id=";
	public static string updateCaptainSugarLumpURL = serverURL + "MemberProfile/UpdateCaptainSugarLump.php?id=";
	public static string updateFirstLaunchURL = serverURL + "MemberProfile/UpdateFirstLaunch.php?id=";
	public static string mentorImageURL = serverURL + "MentorProfile/get_image.php?id=";
	public static string postProfilePhotoURL = serverURL + "MemberProfile/PostProfilePhoto.php";
	public static string getProfilePhotoURL = serverURL + "MemberProfile/GetProfilePhoto.php";
	
#if UNITY_ANDROID
	public static string getAssetBundleURL = serverURL + "AssetBundles/Android/";
#elif UNITY_IPHONE
	public static string getAssetBundleURL = serverURL + "AssetBundles/IPhone/";
#else
	public static string getAssetBundleURL = serverURL + "AssetBundles/";
#endif
	
	public static string pictureUploadURL = serverURL + "MentorProfile/upload_image.php";
	public static string eulaURL = serverURL + "EULA/eulaAgreed.php?id=";
	public static string disclaimerURL = serverURL + "EULA/disclaimerAgreed.php?id=";
	public static string wordPressURL = serverURL + "/wordpress/";
	public static string updateEventsURL = serverURL + "/Calendar/UpdateEvents.php";
	public static string getEventsURL = serverURL + "/Calendar/GetEvents.php";
	// Goal map
	public static string postGoalMapURL = serverURL + "GoalMaps/PostGoalMap.php";
	public static string updateGoalMapURL = serverURL + "GoalMaps/UpdateGoalMap.php";
	public static string getGoalMapURL = serverURL + "GoalMaps/GetGoalMap.php";
	public static string getTaskListURL = serverURL + "TaskList/GetTaskList.php";
	public static string updateTaskListURL = serverURL + "TaskList/UpdateTaskList.php";
	public static string postMailURL = serverURL + "Mail/PostMail.php";
	public static string postImagesURL = serverURL + "Images/PostImage.php";
	public static string getImagesURL = serverURL + "Images/GetImages.php?id=";
	public static string getNumberOfImagesURL = serverURL + "Images/GetNumberOfImages.php?id=";
	public static string getNumberOfClipArtImagesURL = serverURL + "Images/GetNumberOfClipArtImages.php?category=";
	public static string getClipArtURL = serverURL + "Images/GetClipArt.php?category=";
	public static string deletePictureUrl = serverURL + "Images/DeleteImage.php";
	public static string createAccountURL = serverURL + "CreateAccount.php?username=";
	
	public static string createdUserEmail = "";
	public static string createdUserMessage = "";
	public static bool loginCompleted = false;
	public static bool loginSuccess;
	public static ZingMessages zingMessages;
	public static ZingMessages zingDeletedMessages;
	public static ZingMessages zingSentMessages;
	public static ZingNotifications zingNotifications;
	public static Friend selectedFriend;
	public static bool ownProfile;
	public static UserInfo userInfo;
	public static MemberProfile friendMemberProfile;
	public static string friendUsername;
	public static System.DateTime utcNow;
	public static AvatarImage currentAvatar;
	public static Byte[] mentorProfilePicture;
	public static bool highScoresRecieved = false;
	public static bool welcomeBackMessageTripped;
	public static string downLoadProgess;
	public static bool downloadComplete;
	public static bool friendsListNeedsUpdating = false;
	public static Friend tempFriendInfo;
	public static string messageFromServer;
	public static string webURLToPlay;
	public static bool isTransmitting = false;
	public static bool purchaseFailed = false;
	public static WordPressPost[] forumPosts;
	public static Texture2D loadedTexture;
	public static string loadedString;
	public static string loadedFilePath;
	public static string audioFilePath;
	public static string videoFilePath;
	public static AudioClip loadedAudio;
#if !UNITY_ANDROID && !UNITY_IPHONE
	public static MovieTexture loadedVideo;
#endif
	public static string mediaName;
	public static GoalMapJsonList goalMapsJsons;
	public static List<GoalMap> goalMaps;
	public static bool pulledGoalMaps = true;
	public static List<Texture2D> pulledImages;
	public static int numberOfImages = 0;
	
	public static List<Task> taskList;
	
	public static WWW mediaDownload;
	public static WWW assetBundleDownload;
	public static AssetBundle downloadedAssetBundle;
	public static AssetBundle bronzeAssetBundle;
	public static AssetBundle silverAssetBundle;
	public static AssetBundle goldAssetBundle;
	public static AssetBundle clipArtBundle;
	
	public static string textResourcePathLoaded = "";
	public static string undefinedText = "!Undefined!";
	public static bool textResourcesLoaded = false;
	
	[SerializeField]
	public static Dictionary<int, string> textResources = new Dictionary<int, string>();
	
	/// <summary>
	/// Loads the text resources.
	/// </summary>
	/// <returns><c>true</c>, if text resources was loaded, <c>false</c> otherwise.</returns>
	/// <param name="path">Text asset file path.</param>
	public static bool LoadTextResources( string path )
	{
		// Don't reload the same text file.
		if ( textResourcePathLoaded == path )
		{
			return false;
		}
		
		// Clear the current text resources.
		textResources.Clear();
		
		// Lead the text file.
		TextAsset ta = Resources.Load( path ) as TextAsset;
		
		if ( ta == null )
		{
			return false;
		}
		
		// Track the number of strings in the file.
		int loadCount = 0;
		
		// Read the text file.
		System.IO.StringReader reader = null;
		reader = new System.IO.StringReader( ta.text );
		
		// Return false if the file cannot be read.
		if ( reader == null )
		{
			Debug.Log( "Text Resource FilePath: " + path + " cannot be loaded!" );
			return false;
		}
		
		string txt = null;
		char[] trimChars = { ' ' };
		
		// Read through each line of the file.
		while( (txt = reader.ReadLine()) != null )
		{
			// If there are any characters in this line...
			if ( txt.Length > 0 )
			{
				// Strip leading spaces.
				txt = txt.TrimStart( trimChars ); 
				// Replace newlines.
				txt = txt.Replace( "\\n", System.Environment.NewLine ); 
				
				// Skip comments and new lines.
				if ( txt[0] == '\n' || txt[0] == '#' ) 
				{
					continue;
				}
				
				// Split on tab.
				string[] split = txt.Split( '\t' );
				
				if ( split.Length != 2 )
				{
					continue;
				}
				
				// Get the id of the string.
				int id = int.Parse( split[0] );
				
				// If this id is already used, then don't add it again but flag it.
				if ( textResources.ContainsKey(id))
				{
					Debug.Log( "Text resource file: " + path + " already contains text for id: " + id.ToString() + " ( '" + split[1] + "' )" );
					continue;
				}
				else
				{
					// If this is not mobile, then change tap to click.
					if ( !Application.isMobilePlatform )
					{
						split[1] = split[1].Replace( "Tapped", "Clicked" );
						split[1] = split[1].Replace( "tapped", "clicked" );
						split[1] = split[1].Replace( "tapping", "clicking" );
						split[1] = split[1].Replace( "Tapping", "Clicking" );
						split[1] = split[1].Replace( "Tap", "Click" );
						split[1] = split[1].Replace( "tap", "click" );
					}
					
					// Add this text to the dictionary.
					textResources.Add( id, split[1] );	
					loadCount++;
				}
			}
		}
		
		// If any strings have been loaded, store the filepath to avoid loading it again.
		if ( loadCount > 0 )
		{
			textResourcePathLoaded = path;
		}
		
		Debug.Log( "Loaded " + loadCount.ToString() + " strings from file '" + path + "'" );
		textResourcesLoaded = true;
		return true;
	}

	public static class Constants
	{
		public const string version = "0.0.1.1";
	}
	
	public class GoalMap
	{
		// Object for goal map creation
		public string id;
		public string name;
		public string reference;
		public List<Goal> goals;
		public List<Why> whys;
		public List<How> hows;
		public List<Who> whos;
		public DateTime start;
		public DateTime finish;
	}
	
	public class GoalMapJson
	{
		// Object for goal map creation
		public string id;
		public string name;
		public string reference;
		public string goalJson;
		public string whysJson;
		public string howsJson;
		public string whosJson;
		public string startData;
		public string finishData;
	}
	
	public class GoalMapJsonList
	{
		public List<GoalMapJson> goalMapJson;
	}
	
	public class Goal
	{
		// Sub object for goal map goals
		public string goal;
		public string imageRef;
		public Texture2D image;
		public byte[] imageBytes;
	}
	
	public class Why
	{
		// Sub object for goal map goals
		public string why;
		public string imageRef;
		public Texture2D image;
		public byte[] imageBytes;
	}
	
	public class How
	{
		// Sub object for goal map goals
		public string how;
		public string imageRef;
		public Texture2D image;
		public byte[] imageBytes;
	}
	
	public class Who
	{
		// Sub object for goal map goals
		public string who;
		public string imageRef;
		public Texture2D image;
		public byte[] imageBytes;
	}
	
	public class UserInfo
	{
		public string loginSuccess;
		public string firstLaunch;
		public System.DateTime created;
		public string isTrial;
		public System.DateTime lastLogin;
		public System.DateTime lastLoginWeb;
		public System.DateTime lastLoginApp;
		public string username;
		public string firstName;
		public string lastName;
		public string email;
		public bool isMentor;
		public bool eulaAgreed;
		public string guid;
		public System.DateTime lastMessageSent;
		public System.DateTime lastPurchaseSent;
		public System.DateTime lastZingDay;
		public string suggestionsSent;
		public MyAvatar myAvatar;
		public MemberProfile memberProfile;
		public MentorProfile mentorProfile;
		public Events eventReminders;
		public Texture background;
		public string themeName;
		public Friends friend_list;
		public string isVerified;
		public string selectedFriendUsername;
		public int currentZingMonth;
	}

	public class MemberProfile
	{
		public string firstName;
		public string lastName;
		public string goalMapLevel;
		public string avatarJSON;
		public MyAvatar profileImage;
		public string colorSwatchJson;
		public string clipArtCategoriesOwnedJson;
		public string contact;
		public string numberOfGoalMaps;
		public string usingPhoto;
		public string country;
		public string aboutMe;
		public string myGoals;
	}

	public class EventReminders
	{
		public string date;
		public string message;
	}

	public class Events
	{
		public List<EventReminders> eventReminders;
	}
	
	public class Inventory
	{
		public List<MyTheme> themes;
		public List<MyAvatar> avatars;
		public List<MyProfileAddons> profileAddons;
		public List<MyMusic> music;
			
		public Inventory()
		{
			themes = new List<MyTheme>();
			avatars = new List<MyAvatar>();
			profileAddons = new List<MyProfileAddons>();
			music = new List<MyMusic>();
		}
	}

	public class MyTheme
	{
		public string themeIndex;
		public string name;
	}
		
	public class MyProfileAddons
	{
		public string addonIndex;
		public string name;
		public string bought;
		public string unlocked;
		public string showing;
		public string content;
	}
	
	public class MyMusic
	{
		public string name;
		public string clip;
		public bool selectedBackgroundMusic;
		public bool selectedActivityMusic;
	}

	public class MentorProfile
	{
		public string firstName;
		public string lastName;
		public string contact;
		public string title;
		public Texture2D picture;
		public string gender;
		public string mission;
		public string charisma;
		public string drive;
		public string loyalty;
		public string fellowship;
		public string health;
		public string experience;
		public int currentTrainingLevel;
		public List<TrainingStage> progress;
		
		public class TrainingStage
		{
			public bool completed;
			public int score;
			public int trainingLevel;
		}
	}

	public class Friends
	{
		public List<Friend> friends;
	}

	public class Friend
	{
		public string firstName;
		public string lastName;
		public string username;
		public string courseName;
		public string courseCompletion;
	}

	public class AvatarImage
	{
		public Texture avatarBackground;
		public Texture avatarImage;
	}

	public class MyAvatar
	{
		public string name;
		public int indexX;
		public int indexY;
		public int indexColor;
		public int sheetIndex;
	}

	public class ZingMessages
	{
		public List<ZingMessage> messages;
	}
	
	public class ZingNotifications
	{
		public List<ZingMessage> messages;
	}

	public class ZingMessage
	{
		public string id;
		public System.DateTime date;
		public string type;
		public string from;
		public string username;
		public string subject;
		public string message;
		public string hasTripped;
		public string viewed;
	}

	public class OfflineTransmissionLog
	{
		public List<TransmissionLogURLEntry> transmissionLog;
		public bool hasTransmitted;
	}

	public class TransmissionLogURLEntry
	{
		public DateTime timeStamp;
		public string guid;
		public List<string> url;
		public bool hasTransmitted;
	}
	
	public class Task
	{
		public string taskString;
		public string taskComplete;
		public string gmReference;
		public string howNumber;
	}
	
	public static OfflineTransmissionLog offlineTransmissionLog;
	
	public static int gmIndex = -1;
	public static bool viewingFriendGM = false;

	public static string currentGoalMapUser = "";
	
	/// <summary>
	/// Used for initialization.
	/// </summary>
	void Start()
	{
		// Set the target frame rate for the program.
		Application.targetFrameRate = 60;		
		// Get a local reference for utc time.
		utcNow = System.DateTime.UtcNow;
		// Stop this object being destroyed on scene change.
		DontDestroyOnLoad( gameObject );
		// Check the current version.
		StartCoroutine( MCP.CheckVersion() );
		
		if( Debug.isDebugBuild )
		{
			Debug.Log( "Launching - ZingApp - Debug - Version " + Constants.version );
			Debug.Log( "Server URL = " + serverURL );

			Debug.Log( "ZingDate: = " + utcNow.ToString() );
			Debug.Log( "Unity GUID implementation: = " + System.Guid.NewGuid() );
			Debug.Log( "System info: " + SystemInfo.deviceType + ", " + SystemInfo.deviceModel + ", " + SystemInfo.operatingSystem );
		}
			

#if UNITY_IOS
		if(File.Exists(GetAppDataPathIOS() + "/TransmissionLog.txt"))
		{
			offlineTransmissionLog = JsonMapper.ToObject<OfflineTransmissionLog>( ReadFile(GetAppDataPathIOS(),"TransmissionLog.txt"));
			Debug.Log("Found iOS transmission log found at " + GetAppDataPathIOS());
		}
	 	else
		{
			offlineTransmissionLog = new OfflineTransmissionLog();
			offlineTransmissionLog.transmissionLog = new List<TransmissionLogURLEntry>();
			offlineTransmissionLog.hasTransmitted = false;
			CreateFile(GetAppDataPathIOS(),"TransmissionLog.txt");
			WriteFile(GetAppDataPathIOS(),"TransmissionLog.txt",JsonMapper.ToJson(offlineTransmissionLog));
			Debug.Log("iOS transmission log not found, creating at " + GetAppDataPathIOS()+"/"+"TransmissionLog.txt");
		}
#else



#endif


	}
	
	/// <summary>
	/// Get the text for the specified ID.
	/// </summary>
	/// <param name="id">Identifier.</param>
	public static string Text( int id )
	{
		// If the text resource has not been loaded, return undefined.
		if ( textResources == null )
		{
			return undefinedText;
		}
		else
		{
			// If the text resource has no strings in it, return undefined.
			if ( textResources.Count == 0 )
			{
				return undefinedText;
			}
			else
			{
				// If the text resource contains a string for this id.
				if ( textResources.ContainsKey(id) )
				{
					return textResources[id];
				}
				// Otherwise, return undefined.
				else
				{
					return undefinedText;
				}
			}
		}		
	}
	
	/// <summary>
	/// Creates a file.
	/// </summary>
	/// <param name="path">File path.</param>
	/// <param name="fileName">File name.</param>
	public static void CreateFile(string path, string fileName)
	{ 
		// If this file does not already exist, create it.
		if(!File.Exists(path+"/"+fileName))
		{
			File.Create(path+"/"+fileName).Dispose();
		}
	}
	
	/// <summary>
	/// Writes to a file.
	/// </summary>
	/// <param name="path">File path.</param>
	/// <param name="fileName">File name.</param>
	/// <param name="data">Data to write.</param>
	public static void WriteFile(string path,string fileName,string data)
	{ 
		// If the file exists, write the data to it.
		if(File.Exists(path+"/"+fileName))
		{ 
			StreamWriter sw = new StreamWriter(path+"/"+fileName); 
			sw.WriteLine(data); sw.Flush(); 
			sw.Close(); 
		} 
	}
	
	/// <summary>
	/// Reads a file.
	/// </summary>
	/// <returns>The file.</returns>
	/// <param name="path">File path.</param>
	/// <param name="fileName">File name.</param>
	public static string ReadFile(string path,string fileName)
	{
		string output = ""; 
		// If the file exists, read it.
		if(File.Exists(path + "/" + fileName))
		{ 
			StreamReader sr = new StreamReader(path+"/"+fileName); 
			output = sr.ReadLine();
			sr.Close();
		} 
		return output; 
	}

	/// <summary>
	/// Gets the app data path for IOS.
	/// </summary>
	/// <returns>The app data path for IOS.</returns>
	public static string GetAppDataPathIOS()
	{
		string path = Application.persistentDataPath;    

		return path;
	}
	
	/// <summary>
	/// Gets the app data path for android.
	/// </summary>
	/// <returns>The app data path for android.</returns>
	public static string GetAppDataPathAndroid()
	{
		return Application.persistentDataPath;
	}
	
	/// <summary>
	/// Gets the theme.
	/// </summary>
	/// <param name="themeIndex">Theme index.</param>
	public static void GetTheme( int themeIndex )
	{
		switch( themeIndex )
		{
			case 0:
				userInfo.background = Resources.Load( "app_bg2" ) as Texture;
				break;
				
			case 1:
				// Footie theme
				userInfo.background = Resources.Load( "GUI/gamesbg_stadium" ) as Texture;
				break;
				
			case 2:
				// Dark theme
				userInfo.background = Resources.Load( "GUI/game_bg" ) as Texture;
				break;
				
			case 3:
				// Cubes theme
				userInfo.background = Resources.Load( "GUI/gamesbg_interior" ) as Texture;
				break;
				
			case 4:
				// Maze theme
				userInfo.background = Resources.Load( "GUI/gamesbg_maze" ) as Texture;
				break;
				
			case 5:
				// Curves theme
				userInfo.background = Resources.Load( "GUI/gamesbg_curves" ) as Texture;
				break;
				
			case 6:
				// 8-bit theme
//				userInfo.background = Resources.Load( "GUI/6974_8_bit_8-bit" ) as Texture;
				break;
				
			case 7:
				// Clouds theme
				userInfo.background = Resources.Load( "GUI/appbg_clouds" ) as Texture;
				break;
				
			case 8:
				// Wormhole theme
				userInfo.background = Resources.Load( "GUI/abstract2" ) as Texture;
				break;
		}
	}
	
	/// <summary>
	/// Posts the private message.
	/// </summary>
	/// <returns>The private message.</returns>
	/// <param name="to_user">To user.</param>
	/// <param name="from_user">From user.</param>
	/// <param name="subject">Subject.</param>
	/// <param name="message">Message.</param>
	/// <param name="messageType">Message type.</param>
	public static IEnumerator PostPrivateMessage( string to_user, string from_user, string subject, string message, string messageType = "0" )
	{
		isTransmitting = true;
		
		string post_url = sendPMURL + "to_user=" + WWW.EscapeURL( to_user ) + "&from_user=" + WWW.EscapeURL( from_user ) + "&subject=" + WWW.EscapeURL( subject ) + "&message=" + WWW.EscapeURL( message ) + "&messageType=" + WWW.EscapeURL( messageType ) + "&hash=" + Md5Sum( to_user + MCP.userInfo.username + secretKey );
		
		// Post the URL to the site and create a download object to get the result.
		WWW hs_post = new WWW( post_url );
		
		Debug.Log( "Post message" + post_url );
		
		yield return hs_post; // Wait until the download is done
		
		isTransmitting = false;
		
		if( hs_post.error != null )
		{
			Debug.Log( "PostPrivateMessage(): ERROR: " + hs_post.error );
		}
		else
		{
			Debug.Log( "PostPrivateMessage(): Success" );
		}
	}

	/// <summary>
	/// Sends a single user notification.
	/// </summary>
	/// <returns>The single user notification.</returns>
	/// <param name="to_user">To user.</param>
	/// <param name="from_user">From user.</param>
	/// <param name="subject">Subject.</param>
	/// <param name="message">Message.</param>
	/// <param name="messageType">Message type.</param>
	public static IEnumerator SendSingleUserNotification( string to_user, string from_user, string subject, string message, string messageType )
	{
		//isTransmitting = true;
		
		string post_url = sendNotURL + "to_user=" + WWW.EscapeURL( to_user ) + "&from_user=" + WWW.EscapeURL( from_user ) + "&subject=" + WWW.EscapeURL( subject ) + "&message=" + WWW.EscapeURL( message ) + "&messageType=" + messageType + "&hash=" + Md5Sum( to_user + MCP.userInfo.username + secretKey );
		
		// Post the URL to the site and create a download object to get the result.
		WWW hs_post = new WWW( post_url );
		
		Debug.Log( "" + post_url );
		
		yield return hs_post; // Wait until the download is done
		
		//isTransmitting = false;
		
		if( hs_post.error != null )
		{
			Debug.Log( "PostPrivateMessage(): ERROR: " + hs_post.error );
		}
		else
		{
			Debug.Log( "PostPrivateMessage(): Success: " + hs_post.text );
		}
	}

	/// <summary>
	/// Checks the login.
	/// </summary>
	/// <returns>The login.</returns>
	/// <param name="name">Name.</param>
	/// <param name="password">Password.</param>
	public static IEnumerator CheckLogin( string name, string password )
	{
		//isTransmitting = true;
		string juniorCheck;
		if( isJunior )
		{
			juniorCheck = "1";
		}
		else
		{
			juniorCheck = "0";
		}

		// Supply it with a string representing the players name and the players password.
		string hash = Md5Sum( name + secretKey );

		string post_url = checkLoginURL + "username=" + name + "&password=" + password + "&isJunior=" + juniorCheck + "&hash=" + hash;
		
		// Post the URL to the site and create a download object to get the result.
		WWW hs_post = new WWW( post_url );

		Debug.Log ("CheckLogin URL: " + post_url);

		yield return hs_post; // Wait until the download is done
		
		//isTransmitting = false;
		
		if( hs_post.error != null )
		{
			Debug.Log( "CheckLogin(" + post_url + "): ERROR: " + hs_post.error );
		}
		else
		{
			Debug.Log( "CheckLogin(): Completed" );
		}
		
		if( hs_post.text != null )
		{
			char[] charsToTrim = { ' ', '\t', '\r', '\n' };						// trim tabs and spaces from result from server
			
			string postText = hs_post.text.Trim( charsToTrim );
			
			if( postText.CompareTo( "failed" ) == 0 )
			{
				loginSuccess = false;
				loginCompleted = true;
			}
			else
			{
				Debug.Log( "Output from server: " + hs_post.text );
				userInfo = JsonMapper.ToObject<UserInfo>( hs_post.text );
		
				if( MCP.userInfo.loginSuccess.CompareTo( "true" ) == 0 )
				{
					loginSuccess = true;
					Debug.Log( "Login success: GUID= " + userInfo.guid + " Username=" + userInfo.username + " CourseName= " );
				}
				else if( userInfo.loginSuccess.CompareTo( "true" ) != 0 )
				{
					loginSuccess = false;
				}
			}
			
			loginCompleted = true;
		}
	}

	/// <summary>
	/// Checks the login.
	/// </summary>
	/// <returns>The login.</returns>
	/// <param name="name">Name.</param>
	public static IEnumerator GetFriendMemberProfile( string name )
	{
		isTransmitting = true;		
		
		friendUsername = name;
		
		Debug.Log( memberProfileURL + name + "&hash=" + Md5Sum( name + secretKey ) );
		WWW hs_get = new WWW( memberProfileURL + name + "&hash=" + Md5Sum( name + secretKey ) );
		
		Debug.Log ("getFriendUserInfo URL: " + hs_get);
		
		yield return hs_get; // Wait until the download is done
		
		
		if( hs_get.error != null )
		{
			Debug.Log( "GetFriendUserInfo(" + hs_get + "): ERROR: " + hs_get.error );
		}
		else
		{			
			if( hs_get.text != null )
			{
				if( hs_get.text == "Not Found" )
				{
					friendMemberProfile = new MemberProfile();
					friendMemberProfile.numberOfGoalMaps = hs_get.text;
				}
				else
				{
					friendMemberProfile = JsonMapper.ToObject<MemberProfile>( hs_get.text );			
				}
				
				Debug.Log( "Output from server: " + hs_get.text );
			}
			
			Debug.Log( "GetFriendUserInfo(): Completed" );
		}
		
		isTransmitting = false;
	}
	
	/// <summary>
	/// Registers the client.
	/// </summary>
	/// <returns>The client.</returns>
	/// <param name="firstName">First name.</param>
	/// <param name="lastName">Last name.</param>
	/// <param name="email">Email.</param>
	/// <param name="telephone">Telephone.</param>
	public static IEnumerator RegisterClient(string username, string firstName, string lastName, string email, DateTime dob, string password, string code )
	{
		isTransmitting = true;

		// Supply it with a string representing the players name and the players password.
		//string hash = Md5Sum (name + password + secretKey);

		WWWForm submit = new WWWForm();
		submit.AddField("submit", "Submit");
		submit.AddField("terms", "1");
		submit.AddField("firstname", WWW.EscapeURL( firstName ));
		submit.AddField("surname", WWW.EscapeURL( lastName ));
		submit.AddField("email", email);
		submit.AddField("dob_day", WWW.EscapeURL( dob.Day.ToString () ));
		submit.AddField("dob_month", WWW.EscapeURL( dob.Month.ToString () ));
		submit.AddField("dob_year", WWW.EscapeURL( dob.Year.ToString () ));
		submit.AddField("username", WWW.EscapeURL( username ));
		submit.AddField("password", WWW.EscapeURL( password ));
		submit.AddField("password2", WWW.EscapeURL( password ));
		submit.AddField("code", WWW.EscapeURL( code ));
		
		Debug.Log ("Password: " + Md5Sum (password + MCP.secretKey) );
		
		// Post the URL to the site and create a download object to get the result.
		WWW hs_post = new WWW( register_url, submit );

		yield return hs_post; // Wait until the download is done
		
		isTransmitting = false;

		if( hs_post.text != null )
		{
			if( hs_post.text.Contains( "success" ) )
			{
				createdUserMessage = "Success";
			}
			else if( hs_post.text.Contains( "username" )  && hs_post.text.Contains( "valid" ))
			{
				// Username is already taken.
				createdUserMessage = "UsernameTaken";
			}
			else
			{
				Debug.Log( "From Server: " + hs_post.text );
			}
		}
	}
	
	/// <summary>
	/// Creates the account.
	/// </summary>
	/// <returns>The account.</returns>
	/// <param name="username">Username.</param>
	/// <param name="password">Password.</param>
	/// <param name="email">Email.</param>
	/// <param name="promoCode">Promo code.</param>
	/// <param name="referrer">Referrer.</param>
	public static IEnumerator CreateAccount(string username, string password, string email, string promoCode, string referrer)
	{
		isTransmitting = true;
		
		string encodedPassword = Md5Sum (password);
		WWW hs_post = new WWW(createAccountURL + username + "&password=" + encodedPassword + "&email=" + email + "&promoCode=" + promoCode + "&referrer=" + referrer);
		
		yield return hs_post;
		
		isTransmitting = false;
		
		if(hs_post.error != null)
		{
			Debug.Log ("CreateAccount(): ERROR: " + hs_post.error);
		}
		else
		{
			Debug.Log ("CreateAccount(): Success: " + hs_post.text);
		}
	}

	/// <summary>
	/// Checks the version.
	/// </summary>
	/// <returns>The version.</returns>
	public static IEnumerator CheckVersion()
	{
		// isTransmitting = true;

		WWW hs_get = new WWW( checkVersionURL );
		
		yield return hs_get;
		
		//isTransmitting = false;

		if( hs_get.error != null )
		{
			Debug.Log( "CheckVersion(): ERROR: " + hs_get.error );
		}
		else
		{
			cortexVersion = hs_get.text; 
			Debug.Log( "CheckVersion(): Success: " + cortexVersion );
		}
	}

	/// <summary>
	/// Gets the users messages.
	/// </summary>
	/// <returns>The messages.</returns>
	public static IEnumerator GetMessages()
	{
		//isTransmitting = true;

		WWW hs_get = new WWW( messagesURL + "uid=" + userInfo.username + "&hash=" + Md5Sum( MCP.userInfo.username + secretKey ) );
		
		yield return hs_get;
		
		//isTransmitting = false;

		if( hs_get.error != null )
		{
			Debug.Log( "GetMessages(): ERROR: " + hs_get.error );
		}
		else
		{	
			Debug.Log( "Output = " + hs_get.text );
			Debug.Log( "GetMessages(): Success" );
			zingMessages = JsonMapper.ToObject<ZingMessages>( hs_get.text );
		}
	}
	
	/// <summary>
	/// Gets the users notifications.
	/// </summary>
	/// <returns>The notifications.</returns>
	public static IEnumerator GetNotifications()
	{
		//isTransmitting = true;
	
		WWW hs_get = new WWW( notificationsURL + "uid=" + userInfo.username + "&hash=" + Md5Sum( MCP.userInfo.username + secretKey ) );
		
		yield return hs_get;
		
		//isTransmitting = false;

		if( hs_get.error != null )
		{
			Debug.Log( "GetNotifications(): ERROR: " + hs_get.error );
		}
		else
		{
			Debug.Log( "GetNotifications(): Success: " + hs_get.text );
			zingNotifications = JsonMapper.ToObject<ZingNotifications>( hs_get.text );
		}
	}

	/// <summary>
	/// Marks the notification as read.
	/// </summary>
	/// <returns>The notification as read.</returns>
	/// <param name="id">Identifier.</param>
	public static IEnumerator MarkNotificationAsRead( string id )
	{
		//isTransmitting = true;

		WWW hs_get = new WWW( notificationReadURL + "id=" + id );
		Debug.Log( "Killing notification @ : " + notificationReadURL + "id=" + id );
		
		yield return hs_get;
		
		//isTransmitting = false;

		if( hs_get.error != null )
		{
			Debug.Log( "MarkNotificationAsRead(): ERROR: " + hs_get.error );
		}
		else
		{
			Debug.Log( "MarkNotificationsAsRead(): Success: " + hs_get.text );
		}
	}

	/// <summary>
	/// Marks the notification as tripped.
	/// </summary>
	/// <returns>The notification as tripped.</returns>
	/// <param name="id">Identifier.</param>
	public static IEnumerator MarkNotificationAsTripped( string id )
	{
		//isTransmitting = true;
		
		WWW hs_get = new WWW( notificationTrippedURL + "id=" + id + "&hash=" + Md5Sum ( id + secretKey ));
		Debug.Log( "Killing notification @ : " + notificationTrippedURL + "id=" + id );
		
		yield return hs_get;
		
		//isTransmitting = false;

		if( hs_get.error != null )
		{
			Debug.Log( "MarkNotificationAsTripped(): ERROR: " + hs_get.error );
		}
		else
		{
			Debug.Log( "MarkNotificationsAsTripped(): Success: " + hs_get.text );
		}
	}

	/// <summary>
	/// Marks the message as tripped.
	/// </summary>
	/// <returns>The message as tripped.</returns>
	/// <param name="id">Identifier.</param>
	public static IEnumerator MarkMessageAsTripped( string id )
	{
		//isTransmitting = true;

		WWW hs_get = new WWW( messageTrippedURL + "id=" + id + "&hash=" + Md5Sum( MCP.userInfo.username + secretKey ) );
		Debug.Log( "Killing notification @ : " + messageTrippedURL + "id=" + id + "&hash=" + Md5Sum( id + secretKey ) );
		
		yield return hs_get;
		
		//isTransmitting = false;

		if( hs_get.error != null )
		{
			Debug.Log( "MarkMessageAsTripped(): ERROR: " + hs_get.error );
		}
		else
		{
			Debug.Log( "MarkMessageAsTripped(): Success: " + hs_get.text );
		}
	}

	/// <summary>
	/// Deletes the message.
	/// </summary>
	/// <returns>The message.</returns>
	/// <param name="id">Identifier.</param>
	public static IEnumerator DeleteMessage( string id )
	{
		//isTransmitting = true;
			
		WWW hs_get = new WWW( messageDeleteURL + "id=" + id + "&hash=" + Md5Sum( id + secretKey ) );
		Debug.Log( "Killing notification @ : " + messageDeleteURL + "id=" + id + "&hash=" + Md5Sum( id + secretKey ) );
			
		yield return hs_get;
			
		//isTransmitting = false;
			
		if( hs_get.error != null )
		{
			Debug.Log( "DeleteMessage(): ERROR: " + hs_get.error );
		}
		else
		{
			Debug.Log( "DeleteMessage(): Success: " + hs_get.text );
		}
	}
	
	/// <summary>
	/// Gets the mentor profile.
	/// </summary>
	/// <returns>The mentor profile.</returns>
	public static IEnumerator GetMentorProfile()
	{
		//isTransmitting = true;
	
		WWW hs_get = new WWW( mentorProfileURL + userInfo.guid + "&hash=" + Md5Sum( MCP.userInfo.guid + secretKey ) );
		Debug.Log( "Sending to server :" + mentorProfileURL + userInfo.guid );
		yield return hs_get;
		
		//isTransmitting = false;

		if( hs_get.error != null )
		{
			Debug.Log( "GetMentorProfile(): ERROR: " + hs_get.error );
		}
		else
		{
			Debug.Log( "GetMentorProfile(): success" );
			userInfo.mentorProfile = JsonMapper.ToObject<MentorProfile>( hs_get.text );		
		}
	}
	
	/// <summary>
	/// Updates the member profile.
	/// </summary>
	/// <returns>The member profile.</returns>
	/// <param name="firstLaunched">If set to <c>true</c> first launched.</param>
	public static IEnumerator UpdateMemberProfile( bool firstLaunched = false )
	{
		//isTransmitting = true;
		
		WWWForm form = new WWWForm();
		
		if(MCP.userInfo.memberProfile.goalMapLevel == null)
		{
			MCP.userInfo.memberProfile.goalMapLevel = "0";
		}
		
		form.AddField( "id", MCP.userInfo.guid );
		form.AddField( "username", MCP.userInfo.username );
		form.AddField( "firstName", MCP.userInfo.firstName );
		form.AddField( "lastName", MCP.userInfo.lastName );
		form.AddField( "goalMapLevel", MCP.userInfo.memberProfile.goalMapLevel );
		form.AddField( "profileImage", JsonMapper.ToJson( MCP.userInfo.memberProfile.profileImage ) );
		form.AddField( "colorSwatchJson", MCP.userInfo.memberProfile.colorSwatchJson );
		form.AddField( "numberOfGoalMaps", MCP.userInfo.memberProfile.numberOfGoalMaps );
		form.AddField( "usingPhoto", MCP.userInfo.memberProfile.usingPhoto );
		form.AddField( "country", MCP.userInfo.memberProfile.country );
		form.AddField( "aboutMe", MCP.userInfo.memberProfile.aboutMe );
		form.AddField( "myGoals", MCP.userInfo.memberProfile.myGoals );
		form.AddField( "hash", Md5Sum( MCP.userInfo.guid + secretKey ) );
		string url = updateMemberProfileURL;
		
		WWW hs_get = new WWW( url, form );
		
		yield return hs_get;
		
		//isTransmitting = false;
		
		if( hs_get.error != null )
		{
			Debug.Log( "UpdateMemberProfile(): ERROR: " + hs_get.error );
		}
		else
		{
			Debug.Log( "UpdateMemberProfile(): Success: " + hs_get.text );
		}
	}
	
	/// <summary>
	/// Updates the purchase.
	/// </summary>
	/// <returns>The purchase.</returns>
	public static IEnumerator UpdatePurchase()
	{
		isTransmitting = true;
		purchaseFailed = false;
	
		WWWForm form = new WWWForm();
		
		form.AddField( "id", MCP.userInfo.guid );
		form.AddField( "lumps", "" );
		form.AddField( "hash", Md5Sum( MCP.userInfo.guid + secretKey ) );
	
		WWW hs_post = new WWW( updatePurchaseURL, form );
		
		yield return hs_post;
		
		isTransmitting = false;
		
		Debug.Log( hs_post.text );
		
		if( hs_post.error != null )
		{
			Debug.Log( "UpdatePurchase(): ERROR: " + hs_post.error );
			purchaseFailed = true;
		}
		else
		{
			Debug.Log( "UpdatePurchase(): Success: " + hs_post.text );
		}
	}

	/// <summary>
	/// Updates the member prefs.
	/// </summary>
	/// <returns>The member prefs.</returns>
	public static IEnumerator UpdateMemberPrefs()
	{
		//isTransmitting = true;

		userInfo.memberProfile.avatarJSON = JsonMapper.ToJson( userInfo.myAvatar );
		Debug.Log( JsonMapper.ToJson( userInfo.memberProfile ) );

		yield return "";
		
		//isTransmitting = false;

//		if( hs_get.error != null )
//		{
//			Debug.Log( "UpdateMemberPrefs(): ERROR: " + hs_get.error );
//		}
//		else
//		{
//			Debug.Log( "UpdateMemberPrefs(): success: " + hs_get.text );
//		}
	}
	
	/// <summary>
	/// Gets the member profile.
	/// </summary>
	/// <returns>The member profile.</returns>
	public static IEnumerator GetMemberProfile()
	{
		Debug.Log( memberProfileURL + userInfo.username + "&hash=" + Md5Sum( userInfo.username + secretKey ) );
		WWW hs_get = new WWW( memberProfileURL + userInfo.username + "&hash=" + Md5Sum( userInfo.username + secretKey ) );

		yield return hs_get;
	
		if( hs_get.error != null )
		{
			Debug.Log( "GetMemberProfile(): ERROR: " + hs_get.error );
		}
		else
		{
			//	Debug.Log ("" + hs_get.text);
			Debug.Log( "GetMemberProfile(): success" );
			
			if( hs_get.text != "Not Found" )
			{
				userInfo.memberProfile = JsonMapper.ToObject<MemberProfile>( hs_get.text );
				userInfo.memberProfile.profileImage = JsonMapper.ToObject<MyAvatar>( userInfo.memberProfile.avatarJSON );
				
				if(MCP.userInfo.memberProfile.goalMapLevel == null)
				{
					MCP.userInfo.memberProfile.goalMapLevel = "0";
				}
				
				userInfo.myAvatar = JsonMapper.ToObject<MyAvatar>( userInfo.memberProfile.avatarJSON );
			}
			
		}		
	}
	
	/// <summary>
	/// Gets the mentor image.
	/// </summary>
	/// <returns>The mentor image.</returns>
	public static IEnumerator GetMentorImage()
	{
		//isTransmitting = true;
	
		WWW hs_get = new WWW( mentorImageURL + userInfo.guid + "&hash=" + Md5Sum( MCP.userInfo.guid + secretKey ) );
		//WWW hs_get = new WWW("http://images.earthcam.com/ec_metros/ourcams/fridays.jpg");
		//Debug.Log ("Sending to server :" + mentorImageURL + userInfo.guid+ "&hash=" + Md5Sum (MCP.userInfo.guid + secretKey));
		
		yield return hs_get;
		
		//isTransmitting = false;

		if( hs_get.error != null )
		{
			Debug.Log( "GetMentorImage(): ERROR: " + hs_get.error );
		}
		else
		{		
			Debug.Log( "GetMentorImage(): success" );
			Texture2D image = new Texture2D( 4, 4, TextureFormat.ARGB32, false );
			image.LoadImage( hs_get.bytes );

			//hs_get.LoadImageIntoTexture(image);
			Debug.Log( "Attempting to load server byte[]..." + hs_get.isDone );
			image.Apply();
			//userInfo.mentorProfile.picture = image;
			//TextureScale.Bilinear (image as Texture2D, 256, 256);
			userInfo.mentorProfile.picture = image;		
		}
	}

	/// <summary>
	/// Gets the forum posts.
	/// </summary>
	/// <returns>The forum posts.</returns>
	/// <param name="args">Arguments.</param>
	/// <param name="actionName">Action name.</param>
	public static IEnumerator GetForumPosts( string args, string actionName )
	{
		//isTransmitting = true;
		
		//Debug.Log (memberProfileURL + userInfo.username + "&hash=" + Md5Sum (MCP.userInfo.guid + secretKey));
		WWW hs_get = new WWW( wordPressURL + "?" + args );

		yield return hs_get;
		
		//isTransmitting = false;

		if( hs_get.error != null )
		{
			Debug.Log( "GetForumPosts(): ERROR: " + hs_get.error );

			GameObject.FindGameObjectWithTag( "MainCamera" ).GetComponent<Forum_Screen>().ServerResponseReceived( actionName, "There was an error retrieving the forum request: " + hs_get.error );
			yield break;
		}
		else
		{	
			Debug.Log( "GetForumPosts(): success" );		
			JsonData data = JsonMapper.ToObject( hs_get.text );
			
			if( data["status"].ToString() == "ok" )
			{
				forumPosts = new WordPressPost[(int) data["count"]]; 
				
				for( int i=0; i<forumPosts.Length; i++ )
				{
					forumPosts[i] = JsonMapper.ToObject<WordPressPost>( data["posts"][i].ToJson() );
					//(ZingForumPost)data ["posts"] [i];
				}

				GameObject.FindGameObjectWithTag( "MainCamera" ).GetComponent<Forum_Screen>().ServerResponseReceived( actionName, MCP.Text (203).ToLower()/*"ok"*/ );
				//forumPosts =(ZingForumPost[]) data ["posts"];	
			}
			else
			{
				Debug.Log( "There was an error retrieving the forum request: " + data["error"].ToString() );
				GameObject.FindGameObjectWithTag( "MainCamera" ).GetComponent<Forum_Screen>().ServerResponseReceived( actionName, MCP.Text (2602)/*"There was an error retrieving the forum request: "*/ + data["error"].ToString() );
				yield break;
			}
		}
	}
	
	/// <summary>
	/// Gets the audio file.
	/// </summary>
	/// <returns>The audio file.</returns>
	public static IEnumerator GetAudioFile()
	{
#if UNITY_ANDROID || UNITY_IOS
		// If the video has not already been downloaded, download it.
		bool fileExists = false;
		string filename = /*Application.persistentDataPath + */MCP.audioFilePath.Split (new char[]{'/'})[MCP.audioFilePath.Split (new char[]{'/'}).Length - 1];
		
		if(File.Exists (Application.persistentDataPath + "/" + filename.Replace (" ", "%20")))
		{
//			fileExists = true;
		}
		
		if(fileExists)
		{
			mediaDownload = new WWW("file://" + Application.persistentDataPath + "/" + filename.Replace (" ", "%20"));
		}
		else
		{
			mediaDownload = new WWW(MCP.audioFilePath);
		}
#else
		mediaDownload = new WWW(MCP.audioFilePath);
#endif
		
		yield return mediaDownload;
		
		// Otherwise, load it from the file.
		if(mediaDownload != null)
		{
			if (mediaDownload.error != null)
			{
				Debug.Log ("GetAudioFile(): ERROR: ");
			}
			else
			{
				//Debug.Log ("GetAudioFile(): Success: " + hs_get.text);
				
				MCP.loadedAudio = MCP.mediaDownload.GetAudioClip (false, true);
				MCP.audioFilePath = "";
				
	#if UNITY_ANDROID || UNITY_IOS
	//			if(!fileExists)
	//			{
	//				FileStream fs = File.Create (Application.persistentDataPath + "/" + filename.Replace (" ", "%20"));
	//				fs.Write (mediaDownload.bytes, 0, mediaDownload.bytes.Length);
	//				fs.Close ();
	//			}
	#endif
			
			    MCP.mediaDownload = null;
			}
		}
	}
	
	/// <summary>
	/// Gets the video file.
	/// </summary>
	/// <returns>The video file.</returns>
	public static IEnumerator GetVideoFile()
	{
		isTransmitting = true;
	
		mediaDownload = new WWW(MCP.videoFilePath);
		
		yield return mediaDownload;
		
		if(mediaDownload != null)
		{
			if (mediaDownload.error != null)
			{
				Debug.Log ("GetVideoFile(): ERROR: " + mediaDownload.error);
			}
			else
			{
				Debug.Log ("GetVideoFile(): Success");
				
#if !UNITY_ANDROID && !UNITY_IPHONE
				MCP.loadedVideo = mediaDownload.GetMovieTexture();
				MCP.videoFilePath = "";
#endif
			}
		}
        
        isTransmitting = false;
	}
	
	/// <summary>
	/// Sends the forum request.
	/// </summary>
	/// <returns>The forum request.</returns>
	/// <param name="args">Arguments.</param>
	/// <param name="nonceArgs">Nonce arguments.</param>
	/// <param name="actionName">Action name.</param>
	public static IEnumerator SendForumRequest( string args, string nonceArgs, string actionName )
	{
		//isTransmitting = true;
			
		/////////////
		//string link1 = "http://192.168.1.210/wordpress/?nonce=82e9ec9324&json=submit_comment&parent=8&content=uuu&name=Richard&post_id=78&email=aa@aa.aa";
		/*string link1 = "http://127.0.0.1/wordpress/?nonce=1fd82bb28b&json=submit_comment&post_id=28&name=riso&content=unity5&parent=11";
	
			WWW www1 = new WWW (link1);
	
			yield return www1;
	
			if (www1.error != null) {
			}*/
		/////////////
	
		WWW getNonce = new WWW( wordPressURL + "?json=get_nonce&" + nonceArgs );
	
		yield return getNonce;
	
		string nonce = "";
			
		if( getNonce.error != null )
		{
			Debug.Log( "There was an error retrieving the forum request: " + getNonce.error );
			GameObject.FindGameObjectWithTag( "MainCamera" ).GetComponent<Forum_Screen>().ServerResponseReceived( actionName, MCP.Text (2602)/*"There was an error retrieving the forum request: "*/ + getNonce.error );
			yield break;
		}
		else
		{
			JsonData data = JsonMapper.ToObject( getNonce.text );
				
			if( data["status"].ToString() == "ok" )
			{
				nonce = data["nonce"].ToString();
			}
			else
			{
				Debug.Log( "There was an error retrieving the forum request: " + data["error"].ToString() );
				GameObject.FindGameObjectWithTag( "MainCamera" ).GetComponent<Forum_Screen>().ServerResponseReceived( actionName, MCP.Text (2602)/*"There was an error retrieving the forum request: "*/ + data["error"].ToString() );
				yield break;
			}		
		}
	
		string link = wordPressURL + "?nonce=" + nonce + "&" + args;
		WWW www = new WWW( link );
	
		yield return www;
			
		//isTransmitting = false;
	 
		if( www.error != null )
		{
			Debug.Log( "SendForumRequest(): ERROR: " + www.error );
			GameObject.FindGameObjectWithTag( "MainCamera" ).GetComponent<Forum_Screen>().ServerResponseReceived( actionName, MCP.Text (2602)/*"There was an error retrieving the forum request: "*/ + www.error );
			yield break;
		}
		else
		{
			Debug.Log( "SendForumRequest(): success" );
			JsonData data = JsonMapper.ToObject( www.text );
				
			if( data["status"].ToString() == "ok" )
			{
				GameObject.FindGameObjectWithTag( "MainCamera" ).GetComponent<Forum_Screen>().ServerResponseReceived( actionName, MCP.Text (203).ToLower ()/*"ok"*/ );
			}
			else
			{
				Debug.Log( "There was an error sending the forum request: " + data["error"].ToString() );
				GameObject.FindGameObjectWithTag( "MainCamera" ).GetComponent<Forum_Screen>().ServerResponseReceived( actionName, MCP.Text (2602)/*"There was an error sending the forum request: "*/ + data["error"].ToString() );
				yield break;
			}
		}
	}

	/// <summary>
	/// Sends the picture to server.
	/// </summary>
	/// <returns>The picture to server.</returns>
	public static IEnumerator SendPictureToServer()
	{
		//isTransmitting = true;

		WWWForm form = new WWWForm();
		form.AddField( "GUID", userInfo.guid );

		//TextureScale.Bilinear (mentorProfilePicture, 256, 256);
		//Byte[] byteData = mentorProfilePicture.EncodeToPNG ();
		form.AddBinaryData( "file", mentorProfilePicture, "mentorPictureTest.png", "image/png" );
		Debug.Log( form.data[0].ToString() );
		// Upload to a cgi script    
		WWW w = new WWW( pictureUploadURL, form );
		
		yield return w;
		
		//isTransmitting = false;
		
		if( w.error != null )
		{
			Debug.Log( "SendPitcureToServer(): ERROR: " + w.error );
		}
		else
		{
			Debug.Log( "SendPictureToServer(): success: " + w.text );
		}
	}

	/// <summary>
	/// Tests the download.
	/// </summary>
	/// <returns>The download.</returns>
	public static IEnumerator TestDownload()
	{
		#if UNITY_WEBPLAYER
		
			return null;
			
		#else
		
		//isTransmitting = true;
	
		WWW www = new WWW( serverURL + "/ScienceVideos/circle_chuck.mp4" );
		//yield return www;
		Debug.Log( "Beginning download" );
		downloadComplete = false;
			
		while( !www.isDone )
		{
			downLoadProgess = "downloaded " + ( www.progress * 100 ).ToString() + "%...";
			Debug.Log( "downloaded " + ( www.progress * 100 ).ToString() + "%..." );
			yield return www.isDone;
		}
			
		//isTransmitting = false;

		string fullPath = Application.persistentDataPath + "/Science3.mp4";
		File.WriteAllBytes( fullPath, www.bytes );
		Debug.Log( "Finished download" );
		downLoadProgess = "downloaded to " + fullPath;
		downloadComplete = true;
			
		#endif
	}
	
	/// <summary>
	/// Gets the users friends.
	/// </summary>
	/// <returns>The users friends.</returns>
	/// <param name="username">Username.</param>
	public static IEnumerator GetFriends( string username )
	{
		isTransmitting = true;

		string dataToPost;
		dataToPost = "username=" + WWW.EscapeURL( username ) + "&guid=" + WWW.EscapeURL( MCP.userInfo.guid ) + "&hash=" + Md5Sum( username + MCP.userInfo.guid + secretKey );
		
		// Post the URL to the site and create a download object to get the result.
		WWW hs_post = new WWW( getFriends_URL + dataToPost );

		yield return hs_post; // Wait until the download is done
		
		//isTransmitting = false;
		
		if( hs_post.error != null )
		{
			Debug.Log( "GetFriends(): ERROR: " + hs_post.error );
		}
		else
		{
			Debug.Log( "GetFriends(): success, output=" + hs_post.text.ToString());
		}

		if( hs_post.text != null )
		{
			if( hs_post.text.ToString().CompareTo( "Error" ) == 0 )
			{
				Debug.Log( "User not found - From Server: " + hs_post.text );		
			}
			else
			{
				Debug.Log ("Found user - From Server: " + hs_post.text);
				MCP.userInfo.friend_list = JsonMapper.ToObject<MCP.Friends>( hs_post.text );
			}
		}
		isTransmitting = false;
	}
	
	/// <summary>
	/// Gets the friend info.
	/// </summary>
	/// <returns>The friend info.</returns>
	/// <param name="username">Username.</param>
	public static IEnumerator GetFriendInfo( string username )
	{
		isTransmitting = true;

		string dataToPost;
		dataToPost = "username=" + WWW.EscapeURL( username ) + "&hash=" + Md5Sum( WWW.EscapeURL( username ) + secretKey );
		Debug.Log( getFriendInfo_URL + dataToPost );
		// Post the URL to the site and create a download object to get the result.
		WWW hs_post = new WWW( getFriendInfo_URL + dataToPost );

		yield return hs_post; // Wait until the download is done
		
		isTransmitting = false;

		if( hs_post.error == null )
		{
			Debug.Log( "GetFriendInfo(): success" );
		
			if( hs_post.text != null )
			{
				if( hs_post.text.ToString().CompareTo( "Error" ) == 0 )
				{
					Debug.Log( "User not found - From Server: " + hs_post.text );
					
				}
				else
				{
					Debug.Log( "Found user - From Server: " + hs_post.text );
					
					if( MCP.userInfo.friend_list == null )
					{
						MCP.userInfo.friend_list = new Friends();
						MCP.userInfo.friend_list.friends = new List<Friend>();
					}
					
					tempFriendInfo = new Friend();
					tempFriendInfo = JsonMapper.ToObject<MCP.Friend>( hs_post.text );
					
					if( MCP.userInfo.friend_list.friends.Find( x => x.username == tempFriendInfo.username ) == null )
					{
						MCP.userInfo.friend_list.friends.Add( tempFriendInfo );
						friendsListNeedsUpdating = true;
					}
				}
			}
		}
		else
		{
			Debug.Log( "GetFriendInfo(): ERROR: " + hs_post.error );
		}
	}
	
	/// <summary>
	/// Accepts the friend.
	/// </summary>
	/// <returns>The friend.</returns>
	public static IEnumerator AcceptFriend(string friendUsername)
	{
		//isTransmitting = true;
	
		string dataToPost;
		dataToPost = "username=" + WWW.EscapeURL( MCP.userInfo.username ) + "&from_user=" + WWW.EscapeURL( friendUsername ) + "&friendsList=" + WWW.EscapeURL( JsonMapper.ToJson( MCP.userInfo.friend_list ) )
			+ "&hash=" + Md5Sum( WWW.EscapeURL( MCP.userInfo.username ) + WWW.EscapeURL( friendUsername ) + JsonMapper.ToJson( MCP.userInfo.friend_list ) + secretKey );
		Debug.Log( acceptFriend_URL + dataToPost );
		// Post the URL to the site and create a download object to get the result.
		WWW hs_post = new WWW( acceptFriend_URL + dataToPost );

		yield return hs_post; // Wait until the download is done
		
		//isTransmitting = false;
		
		if( hs_post.error != null )
		{
			Debug.Log( "AcceptFriend(): ERROR: " + hs_post.error );
		}
		else
		{
			Debug.Log( "AcceptFriend(): success" );
		}

		if( hs_post.text != null )
		{
			if( hs_post.text.CompareTo( " User not found" ) == 0 )
			{
				Debug.Log( "User not found - From Server: " + hs_post.text );
			}
			else
			{
				Debug.Log( "Found user - From Server: " + hs_post.text );
				//MCP.userInfo.friend_list = JsonMapper.ToObject<MCP.Friends> (hs_post.text);
				friendsListNeedsUpdating = false;
			}
		}
	}

	/// <summary>
	/// Updates the friends list.
	/// </summary>
	/// <returns>The friends list.</returns>
	public static IEnumerator UpdateFriends()
	{
		isTransmitting = true;

		string dataToPost;
		dataToPost = "username=" + WWW.EscapeURL( MCP.userInfo.username ) + "&from_user=" + WWW.EscapeURL( tempFriendInfo.username ) + "&friendsList=" + WWW.EscapeURL( JsonMapper.ToJson( MCP.userInfo.friend_list ) )
			+ "&hash=" + Md5Sum( WWW.EscapeURL( MCP.userInfo.username ) + WWW.EscapeURL( tempFriendInfo.username ) + JsonMapper.ToJson( MCP.userInfo.friend_list ) + secretKey );
		Debug.Log( updateFriends_URL + dataToPost );
		// Post the URL to the site and create a download object to get the result.
		WWW hs_post = new WWW( updateFriends_URL + dataToPost );

		yield return hs_post; // Wait until the download is done
		
		isTransmitting = false;
		
		if( hs_post.error != null )
		{
			Debug.Log( "UpdateFriends(): ERROR: " + hs_post.error );
		}
		else
		{
			Debug.Log( "UpdateFriends(): success" );
		}

		if( hs_post.text != null )
		{
			if( hs_post.text.CompareTo( " User not found" ) == 0 )
			{
				Debug.Log( "User not found - From Server: " + hs_post.text );
			}
			else
			{
				Debug.Log( "Found user - From Server: " + hs_post.text );
				friendsListNeedsUpdating = false;
			}
		}
	}
	
	/// <summary>
	/// Adds the friend.
	/// </summary>
	/// <returns>The friend.</returns>
	/// <param name="username">Username.</param>
	public static IEnumerator AddFriend( string username )
	{
		//isTransmitting = true;
		
		WWWForm form = new WWWForm();
		
		form.AddField("username", WWW.EscapeURL( username ));
		form.AddField("from_user", WWW.EscapeURL( MCP.userInfo.username ));
		form.AddField("friendList", JsonMapper.ToJson( MCP.userInfo.friend_list ));
		form.AddField("hash", Md5Sum( WWW.EscapeURL( username ) + WWW.EscapeURL( userInfo.username ) + secretKey ));

		WWW hs_post = new WWW( addFriend_URL, form );

		yield return hs_post; // Wait until the download is done
		
		//isTransmitting = false;

		//Debug.Log ("From Server: " + hs_post.text);
		
		if( hs_post.error != null )
		{
			Debug.Log( "AddFriend(): ERROR: " + hs_post.error );
		}
		else
		{
			Debug.Log( "AddFriend(): success" );
		}

		if( hs_post.text != null )
		{
			if( hs_post.text.Contains( "User found" ) )
			{
				messageFromServer = "User found";
			}
			else
			{
				messageFromServer = "User not found";
			}
			
			if( hs_post.text.Contains( "PM succesfully sent" ) )
			{
				messageFromServer += ", request sent";
			}
			Debug.Log ("Message: " + messageFromServer);
		}
	}
	
	/// <summary>
	/// Submits the contact message.
	/// </summary>
	/// <returns>The contact message.</returns>
	/// <param name="subject">Subject.</param>
	/// <param name="message">Message.</param>
	public static IEnumerator SubmitContactMessage( string subject, string message )
	{
		//This connects to a server side php script that will add the name and score to a MySQL DB.
		// Supply it with a string representing the players name and the players score.
		//string hash = Md5Sum (name + score + secretKey);
		
		//isTransmitting = true;

		string post_url = sendPMURL + "to_user=" + WWW.EscapeURL( "Cortex" ) + "&from_user=" + WWW.EscapeURL( MCP.userInfo.username ) + "&subject=" + WWW.EscapeURL( subject ) + "&message=" + WWW.EscapeURL( message ) + "&hash=" + Md5Sum( MCP.userInfo.username + WWW.EscapeURL( MCP.userInfo.username ) + secretKey );

		// Post the URL to the site and create a download object to get the result.
		WWW hs_post = new WWW( post_url );
		
		//isTransmitting = false;

		Debug.Log( "" + post_url );
		yield return hs_post; // Wait until the download is done
		
		if( hs_post.error != null )
		{
			Debug.Log( "SubmitContactMessage(): ERROR: " + hs_post.error );
		}
		else
		{
			Debug.Log( "SubmitContactMessage(): success" );
		}
	}
	
	/// <summary>
	/// Submits the suggestion.
	/// </summary>
	/// <returns>The suggestion.</returns>
	/// <param name="suggestion">Suggestion.</param>
	public static IEnumerator SubmitSuggestion( string suggestion )
	{
		//isTransmitting = true;
	
		string dataToPost;
		dataToPost = "from_user=" + WWW.EscapeURL( MCP.userInfo.username ) + "&message=" + WWW.EscapeURL( suggestion ) + "&hash=" + Md5Sum( MCP.userInfo.username + suggestion + secretKey );
		Debug.Log( submitSuggestionURL + dataToPost );
		// Post the URL to the site and create a download object to get the result.
		WWW hs_post = new WWW( submitSuggestionURL + dataToPost );

		yield return hs_post; // Wait until the download is done
		
		//isTransmitting = false;

		if( hs_post.text != null )
		{
			if( hs_post.text.CompareTo( " success" ) == 0 )
			{
				Debug.Log( "SubmitSuggestion(): success...From Server: " + hs_post.text );
			}
			else
			{
				Debug.Log( "SubmitSuggestion(): ERROR...From Server: " + hs_post.text );
			}
		}
	}

	/// <summary>
	/// Submits the bug.
	/// </summary>
	/// <returns>The bug.</returns>
	/// <param name="bugInfo">Bug info.</param>
	/// <param name="pdata">Pdata.</param>
	public static IEnumerator SubmitBug( string bugInfo, string pdata )
	{
		//isTransmitting = true;

		string dataToPost;
		dataToPost = "from_user=" + WWW.EscapeURL( MCP.userInfo.username ) + "&message=" + WWW.EscapeURL( bugInfo ) + "&pdata=" + WWW.EscapeURL( pdata ) + "&hash=" + Md5Sum( MCP.userInfo.username + bugInfo + secretKey );
		//Debug.Log (submitBugURL + dataToPost);
		
		// Post the URL to the site and create a download object to get the result.
		WWW hs_post = new WWW( submitBugURL + dataToPost );

		yield return hs_post; // Wait until the download is done
		
		//isTransmitting = false;

		if( hs_post.text != null )
		{
			if( hs_post.text.CompareTo( " success" ) == 0 )
			{
				Debug.Log( "SubmitBug(): success... From Server: " + hs_post.text );
			}
			else
			{
				Debug.Log( "SubmitBug(): ERROR... From Server: " + hs_post.text );
			}
		}
	}
	
	/// <summary>
	/// Encrypts the string with Md5Sum.
	/// </summary>
	/// <returns>The sum.</returns>
	/// <param name="strToEncrypt">String to encrypt.</param>
	public  static string Md5Sum( string strToEncrypt )
	{
		System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
		byte[] bytes = ue.GetBytes( strToEncrypt );

		// encrypt bytes
		System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
		byte[] hashBytes = md5.ComputeHash( bytes );

		// Convert the encrypted bytes back to a string (base 16)
		string hashString = "";

		for( int i = 0; i < hashBytes.Length; i++ )
		{
			hashString += System.Convert.ToString( hashBytes[i], 16 ).PadLeft( 2, '0' );
		}

		return hashString.PadLeft( 32, '0' );
	}
	
	/// <summary>
	/// EULA is agreed.
	/// </summary>
	/// <returns>Post success.</returns>
	public static IEnumerator EULAagreed()
	{
		//isTransmitting = true;

		string hash = Md5Sum( userInfo.username + secretKey );

		string post_url = eulaURL + WWW.EscapeURL( userInfo.username ) + "&version=" + Constants.version + "&hash=" + hash;
		Debug.Log( "EULA has been agreed: " + post_url );
		// Post the URL to the site and create a download object to get the result.
		WWW hs_post = new WWW( post_url );
		
		yield return hs_post; // Wait until the download is done
		
		//isTransmitting = false;

		if( hs_post.error != null )
		{
			Debug.Log( "There was an error posting EULA data: " + hs_post.error );
		}
		else
			Debug.Log( "EULAagreed(): " + hs_post.text );
	}
	
	/// <summary>
	/// Disclaimer is agreed.
	/// </summary>
	/// <returns>Post success.</returns>
	public static IEnumerator DisclaimerAgreed()
	{
		//isTransmitting = true;
		
		string hash = Md5Sum( userInfo.username + secretKey );

		string post_url = disclaimerURL + WWW.EscapeURL( userInfo.username ) + "&version=" + Constants.version + "&hash=" + hash;
		Debug.Log( "EULA has been agreed: " + post_url );
		// Post the URL to the site and create a download object to get the result.
		WWW hs_post = new WWW( post_url );
		
		yield return hs_post; // Wait until the download is done
		
		//isTransmitting = false;

		if( hs_post.error != null )
		{
			Debug.Log( "There was an error posting EULA data: " + hs_post.error );
		}
		else
			Debug.Log( "DisclaimerAgreed(): " + hs_post.text );
	}
	
	/// <summary>
	/// Sets the name of the player.
	/// </summary>
	/// <param name="playerNameIn">Player name in.</param>
	public static void SetPlayerName( string playerNameIn )
	{
		playerName = playerNameIn;
	}

	/// <summary>
	/// Gets the name of the player.
	/// </summary>
	/// <returns>The player name.</returns>
	public static string GetPlayerName()
	{
		return playerName;
	}
	
	/// <summary>
	/// Gets the time stamp.
	/// </summary>
	/// <returns>The time stamp.</returns>
	public static string getTimeStamp()
	{
		System.DateTime dateValue = System.DateTime.UtcNow;
		string formatForMySql = dateValue.ToString( "yyyy-MM-dd HH:mm:ss" );

		return formatForMySql;
	}

	/// <summary>
	/// Updates the member streak.
	/// </summary>
	/// <returns>The member streak.</returns>
	public static IEnumerator UpdateMemberStreak()
	{
		//isTransmitting = true;

//		Debug.Log( updateMemberStreakURL + userInfo.username + "&streak=" + userInfo.memberProfile.currentZingStreak + "&hash=" + Md5Sum( MCP.userInfo.username + userInfo.memberProfile.currentZingStreak + secretKey ) );
//		WWW hs_get = new WWW( updateMemberStreakURL + userInfo.guid + "&streak=" + userInfo.memberProfile.currentZingStreak + "&hash=" + Md5Sum( MCP.userInfo.guid + userInfo.memberProfile.currentZingStreak + secretKey ) );

		yield return "";
		
		//isTransmitting = false;

//		if( hs_get.error != null )
//		{
//			Debug.Log( "There was an error updating the member profile " + hs_get.error );
//		}
//		else
//		{
//			Debug.Log( "UpdateMemberStreak(): " + hs_get.text );
//		}
	}

	/// <summary>
	/// Updates captain sugar lump's profile.
	/// </summary>
	/// <returns>The captain sugar lump.</returns>
	public static IEnumerator UpdateCaptainSugarLump()
	{
		isTransmitting = true;
	
		string postString = ( updateCaptainSugarLumpURL + userInfo.guid + "&hash=" + Md5Sum( MCP.userInfo.guid + secretKey ) );
		WWW hs_get = new WWW( postString );

		yield return hs_get;
		
		isTransmitting = false;

		if( hs_get.error != null )
		{
			Debug.Log( "There was an error updating the member profile " + hs_get.error );
		}
		else
		{
			Debug.Log( "UpdateCaptainSugarLump(): " + hs_get.text );
		}
	}

	/// <summary>
	/// Updates the first launch.
	/// </summary>
	/// <returns>The first launch.</returns>
	public static IEnumerator UpdateFirstLaunch()
	{
		isTransmitting = true;

		Debug.Log( updateFirstLaunchURL + userInfo.guid + "&hash=" + Md5Sum( MCP.userInfo.guid + secretKey ) );
		WWW hs_get = new WWW( updateFirstLaunchURL + userInfo.guid + "&hash=" + Md5Sum( MCP.userInfo.guid + secretKey ) );

		yield return hs_get;
		
		isTransmitting = false;

		if( hs_get.error != null )
		{
			Debug.Log( "There was an error updating the member profile " + hs_get.error );
		}
		else
		{
			Debug.Log( "UpdateFirstLaunch(): " + hs_get.text );
		}
	}
	
	// Check if user logged in on a consecutive day, and update the map position ( currentZingDay ) appropriately , returns daytype to mainscreen caller
	public static int CheckConsecutiveDay ()
	{ 
		int message = 0; // Next day has not been tripped
		
		if (!hasDayTripped)
		{
			hasDayTripped = true;
			
			DateTime lastLoginDate = new DateTime (userInfo.lastLogin.Year, userInfo.lastLogin.Month, userInfo.lastLogin.Day, 0, 0, 0);
			DateTime currentDateTime = System.DateTime.UtcNow;
			DateTime endDayTime = lastLoginDate.AddDays (1);
			DateTime goneOverDate = lastLoginDate.AddDays (2);
			
			if (currentDateTime.Ticks >= endDayTime.Ticks && currentDateTime.Ticks <= goneOverDate.Ticks)	// Next day
			{
//				Debug.Log ("Think its next day. Yesterdays exercise count is: " + userInfo.memberProfile.todaysExerciseCount.ToString ());
//				isNewZingDay = true;
//				
//				if (int.Parse(userInfo.memberProfile.todaysExerciseCount) > 0)
//				{
//					userInfo.currentZingDay++;
//					userInfo.memberProfile.currentZingDay = userInfo.currentZingDay.ToString ();
//				}
//				todaysExerciseCount = 0;
//				userInfo.memberProfile.todaysExerciseCount = todaysExerciseCount.ToString ();
//				
//				if (userInfo.currentZingDay % 30 == 0) // check if 30 days have passed, and incriment zingMonth.
//					userInfo.currentZingMonth++;
//				
//				message = 1; // Zing streak
			}
			else if (currentDateTime.Ticks > goneOverDate.Ticks)
			{ // 2 or more days since login
//				Debug.Log ("Lost bonus, shame.");
//				isNewZingDay = true;
//				
//				if (int.Parse (userInfo.isTrial) == 1) {  // 2 or more days since login AND trial
//					userInfo.currentZingDay += 2;
//				} else { // 2 or more days since login and PAID version
//					if (int.Parse (userInfo.memberProfile.todaysExerciseCount) > 0) { // Move them on if they have done 1 or more exercise the previous day
//						userInfo.currentZingDay++;
//					}
//				}
//				
//				userInfo.memberProfile.currentZingDay = userInfo.currentZingDay.ToString ();
//				todaysExerciseCount = 0;
//				userInfo.memberProfile.todaysExerciseCount = todaysExerciseCount.ToString (); // reset todays exercise count to 0
//				
//				message = 3; // Lost consecutive day bonus
			}
			else
			{
				message = 2; // Same day login
			}
		}

		return message;
	}
	
	/// <summary>
	/// Compares the DateTimes.
	/// </summary>
	/// <param name="date1">Date1.</param>
	/// <param name="date2">Date2.</param>
	public static void CompareDateTime( System.DateTime date1, System.DateTime date2 )
	{
		//86400
		int result = DateTime.Compare( date1, date2 );
		string relationship = "";

		if( result < 0 )
			relationship = "is earlier than";
		else
		if( result == 0 )
			relationship = "is the same time as";
		else
			relationship = "is later than";
				
		TimeSpan span1 = date1.Subtract( new DateTime( 1970, 1, 1, 0, 0, 0 ) ); 
		//TimeSpan span2 = date2.Subtract( new DateTime( 1970, 1, 1, 0, 0, 0 ) ); 
		
		Debug.Log( "DateCompare: " + span1 + " is " + relationship + date2.ToString() );
	}

	/// <summary>
	/// Updates the EULA.
	/// </summary>
	public void UpdateEula()
	{
		StartCoroutine( MCP.EULAagreed() );
	}

	/// <summary>
	/// Updates the disclaimer.
	/// </summary>
	public void UpdateDisclaimer()
	{
		StartCoroutine( MCP.DisclaimerAgreed() );
	}
	
	/// <summary>
	/// Posts the goal map.
	/// </summary>
	/// <returns>The goal map.</returns>
	/// <param name="goalMap">Goal map.</param>
	public static IEnumerator PostGoalMap(GoalMap goalMap)
	{
		isTransmitting = true;
		
		string goalJson = goalMap.goals == null ? "" : JsonMapper.ToJson (goalMap.goals);
		string whysJson = goalMap.whys == null ? "" : JsonMapper.ToJson (goalMap.whys);
		string whosJson = goalMap.whos == null ? "" : JsonMapper.ToJson (goalMap.whos);
		string howsJson = goalMap.hows == null ? "" : JsonMapper.ToJson (goalMap.hows);
		
		if(goalMap.name == null)
		{
			goalMap.name = MCP.Text (2501);/*"My Goal Map";	*/
		}
		
		WWWForm form = new WWWForm();
		form.AddField ( "id", userInfo.guid);
		form.AddField ( "username", userInfo.username);
		form.AddField ( "name", goalMap.name.Replace (' ', '_'));
		form.AddField ( "reference", goalMap.reference);
		form.AddField ( "goalJsonData", goalJson);
		form.AddField ( "whysJsonData", whysJson);
		form.AddField ( "whosJsonData", whosJson);
		form.AddField ( "howsJsonData", howsJson);
		form.AddField ( "startData", goalMap.start.ToString ());
		form.AddField ( "finishData", goalMap.finish.ToString ());
		
		WWW hs_post = new WWW(postGoalMapURL, form);
		
		yield return hs_post;
		
		isTransmitting = false;
		
		if( hs_post.error != null )
		{
			Debug.Log( "Error: PostGoalMap: " + hs_post.error );
		}
		else
		{
			Debug.Log( "Success: PostGoalMap(): " + hs_post.text );
		}
	}
	
	public static IEnumerator UpdateGoalMap(GoalMap goalMap)
	{
		isTransmitting = true;
		
		string goalJson = goalMap.goals == null ? "" : JsonMapper.ToJson (goalMap.goals);
		string whysJson = goalMap.whys == null ? "" : JsonMapper.ToJson (goalMap.whys);
		string whosJson = goalMap.whos == null ? "" : JsonMapper.ToJson (goalMap.whos);
		string howsJson = goalMap.hows == null ? "" : JsonMapper.ToJson (goalMap.hows);
		string reference = goalMap.reference == null ? MCP.userInfo.username + DateTime.UtcNow.Ticks.ToString () : goalMap.reference;
		
		if(goalMap.name == null)
		{
			goalMap.name = MCP.Text (2501);/*"My Goal Map";	*/
		}
		
		WWWForm form = new WWWForm();
		form.AddField ( "id", MCP.userInfo.guid );
		form.AddField ( "username", MCP.userInfo.username );
		form.AddField ( "name", goalMap.name.Replace (' ', '_') );
		form.AddField ( "reference", reference);
		form.AddField ( "goalJsonData", goalJson );
		form.AddField ( "whysJsonData", whysJson );
		form.AddField ( "whosJsonData", whosJson );
		form.AddField ( "howsJsonData", howsJson );
		form.AddField ( "startData", goalMap.start.ToString () );
		form.AddField ( "finishData", goalMap.finish.ToString () );
		
		WWW hs_post = new WWW(updateGoalMapURL, form);
		
		yield return hs_post;
		
		isTransmitting = false;
		
		if( hs_post.error != null )
		{
			Debug.Log( "Error: PostGoalMap: " + hs_post.error );
		}
		else
		{
			Debug.Log( "Success: PostGoalMap(): " + hs_post.text );
		}
	}
	
	/// <summary>
	/// Gets the goal map.
	/// </summary>
	/// <returns>The goal map.</returns>
	/// <param name="username">Username.</param>
	public static IEnumerator GetGoalMap(string username)
	{
		isTransmitting = true;

		currentGoalMapUser = username;
		
		string getString = ( getGoalMapURL + "?id=" + username + "&hash=" + Md5Sum( username + secretKey ) );
		WWW hs_get = new WWW( getString );
		
		yield return hs_get;
		
		isTransmitting = false;
		
		if( hs_get.error != null )
		{
			Debug.Log( "Error: GetGoalMap: " + hs_get.error );
		}
		else
		{
			Debug.Log( "Success: GetGoalMap(): " + hs_get.text );
			
			// Get the json string.
			string json = hs_get.text.Replace ("_", " ");
			// Seperate the list of goal maps.
			MCP.goalMapsJsons = JsonMapper.ToObject<GoalMapJsonList>(json);
			MCP.goalMaps = new List<GoalMap>();
			
			// For each goal map in the list, pull the object data from the jsons.
			for(int i = 0; i < goalMapsJsons.goalMapJson.Count; i++)
			{
				GoalMap gm = new GoalMap();
				if(goalMapsJsons.goalMapJson[i].name != null)
				{
					gm.name = goalMapsJsons.goalMapJson[i].name.Replace ('_', ' ');
				}
				else
				{
					gm.name = MCP.Text (2501);/*"My Goal Map";*/
				}
				
				gm.reference = goalMapsJsons.goalMapJson[i].reference;
				
				gm.goals = JsonMapper.ToObject<List<Goal>>(goalMapsJsons.goalMapJson[i].goalJson);
				gm.whys = JsonMapper.ToObject<List<Why>>(goalMapsJsons.goalMapJson[i].whysJson);
				gm.whos = JsonMapper.ToObject<List<Who>>(goalMapsJsons.goalMapJson[i].whosJson);
				gm.hows = JsonMapper.ToObject<List<How>>(goalMapsJsons.goalMapJson[i].howsJson);
				
				gm.start = DateTime.Parse(goalMapsJsons.goalMapJson[i].startData);
				gm.finish = DateTime.Parse(goalMapsJsons.goalMapJson[i].finishData);
				
				MCP.goalMaps.Add (gm);
			}
			
			// Flag as pulled.
			pulledGoalMaps = true;
		}
		
		isTransmitting = false;
	}
	
	/// <summary>
	/// Posts the image.
	/// </summary>
	/// <returns>The image.</returns>
	/// <param name="username">Username.</param>
	/// <param name="reference">Reference.</param>
	/// <param name="imageBytes">Image bytes.</param>
	public static IEnumerator PostImage(string username, string reference, byte[] imageBytes)
	{
		isTransmitting = true;
		
		WWWForm form = new WWWForm();
		form.AddField ("id", username);
		form.AddField ("reference", reference);
		form.AddBinaryData( "file", imageBytes, "imageUpload.png", "image/png" );
		
		WWW hs_post = new WWW( postImagesURL, form );
		
		yield return hs_post;
		
		isTransmitting = false;
		
		if( hs_post.error != null )
		{
			Debug.Log( "PostImage() Error: " + hs_post.error );
		}
		else
		{
			Debug.Log( "PostImage() Success: " + hs_post.text );
		}
	}
	
	/// <summary>
	/// Gets the image.
	/// </summary>
	/// <returns>The image.</returns>
	/// <param name="username">Username.</param>
	/// <param name="reference">Reference.</param>
	/// <param name="imageNumber">Image number.</param>
	public static IEnumerator GetImage(string username, string reference = "", string imageNumber = "")
	{
		isTransmitting = true;
		
		string postString = ( getImagesURL + username + "&reference=" + reference + "&imageNumber=" + imageNumber );
		WWW hs_get = new WWW( postString );
		
		yield return hs_get;
		
		isTransmitting = false;
		
		if( hs_get.error != null )
		{
			Debug.Log( "GetImage() Error: " + hs_get.error );
		}
		else
		{
			Debug.Log( "GetImage() Success: " + hs_get.text );
			
			Texture2D pulledTex = new Texture2D(4, 4, TextureFormat.ARGB32, false);
			
			hs_get.LoadImageIntoTexture(pulledTex);
			
			MCP.pulledImages.Add (pulledTex);
		}
	}
	
	/// <summary>
	/// Gets the number of images the user has on the server.
	/// </summary>
	/// <returns>The number of images.</returns>
	/// <param name="username">Username.</param>
	public static IEnumerator GetNumberOfImages(string username)
	{
		isTransmitting = true;
		
		string postString = ( getNumberOfImagesURL + username );
		WWW hs_get = new WWW( postString );
		
		yield return hs_get;
		
		isTransmitting = false;
		
		if( hs_get.error != null )
		{
			Debug.Log( "GetNumberOfImage() Error: " + hs_get.error );
		}
		else
		{
			Debug.Log( "GetNumberOfImage() Success: " + hs_get.text );
			
			MCP.numberOfImages = int.Parse (hs_get.text);
		}
	}
	
	/// <summary>
	/// Gets the number of clip art images the user has access to.
	/// </summary>
	/// <returns>The number of clip art images.</returns>
	/// <param name="allowedCategoriesJson">Allowed categories json.</param>
	public static IEnumerator GetNumberOfClipArtImages(string allowedCategoriesJson)
	{
		isTransmitting = true;
		
		string postString = ( getNumberOfClipArtImagesURL + allowedCategoriesJson );
		WWW hs_get = new WWW( postString );
		
		yield return hs_get;
		
		isTransmitting = false;
		
		if( hs_get.error != null )
		{
			Debug.Log( "GetNumberOfClipArtImage() Error: " + hs_get.error );
		}
		else
		{
			Debug.Log( "GetNumberOfClipArtImage() Success: " + hs_get.text );
			
			try
			{
				MCP.numberOfImages = int.Parse (hs_get.text);
			}
			catch(Exception e)
			{
				Debug.Log ("No images found: " + e);
				MCP.numberOfImages = 0;
			}
		}
	}
	
	/// <summary>
	/// Gets the clip art.
	/// </summary>
	/// <returns>The clip art.</returns>
	/// <param name="allowedCategoriesJson">Allowed categories json.</param>
	/// <param name="imageNumber">Image number.</param>
	public static IEnumerator GetClipArt(string allowedCategoriesJson, string imageNumber)
	{
		isTransmitting = true;
		
		// Switch the category json to a list for easier processing.
		
		string postString = (getClipArtURL + allowedCategoriesJson + "&imageNumber=" + imageNumber);
		WWW hs_get = new WWW(postString);
		
		yield return hs_get;
		
		isTransmitting = false;
		
		if( hs_get.error != null )
		{
			Debug.Log( "GetClipArt() Error: " + hs_get.error );
		}
		else
		{
			Debug.Log( "GetClipArt() Success: " + hs_get.text );
			
			Texture2D pulledTex = new Texture2D(4, 4, TextureFormat.ARGB32, false);
			
			hs_get.LoadImageIntoTexture(pulledTex);
			
			MCP.pulledImages.Add (pulledTex);
		}
	}
	
	public static IEnumerator DeletePicture(int imageNumber)
	{
		isTransmitting = true;
		
		WWWForm form = new WWWForm();
		
		form.AddField ("id", MCP.userInfo.username);
		form.AddField ("imageNumber", imageNumber);
		
		WWW hs_post = new WWW(deletePictureUrl, form);
		
		yield return hs_post;
		
		isTransmitting = false;
		
		if (hs_post.error != null) 
		{
			Debug.Log ("DeletePicture(): ERROR " + hs_post.error );
		}
		else
		{
			Debug.Log( "DeletePicture(): Success: " + hs_post.text );
		}
	}

	/// <summary>
	/// Updates the events.
	/// </summary>
	/// <returns>The events.</returns>
	public static IEnumerator UpdateEvents()
	{
		isTransmitting = true;
		
		WWWForm form = new WWWForm ();
		
		form.AddField ("id", MCP.userInfo.guid);
		form.AddField ("eventJson", JsonMapper.ToJson (userInfo.eventReminders));
		
		string url = updateEventsURL;
		
		WWW hs_post = new WWW (url, form);
		
		yield return hs_post;
		
		isTransmitting = false;
		
		if (hs_post.error != null) 
		{
			Debug.Log ("UpdateEvents(): ERROR " + hs_post.error );
		}
		else
		{
			Debug.Log( "UpdateEvents(): Success: " + hs_post.text );
		}
	}
	
	/// <summary>
	/// Gets the events.
	/// </summary>
	/// <returns>The events.</returns>
	public static IEnumerator GetEvents()
	{
		isTransmitting = true;
		
		WWWForm form = new WWWForm ();
		
		form.AddField ("id", MCP.userInfo.guid);
		
		WWW hs_get = new WWW (getEventsURL, form);
		
		yield return hs_get;
		
		isTransmitting = false;
		
		if (hs_get.error != null)
		{
			Debug.Log ("GetEvents(): ERROR: " + hs_get.error);
		}
		else
		{
			MCP.userInfo.eventReminders = JsonMapper.ToObject<Events> (hs_get.text);
			
			if (MCP.userInfo.eventReminders == null)
			{
				MCP.userInfo.eventReminders = new Events ();
				MCP.userInfo.eventReminders.eventReminders = new List<EventReminders> ();
			}
			
			Debug.Log ("GetEvents(): Success: Count=" + MCP.userInfo.eventReminders.eventReminders.Count + " JSON= " + hs_get.text);
		}
	}
	
	/// <summary>
	/// Posts the profile photo.
	/// </summary>
	/// <returns>The profile photo.</returns>
	/// <param name="imageBytes">Image bytes.</param>
	public static IEnumerator PostProfilePhoto(byte[] imageBytes)
	{
		isTransmitting = true;
		
		WWWForm form = new WWWForm();
		
		form.AddField ("id", MCP.userInfo.username);
		form.AddBinaryData ("file", imageBytes, "imageUpload.png", "image/png" );
		
		WWW hs_post = new WWW(postProfilePhotoURL, form);
		
		yield return hs_post;
		
		if(hs_post.error != null)
		{
			Debug.Log ("PostProfilePhoto() Error: " + hs_post.error);
		}
		else
		{
			Debug.Log ("PostProfilePhoto() Success: " + hs_post.text);
		}
		
		isTransmitting = false;
	}
	
	/// <summary>
	/// Gets the profile photo.
	/// </summary>
	/// <returns>The profile photo.</returns>
	public static IEnumerator GetProfilePhoto( string name )
	{
		isTransmitting = true;
		
		WWWForm form = new WWWForm();
		
		form.AddField ("id", name);
		
		WWW hs_get = new WWW(getProfilePhotoURL, form);
		
		yield return hs_get;
		
		if(hs_get.error != null)
		{
			Debug.Log ("GetProfilePhoto() Error: " + hs_get.error);
		}
		else
		{
			Debug.Log ("GetProfilePhoto() Success: " + hs_get.text);
			MCP.loadedTexture = hs_get.texture;
		}
		
		isTransmitting = false;
	}
	
	public static IEnumerator GetTaskList(string username)
	{
		isTransmitting = true;
		
		WWWForm form = new WWWForm();
		form.AddField ("id", username);
		
		WWW hs_get = new WWW(getTaskListURL, form);
		
		yield return hs_get;
		
		if(hs_get.error != null)
		{
			Debug.Log ("GetTaskList() Error: " + hs_get.error);
		}
		else
		{
			Debug.Log ("GetTaskList() Success: " + hs_get.text);
			
			string taskListJson = hs_get.text;
			
			try
			{
				MCP.taskList = JsonMapper.ToObject<List<Task>>(taskListJson);
			}
			catch(Exception e)
			{
				Debug.Log ("GetTaskList() Error: " + e);
			}
			
			if(MCP.taskList == null)
			{
				MCP.taskList = new List<Task>();
			}
		}
		
		isTransmitting = false;
	}
	
	public static IEnumerator UpdateTaskList()
	{
		isTransmitting = true;
		
		string taskListJson = JsonMapper.ToJson (taskList);
		
		WWWForm form = new WWWForm();
		form.AddField ("id", MCP.userInfo.guid);
		form.AddField ("username", MCP.userInfo.username);
		form.AddField ("json", taskListJson);
		
		WWW hs_post = new WWW(updateTaskListURL, form);
		
		yield return hs_post;
		
		if(hs_post.error != null)
		{
			Debug.Log ("UpdateTaskList() Error: " + hs_post.error);
		}
		else
		{
			Debug.Log ("UpdateTaskList() Success: " + hs_post.text);
		}
		
		isTransmitting = false;
	}
	
	public static IEnumerator SendMail(string to, string from, string subject, string message, byte[] screenshot)
	{
		isTransmitting = true;
		
		WWWForm form = new WWWForm();
		form.AddField ("to", to);
		form.AddField ("from", from);
		form.AddField ("subject", subject);
		form.AddField ("message", message);
		form.AddBinaryData ("file", screenshot, "imageUpload.png", "image/png");
		
		WWW hs_post = new WWW(postMailURL, form);
		
		yield return hs_post;
		
		if(hs_post.error != null)
		{
			Debug.Log ("SendMail() Error: " + hs_post.error);
		}
		else
		{
			Debug.Log ("SendMail() Success: " + hs_post.text);
		}
		
		isTransmitting = false;
	}
	
	public static IEnumerator GetAssetBundle(string url, bool localFile)
	{
		isTransmitting = true;
		
		if(localFile)
		{
			//TODO: Make work.
			assetBundleDownload = new WWW("file://" + Application.dataPath + "/" + url + "/" + "bronzeresources01.bronze" );
		}
		else
		{
//			assetBundleDownload = new WWW(getAssetBundleURL + url);
			assetBundleDownload = WWW.LoadFromCacheOrDownload(getAssetBundleURL + url, 1);
	  	}
		
		yield return assetBundleDownload;
		
		if(assetBundleDownload != null)
		{
			if(assetBundleDownload.error != null)
			{
				Debug.Log ("GetAssetBundle() Fail: " + assetBundleDownload.error);
			}
			else
			{
				Debug.Log ("GetAssetBundle() Success");
				downloadedAssetBundle = assetBundleDownload.assetBundle;
			}
		}
		
		isTransmitting = false;
	}
	
	public static void StopCurrentAssetBundleDownload()
	{
		if(assetBundleDownload != null)
		{
			assetBundleDownload.Dispose ();
			assetBundleDownload = null;
		}
	}
}
