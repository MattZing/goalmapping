<b><color=#144766>Messages</color></b>

Friends list - <i>View a list of people you have linked with.</i>

Inbox - <i>Access all your current messages.</i>

Sent - <i>View the messages you have previously sent.</i>

Deleted - <i>View the messages you have previously deleted.</i>